
# solsvc

solsvc is an implementation of SOL005 standard, which defines API for Network Service Management and VNF on boarding towards OSS/BSS (Operations Support System / Business Support System) fulfilling the requirements defined in ETSI NFV IFA013

## Installation

cd helm

helm install solsvc solsvc -n <namespace>

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Build docker image

docker build -t \<repository\>/solsvc:\<tag\> .
