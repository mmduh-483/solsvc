package notificationInfra

import (
	"aarna.com/solsvc/lib/common"
	notificationLib "aarna.com/solsvc/lib/nsfmnotification"
	nslcm "aarna.com/solsvc/lib/nslcm"
	"aarna.com/solsvc/pkg/fmnotification"
	statusnotifypb "aarna.com/solsvc/pkg/grpc/statusnotify"
	"aarna.com/solsvc/pkg/subscriptionclient"
	"context"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/encoding/protojson"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"
)

type EmcoNotifyConf struct {
	NsId                string `json:"nsId"`
	Endpoint            string `json:"endpoint"`
	Project             string `json:"project"`
	CompositeApp        string `json:"compositeApp"`
	CompositeAppVersion string `json:"compositeAppVersion"`
	NsName              string `json:"nsName"`
	NsLCMOpOccId        string `json:"nsLCMOpOccId"`
}

type EmcoNotificationHandler struct {
	Manager   *subscriptionclient.SubscriptionClient
	Instances map[string]InstancesStruct
}

type InstancesStruct struct {
	cancel context.CancelFunc
}
type NotificationConf struct {
	Conf json.RawMessage
}

type Event struct {
	ServiceStatusValue string `json:"serviceStatusValue"`
}

func NewEmcoNotificationHandler(manager *subscriptionclient.SubscriptionClient) EmcoNotificationHandler {
	return EmcoNotificationHandler{
		Manager:   manager,
		Instances: make(map[string]InstancesStruct),
	}
}

func (h EmcoNotificationHandler) DeRegisterNotifyEmcoInstantiate(nsId string) {
	if _, ok := h.Instances[nsId]; ok {
		h.Instances[nsId].cancel()
		delete(h.Instances, nsId)
	}
}

func (h EmcoNotificationHandler) RegisterInstanceReadyStatus(c NotificationConf, isRollback bool) error {
	var (
		reg  statusnotifypb.ServiceStatusRegistration
		conf EmcoNotifyConf
	)
	if err := json.Unmarshal(c.Conf, &conf); err != nil {
		return err
	}
	endpoint := conf.Endpoint
	reg.ClientId = uuid.New().String()
	reg.Key = &statusnotifypb.ServiceKey{
		Project: conf.Project,
		Service: conf.NsName,
	}

	conn, err := newGrpcClient(endpoint)
	if err != nil {
		return err
	}

	client := statusnotifypb.NewStatusNotifyClient(conn)
	stream, err := client.ServiceStatusRegister(context.Background(), &reg, grpc.WaitForReady(true))
	if err != nil {
		return err
	}
	ctx, cancel := context.WithCancel(context.Background())
	h.Instances[conf.NsId] = InstancesStruct{cancel: cancel}
	depDetails := conf.Project + "/" + conf.NsName
	log.Infof("Registration successful with Emco notification for %s", depDetails)
	go h.eventWatch(ctx, conf.NsId, conf.NsLCMOpOccId, stream, isRollback)
	return nil
}

func (h EmcoNotificationHandler) eventWatch(ctx context.Context, nsId string, nsLCMOpOccId string, stream statusnotifypb.StatusNotify_ServiceStatusRegisterClient, isRollback bool) {
	var alarm notificationLib.Alarm
	for {
		status, err := stream.Recv()
		if err != nil {
			log.Errorf("Emco event watch error %v", err)
			time.Sleep(5 * time.Second)
			continue
		}
		eventJson, err := protojson.Marshal(status)
		if err != nil {
			log.Errorf("Emco event watch error %v", err)
			continue
		}
		event := Event{}
		if err := json.Unmarshal(eventJson, &event); err != nil {
			log.Errorf("Emco event watch error %v", err)
			continue
		}
		log.Infof("New event recieved: nsid %s, event %v", nsId, event)
		select {
		case <-ctx.Done():
			return
		default:
			if event.ServiceStatusValue == "INSTANTIATED" {
				state := "COMPLETED"
				opType := "INSTANTIATE"
				if isRollback {
					opType = "TERMINATE"
					state = "ROLLED_BACK"
				}
				h.Manager.SendNsLcmOperationOccurrenceNotification(opType, "RESULT", state, nsLCMOpOccId, nsId, time.Now(), nil, nil)
				fmnotification.CreateAlarm(nsId, "CLEARED", notificationLib.QOS_ALARM, "NS is ready")
				common.UpdateNSInstances(opType, state, nsLCMOpOccId, nsId, "")
				h.Manager.SendAlarmClearedNotification(alarm, time.Now())
			}
			log.Infof("Received event: %+v", event)
			if event.ServiceStatusValue == "NOT_READY" {
				alarm = fmnotification.CreateAlarm(nsId, "CRITICAL", notificationLib.QOS_ALARM, "NS not ready")
				h.Manager.SendAlarmNotification(alarm, time.Now())
			}
		}
	}
}

// CreateGrpcClient creates the gRPC Client Connection
func newGrpcClient(endpoint string) (*grpc.ClientConn, error) {
	var err error
	var opts []grpc.DialOption

	opts = append(opts, grpc.WithInsecure())

	conn, err := grpc.Dial(endpoint, opts...)
	if err != nil {
		log.Errorf("Grpc Client Initialization failed with error: %v\n", err)
	}

	return conn, err
}

func (h EmcoNotificationHandler) RegisterNotifyEmcoTerminate(ctx context.Context, conf common.SOLConf, nsInfo common.NsInstanceInfo, nsLCMOpOcc common.NsLCMOpOcc, isRollback bool) {
	go func() {
		problemCount := 0
		for {

			status, problem := h.pollEmcoStatusTerminate(ctx, conf, nsInfo)
			if problem != nil {
				problemCount++
				if problemCount > 10 {
					log.Errorf("Failed to determine terminate status %+v", problem)
					//TODO Need to raise a fault alarm here
					return
				}
			}
			if status {
				opType := "TERMINATE"
				state := "COMPLETED"
				if isRollback {
					opType = "INSTANTIATE"
					state = "ROLLED_BACK"
				}
				common.UpdateNSInstances(opType, state, nsLCMOpOcc.NsLCMOpOccId, nsInfo.NsId, conf.MiddleEnd)
				h.Manager.SendNsLcmOperationOccurrenceNotification(opType, "RESULT", state, nsLCMOpOcc.NsLCMOpOccId, nsInfo.NsId, time.Now(), nil, nil)
				return
			}
			time.Sleep(20 * time.Second)
		}
	}()
}

func (h EmcoNotificationHandler) RegisterNotifyEmcoInstantiate(ctx context.Context, conf common.SOLConf, nsInfo common.NsInstanceInfo, nsLCMOpOcc common.NsLCMOpOcc, isRollback bool) {
	ctx, cancel := context.WithCancel(context.Background())
	h.Instances[nsInfo.NsId] = InstancesStruct{cancel: cancel}
	go h.pollInstantiateStatus(ctx, conf, nsInfo, nsLCMOpOcc, isRollback)
}

func (h EmcoNotificationHandler) pollInstantiateStatus(ctx context.Context, conf common.SOLConf, nsInfo common.NsInstanceInfo, nsLCMOpOcc common.NsLCMOpOcc, isRollback bool) {
	var (
		alarm       notificationLib.Alarm
		nsReady     bool
		executeOnce sync.Once
	)

	startTime := time.Now()
	for {
		select {
		case <-ctx.Done():
			return
		default:
			status, problem := h.pollEmcoStatusInstantiate(ctx, conf, nsInfo)
			log.Debugf("status: %+v", status)
			if problem != nil {
				log.Errorf("Failed to determine instantiate status %+v", problem)
				continue
			}
			if status.DeployedStatus == common.StatusFailed {
				executeOnce.Do(func() {
					state := "FAILED_TEMP"
					opType := "INSTANTIATE"
					if isRollback {
						opType = "TERMINATE"
						state = "ROLLED_BACK"
					}
					common.UpdateNSInstances(opType, state, nsLCMOpOcc.NsLCMOpOccId, nsInfo.NsId, conf.MiddleEnd)
					h.Manager.SendNsLcmOperationOccurrenceNotification(opType, "RESULT", state, nsLCMOpOcc.NsLCMOpOccId, nsInfo.NsId, time.Now(), nil, nil)
					alarm = fmnotification.CreateAlarm(nsInfo.NsId, "CRITICAL", notificationLib.QOS_ALARM, "NS not ready")
					h.Manager.SendAlarmNotification(alarm, time.Now())
				})
			}
			if nsReady && status.DeployedStatus == common.StatusInstantiated && status.ReadyStatus != common.StatusReady {
				log.Infof("NS %s has gone down", nsInfo.NsId)
				alarm = fmnotification.CreateAlarm(nsInfo.NsId, "CRITICAL", notificationLib.QOS_ALARM, "NS not ready")
				h.Manager.SendAlarmNotification(alarm, time.Now())
				nsReady = false
			}

			if status.DeployedStatus == common.StatusInstantiated && status.ReadyStatus == common.StatusReady {
				executeOnce.Do(func() {
					state := "COMPLETED"
					opType := "INSTANTIATE"
					if isRollback {
						opType = "TERMINATE"
						state = "ROLLED_BACK"
					}
					log.Debugf("NS is instantiated and ready")
					common.UpdateNSInstances(opType, state, nsLCMOpOcc.NsLCMOpOccId, nsInfo.NsId, conf.MiddleEnd)
					h.Manager.SendNsLcmOperationOccurrenceNotification(opType, "RESULT", state, nsLCMOpOcc.NsLCMOpOccId, nsInfo.NsId, time.Now(), nil, nil)
					nsReady = true
				})
			}

			if !nsReady && status.DeployedStatus == common.StatusInstantiated && status.ReadyStatus == common.StatusReady {
				log.Info("NS %s has come up and running", nsInfo.NsId)
				h.Manager.SendAlarmClearedNotification(alarm, time.Now())
				nsReady = true
			}

			if status.DeployedStatus == common.StatusInstantiated && status.ReadyStatus != common.StatusReady {
				endTime := time.Now()
				timeDiff := endTime.Sub(startTime).Minutes()
				log.Debugf("NS is instantiated and not ready for last: %d minutes", int(timeDiff))
				if int(timeDiff) > common.WATCH_STATUS_RETRYMAX {
					executeOnce.Do(func() {
						state := "FAILED_TEMP"
						opType := "INSTANTIATE"
						if isRollback {
							opType = "TERMINATE"
							state = "ROLLED_BACK"
						}
						log.Infof("NS %s is instantiated and not ready for last: %d minutes, hence marking it as %s", nsInfo.NsId, int(timeDiff), state)
						common.UpdateNSInstances(opType, state, nsLCMOpOcc.NsLCMOpOccId, nsInfo.NsId, conf.MiddleEnd)
						h.Manager.SendNsLcmOperationOccurrenceNotification(opType, "RESULT", state, nsLCMOpOcc.NsLCMOpOccId, nsInfo.NsId, time.Now(), nil, nil)
						alarm = fmnotification.CreateAlarm(nsInfo.NsId, "CRITICAL", notificationLib.QOS_ALARM, "NS not ready")
						h.Manager.SendAlarmNotification(alarm, time.Now())
					})
				}
			}
			time.Sleep(common.WATCH_STATUS_RETRYDELAY)
		}
	}
}

func (h EmcoNotificationHandler) pollEmcoStatusTerminate(_ context.Context, conf common.SOLConf, nsInfo common.NsInstanceInfo) (bool, *nslcm.ProblemDetails) {
	var errMsg string
	handler := common.HttpHandler{}
	handler.Client = &http.Client{}
	serviceStatus := common.ServiceStatusInfo{}
	URL := "http://" + conf.Orchestrator + "/v2/projects/" +
		common.DEFAULT_PROJECT + "/services/" + nsInfo.NsName + "/status"
	status, data, err := handler.ApiGet(URL)
	if status != http.StatusOK || err != nil {
		errMsg = fmt.Sprintf("Encountered error while fetching status for NS service")
		detail := " EMCO status: " + strconv.Itoa(status.(int))
		if err != nil {
			detail += " Err: " + err.Error()
		}
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: detail,
			Status: int32(status.(int)),
		}
		return false, &problem
	}

	err = json.Unmarshal(data, &serviceStatus)
	if err != nil {
		errMsg = "Failed to unmarshal DIG json: " + common.PrintFunctionName()
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return false, &problem
	}
	digStatus := strings.ToUpper(serviceStatus.DeployedStatus)
	if digStatus == common.NS_LCM_TERMINATED_STATE {
		return true, nil
	}
	return false, nil
}

func (h EmcoNotificationHandler) pollEmcoStatusInstantiate(_ context.Context, conf common.SOLConf, nsInfo common.NsInstanceInfo) (common.ServiceStatusInfo, *nslcm.ProblemDetails) {
	var errMsg string
	handler := common.HttpHandler{}
	handler.Client = &http.Client{}
	serviceStatus := common.ServiceStatusInfo{}
	URL := "http://" + conf.Orchestrator + "/v2/projects/" +
		common.DEFAULT_PROJECT + "/services/" + nsInfo.NsName + "/status"
	status, data, err := handler.ApiGet(URL)
	if status != http.StatusOK || err != nil {
		errMsg = fmt.Sprintf("Encountered error while fetching status for NS service")
		detail := " EMCO status: " + strconv.Itoa(status.(int))
		if err != nil {
			detail += " Err: " + err.Error()
		}
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: detail,
			Status: int32(status.(int)),
		}
		return serviceStatus, &problem
	}

	err = json.Unmarshal(data, &serviceStatus)
	if err != nil {
		errMsg = "Failed to unmarshal DIG json: " + common.PrintFunctionName()
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return serviceStatus, &problem
	}
	return serviceStatus, nil
}
