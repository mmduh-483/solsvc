package notificationInfra

import (
	nslcm "aarna.com/solsvc/lib/nslcm"
	nslcmnotification "aarna.com/solsvc/lib/nslcmnotification"
	"aarna.com/solsvc/pkg/subscriptionclient"
	"time"
)

type LocalNotificationHandler struct {
	UpdateStream chan subscriptionclient.UpdateEvent
	manager      *subscriptionclient.SubscriptionClient
}

func NewLocalNotificationHandler(manager *subscriptionclient.SubscriptionClient) LocalNotificationHandler {
	ch := make(chan subscriptionclient.UpdateEvent)
	h := LocalNotificationHandler{
		UpdateStream: ch,
		manager:      manager,
	}
	go h.WatchLocalSubscriptions()
	return h
}

func (h *LocalNotificationHandler) WatchLocalSubscriptions() {
	for {
		event := <-h.UpdateStream
		switch event.Operation {
		case "NsCreation":
			h.manager.SendNsIdentifierCreationNotification(event.NsId, event.Ts)
		case "NsDeletion":
			h.manager.SendNsIdentifierDeletionNotification(event.NsId, event.Ts)
		case "NsInstantiateStarted":
			h.manager.SendNsLcmOperationOccurrenceNotification("INSTANTIATE", "START", "PROCESSING", event.NsLCMOpOccId, event.NsId, event.Ts, nil, nil)
		case "NsTerminateStarted":
			h.manager.SendNsLcmOperationOccurrenceNotification("TERMINATE", "START", "PROCESSING", event.NsLCMOpOccId, event.NsId, event.Ts, nil, nil)
		case "NsInstantiateFailedTemp":
			h.manager.SendNsLcmOperationOccurrenceNotification("INSTANTIATE", "RESULT", "FAILED_TEMP", event.NsLCMOpOccId, event.NsId, event.Ts, &event.Error, nil)
		case "NsTerminateFailedTemp":
			h.manager.SendNsLcmOperationOccurrenceNotification("TERMINATE", "RESULT", "FAILED_TEMP", event.NsLCMOpOccId, event.NsId, event.Ts, &event.Error, nil)
		case "NsInstantiateRollingBack":
			h.manager.SendNsLcmOperationOccurrenceNotification("INSTANTIATE", "START", "ROLLING_BACK", event.NsLCMOpOccId, event.NsId, event.Ts, nil, nil)
		case "NsTerminateRollingBack":
			h.manager.SendNsLcmOperationOccurrenceNotification("TERMINATE", "START", "ROLLING_BACK", event.NsLCMOpOccId, event.NsId, event.Ts, nil, nil)
		case "NsInstantiateRollBackFailed":
			h.manager.SendNsLcmOperationOccurrenceNotification("INSTANTIATE", "RESULT", "FAILED_TEMP", event.NsLCMOpOccId, event.NsId, event.Ts, &event.Error, nil)
		case "NsTerminateRollBackFailed":
			h.manager.SendNsLcmOperationOccurrenceNotification("TERMINATE", "RESULT", "FAILED_TEMP", event.NsLCMOpOccId, event.NsId, event.Ts, &event.Error, nil)
		case "NsScaleFailedTemp":
			h.manager.SendNsLcmOperationOccurrenceNotification("SCALE", "RESULT", "FAILED_TEMP", event.NsLCMOpOccId, event.NsId, event.Ts, &event.Error, event.AffectedVnf)
		case "NsUpdateFailedTemp":
			h.manager.SendNsLcmOperationOccurrenceNotification("UPDATE", "RESULT", "FAILED_TEMP", event.NsLCMOpOccId, event.NsId, event.Ts, &event.Error, event.AffectedVnf)
		case "NsScaleCompleted":
			h.manager.SendNsLcmOperationOccurrenceNotification("SCALE", "RESULT", "COMPLETED", event.NsLCMOpOccId, event.NsId, event.Ts, nil, event.AffectedVnf)
		case "NsUpdateCompleted":
			h.manager.SendNsLcmOperationOccurrenceNotification("UPDATE", "RESULT", "COMPLETED", event.NsLCMOpOccId, event.NsId, event.Ts, nil, event.AffectedVnf)
		case "NsScaleStarted":
			h.manager.SendNsLcmOperationOccurrenceNotification("SCALE", "START", "PROCESSING", event.NsLCMOpOccId, event.NsId, event.Ts, nil, nil)
		case "NsUpdateStarted":
			h.manager.SendNsLcmOperationOccurrenceNotification("UPDATE", "START", "PROCESSING", event.NsLCMOpOccId, event.NsId, event.Ts, nil, nil)
		case "NsHealProcessing":
			h.manager.SendNsLcmOperationOccurrenceNotification("HEAL", "START", "PROCESSING", event.NsLCMOpOccId, event.NsId, event.Ts, nil, nil)
		case "NsHealFailedTemp":
			h.manager.SendNsLcmOperationOccurrenceNotification("HEAL", "RESULT", "FAILED_TEMP", event.NsLCMOpOccId, event.NsId, event.Ts, &event.Error, nil)
		case "NsHealDone":
			h.manager.SendNsLcmOperationOccurrenceNotification("HEAL", "RESULT", "COMPLETED", event.NsLCMOpOccId, event.NsId, event.Ts, nil, nil)
		case "NsInstantiateCancelStarted":
			h.manager.SendNsLcmOperationOccurrenceNotification("INSTANTIATE", "START", "PROCESSING", event.NsLCMOpOccId, event.NsId, event.Ts, nil, nil)
		case "NsInstantiateContinueStarted":
			h.manager.SendNsLcmOperationOccurrenceNotification("INSTANTIATE", "START", "PROCESSING", event.NsLCMOpOccId, event.NsId, event.Ts, nil, nil)
		case "NsTerminateCancelStarted":
			h.manager.SendNsLcmOperationOccurrenceNotification("TERMINATE", "START", "PROCESSING", event.NsLCMOpOccId, event.NsId, event.Ts, nil, nil)
		case "NsTerminateContinueStarted":
			h.manager.SendNsLcmOperationOccurrenceNotification("TERMINATE", "START", "PROCESSING", event.NsLCMOpOccId, event.NsId, event.Ts, nil, nil)
		}
	}
}

func (h *LocalNotificationHandler) NsCreateDone(nsId string) {
	e := subscriptionclient.UpdateEvent{
		Operation: "NsCreation",
		NsId:      nsId,
		Ts:        time.Now(),
	}
	h.UpdateStream <- e
}

func (h *LocalNotificationHandler) NsDeleteDone(nsId string) {
	e := subscriptionclient.UpdateEvent{
		Operation: "NsDeletion",
		NsId:      nsId,
		Ts:        time.Now(),
	}
	h.UpdateStream <- e
}

func (h *LocalNotificationHandler) NsInstantiateStarted(nsId string, opId string) {
	e := subscriptionclient.UpdateEvent{
		Operation:    "NsInstantiateStarted",
		NsId:         nsId,
		Ts:           time.Now(),
		NsLCMOpOccId: opId,
	}
	h.UpdateStream <- e
}

func (h *LocalNotificationHandler) NsTerminateStarted(nsId string, opId string) {
	e := subscriptionclient.UpdateEvent{
		Operation:    "NsTerminateStarted",
		NsId:         nsId,
		Ts:           time.Now(),
		NsLCMOpOccId: opId,
	}
	h.UpdateStream <- e
}

func (h *LocalNotificationHandler) NsInstantiateFailedTemp(nsId string, opId string, problem nslcm.ProblemDetails) {
	e := subscriptionclient.UpdateEvent{
		Operation:    "NsInstantiateFailedTemp",
		NsId:         nsId,
		Ts:           time.Now(),
		NsLCMOpOccId: opId,
		Error:        nslcmnotification.ProblemDetails(problem),
	}
	h.UpdateStream <- e
}

func (h *LocalNotificationHandler) NsTerminateFailedTemp(nsId string, opId string, problem nslcm.ProblemDetails) {
	e := subscriptionclient.UpdateEvent{
		Operation:    "NsTerminateFailedTemp",
		NsId:         nsId,
		Ts:           time.Now(),
		NsLCMOpOccId: opId,
		Error:        nslcmnotification.ProblemDetails(problem),
	}
	h.UpdateStream <- e
}

func (h *LocalNotificationHandler) NsInstantiateRollingBack(nsId string, opId string) {
	e := subscriptionclient.UpdateEvent{
		Operation:    "NsInstantiateRollingBack",
		NsId:         nsId,
		Ts:           time.Now(),
		NsLCMOpOccId: opId,
	}
	h.UpdateStream <- e
}

func (h *LocalNotificationHandler) NsTerminateRollingBack(nsId string, opId string) {
	e := subscriptionclient.UpdateEvent{
		Operation:    "NsInstantiateRollingBack",
		NsId:         nsId,
		Ts:           time.Now(),
		NsLCMOpOccId: opId,
	}
	h.UpdateStream <- e
}

func (h *LocalNotificationHandler) NsInstantiateRollBackFailed(nsId string, opId string, problem nslcm.ProblemDetails) {
	e := subscriptionclient.UpdateEvent{
		Operation:    "NsInstantiateRollBackFailed",
		NsId:         nsId,
		Ts:           time.Now(),
		NsLCMOpOccId: opId,
		Error:        nslcmnotification.ProblemDetails(problem),
	}
	h.UpdateStream <- e
}

func (h *LocalNotificationHandler) NsTerminateRollBackFailed(nsId string, opId string, problem nslcm.ProblemDetails) {
	e := subscriptionclient.UpdateEvent{
		Operation:    "NsTerminateRollBackFailed",
		NsId:         nsId,
		Ts:           time.Now(),
		NsLCMOpOccId: opId,
		Error:        nslcmnotification.ProblemDetails(problem),
	}
	h.UpdateStream <- e
}

func (h *LocalNotificationHandler) NsScaleStarted(nsId string, opId string) {
	e := subscriptionclient.UpdateEvent{
		Operation:    "NsScaleStarted",
		NsId:         nsId,
		Ts:           time.Now(),
		NsLCMOpOccId: opId,
	}
	h.UpdateStream <- e
}

func (h *LocalNotificationHandler) NsHealProcessing(nsId string, opId string) {
	e := subscriptionclient.UpdateEvent{
		Operation:    "NsHealProcessing",
		NsId:         nsId,
		Ts:           time.Now(),
		NsLCMOpOccId: opId,
	}
	h.UpdateStream <- e
}

func (h *LocalNotificationHandler) NsScaleFailedTemp(nsId string, opId string, problem nslcm.ProblemDetails, affectedVnf []nslcmnotification.AffectedVnf) {
	e := subscriptionclient.UpdateEvent{
		Operation:    "NsScaleFailedTemp",
		NsId:         nsId,
		Ts:           time.Now(),
		NsLCMOpOccId: opId,
		Error:        nslcmnotification.ProblemDetails(problem),
		AffectedVnf:  affectedVnf,
	}
	h.UpdateStream <- e
}

func (h *LocalNotificationHandler) NsHealFailedTemp(nsId string, opId string, problem nslcm.ProblemDetails) {
	e := subscriptionclient.UpdateEvent{
		Operation:    "NsHealFailedTemp",
		NsId:         nsId,
		Ts:           time.Now(),
		NsLCMOpOccId: opId,
		Error:        nslcmnotification.ProblemDetails(problem),
	}
	h.UpdateStream <- e
}

func (h *LocalNotificationHandler) NsScaleCompleted(nsId string, opId string, affectedVnf []nslcmnotification.AffectedVnf) {
	e := subscriptionclient.UpdateEvent{
		Operation:    "NsScaleCompleted",
		NsId:         nsId,
		Ts:           time.Now(),
		NsLCMOpOccId: opId,
		AffectedVnf:  affectedVnf,
	}
	h.UpdateStream <- e
}

func (h *LocalNotificationHandler) NsUpdateStarted(nsId string, opId string) {
	e := subscriptionclient.UpdateEvent{
		Operation:    "NsUpdateStarted",
		NsId:         nsId,
		Ts:           time.Now(),
		NsLCMOpOccId: opId,
	}
	h.UpdateStream <- e
}

func (h *LocalNotificationHandler) NsUpdateFailedTemp(nsId string, opId string, problem nslcm.ProblemDetails, affectedVnf []nslcmnotification.AffectedVnf) {
	e := subscriptionclient.UpdateEvent{
		Operation:    "NsUpdateFailedTemp",
		NsId:         nsId,
		Ts:           time.Now(),
		NsLCMOpOccId: opId,
		Error:        nslcmnotification.ProblemDetails(problem),
		AffectedVnf:  affectedVnf,
	}
	h.UpdateStream <- e
}

func (h *LocalNotificationHandler) NsUpdateCompleted(nsId string, opId string, affectedVnf []nslcmnotification.AffectedVnf) {
	e := subscriptionclient.UpdateEvent{
		Operation:    "NsUpdateCompleted",
		NsId:         nsId,
		Ts:           time.Now(),
		NsLCMOpOccId: opId,
		AffectedVnf:  affectedVnf,
	}
	h.UpdateStream <- e
}

func (h *LocalNotificationHandler) NsHealDone(nsId string, opId string) {
	e := subscriptionclient.UpdateEvent{
		Operation:    "NsHealDone",
		NsId:         nsId,
		Ts:           time.Now(),
		NsLCMOpOccId: opId,
	}
	h.UpdateStream <- e
}

func (h *LocalNotificationHandler) NsInstantiateCancelStarted(nsId string, opId string) {
	e := subscriptionclient.UpdateEvent{
		Operation:    "NsInstantiateCancelStarted",
		NsId:         nsId,
		Ts:           time.Now(),
		NsLCMOpOccId: opId,
	}
	h.UpdateStream <- e
}

func (h *LocalNotificationHandler) NsInstantiateContinueStarted(nsId string, opId string) {
	e := subscriptionclient.UpdateEvent{
		Operation:    "NsInstantiateContinueStarted",
		NsId:         nsId,
		Ts:           time.Now(),
		NsLCMOpOccId: opId,
	}
	h.UpdateStream <- e
}

func (h *LocalNotificationHandler) NsTerminateCancelStarted(nsId string, opId string) {
	e := subscriptionclient.UpdateEvent{
		Operation:    "NsTerminateCancelStarted",
		NsId:         nsId,
		Ts:           time.Now(),
		NsLCMOpOccId: opId,
	}
	h.UpdateStream <- e
}

func (h *LocalNotificationHandler) NsTerminateContinueStarted(nsId string, opId string) {
	e := subscriptionclient.UpdateEvent{
		Operation:    "NsTerminateContinueStarted",
		NsId:         nsId,
		Ts:           time.Now(),
		NsLCMOpOccId: opId,
	}
	h.UpdateStream <- e
}
