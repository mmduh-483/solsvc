package fmsubscription

import (
	"aarna.com/solsvc/db"
	"aarna.com/solsvc/lib/common"
	notificationLib "aarna.com/solsvc/lib/nsfmnotification"
	fmsubscriptionLib "aarna.com/solsvc/lib/nsfmsubscription"
	nslcm "aarna.com/solsvc/lib/nslcm"
	"aarna.com/solsvc/pkg/fmnotification"
	"aarna.com/solsvc/pkg/notificationInfra"
	"aarna.com/solsvc/pkg/subscriptionclient"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"net/http"
	"regexp"
	"strings"
	"time"
)

var NS_COLLECTION = "nsinfo"

type Client struct {
	SubscriptionClient      subscriptionclient.SubscriptionClient
	EmcoNotificationHandler notificationInfra.EmcoNotificationHandler
	Conf                    SOLConf
}

type SOLConf struct {
	OwnPort            string `json:"ownport"`
	MiddleEnd          string `json:"middleend"`
	Orchestrator       string `json:"orchestrator"`
	Mongo              string `json:"mongo"`
	LogLevel           string `json:"logLevel"`
	StoreName          string `json:"storeName"`
	Dcm                string `json:"dcm"`
	OrchestratorStatus string `json:"orchestratorStatus"`
}

type GrafanaAlert struct {
	Receiver string `json:"receiver"`
	Status   string `json:"status"`
	Alerts   []struct {
		Status string `json:"status"`
		Labels struct {
			Alertname     string `json:"alertname"`
			GrafanaFolder string `json:"grafana_folder"`
		} `json:"labels"`
		Annotations struct {
		} `json:"annotations"`
		StartsAt     time.Time `json:"startsAt"`
		EndsAt       time.Time `json:"endsAt"`
		GeneratorURL string    `json:"generatorURL"`
		Fingerprint  string    `json:"fingerprint"`
		SilenceURL   string    `json:"silenceURL"`
		DashboardURL string    `json:"dashboardURL"`
		PanelURL     string    `json:"panelURL"`
		ValueString  string    `json:"valueString"`
	} `json:"alerts"`
	GroupLabels struct {
		Alertname     string `json:"alertname"`
		GrafanaFolder string `json:"grafana_folder"`
	} `json:"groupLabels"`
	CommonLabels struct {
		Alertname     string `json:"alertname"`
		GrafanaFolder string `json:"grafana_folder"`
	} `json:"commonLabels"`
	CommonAnnotations struct {
	} `json:"commonAnnotations"`
	ExternalURL     string `json:"externalURL"`
	Version         string `json:"version"`
	GroupKey        string `json:"groupKey"`
	TruncatedAlerts int    `json:"truncatedAlerts"`
	OrgID           int    `json:"orgId"`
	Title           string `json:"title"`
	State           string `json:"state"`
	Message         string `json:"message"`
}

// Receive grafana alerts
func (c Client) ReceiveAlerts(w http.ResponseWriter, r *http.Request) {
	var (
		errMsg string
		alert  GrafanaAlert
		summary string
		severity string
	)

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&alert)
	log.Infof("received grafana alert: %+v", alert)
	if err != nil {
		errMsg = fmt.Sprintf("Failed to parse json: %s", err)
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusBadRequest, w, r)
		return
	}

	// Parse labels info
	if len(alert.Alerts) > 0 {
		stringToParse := alert.Alerts[0].ValueString
		re1 := regexp.MustCompile("labels=\\{(.*?)\\}\\s")
		matched := re1.FindAllString(stringToParse, 1)
		log.Debugf("matched expression: %+v", matched)
		re2 := regexp.MustCompile("emco_service_[0-9]+=[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{12}")
		if len(matched) > 0 {
			log.Debugf("Grafana Labels: %+v", matched[0])
			serviceList := re2.FindAllString(matched[0], -1)
			log.Debugf("serviceList: %+v", serviceList)
			re3 := regexp.MustCompile("summary = (.*?)\\n")
			summaryInfo := re3.FindAllString(alert.Message, 1)
			log.Debugf("summaryInfo: %+v", summaryInfo)
			re4 := regexp.MustCompile("Severity = (.*?)\\n")
			severityInfo := re4.FindAllString(alert.Message, 1)
			if len(summaryInfo) > 0 {
				summaryList := strings.Split(summaryInfo[0], "=")
				summary = strings.TrimSuffix(summaryList[1], "\n")
			}
			if len(severityInfo) > 0 {
				severityInfoList := strings.Split(severityInfo[0], "=")
				severity = strings.TrimSuffix(severityInfoList[1], "\n")
				log.Infof("Severity is: %s", severity)
			}
			for _, id := range serviceList {
				idInfo := strings.Split(id, "=")
				nsId := idInfo[1]
				if len(summary) == 0 {
					summary = fmt.Sprintf("NS %s has encountered an error", nsId)
				}
				alarm := fmnotification.CreateAlarm(nsId, severity, notificationLib.QOS_ALARM, summary)
				c.EmcoNotificationHandler.Manager.SendAlarmNotification(alarm, time.Now())
			}
		}
	}
}

// Fetches all current alarms created in the systems
func (c Client) GetAlarms(w http.ResponseWriter, r *http.Request) {
	values, skip, limit, maxLimit := common.QueryLCMObjects(w, r, "alarm")

	if skip > maxLimit {
		errMsg := "Invalid nextpage_opaque_marker"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
		}
		common.WriteError(problem, http.StatusBadRequest, w, r)
		return
	}

	// UnMarshal the fetched alarms data
	alarmList := make([]fmsubscriptionLib.Alarm, 0)
	for _, val := range values {
		alarmInfo := fmsubscriptionLib.Alarm{}
		err := db.DBconn.Unmarshal(val, &alarmInfo)
		log.Infof("alarmInfo after Unmarshalling: %v", alarmInfo)
		if err != nil {
			errMsg := fmt.Sprintf("Unmarshalling alarmInfo failed: %s", err)
			log.Errorf(errMsg)
			return
		}
		alarmList = append(alarmList, alarmInfo)
	}
	log.Debugf("alarmList: %+v", alarmList)
	result := alarmList[skip:limit]
	retval, err := json.Marshal(result)
	if err != nil {
		errMsg := fmt.Sprintf("Marshalling alarmList failed: %s", err)
		log.Errorf(errMsg)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(retval)
}

// Fetches specified alarm in the database
func (c Client) GetAlarm(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	// Fetch alarmInfo from database
	retCode, alarmInfo, err := common.FetchNSAlarm(vars["alarmId"])
	if retCode != http.StatusOK {
		errMsg := "encountered error while fetching alarmInfo"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, retCode, w, r)
		return
	}

	log.Infof("alarmInfo: %+v", alarmInfo)
	response, err := json.Marshal(alarmInfo)
	if err != nil {
		errMsg := "Encountered error while marshalling alarmInfo"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

// Acknowledge specified alarm in the database
func (c Client) AcknowledgeAlarm(w http.ResponseWriter, r *http.Request) {
	var (
		alarmModReq fmsubscriptionLib.AlarmModifications
		errMsg      string
	)

	vars := mux.Vars(r)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&alarmModReq)
	if err != nil {
		errMsg = fmt.Sprintf("Failed to parse json: %s", err)
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusBadRequest, w, r)
		return
	}

	// Validate ACKSTATE
	if alarmModReq.AckState != common.ACKNOWLEDGED {
		errMsg := fmt.Sprintf("Invalid ackState provided, valid value is %s", common.ACKNOWLEDGED)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
		}
		common.WriteError(problem, http.StatusBadRequest, w, r)
		return
	}

	// Fetch alarmInfo from database
	retCode, alarmInfo, err := common.FetchNSAlarm(vars["alarmId"])
	if retCode != http.StatusOK {
		errMsg := "encountered error while fetching alarmInfo"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, retCode, w, r)
		return
	}

	if alarmInfo.AckState == common.ACKNOWLEDGED {
		errMsg := fmt.Sprintf("Alarm %s is already in %s state", vars["alarmId"], common.ACKNOWLEDGED)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
		}
		common.WriteError(problem, http.StatusConflict, w, r)
		return
	}

	alarmInfo.AckState = alarmModReq.AckState

	key := new(common.AlarmKey)
	key.AlarmId = alarmInfo.Id
	err = db.DBconn.Insert(NS_COLLECTION, key, nil, "alarm", alarmInfo)
	if err != nil {
		errMsg := fmt.Sprintf("Encountered error during insert of alarm %s: %s", alarmInfo.Id, err)
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}
	log.Infof("Alarm %+v successfully updated in database", alarmInfo)

	response, err := json.Marshal(alarmModReq)
	if err != nil {
		errMsg := "Encountered error while marshalling alarmModReq"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

// Subscribe to alarms related to NSs
func (c Client) SubscribeAlarms(w http.ResponseWriter, r *http.Request) {
	var (
		alarmSubReq fmsubscriptionLib.FmSubscriptionRequest
		errMsg      string
	)

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&alarmSubReq)
	if err != nil {
		errMsg = fmt.Sprintf("Failed to parse json: %s", err)
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusBadRequest, w, r)
		return
	}

	subscription, err := c.CreateFMSubscription(alarmSubReq, r)
	if err != nil {
		title := "Create FM Subscription failed"
		problem := nslcm.ProblemDetails{
			Type:     nil,
			Title:    &title,
			Status:   0,
			Detail:   err.Error(),
			Instance: nil,
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}
	subscriptionJson, err := json.Marshal(subscription)
	if err != nil {
		title := "Create FM Subscription failed"
		problem := nslcm.ProblemDetails{
			Type:     nil,
			Title:    &title,
			Status:   0,
			Detail:   err.Error(),
			Instance: nil,
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}
	log.Infof("Create subscription successful %s, %s", subscription.Id, subscription.CallbackUri)
	w.Header().Set("Location", "url")
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	_, err = w.Write(subscriptionJson)
	if err != nil {
		log.Errorf("Error while http write: %v", err.Error())
	}

}

// Fetch all FM subscriptions to alarms related to NSs
func (c Client) GetFMSubcriptions(w http.ResponseWriter, r *http.Request) {
	values, skip, limit, maxLimit := common.QueryLCMObjects(w, r, "fm_subscription")

	if skip > maxLimit {
		errMsg := "Invalid nextpage_opaque_marker"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
		}
		common.WriteError(problem, http.StatusBadRequest, w, r)
		return
	}

	// UnMarshal the fetched alarms data
	subscriptionList := make([]fmsubscriptionLib.FmSubscription, 0)
	for _, val := range values {
		subscriptionInfo := fmsubscriptionLib.FmSubscription{}
		err := db.DBconn.Unmarshal(val, &subscriptionInfo)
		log.Infof("subscriptionInfo after Unmarshalling: %v", subscriptionInfo)
		if err != nil {
			errMsg := fmt.Sprintf("Unmarshalling subscriptionInfo failed: %s", err)
			log.Errorf(errMsg)
			return
		}
		subscriptionList = append(subscriptionList, subscriptionInfo)
	}
	log.Debugf("subscriptionList: %+v", subscriptionList)
	result := subscriptionList[skip:limit]
	retval, err := json.Marshal(result)
	if err != nil {
		errMsg := fmt.Sprintf("Marshalling subscriptionList failed: %s", err)
		log.Errorf(errMsg)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(retval)
}

// Fetch a given FM subscription
func (c Client) GetFMSubcription(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	// Fetch alarmInfo from database
	retCode, subscriptionInfo, err := common.FetchFMSubcription(vars["subscriptionId"])
	if retCode != http.StatusOK {
		errMsg := "encountered error while fetching subscriptionInfo"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, retCode, w, r)
		return
	}

	log.Infof("subscriptionInfo: %+v", subscriptionInfo)
	response, err := json.Marshal(subscriptionInfo)
	if err != nil {
		errMsg := "Encountered error while marshalling subscriptionInfo"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

// Terminates a given FM subscription
func (c Client) DeleteFMSubcription(w http.ResponseWriter, r *http.Request) {
	var (
		errMsg string
	)
	vars := mux.Vars(r)

	// Fetch alarmInfo from database
	retCode, subscriptionInfo, err := common.FetchFMSubcription(vars["subscriptionId"])
	if retCode != http.StatusOK {
		errMsg := "encountered error while fetching subscriptionInfo"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, retCode, w, r)
		return
	}

	log.Infof("subscriptionInfo: %+v", subscriptionInfo)

	// Remove subcription from database
	var key subscriptionKey
	key.Id = vars["subscriptionId"]
	err = db.DBconn.Remove(common.NS_COLLECTION, key)
	if err != nil {
		errMsg = fmt.Sprintf("Encountered error during removal of FM subcription %s : %s", key.Id, err)
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	// If success, return the last success status
	w.WriteHeader(http.StatusNoContent)
}
