package fmsubscription

import (
	"aarna.com/solsvc/db"
	"aarna.com/solsvc/lib/common"
	fmsubscriptionLib "aarna.com/solsvc/lib/nsfmsubscription"
	uuid2 "github.com/google/uuid"
	"github.com/pkg/errors"
	"net/http"
)

type subscriptionKey struct {
	Id string `json:"id"`
}

func (c Client) CreateFMSubscription(request fmsubscriptionLib.FmSubscriptionRequest, r *http.Request) (fmsubscriptionLib.FmSubscription, error) {
	id, err := NewSubscriptionId()
	if err != nil {
		return fmsubscriptionLib.FmSubscription{}, err
	}
	subscription := fmsubscriptionLib.FmSubscription {
		Id:          id,
		Filter:      request.Filter,
		CallbackUri: request.CallbackUri,
		Links: fmsubscriptionLib.AlarmLinks{Self: fmsubscriptionLib.Link{Href: "http://" + r.Host + r.URL.String() + "/" + id}},
	}
	if err := c.VerifyCallbackUri(request.CallbackUri); err != nil {
		return fmsubscriptionLib.FmSubscription{}, errors.Wrap(err, "CallbackUri verification failed")
	}
	if err := c.VerifyNotificationTypes(request.Filter.NotificationTypes); err != nil {
		return fmsubscriptionLib.FmSubscription{}, err
	}
	if request.HasAuthentication() {
		if err := storeFMAuthentication(id, request.Authentication); err != nil {
			return fmsubscriptionLib.FmSubscription{}, err
		}
	}

	err = storeFMSubscription(subscription)
	if err != nil {
		return fmsubscriptionLib.FmSubscription{}, err
	}
	go c.SubscriptionClient.UpdateFMSubscriptions()
	return subscription, nil
}

func storeFMAuthentication(subscriptionId string, auth interface{}) error {
	key := subscriptionKey{
		Id: subscriptionId,
	}
	tag := "fm_subscription_auth"
	return db.DBconn.Insert(common.NS_COLLECTION, key, nil, tag, auth)
}

func (c Client) VerifyCallbackUri(url string) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	resp.Body.Close()
	return nil
}

func (c Client) VerifyNotificationTypes(types []string) error {
	supportedTypes := map[string]bool{
		"AlarmNotification": true,
		"AlarmClearedNotification": true,
		"AlarmListRebuiltNotification": true,
	}
	for _, t := range types {
		if valid := supportedTypes[t]; !valid {
			return errors.New("Unsupported Notification type " + t)
		}
	}
	return nil
}

func (c Client) GetFMSubscription(subscriptionId string) (*fmsubscriptionLib.FmSubscription, error) {
	return getFMSubscriptionFromDb(subscriptionId)
}

func (c Client) DeleteFMSubscription(subscriptionId string) error {
	return deleteFMSubscription(subscriptionId)
}

func (c Client) GetAllFMSubscriptions() ([]fmsubscriptionLib.FmSubscription, error) {
	subscriptions, err := c.SubscriptionClient.GetAllFMSubscriptions()
	if err != nil {
		return nil, errors.Wrap(err, "GetAllFMSubscriptions: error while reading from db")
	}
	return subscriptions, nil
}

func getFMSubscriptionFromDb(id string) (*fmsubscriptionLib.FmSubscription, error) {
	if exists := db.DBconn.CheckCollectionExists(common.NS_COLLECTION); !exists {
		return nil, errors.New("Collections doesn;t exits")
	}
	tag := "fm_subscription"
	key := subscriptionKey{Id: id}
	values, err := db.DBconn.Find(common.NS_COLLECTION, key, tag)
	if err != nil || len(values) == 0 {
		return nil, errors.Wrap(err, "getFMSubscriptionFromDb: error while reading from db")
	}
	return unMarshalSubscription(values[0])
}

func unMarshalSubscription(item []byte) (*fmsubscriptionLib.FmSubscription, error) {
	s := fmsubscriptionLib.FmSubscription{}
	if err := db.DBconn.Unmarshal(item, &s); err != nil {
		return nil, err
	}
	return &s, nil
}

func storeFMSubscription(subscription fmsubscriptionLib.FmSubscription) error {
	key := subscriptionKey{
		Id: subscription.Id,
	}
	tag := "fm_subscription"
	return db.DBconn.Insert(common.NS_COLLECTION, key, nil, tag, subscription)
}

func deleteFMSubscription(id string) error {
	key := subscriptionKey{
		Id: id,
	}
	err := db.DBconn.Remove(common.NS_COLLECTION, key)
	if err != nil {
		return errors.Wrap(err, "deleteSubscription: Could not delete from db")
	}
	return nil
}

func NewSubscriptionId() (string, error) {
	uuid := uuid2.New()
	return uuid.String(), nil
}
