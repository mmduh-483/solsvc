package subscriptionclient

import (
	"aarna.com/solsvc/db"
	"aarna.com/solsvc/lib/common"
	nsfmnotification "aarna.com/solsvc/lib/nsfmnotification"
	fmsubscriptionLib "aarna.com/solsvc/lib/nsfmsubscription"
	nslcmnotification "aarna.com/solsvc/lib/nslcmnotification"
	subscriptionLib "aarna.com/solsvc/lib/nslcmsubscription"
	"aarna.com/solsvc/pkg/fmnotification"
	"aarna.com/solsvc/pkg/lcmnotification"
	"context"
	"encoding/json"
	uuid2 "github.com/google/uuid"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"strings"
	"sync"
	"time"
)

type UpdateEvent struct {
	Operation    string
	NsId         string
	Ts           time.Time
	NsLCMOpOccId string
	Error        nslcmnotification.ProblemDetails
	AffectedVnf  []nslcmnotification.AffectedVnf
}

type SubscriptionClient struct {
	Subscriptions []Subscription
	TokenCache    *common.TokenCacheStore
}

type Subscription struct {
	id               string
	category         string
	subscriptionType string
	details          json.RawMessage
}

var subcriptionClient *SubscriptionClient
var once sync.Once

func (c *SubscriptionClient) GetDummycmSubscriptions() ([]subscriptionLib.LccnSubscription, error) {
	n := subscriptionLib.LccnSubscription{}
	n.Id = "susb-sss"
	n.CallbackUri = "http://127.0.0.1:8080/hello"
	n.Filter = &subscriptionLib.LifecycleChangeNotificationsFilter{}
	n.Filter.NotificationTypes = []string{"NsIdentifierCreationNotification", "NsLcmOperationOccurrenceNotification", "NsIdentifierDeletionNotification"}
	return []subscriptionLib.LccnSubscription{n}, nil
}

// Implemented singleton pattern to ensure we have only single instance of subscription client
func getSubcriptionClientInstance() *SubscriptionClient {
	if subcriptionClient == nil {
		once.Do(
			func() {
				cache := common.IntializeTokenCache()
				subcriptionClient = &SubscriptionClient{Subscriptions: []Subscription{}, TokenCache: &cache}
				log.Infof("subscriptionClient singleton object created")
			})
	} else {
		log.Infof("subscriptionClient object already exists...")
	}
	return subcriptionClient
}

func InitializeSubscriptionClient() SubscriptionClient {
	c := getSubcriptionClientInstance()
	go c.PeriodicUpdate()
	return *c
}

func (c *SubscriptionClient) PeriodicUpdate() {
	for {
		c.UpdateLcmSubscriptions()
		c.UpdateFMSubscriptions()
		time.Sleep(5 * time.Minute)
	}
}

func (c *SubscriptionClient) UpdateLcmSubscriptions() {
	var entries []Subscription
	subscriptions, err := c.GetAllLcmSubscriptions()
	if err != nil {
		log.Fatal(err)
	}
	for _, s := range subscriptions {
		details, err := json.Marshal(s)
		if err != nil {
			log.Warn("Ignored error while updating subscriptions", err.Error())
			continue
		}
		for _, nType := range s.Filter.NotificationTypes {
			entry := Subscription{
				category:         "nslcm",
				id:               s.Id,
				subscriptionType: nType,
				details:          details,
			}
			entries = append(entries, entry)
		}
	}
	c.Subscriptions = entries
}

func (c *SubscriptionClient) UpdateFMSubscriptions() {
	var entries []Subscription
	subscriptions, err := c.GetAllFMSubscriptions()
	if err != nil {
		log.Fatal(err)
	}
	for _, s := range subscriptions {
		details, err := json.Marshal(s)
		if err != nil {
			log.Warn("Ignored error while updating subscriptions", err.Error())
			continue
		}
		for _, nType := range s.Filter.NotificationTypes {
			entry := Subscription{
				category:         "nsfm",
				id:               s.Id,
				subscriptionType: nType,
				details:          details,
			}
			entries = append(entries, entry)
		}
	}
	c.Subscriptions = entries
}

func (c *SubscriptionClient) SendAlarmNotification(alarm nsfmnotification.Alarm, ts time.Time) {
	c.UpdateFMSubscriptions()
	for _, s := range c.Subscriptions {
		log.Debugf("subscription: %+v", s)
		if s.category == "nsfm" && s.subscriptionType == "AlarmNotification" {
			var details fmsubscriptionLib.FmSubscription
			notification := EncodeAlarmNotification(alarm, ts, s)
			if !checkNotificationTypeFMFilter(s.subscriptionType, s.details) {
				log.Infof("Notification skipped after filtering (notification type) notification= %v, nsId = %v, subscription = %v", notification, alarm.ManagedObjectId, s.id)
				continue
			}
			if !checkNsInstanceSubscriptionFilter(alarm.ManagedObjectId, s.details) {
				log.Infof("Notification skipped after filtering notification= %v, nsId = %v, subscription = %v", notification, alarm.ManagedObjectId, s.id)
				continue
			}
			if err := json.Unmarshal(s.details, &details); err != nil {
				log.Warn("SendAlarmNotification:", err.Error())
				continue
			}

			notification.SubscriptionId = s.id
			ctx := context.Background()
			auth, err := common.GetFMSubcriptionAuth(s.id)
			if err != nil {
				log.Warn("SendAlarmNotification: Failure while reading authentication details.This could be temporary issue")
				log.Warn("Ignoring the error. Notification not send", err.Error())
				continue
			}
			if auth != nil && auth.AuthType != nil && len(auth.AuthType) > 0 {
				authType := strings.ToUpper(auth.AuthType[0])
				switch authType {
				case "BASIC":
					authParam := nsfmnotification.BasicAuth{
						UserName: *auth.ParamsBasic.UserName,
						Password: *auth.ParamsBasic.Password,
					}
					ctx = context.WithValue(ctx, nsfmnotification.ContextBasicAuth, authParam)
				case "OAUTH2_CLIENT_CREDENTIALS":
					authParamOAuth := common.OAuth{
						Id:       *auth.ParamsOauth2ClientCredentials.ClientId,
						Password: *auth.ParamsOauth2ClientCredentials.ClientPassword,
						Url:      *auth.ParamsOauth2ClientCredentials.TokenEndpoint,
						Cache:    c.TokenCache,
					}
					ctx = context.WithValue(ctx, nsfmnotification.ContextOAuth2, authParamOAuth)
				default:
					log.Warn("SendAlarmNotification: Auth type not supported", auth.AuthType)
				}
			}
			go func() {
				retryTimeout := time.Now().Add(common.NotificationRetryMax)
				for time.Now().Before(retryTimeout) {
					response, err := fmnotification.HandleAlarmNotification(ctx, notification, details.CallbackUri)
					if err == nil {
						log.Info("Successfully send notification. Response:", response)
						return
					}
					log.Warn("SendAlarmNotification  error while processing notification", err.Error())
					time.Sleep(common.NotificationRetryDelay)
				}
			}()
		}
	}
}

func (c *SubscriptionClient) SendAlarmClearedNotification(alarm nsfmnotification.Alarm, ts time.Time) {
	c.UpdateFMSubscriptions()
	for _, s := range c.Subscriptions {
		log.Debugf("subscription: %+v", s)
		if s.category == "nsfm" && s.subscriptionType == "AlarmClearedNotification" {
			var details fmsubscriptionLib.FmSubscription
			notification := EncodeAlarmClearedNotification(alarm, ts, s)
			if !checkNotificationTypeFMFilter(s.subscriptionType, s.details) {
				log.Infof("Notification skipped after filtering (notification type) notification= %v, nsId = %v, subscription = %v", notification, alarm.ManagedObjectId, s.id)
				continue
			}
			if !checkNsInstanceSubscriptionFilter(alarm.ManagedObjectId, s.details) {
				log.Infof("Notification skipped after filtering notification= %v, nsId = %v, subscription = %v", notification, alarm.ManagedObjectId, s.id)
				continue
			}
			if err := json.Unmarshal(s.details, &details); err != nil {
				log.Warn("SendAlarmClearedNotification:", err.Error())
				continue
			}

			notification.SubscriptionId = s.id
			ctx := context.Background()
			auth, err := common.GetFMSubcriptionAuth(s.id)
			if err != nil {
				log.Warn("SendAlarmClearedNotification: Failure while reading authentication details.This could be temporary issue")
				log.Warn("Ignoring the error. Notification not send", err.Error())
				continue
			}
			if auth != nil && auth.AuthType != nil && len(auth.AuthType) > 0 {
				authType := strings.ToUpper(auth.AuthType[0])
				switch authType {
				case "BASIC":
					authParam := nsfmnotification.BasicAuth{
						UserName: *auth.ParamsBasic.UserName,
						Password: *auth.ParamsBasic.Password,
					}
					ctx = context.WithValue(ctx, nsfmnotification.ContextBasicAuth, authParam)
				case "OAUTH2_CLIENT_CREDENTIALS":
					authParamOAuth := common.OAuth{
						Id:       *auth.ParamsOauth2ClientCredentials.ClientId,
						Password: *auth.ParamsOauth2ClientCredentials.ClientPassword,
						Url:      *auth.ParamsOauth2ClientCredentials.TokenEndpoint,
						Cache:    c.TokenCache,
					}
					ctx = context.WithValue(ctx, nsfmnotification.ContextOAuth2, authParamOAuth)
				default:
					log.Warn("SendAlarmClearedNotification: Auth type not supported", auth.AuthType)
				}
			}

			go func() {
				retryTimeout := time.Now().Add(common.NotificationRetryMax)
				for time.Now().Before(retryTimeout) {
					response, err := fmnotification.HandleAlarmClearedNotification(ctx, notification, details.CallbackUri)
					if err == nil {
						log.Info("Successfully send notification. Response:", response)
						return
					}
					log.Warn("SendAlarmClearedNotification  error while processing notification", err.Error())
					time.Sleep(common.NotificationRetryDelay)
				}
			}()
		}
	}
}

func (c *SubscriptionClient) SendNsIdentifierCreationNotification(nsId string, ts time.Time) {
	notification := EncodeNsIdentifierCreationNotification(nsId, ts)
	c.UpdateLcmSubscriptions()
	for _, s := range c.Subscriptions {
		var details subscriptionLib.LccnSubscription
		if s.category == "nslcm" && s.subscriptionType == "NsIdentifierCreationNotification" {
			if !checkNsInstanceSubscriptionFilter(nsId, s.details) {
				log.Infof("Notification skipped after filtering notification= %v, nsId = %v, subscription = %v", notification, nsId, s.id)
				continue
			}
			if err := json.Unmarshal(s.details, &details); err != nil {
				log.Warn("SendNsIdentifierCreationNotification Ignored error while processing notification.", err.Error())
				continue
			}

			notification.SubscriptionId = s.id
			ctx := context.Background()
			auth, err := common.GetLCMSubcriptionAuth(s.id)
			if err != nil {
				log.Warn("SendNsIdentifierCreationNotification: Failure while reading authentication details.This could be temporary issue")
				log.Warn("Ignoring the error. Notification not send", err.Error())
				continue
			}
			if auth != nil && auth.AuthType != nil && len(auth.AuthType) > 0 {
				authType := strings.ToUpper(auth.AuthType[0])
				switch authType {
				case "BASIC":
					authParam := nslcmnotification.BasicAuth{
						UserName: *auth.ParamsBasic.UserName,
						Password: *auth.ParamsBasic.Password,
					}
					ctx = context.WithValue(ctx, nslcmnotification.ContextBasicAuth, authParam)
				case "OAUTH2_CLIENT_CREDENTIALS":
					authParamOAuth := common.OAuth{
						Id:       *auth.ParamsOauth2ClientCredentials.ClientId,
						Password: *auth.ParamsOauth2ClientCredentials.ClientPassword,
						Url:      *auth.ParamsOauth2ClientCredentials.TokenEndpoint,
						Cache:    c.TokenCache,
					}
					ctx = context.WithValue(ctx, nslcmnotification.ContextOAuth2, authParamOAuth)
				default:
					log.Warn("SendNsIdentifierCreationNotification: Auth type not supported", auth.AuthType)
				}
			}

			go func() {
				retryTimeout := time.Now().Add(common.NotificationRetryMax)
				for time.Now().Before(retryTimeout) {
					response, err := lcmnotification.HandleNsIdentifierCreationNotification(ctx, notification, details.CallbackUri)
					if err == nil {
						log.Info("Successfully send notification. Response:", response)
						return
					}
					log.Warn("SendIdentifierCreationNotification  error while processing notification", err.Error())
					time.Sleep(common.NotificationRetryDelay)
				}
			}()
		}
	}
}

func EncodeNsIdentifierCreationNotification(nsId string, ts time.Time) nslcmnotification.NsIdentifierCreationNotification {
	return nslcmnotification.NsIdentifierCreationNotification{
		Id:               GenerateNotificationId(),
		NotificationType: "NsIdentifierCreationNotification",
		SubscriptionId:   "",
		Timestamp:        ts.Format(time.RFC3339),
		NsInstanceId:     nsId,
		Links:            nslcmnotification.LccnLinks{},
	}
}

func EncodeAlarmNotification(alarm nsfmnotification.Alarm, ts time.Time, s Subscription) nsfmnotification.AlarmNotification {
	subLink := "http://" + common.LOCALHOST + ":30452/nsfm/v3/subscribe/" + s.id
	return nsfmnotification.AlarmNotification{
		Id:               GenerateNotificationId(),
		NotificationType: "AlarmNotification",
		SubscriptionId:   "",
		TimeStamp:        ts,
		Alarm:            alarm,
		Links:            nsfmnotification.AlarmNotificationLinks{Subscription: nsfmnotification.NotificationLink{Href: subLink}},
	}
}

func EncodeAlarmClearedNotification(alarm nsfmnotification.Alarm, ts time.Time, s Subscription) nsfmnotification.AlarmClearedNotification {
	subLink := "http://" + common.LOCALHOST + ":30452/nsfm/v3/subscribe/" + s.id
	return nsfmnotification.AlarmClearedNotification{
		Id:               GenerateNotificationId(),
		NotificationType: "AlarmClearedNotification",
		SubscriptionId:   "",
		TimeStamp:        ts,
		AlarmId:          alarm.Id,
		AlarmClearedTime: ts,
		Links: nsfmnotification.AlarmClearedNotificationLinks{Alarm: nsfmnotification.NotificationLink{Href: alarm.Links.Self.Href},
			Subscription: nsfmnotification.NotificationLink{Href: subLink}},
	}
}

func GenerateNotificationId() string {
	uuid := uuid2.New()
	return uuid.String()
}

func (c *SubscriptionClient) SendNsLcmOperationOccurrenceNotification(operationType string, status string, operationStateType string, opId string, nsId string, ts time.Time, error *nslcmnotification.ProblemDetails, affectedVnfs []nslcmnotification.AffectedVnf) {

	c.UpdateLcmSubscriptions()
	notification := EncodeNsLcmOperationOccurrenceNotification(operationType, status, operationStateType, opId, nsId, ts, error, affectedVnfs)
	for _, s := range c.Subscriptions {
		var details subscriptionLib.LccnSubscription
		if s.category == "nslcm" && s.subscriptionType == "NsLcmOperationOccurrenceNotification" {
			if !checkOperationTypeFilter(operationType, s.details) {
				log.Infof("Notification skipped after filtering (operation type) notification= %v, nsId = %v, subscription = %v", notification, nsId, s.id)
				continue
			}
			if !checkOperationStateTypeFilter(operationStateType, s.details) {
				log.Infof("Notification skipped after filtering (operation state) notification= %v, nsId = %v, subscription = %v", notification, nsId, s.id)
				continue
			}
			if !checkNsInstanceSubscriptionFilter(nsId, s.details) {
				log.Infof("Notification skipped after filtering notification= %v, nsId = %v, subscription = %v", notification, nsId, s.id)
				continue
			}
			if err := json.Unmarshal(s.details, &details); err != nil {
				log.Warn("SendNsLcmOperationOccurrenceNotification Ignored error while processing notification. Notification not send. Error:", err.Error())
				continue
			}
			notification.SubscriptionId = s.id
			ctx := context.Background()
			auth, err := common.GetLCMSubcriptionAuth(s.id)
			if err != nil {
				log.Warn("SendNsLcmOperationOccurrenceNotification: Failure while reading authentication details.This could be temporary issue")
				log.Warn("Ignoring the error. Notification not send", err.Error())
				continue
			}
			log.Infof("Subscription info %v", auth)
			if auth != nil && auth.AuthType != nil && len(auth.AuthType) > 0 {
				authType := strings.ToUpper(auth.AuthType[0])
				switch authType {
				case "BASIC":
					authParam := nslcmnotification.BasicAuth{
						UserName: *auth.ParamsBasic.UserName,
						Password: *auth.ParamsBasic.Password,
					}
					ctx = context.WithValue(ctx, nslcmnotification.ContextBasicAuth, authParam)
				case "OAUTH2_CLIENT_CREDENTIALS":
					authParamOAuth := common.OAuth{
						Id:       *auth.ParamsOauth2ClientCredentials.ClientId,
						Password: *auth.ParamsOauth2ClientCredentials.ClientPassword,
						Url:      *auth.ParamsOauth2ClientCredentials.TokenEndpoint,
						Cache:    c.TokenCache,
					}
					ctx = context.WithValue(ctx, nslcmnotification.ContextOAuth2, authParamOAuth)
				default:
					log.Warn("SendNsLcmOperationOccurrenceNotification: Auth type not supported", auth.AuthType)
				}
			}

			go func() {
				retryTimeout := time.Now().Add(common.NotificationRetryMax)
				for time.Now().Before(retryTimeout) {
					response, err := lcmnotification.HandleNsLcmOperationOccurrenceNotification(ctx, notification, details.CallbackUri)
					if err == nil {
						log.Info("Successfully send notification. Response:", response)
						return
					}
					log.Warn("SendNsLcmOperationOccurrenceNotification  error while processing notification", err.Error())
					time.Sleep(common.NotificationRetryDelay)
				}
			}()
		}
	}
}

func (c *SubscriptionClient) SendNsIdentifierDeletionNotification(nsId string, ts time.Time) {
	c.UpdateLcmSubscriptions()
	notification := EncodeNsIdentifierDeletionNotification(nsId, ts)
	for _, s := range c.Subscriptions {
		var details subscriptionLib.LccnSubscription
		if s.category == "nslcm" && s.subscriptionType == "NsIdentifierDeletionNotification" {
			if err := json.Unmarshal(s.details, &details); err != nil {
				log.Warn("Ignored error while processing notification.", err.Error())
				continue
			}

			notification.SubscriptionId = s.id
			ctx := context.Background()
			auth, err := common.GetLCMSubcriptionAuth(s.id)
			if err != nil {
				log.Warn("SendNsIdentifierDeletionNotification: Failure while reading authentication details.This could be temporary issue")
				log.Warn("Ignoring the error. Notification not send", err.Error())
				continue
			}
			if auth != nil && auth.AuthType != nil && len(auth.AuthType) > 0 {
				authType := strings.ToUpper(auth.AuthType[0])
				switch authType {
				case "BASIC":
					authParam := nslcmnotification.BasicAuth{
						UserName: *auth.ParamsBasic.UserName,
						Password: *auth.ParamsBasic.Password,
					}
					ctx = context.WithValue(ctx, nslcmnotification.ContextBasicAuth, authParam)
				case "OAUTH2_CLIENT_CREDENTIALS":
					authParamOAuth := common.OAuth{
						Id:       *auth.ParamsOauth2ClientCredentials.ClientId,
						Password: *auth.ParamsOauth2ClientCredentials.ClientPassword,
						Url:      *auth.ParamsOauth2ClientCredentials.TokenEndpoint,
						Cache:    c.TokenCache,
					}
					ctx = context.WithValue(ctx, nslcmnotification.ContextOAuth2, authParamOAuth)
				default:
					log.Warn("SendNsIdentifierDeletionNotification: Auth type not supported", auth.AuthType)
				}
			}

			go func() {
				retryTimeout := time.Now().Add(common.NotificationRetryMax)
				for time.Now().Before(retryTimeout) {
					response, err := lcmnotification.HandleNsIdentifierDeletionNotification(ctx, notification, details.CallbackUri)
					if err == nil {
						log.Info("Successfully send notification. Response:", response)
						return
					}
					log.Warn("SendNsIdentifierDeletionNotification  error while processing notification", err.Error())
					time.Sleep(common.NotificationRetryDelay)
				}
			}()
		}
	}
}

func EncodeNsIdentifierDeletionNotification(nsId string, ts time.Time) nslcmnotification.NsIdentifierDeletionNotification {
	return nslcmnotification.NsIdentifierDeletionNotification{
		Id:               GenerateNotificationId(),
		NotificationType: "NsIdentifierDeletionNotification",
		SubscriptionId:   "",
		Timestamp:        ts.Format(time.RFC3339),
		NsInstanceId:     nsId,
		Links:            nslcmnotification.LccnLinks{},
	}
}

func EncodeNsLcmOperationOccurrenceNotification(operationType string, status string, operationStateType string, opId string, nsId string, ts time.Time, error *nslcmnotification.ProblemDetails, affectedVnfs []nslcmnotification.AffectedVnf) nslcmnotification.NsLcmOperationOccurrenceNotification {
	return nslcmnotification.NsLcmOperationOccurrenceNotification{
		Id:                    GenerateNotificationId(),
		NotificationType:      "NsLcmOperationOccurrenceNotification",
		NsLcmOpOccId:          opId,
		Operation:             nslcmnotification.NsLcmOpType(operationType),
		SubscriptionId:        "",
		Timestamp:             ts.Format(time.RFC3339),
		NsInstanceId:          nsId,
		NotificationStatus:    status,
		OperationState:        nslcmnotification.NsLcmOperationStateType(operationStateType),
		IsAutomaticInvocation: false, //true if autohealing
		Error:                 error,
		Links:                 nslcmnotification.LccnLinks{},
		AffectedVnf:           affectedVnfs,
	}
}

func (c *SubscriptionClient) GetAllLcmSubscriptions() ([]subscriptionLib.LccnSubscription, error) {
	tag := "ns_subscription"
	subscriptionsJson, err := db.DBconn.FindAll(common.NS_COLLECTION, "", "", nil, tag)
	if err != nil {
		return nil, errors.Wrap(err, "GetSubscriptions: Error while reading from db")
	}
	var subscriptions []subscriptionLib.LccnSubscription
	for _, s := range subscriptionsJson {
		subscription, err := unMarshalLcmSubscription(s)
		if err != nil {
			return nil, errors.Wrap(err, "GetSubscriptions: Error while unmarshalling subscription")
		}
		subscriptions = append(subscriptions, *subscription)
	}
	return subscriptions, nil
}

func (c *SubscriptionClient) GetAllFMSubscriptions() ([]fmsubscriptionLib.FmSubscription, error) {
	tag := "fm_subscription"
	subscriptionsJson, err := db.DBconn.FindAll(common.NS_COLLECTION, "", "", nil, tag)
	if err != nil {
		return nil, errors.Wrap(err, "GetSubscriptions: Error while reading from db")
	}
	var subscriptions []fmsubscriptionLib.FmSubscription
	for _, s := range subscriptionsJson {
		subscription, err := unMarshalFmSubscription(s)
		if err != nil {
			return nil, errors.Wrap(err, "GetSubscriptions: Error while unmarshalling subscription")
		}
		subscriptions = append(subscriptions, *subscription)
	}
	return subscriptions, nil
}

func unMarshalLcmSubscription(item []byte) (*subscriptionLib.LccnSubscription, error) {
	s := subscriptionLib.LccnSubscription{}
	if err := db.DBconn.Unmarshal(item, &s); err != nil {
		return nil, err
	}
	return &s, nil
}

func unMarshalFmSubscription(item []byte) (*fmsubscriptionLib.FmSubscription, error) {
	s := fmsubscriptionLib.FmSubscription{}
	if err := db.DBconn.Unmarshal(item, &s); err != nil {
		return nil, err
	}
	return &s, nil
}

// checkNsInstanceSubscriptionFilter does the filtering process for notification.
func checkNsInstanceSubscriptionFilter(nsId string, details []byte) bool {
	var filterPresent bool
	s := subscriptionLib.LccnSubscription{}
	err := json.Unmarshal(details, &s)
	if err != nil {
		log.Errorf("Error while decoding subscription info %s %s", err.Error(), common.PrintFunctionName())
		return false
	}
	if s.Filter == nil || s.Filter.NsInstanceSubscriptionFilter == nil {
		return true
	}
	filter := s.Filter.NsInstanceSubscriptionFilter
	if filter.NsInstanceIds != nil && len(filter.NsInstanceIds) > 0 {
		filterPresent = true
		for _, name := range filter.NsInstanceIds {
			if name == nsId {
				return true
			}
		}
	}
	_, serviceInfo, err := common.FetchNSInfo(nsId)
	if err != nil {
		log.Errorf("Error while decoding subscription info %s, %s", err.Error(), common.PrintFunctionName())
		return false
	}
	nsInfo, ok := serviceInfo.(common.NsInstanceInfo)

	if !ok {
		log.Errorf("Error while decoding subscription info %s", common.PrintFunctionName())
		return false
	}

	if filter.NsdIds != nil && len(filter.NsdIds) > 0 {
		filterPresent = true
		for _, id := range filter.NsdIds {
			if id == nsInfo.NsdId {
				return true
			}
		}
	}

	if filter.NsInstanceNames != nil && len(filter.NsInstanceNames) > 0 {
		filterPresent = true
		for _, name := range filter.NsInstanceNames {
			if name == nsInfo.NsName {
				return true
			}
		}
	}

	if filter.VnfdIds != nil && len(filter.VnfdIds) > 0 {
		filterPresent = true
		vnfs := make(map[string]bool)
		for _, v := range nsInfo.VnfInstance {
			vnfs[v.VnfInstanceId] = true
		}
		for _, name := range filter.VnfdIds {
			if vnfs[name] {
				return true
			}
		}
	}

	return !filterPresent
}

func checkOperationTypeFilter(opType string, details []byte) bool {
	s := subscriptionLib.LccnSubscription{}
	err := json.Unmarshal(details, &s)
	if err != nil {
		log.Errorf("Error while decoding subscription info %s %s", err.Error(), common.PrintFunctionName())
		return false
	}
	filter := s.Filter.OperationTypes
	if filter == nil || len(filter) == 0 {
		return true
	}
	for _, t := range filter {
		if string(t) == opType {
			return true
		}
	}
	return false
}

func checkOperationStateTypeFilter(opState string, details []byte) bool {
	s := subscriptionLib.LccnSubscription{}
	err := json.Unmarshal(details, &s)
	if err != nil {
		log.Errorf("Error while decoding subscription info %s %s", err.Error(), common.PrintFunctionName())
		return false
	}
	filter := s.Filter.OperationStates
	if filter == nil || len(filter) == 0 {
		return true
	}
	for _, t := range filter {
		if string(t) == opState {
			return true
		}
	}
	return false
}

// checkNsInstanceFMSubscriptionFilter does the filtering process for notification.
func checkNsInstanceFMSubscriptionFilter(nsId string, details []byte) bool {
	var filterPresent bool
	s := fmsubscriptionLib.FmSubscription{}
	err := json.Unmarshal(details, &s)
	if err != nil {
		log.Errorf("Error while decoding subscription info %s %s", err.Error(), common.PrintFunctionName())
		return false
	}
	if s.Filter == nil || s.Filter.NsInstanceSubscriptionFilter == nil {
		return true
	}
	filter := s.Filter.NsInstanceSubscriptionFilter
	if filter.NsInstanceIds != nil && len(filter.NsInstanceIds) > 0 {
		filterPresent = true
		for _, name := range filter.NsInstanceIds {
			if name == nsId {
				return true
			}
		}
	}
	_, serviceInfo, err := common.FetchNSInfo(nsId)
	if err != nil {
		log.Errorf("Error while decoding subscription info %s, %s", err.Error(), common.PrintFunctionName())
		return false
	}
	nsInfo, ok := serviceInfo.(common.NsInstanceInfo)

	if !ok {
		log.Errorf("Error while decoding subscription info %s", common.PrintFunctionName())
		return false
	}

	if filter.NsdIds != nil && len(filter.NsdIds) > 0 {
		filterPresent = true
		for _, id := range filter.NsdIds {
			if id == nsInfo.NsdId {
				return true
			}
		}
	}

	if filter.NsInstanceNames != nil && len(filter.NsInstanceNames) > 0 {
		filterPresent = true
		for _, name := range filter.NsInstanceNames {
			if name == nsInfo.NsName {
				return true
			}
		}
	}

	if filter.VnfdIds != nil && len(filter.VnfdIds) > 0 {
		filterPresent = true
		vnfs := make(map[string]bool)
		for _, v := range nsInfo.VnfInstance {
			vnfs[v.VnfInstanceId] = true
		}
		for _, name := range filter.VnfdIds {
			if vnfs[name] {
				return true
			}
		}
	}

	return !filterPresent
}

func checkNotificationTypeFMFilter(notificationType string, details []byte) bool {
	s := fmsubscriptionLib.FmSubscription{}
	err := json.Unmarshal(details, &s)
	if err != nil {
		log.Errorf("Error while decoding subscription info %s %s", err.Error(), common.PrintFunctionName())
		return false
	}
	filter := s.Filter.NotificationTypes
	if filter == nil || len(filter) == 0 {
		return true
	}
	for _, t := range filter {
		if string(t) == notificationType {
			return true
		}
	}
	return false
}
