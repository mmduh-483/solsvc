package lcmsubscription

import (
	"aarna.com/solsvc/db"
	"aarna.com/solsvc/lib/common"
	subscriptionLib "aarna.com/solsvc/lib/nslcmsubscription"
	"crypto/tls"
	"crypto/x509"
	uuid2 "github.com/google/uuid"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"net/http"
	"os"
	"strconv"
	"strings"
)

type subscriptionKey struct {
	Id string `json:"id"`
}

func (c Client) CreateLcmSubscription(request subscriptionLib.LccnSubscriptionRequest) (subscriptionLib.LccnSubscription, error) {
	if duplicate := isSubscriptionExits(request); duplicate != nil {
		return subscriptionLib.LccnSubscription{}, errors.Errorf("Subscription exits %v", duplicate)
	}
	id, err := NewSubscriptionId()
	if err != nil {
		return subscriptionLib.LccnSubscription{}, err
	}
	subscription := subscriptionLib.LccnSubscription{
		Id:          id,
		Filter:      request.Filter,
		CallbackUri: request.CallbackUri,
		Verbosity:   request.Verbosity,
		Links:       subscriptionLib.LccnSubscriptionLinks{},
	}
	if err := c.VerifyCallbackUri(request.CallbackUri); err != nil {
		return subscriptionLib.LccnSubscription{}, errors.Wrap(err, "CallbackUri verification failed")
	}
	if err := c.VerifyNotificationTypes(request.Filter.NotificationTypes); err != nil {
		return subscriptionLib.LccnSubscription{}, err
	}
	if request.HasAuthentication() {
		if err := storeAuthentication(subscription.Id, request.Authentication); err != nil {
			return subscriptionLib.LccnSubscription{}, err
		}
	}
	err = storeSubscription(subscription)
	if err != nil {
		return subscriptionLib.LccnSubscription{}, err
	}
	go c.SubscriptionClient.UpdateLcmSubscriptions()
	return subscription, nil
}

func (c Client) VerifyCallbackUri(url string) error {

	isHttps := strings.HasPrefix(url, "https")
	client := http.Client{}
	if isHttps {
		caCert, err := os.ReadFile("/opt/emco/rootCA.pem")
		if err != nil {
			log.Warnf("Error while reading certificate from rootCA.crt %v", err)
			return errors.Wrap(err, "Could not read certificate")
		}
		caCertPool := x509.NewCertPool()
		caCertPool.AppendCertsFromPEM(caCert)
		client = http.Client{Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				RootCAs: caCertPool,
			}}}
	}
	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Errorf("Error while creating request %s\n", err.Error())
		return err
	}
	response, err := client.Do(request)
	if err != nil {
		log.Errorf("Error while accessing server %s\n", err.Error())
		return err
	}

	if response.StatusCode != http.StatusNoContent && response.StatusCode != http.StatusOK {
		log.Errorf("Error while accessing token, status code: %v \n", response.StatusCode)
		return errors.New("Response statuscode EXPECTED=204 GOT " + strconv.Itoa(int(response.StatusCode)))
	}

	response.Body.Close()
	return nil
}

func (c Client) VerifyNotificationTypes(types []string) error {
	supportedTypes := map[string]bool{
		"NsIdentifierCreationNotification":     true,
		"NsLcmOperationOccurrenceNotification": true,
		"NsIdentifierDeletionNotification":     true,
	}
	for _, t := range types {
		if valid := supportedTypes[t]; !valid {
			return errors.New("Unsupported Notification type " + t)
		}
	}
	return nil
}

func (c Client) GetLcmSubscription(subscriptionId string) (*subscriptionLib.LccnSubscription, error) {
	return getLcmSubscriptionFromDb(subscriptionId)
}

func (c Client) DeleteSubscription(subscriptionId string) error {
	return deleteSubscription(subscriptionId)
}

func getLcmSubscriptionFromDb(id string) (*subscriptionLib.LccnSubscription, error) {
	if exists := db.DBconn.CheckCollectionExists(common.NS_COLLECTION); !exists {
		return nil, errors.New("Collections doesn;t exits")
	}
	tag := "ns_subscription"
	key := subscriptionKey{Id: id}
	values, err := db.DBconn.Find(common.NS_COLLECTION, key, tag)
	if err != nil || len(values) == 0 {
		return nil, errors.Wrap(err, "getLcmSubscriptionFromDb: error while reading from db")
	}
	return unMarshalSubscription(values[0])
}

func unMarshalSubscription(item []byte) (*subscriptionLib.LccnSubscription, error) {
	s := subscriptionLib.LccnSubscription{}
	if err := db.DBconn.Unmarshal(item, &s); err != nil {
		return nil, err
	}
	return &s, nil
}

func storeSubscription(subscription subscriptionLib.LccnSubscription) error {
	key := subscriptionKey{
		Id: subscription.Id,
	}
	tag := "ns_subscription"
	return db.DBconn.Insert(common.NS_COLLECTION, key, nil, tag, subscription)
}

func deleteSubscription(id string) error {
	key := subscriptionKey{
		Id: id,
	}
	err := db.DBconn.Remove(common.NS_COLLECTION, key)
	if err != nil {
		return errors.Wrap(err, "deleteSubscription: Could not delete from db")
	}
	return nil
}

func isSubscriptionExits(_ subscriptionLib.LccnSubscriptionRequest) *subscriptionLib.LccnSubscription {
	return nil
}

func NewSubscriptionId() (string, error) {
	uuid := uuid2.New()
	return uuid.String(), nil
}

func storeAuthentication(subscriptionId string, auth interface{}) error {
	key := subscriptionKey{
		Id: subscriptionId,
	}
	tag := "ns_subscription_auth"
	return db.DBconn.Insert(common.NS_COLLECTION, key, nil, tag, auth)
}
