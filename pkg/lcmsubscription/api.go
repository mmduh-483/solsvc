package lcmsubscription

import (
	subscriptionLib "aarna.com/solsvc/lib/nslcmsubscription"
	"aarna.com/solsvc/pkg/notificationInfra"
	"aarna.com/solsvc/pkg/subscriptionclient"
	"encoding/json"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"net/http"
)

type Client struct {
	SubscriptionClient       subscriptionclient.SubscriptionClient
	EmcoNotificationHandler  notificationInfra.EmcoNotificationHandler
	LocalNotificationHandler notificationInfra.LocalNotificationHandler
	Conf                     SOLConf
}

type SOLConf struct {
	OwnPort            string `json:"ownport"`
	MiddleEnd          string `json:"middleend"`
	Orchestrator       string `json:"orchestrator"`
	Mongo              string `json:"mongo"`
	LogLevel           string `json:"logLevel"`
	StoreName          string `json:"storeName"`
	Dcm                string `json:"dcm"`
	OrchestratorStatus string `json:"orchestratorStatus"`
}

func (c Client) CreateHandler(w http.ResponseWriter, r *http.Request) {
	var (
		request subscriptionLib.LccnSubscriptionRequest
	)
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&request); err != nil {
		title := "Create LCM Subscription failed"
		problem := subscriptionLib.ProblemDetails{
			Type:     nil,
			Title:    &title,
			Status:   0,
			Detail:   err.Error(),
			Instance: nil,
		}
		WriteError(problem, http.StatusBadRequest, w)
		return
	}
	log.Infof("CreateHandler request: %v", request)

	subscription, err := c.CreateLcmSubscription(request)
	if err != nil {
		title := "Create LCM Subscription failed"
		problem := subscriptionLib.ProblemDetails{
			Type:     nil,
			Title:    &title,
			Status:   0,
			Detail:   err.Error(),
			Instance: nil,
		}
		WriteError(problem, http.StatusInternalServerError, w)
		return
	}
	subscriptionJson, err := json.Marshal(subscription)
	if err != nil {
		title := "Create LCM Subscription failed"
		problem := subscriptionLib.ProblemDetails{
			Type:     nil,
			Title:    &title,
			Status:   0,
			Detail:   err.Error(),
			Instance: nil,
		}
		WriteError(problem, http.StatusInternalServerError, w)
		return
	}
	log.Infof("Create subscription successful %s, %s", subscription.Id, subscription.CallbackUri)
	w.Header().Set("Location", "url")
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	_, err = w.Write(subscriptionJson)
	if err != nil {
		log.Errorf("Error while http write: %v", err.Error())
	}
}

func (c Client) GetHandler(w http.ResponseWriter, _ *http.Request) {
	subscriptions, err := c.SubscriptionClient.GetAllLcmSubscriptions()
	if err != nil {
		title := "Get LCM Subscriptions failed"
		problem := subscriptionLib.ProblemDetails{
			Type:     nil,
			Title:    &title,
			Status:   0,
			Detail:   err.Error(),
			Instance: nil,
		}
		WriteError(problem, http.StatusInternalServerError, w)
		return
	}
	if subscriptions == nil || len(subscriptions) == 0 {
		title := "No subscriptions found"
		problem := subscriptionLib.ProblemDetails{
			Type:     nil,
			Title:    &title,
			Status:   0,
			Detail:   "No subscriptions found",
			Instance: nil,
		}
		WriteError(problem, http.StatusNoContent, w)
		return
	}
	subscriptionsJson, err := json.Marshal(subscriptions)
	if err != nil {
		title := "Get LCM Subscriptions failed"
		problem := subscriptionLib.ProblemDetails{
			Type:     nil,
			Title:    &title,
			Status:   0,
			Detail:   err.Error(),
			Instance: nil,
		}
		WriteError(problem, http.StatusInternalServerError, w)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, err = w.Write(subscriptionsJson)
	if err != nil {
		log.Errorf("Error while http write: %v", err.Error())
	}
}

func (c Client) GetSubscriptionHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	subscriptionId := vars["subscriptionId"]
	subscription, err := c.GetLcmSubscription(subscriptionId)
	if err != nil {
		title := "Get LCM Subscription failed"
		problem := subscriptionLib.ProblemDetails{
			Type:     nil,
			Title:    &title,
			Status:   0,
			Detail:   err.Error(),
			Instance: &subscriptionId,
		}
		WriteError(problem, http.StatusInternalServerError, w)
		return
	}
	if subscription == nil {
		title := "Get LCM Subscriptions failed: Id not found"
		problem := subscriptionLib.ProblemDetails{
			Type:     nil,
			Title:    &title,
			Status:   0,
			Detail:   "Provided subscription Id not valid",
			Instance: &subscriptionId,
		}
		WriteError(problem, http.StatusNotFound, w)
		return
	}

	subscriptionJson, err := json.Marshal(subscription)
	if err != nil {
		return
	}

	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write(subscriptionJson)
	if err != nil {
		log.Errorf("Error while http write: %v", err.Error())
	}
}

func (c Client) DeleteHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	subscriptionId := vars["subscriptionId"]

	err := c.DeleteSubscription(subscriptionId)
	if err != nil {
		title := "LCM Subscription delete failed"
		problem := subscriptionLib.ProblemDetails{
			Type:     nil,
			Title:    &title,
			Status:   0,
			Detail:   err.Error(),
			Instance: &subscriptionId,
		}
		WriteError(problem, http.StatusInternalServerError, w)
		return
	}
	log.Infof("Delete succesful subscriptionId %s", subscriptionId)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusNoContent)
	return
}

func WriteError(problem subscriptionLib.ProblemDetails, statusCode int, w http.ResponseWriter) {
	problem.Status = int32(statusCode)
	responseJson, err := json.Marshal(problem)
	if err != nil {
		log.Errorf("Error while parsing: %v", err.Error())
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	_, err = w.Write(responseJson)
	if err != nil {
		log.Errorf("Error while http write: %v", err.Error())
	}
	return
}
