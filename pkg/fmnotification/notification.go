package fmnotification

import (
	"aarna.com/solsvc/db"
	"aarna.com/solsvc/lib/common"
	notificationLib "aarna.com/solsvc/lib/nsfmnotification"
	"context"
	"fmt"
	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"net/http"
	"strings"
	"time"
)


// Creates Alarm object for given event
func CreateAlarm(nsId string, severity string, eventType notificationLib.EventType, probCause string) notificationLib.Alarm {
	var (
		alarm notificationLib.Alarm
	)
	alarm.Id = uuid.New().String()
	alarm.ManagedObjectId = nsId
	alarm.EventTime = time.Now()
	alarm.AckState = common.UNACKNOWLEDGED
	alarm.EventType = eventType
	alarm.AlarmRaisedTime = time.Now()
	alarm.PerceivedSeverity = notificationLib.PerceivedSeverityType(strings.ToUpper(severity))
	alarm.ProbableCause = probCause
	alarm.IsRootCause = false

	alarmLink := "http://" + common.LOCALHOST + ":30452/nsfm/v3/alarms/" + alarm.Id
	alarm.Links = notificationLib.AlarmLinks{Self: notificationLib.Link{Href: alarmLink}}

	key := new(common.AlarmKey)
	key.AlarmId = alarm.Id
	err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "alarm", alarm)
	if err != nil {
		errMsg := fmt.Sprintf("Encountered error during insert of alarm %s: %s", alarm.Id, err)
		log.Errorf(errMsg)
		return alarm
	}
	log.Infof("Alarm %+v successfully inserted in database", alarm)
	return alarm
}



func HandleAlarmNotification(ctx context.Context, notification notificationLib.AlarmNotification, url string) (*http.Response, error) {
	log.Debugf("Processing alarm notification")
	cfg := notificationLib.NewConfiguration()
	server := notificationLib.ServerConfiguration{URL: url}
	endPoint := "DefaultApiService.AlarmNotificationPost"
	cfg.OperationServers[endPoint] = notificationLib.ServerConfigurations{server}
	client := NewAPIClient(cfg)
	apiService := DefaultApiService{
		client: client,
	}
	request := AlarmNotificationPostRequest{
		ctx:        ctx,
		ApiService: &apiService,
	}
	request = request.AlarmNotification(notification)
	request = request.Version("v2")
	request = request.Endpoint(endPoint)
	request = request.Accept("application/json")
	request = request.ContentType("application/json")
	return request.Execute()
}


func HandleAlarmClearedNotification(ctx context.Context, notification notificationLib.AlarmClearedNotification, url string) (*http.Response, error) {
	log.Debugf("Processing handle alarm cleared notification")
	cfg := notificationLib.NewConfiguration()
	server := notificationLib.ServerConfiguration{URL: url}
	endPoint := "DefaultApiService.AlarmClearedNotificationPost"
	cfg.OperationServers[endPoint] = notificationLib.ServerConfigurations{server}
	client := NewAPIClient(cfg)
	apiService := DefaultApiService{
		client: client,
	}
	request := AlarmClearedNotificationPostRequest{
		ctx:        ctx,
		ApiService: &apiService,
	}
	request = request.AlarmClearedNotification(notification)
	request = request.Version("v2")
	request = request.Endpoint(endPoint)
	request = request.Accept("application/json")
	request = request.ContentType("application/json")
	return request.Execute()
}
