package lcmops

import (
	"aarna.com/solsvc/db"
	"aarna.com/solsvc/lib/common"
	nslcm "aarna.com/solsvc/lib/nslcm"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"net/http"
	"strconv"
	"strings"
)

// Create VNFD Map
func (c *Client) CreateVNFDMap() (map[string]string, *nslcm.ProblemDetails) {
	var (
		errMsg string
	)

	// Building mapping of vnfdid to composite app + version
	vnfdMap := make(map[string]string)
	var compappList []CompositeAppsInProjectShrunk
	h := common.HttpHandler{}
	h.Client = &http.Client{}

	URL := "http://" + c.Info.Conf.MiddleEnd + "/middleend/projects/" +
		common.DEFAULT_PROJECT + "/composite-apps"
	retcode, resp, err := h.ApiGet(URL)
	if retcode != http.StatusOK {
		errMsg = "Unable to fetch list of compapps"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
			Status: int32(retcode.(int)),
		}
		return vnfdMap, &problem
	}

	log.Debugf("compositeapps: %+v", resp)
	err = json.Unmarshal(resp, &compappList)
	if err != nil {
		errMsg = "Unable to unmarshal response..."
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return vnfdMap, &problem
	}

	for _, compapp := range compappList {
		for _, compversion := range compapp.Spec {
			vnfdMap[compversion.Id] = compapp.Metadata.Name + " " + compversion.Version
		}
	}
	return vnfdMap, nil
}

// Validate VNFD Map
func (c *Client) ValidateVNFDMap(nsdInfo common.NsdInfo, vnfdMap map[string]string) *nslcm.ProblemDetails {
	var (
		errMsg string
	)

	// Iterate through each vnfdid and check if supplied vnfdids are valid
	for _, vnfdId := range nsdInfo.Spec.VnfdIds {
		_, ok := vnfdMap[vnfdId]
		if !ok {
			errMsg = fmt.Sprintf("vnfdid does not exists")
			problem := nslcm.ProblemDetails{
				Title:  &errMsg,
				Detail: errMsg,
				Status: http.StatusInternalServerError,
			}
			return &problem
		}
	}
	return nil
}

// Fetch cluster info from logical cloud
func (c *Client) FetchClusterInfo(nsInfo common.NsInstanceInfo) ([]common.ClusterInfo, *nslcm.ProblemDetails) {
	var (
		errMsg string
	)

	cInfoList := make([]common.ClusterInfo, 0)

	// Check if logicalCloud parameter exists, if not use defaultlc
	if nsInfo.LogicalCloud == "" {
		nsInfo.LogicalCloud = "defaultlc"
	}

	h := common.HttpHandler{}
	h.Client = &http.Client{}

	// Fetch clusters part of logical cloud
	var lcRefList []common.ClusterReferenceFlat
	url := "http://" + c.Info.Conf.Dcm + "/v2/projects/" +
		common.DEFAULT_PROJECT + "/logical-clouds/" + nsInfo.LogicalCloud + "/cluster-references"
	retcode, respval, err := h.ApiGet(url)
	if err != nil {
		errMsg = fmt.Sprintf("Failed to fetch LC reference for %s", nsInfo.LogicalCloud)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return cInfoList, &problem
	}
	if retcode != http.StatusOK {
		errMsg = fmt.Sprintf("Bad error code %d while getting LC references %s",
			retcode.(int), nsInfo.LogicalCloud)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
			Status: int32(retcode.(int)),
		}
		return cInfoList, &problem
	}
	err = json.Unmarshal(respval, &lcRefList)
	if err != nil {
		errMsg = "Failed to unmarshal json"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return cInfoList, &problem
	}
	log.Debugf("lc references: %+v", lcRefList)

	// Create clusterInfo list
	for _, clRef := range lcRefList {
		var cInfo common.ClusterInfo
		cInfo.SelectedClusters = []common.SelectedCluster{{Name: clRef.Spec.ClusterName}}
		cInfo.Provider = clRef.Spec.ClusterProvider
		cInfoList = append(cInfoList, cInfo)
	}
	return cInfoList, nil
}

// Create DIG
func (c *Client) CreateDIG(nsInfo common.NsInstanceInfo, compappname string, version string, counter int,
	clusterInfo []common.ClusterInfo, configData []common.Day0Config) (DeploymentIntentGroup, *nslcm.ProblemDetails) {
	var (
		errMsg string
		dig    DeploymentIntentGroup
	)

	h := common.HttpHandler{}
	h.Client = &http.Client{}

	// Fetch the apps part of composite app
	var appList []common.Application
	URL := "http://" + c.Info.Conf.Orchestrator + "/v2/projects/" +
		common.DEFAULT_PROJECT + "/composite-apps/" + compappname + "/" + version + "/apps"
	log.Info("URL: %s", URL)
	respcode, respdata, err := h.ApiGet(URL)
	log.Infof("Get apps list: %d", respcode)
	if err != nil {
		errMsg = fmt.Sprintf("Encountered error while fetching apps list: %s", err.Error())
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return dig, &problem
	}
	if respcode != http.StatusOK {
		errMsg := "encountered error while fetching apps"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
			Status: int32(respcode.(int)),
		}
		return dig, &problem
	}
	err = json.Unmarshal(respdata, &appList)
	if err != nil {
		errMsg = "Failed to unmarshal json"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return dig, &problem
	}
	log.Infof("applist: %s", appList)

	// Compose the DIG creation payload for middleend
	appsList := make([]common.AppsData, 0)

	// Create a map for mapping app name with day0config
	configList := make(map[string][]common.ResourceInfo)
	for _, appData := range configData {
		configList[appData.Name] = appData.ConfigInfo
	}
	log.Debugf("configList: %+v", configList)

	// Compose the DIG creation payload for middleend
	for _, app := range appList {
		var appData common.AppsData
		appData.Metadata.Name = app.Metadata.Name
		appData.Metadata.Description = app.Metadata.Description
		appData.Metadata.FileName = ""
		appData.PlacementCriterion = "allOf"
		appData.Clusters = clusterInfo
		appData.RsInfo = configList[appData.Metadata.Name]
		log.Debugf("resinfo: %+v", appData.RsInfo)

		appsList = append(appsList, appData)
	}

	var digspec common.DigSpec
	digspec.ProjectName = common.DEFAULT_PROJECT
	digspec.Apps = appsList

	id := uuid.New().String()
	idList := strings.Split(id, "-")
	digVersion := "v" + idList[0]
	digName := nsInfo.NsName + "-" + strconv.Itoa(counter)
	digData := common.DeployDigData{
		Name:                digName,
		Description:         nsInfo.NsDescription,
		CompositeAppName:    compappname,
		CompositeProfile:    compappname + "_profile",
		DigVersion:          digVersion,
		CompositeAppVersion: version,
		LogicalCloud:        nsInfo.LogicalCloud,
		Spec:                digspec,
	}

	// Invoke createDIG call of middleend service
	log.Infof("digdata: %s", digData)
	jsonLoad, _ := json.Marshal(digData)
	URL = "http://" + c.Info.Conf.MiddleEnd + "/middleend/projects/" + common.DEFAULT_PROJECT +
		"/composite-apps/" + compappname + "/" + version +
		"/deployment-intent-groups"
	status, response, err := h.ApiPostMultipart(jsonLoad, nil, URL)
	if err != nil {
		errMsg = fmt.Sprintf("Encountered error while creating DIG for NS service: %s", err.Error())
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return dig, &problem
	}
	if status != http.StatusOK {
		errMsg = fmt.Sprintf("Encountered error while creating DIG %s for NS service: %s", digName)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
			Status: int32(status),
		}
		return dig, &problem
	}

	err = json.Unmarshal(response, &dig)
	if err != nil {
		errMsg = "Failed to unmarshal DIG response"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
			Status: http.StatusInternalServerError,
		}
		return dig, &problem
	}
	return dig, nil
}

// Create Service
func (c *Client) CreateService(nsInfo common.NsInstanceInfo, digIdList []string) (Service, *nslcm.ProblemDetails) {
	var (
		errMsg          string
		serviceResponse Service
	)

	h := common.HttpHandler{}
	h.Client = &http.Client{}

	// Call EMCO CREATE NS API to create logical mapping of DIGs
	service := ServiceRequest{
		MetaData: ServiceRequestMetaData{
			Name: nsInfo.NsName,
		},
		Spec: ServiceSpec{
			Digs: digIdList,
		},
	}
	log.Debugf("service payload: %+v", service)
	jsonPayload, _ := json.Marshal(service)
	url := "http://" + c.Info.Conf.Orchestrator + "/v2/projects/" +
		common.DEFAULT_PROJECT + "/services"

	status, resp, err := h.ApiPostRespBody(jsonPayload, url)
	if err != nil {
		errMsg = fmt.Sprintf("Encountered error while creating NS service: %s", err.Error())
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return serviceResponse, &problem
	}
	if status != http.StatusCreated {
		errMsg = fmt.Sprintf("Encountered error while creating NS service %s: %s", nsInfo.NsName, string(resp))
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
			Status: int32(status),
		}
		return serviceResponse, &problem
	}

	err = json.Unmarshal(resp, &serviceResponse)
	if err != nil {
		errMsg = fmt.Sprintf("Encountered error during unmarshal of serviceInfo: %s", err)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return serviceResponse, &problem
	}
	return serviceResponse, nil
}

// Update resource links
func (c *Client) UpdateLinks(nsInfo *common.NsInstanceInfo, host string, url string) {
	var instantiateLink common.Link
	instantiateLink.Href = "http://" + host + url + "/" + (*nsInfo).NsId + "/instantiate"
	(*nsInfo).Links.Instantiate = &instantiateLink

	var selfLink common.Link
	selfLink.Href = "http://" + host + url + "/" + (*nsInfo).NsId
	(*nsInfo).Links.Self = &selfLink

	var updateLink common.Link
	updateLink.Href = "http://" + host + url + "/" + (*nsInfo).NsId + "/update"
	(*nsInfo).Links.Update = &updateLink

	var scaleLink common.Link
	scaleLink.Href = "http://" + host + url + "/" + (*nsInfo).NsId + "/scale"
	(*nsInfo).Links.Scale = &scaleLink

	var healLink common.Link
	healLink.Href = "http://" + host + url + "/" + (*nsInfo).NsId + "/heal"
	(*nsInfo).Links.Heal = &healLink
	return
}

// Validate if NS exists
func (c *Client) ValidateServiceExists(serviceName string) bool {
	var (
		errMsg string
	)

	h := common.HttpHandler{}
	h.Client = &http.Client{}

	url := "http://" + c.Info.Conf.Orchestrator + "/v2/projects/" +
		common.DEFAULT_PROJECT + "/services/" + serviceName

	status, _, err := h.ApiGet(url)
	if err != nil {
		errMsg = fmt.Sprintf("Encountered error while fetching NS service: %s", err.Error())
		log.Errorf(errMsg)
	}
	if status == http.StatusOK {
		log.Infof("NS service %s already exists", serviceName)
		return true
	}
	return false
}

// Create Network Service Information
func (c *Client) CreateNSInfo(w http.ResponseWriter, r *http.Request) {
	var (
		nsInfo       common.NsInstanceInfo
		nsCreateInfo CreateNsRequest
		errMsg       string
		digIdList    []string
	)

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&nsCreateInfo)
	if err != nil {
		errMsg = fmt.Sprintf("failed to parse json: %s", err)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusBadRequest, w, r)
		errMsg = fmt.Sprintf("%s: %s", common.PrintFunctionName(), errMsg)
		log.Error(errMsg)
		return
	}
	log.Debugf("nsCreateInfo: %+v", nsCreateInfo)

	// Validate if NS exists
	if c.ValidateServiceExists(nsCreateInfo.NsName) {
		errMsg = fmt.Sprintf("NS %s already exists:", nsCreateInfo.NsName)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		errMsg = fmt.Sprintf("%s: %s", common.PrintFunctionName(), errMsg)
		log.Error(errMsg)
		return
	}

	// Extract info from createNsRequest and populate nsInstanceInfo
	nsInfo.NsdId = nsCreateInfo.NsdId
	if nsCreateInfo.LogicalCloud == "" {
		nsCreateInfo.LogicalCloud = "defaultlc"
	}
	nsInfo.LogicalCloud = nsCreateInfo.LogicalCloud
	nsInfo.NsDescription = nsCreateInfo.NsDescription
	nsInfo.NsName = nsCreateInfo.NsName
	nsInfo.NsdInfoId = nsCreateInfo.NsdId

	// Extract nsd information
	retCode, nsd, err := common.FetchNSDInfo(nsCreateInfo.NsdId)
	if retCode != http.StatusOK {
		errMsg = "encountered error while fetching NsdInfo"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, retCode, w, r)
		errMsg = fmt.Sprintf("%s: %s", common.PrintFunctionName(), errMsg)
		log.Error(errMsg)
		return
	}

	nsdInfo, ok := nsd.(common.NsdInfo)
	if !ok {
		errMsg = "Unable to parse nsdInfo structure..."
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		errMsg = fmt.Sprintf("%s: %s", common.PrintFunctionName(), errMsg)
		log.Error(errMsg)
		return
	}

	// Create VNFD Map
	vnfdMap, pb := c.CreateVNFDMap()
	if pb != nil {
		common.WriteError(*pb, int(pb.Status), w, r)
		errMsg = fmt.Sprintf("Encountered error during creation of vnfdMap: %s", common.PrintFunctionName())
		log.Error(errMsg)
		return
	}
	log.Debugf("vnfdmap: %+v", vnfdMap)

	// Validate VNFD Map
	pb = c.ValidateVNFDMap(nsdInfo, vnfdMap)
	if pb != nil {
		common.WriteError(*pb, int(pb.Status), w, r)
		errMsg = fmt.Sprintf("Encountered error during validation of vnfdMap: %s", common.PrintFunctionName())
		log.Error(errMsg)
		return
	}

	// Fetch cluster info
	cInfoList, pb := c.FetchClusterInfo(nsInfo)
	if pb != nil {
		common.WriteError(*pb, int(pb.Status), w, r)
		errMsg = fmt.Sprintf("Encountered error while fetching clusters part of logical cloud %s: %s",
			nsInfo.LogicalCloud, common.PrintFunctionName())
		log.Error(errMsg)
		return
	}

	vnfInstList := make([]common.VnfInstance, 0)
	nsInfo.NsState = common.NOT_INSTANTIATED

	// Iterate through each vnfdid and create corresponding DIG
	for i, vnfdId := range nsdInfo.Spec.VnfdIds {
		val, _ := vnfdMap[vnfdId]
		log.Debugf("retreival for %s from vnfdMap: %s", vnfdId, val)
		valList := strings.Split(val, " ")
		compappname, version := valList[0], valList[1]

		dig, pb := c.CreateDIG(nsInfo, compappname, version, i, cInfoList, nsCreateInfo.ConfigData)
		if pb != nil {
			common.WriteError(*pb, int(pb.Status), w, r)
			errMsg = fmt.Sprintf("Encountered error while creating dig %s: %s",
				dig.MetaData.Name, common.PrintFunctionName())
			log.Error(errMsg)
			return
		}

		// Update digIdMap
		digName := dig.MetaData.Name
		digId := common.DEFAULT_PROJECT + "." + compappname + "." + version + "." + digName
		digIdList = append(digIdList, digId)

		// Update VnfInstance list
		var vnfInst common.VnfInstance
		vnfInst.VnfInstanceName = digId
		vnfInst.VnfInstanceId = dig.Spec.Id
		vnfInst.VnfdId = vnfdId
		vnfInst.Description = "create " + digName
		vnfInst.State = common.NOT_INSTANTIATED
		vnfInstList = append(vnfInstList, vnfInst)
	}

	// Update nsinfo structure with vnf instance list
	nsInfo.VnfInstance = vnfInstList

	serviceResponse, pb := c.CreateService(nsInfo, digIdList)
	if pb != nil {
		common.WriteError(*pb, int(pb.Status), w, r)
		errMsg = fmt.Sprintf("Encountered error while creating service: %s",
			common.PrintFunctionName())
		log.Error(errMsg)
		return
	}

	// Update NsId in nsInfo struct
	nsInfo.NsId = serviceResponse.MetaData.Id
	//nsInfo.NsScaleStatus = []nslcm.NsScaleInfo{}
	nsInfo.NsScaleStatus = make([]nslcm.NsScaleInfo, 0)
	// Update resource links
	c.UpdateLinks(&nsInfo, r.Host, r.URL.String())

	key := new(common.NsInstanceKey)
	key.NsId = nsInfo.NsId
	err = db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nsinfo", nsInfo)
	if err != nil {
		errMsg = fmt.Sprintf("Encountered error during insert of NS info for %s: %s", nsInfo.NsName, err)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		errMsg = fmt.Sprintf("%s: %s", common.PrintFunctionName(), errMsg)
		log.Error(errMsg)
		return
	}

	// Form the return response
	retval, err := json.Marshal(nsInfo)
	if err != nil {
		errMsg = fmt.Sprintf("Encountered error during marshal of nsInfo: %s", err)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		errMsg = fmt.Sprintf("%s: %s", common.PrintFunctionName(), errMsg)
		log.Error(errMsg)
		return
	}

	// Set the location header, as request is successful
	nsInstanceURL := r.URL.String() + "/" + nsInfo.NsId
	c.LocalNotificationHandler.NsCreateDone(nsInfo.NsId)
	w.Header().Set("Location", nsInstanceURL)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(retval)
}
