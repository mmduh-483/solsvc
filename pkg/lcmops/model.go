package lcmops

import (
	"aarna.com/solsvc/lib/common"
	"aarna.com/solsvc/pkg/notificationInfra"
)

type Client struct {
	EmcoNotificationHandler  notificationInfra.EmcoNotificationHandler
	LocalNotificationHandler notificationInfra.LocalNotificationHandler
	Info                     common.ClientInfo
}

type CreateNsRequest struct {
	NsdId         string              `json:"nsdId"`
	NsName        string              `json:"nsName"`
	NsDescription string              `json:"nsDescription"`
	LogicalCloud  string              `json:"logicalCloud,omitempty"`
	ConfigData    []common.Day0Config `json:"config,omitempty"`
	RequestHost   string              `json:"requestHost"`
	RequestUrl    string              `json:"requestUrl"`
}

type InstantiateNsRequest struct {
	NsFlavourId     string            `json:"nsFlavorId"`
	VnfInstanceData []VnfInstanceData `json:"vnfInstanceData"`
}

type VnfInstanceData struct {
	VnfInstanceId string `json:"vnfInstanceId"`
}

type CreateNsdRequest struct {
	Metadata common.Metadata `json:"metadata"`
	Spec     common.NsdSpec  `json:"spec"`
}

// ServiceRequest Rest request struct
type ServiceRequest struct {
	MetaData ServiceRequestMetaData `json:"metadata"`
	Spec     ServiceSpec            `json:"spec"`
}

// ServiceDigsUpdate contains update info for adding/removing digs to/from Service
type ServiceDigsUpdate struct {
	Add    []string `json:"add"`
	Remove []string `json:"remove"`
}

// Service represents a logical group of DIGs
type Service struct {
	MetaData ServiceMetaData `json:"metadata"`
	Spec     ServiceSpec     `json:"spec"`
	Status   ServiceStatus   `json:"status"`
}

// ServiceRequestMetaData metadata of the request Service
type ServiceRequestMetaData struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

// ServiceMetaData metadata of the Service
type ServiceMetaData struct {
	Name        string `json:"name"`
	Id          string `json:"id"`
	Project     string `json:"project"`
	Description string `json:"description"`
}

// ServiceSpec Spec info of the service
type ServiceSpec struct {
	Digs []string `json:"digs"`
}

// ServiceStatus Spec info of the service
type ServiceStatus struct {
	DigsStatusMap map[string]bool
}

// DeploymentIntentGroup shall have 2 fields - MetaData and Spec
type DeploymentIntentGroup struct {
	MetaData MetaData    `json:"metadata"`
	Spec     DepSpecData `json:"spec"`
}

// MetaData has Name, description, userdata1, userdata2
type MetaData struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	UserData1   string `json:"userData1"`
	UserData2   string `json:"userData2"`
}

// DepSpecData has profile, version, OverrideValuesObj
type DepSpecData struct {
	Id                   string                 `json:"Id"`
	Profile              string                 `json:"compositeProfile"`
	Version              string                 `json:"version"`
	OverrideValuesObj    []OverrideValues       `json:"overrideValues"`
	LogicalCloud         string                 `json:"logicalCloud"`
	Action               string                 `json:"action,omitempty"`
	DeployedStatus       string                 `json:"deployedStatus,omitempty"`
	ReadyStatus          string                 `json:"readyStatus,omitempty"`
	IsCheckedOut         bool                   `json:"is_checked_out,omitempty"`
	Services             map[string]interface{} `json:"services"`
	InstantiatedServices map[string]interface{} `json:"instantiatedServices"`
}

// OverrideValues has appName and ValuesObj
type OverrideValues struct {
	AppName   string            `json:"app-name"`
	ValuesObj map[string]string `json:"values"`
}

type CompositeAppsInProjectShrunk struct {
	Metadata apiMetaData        `json:"metadata" bson:"metadata"`
	Spec     []CompositeAppSpec `json:"spec" bson:"spec"`
}

type CompositeAppSpec struct {
	Id           string                   `json:"Id" bson:"Id"`
	Status       string                   `json:"status" bson:"status"`
	Version      string                   `json:"compositeAppVersion" bson:"compositeAppVersion"`
	AppsArray    []*Application           `json:"apps,omitempty" bson:"apps,omitempty"`
	ProfileArray []*Profiles              `json:"compositeProfiles,omitempty" bson:"compositeProfiles,omitempty"`
	DigArray     []*DeploymentIntentGroup `json:"deploymentIntentGroups,omitempty" bson:"deploymentIntentGroups,omitempty"`
}

// Application structure
type Application struct {
	Metadata appMetaData `json:"metadata" bson:"metadata"`
}

type Profiles struct {
	Metadata appMetaData `json:"metadata,omitempty" bson:"metadata,omitempty"`
	Spec     struct {
		ProfilesArray []ProfileMeta `json:"profile,omitempty" bson:"profile,omitempty"`
	} `json:"spec,omitempty" bson:"spec,omitempty"`
}

// ProfileMeta is metadata for the profile APIs
type ProfileMeta struct {
	Metadata appMetaData `json:"metadata" bson:"metadata"`
	Spec     ProfileSpec `json:"spec" bson:"spec"`
}

// ProfileSpec is the spec for the profile APIs
type ProfileSpec struct {
	AppName string `json:"app"`
}

type appMetaData struct {
	Name         string `json:"name" bson:"name"`
	Description  string `json:"description" bson:"description"`
	UserData1    string `userData1:"userData1"`
	UserData2    string `userData2:"userData2"`
	ChartContent string `json:"chartContent" bson:"chartContent,omitempty"`
	Status       string `json:"status"bson:"status",omitempty`
}

type apiMetaData struct {
	Name        string `json:"name" bson:"name"`
	Description string `json:"description" bson:"description"`
	UserData1   string `userData1:"userData1"`
	UserData2   string `userData2:"userData2"`
}

type NsdInfoSpec struct {
	VnfdIds []string `json:"vnfdIds"`
	Id      string   `json:"id"`
}

type NsdInfoKey struct {
	Id string `json:"id"`
}

type ScaleSpecRequest struct {
	NsScaleSpec  []NsScaleSpec  `json:"nsScaleSpec"`
	VnfScaleSpec []VnfScaleSpec `json:"vnfScaleSpec"`
}

type NsScaleSpec struct {
	Id        string         `json:"id"`
	VnfCounts map[string]int `json:"vnfCounts"`
}

type VnfScaleSpec struct {
	Id       string `json:"id"`
	VnfCount int    `json:"vnfCount"`
}
type ServiceDigsAction struct {
	Digs              []string `json:"digs"`
	ContinueOnFailure bool     `json:"continueOnFailure"`
}

// HealNsRequest Definition of HealNs in generated library is not correct. Also we need to
// customize. Hence redefining
type HealNsRequest struct {
	HealNsData HealNsData `json:"healNsData"`
}

type HealNsData struct {
	DegreeHealing         string            `json:"degreeHealing"`
	HealScript            string            `json:"healScript"`
	AdditionalParamsforNs map[string]string `json:"additionalParamsforNs"`
}
