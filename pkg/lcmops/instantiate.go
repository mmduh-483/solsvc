package lcmops

import (
	"aarna.com/solsvc/db"
	"aarna.com/solsvc/lib/common"
	nslcm "aarna.com/solsvc/lib/nslcm"
	"context"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

// InstantiateNS Instantiates Network Service
func (c *Client) InstantiateNS(w http.ResponseWriter, r *http.Request) {
	var (
		nsInsReq InstantiateNsRequest
		errMsg   string
	)
	vars := mux.Vars(r)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&nsInsReq)
	if err != nil {
		errMsg = fmt.Sprintf("%s(): failed to parse json: %s", common.PrintFunctionName(), err)
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusBadRequest,
		}
		common.WriteError(problem, http.StatusBadRequest, w, r)
		errMsg = fmt.Sprintf("%s: %s", common.PrintFunctionName(), errMsg)
		log.Error(errMsg)
		return
	}

	nsInfo, problem := c.InstantiateNSRequest(vars["nsInstanceId"])
	if problem != nil {
		common.WriteError(*problem, int(problem.Status), w, r)
		return
	}
	nsLCMOpp, problem := c.createNsLCMOpp(nsInfo, r)
	if problem != nil {
		common.WriteError(*problem, int(problem.Status), w, r)
		return
	}
	c.LocalNotificationHandler.NsInstantiateStarted(nsInfo.NsId, nsLCMOpp.NsLCMOpOccId)
	go c.InstantiateHandler(nsInfo, nsLCMOpp, nsInsReq.VnfInstanceData, false)
	w.Header().Set("Location", nsLCMOpp.Links.Self.Href)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusAccepted)
}

func (c *Client) InstantiateNSRequest(nsInstanceId string) (common.NsInstanceInfo, *nslcm.ProblemDetails) {
	// Fetch NsInfo from database
	retCode, nserviceInfo, err := common.FetchNSInfo(nsInstanceId)
	if retCode != http.StatusOK {
		errMsg := "failed to fetch NsInfo from db"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: int32(retCode),
		}
		return common.NsInstanceInfo{}, &problem
	}

	nsInfo, ok := nserviceInfo.(common.NsInstanceInfo)
	if !ok {
		errMsg := "Unable to parse nsInfo structure..."
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return common.NsInstanceInfo{}, &problem
	}
	if nsInfo.NsState == "INSTANTIATED" {
		errMsg := fmt.Sprintf("NS service is already instantiated")
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: "NS Service state:" + nsInfo.NsState,
			Status: http.StatusConflict,
		}
		return nsInfo, &problem
	}
	return nsInfo, nil
}

func (c *Client) createNsLCMOpp(nsInfo common.NsInstanceInfo, r *http.Request) (common.NsLCMOpOcc, *nslcm.ProblemDetails) {
	// Create NSLCMOppOcc structure in NS COLLECTION
	key := new(common.NsLCMOppOccKey)
	// Generate GUID for NSLCMOPPOCC
	id := uuid.New()
	key.NsLCMOppOccId = strings.Replace(id.String(), "-", "", -1)
	var nsLCMOpp common.NsLCMOpOcc
	nsLCMOpp.NsLCMOpOccId = key.NsLCMOppOccId
	nsLCMOpp.LcmOperationType = common.OPERATION_INSTANTIATE
	nsLCMOpp.NsInstanceId = nsInfo.NsId
	nsLCMOpp.OperationState = common.LCMOPP_OCC_PROCESSING
	nsLCMOpp.StartTime = time.Now().Format(time.RFC3339)
	nsLCMOpp.StatusEnteredTime = time.Now().Format(time.RFC3339)
	nsLCMOpp.IsAutomaticInvocation = false
	nsLCMOpp.IsCancelPending = false

	// Set the location header, as request is successful
	parsedUrl, err := url.Parse(r.URL.String())
	if err != nil {
		errMsg := fmt.Sprintf("%s : Encountered error while parsing url: %s", common.PrintFunctionName(), r.URL.String())
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return nsLCMOpp, &problem
	}
	split := strings.Split(parsedUrl.Path, "/")

	nsLCMOppOccURL := "http://" + r.Host + strings.Join(split[:len(split)-3], "/") + "/ns_lcm_op_occs/" + nsLCMOpp.NsLCMOpOccId

	var selfLink common.Link
	selfLink.Href = nsLCMOppOccURL
	nsLCMOpp.Links.Self = &selfLink
	var nsInstanceLink common.Link
	nsInstanceLink.Href = "http://" + r.Host + strings.Join(split[:len(split)-3], "/") + "/ns_instances/" + nsInfo.NsId
	nsLCMOpp.Links.NsInstance = &nsInstanceLink

	err = db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", nsLCMOpp)
	if err != nil {
		errMsg := fmt.Sprintf("Encountered error during insert of nsLCMOppOcc for %s: %s", nsInfo.NsName, err)
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return nsLCMOpp, &problem
	}
	return nsLCMOpp, nil
}

func (c *Client) InstantiateHandler(nsInfo common.NsInstanceInfo, nsLCMOpp common.NsLCMOpOcc, vnfInstList []VnfInstanceData, continueOnFail bool) {
	problem := c.InstantiateNSAction(nsInfo, nsLCMOpp, vnfInstList, continueOnFail)
	if problem != nil {
		log.Errorf("Instantiation Failed: %+v", problem)
		c.LocalNotificationHandler.NsInstantiateFailedTemp(nsInfo.NsId, nsLCMOpp.NsLCMOpOccId, *problem)
		nsLCMOpp.OperationState = common.LCMOPP_OCC_FAILED_TEMP
		var pb common.ProblemDetails
		pb.Detail = problem.Detail
		pb.Title = problem.Title
		pb.Status = problem.Status
		nsLCMOpp.Error = &pb
		key := new(common.NsLCMOppOccKey)
		key.NsLCMOppOccId = nsLCMOpp.NsLCMOpOccId
		err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", nsLCMOpp)
		if err != nil {
			log.Errorf("Error while recording Instantiation failure %v", err.Error())
		}
	}
}

// Update NsInfo with newly added vnfInstances
func (c *Client) AddVNFInstancesToNsInfo(nsInfo common.NsInstanceInfo, vnfInstances []string, vnfMap map[string]map[string]string) *nslcm.ProblemDetails {
	var (
		vnfInstList []common.VnfInstance
		errMsg      string
	)

	for _, vnfinstance := range vnfInstances {
		_, compositeapp, version, digName := common.FetchDIGDetails(vnfinstance)
		digStatus, pb := common.FetchDigStatus(digName, compositeapp, version, c.Info.Conf.MiddleEnd)
		log.Infof("digStatus: %+v", digStatus)
		if pb != nil {
			return pb
		}
		var vnfInstance common.VnfInstance
		vnfInstance.VnfInstanceId = digStatus.DigId
		vnfInstance.VnfInstanceName = vnfinstance
		if len(vnfMap) > 0 && vnfMap[compositeapp] != nil {
			versionIdMap := vnfMap[compositeapp]
			vnfInstance.VnfdId = versionIdMap[version]
		}
		if strings.ToUpper(digStatus.DeployedStatus) != common.NS_LCM_INSTANTIATED_STATE {
			vnfInstance.State = common.NOT_INSTANTIATED
		} else {
			vnfInstance.State = strings.ToUpper(digStatus.DeployedStatus)
		}

		vnfInstance.Description = "Create " + digName
		vnfInstList = append(vnfInstList, vnfInstance)
	}

	nsInfo.VnfInstance = append(nsInfo.VnfInstance, vnfInstList...)

	key := new(common.NsInstanceKey)
	key.NsId = nsInfo.NsId
	err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nsinfo", nsInfo)
	if err != nil {
		errMsg = fmt.Sprintf("Encountered error during insert of NS info for %s: %s", nsInfo.NsName, err.Error())
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
			Status: http.StatusInternalServerError,
		}
		return &problem
	}
	return nil
}

// Building map of map for mapping composite app to composite app version + id
func (c *Client) CreateCompAppMap() (map[string]map[string]string, *nslcm.ProblemDetails) {
	var (
		errMsg string
	)

	// Building map of map for mapping composite app to composite app version + id
	vnfdMap := make(map[string]map[string]string)
	var compappList []CompositeAppsInProjectShrunk
	h := common.HttpHandler{}
	h.Client = &http.Client{}

	URL := "http://" + c.Info.Conf.MiddleEnd + "/middleend/projects/" +
		common.DEFAULT_PROJECT + "/composite-apps"
	retcode, resp, err := h.ApiGet(URL)
	if retcode != http.StatusOK {
		errMsg = "Unable to fetch list of compapps"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
			Status: int32(retcode.(int)),
		}
		return vnfdMap, &problem
	}

	log.Debugf("compositeapps: %+v", resp)
	err = json.Unmarshal(resp, &compappList)
	if err != nil {
		errMsg = "Unable to unmarshal response..."
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return vnfdMap, &problem
	}

	for _, compapp := range compappList {
		versionToIdMap := make(map[string]string, 0)
		for _, compversion := range compapp.Spec {
			versionToIdMap[compversion.Version] = compversion.Id
		}
		vnfdMap[compapp.Metadata.Name] = versionToIdMap
	}
	return vnfdMap, nil
}

func (c *Client) UpdateNsService(serviceName string, vnfInstanceNames []string) *nslcm.ProblemDetails {
	var (
		errMsg string
	)

	if len(vnfInstanceNames) == 0 {
		return nil
	}

	h := common.HttpHandler{}
	h.Client = &http.Client{}

	serviceUpdatePayLoad := ServiceDigsUpdate{
		Add: vnfInstanceNames,
	}

	payload, err := json.Marshal(serviceUpdatePayLoad)
	if err != nil {
		errMsg = fmt.Sprintf("Encountered error while marshalling service update payload: %s", err.Error())
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
			Status: http.StatusInternalServerError,
		}
		return &problem
	}

	URL := "http://" + c.Info.Conf.Orchestrator + "/v2/projects/" +
		common.DEFAULT_PROJECT + "/services/" + serviceName + "/update"
	retCode, response, err := h.ApiPost(payload, URL)
	if err != nil {
		errMsg = fmt.Sprintf("Encountered error while updating NS service: %s", err.Error())
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
			Status: int32(retCode),
		}
		return &problem
	}

	if retCode != http.StatusAccepted {
		errMsg = fmt.Sprintf("Encountered error while updating NS service %s: %s", serviceName, string(response))
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
			Status: int32(retCode),
		}
		return &problem
	}
	return nil
}

func (c *Client) InstantiateNSAction(nsInfo common.NsInstanceInfo, nsLCMOpp common.NsLCMOpOcc, vnfInstList []VnfInstanceData, continueOnFail bool) *nslcm.ProblemDetails {
	var (
		errMsg           string
		payload          []byte
		vnfInstanceNames []string
		err              error
	)

	// Create a list of VNFs to be added to NS service
	for _, vnfinstance := range vnfInstList {
		vnfInstanceNames = append(vnfInstanceNames, vnfinstance.VnfInstanceId)
	}

	if len(vnfInstanceNames) > 0 {
		// Update NS service with vnfs supplied as part of payload vnfInstanceData
		if pb := c.UpdateNsService(nsInfo.NsName, vnfInstanceNames); pb != nil {
			errMsg = fmt.Sprintf("Encountered error while updating NS service")
			problem := nslcm.ProblemDetails{
				Title:  &errMsg,
				Detail: errMsg,
				Status: int32(pb.Status),
			}
			log.Errorf(errMsg)
			return &problem
		}

		// Create composite app map
		vnfMap, pb := c.CreateCompAppMap()
		if pb != nil {
			errMsg = fmt.Sprintf("Encountered error while creation of VnfMap for NS service")
			problem := nslcm.ProblemDetails{
				Title:  &errMsg,
				Detail: errMsg,
				Status: int32(pb.Status),
			}
			log.Errorf(errMsg)
			return &problem
		}

		// Update NS info with newly added vnfInstances
		if pb = c.AddVNFInstancesToNsInfo(nsInfo, vnfInstanceNames, vnfMap); pb != nil {
			errMsg = fmt.Sprintf("Encountered error while updating nsinfo for NS service")
			problem := nslcm.ProblemDetails{
				Title:  &errMsg,
				Detail: errMsg,
				Status: int32(pb.Status),
			}
			log.Errorf(errMsg)
			return &problem
		}
	}

	if continueOnFail {
		payloadJson := ServiceDigsAction{
			Digs:              nil,
			ContinueOnFailure: true,
		}
		payload, err = json.Marshal(payloadJson)
		if err != nil {
			errMsg = fmt.Sprintf("Encountered error while terminating NS service(json marshelling issue)")
			problem := nslcm.ProblemDetails{
				Title:  &errMsg,
				Detail: err.Error(),
				Status: http.StatusInternalServerError,
			}
			return &problem
		}
	}
	// Invoke instantiate service call of emco service
	h := common.HttpHandler{}
	h.Client = &http.Client{}

	URL := "http://" + c.Info.Conf.Orchestrator + "/v2/projects/" +
		common.DEFAULT_PROJECT + "/services/" + nsInfo.NsName + "/instantiate"
	retCode, response, err := h.ApiPost(payload, URL)
	if err != nil {
		errMsg = fmt.Sprintf("Encountered error while instantiating NS service: %s", err.Error())
		log.Errorf(errMsg)
	}
	if retCode != http.StatusAccepted {
		errMsg = fmt.Sprintf("Encountered error while instantiating NS %s: %s", nsInfo.NsId, string(response))
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
			Status: int32(retCode),
		}
		return &problem
	}

	// Register for instantiate NS notification
	c.EmcoNotificationHandler.RegisterNotifyEmcoInstantiate(context.TODO(), c.Info.Conf, nsInfo, nsLCMOpp, false)
	return nil
}

func (c *Client) InstantiateRetry(nsLCMOpp common.NsLCMOpOcc) *nslcm.ProblemDetails {
	if nsLCMOpp.OperationState != common.LCMOPP_OCC_FAILED_TEMP {
		errMsg := fmt.Sprintf("Retry failed: NS operatation state not in FAILED_TEMP state")
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: "State of operation id: " + nsLCMOpp.NsLCMOpOccId + " is " + nsLCMOpp.OperationState,
			Status: http.StatusConflict,
		}
		return &problem
	}
	// Fetch NsInfo from database
	retCode, nserviceInfo, err := common.FetchNSInfo(nsLCMOpp.NsInstanceId)
	if retCode != http.StatusOK || err != nil {
		detail := "Mongo Fetch status code:" + strconv.Itoa(retCode)
		if err != nil {
			detail += "Error: " + err.Error()
		}
		errMsg := "encountered error while fetching NSInfo"
		problem := &nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: detail,
			Status: int32(retCode),
		}
		return problem
	}

	nsInfo, ok := nserviceInfo.(common.NsInstanceInfo)
	if !ok {
		errMsg := "Unable to parse nsInfo structure..."
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return &problem
	}

	nsLCMOpp.OperationState = common.LCMOPP_OCC_PROCESSING
	key := new(common.NsLCMOppOccKey)
	key.NsLCMOppOccId = nsLCMOpp.NsLCMOpOccId
	err = db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", nsLCMOpp)
	if err != nil {
		errMsg := fmt.Sprintf("Retry failed: Recording state  to db failed")
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return &problem
	}
	c.LocalNotificationHandler.NsInstantiateStarted(nsInfo.NsId, nsLCMOpp.NsLCMOpOccId)
	var vnfInstances []VnfInstanceData
	go c.InstantiateHandler(nsInfo, nsLCMOpp, vnfInstances, false)
	return nil
}

func (c *Client) InstantiateRollback(nsLCMOpp common.NsLCMOpOcc) *nslcm.ProblemDetails {
	if nsLCMOpp.OperationState != common.LCMOPP_OCC_FAILED_TEMP {
		errMsg := fmt.Sprintf("Retry failed: NS operatation state not in FAILED_TEMP state")
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: "State of operation id: " + nsLCMOpp.NsLCMOpOccId + " is " + nsLCMOpp.OperationState,
			Status: http.StatusConflict,
		}
		return &problem
	}
	// Fetch NsInfo from database
	retCode, nserviceInfo, err := common.FetchNSInfo(nsLCMOpp.NsInstanceId)
	if retCode != http.StatusOK {
		errMsg := "failed to fetch NsInfo from db"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return &problem
	}

	nsInfo, ok := nserviceInfo.(common.NsInstanceInfo)
	if !ok {
		errMsg := "Unable to parse nsInfo structure..."
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return &problem
	}

	nsLCMOpp.OperationState = common.LCMOPP_OCC_ROLLING_BACK
	key := new(common.NsLCMOppOccKey)
	key.NsLCMOppOccId = nsLCMOpp.NsLCMOpOccId
	err = db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", nsLCMOpp)
	if err != nil {
		log.Errorf("Error while recording Instantiation status %v", err.Error())
	}
	c.LocalNotificationHandler.NsInstantiateRollingBack(nsInfo.NsdId, nsLCMOpp.NsLCMOpOccId)
	go c.InstantiateRollbackHandler(nsInfo, nsLCMOpp)
	return nil
}

func (c *Client) InstantiateCancel(nsLCMOpp common.NsLCMOpOcc, isContinue bool) (*nslcm.ProblemDetails, bool) {
	if nsLCMOpp.OperationState != common.LCMOPP_OCC_PROCESSING && !isContinue {
		errMsg := fmt.Sprintf("Retry failed: NS operatation state not in PROCESSING state")
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: "State of operation id: " + nsLCMOpp.NsLCMOpOccId + " is " + nsLCMOpp.OperationState,
			Status: http.StatusConflict,
		}
		return &problem, false
	}
	// Fetch NsInfo from database
	retCode, nserviceInfo, err := common.FetchNSInfo(nsLCMOpp.NsInstanceId)
	if retCode != http.StatusOK {
		errMsg := "failed to fetch NsInfo from db"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return &problem, false
	}

	nsInfo, ok := nserviceInfo.(common.NsInstanceInfo)
	if !ok {
		errMsg := "Unable to parse nsInfo structure..."
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return &problem, false
	}
	c.LocalNotificationHandler.NsInstantiateCancelStarted(nsInfo.NsId, nsLCMOpp.NsLCMOpOccId)
	problem, promptContinue := c.InstantiateCancelAction(nsInfo, nsLCMOpp)
	if problem != nil {
		log.Errorf("Error while cancelling %v", problem)
	}
	return problem, promptContinue
}

func (c *Client) TerminateRollback(nsLCMOpp common.NsLCMOpOcc) *nslcm.ProblemDetails {
	// TODO  nsLCMOpp.OperationState != "PROCESSING" need to be removed once FM able to update failures
	if nsLCMOpp.OperationState != common.LCMOPP_OCC_FAILED_TEMP && nsLCMOpp.OperationState != common.LCMOPP_OCC_PROCESSING {
		errMsg := fmt.Sprintf("Rollback failed: NS operatation state not in FAILED_TEMP state")
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: "State of operation id: " + nsLCMOpp.NsLCMOpOccId + " is " + nsLCMOpp.OperationState,
			Status: http.StatusConflict,
		}
		return &problem
	}
	// Fetch NsInfo from database
	retCode, nserviceInfo, err := common.FetchNSInfo(nsLCMOpp.NsInstanceId)
	if retCode != http.StatusOK {
		errMsg := "failed to fetch NsInfo from db"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return &problem
	}

	nsInfo, ok := nserviceInfo.(common.NsInstanceInfo)
	if !ok {
		errMsg := "Unable to parse nsInfo structure..."
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return &problem
	}

	nsLCMOpp.OperationState = common.LCMOPP_OCC_ROLLING_BACK
	key := new(common.NsLCMOppOccKey)
	key.NsLCMOppOccId = nsLCMOpp.NsLCMOpOccId
	err = db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", nsLCMOpp)
	if err != nil {
		log.Errorf("Error while recording Instantiation status %v", err.Error())
	}
	c.LocalNotificationHandler.NsTerminateRollingBack(nsInfo.NsId, nsLCMOpp.NsLCMOpOccId)
	go c.TerminateRollbackHandler(nsInfo, nsLCMOpp)
	return nil
}

func (c *Client) TerminateCancel(nsLCMOpp common.NsLCMOpOcc) *nslcm.ProblemDetails {
	if nsLCMOpp.OperationState != common.LCMOPP_OCC_PROCESSING {
		errMsg := fmt.Sprintf("Cancel failed: NS operatation state not in PROCESSING state")
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: "State of operation id: " + nsLCMOpp.NsLCMOpOccId + " is " + nsLCMOpp.OperationState,
			Status: http.StatusConflict,
		}
		return &problem
	}
	// Fetch NsInfo from database
	retCode, nserviceInfo, err := common.FetchNSInfo(nsLCMOpp.NsInstanceId)
	if retCode != http.StatusOK {
		errMsg := "failed to fetch NsInfo from db"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return &problem
	}

	nsInfo, ok := nserviceInfo.(common.NsInstanceInfo)
	if !ok {
		errMsg := "Unable to parse nsInfo structure..."
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return &problem
	}
	c.LocalNotificationHandler.NsTerminateCancelStarted(nsInfo.NsId, nsLCMOpp.NsLCMOpOccId)
	return c.TerminateNSCancelAction(&nsInfo, nsLCMOpp)
}

func (c *Client) InstantiateRollbackHandler(nsInfo common.NsInstanceInfo, nsLcmOpp common.NsLCMOpOcc) {
	problem := c.InstantiateRollbackAction(nsInfo, nsLcmOpp)
	if problem != nil {
		log.Errorf("Instantiation Rollback Failed: %+v", problem)
		c.LocalNotificationHandler.NsInstantiateRollBackFailed(nsInfo.NsId, nsLcmOpp.NsLCMOpOccId, *problem)
		nsLcmOpp.OperationState = common.LCMOPP_OCC_FAILED_TEMP
		var pb common.ProblemDetails
		pb.Detail = problem.Detail
		pb.Title = problem.Title
		pb.Status = problem.Status
		nsLcmOpp.Error = &pb
		key := new(common.NsLCMOppOccKey)
		key.NsLCMOppOccId = nsLcmOpp.NsLCMOpOccId
		err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", nsLcmOpp)
		if err != nil {
			log.Errorf("Error while recording Instantiation failure %v", err.Error())
		}
		return
	}
	nsLcmOpp.OperationState = common.LCMOPP_OCC_ROLLED_BACK
	key := new(common.NsLCMOppOccKey)
	key.NsLCMOppOccId = nsLcmOpp.NsLCMOpOccId
	err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", nsLcmOpp)
	if err != nil {
		log.Errorf("Error while recording Instantiation status %v", err.Error())
	}
}

func (c *Client) TerminateRollbackHandler(nsInfo common.NsInstanceInfo, nsLcmOpp common.NsLCMOpOcc) {
	problem := c.TerminateNSRollbackAction(&nsInfo, nsLcmOpp)
	if problem != nil {
		log.Errorf("Terminate Rollback Failed: %+v", problem)
		c.LocalNotificationHandler.NsTerminateRollBackFailed(nsInfo.NsId, nsLcmOpp.NsLCMOpOccId, *problem)
		nsLcmOpp.OperationState = common.LCMOPP_OCC_FAILED_TEMP
		var pb common.ProblemDetails
		pb.Detail = problem.Detail
		pb.Title = problem.Title
		pb.Status = problem.Status
		nsLcmOpp.Error = &pb
		key := new(common.NsLCMOppOccKey)
		key.NsLCMOppOccId = nsLcmOpp.NsLCMOpOccId
		err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", nsLcmOpp)
		if err != nil {
			log.Errorf("Error while recording rollback status %v", err.Error())
		}
		return
	}
}

func (c *Client) InstantiateCancelAction(nsInfo common.NsInstanceInfo, nsLcmOpp common.NsLCMOpOcc) (*nslcm.ProblemDetails, bool) {
	// Fetch NS status
	var errMsg string
	h := common.HttpHandler{}
	h.Client = &http.Client{}
	serviceStatus := common.ServiceStatusInfo{}
	URL := "http://" + c.Info.Conf.Orchestrator + "/v2/projects/" +
		common.DEFAULT_PROJECT + "/services/" + nsInfo.NsName + "/status"
	status, data, err := h.ApiGet(URL)
	if err != nil {
		errMsg = fmt.Sprintf("Encountered error while fetching DIG status for NS service: %s", err.Error())
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: status.(int32),
		}
		return &problem, false
	}
	if status != http.StatusOK {
		errMsg = fmt.Sprintf("Encountered error while fetching DIG status for NS service")
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: "Check status code",
			Status: status.(int32),
		}
		return &problem, false
	}

	err = json.Unmarshal(data, &serviceStatus)
	if err != nil {
		errMsg = "Failed to unmarshal DIG json: " + common.PrintFunctionName()
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return &problem, false
	}

	deployedStatus := serviceStatus.DeployedStatus
	if deployedStatus != common.StatusInstantiated {
		errMsg = fmt.Sprintf("State of operation is not cancellable ")
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: "backend state:" + deployedStatus,
			Status: http.StatusInternalServerError,
		}
		return &problem, false
	}
	status, internalStat, err := common.FetchNSLCMOpOccInternalInfo(nsLcmOpp.NsLCMOpOccId)
	if err != nil {
		errMsg = "Failed to get info from db: " + common.PrintFunctionName()
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return &problem, false
	}
	if status == http.StatusOK && internalStat.ContinueStatus == "PROMPTED" {
		go func() {
			internalStatus := common.NsLCMOpOccInternal{
				NsLCMOpOccId:   nsLcmOpp.NsLCMOpOccId,
				ContinueStatus: "EXECUTED",
			}
			key := new(common.NsLCMOppOccKey)
			key.NsLCMOppOccId = nsLcmOpp.NsLCMOpOccId
			err = db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppoccinternal", internalStatus)
			problem := c.TerminateNSAction(&nsInfo, nsLcmOpp, true, false)
			if problem != nil {
				log.Errorln(problem)
			}

		}()
		return nil, false
	}
	internalStatus := common.NsLCMOpOccInternal{
		NsLCMOpOccId:   nsLcmOpp.NsLCMOpOccId,
		ContinueStatus: "PROMPTED",
	}
	key := new(common.NsLCMOppOccKey)
	key.NsLCMOppOccId = nsLcmOpp.NsLCMOpOccId
	err = db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppoccinternal", internalStatus)
	if err != nil {
		errMsg = fmt.Sprintf("Encountered error while writing cancel status ")
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg + err.Error(),
			Status: http.StatusInternalServerError,
		}
		return &problem, false
	}
	return nil, true
}

func (c *Client) InstantiateRollbackAction(nsInfo common.NsInstanceInfo, nsLcmOpp common.NsLCMOpOcc) *nslcm.ProblemDetails {
	// Fetch NS status
	var errMsg string
	h := common.HttpHandler{}
	h.Client = &http.Client{}
	var serviceStatus common.ServiceStatusInfo

	URL := "http://" + c.Info.Conf.Orchestrator + "/v2/projects/" +
		common.DEFAULT_PROJECT + "/services/" + nsInfo.NsName + "/status"

	status, data, err := h.ApiGet(URL)
	if err != nil {
		errMsg = fmt.Sprintf("Encountered error while fetching DIG status for NS service: %s", err.Error())
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: status.(int32),
		}
		return &problem
	}
	if status != http.StatusOK {
		errMsg = fmt.Sprintf("Encountered error while fetching DIG status for NS service")
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: "Check status code",
			Status: http.StatusInternalServerError,
		}
		return &problem
	}

	err = json.Unmarshal(data, &serviceStatus)
	if err != nil {
		errMsg = "Failed to unmarshal DIG json: " + common.PrintFunctionName()
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return &problem
	}

	sStatus := serviceStatus.DeployedStatus
	if sStatus != common.StatusInstantiated && sStatus != common.StatusFailed {
		errMsg = "State of  backend status is not expected "
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: "Service status = " + sStatus,
			Status: http.StatusInternalServerError,
		}
		return &problem
	}
	problem := c.TerminateNSAction(&nsInfo, nsLcmOpp, true, true)
	return problem
}

func (c *Client) TerminateNSRollbackAction(nsInfo *common.NsInstanceInfo, nsLcmOpp common.NsLCMOpOcc) *nslcm.ProblemDetails {
	var errMsg string
	h := common.HttpHandler{}
	h.Client = &http.Client{}
	var serviceStatus common.ServiceStatusInfo

	URL := "http://" + c.Info.Conf.Orchestrator + "/v2/projects/" +
		common.DEFAULT_PROJECT + "/services/" + nsInfo.NsName + "/status"

	status, data, err := h.ApiGet(URL)
	if err != nil {
		errMsg = fmt.Sprintf("Encountered error while fetching DIG status for NS service: %s", err.Error())
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: status.(int32),
		}
		return &problem
	}
	if status != http.StatusOK {
		errMsg = fmt.Sprintf("Encountered error while fetching DIG status for NS service")
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: "Check status code",
			Status: status.(int32),
		}
		return &problem
	}

	err = json.Unmarshal(data, &serviceStatus)
	if err != nil {
		errMsg = "Failed to unmarshal DIG json: " + common.PrintFunctionName()
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return &problem
	}

	sStatus := serviceStatus.DeployedStatus
	for sStatus != "Terminated" {
		log.Infof("Waiting for a safer state for rolling back")
	}

	return c.InstantiateNSAction(*nsInfo, nsLcmOpp, []VnfInstanceData{}, false)
}

func (c *Client) TerminateNSCancelAction(nsInfo *common.NsInstanceInfo, nsLcmOpp common.NsLCMOpOcc) *nslcm.ProblemDetails {
	var errMsg string
	h := common.HttpHandler{}
	h.Client = &http.Client{}
	var serviceStatus common.ServiceStatusInfo

	URL := "http://" + c.Info.Conf.Orchestrator + "/v2/projects/" +
		common.DEFAULT_PROJECT + "/services/" + nsInfo.NsName + "/status"

	status, data, err := h.ApiGet(URL)
	if err != nil {
		errMsg = fmt.Sprintf("Encountered error while fetching DIG status for NS service: %s", err.Error())
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: status.(int32),
		}
		return &problem
	}
	if status != http.StatusOK {
		errMsg = fmt.Sprintf("Encountered error while fetching DIG status for NS service")
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: "Check status code",
			Status: status.(int32),
		}
		return &problem
	}

	err = json.Unmarshal(data, &serviceStatus)
	if err != nil {
		errMsg = "Failed to unmarshal DIG json: " + common.PrintFunctionName()
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return &problem
	}

	sStatus := serviceStatus.DeployedStatus
	if sStatus == "Terminated" {
		errMsg = "VNFs are already terminated. Cancellation not possible"
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: "Service status = " + sStatus,
			Status: http.StatusConflict,
		}
		return &problem
	}

	return c.InstantiateNSAction(*nsInfo, nsLcmOpp, []VnfInstanceData{}, false)
}

func (c *Client) TerminateNSAction(nsInfo *common.NsInstanceInfo, nsLcmOpp common.NsLCMOpOcc, isRollback bool, continueOnFail bool) *nslcm.ProblemDetails {

	var errMsg string
	// Invoke terminate DIG call of emco service
	var payload []byte
	var err error
	h := common.HttpHandler{}
	h.Client = &http.Client{}
	URL := "http://" + c.Info.Conf.Orchestrator + "/v2/projects/" +
		common.DEFAULT_PROJECT + "/services/" + nsInfo.NsName + "/terminate"

	if continueOnFail {
		payloadJson := ServiceDigsAction{
			Digs:              nil,
			ContinueOnFailure: true,
		}
		payload, err = json.Marshal(payloadJson)
		if err != nil {
			errMsg = fmt.Sprintf("Encountered error while terminating NS service(json marshelling issue)")
			problem := nslcm.ProblemDetails{
				Title:  &errMsg,
				Detail: err.Error(),
				Status: http.StatusInternalServerError,
			}
			return &problem
		}
	}
	status, response, err := h.ApiPost(payload, URL)
	if status != http.StatusAccepted || err != nil {
		errMsg = fmt.Sprintf("Encountered error while terminating NS service: %s", string(response))
		detail := "Emco Status code: " + strconv.Itoa(status)
		if err != nil {
			detail += "Error: " + err.Error()
		}
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: detail,
			Status: int32(status),
		}
		return &problem
	}
	c.EmcoNotificationHandler.DeRegisterNotifyEmcoInstantiate(nsInfo.NsId)
	c.EmcoNotificationHandler.RegisterNotifyEmcoTerminate(context.TODO(), c.Info.Conf, *nsInfo, nsLcmOpp, isRollback)
	return nil
}

func (c *Client) TerminateHandler(nsInfo common.NsInstanceInfo, nsLCMOpp common.NsLCMOpOcc, continueOnFail bool) {
	problem := c.TerminateNSAction(&nsInfo, nsLCMOpp, false, continueOnFail)
	if problem != nil {
		log.Errorf("Termination Failed: %+v", problem)
		c.LocalNotificationHandler.NsTerminateFailedTemp(nsInfo.NsId, nsLCMOpp.NsLCMOpOccId, *problem)
		nsLCMOpp.OperationState = common.LCMOPP_OCC_FAILED_TEMP
		var pb common.ProblemDetails
		pb.Detail = problem.Detail
		pb.Title = problem.Title
		pb.Status = problem.Status
		nsLCMOpp.Error = &pb
		key := new(common.NsLCMOppOccKey)
		key.NsLCMOppOccId = nsLCMOpp.NsLCMOpOccId
		err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", nsLCMOpp)
		if err != nil {
			log.Errorf("Error while recording terminate status %v", err.Error())
			return
		}
	}
}

func (c *Client) TerminateNsCreateLCMOpp(nsInfo *common.NsInstanceInfo, r *http.Request) (common.NsLCMOpOcc, *nslcm.ProblemDetails) {

	key := new(common.NsLCMOppOccKey)
	// Generate GUID for NSLCMOPPOCC
	id := uuid.New()
	key.NsLCMOppOccId = strings.Replace(id.String(), "-", "", -1)
	var nsLCMOpp common.NsLCMOpOcc
	nsLCMOpp.LcmOperationType = common.OPERATION_TERMINATE
	nsLCMOpp.NsInstanceId = nsInfo.NsId
	nsLCMOpp.OperationState = common.LCMOPP_OCC_PROCESSING
	nsLCMOpp.StartTime = time.Now().Format(time.RFC3339)
	nsLCMOpp.StatusEnteredTime = time.Now().Format(time.RFC3339)
	nsLCMOpp.NsLCMOpOccId = key.NsLCMOppOccId
	nsLCMOpp.IsAutomaticInvocation = false
	nsLCMOpp.IsCancelPending = false

	// Set the location header, as request is successful
	parsedUrl, err := url.Parse(r.URL.String())
	if err != nil {
		errMsg := fmt.Sprintf("Encountered error while parsing url: %s", r.URL.String())
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return nsLCMOpp, &problem
	}
	split := strings.Split(parsedUrl.Path, "/")

	var nsInstanceLink common.Link
	nsInstanceLink.Href = "http://" + r.Host + strings.Join(split[:len(split)-3], "/") + "/ns_instances/" + nsInfo.NsId
	nsLCMOpp.Links.NsInstance = &nsInstanceLink
	nsLCMOppOccURL := "http://" + r.Host + strings.Join(split[:len(split)-3], "/") + "/ns_lcm_op_occs/" + nsLCMOpp.NsLCMOpOccId
	self := common.Link{Href: nsLCMOppOccURL}
	nsLCMOpp.Links.Self = &self

	err = db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", nsLCMOpp)
	if err != nil {
		errMsg := fmt.Sprintf("Encountered error during insert of nsLCMOppOcc for %s: %s", nsInfo.NsName, err)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return nsLCMOpp, &problem
	}

	return nsLCMOpp, nil
}

func (c *Client) TerminateNsRequest(r *http.Request) (*common.NsLCMOpOcc, *nslcm.ProblemDetails) {
	var (
		errMsg string
	)
	vars := mux.Vars(r)

	// Fetch NsInfo from database
	retCode, nserviceInfo, err := common.FetchNSInfo(vars["nsInstanceId"])
	if retCode != http.StatusOK {
		errMsg = "encountered error while fetching NSInfo"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: int32(retCode),
		}
		return nil, &problem
	}

	nsInfo, ok := nserviceInfo.(common.NsInstanceInfo)
	if !ok {
		errMsg = "Unable to parse nsInfo structure..."
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: int32(retCode),
		}
		return nil, &problem
	}

	// if NS is in NOT_INSTANTIATED state, then raise 409 response
	if nsInfo.NsState == common.NOT_INSTANTIATED {
		errMsg = fmt.Sprintf("NS is already in %s state", common.NOT_INSTANTIATED)
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
			Status: http.StatusConflict,
		}
		return nil, &problem
	}

	nsLcmOpp, problem := c.TerminateNsCreateLCMOpp(&nsInfo, r)
	if problem != nil {
		return &nsLcmOpp, problem
	}
	c.LocalNotificationHandler.NsTerminateStarted(nsInfo.NsId, nsLcmOpp.NsLCMOpOccId)
	go c.TerminateHandler(nsInfo, nsLcmOpp, false)
	return &nsLcmOpp, nil
}

func (c *Client) TerminateNS(w http.ResponseWriter, r *http.Request) {
	nsLcmOpp, problem := c.TerminateNsRequest(r)
	if problem != nil {
		common.WriteError(*problem, int(problem.Status), w, r)
		return
	}
	w.Header().Set("Location", nsLcmOpp.Links.Self.Href)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusAccepted)
}

func (c *Client) RetryTerminate(nsLCMOpp common.NsLCMOpOcc) *nslcm.ProblemDetails {
	if nsLCMOpp.OperationState != common.LCMOPP_OCC_FAILED_TEMP {
		errMsg := fmt.Sprintf("Retry failed: NS operatation state not in FAILED_TEMP state")
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: "State of operation id: " + nsLCMOpp.NsLCMOpOccId + " is " + nsLCMOpp.OperationState,
			Status: http.StatusConflict,
		}
		return &problem
	}
	retCode, nserviceInfo, err := common.FetchNSInfo(nsLCMOpp.NsInstanceId)
	if retCode != http.StatusOK || err != nil {
		detail := "Mongo Fetch status code:" + strconv.Itoa(retCode)
		if err != nil {
			detail += "Error: " + err.Error()
		}
		errMsg := "encountered error while fetching NSInfo"
		problem := &nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: detail,
			Status: int32(retCode),
		}
		return problem
	}

	nsInfo, ok := nserviceInfo.(common.NsInstanceInfo)
	if !ok {
		errMsg := "Unable to parse nsInfo structure..."
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: int32(retCode),
		}
		return &problem
	}
	nsLCMOpp.OperationState = common.LCMOPP_OCC_PROCESSING
	key := new(common.NsLCMOppOccKey)
	key.NsLCMOppOccId = nsLCMOpp.NsLCMOpOccId
	err = db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", nsLCMOpp)
	if err != nil {
		log.Errorf("Error while recording retry status %v", err.Error())
		errMsg := fmt.Sprintf("Error while recording retry status %v", err.Error())
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: "NS Service state:" + nsInfo.NsState,
			Status: http.StatusInternalServerError,
		}
		return &problem
	}
	go c.TerminateHandler(nsInfo, nsLCMOpp, false)
	return nil
}

func (c *Client) RollBackNS(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var problem *nslcm.ProblemDetails
	retCode, nsLCMInfo, err := common.FetchNSLCMOpOccInfo(vars["nsLCMOppOccId"])
	if retCode != http.StatusOK || err != nil {
		detail := "Mongo Fetch status code:" + strconv.Itoa(retCode)
		if err != nil {
			detail += "Error: " + err.Error()
		}
		errMsg := "encountered error while fetching nsLCMOppOccId"
		problem = &nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: detail,
		}
		common.WriteError(*problem, http.StatusInternalServerError, w, r)
		return
	}
	// Clear error field.
	key := new(common.NsLCMOppOccKey)
	key.NsLCMOppOccId = nsLCMInfo.NsLCMOpOccId
	err = db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", nsLCMInfo)

	switch nsLCMInfo.LcmOperationType {
	case common.OPERATION_INSTANTIATE:
		problem = c.InstantiateRollback(nsLCMInfo)
	case common.OPERATION_TERMINATE:
		problem = c.TerminateRollback(nsLCMInfo)
	case common.OPERATION_HEAL, common.OPERATION_SCALE, common.OPERATION_UPDATE:
		title := "Rollback not supported"
		problem = &nslcm.ProblemDetails{
			Title:  &title,
			Status: http.StatusConflict,
			Detail: nsLCMInfo.LcmOperationType + " Operation cannot rolled back. Operation id: " + nsLCMInfo.NsLCMOpOccId,
		}
	default:
		title := "Invalid operation. Not retryable"
		problem = &nslcm.ProblemDetails{
			Title:  &title,
			Status: http.StatusConflict,
			Detail: "Type: " + nsLCMInfo.LcmOperationType + " is not defined for retry. Operation id: " + nsLCMInfo.NsLCMOpOccId,
		}
	}

	if problem != nil {
		common.WriteError(*problem, int(problem.Status), w, r)
		return
	}
	w.Header().Set("Location", nsLCMInfo.Links.Self.Href)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusAccepted)
}

func (c *Client) RetryNS(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var problem *nslcm.ProblemDetails
	retCode, nsLCMInfo, err := common.FetchNSLCMOpOccInfo(vars["nsLCMOppOccId"])
	if retCode != http.StatusOK || err != nil {
		detail := "Mongo Fetch status code:" + strconv.Itoa(retCode)
		if err != nil {
			detail += "Error: " + err.Error()
		}
		errMsg := "encountered error while fetching nsLCMOppOccId"
		problem = &nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: detail,
		}
		common.WriteError(*problem, http.StatusInternalServerError, w, r)
		return
	}

	switch nsLCMInfo.LcmOperationType {
	case common.OPERATION_INSTANTIATE:
		problem = c.InstantiateRetry(nsLCMInfo)
	case common.OPERATION_TERMINATE:
		problem = c.RetryTerminate(nsLCMInfo)
	case common.OPERATION_HEAL, common.OPERATION_SCALE, common.OPERATION_UPDATE:
		title := "Retry not supported"
		problem = &nslcm.ProblemDetails{
			Title:  &title,
			Status: http.StatusConflict,
			Detail: nsLCMInfo.LcmOperationType + " Operation cannot retried. Operation id: " + nsLCMInfo.NsLCMOpOccId,
		}
	default:
		title := "Invalid operation. Not retryable"
		problem = &nslcm.ProblemDetails{
			Title:  &title,
			Status: http.StatusConflict,
			Detail: "Type: " + nsLCMInfo.LcmOperationType + " is not defined for retry. Operation id: " + nsLCMInfo.NsLCMOpOccId,
		}
	}

	if problem != nil {
		common.WriteError(*problem, int(problem.Status), w, r)
		return
	}
	w.Header().Set("Location", nsLCMInfo.Links.Self.Href)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusAccepted)
}

func (c *Client) CancelOps(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var problem *nslcm.ProblemDetails
	var continuePrompt bool
	retCode, nsLCMInfo, err := common.FetchNSLCMOpOccInfo(vars["nsLCMOppOccId"])
	if retCode != http.StatusOK || err != nil {
		detail := "Mongo Fetch status code:" + strconv.Itoa(retCode)
		if err != nil {
			detail += "Error: " + err.Error()
		}
		errMsg := "encountered error while fetching nsLCMOppOccId"
		problem = &nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: detail,
		}
		common.WriteError(*problem, http.StatusInternalServerError, w, r)
		return
	}
	switch nsLCMInfo.LcmOperationType {
	case common.OPERATION_INSTANTIATE:
		problem, continuePrompt = c.InstantiateCancel(nsLCMInfo, false)
	case common.OPERATION_TERMINATE:
		problem = c.TerminateCancel(nsLCMInfo)
	case common.OPERATION_HEAL, common.OPERATION_SCALE, common.OPERATION_UPDATE:
		title := "Operation is not cancellable"
		problem = &nslcm.ProblemDetails{
			Title:  &title,
			Status: http.StatusConflict,
			Detail: nsLCMInfo.LcmOperationType + " Operation is not cancellable. Operation id: " + nsLCMInfo.NsLCMOpOccId,
		}
	default:
		title := "Invalid operation. Not cancellable"
		problem = &nslcm.ProblemDetails{
			Title:  &title,
			Status: http.StatusConflict,
			Detail: "Type: " + nsLCMInfo.LcmOperationType + " is not defined for cancel. Operation id: " + nsLCMInfo.NsLCMOpOccId,
		}
	}

	if problem != nil {
		common.WriteError(*problem, int(problem.Status), w, r)
		return
	}
	if continuePrompt {
		title := "Cancel operation is destructive"
		problem := &nslcm.ProblemDetails{
			Type:     nil,
			Title:    &title,
			Status:   0,
			Detail:   "This Cancel operation can be destructive. It will rollback the operation. Invoke continue for further action",
			Instance: &r.RequestURI,
		}
		c.LocalNotificationHandler.NsInstantiateFailedTemp(nsLCMInfo.NsInstanceId, nsLCMInfo.NsLCMOpOccId, *problem)
		nsLCMInfo.OperationState = common.LCMOPP_OCC_FAILED_TEMP
		nsLCMInfo.Error = &common.ProblemDetails{
			Title:    problem.Title,
			Detail:   problem.Detail,
			Instance: problem.Instance,
		}
		key := new(common.NsLCMOppOccKey)
		key.NsLCMOppOccId = nsLCMInfo.NsLCMOpOccId
		err = db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", nsLCMInfo)
		if err != nil {
			log.Errorf("Error while updating operation %v", err.Error())
			errMsg := "encountered error while updating operation"
			problem = &nslcm.ProblemDetails{
				Title:  &errMsg,
				Detail: err.Error(),
			}
			common.WriteError(*problem, http.StatusInternalServerError, w, r)
			return
		}

		common.WriteError(*problem, http.StatusAccepted, w, r)
		return
	}
	w.Header().Set("Location", nsLCMInfo.Links.Self.Href)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusAccepted)
}

func (c *Client) ContinueOps(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var problem *nslcm.ProblemDetails
	retCode, nsLCMInfo, err := common.FetchNSLCMOpOccInfo(vars["nsLCMOppOccId"])
	if retCode != http.StatusOK || err != nil {
		detail := "Mongo Fetch status code:" + strconv.Itoa(retCode)
		if err != nil {
			detail += "Error: " + err.Error()
		}
		errMsg := "encountered error while fetching nsLCMOppOccId"
		problem = &nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: detail,
		}
		common.WriteError(*problem, http.StatusInternalServerError, w, r)
		return
	}
	_, internalStat, err := common.FetchNSLCMOpOccInternalInfo(nsLCMInfo.NsLCMOpOccId)
	if err != nil {
		errMsg := "Failed to get info from db: " + common.PrintFunctionName()
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}
	// Clear error field.
	key := new(common.NsLCMOppOccKey)
	key.NsLCMOppOccId = nsLCMInfo.NsLCMOpOccId
	err = db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", nsLCMInfo)

	if internalStat.ContinueStatus == "PROMPTED" {
		problem = nil
		if nsLCMInfo.LcmOperationType == common.OPERATION_INSTANTIATE {
			problem, _ = c.InstantiateCancel(nsLCMInfo, true)
		}
		if problem != nil {
			common.WriteError(*problem, int(problem.Status), w, r)
			return
		}
		w.Header().Set("Location", nsLCMInfo.Links.Self.Href)
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusAccepted)
		return
	}

	if nsLCMInfo.LcmOperationType == common.OPERATION_TERMINATE && nsLCMInfo.OperationState == common.LCMOPP_OCC_FAILED_TEMP &&
		internalStat.ContinueStatus != "FAILCONTINUE" {
		problem = nil
		retCode, nServiceInfo, err := common.FetchNSInfo(nsLCMInfo.NsInstanceId)
		if retCode != http.StatusOK {
			errMsg := "failed to fetch NsInfo from db"
			problem := nslcm.ProblemDetails{
				Title:  &errMsg,
				Detail: err.Error(),
				Status: int32(retCode),
			}
			common.WriteError(problem, int(problem.Status), w, r)
			return
		}

		nsInfo, ok := nServiceInfo.(common.NsInstanceInfo)
		if !ok {
			errMsg := "Unable to parse nsInfo structure..."
			problem := nslcm.ProblemDetails{
				Title:  &errMsg,
				Detail: err.Error(),
				Status: http.StatusInternalServerError,
			}
			common.WriteError(problem, int(problem.Status), w, r)
			return
		}
		c.LocalNotificationHandler.NsTerminateContinueStarted(nsInfo.NsId, nsLCMInfo.NsLCMOpOccId)
		go c.TerminateHandler(nsInfo, nsLCMInfo, true)
		if problem != nil {
			common.WriteError(*problem, int(problem.Status), w, r)
			return
		}
		w.Header().Set("Location", nsLCMInfo.Links.Self.Href)
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusAccepted)
		return
	}

	if nsLCMInfo.LcmOperationType == common.OPERATION_INSTANTIATE && nsLCMInfo.OperationState == common.LCMOPP_OCC_FAILED_TEMP &&
		internalStat.ContinueStatus != "FAILCONTINUE" {
		problem = nil
		retCode, nServiceInfo, err := common.FetchNSInfo(nsLCMInfo.NsInstanceId)
		if retCode != http.StatusOK {
			errMsg := "failed to fetch NsInfo from db"
			problem := nslcm.ProblemDetails{
				Title:  &errMsg,
				Detail: err.Error(),
				Status: int32(retCode),
			}
			common.WriteError(problem, int(problem.Status), w, r)
			return
		}

		nsInfo, ok := nServiceInfo.(common.NsInstanceInfo)
		if !ok {
			errMsg := "Unable to parse nsInfo structure..."
			problem := nslcm.ProblemDetails{
				Title:  &errMsg,
				Detail: err.Error(),
				Status: http.StatusInternalServerError,
			}
			common.WriteError(problem, int(problem.Status), w, r)
			return
		}
		c.LocalNotificationHandler.NsInstantiateContinueStarted(nsInfo.NsId, nsLCMInfo.NsLCMOpOccId)
		go c.InstantiateHandler(nsInfo, nsLCMInfo, []VnfInstanceData{}, true)
		if problem != nil {
			common.WriteError(*problem, int(problem.Status), w, r)
			return
		}
		w.Header().Set("Location", nsLCMInfo.Links.Self.Href)
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusAccepted)
		return
	}

	errMsg := "Operation cannot continued"
	problem = &nslcm.ProblemDetails{
		Title:  &errMsg,
		Detail: errMsg,
	}
	common.WriteError(*problem, http.StatusConflict, w, r)

}

func (c *Client) FailOps(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var problem *nslcm.ProblemDetails
	retCode, nsLCMInfo, err := common.FetchNSLCMOpOccInfo(vars["nsLCMOppOccId"])
	if retCode != http.StatusOK || err != nil {
		detail := "Mongo Fetch status code:" + strconv.Itoa(retCode)
		if err != nil {
			detail += "Error: " + err.Error()
		}
		errMsg := "encountered error while fetching nsLCMOppOccId"
		problem = &nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: detail,
		}
		common.WriteError(*problem, http.StatusInternalServerError, w, r)
		return
	}
	if nsLCMInfo.OperationState != common.LCMOPP_OCC_FAILED_TEMP {
		errMsg := "Cannot mark as failed. Operation not in the state FAILED_TEMP."
		problem = &nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: "Operation should be in FAILED_TEMP state to mark as FAILED. Current state is " + nsLCMInfo.OperationState,
		}
		common.WriteError(*problem, http.StatusConflict, w, r)
		return
	}
	nsLCMInfo.OperationState = common.LCMOPP_OCC_FAILED
	key := new(common.NsLCMOppOccKey)
	key.NsLCMOppOccId = nsLCMInfo.NsLCMOpOccId
	err = db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", nsLCMInfo)
	if err != nil {
		log.Errorf("Error while updating operation %v", err.Error())
		errMsg := "encountered error while updating operation"
		problem = &nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(*problem, http.StatusInternalServerError, w, r)
		return
	}

	w.Header().Set("Location", nsLCMInfo.Links.Self.Href)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusAccepted)
}
