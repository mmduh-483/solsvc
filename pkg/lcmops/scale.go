package lcmops

import (
	"aarna.com/solsvc/db"
	"aarna.com/solsvc/lib/common"
	nslcm "aarna.com/solsvc/lib/nslcm"
	nslcmnotification "aarna.com/solsvc/lib/nslcmnotification"
	"encoding/json"
	"fmt"
	uuid2 "github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

func (c *Client) ScalePostRequest(w http.ResponseWriter, r *http.Request) {
	var (
		request nslcm.ScaleNsRequest
	)
	vars := mux.Vars(r)
	nsId := vars["nsInstanceId"]
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&request); err != nil {
		title := "Error while parsing request body"
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusBadRequest, w, r)
		return
	}
	c.doScale(nsId, request, w, r)
}

func (c *Client) ScaleSpecRequest(w http.ResponseWriter, r *http.Request) {
	var (
		request ScaleSpecRequest
	)
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&request); err != nil {
		title := "Error while parsing request body"
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusBadRequest, w, r)
		return
	}
	for _, nsSpec := range request.NsScaleSpec {
		key := struct {
			Id string `json:"id"`
		}{
			Id: nsSpec.Id,
		}
		if err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nsScaleSpec", nsSpec); err != nil {
			title := "Error while inserting to DB "
			problem := nslcm.ProblemDetails{
				Title:  &title,
				Detail: err.Error(),
			}
			common.WriteError(problem, http.StatusBadRequest, w, r)
			return
		}
	}

	for _, vnfSpec := range request.VnfScaleSpec {
		key := struct {
			Id string `json:"id"`
		}{
			Id: vnfSpec.Id,
		}
		if err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "vnfScaleSpec", vnfSpec); err != nil {
			title := "Error while inserting to DB "
			problem := nslcm.ProblemDetails{
				Title:  &title,
				Detail: err.Error(),
			}
			common.WriteError(problem, http.StatusBadRequest, w, r)
			return
		}
	}
}

func (c *Client) ScaleSpecGet(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]
	key := struct {
		Id string `json:"id"`
	}{
		Id: id,
	}
	data, err := db.DBconn.Find(common.NS_COLLECTION, key, "nsScaleSpec")
	if err != nil {
		title := "Error while reading from DB "
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusBadRequest, w, r)
		return
	}
	if len(data) == 0 {
		title := "Id not found "
		problem := nslcm.ProblemDetails{
			Title: &title,
		}
		common.WriteError(problem, http.StatusNotFound, w, r)
		return
	}
	spec := new(NsScaleSpec)
	err = db.DBconn.Unmarshal(data[0], spec)
	if err != nil {
		title := "Error while reading from DB "
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}
	response, err := json.Marshal(spec)
	if err != nil {
		title := "Error while encoding data"
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}
	w.WriteHeader(http.StatusOK)
	_, err = w.Write(response)
	if err != nil {
		log.Errorf("Ignored error: %s", err.Error())
	}
}

func (c *Client) GetVnfScaleSpec(id string) (VnfScaleSpec, *nslcm.ProblemDetails) {
	key := struct {
		Id string `json:"id"`
	}{
		Id: id,
	}
	spec := new(VnfScaleSpec)
	data, err := db.DBconn.Find(common.NS_COLLECTION, key, "vnfScaleSpec")
	if err != nil {
		title := "Error while reading from DB "
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: err.Error(),
		}
		return *spec, &problem
	}

	if len(data) == 0 {
		title := "Scale Id not found "
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: "Scale id " + id,
		}
		return *spec, &problem
	}

	err = db.DBconn.Unmarshal(data[0], spec)
	if err != nil {
		title := "Error while reading from DB "
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: err.Error(),
		}

		return *spec, &problem
	}
	return *spec, nil
}

func (c *Client) VnfScaleSpecGet(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]
	key := struct {
		Id string `json:"id"`
	}{
		Id: id,
	}
	data, err := db.DBconn.Find(common.NS_COLLECTION, key, "vnfScaleSpec")
	if err != nil {
		title := "Error while reading from DB "
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusBadRequest, w, r)
		return
	}
	if len(data) == 0 {
		title := "Id not found "
		problem := nslcm.ProblemDetails{
			Title: &title,
		}
		common.WriteError(problem, http.StatusNotFound, w, r)
		return
	}
	spec := new(VnfScaleSpec)
	err = db.DBconn.Unmarshal(data[0], spec)
	if err != nil {
		title := "Error while reading from DB "
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}
	response, err := json.Marshal(spec)
	if err != nil {
		title := "Error while encoding data"
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}
	w.WriteHeader(http.StatusOK)
	_, err = w.Write(response)
	if err != nil {
		log.Errorf("Ignored error: %s", err.Error())
	}
}

func (c *Client) GetNsScaleSpec(id string) (NsScaleSpec, *nslcm.ProblemDetails) {
	key := struct {
		Id string `json:"id"`
	}{
		Id: id,
	}
	spec := new(NsScaleSpec)
	data, err := db.DBconn.Find(common.NS_COLLECTION, key, "nsScaleSpec")
	if err != nil {
		title := "Error while reading from DB "
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: err.Error(),
		}
		return *spec, &problem
	}
	if len(data) == 0 {
		title := "Scale Id not found "
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: "Scale id " + id,
		}
		return *spec, &problem
	}

	err = db.DBconn.Unmarshal(data[0], spec)
	if err != nil {
		title := "Error while reading from DB "
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: err.Error(),
		}
		return *spec, &problem
	}
	return *spec, nil
}

func (c *Client) UpdatePostRequest(w http.ResponseWriter, r *http.Request) {
	var (
		request    nslcm.UpdateNsRequest
		nsLCMOpOcc common.NsLCMOpOcc
		nsInfo     common.NsInstanceInfo
	)

	vars := mux.Vars(r)
	nsId := vars["nsInstanceId"]
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&request); err != nil {
		title := "Update : Error while parsing request body"
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}
	nsInfo, pb := c.checkNsStateForUpdate(nsId)
	if pb != nil {
		common.WriteError(*pb, int(pb.Status), w, r)
		return
	}
	nsLCMOpOcc, problem := c.doUpdateNs(request, nsInfo, r)
	if problem != nil {
		common.WriteError(*problem, http.StatusInternalServerError, w, r)
		return
	}
	w.Header().Set("Location", nsLCMOpOcc.Links.Self.Href)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusAccepted)
}

func (c *Client) doScale(nsId string, request nslcm.ScaleNsRequest, w http.ResponseWriter, r *http.Request) bool {
	statusCode, serviceInfo, err := common.FetchNSInfo(nsId)
	if err != nil {
		title := "Generic error during scale"
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: err.Error(),
		}
		if statusCode != http.StatusNotFound {
			statusCode = http.StatusInternalServerError
		}
		common.WriteError(problem, statusCode, w, r)
		return false
	}
	nsInfo, ok := serviceInfo.(common.NsInstanceInfo)

	if !ok {
		title := "Generic error during scale"
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: "Parsing nsInfo interface failed",
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return false
	}

	if nsInfo.NsState != common.NS_LCM_INSTANTIATED_STATE {
		title := "NS is not in INSTANTIATED state"
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: "NS is not in INSTANTIATED state",
		}
		common.WriteError(problem, http.StatusConflict, w, r)
		return false
	}
	op, problem := c.CreateLCMOppStructure(nsInfo, "SCALE", r)
	if problem != nil {
		common.WriteError(*problem, http.StatusInternalServerError, w, r)
	}
	switch request.ScaleType {
	case "SCALE_NS":
		{
			problem := c.doScaleNs(nsInfo, request, op)
			if problem != nil {
				common.WriteError(*problem, http.StatusInternalServerError, w, r)
				return false
			}
		}

	case "SCALE_VNF":
		problem := c.doScaleVnf(nsInfo, request, op)
		if problem != nil {
			common.WriteError(*problem, http.StatusInternalServerError, w, r)
			return false
		}
	default:
		title := "ScaleType Not supported"
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: "ScaleType " + request.ScaleType + "not supported",
		}
		common.WriteError(problem, http.StatusBadRequest, w, r)
		return false
	}
	w.Header().Set("Location", op.Links.Self.Href)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusAccepted)
	return true

}

func (c *Client) doScaleVnf(nsInfo common.NsInstanceInfo, request nslcm.ScaleNsRequest, operation common.NsLCMOpOcc) *nslcm.ProblemDetails {
	scaleVnfData := request.VnfData
	vnfInstanceId := scaleVnfData.VnfInstanceId
	switch scaleVnfData.ScaleVnfType {

	case "SCALE_TO_SCALE_LEVEL":
		levelId := scaleVnfData.ScaleToLevelData.VnfInstantiationLevelId
		levelSpec, problem := c.GetVnfScaleSpec(levelId)
		if problem != nil {
			return problem
		}
		level := map[string]int{vnfInstanceId: levelSpec.VnfCount}
		c.LocalNotificationHandler.NsScaleStarted(nsInfo.NsId, operation.NsLCMOpOccId)
		go func() {
			if problem, _ := c.vnfScaleByClone(nsInfo, request, level, operation, &levelId); problem != nil {
				c.LocalNotificationHandler.NsScaleFailedTemp(nsInfo.NsId, operation.NsLCMOpOccId, *problem, nil)
				operation.OperationState = common.LCMOPP_OCC_FAILED_TEMP
				var pb common.ProblemDetails
				pb.Detail = problem.Detail
				pb.Title = problem.Title
				pb.Status = problem.Status
				operation.Error = &pb
				key := new(common.NsLCMOppOccKey)
				key.NsLCMOppOccId = operation.NsLCMOpOccId
				err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", operation)
				if err != nil {
					log.Errorf("Error while recording Scale operation failure %v", err.Error())
				}
				return
			}
			operation.OperationState = common.LCMOPP_OCC_COMPLETED
			key := new(common.NsLCMOppOccKey)
			key.NsLCMOppOccId = operation.NsLCMOpOccId
			err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", operation)
			if err != nil {
				log.Errorf("Error while recording Scale operation failure %v", err.Error())
			}
			c.LocalNotificationHandler.NsScaleCompleted(nsInfo.NsId, operation.NsLCMOpOccId, nil)
		}()
		return nil

	case "SCALE_OUT":

		level := map[string]int{vnfInstanceId: 1}
		c.LocalNotificationHandler.NsScaleStarted(nsInfo.NsId, operation.NsLCMOpOccId)
		go func() {
			var affectedVnfs []nslcmnotification.AffectedVnf

			problem, vnfData := c.vnfScaleByClone(nsInfo, request, level, operation, nil)
			if problem != nil {
				affectedVnfMap, _ := c.GetAffectedVnfDetails(true, nsInfo.NsId)
				fmt.Printf("Jacob vnfData %v", vnfData)
				var vnfs []string
				for _, vnfd := range vnfData {
					vnfs = append(vnfs, vnfd.VnfInstanceId)
				}

				for _, vnf := range vnfs {
					a := nslcmnotification.AffectedVnf{
						VnfInstanceId: vnf,
					}
					if affectedVnfMap != nil {
						if t, ok := affectedVnfMap[vnf]; ok {
							a = t
						}
					}
					a.ChangeType = "ADD"
					a.ChangeResult = "FAILED"
					affectedVnfs = append(affectedVnfs, a)
				}

				c.LocalNotificationHandler.NsScaleFailedTemp(nsInfo.NsId, operation.NsLCMOpOccId, *problem, affectedVnfs)
				operation.AffectedVnfs = affectedVnfs
				operation.OperationState = common.LCMOPP_OCC_FAILED_TEMP
				var pb common.ProblemDetails
				pb.Detail = problem.Detail
				pb.Title = problem.Title
				pb.Status = problem.Status
				operation.Error = &pb
				key := new(common.NsLCMOppOccKey)
				key.NsLCMOppOccId = operation.NsLCMOpOccId
				err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", operation)
				if err != nil {
					log.Errorf("Error while recording Scale operation failure %v", err.Error())
				}
				return
			}
			affectedVnfMap, _ := c.GetAffectedVnfDetails(true, nsInfo.NsId)
			var vnfs []string
			for _, vnfd := range vnfData {
				vnfs = append(vnfs, vnfd.VnfInstanceId)
			}
			for _, vnf := range vnfs {
				a := nslcmnotification.AffectedVnf{
					VnfInstanceId: vnf,
				}
				if affectedVnfMap != nil {
					if t, ok := affectedVnfMap[vnf]; ok {
						a = t
					}
				}
				a.ChangeType = "ADD"
				a.ChangeResult = "COMPLETED"
				affectedVnfs = append(affectedVnfs, a)
			}
			operation.AffectedVnfs = affectedVnfs
			operation.OperationState = common.LCMOPP_OCC_COMPLETED
			key := new(common.NsLCMOppOccKey)
			key.NsLCMOppOccId = operation.NsLCMOpOccId
			err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", operation)
			if err != nil {
				log.Errorf("Error while recording Scale operation failure %v", err.Error())
			}
			c.LocalNotificationHandler.NsScaleCompleted(nsInfo.NsId, operation.NsLCMOpOccId, affectedVnfs)
		}()
		return nil

	case "SCALE_UP":
		var affectedVnfs []nslcmnotification.AffectedVnf
		level := map[string]int{vnfInstanceId: 1}
		c.LocalNotificationHandler.NsScaleStarted(nsInfo.NsId, operation.NsLCMOpOccId)
		go func() {
			if problem, _ := c.vnfScaleByClone(nsInfo, request, level, operation, nil); problem != nil {
				affectedVnfMap, _ := c.GetAffectedVnfDetails(true, nsInfo.NsId)
				vnfs := []string{request.VnfData.VnfInstanceId}
				for _, vnf := range vnfs {
					a := nslcmnotification.AffectedVnf{
						VnfInstanceId: vnf,
					}
					if affectedVnfMap != nil {
						if t, ok := affectedVnfMap[vnf]; ok {
							a = t
						}
					}
					a.ChangeType = "ADD"
					a.ChangeResult = "FAILED"
					affectedVnfs = append(affectedVnfs, a)
				}
				operation.AffectedVnfs = affectedVnfs
				c.LocalNotificationHandler.NsScaleFailedTemp(nsInfo.NsId, operation.NsLCMOpOccId, *problem, affectedVnfs)
				operation.OperationState = common.LCMOPP_OCC_FAILED_TEMP
				var pb common.ProblemDetails
				pb.Detail = problem.Detail
				pb.Title = problem.Title
				pb.Status = problem.Status
				operation.Error = &pb
				key := new(common.NsLCMOppOccKey)
				key.NsLCMOppOccId = operation.NsLCMOpOccId
				err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", operation)
				if err != nil {
					log.Errorf("Error while recording Scale operation failure %v", err.Error())
				}
				return
			}
			affectedVnfMap, _ := c.GetAffectedVnfDetails(true, nsInfo.NsId)
			vnfs := []string{request.VnfData.VnfInstanceId}
			for _, vnf := range vnfs {
				a := nslcmnotification.AffectedVnf{
					VnfInstanceId: vnf,
				}
				if affectedVnfMap != nil {
					if t, ok := affectedVnfMap[vnf]; ok {
						a = t
					}
				}
				a.ChangeType = "ADD"
				a.ChangeResult = "COMPLETED"
				affectedVnfs = append(affectedVnfs, a)
			}
			operation.AffectedVnfs = affectedVnfs
			operation.OperationState = common.LCMOPP_OCC_COMPLETED
			key := new(common.NsLCMOppOccKey)
			key.NsLCMOppOccId = operation.NsLCMOpOccId
			err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", operation)
			if err != nil {
				log.Errorf("Error while recording Scale operation failure %v", err.Error())
			}
			c.LocalNotificationHandler.NsScaleCompleted(nsInfo.NsId, operation.NsLCMOpOccId, affectedVnfs)
		}()
		return nil

	case "SCALE_IN":
		c.LocalNotificationHandler.NsScaleStarted(nsInfo.NsId, operation.NsLCMOpOccId)
		go func() {
			var affectedVnfs []nslcmnotification.AffectedVnf
			removeId := []string{vnfInstanceId}
			affectedVnfMap, _ := c.GetAffectedVnfDetails(true, nsInfo.NsId)
			problem := c.removeVnfFromNs(removeId, nsInfo)
			if problem != nil {
				log.Errorf("Scale operation Failed: %+v", problem)
				vnfs := []string{request.VnfData.VnfInstanceId}
				for _, vnf := range vnfs {
					a := nslcmnotification.AffectedVnf{
						VnfInstanceId: vnf,
					}
					if affectedVnfMap != nil {
						if t, ok := affectedVnfMap[vnf]; ok {
							a = t
						}
					}
					a.ChangeType = "REMOVE"
					a.ChangeResult = "COMPLETED"
					affectedVnfs = append(affectedVnfs, a)
				}
				operation.AffectedVnfs = affectedVnfs
				c.LocalNotificationHandler.NsScaleFailedTemp(nsInfo.NsId, operation.NsLCMOpOccId, *problem, affectedVnfs)
				operation.OperationState = common.LCMOPP_OCC_FAILED_TEMP
				var pb common.ProblemDetails
				pb.Detail = problem.Detail
				pb.Title = problem.Title
				pb.Status = problem.Status
				operation.Error = &pb
				key := new(common.NsLCMOppOccKey)
				key.NsLCMOppOccId = operation.NsLCMOpOccId
				err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", operation)
				if err != nil {
					log.Errorf("Error while recording Scale operation failure %v", err.Error())
				}
				return
			}
			vnfs := []string{request.VnfData.VnfInstanceId}
			for _, vnf := range vnfs {
				a := nslcmnotification.AffectedVnf{
					VnfInstanceId: vnf,
				}
				if affectedVnfMap != nil {
					if t, ok := affectedVnfMap[vnf]; ok {
						a = t
					}
				}
				a.ChangeType = "REMOVE"
				a.ChangeResult = "COMPLETED"
				affectedVnfs = append(affectedVnfs, a)
			}
			operation.AffectedVnfs = affectedVnfs
			operation.OperationState = common.LCMOPP_OCC_COMPLETED
			key := new(common.NsLCMOppOccKey)
			key.NsLCMOppOccId = operation.NsLCMOpOccId
			err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", operation)
			if err != nil {
				log.Errorf("Error while recording Scale operation failure %v", err.Error())
			}
			c.LocalNotificationHandler.NsScaleCompleted(nsInfo.NsId, operation.NsLCMOpOccId, affectedVnfs)
		}()
		return nil
	default:
		title := "Unsupported scale vnf type"
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: "Unsupported scale vnf type" + scaleVnfData.ScaleVnfType,
		}
		return &problem
	}

}

func (c *Client) vnfScaleByClone(nsInfo common.NsInstanceInfo, request nslcm.ScaleNsRequest, level map[string]int, operation common.NsLCMOpOcc, levelId *string) (*nslcm.ProblemDetails, []nslcm.VnfInstanceData) {
	//c.LocalNotificationHandler.NsUpdateCompleted(nsInfo.NsId, operation.NsLCMOpOccId, nil)
	digs, problem := c.ScaleVnfsByClone(level, nsInfo.NsName)
	if problem != nil {
		return problem, nil
	}
	if levelId != nil {
		scaleStatus := nslcm.NsScaleInfo{NsScaleLevelId: *levelId}
		if nsInfo.NsScaleStatus == nil {
			nsInfo.NsScaleStatus = []nslcm.NsScaleInfo{}
		}
		nsInfo.NsScaleStatus = append(nsInfo.NsScaleStatus, scaleStatus)
		key := new(common.NsInstanceKey)
		key.NsId = nsInfo.NsId
		err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nsinfo", nsInfo)
		if err != nil {
			errMsg := fmt.Sprintf("Encountered error during insert of NS info for %s: %s", nsInfo.NsName, err.Error())
			problem := nslcm.ProblemDetails{
				Title:  &errMsg,
				Detail: errMsg,
				Status: http.StatusInternalServerError,
			}
			return &problem, nil
		}
	}
	var vnfData []nslcm.VnfInstanceData
	for _, dig := range digs {
		n := nslcm.VnfInstanceData{VnfInstanceId: dig.Spec.Id}
		vnfData = append(vnfData, n)
	}

	if problem := c.addVnfToNs(vnfData, nsInfo); problem != nil {
		return problem, nil
	}
	return nil, vnfData
}

func (c *Client) updateVnfToScaleLevel(vnfInstanceId string, vnfInstantiationLevelId string) *nslcm.ProblemDetails {
	vnfIds := []string{vnfInstanceId}
	digIdMap, problem := c.GetVnfToDigMap(vnfIds)
	if problem != nil {
		return problem
	}

	dig, ok := digIdMap[vnfInstanceId]
	digSplit := strings.Split(dig, ".")
	if !ok || len(digSplit) < 4 {
		title := "DIG not found"
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: "DIG not found/not valid for vnfinstanceid " + vnfInstanceId + " dig:" + dig,
		}
		return &problem
	}
	/*  This might be required in the future. Hence, keeping as comment
	for appName, count := range scaleSpec.AppsCount {
		if err := c.doGacReplicaUpdate(digSplit[1], digSplit[2], digSplit[3], appName, count); err != nil {
			title := "Error while changing replica count"
			problem := nslcm.ProblemDetails{
				Title:  &title,
				Detail: err.Error(),
			}
			return &problem
		}
	}*/
	return nil
}

func (c *Client) doGacReplicaUpdate(compApp string, version string, dig string, appName string, targetReplica int) error {
	// This function will be tested properly once emco bug is verified.
	// Create New GAC intent
	var rscUrl string
	var digResponse DeploymentIntentGroup
	h := common.HttpHandler{}
	h.Client = &http.Client{}
	//Get DIG
	URL := "http://" + c.Info.Conf.Orchestrator + "/v2/projects/" + common.DEFAULT_PROJECT +
		"/composite-apps/" + compApp + "/" + version +
		"/deployment-intent-groups/" + dig
	status, resp, err := h.ApiGet(URL)
	if err != nil {
		return err
	}
	if status != http.StatusOK {
		return errors.New("Cound not get DIG details in doGacReplicaUpdate.")
	}

	err = json.Unmarshal(resp, &digResponse)
	log.Debugf("digResponse: %+v", digResponse)
	if err != nil {
		return err
	}
	// Create resources
	gacId := compApp + "_genk8sint"
	rscUrl = "http://" + c.Info.Conf.Gac + "/v2/projects/" + common.DEFAULT_PROJECT +
		"/composite-apps/" + compApp + "/" + version +
		"/deployment-intent-groups/" + dig + "/generic-k8s-intents/" +
		gacId + "/resources"
	resourceId := "gacres-" + uuid2.New().String()
	resource := common.Resource{
		Metadata: common.Metadata{
			Name:        resourceId,
			Description: "Autogenerated by solsvc",
		},
		Spec: common.ResourceSpec{
			App:       appName,
			NewObject: "false",
			ResourceGVK: common.ResourceGVK{
				APIVersion: "apps/v1",
				Kind:       "Deployment",
				Name:       digResponse.Spec.Version + "-" + appName,
			},
		},
	}
	resourceJson, err := json.Marshal(resource)
	if err != nil {
		return errors.Wrap(err, "doGacReplicaUpdate:resourceJson marshal failed")
	}

	fileNames := []string{"resourceFile.yaml"}
	fileContents := []string{""}
	vars := make(map[string]string)
	_, err = h.ApiMultiPartUpload(resourceJson, nil, rscUrl, fileNames, fileContents, vars)
	if err != nil {
		return errors.Wrap(err, "doGacReplicaUpdate: POST failed url: "+rscUrl)
	}

	//Apply Customizations
	rscUrl = "http://" + c.Info.Conf.Gac + "/v2/projects/" + common.DEFAULT_PROJECT +
		"/composite-apps/" + compApp + "/" + version +
		"/deployment-intent-groups/" + dig + "/generic-k8s-intents/" +
		gacId + "/resources/" + resourceId + "/customizations"
	customId := "cust-" + uuid2.New().String()
	patchJsonMap := make(map[string]interface{})
	patchJsonMap["op"] = "replace"
	patchJsonMap["path"] = "/spec/replicas"
	patchJsonMap["value"] = targetReplica
	patchJson := []map[string]interface{}{patchJsonMap}
	customizations := common.Customization{
		Metadata: common.Metadata{
			Name:        customId,
			Description: "Autogenerated by solsvc",
		},
		Spec: common.CustomizeSpec{
			ClusterSpecific: "false",
			ClusterInfo: common.ClusterConfigInfo{
				Scope:           "label",
				ClusterProvider: "provider_1",
				ClusterName:     "cluster_1",
				ClusterLabel:    "label_a",
				Mode:            "allow",
			},
			PatchType: "json",
			PatchJSON: patchJson,
		},
	}
	customizationsJson, err := json.Marshal(customizations)
	if err != nil {
		return errors.Wrap(err, "doGacReplicaUpdate:customizationsJson marshal failed")
	}
	_, err = h.ApiMultiPartUpload(customizationsJson, nil, rscUrl, fileNames, fileContents, vars)
	if err != nil {
		return errors.Wrap(err, "doGacReplicaUpdate: POST failed url: "+rscUrl)
	}

	//Update
	var updateJson []byte
	rscUrl = "http://" + c.Info.Conf.Orchestrator + "/v2/projects/" + common.DEFAULT_PROJECT +
		"/composite-apps/" + compApp + "/" + version +
		"/deployment-intent-groups/" + dig + "/update"
	_, _, err = h.ApiPost(updateJson, rscUrl)
	if err != nil {
		return errors.Wrap(err, "doGacReplicaUpdate: POST failed url: "+rscUrl)
	}
	return nil
}

func (c *Client) checkNsStateForUpdate(nsId string) (common.NsInstanceInfo, *nslcm.ProblemDetails) {
	var (
		nsInfo common.NsInstanceInfo
	)
	statusCode, serviceInfo, err := common.FetchNSInfo(nsId)
	if err != nil {
		title := "Generic error during update"
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: err.Error(),
			Status: int32(statusCode),
		}
		return nsInfo, &problem
	}
	nsInfo, ok := serviceInfo.(common.NsInstanceInfo)

	if !ok {
		title := "Generic error during update"
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: "Parsing nsInfo interface failed",
			Status: http.StatusInternalServerError,
		}
		return nsInfo, &problem
	}
	if nsInfo.NsState != common.NS_LCM_INSTANTIATED_STATE {
		title := "NS is not in INSTANTIATED state"
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: "NS is not in INSTANTIATED state",
			Status: http.StatusConflict,
		}
		return nsInfo, &problem
	}
	return nsInfo, nil
}

func (c *Client) doUpdateNs(request nslcm.UpdateNsRequest, nsInfo common.NsInstanceInfo, r *http.Request) (common.NsLCMOpOcc, *nslcm.ProblemDetails) {
	var (
		operation common.NsLCMOpOcc
	)

	operation, problem := c.CreateLCMOppStructure(nsInfo, "UPDATE", r)
	if problem != nil {
		return operation, problem
	}
	switch request.UpdateType {
	case common.UPDATE_TYPE_ADD_VNF:
		go func() {
			var affectedVnfs []nslcmnotification.AffectedVnf
			problem := c.addVnfToNs(request.AddVnfIstance, nsInfo)
			affectedVnfMap, _ := c.GetAffectedVnfDetails(true, nsInfo.NsId)
			if problem != nil {
				log.Errorf("Update operation Failed: %+v", problem)
				for _, vnf := range request.AddVnfIstance {
					a := nslcmnotification.AffectedVnf{
						VnfInstanceId: vnf.VnfInstanceId,
					}
					if affectedVnfMap != nil {
						if t, ok := affectedVnfMap[vnf.VnfInstanceId]; ok {
							a = t
						}
					}
					a.ChangeType = "ADD"
					a.ChangeResult = "FAILED"
					affectedVnfs = append(affectedVnfs, a)
				}
				operation.AffectedVnfs = affectedVnfs
				c.LocalNotificationHandler.NsUpdateFailedTemp(nsInfo.NsId, operation.NsLCMOpOccId, *problem, affectedVnfs)
				operation.OperationState = common.LCMOPP_OCC_FAILED_TEMP
				var pb common.ProblemDetails
				pb.Detail = problem.Detail
				pb.Title = problem.Title
				pb.Status = problem.Status
				operation.Error = &pb
				key := new(common.NsLCMOppOccKey)
				key.NsLCMOppOccId = operation.NsLCMOpOccId
				err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", operation)
				if err != nil {
					log.Errorf("Error while recording Scale operation failure %v", err.Error())
				}
				return
			}
			for _, vnf := range request.AddVnfIstance {
				a := nslcmnotification.AffectedVnf{
					VnfInstanceId: vnf.VnfInstanceId,
				}
				if affectedVnfMap != nil {
					if t, ok := affectedVnfMap[vnf.VnfInstanceId]; ok {
						a = t
					}
				}
				a.ChangeType = "ADD"
				a.ChangeResult = "COMPLETED"
				affectedVnfs = append(affectedVnfs, a)
			}
			operation.AffectedVnfs = affectedVnfs
			operation.OperationState = common.LCMOPP_OCC_COMPLETED
			key := new(common.NsLCMOppOccKey)
			key.NsLCMOppOccId = operation.NsLCMOpOccId
			err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", operation)
			if err != nil {
				log.Errorf("Error while recording Scale operation failure %v", err.Error())
			}
			c.LocalNotificationHandler.NsUpdateCompleted(nsInfo.NsId, operation.NsLCMOpOccId, affectedVnfs)
		}()
		c.LocalNotificationHandler.NsUpdateStarted(nsInfo.NsId, operation.NsLCMOpOccId)
		return operation, nil
	case common.UPDATE_TYPE_REMOVE_VNF:
		go func() {
			var affectedVnfs []nslcmnotification.AffectedVnf
			//Get vnf details before removing
			affectedVnfMap, _ := c.GetAffectedVnfDetails(true, nsInfo.NsId)
			problem := c.removeVnfFromNs(request.RemoveVnfInstanceId, nsInfo)
			if problem != nil {
				log.Errorf("Update operation Failed: %+v", problem)
				for _, vnf := range request.RemoveVnfInstanceId {
					a := nslcmnotification.AffectedVnf{
						VnfInstanceId: vnf,
					}
					if affectedVnfMap != nil {
						if t, ok := affectedVnfMap[vnf]; ok {
							a = t
						}
					}
					a.ChangeType = "REMOVE"
					a.ChangeResult = "FAILED"
					affectedVnfs = append(affectedVnfs, a)
				}
				operation.AffectedVnfs = affectedVnfs
				c.LocalNotificationHandler.NsUpdateFailedTemp(nsInfo.NsId, operation.NsLCMOpOccId, *problem, affectedVnfs)
				operation.OperationState = common.LCMOPP_OCC_FAILED_TEMP
				var pb common.ProblemDetails
				pb.Detail = problem.Detail
				pb.Title = problem.Title
				pb.Status = problem.Status
				operation.Error = &pb
				key := new(common.NsLCMOppOccKey)
				key.NsLCMOppOccId = operation.NsLCMOpOccId
				err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", operation)
				if err != nil {
					log.Errorf("Error while recording Scale operation failure %v", err.Error())
				}
				return
			}
			for _, vnf := range request.RemoveVnfInstanceId {
				a := nslcmnotification.AffectedVnf{
					VnfInstanceId: vnf,
				}
				if affectedVnfMap != nil {
					if t, ok := affectedVnfMap[vnf]; ok {
						a = t
					}
				}
				a.ChangeType = "REMOVE"
				a.ChangeResult = "COMPLETED"
				affectedVnfs = append(affectedVnfs, a)
			}
			operation.AffectedVnfs = affectedVnfs
			operation.OperationState = common.LCMOPP_OCC_COMPLETED
			key := new(common.NsLCMOppOccKey)
			key.NsLCMOppOccId = operation.NsLCMOpOccId
			err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", operation)
			if err != nil {
				log.Errorf("Error while recording Scale operation failure %v", err.Error())
			}
			c.LocalNotificationHandler.NsUpdateCompleted(nsInfo.NsId, operation.NsLCMOpOccId, affectedVnfs)
		}()
		c.LocalNotificationHandler.NsUpdateStarted(nsInfo.NsId, operation.NsLCMOpOccId)
		return operation, nil
	case common.UPDATE_TYPE_MOVE_VNF:
		go func() {
			problem := c.moveVnf(request.MoveVnfInstanceData, nsInfo)
			if problem != nil {
				log.Errorf("Update operation Failed: %+v", problem)
				c.LocalNotificationHandler.NsUpdateFailedTemp(nsInfo.NsId, operation.NsLCMOpOccId, *problem, nil)
				operation.OperationState = common.LCMOPP_OCC_FAILED_TEMP
				var pb common.ProblemDetails
				pb.Detail = problem.Detail
				pb.Title = problem.Title
				pb.Status = problem.Status
				operation.Error = &pb
				key := new(common.NsLCMOppOccKey)
				key.NsLCMOppOccId = operation.NsLCMOpOccId
				err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", operation)
				if err != nil {
					log.Errorf("Error while recording Scale operation failure %v", err.Error())
				}
				return
			}

			operation.OperationState = common.LCMOPP_OCC_COMPLETED
			key := new(common.NsLCMOppOccKey)
			key.NsLCMOppOccId = operation.NsLCMOpOccId
			err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", operation)
			if err != nil {
				log.Errorf("Error while recording Scale operation failure %v", err.Error())
			}
			c.LocalNotificationHandler.NsUpdateCompleted(nsInfo.NsId, operation.NsLCMOpOccId, nil)
		}()
		c.LocalNotificationHandler.NsUpdateStarted(nsInfo.NsId, operation.NsLCMOpOccId)
		return operation, nil
	case common.UPDATE_TYPE_INSTANTIATE_VNF:

		go func() {
			var affectedVnfs []nslcmnotification.AffectedVnf
			problem := c.InstantiateVnf(request.InstantiateVnfData, nsInfo)
			affectedVnfMap, _ := c.GetAffectedVnfDetails(false, nsInfo.NsId)
			if problem != nil {
				log.Errorf("Update operation Failed: %+v", problem)
				for _, vnf := range request.InstantiateVnfData {
					a := nslcmnotification.AffectedVnf{
						VnfdId: *vnf.VnfdId,
					}
					if affectedVnfMap != nil {
						if t, ok := affectedVnfMap[*vnf.VnfdId]; ok {
							a = t
						}
					}
					a.ChangeType = "INSTANTIATE"
					a.ChangeResult = "FAILED"
					affectedVnfs = append(affectedVnfs, a)
				}
				operation.AffectedVnfs = affectedVnfs
				c.LocalNotificationHandler.NsUpdateFailedTemp(nsInfo.NsId, operation.NsLCMOpOccId, *problem, affectedVnfs)
				operation.OperationState = common.LCMOPP_OCC_FAILED_TEMP
				var pb common.ProblemDetails
				pb.Detail = problem.Detail
				pb.Title = problem.Title
				pb.Status = problem.Status
				operation.Error = &pb
				key := new(common.NsLCMOppOccKey)
				key.NsLCMOppOccId = operation.NsLCMOpOccId
				err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", operation)
				if err != nil {
					log.Errorf("Error while recording Scale operation failure %v", err.Error())
				}
				return
			}
			for _, vnf := range request.InstantiateVnfData {

				a := nslcmnotification.AffectedVnf{
					VnfdId: *vnf.VnfdId,
				}
				if affectedVnfMap != nil {
					if t, ok := affectedVnfMap[*vnf.VnfdId]; ok {
						a = t
					}
				}
				a.ChangeType = "INSTANTIATE"
				a.ChangeResult = "COMPLETED"
				affectedVnfs = append(affectedVnfs, a)
			}
			operation.AffectedVnfs = affectedVnfs
			operation.OperationState = common.LCMOPP_OCC_COMPLETED
			key := new(common.NsLCMOppOccKey)
			key.NsLCMOppOccId = operation.NsLCMOpOccId
			err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", operation)
			if err != nil {
				log.Errorf("Error while recording Update operation failure %v", err.Error())
			}
			c.LocalNotificationHandler.NsUpdateCompleted(nsInfo.NsId, operation.NsLCMOpOccId, affectedVnfs)
		}()
		c.LocalNotificationHandler.NsUpdateStarted(nsInfo.NsId, operation.NsLCMOpOccId)
		return operation, nil
	case common.UPDATE_TYPE_OPERATE_VNF:
		go func() {
			var affectedVnfs []nslcmnotification.AffectedVnf
			problem := c.OperateVnf(request.OperateVnfData, nsInfo)
			affectedVnfMap, _ := c.GetAffectedVnfDetails(true, nsInfo.NsId)
			if problem != nil {
				log.Errorf("Update operation Failed: %+v", problem)
				for _, vnf := range request.OperateVnfData {
					a := nslcmnotification.AffectedVnf{
						VnfInstanceId: vnf.VnfInstanceId,
					}
					if affectedVnfMap != nil {
						if t, ok := affectedVnfMap[vnf.VnfInstanceId]; ok {
							a = t
						}
					}
					a.ChangeType = "OPERATE"
					a.ChangeResult = "FAILED"
					affectedVnfs = append(affectedVnfs, a)
				}
				operation.AffectedVnfs = affectedVnfs
				c.LocalNotificationHandler.NsUpdateFailedTemp(nsInfo.NsId, operation.NsLCMOpOccId, *problem, affectedVnfs)
				operation.OperationState = common.LCMOPP_OCC_FAILED_TEMP
				var pb common.ProblemDetails
				pb.Detail = problem.Detail
				pb.Title = problem.Title
				pb.Status = problem.Status
				operation.Error = &pb
				key := new(common.NsLCMOppOccKey)
				key.NsLCMOppOccId = operation.NsLCMOpOccId
				err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", operation)
				if err != nil {
					log.Errorf("Error while recording Scale operation failure %v", err.Error())
				}
				return
			}
			for _, vnf := range request.OperateVnfData {
				a := nslcmnotification.AffectedVnf{
					VnfInstanceId: vnf.VnfInstanceId,
				}
				if affectedVnfMap != nil {
					if t, ok := affectedVnfMap[vnf.VnfInstanceId]; ok {
						a = t
					}
				}
				a.ChangeType = "OPERATE"
				a.ChangeResult = "COMPLETED"
				affectedVnfs = append(affectedVnfs, a)
			}
			operation.AffectedVnfs = affectedVnfs
			operation.OperationState = common.LCMOPP_OCC_COMPLETED
			key := new(common.NsLCMOppOccKey)
			key.NsLCMOppOccId = operation.NsLCMOpOccId
			err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", operation)
			if err != nil {
				log.Errorf("Error while recording Update operation failure %v", err.Error())
			}
			c.LocalNotificationHandler.NsUpdateCompleted(nsInfo.NsId, operation.NsLCMOpOccId, affectedVnfs)
		}()
		c.LocalNotificationHandler.NsUpdateStarted(nsInfo.NsId, operation.NsLCMOpOccId)
		return operation, nil
	default:
		title := "Unsupported Update type"
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: "Update Type is missing or unsupported. Provided type =" + request.UpdateType,
		}
		return operation, &problem
	}
}

func (c *Client) addVnfToNs(vnfData []nslcm.VnfInstanceData, nsInfo common.NsInstanceInfo) *nslcm.ProblemDetails {
	var (
		compositeApps []CompositeAppsInProjectShrunk
		digsToAdd     []string
		project       = common.DEFAULT_PROJECT
		nsId          = nsInfo.NsName
	)
	vnfMap := make(map[string]bool)
	h := common.HttpHandler{}
	h.Client = &http.Client{}
	for _, vnf := range vnfData {
		vnfMap[vnf.VnfInstanceId] = true
	}
	URL := "http://" + c.Info.Conf.MiddleEnd + "/middleend/projects/" +
		common.DEFAULT_PROJECT + "/composite-apps"
	statusCode, resp, err := h.ApiGet(URL)
	if statusCode != http.StatusOK || err != nil {
		errMsg := "Issue while connecting to backend"
		if err == nil {
			message := fmt.Sprintf("URL %s returned status code: %v", URL, statusCode)
			err = errors.New(message)
		}
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		log.Errorf("%s: %s :ERR: %v", common.PrintFunctionName(), errMsg, err.Error())
		return &problem
	}
	err = json.Unmarshal(resp, &compositeApps)
	if err != nil {
		errMsg := "Issue while decoding backend response"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		log.Errorf("%s: %s : %s", common.PrintFunctionName(), errMsg, err.Error())
		return &problem
	}

	for _, compApp := range compositeApps {
		for _, spec := range compApp.Spec {
			var digs []DeploymentIntentGroup
			digUrl := "http://" + c.Info.Conf.MiddleEnd + "/middleend/projects/" +
				common.DEFAULT_PROJECT + "/composite-apps/" + compApp.Metadata.Name + "/" + spec.Version + "/deployment-intent-groups"
			statusCode, response, err := h.ApiGet(digUrl)
			if statusCode != http.StatusOK || err != nil {
				errMsg := "Issue while connecting to backend"
				if err == nil {
					message := fmt.Sprintf("URL %s returned status code: %v", URL, statusCode)
					err = errors.New(message)
				}
				problem := nslcm.ProblemDetails{
					Title:  &errMsg,
					Detail: err.Error(),
					Status: http.StatusInternalServerError,
				}
				log.Errorf("%s: %s :ERR: %v", common.PrintFunctionName(), errMsg, err.Error())
				return &problem
			}

			err = json.Unmarshal(response, &digs)
			if err != nil {
				errMsg := "Issue while decoding backend response"
				problem := nslcm.ProblemDetails{
					Title:  &errMsg,
					Detail: err.Error(),
				}
				log.Errorf("%s: %s : %s", common.PrintFunctionName(), errMsg, err.Error())
				return &problem
			}
			for _, dig := range digs {
				if vnfMap[dig.Spec.Id] {
					d := project + "." + compApp.Metadata.Name + "." + spec.Version + "." + dig.MetaData.Name
					digsToAdd = append(digsToAdd, d)
				}
			}
		}
	}

	if problem := c.addVnfHelper(digsToAdd, nsId); problem != nil {
		return problem
	}
	vnfMapAll, problem := c.CreateCompAppMap()
	if problem != nil {
		return problem
	}

	// Quickfix: Wait for sometime to all digs to change state. Else we reach status check for digs
	// too early
	time.Sleep(30 * time.Second)

	return c.AddVNFInstancesToNsInfo(nsInfo, digsToAdd, vnfMapAll)
}

func (c *Client) addVnfHelper(digIds []string, nsId string) *nslcm.ProblemDetails {
	h := common.HttpHandler{}
	h.Client = &http.Client{}
	updateUrl := "http://" + c.Info.Conf.Orchestrator + "/v2/projects/" +
		common.DEFAULT_PROJECT + "/services/" + nsId + "/update"
	updateSpec := struct {
		Add []string `json:"add"`
	}{Add: digIds}
	data, err := json.Marshal(updateSpec)
	if err != nil {
		errMsg := "Issue while packing data"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		log.Errorf("%s: %s :ERR: %v", common.PrintFunctionName(), errMsg, err.Error())
		return &problem
	}
	statusCode, response, err := h.ApiPostRespBody(data, updateUrl)
	if statusCode != http.StatusAccepted || err != nil {
		errMsg := fmt.Sprintf("Issue while connecting to backend: %s", string(response))
		if err == nil {
			message := fmt.Sprintf("URL %s returned status code: %v", updateUrl, statusCode)
			err = errors.New(message)
		}
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		log.Errorf("%s: %s :ERR: %v", common.PrintFunctionName(), errMsg, err.Error())
		return &problem
	}
	return nil
}
func (c *Client) removeVnfFromNs(vnfIds []string, nsInfo common.NsInstanceInfo) *nslcm.ProblemDetails {
	var (
		compositeApps []CompositeAppsInProjectShrunk
		digsToRemove  []string
		project       = common.DEFAULT_PROJECT
		nsId          = nsInfo.NsName
	)
	vnfMap := make(map[string]bool)
	h := common.HttpHandler{}
	h.Client = &http.Client{}
	for _, vnf := range vnfIds {
		vnfMap[vnf] = true
	}
	URL := "http://" + c.Info.Conf.MiddleEnd + "/middleend/projects/" +
		common.DEFAULT_PROJECT + "/composite-apps"
	statusCode, resp, err := h.ApiGet(URL)
	if statusCode != http.StatusOK || err != nil {
		errMsg := "Issue while connecting to backend"
		if err == nil {
			message := fmt.Sprintf("URL %s returned status code: %v", URL, statusCode)
			err = errors.New(message)
		}
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		log.Errorf("%s: %s :ERR: %v", common.PrintFunctionName(), errMsg, err.Error())
		return &problem
	}
	err = json.Unmarshal(resp, &compositeApps)
	if err != nil {
		errMsg := "Issue while decoding backend response"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		log.Errorf("%s: %s : %s", common.PrintFunctionName(), errMsg, err.Error())
		return &problem
	}

	for _, compApp := range compositeApps {
		for _, spec := range compApp.Spec {
			var digs []DeploymentIntentGroup
			digUrl := "http://" + c.Info.Conf.MiddleEnd + "/middleend/projects/" +
				common.DEFAULT_PROJECT + "/composite-apps/" + compApp.Metadata.Name + "/" + spec.Version + "/deployment-intent-groups"
			statusCode, response, err := h.ApiGet(digUrl)
			if statusCode != http.StatusOK || err != nil {
				errMsg := "Issue while connecting to backend"
				if err == nil {
					message := fmt.Sprintf("URL %s returned status code: %v", URL, statusCode)
					err = errors.New(message)
				}
				problem := nslcm.ProblemDetails{
					Title:  &errMsg,
					Detail: err.Error(),
					Status: http.StatusInternalServerError,
				}
				log.Errorf("%s: %s :ERR: %v", common.PrintFunctionName(), errMsg, err.Error())
				return &problem
			}

			err = json.Unmarshal(response, &digs)
			if err != nil {
				errMsg := "Issue while decoding backend response"
				problem := nslcm.ProblemDetails{
					Title:  &errMsg,
					Detail: err.Error(),
				}
				log.Errorf("%s: %s : %s", common.PrintFunctionName(), errMsg, err.Error())
				return &problem
			}
			for _, dig := range digs {
				if vnfMap[dig.Spec.Id] {
					d := project + "." + compApp.Metadata.Name + "." + spec.Version + "." + dig.MetaData.Name
					digsToRemove = append(digsToRemove, d)
				}
			}
		}
	}
	if problem := c.removeVnfHelper(digsToRemove, nsId); problem != nil {
		return problem
	}
	return c.RemoveVnfsFromNSInfo(nsInfo, digsToRemove)
}

func (c *Client) removeVnfHelper(digIds []string, nsId string) *nslcm.ProblemDetails {
	h := common.HttpHandler{}
	h.Client = &http.Client{}
	updateUrl := "http://" + c.Info.Conf.Orchestrator + "/v2/projects/" +
		common.DEFAULT_PROJECT + "/services/" + nsId + "/update"
	updateSpec := struct {
		Remove []string `json:"remove"`
	}{Remove: digIds}
	data, err := json.Marshal(updateSpec)
	if err != nil {
		errMsg := "Issue while packing data"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		log.Errorf("%s: %s :ERR: %v", common.PrintFunctionName(), errMsg, err.Error())
		return &problem
	}
	statusCode, response, err := h.ApiPostRespBody(data, updateUrl)
	if statusCode != http.StatusAccepted || err != nil {
		errMsg := fmt.Sprintf("Issue while connecting to backend: %s", string(response))
		if err == nil {
			message := fmt.Sprintf("URL %s returned status code: %v", updateUrl, statusCode)
			err = errors.New(message)
		}
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		log.Errorf("%s: %s :ERR: %v", common.PrintFunctionName(), errMsg, err.Error())
		return &problem
	}
	return nil
}

func (c *Client) InstantiateVnf(vnfData []nslcm.InstantiateVnfData, nsInfo common.NsInstanceInfo) *nslcm.ProblemDetails {
	if nsInfo.NsState != common.NS_LCM_INSTANTIATED_STATE {
		errMsg := "For instantiate vnf NS should be in instantiated state"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: "Current state: " + nsInfo.NsState,
			Status: http.StatusInternalServerError,
		}
		return &problem
	}
	// Create VNFD Map
	vnfdMap, problem := c.CreateVNFDMap()
	var digIdList []string
	if problem != nil {
		return problem
	}

	// Fetch cluster info
	cInfoList, problem := c.FetchClusterInfo(nsInfo)
	if problem != nil {
		return problem
	}

	// Iterate through each vnfdid and create corresponding DIG
	for i, vnfdata := range vnfData {
		vnfdId := *vnfdata.VnfdId
		val := vnfdMap[vnfdId]
		log.Debugf("retreival for %s from vnfdMap: %s", vnfdId, val)
		valList := strings.Split(val, " ")
		compappname, version := valList[0], valList[1]

		dig, pb := c.CreateDIG(nsInfo, compappname, version, int(time.Now().Unix())+i, cInfoList, []common.Day0Config{})
		if pb != nil {
			return pb
		}

		// Update digIdMap
		digName := dig.MetaData.Name
		digId := common.DEFAULT_PROJECT + "." + compappname + "." + version + "." + digName
		digIdList = append(digIdList, digId)

	}
	if problem := c.addVnfHelper(digIdList, nsInfo.NsName); problem != nil {
		return problem
	}

	vnfMapAll, problem := c.CreateCompAppMap()
	if problem != nil {
		return problem
	}

	// Quickfix: Wait for sometime to all digs to change state. Else we reach status check for digs
	// too early
	time.Sleep(30 * time.Second)

	return c.AddVNFInstancesToNsInfo(nsInfo, digIdList, vnfMapAll)
	return nil
}

func (c *Client) OperateVnf(opData []nslcm.OperateVnfData, nsInfo common.NsInstanceInfo) *nslcm.ProblemDetails {
	if nsInfo.NsState != common.NS_LCM_INSTANTIATED_STATE {
		errMsg := "For operate vnf NS should be in instantiated state"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: "Current state: " + nsInfo.NsState,
			Status: http.StatusInternalServerError,
		}
		return &problem
	}
	var stopVnfs, startVnfs []string
	for _, data := range opData {
		switch data.ChangeStateTo {
		case nslcm.OperationalStates("STARTED"):
			startVnfs = append(startVnfs, data.VnfInstanceId)
		case nslcm.OperationalStates("STOPPED"):
			stopVnfs = append(stopVnfs, data.VnfInstanceId)
		default:
			errMsg := "Unsupported ChangeStateTo value"
			problem := nslcm.ProblemDetails{
				Title:  &errMsg,
				Detail: "Provided state: " + string(data.ChangeStateTo),
				Status: http.StatusInternalServerError,
			}
			return &problem
		}
	}
	if len(startVnfs) > 0 {
		if problem := c.StartVnf(startVnfs, nsInfo); problem != nil {
			return problem
		}
		if problem := c.UpdateVNFStatus(startVnfs, nsInfo, true); problem != nil {
			return problem
		}

	}
	if len(stopVnfs) > 0 {
		if problem := c.StopVnf(stopVnfs, nsInfo); problem != nil {
			return problem
		}
		if problem := c.UpdateVNFStatus(stopVnfs, nsInfo, false); problem != nil {
			return problem
		}
	}

	return nil
}

func (c *Client) StartVnf(vnfIds []string, nsInfo common.NsInstanceInfo) *nslcm.ProblemDetails {
	h := common.HttpHandler{}
	h.Client = &http.Client{}
	digs, problem := c.convertVnfIdsToDigs(vnfIds)
	if problem != nil {
		return problem
	}
	backendUrl := "http://" + c.Info.Conf.Orchestrator + "/v2/projects/" +
		common.DEFAULT_PROJECT + "/services/" + nsInfo.NsName + "/instantiate-apps"

	payload := struct {
		Digs []string `json:"digs"`
	}{Digs: digs}
	data, err := json.Marshal(payload)
	if err != nil {
		errMsg := "Issue while packing data"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		log.Errorf("%s: %s :ERR: %v", common.PrintFunctionName(), errMsg, err.Error())
		return &problem
	}
	statusCode, response, err := h.ApiPostRespBody(data, backendUrl)
	if statusCode != http.StatusAccepted || err != nil {
		errMsg := fmt.Sprintf("Issue while connecting to backend: %s", string(response))
		if err == nil {
			message := fmt.Sprintf("URL %s returned status code: %v", backendUrl, statusCode)
			err = errors.New(message)
		}
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		log.Errorf("%s: %s :ERR: %v", common.PrintFunctionName(), errMsg, err.Error())
		return &problem
	}
	return nil

}

func (c *Client) StopVnf(vnfIds []string, nsInfo common.NsInstanceInfo) *nslcm.ProblemDetails {
	h := common.HttpHandler{}
	h.Client = &http.Client{}
	digs, problem := c.convertVnfIdsToDigs(vnfIds)
	if problem != nil {
		return problem
	}
	backendUrl := "http://" + c.Info.Conf.Orchestrator + "/v2/projects/" +
		common.DEFAULT_PROJECT + "/services/" + nsInfo.NsName + "/terminate-apps"

	payload := struct {
		Digs []string `json:"digs"`
	}{Digs: digs}
	data, err := json.Marshal(payload)
	if err != nil {
		errMsg := "Issue while packing data"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		log.Errorf("%s: %s :ERR: %v", common.PrintFunctionName(), errMsg, err.Error())
		return &problem
	}
	statusCode, response, err := h.ApiPostRespBody(data, backendUrl)
	if statusCode != http.StatusAccepted || err != nil {
		errMsg := fmt.Sprintf("Issue while connecting to backend: %s", string(response))
		if err == nil {
			message := fmt.Sprintf("URL %s returned status code: %v", backendUrl, statusCode)
			err = errors.New(message)
		}
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		log.Errorf("%s: %s :ERR: %v", common.PrintFunctionName(), errMsg, err.Error())
		return &problem
	}
	return nil

}

// Update NsInfo
func (c *Client) UpdateNsInfoVNFStatus(nsInfo common.NsInstanceInfo) *nslcm.ProblemDetails {
	vnfList := nsInfo.VnfInstance
	var newVnfList []common.VnfInstance
	for _, vnfinstance := range vnfList {
		_, compositeapp, version, digName := common.FetchDIGDetails(vnfinstance.VnfInstanceId)
		digStatus, pb := common.FetchDigStatus(digName, compositeapp, version, c.Info.Conf.MiddleEnd)
		if pb != nil {
			return pb
		}

		if strings.ToUpper(digStatus.DeployedStatus) != common.NS_LCM_INSTANTIATED_STATE {
			vnfinstance.State = common.NOT_INSTANTIATED
		} else {
			vnfinstance.State = strings.ToUpper(digStatus.DeployedStatus)
		}

		newVnfList = append(newVnfList, vnfinstance)
	}

	nsInfo.VnfInstance = newVnfList

	key := new(common.NsInstanceKey)
	key.NsId = nsInfo.NsId
	err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nsinfo", nsInfo)
	if err != nil {
		errMsg := fmt.Sprintf("Encountered error during insert of NS info for %s: %s", nsInfo.NsName, err.Error())
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
			Status: http.StatusInternalServerError,
		}
		return &problem
	}
	return nil
}

func (c *Client) WaitForVnfState(state string, vnfId string) *nslcm.ProblemDetails {
	_, compositeApp, version, digName := common.FetchDIGDetails(vnfId)

	for {
		digStatus, pb := common.FetchDigStatus(digName, compositeApp, version, c.Info.Conf.MiddleEnd)
		if pb != nil {
			return pb
		}
		select {
		case <-time.After(5 * time.Minute):
			title := fmt.Sprintf("status check timed out for vnf id %s. State of vnf is %s", vnfId, digStatus.DeployedStatus)
			problem := nslcm.ProblemDetails{
				Title:  &title,
				Detail: "This aborts further check of vnf state. Check GUI 'Service Instances' page for actual state of VNF",
			}
			return &problem
		default:
			if strings.ToUpper(digStatus.DeployedStatus) == state {
				return nil
			}
		}
		time.Sleep(15 * time.Second)
	}
}

// Update NsInfo
func (c *Client) UpdateVNFStatus(vnfIdList []string, nsInfo common.NsInstanceInfo, isInstantiate bool) *nslcm.ProblemDetails {
	vnfList := nsInfo.VnfInstance
	vnfStateMap := make(map[string]string)
	if isInstantiate {
		for _, vnfId := range vnfIdList {
			// TODO Wait for actual status. Need to manage multiple instance case.
			vnfStateMap[vnfId] = common.NS_LCM_INSTANTIATED_STATE
		}
	} else {
		for _, vnfId := range vnfIdList {
			//We dont wait for actual state since it might depend on other NS references
			vnfStateMap[vnfId] = common.NOT_INSTANTIATED
		}
	}

	for i := range vnfList {
		if state, ok := vnfStateMap[vnfList[i].VnfInstanceId]; ok && state != vnfList[i].State {
			vnfList[i].State = state
		}
	}

	nsInfo.VnfInstance = vnfList

	key := new(common.NsInstanceKey)
	key.NsId = nsInfo.NsId
	err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nsinfo", nsInfo)
	if err != nil {
		errMsg := fmt.Sprintf("Encountered error during insert of NS info for %s: %s", nsInfo.NsName, err.Error())
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
			Status: http.StatusInternalServerError,
		}
		return &problem
	}
	return nil
}

func (c *Client) moveVnf(vnfData []nslcm.MoveVnfInstanceData, nsInfo common.NsInstanceInfo) *nslcm.ProblemDetails {
	nsId := nsInfo.NsName
	for _, moveData := range vnfData {
		digIds, problem := c.convertVnfIdsToDigs(moveData.VnfInstanceId)
		if problem != nil {
			return problem
		}
		if problem := c.removeVnfHelper(digIds, nsId); problem != nil {
			return problem
		}
		if problem := c.RemoveVnfsFromNSInfo(nsInfo, digIds); problem != nil {
			return problem
		}

		retCode, nserviceInfo, err := common.FetchNSInfo(moveData.TargetNsInstanceId)
		if retCode != http.StatusOK {
			errMsg := "failed to fetch NsInfo from db"
			problem := nslcm.ProblemDetails{
				Title:  &errMsg,
				Detail: err.Error(),
				Status: int32(retCode),
			}
			return &problem
		}

		targetNsInfo, ok := nserviceInfo.(common.NsInstanceInfo)
		if !ok {
			errMsg := "Unable to parse nsInfo structure..."
			problem := nslcm.ProblemDetails{
				Title:  &errMsg,
				Detail: err.Error(),
				Status: http.StatusInternalServerError,
			}
			return &problem
		}
		if problem := c.addVnfHelper(digIds, targetNsInfo.NsName); problem != nil {
			return problem
		}
		vnfMapAll, problem := c.CreateCompAppMap()
		if problem != nil {
			return problem
		}
		// Quickfix: Wait for sometime to all digs to change state. Else we reach status check for digs
		// too early
		time.Sleep(30 * time.Second)

		if problem := c.AddVNFInstancesToNsInfo(targetNsInfo, digIds, vnfMapAll); problem != nil {
			return problem
		}
	}
	return nil
}

func (c *Client) convertVnfIdsToDigs(vnfIds []string) ([]string, *nslcm.ProblemDetails) {
	var (
		compositeApps []CompositeAppsInProjectShrunk
		digsToMove    []string
		project       = common.DEFAULT_PROJECT
	)
	h := common.HttpHandler{}
	h.Client = &http.Client{}
	vnfMap := make(map[string]bool)
	for _, vnfId := range vnfIds {
		vnfMap[vnfId] = true
	}
	URL := "http://" + c.Info.Conf.MiddleEnd + "/middleend/projects/" +
		common.DEFAULT_PROJECT + "/composite-apps"
	statusCode, resp, err := h.ApiGet(URL)
	if statusCode != http.StatusOK || err != nil {
		errMsg := "Issue while connecting to backend"
		if err == nil {
			message := fmt.Sprintf("URL %s returned status code: %v", URL, statusCode)
			err = errors.New(message)
		}
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		log.Errorf("%s: %s :ERR: %v", common.PrintFunctionName(), errMsg, err.Error())
		return []string{}, &problem
	}
	err = json.Unmarshal(resp, &compositeApps)
	if err != nil {
		errMsg := "Issue while decoding backend response"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		log.Errorf("%s: %s : %s", common.PrintFunctionName(), errMsg, err.Error())
		return []string{}, &problem
	}

	for _, compApp := range compositeApps {
		for _, spec := range compApp.Spec {
			var digs []DeploymentIntentGroup
			digUrl := "http://" + c.Info.Conf.MiddleEnd + "/middleend/projects/" +
				common.DEFAULT_PROJECT + "/composite-apps/" + compApp.Metadata.Name + "/" + spec.Version + "/deployment-intent-groups"
			statusCode, response, err := h.ApiGet(digUrl)
			if statusCode != http.StatusOK || err != nil {
				errMsg := "Issue while connecting to backend"
				if err == nil {
					message := fmt.Sprintf("URL %s returned status code: %v", URL, statusCode)
					err = errors.New(message)
				}
				problem := nslcm.ProblemDetails{
					Title:  &errMsg,
					Detail: err.Error(),
					Status: http.StatusInternalServerError,
				}
				log.Errorf("%s: %s :ERR: %v", common.PrintFunctionName(), errMsg, err.Error())
				return []string{}, &problem
			}

			err = json.Unmarshal(response, &digs)
			if err != nil {
				errMsg := "Issue while decoding backend response"
				problem := nslcm.ProblemDetails{
					Title:  &errMsg,
					Detail: err.Error(),
				}
				log.Errorf("%s: %s : %s", common.PrintFunctionName(), errMsg, err.Error())
				return []string{}, &problem
			}
			for _, dig := range digs {
				if vnfMap[dig.Spec.Id] {
					d := project + "." + compApp.Metadata.Name + "." + spec.Version + "." + dig.MetaData.Name
					digsToMove = append(digsToMove, d)
				}
			}
		}
	}
	return digsToMove, nil
}

func (c *Client) doScaleNs(nsInfo common.NsInstanceInfo, request nslcm.ScaleNsRequest, operation common.NsLCMOpOcc) *nslcm.ProblemDetails {
	if request.NsData.ScaleNsByStepsData != nil {
		title := "AMCOP does not support ScaleNsByStepsData"
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Status: http.StatusConflict,
		}
		return &problem
	}
	if request.NsData.VnfInstanceToBeAdded != nil || request.NsData.VnfInstanceToBeRemoved != nil {
		if request.NsData.ScaleNsToLevelData != nil {
			title := "ScaleNsToLevelData or cannot be provided along with VnfInstanceToBeAdded/VnfInstanceToBeRemoved"
			problem := nslcm.ProblemDetails{
				Title:  &title,
				Status: http.StatusConflict,
			}
			return &problem
		}
		go func() {
			affectedVnfMapRemove, _ := c.GetAffectedVnfDetails(true, nsInfo.NsId)
			problem := c.addRemoveVnfs(nsInfo, request)
			affectedVnfMapAdd, _ := c.GetAffectedVnfDetails(true, nsInfo.NsId)
			if problem != nil {
				log.Errorf("Scale operation Failed: %+v", problem)

				var affectedVnfs []nslcmnotification.AffectedVnf
				for _, vnf := range request.NsData.VnfInstanceToBeAdded {
					a := nslcmnotification.AffectedVnf{
						VnfInstanceId: vnf.VnfInstanceId,
					}
					if affectedVnfMapAdd != nil {
						if t, ok := affectedVnfMapAdd[vnf.VnfInstanceId]; ok {
							a = t
						}
					}
					a.ChangeType = "ADD"
					a.ChangeResult = "FAILED"
					affectedVnfs = append(affectedVnfs, a)
				}
				for _, vnf := range request.NsData.VnfInstanceToBeRemoved {
					a := nslcmnotification.AffectedVnf{
						VnfInstanceId: vnf,
					}
					if affectedVnfMapRemove != nil {
						if t, ok := affectedVnfMapRemove[vnf]; ok {
							a = t
						}
					}
					a.ChangeType = "REMOVE"
					a.ChangeResult = "FAILED"
					affectedVnfs = append(affectedVnfs, a)
				}
				c.LocalNotificationHandler.NsScaleFailedTemp(nsInfo.NsId, operation.NsLCMOpOccId, *problem, affectedVnfs)
				operation.AffectedVnfs = affectedVnfs
				operation.OperationState = common.LCMOPP_OCC_FAILED_TEMP
				var pb common.ProblemDetails
				pb.Detail = problem.Detail
				pb.Title = problem.Title
				pb.Status = problem.Status
				operation.Error = &pb
				key := new(common.NsLCMOppOccKey)
				key.NsLCMOppOccId = operation.NsLCMOpOccId
				err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", operation)
				if err != nil {
					log.Errorf("Error while recording Scale operation failure %v", err.Error())
				}
				return
			}
			operation.OperationState = common.LCMOPP_OCC_COMPLETED
			key := new(common.NsLCMOppOccKey)
			key.NsLCMOppOccId = operation.NsLCMOpOccId
			err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", operation)
			if err != nil {
				log.Errorf("Error while recording Scale operation failure %v", err.Error())
			}
			var affectedVnfs []nslcmnotification.AffectedVnf
			for _, vnf := range request.NsData.VnfInstanceToBeAdded {
				a := nslcmnotification.AffectedVnf{
					VnfInstanceId: vnf.VnfInstanceId,
				}
				if affectedVnfMapAdd != nil {
					if t, ok := affectedVnfMapAdd[vnf.VnfInstanceId]; ok {
						a = t
					}
				}
				a.ChangeType = "ADD"
				a.ChangeResult = "COMPLETED"
				affectedVnfs = append(affectedVnfs, a)
			}
			for _, vnf := range request.NsData.VnfInstanceToBeRemoved {
				a := nslcmnotification.AffectedVnf{
					VnfInstanceId: vnf,
				}
				if affectedVnfMapRemove != nil {
					if t, ok := affectedVnfMapRemove[vnf]; ok {
						a = t
					}
				}
				a.ChangeType = "REMOVE"
				a.ChangeResult = "COMPLETED"
				affectedVnfs = append(affectedVnfs, a)
			}
			operation.AffectedVnfs = affectedVnfs
			operation.OperationState = common.LCMOPP_OCC_COMPLETED
			key = new(common.NsLCMOppOccKey)
			key.NsLCMOppOccId = operation.NsLCMOpOccId
			err = db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", operation)
			if err != nil {
				log.Errorf("Error while recording Scale operation failure %v", err.Error())
			}
			c.LocalNotificationHandler.NsScaleCompleted(nsInfo.NsId, operation.NsLCMOpOccId, affectedVnfs)
		}()

		c.LocalNotificationHandler.NsScaleStarted(nsInfo.NsId, operation.NsLCMOpOccId)
		return nil
	}
	if request.NsData.ScaleNsToLevelData != nil {
		go func() {
			problem := c.ScaleNsToLevel(request.NsData.ScaleNsToLevelData.VnfInstantiationLevelId, nsInfo)
			if problem != nil {
				log.Errorf("Scale operation Failed: %+v", problem)
				c.LocalNotificationHandler.NsScaleFailedTemp(nsInfo.NsId, operation.NsLCMOpOccId, *problem, nil)
				operation.OperationState = common.LCMOPP_OCC_FAILED_TEMP
				var pb common.ProblemDetails
				pb.Detail = problem.Detail
				pb.Title = problem.Title
				pb.Status = problem.Status
				operation.Error = &pb
				key := new(common.NsLCMOppOccKey)
				key.NsLCMOppOccId = operation.NsLCMOpOccId
				err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", operation)
				if err != nil {
					log.Errorf("Error while recording Scale operation failure %v", err.Error())
				}
				return
			}
			operation.OperationState = common.LCMOPP_OCC_COMPLETED
			key := new(common.NsLCMOppOccKey)
			key.NsLCMOppOccId = operation.NsLCMOpOccId
			err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", operation)
			if err != nil {
				log.Errorf("Error while recording Scale operation failure %v", err.Error())
			}
			c.LocalNotificationHandler.NsScaleCompleted(nsInfo.NsId, operation.NsLCMOpOccId, nil)
		}()
		c.LocalNotificationHandler.NsScaleStarted(nsInfo.NsId, operation.NsLCMOpOccId)
		return nil
	}

	title := "Need to provide VnfInstanceToBeAdded or VnfInstanceToBeRemoved or ScaleNsToLevelData"
	problem := nslcm.ProblemDetails{
		Title:  &title,
		Status: http.StatusConflict,
	}
	return &problem

}

func (c *Client) addRemoveVnfs(nsInfo common.NsInstanceInfo, request nslcm.ScaleNsRequest) *nslcm.ProblemDetails {
	if request.NsData.VnfInstanceToBeAdded != nil {
		var vnfIds []string
		for _, vnfData := range request.NsData.VnfInstanceToBeAdded {
			vnfIds = append(vnfIds, vnfData.VnfInstanceId)
		}
		digs, problem := c.convertVnfIdsToDigs(vnfIds)
		if problem != nil {
			return problem
		}
		nsId := nsInfo.NsName
		problem = c.addVnfHelper(digs, nsId)
		if problem != nil {
			return problem
		}
		vnfMapAll, problem := c.CreateCompAppMap()
		if problem != nil {
			return problem
		}
		// Quickfix: Wait for sometime to all digs to change state. Else we reach status check for digs
		// too early
		time.Sleep(30 * time.Second)
		if problem := c.AddVNFInstancesToNsInfo(nsInfo, digs, vnfMapAll); problem != nil {
			return problem
		}
	}
	//Update NSInfo. NSInfo might get modified in above operation
	statusCode, serviceInfo, err := common.FetchNSInfo(nsInfo.NsId)
	if err != nil {
		title := "Generic error during scale"
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: err.Error(),
		}
		if statusCode != http.StatusNotFound {
			statusCode = http.StatusInternalServerError
		}
		return &problem
	}
	nsInfo, ok := serviceInfo.(common.NsInstanceInfo)

	if !ok {
		title := "Generic error during scale"
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: "Parsing nsInfo interface failed",
		}
		return &problem
	}
	if request.NsData.VnfInstanceToBeRemoved != nil {
		vnfIds := request.NsData.VnfInstanceToBeRemoved
		digs, problem := c.convertVnfIdsToDigs(vnfIds)
		if problem != nil {
			return problem
		}
		nsId := nsInfo.NsName
		problem = c.removeVnfHelper(digs, nsId)
		if problem != nil {
			return problem
		}
		return c.RemoveVnfsFromNSInfo(nsInfo, digs)
	}
	return nil
}

func (c *Client) ScaleNsToLevel(levelId string, nsInfo common.NsInstanceInfo) *nslcm.ProblemDetails {
	h := common.HttpHandler{}
	h.Client = &http.Client{}
	spec, problem := c.GetNsScaleSpec(levelId)
	if problem != nil {
		return problem
	}
	digs, problem := c.ScaleVnfsByClone(spec.VnfCounts, nsInfo.NsName)
	if problem != nil {
		return problem
	}

	var vnfData []nslcm.VnfInstanceData
	for _, dig := range digs {
		n := nslcm.VnfInstanceData{VnfInstanceId: dig.Spec.Id}
		vnfData = append(vnfData, n)
	}
	scaleStatus := nslcm.NsScaleInfo{NsScaleLevelId: levelId}
	if nsInfo.NsScaleStatus == nil {
		nsInfo.NsScaleStatus = []nslcm.NsScaleInfo{}
	}
	nsInfo.NsScaleStatus = append(nsInfo.NsScaleStatus, scaleStatus)
	key := new(common.NsInstanceKey)
	key.NsId = nsInfo.NsId
	err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nsinfo", nsInfo)
	if err != nil {
		errMsg := fmt.Sprintf("Encountered error during insert of NS info for %s: %s", nsInfo.NsName, err.Error())
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
			Status: http.StatusInternalServerError,
		}
		return &problem
	}
	if problem := c.addVnfToNs(vnfData, nsInfo); problem != nil {
		return problem
	}

	return nil
}

func (c *Client) ScaleVnfsByClone(vnfLevels map[string]int, nsName string) ([]DeploymentIntentGroup, *nslcm.ProblemDetails) {
	var vnfList []string
	var digsAll []DeploymentIntentGroup
	for vnf := range vnfLevels {
		vnfList = append(vnfList, vnf)
	}
	t := strconv.Itoa(int(time.Now().Unix()))
	digMap, problem := c.GetVnfToDigMap(vnfList)
	if problem != nil {
		return digsAll, problem
	}
	for vnf, scaleCount := range vnfLevels {
		if _, ok := digMap[vnf]; !ok {
			title := "DIG not found for vnfid: " + vnf
			problem := nslcm.ProblemDetails{
				Title:  &title,
				Status: http.StatusInternalServerError,
				Detail: "Make sure vnfids in scale spec are valid",
			}
			return digsAll, &problem
		}
		digs, problem := c.CloneVnf(digMap[vnf], nsName+"-"+t, scaleCount, 1)
		if problem != nil {
			return digs, problem
		}
		digsAll = append(digsAll, digs...)
	}
	return digsAll, nil
}

func (c *Client) CloneVnf(digId string, prefix string, count int, startNumber int) ([]DeploymentIntentGroup, *nslcm.ProblemDetails) {
	h := common.HttpHandler{}
	h.Client = &http.Client{}
	var digs []DeploymentIntentGroup
	cloneData := struct {
		CloneDigNamePrefix string `json:"cloneDigNamePrefix"`
		NumberOfClones     int    `json:"numberOfClones"`
		StartNumber        int    `json:"startNumber"`
	}{
		CloneDigNamePrefix: prefix,
		NumberOfClones:     count,
		StartNumber:        startNumber,
	}
	cloneJson, err := json.Marshal(cloneData)
	if err != nil {
		title := "Error while encoding clone data"
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Status: http.StatusInternalServerError,
			Detail: err.Error(),
		}
		return digs, &problem
	}
	digSplit := strings.Split(digId, ".")
	if len(digSplit) < 4 {
		log.Errorf("Error in dig id during scale: digid %s, digsplit %v", digId, digSplit)
		title := "Error in dig id during scale: digid:" + digId
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Status: http.StatusInternalServerError,
			Detail: "Internal error",
		}
		return digs, &problem
	}

	cloneUrl := "http://" + c.Info.Conf.Orchestrator + "/v2/projects/" + digSplit[0] +
		"/composite-apps/" + digSplit[1] + "/" + digSplit[2] +
		"/deployment-intent-groups/" + digSplit[3] + "/clone"
	status, response, err := h.ApiPostRespBody(cloneJson, cloneUrl)
	if err != nil {
		title := "Error on backend request to clone DIG"
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Status: http.StatusInternalServerError,
			Detail: err.Error(),
		}
		return digs, &problem
	}
	if status != http.StatusOK {
		title := fmt.Sprintf("Error on backend request to clone DIG: %s", string(response))
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Status: http.StatusInternalServerError,
			Detail: "Request status: " + strconv.Itoa(status),
		}
		return digs, &problem
	}
	err = json.Unmarshal(response, &digs)
	if err != nil {
		title := "Error while decoding backend result"
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Status: http.StatusInternalServerError,
			Detail: err.Error(),
		}
		return digs, &problem
	}
	return digs, nil
}

func (c *Client) GetVnfToDigMap(vnfIds []string) (map[string]string, *nslcm.ProblemDetails) {
	var (
		compositeApps []CompositeAppsInProjectShrunk
		project       = common.DEFAULT_PROJECT
	)
	h := common.HttpHandler{}
	h.Client = &http.Client{}
	vnfMap := make(map[string]string)
	for _, vnfId := range vnfIds {
		vnfMap[vnfId] = ""
	}
	URL := "http://" + c.Info.Conf.MiddleEnd + "/middleend/projects/" +
		common.DEFAULT_PROJECT + "/composite-apps"
	statusCode, resp, err := h.ApiGet(URL)
	if statusCode != http.StatusOK || err != nil {
		errMsg := "Issue while connecting to backend"
		if err == nil {
			message := fmt.Sprintf("URL %s returned status code: %v", URL, statusCode)
			err = errors.New(message)
		}
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		log.Errorf("%s: %s :ERR: %v", common.PrintFunctionName(), errMsg, err.Error())
		return vnfMap, &problem
	}
	err = json.Unmarshal(resp, &compositeApps)
	if err != nil {
		errMsg := "Issue while decoding backend response"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		log.Errorf("%s: %s : %s", common.PrintFunctionName(), errMsg, err.Error())
		return vnfMap, &problem
	}

	for _, compApp := range compositeApps {
		for _, spec := range compApp.Spec {
			var digs []DeploymentIntentGroup
			digUrl := "http://" + c.Info.Conf.MiddleEnd + "/middleend/projects/" +
				common.DEFAULT_PROJECT + "/composite-apps/" + compApp.Metadata.Name + "/" + spec.Version + "/deployment-intent-groups"
			statusCode, response, err := h.ApiGet(digUrl)
			if statusCode != http.StatusOK || err != nil {
				errMsg := "Issue while connecting to backend"
				if err == nil {
					message := fmt.Sprintf("URL %s returned status code: %v", URL, statusCode)
					err = errors.New(message)
				}
				problem := nslcm.ProblemDetails{
					Title:  &errMsg,
					Detail: err.Error(),
					Status: http.StatusInternalServerError,
				}
				log.Errorf("%s: %s :ERR: %v", common.PrintFunctionName(), errMsg, err.Error())
				return vnfMap, &problem
			}

			err = json.Unmarshal(response, &digs)
			if err != nil {
				errMsg := "Issue while decoding backend response"
				problem := nslcm.ProblemDetails{
					Title:  &errMsg,
					Detail: err.Error(),
				}
				log.Errorf("%s: %s : %s", common.PrintFunctionName(), errMsg, err.Error())
				return vnfMap, &problem
			}
			for _, dig := range digs {
				if _, ok := vnfMap[dig.Spec.Id]; ok {
					d := project + "." + compApp.Metadata.Name + "." + spec.Version + "." + dig.MetaData.Name
					vnfMap[dig.Spec.Id] = d
				}
			}
		}
	}
	return vnfMap, nil
}

func (c *Client) CreateLCMOppStructure(nsInfo common.NsInstanceInfo, opType string, r *http.Request) (common.NsLCMOpOcc, *nslcm.ProblemDetails) {
	key := new(common.NsLCMOppOccKey)
	id := uuid2.New()
	key.NsLCMOppOccId = strings.Replace(id.String(), "-", "", -1)
	var nsLCMOpp common.NsLCMOpOcc
	nsLCMOpp.NsLCMOpOccId = key.NsLCMOppOccId
	nsLCMOpp.LcmOperationType = opType
	nsLCMOpp.NsInstanceId = nsInfo.NsId
	nsLCMOpp.OperationState = common.LCMOPP_OCC_PROCESSING
	nsLCMOpp.StartTime = time.Now().Format(time.RFC3339)
	nsLCMOpp.StatusEnteredTime = time.Now().Format(time.RFC3339)
	nsLCMOpp.IsAutomaticInvocation = false
	nsLCMOpp.IsCancelPending = false

	// Set the location header, as request is successful
	parsedUrl, err := url.Parse(r.URL.String())
	if err != nil {
		errMsg := fmt.Sprintf("%s : Encountered error while parsing url: %s", common.PrintFunctionName(), r.URL.String())
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return nsLCMOpp, &problem
	}
	split := strings.Split(parsedUrl.Path, "/")

	nsLCMOppOccURL := "http://" + r.Host + strings.Join(split[:len(split)-3], "/") + "/ns_lcm_op_occs/" + nsLCMOpp.NsLCMOpOccId

	var selfLink common.Link
	selfLink.Href = nsLCMOppOccURL
	nsLCMOpp.Links.Self = &selfLink

	err = db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", nsLCMOpp)
	if err != nil {
		errMsg := fmt.Sprintf("Encountered error during insert of nsLCMOppOcc for %s: %s", nsInfo.NsName, err)
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return nsLCMOpp, &problem
	}
	return nsLCMOpp, nil
}

func (c *Client) RemoveVnfsFromNSInfo(nsInfo common.NsInstanceInfo, vnfInstances []string) *nslcm.ProblemDetails {
	var (
		vnfInstList = nsInfo.VnfInstance
		errMsg      string
	)
	digMap := make(map[string]bool)
	for _, vnfInstance := range vnfInstances {
		digMap[vnfInstance] = true
	}
	var vnfInstListNew []common.VnfInstance
	for _, v := range vnfInstList {
		if !digMap[v.VnfInstanceName] {
			vnfInstListNew = append(vnfInstListNew, v)
		}
	}

	nsInfo.VnfInstance = vnfInstListNew
	key := new(common.NsInstanceKey)
	key.NsId = nsInfo.NsId
	err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nsinfo", nsInfo)
	if err != nil {
		errMsg = fmt.Sprintf("Encountered error during insert of NS info for %s: %s", nsInfo.NsName, err.Error())
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
			Status: http.StatusInternalServerError,
		}
		return &problem
	}
	return nil
}

func (c *Client) GetAffectedVnfDetails(isVnfId bool, nsId string) (map[string]nslcmnotification.AffectedVnf, *nslcm.ProblemDetails) {

	var (
		nsInfo common.NsInstanceInfo
	)
	statusCode, serviceInfo, err := common.FetchNSInfo(nsId)
	if err != nil {
		title := "Generic error during update"
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: err.Error(),
			Status: int32(statusCode),
		}
		return nil, &problem
	}
	nsInfo, ok := serviceInfo.(common.NsInstanceInfo)

	if !ok {
		title := "Generic error during update"
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: "Parsing nsInfo interface failed",
			Status: http.StatusInternalServerError,
		}
		return nil, &problem
	}
	if nsInfo.NsState != common.NS_LCM_INSTANTIATED_STATE {
		title := "NS is not in INSTANTIATED state"
		problem := nslcm.ProblemDetails{
			Title:  &title,
			Detail: "NS is not in INSTANTIATED state",
			Status: http.StatusConflict,
		}
		return nil, &problem
	}
	vnfMap := make(map[string]nslcmnotification.AffectedVnf)
	if isVnfId {
		for _, vnf := range nsInfo.VnfInstance {
			vnfMap[vnf.VnfInstanceId] = nslcmnotification.AffectedVnf{
				VnfInstanceId: vnf.VnfInstanceId,
				VnfdId:        vnf.VnfdId,
				VnfName:       vnf.VnfInstanceName,
			}
		}
		return vnfMap, nil
	}
	for _, vnf := range nsInfo.VnfInstance {
		vnfMap[vnf.VnfdId] = nslcmnotification.AffectedVnf{
			VnfInstanceId: vnf.VnfInstanceId,
			VnfdId:        vnf.VnfdId,
			VnfName:       vnf.VnfInstanceName,
		}
	}
	return vnfMap, nil

}
