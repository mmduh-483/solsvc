package lcmops

import (
	"aarna.com/solsvc/db"
	"aarna.com/solsvc/lib/common"
	nslcm "aarna.com/solsvc/lib/nslcm"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"net/http"
	"net/url"
	"os/exec"
	"strings"
	"time"
)

// Fetch DIGs part of NS
func (c *Client) FetchNSDetails(nsInfo common.NsInstanceInfo) ([]string, *nslcm.ProblemDetails) {
	var (
		digs   []string
		errMsg string
	)

	// Fetch DIGs part of NS
	h := common.HttpHandler{}
	h.Client = &http.Client{}

	var serviceinfo Service
	url := "http://" + c.Info.Conf.Orchestrator + "/v2/projects/" +
		common.DEFAULT_PROJECT + "/services/" + nsInfo.NsName
	log.Debugf("url: %s", url)
	status, resp, err := h.ApiGet(url)
	if err != nil {
		errMsg = fmt.Sprintf("Encountered error while fetching details of NS service: %s", err.Error())
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
			Status: http.StatusInternalServerError,
		}
		return digs, &problem
	}
	if status != http.StatusOK {
		errMsg = fmt.Sprintf("Encountered error while fetching details of NS service %s", nsInfo.NsName)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
			Status: int32(status.(int)),
		}
		return digs, &problem
	}

	err = json.Unmarshal(resp, &serviceinfo)
	if err != nil {
		errMsg = "Unable to unmarshal response..."
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return digs, &problem
	}

	digs = serviceinfo.Spec.Digs
	return digs, nil
}

// Check DIG reference counts, and if reference count is zero, delete DIG
func (c *Client) DeleteNSDigs(nsInfo common.NsInstanceInfo, digs []string) *nslcm.ProblemDetails {
	var (
		digResponse DeploymentIntentGroup
		errMsg      string
	)

	h := common.HttpHandler{}
	h.Client = &http.Client{}

	// Iterate each DIG and check reference count, if reference count is zero, we can delete DIG
	for _, dig := range digs {
		_, compositeapp, version, digName := common.FetchDIGDetails(dig)
		log.Debugf("parsed dig name: %s, compositeapp: %s, version: %s", digName, compositeapp, version)
		if digName == "" {
			errMsg = fmt.Sprintf("Encountered error while parsing digId")
			problem := nslcm.ProblemDetails{
				Title:  &errMsg,
				Detail: errMsg,
				Status: http.StatusInternalServerError,
			}
			return &problem
		}

		// Fetch DIG call of emco service
		URL := "http://" + c.Info.Conf.Orchestrator + "/v2/projects/" + common.DEFAULT_PROJECT +
			"/composite-apps/" + compositeapp + "/" + version +
			"/deployment-intent-groups/" + digName
		status, resp, err := h.ApiGet(URL)
		if err != nil {
			errMsg = fmt.Sprintf("Encountered error while fetching DIG for NS service: %s", err.Error())
			log.Errorf(errMsg)
			problem := nslcm.ProblemDetails{
				Title:  &errMsg,
				Detail: err.Error(),
				Status: http.StatusInternalServerError,
			}
			return &problem
		}
		if status != http.StatusOK {
			errMsg = fmt.Sprintf("Encountered error while fetching DIG %s for NS service", digName)
			problem := nslcm.ProblemDetails{
				Title:  &errMsg,
				Detail: errMsg,
				Status: int32(status.(int)),
			}
			return &problem
		}

		err = json.Unmarshal(resp, &digResponse)
		log.Debugf("digResponse: %+v", digResponse)
		if err != nil {
			errMsg := fmt.Sprintf("Unmarshalling dig response failed: %s", err)
			problem := nslcm.ProblemDetails{
				Title:  &errMsg,
				Detail: err.Error(),
				Status: http.StatusInternalServerError,
			}
			return &problem
		}

		if len(digResponse.Spec.Services) == 0 {
			// Invoke Delete DIG call of middleend service
			URL := "http://" + c.Info.Conf.MiddleEnd + "/middleend/projects/" + common.DEFAULT_PROJECT +
				"/composite-apps/" + compositeapp + "/" + version +
				"/deployment-intent-groups/" + digName
			status, _, err := h.ApiDel(URL)
			if err != nil {
				errMsg = fmt.Sprintf("Encountered error while deleting DIG for NS service: %s", err.Error())
				log.Errorf(errMsg)
				problem := nslcm.ProblemDetails{
					Title:  &errMsg,
					Detail: err.Error(),
					Status: http.StatusInternalServerError,
				}
				return &problem
			}
			if status != http.StatusNoContent {
				errMsg = fmt.Sprintf("Encountered error while deleting DIG %s for NS service", digName)
				problem := nslcm.ProblemDetails{
					Title:  &errMsg,
					Detail: errMsg,
					Status: int32(status.(int)),
				}
				return &problem
			}
		}
	}
	return nil
}

// Delete NS service
func (c *Client) DeleteNSService(nsInfo common.NsInstanceInfo) *nslcm.ProblemDetails {
	var (
		errMsg string
	)

	h := common.HttpHandler{}
	h.Client = &http.Client{}

	URL := "http://" + c.Info.Conf.Orchestrator + "/v2/projects/" +
		common.DEFAULT_PROJECT + "/services/" + nsInfo.NsName
	status, response, err := h.ApiDel(URL)
	if err != nil {
		errMsg = fmt.Sprintf("Encountered error while deleting NS service: %s", err.Error())
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
			Status: http.StatusInternalServerError,
		}
		return &problem
	}
	if status != http.StatusNoContent {
		errMsg = fmt.Sprintf("Encountered error while deleting NS service %s: %s", nsInfo.NsName, string(response))
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
			Status: int32(status.(int)),
		}
		return &problem
	}
	return nil
}

// Deletes Network Service
func (c *Client) DeleteNSInfo(w http.ResponseWriter, r *http.Request) {
	var (
		errMsg string
	)

	h := common.HttpHandler{}
	h.Client = &http.Client{}
	vars := mux.Vars(r)

	// Fetch NsInfo from database
	retCode, nserviceInfo, err := common.FetchNSInfo(vars["nsInstanceId"])
	if retCode != http.StatusOK {
		errMsg = "encountered error while fetching NSInfo"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, retCode, w, r)
		return
	}

	nsInfo, ok := nserviceInfo.(common.NsInstanceInfo)
	if !ok {
		errMsg = "Unable to parse nsInfo structure..."
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}

	log.Debugf("nsinfo: %+v", nsInfo)

	// Check if NS is instantiated, throw error
	if nsInfo.NsState == common.NS_LCM_INSTANTIATED_STATE {
		errMsg = fmt.Sprintf("Cannot delete NS %s in instantiated state", nsInfo.NsId)
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}

	// Fetch digs part of NS
	digs, pb := c.FetchNSDetails(nsInfo)
	log.Debugf("digs part of NS: %+v", digs)
	if pb != nil {
		common.WriteError(*pb, int(pb.Status), w, r)
		return
	}

	// Call NS delete call of emco service
	pb = c.DeleteNSService(nsInfo)
	if pb != nil {
		common.WriteError(*pb, int(pb.Status), w, r)
		return
	}

	// Check reference count of DIG, if zero delete DIG
	pb = c.DeleteNSDigs(nsInfo, digs)
	if pb != nil {
		common.WriteError(*pb, int(pb.Status), w, r)
		return
	}

	// Remove NS identifier, if all digs part of NS are deleted
	var key common.NsInstanceKey
	key.NsId = nsInfo.NsId
	err = db.DBconn.Remove(common.NS_COLLECTION, key)
	if err != nil {
		errMsg = fmt.Sprintf("Encountered error during removal of NS identifier %s : %s", nsInfo.NsId, err)
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}
	c.LocalNotificationHandler.NsDeleteDone(nsInfo.NsId)
	w.Header().Set("Content-Type", "application/json")
	// If success, return the last success status
	w.WriteHeader(http.StatusNoContent)
}

// Fetches Network Service LCM Operation Occurrence
func (c *Client) GetLCMOppOCCStatus(w http.ResponseWriter, r *http.Request) {
	var (
		errMsg string
	)
	vars := mux.Vars(r)
	// Fetch NSLCMOpOccInfo from database
	retCode, nsLCMInfo, err := common.FetchNSLCMOpOccInfo(vars["nsLCMOppOccId"])
	if retCode != http.StatusOK {
		errMsg = "encountered error while fetching nsLCMOppOccId"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}

	log.Infof("nsLCMOppOccInfo: %+v", nsLCMInfo)

	response, err := json.Marshal(nsLCMInfo)
	if err != nil {
		errMsg = "Encountered error while marshalling nsLCMInfo"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

// Fetch Network Service
func (c *Client) GetNSInfo(w http.ResponseWriter, r *http.Request) {
	var (
		errMsg string
	)
	vars := mux.Vars(r)
	// Fetch NsInfo from database
	retCode, nserviceInfo, err := common.FetchNSInfo(vars["nsInstanceId"])
	if retCode != http.StatusOK {
		errMsg = "encountered error while fetching NSInfo"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, retCode, w, r)
		return
	}

	nsInfo, ok := nserviceInfo.(common.NsInstanceInfo)
	if !ok {
		errMsg = "Unable to parse nsInfo structure..."
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}
	if nsInfo.NsScaleStatus == nil {
		nsInfo.NsScaleStatus = make([]nslcm.NsScaleInfo, 0)
	}
	log.Infof("nsInstance: %+v", nsInfo)
	response, err := json.Marshal(nsInfo)
	if err != nil {
		errMsg = "Encountered error while marshalling nsInstance"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

// Fetch all existing NS instances
func (c *Client) GetNSInstances(w http.ResponseWriter, r *http.Request) {
	values, skip, limit, maxLimit := common.QueryLCMObjects(w, r, "nsinfo")

	if skip > maxLimit {
		errMsg := "Invalid nextpage_opaque_marker"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
		}
		common.WriteError(problem, http.StatusBadRequest, w, r)
		return
	}

	// UnMarshal the fetched nsInfo data
	nsInstanceList := make([]common.NsInstanceInfo, 0)
	for _, val := range values {
		nsInfo := common.NsInstanceInfo{}
		err := db.DBconn.Unmarshal(val, &nsInfo)
		log.Debugf("NsInfo after Unmarshalling: %v", nsInfo)
		if err != nil {
			errMsg := fmt.Sprintf("Unmarshalling NsInfo failed: %s", err)
			problem := nslcm.ProblemDetails{
				Title:  &errMsg,
				Detail: err.Error(),
			}
			common.WriteError(problem, http.StatusInternalServerError, w, r)
			return
		}
		if nsInfo.NsScaleStatus == nil {
			nsInfo.NsScaleStatus = make([]nslcm.NsScaleInfo, 0)
		}
		nsInstanceList = append(nsInstanceList, nsInfo)
	}
	log.Infof("nsInstanceList: %+v", nsInstanceList)
	result := nsInstanceList[skip:limit]
	retval, err := json.Marshal(result)
	if err != nil {
		errMsg := fmt.Sprintf("Marshalling NsInstanceList failed: %s", err)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(retval)
}

// Fetch all existing NsLcmOpOcc objects
func (c *Client) GetNsLcmOpOccInstances(w http.ResponseWriter, r *http.Request) {
	values, skip, limit, maxLimit := common.QueryLCMObjects(w, r, "nslcmoppocc")

	if skip > maxLimit {
		errMsg := "Invalid nextpage_opaque_marker"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
		}
		common.WriteError(problem, http.StatusBadRequest, w, r)
		return
	}

	// UnMarshal the fetched nsInfo data
	nsLcmOppOccList := make([]common.NsLCMOpOcc, 0)
	for _, val := range values {
		nsLcmOppOccInfo := common.NsLCMOpOcc{}
		err := db.DBconn.Unmarshal(val, &nsLcmOppOccInfo)
		log.Debugf("NsLCMOpOcc after Unmarshalling: %v", nsLcmOppOccInfo)
		if err != nil {
			errMsg := fmt.Sprintf("Unmarshalling NsLCMOpOcc failed: %s", err)
			problem := nslcm.ProblemDetails{
				Title:  &errMsg,
				Detail: err.Error(),
			}
			common.WriteError(problem, http.StatusInternalServerError, w, r)
			return
		}
		nsLcmOppOccList = append(nsLcmOppOccList, nsLcmOppOccInfo)
	}
	log.Infof("nsLcmOppOccList: %+v", nsLcmOppOccList)
	result := nsLcmOppOccList[skip:limit]
	retval, err := json.Marshal(result)
	if err != nil {
		errMsg := fmt.Sprintf("Marshalling nsLcmOppOccList failed: %s", err)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(retval)
}

// Create NSD identifier
func (c *Client) CreateNSD(w http.ResponseWriter, r *http.Request) {
	var (
		nsdCreateInfo CreateNsdRequest
		errMsg        string
	)

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&nsdCreateInfo)
	if err != nil {
		errMsg = fmt.Sprintf("failed to parse json: %s", err)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusBadRequest, w, r)
		errMsg = fmt.Sprintf("%s: %s", common.PrintFunctionName(), errMsg)
		log.Error(errMsg)
		return
	}
	log.Debugf("nsdCreateInfo: %+v", nsdCreateInfo)

	// Validate if input VNFDIDs are valid UUID
	for _, vnfdid := range nsdCreateInfo.Spec.VnfdIds {
		if !common.IsValidUUID(vnfdid) {
			errMsg = fmt.Sprintf("Invalid VNFDId: %s", vnfdid)
			problem := nslcm.ProblemDetails{
				Title:  &errMsg,
				Detail: errMsg,
			}
			common.WriteError(problem, http.StatusBadRequest, w, r)
			errMsg = fmt.Sprintf("%s: %s", common.PrintFunctionName(), errMsg)
			log.Error(errMsg)
			return
		}
	}

	// Fetch all NSDs, and check if NSD with same name exists
	nsdInfoList, pb := c.FetchAllNSD()
	if pb != nil && pb.Status != http.StatusOK {
		common.WriteError(*pb, int(pb.Status), w, r)
		return
	}

	for _, nsd := range nsdInfoList {
		if nsd.Metadata.Name == nsdCreateInfo.Metadata.Name {
			errMsg = fmt.Sprintf("Nsd %s already exists", nsdCreateInfo.Metadata.Name)
			problem := nslcm.ProblemDetails{
				Title:  &errMsg,
				Detail: errMsg,
			}
			common.WriteError(problem, http.StatusConflict, w, r)
			return
		}
	}

	// Insert Nsd Info in database
	var nsdInfo common.NsdInfo
	key := new(common.NsdInfoKey)
	key.Id = uuid.New().String()
	nsdInfo.Metadata = nsdCreateInfo.Metadata
	nsdInfo.Spec.VnfdIds = nsdCreateInfo.Spec.VnfdIds
	nsdInfo.Spec.Id = key.Id

	err = db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nsdinfo", nsdInfo)
	if err != nil {
		errMsg = fmt.Sprintf("Encountered error during insert of Nsd info for %s: %s", nsdInfo.Metadata.Name, err)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		errMsg = fmt.Sprintf("%s: %s", common.PrintFunctionName(), errMsg)
		log.Error(errMsg)
		return
	}

	retval, err := json.Marshal(nsdInfo)
	if err != nil {
		errMsg := fmt.Sprintf("Marshalling nsdInfo failed: %s", err)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(retval)
}

// Get NSD details
func (c *Client) GetNSD(w http.ResponseWriter, r *http.Request) {
	var (
		errMsg string
	)
	vars := mux.Vars(r)
	// Fetch NsInfo from database
	retCode, nsdInfo, err := common.FetchNSDInfo(vars["nsdId"])
	if retCode != http.StatusOK {
		errMsg = "encountered error while fetching NsdInfo"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, retCode, w, r)
		return
	}

	nsdInfo, ok := nsdInfo.(common.NsdInfo)
	if !ok {
		errMsg = "Unable to parse nsdInfo structure..."
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}

	log.Infof("nsdInfo: %+v", nsdInfo)
	response, err := json.Marshal(nsdInfo)
	if err != nil {
		errMsg = "Encountered error while marshalling nsdInfo"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

func (c *Client) FetchAllNSD() ([]common.NsdInfo, *nslcm.ProblemDetails) {
	var (
		nsdInfoList []common.NsdInfo
		errMsg      string
	)

	// Check if nsInfo collection exists
	exists := db.DBconn.CheckCollectionExists(common.NS_COLLECTION)
	if !exists {
		errMsg = "nsInfo collection does not exists..."
		pb := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
			Status: http.StatusOK,
		}
		log.Errorf(errMsg)
		return nsdInfoList, &pb
	}

	key := new(common.NsdInfoKey)
	key.Id = ""

	// Fetch the nsdInfo data using the nsdId key
	values, err := db.DBconn.Find(common.NS_COLLECTION, key, "nsdinfo")
	if err != nil {
		errMsg = fmt.Sprintf("Encountered error while fetching NsdInfo: %s", err)
		pb := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
			Status: http.StatusInternalServerError,
		}
		return nsdInfoList, &pb
	} else if len(values) == 0 {
		errMsg = "Unable to find NsdInfo"
		log.Errorf(errMsg)
		pb := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
			Status: http.StatusOK,
		}
		return nsdInfoList, &pb
	}

	// UnMarshal the fetched nsInfo data
	log.Debugf("NsInfoList: %+v", values)

	for _, value := range values {
		var nsdInfo common.NsdInfo
		err = db.DBconn.Unmarshal(value, &nsdInfo)
		if err != nil {
			errMsg = "Encountered error while unmarshalling nsdInfo"
			log.Errorf(errMsg)
			pb := nslcm.ProblemDetails{
				Title:  &errMsg,
				Detail: errMsg,
				Status: http.StatusInternalServerError,
			}
			return nsdInfoList, &pb
		}
		nsdInfoList = append(nsdInfoList, nsdInfo)
	}
	return nsdInfoList, nil
}

// Get all existing NSD in sol layer
func (c *Client) GetAllNSD(w http.ResponseWriter, r *http.Request) {
	var (
		errMsg string
	)

	nsdInfoList, pb := c.FetchAllNSD()
	if pb != nil && pb.Status != http.StatusOK {
		common.WriteError(*pb, int(pb.Status), w, r)
		return
	}

	jsonLoad, err := json.Marshal(nsdInfoList)
	if err != nil {
		errMsg = "Encountered error while marshalling nsdInfo"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(jsonLoad)
}

func (c *Client) HealNS(w http.ResponseWriter, r *http.Request) {
	var (
		req    HealNsRequest
		errMsg string
	)

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&req)
	if err != nil {
		errMsg = fmt.Sprintf("%s(): failed to parse json: %s", common.PrintFunctionName(), err)
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusBadRequest,
		}
		common.WriteError(problem, int(problem.Status), w, r)
		return
	}
	nsInfo, problem := c.GetNsDetails(r)
	if problem != nil {
		common.WriteError(*problem, int(problem.Status), w, r)
		return
	}
	allowedDegreeHealing := map[string]bool{
		"HEAL_RESTORE":    true,
		"HEAL_QOS":        true,
		"HEAL_RESET":      true,
		"PARTIAL_HEALING": true,
	}

	if !allowedDegreeHealing[req.HealNsData.DegreeHealing] {
		errMsg := fmt.Sprintf("DegreeHealing is not supported.")
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: "DegreeHealing is not supported. Provided value: " + req.HealNsData.DegreeHealing,
			Status: http.StatusBadRequest,
		}
		common.WriteError(problem, http.StatusBadRequest, w, r)
	}
	nsLCMOpp, problem := c.GenerateHealOps(nsInfo, r)
	if problem != nil {
		common.WriteError(*problem, int(problem.Status), w, r)
		return
	}
	c.LocalNotificationHandler.NsHealProcessing(nsInfo.NsId, nsLCMOpp.NsLCMOpOccId)
	go c.HealOperationHandler(nsInfo, nsLCMOpp, req)
	w.Header().Set("Location", nsLCMOpp.Links.Self.Href)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusAccepted)
}

func (c *Client) GetNsDetails(r *http.Request) (common.NsInstanceInfo, *nslcm.ProblemDetails) {
	vars := mux.Vars(r)
	// Fetch NsInfo from database
	retCode, nserviceInfo, err := common.FetchNSInfo(vars["nsInstanceId"])
	if retCode != http.StatusOK {
		errMsg := "failed to fetch NsInfo from db"
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: int32(retCode),
		}
		return common.NsInstanceInfo{}, &problem
	}

	nsInfo, ok := nserviceInfo.(common.NsInstanceInfo)
	if !ok {
		errMsg := "Unable to parse nsInfo structure..."
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return common.NsInstanceInfo{}, &problem
	}
	if nsInfo.NsState == common.NOT_INSTANTIATED {
		errMsg := fmt.Sprintf("NS service is not instantiated. Heal operation failed")
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: "NS Service state:" + nsInfo.NsState,
			Status: http.StatusConflict,
		}
		return nsInfo, &problem
	}
	return nsInfo, nil
}

func (c *Client) GenerateHealOps(nsInfo common.NsInstanceInfo, r *http.Request) (common.NsLCMOpOcc, *nslcm.ProblemDetails) {
	key := new(common.NsLCMOppOccKey)
	id := uuid.New()
	key.NsLCMOppOccId = strings.Replace(id.String(), "-", "", -1)
	var nsLCMOpp common.NsLCMOpOcc
	nsLCMOpp.NsLCMOpOccId = key.NsLCMOppOccId
	nsLCMOpp.LcmOperationType = common.OPERATION_HEAL
	nsLCMOpp.NsInstanceId = nsInfo.NsId
	nsLCMOpp.OperationState = common.LCMOPP_OCC_PROCESSING
	nsLCMOpp.StartTime = time.Now().Format(time.RFC3339)
	nsLCMOpp.StatusEnteredTime = time.Now().Format(time.RFC3339)
	nsLCMOpp.IsAutomaticInvocation = false
	nsLCMOpp.IsCancelPending = false

	// Set the location header, as request is successful
	parsedUrl, err := url.Parse(r.URL.String())
	if err != nil {
		errMsg := fmt.Sprintf("%s : Encountered error while parsing url: %s", common.PrintFunctionName(), r.URL.String())
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return nsLCMOpp, &problem
	}
	split := strings.Split(parsedUrl.Path, "/")

	nsLCMOppOccURL := "http://" + r.Host + strings.Join(split[:len(split)-3], "/") + "/ns_lcm_op_occs/" + nsLCMOpp.NsLCMOpOccId

	var selfLink common.Link
	selfLink.Href = nsLCMOppOccURL
	nsLCMOpp.Links.Self = &selfLink

	err = db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", nsLCMOpp)
	if err != nil {
		errMsg := fmt.Sprintf("Encountered error during insert of heal ops structure for %s: %s", nsInfo.NsName, err)
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return nsLCMOpp, &problem
	}
	return nsLCMOpp, nil
}

func (c *Client) HealOperationHandler(nsInfo common.NsInstanceInfo, nsLCMOpp common.NsLCMOpOcc, req HealNsRequest) {
	problem := c.DoHealOperation(nsInfo, nsLCMOpp, req)
	if problem != nil {
		log.Errorf("Heal Failed: %+v", problem)
		c.LocalNotificationHandler.NsHealFailedTemp(nsInfo.NsId, nsLCMOpp.NsLCMOpOccId, *problem)
		nsLCMOpp.OperationState = common.LCMOPP_OCC_FAILED_TEMP
		var pb common.ProblemDetails
		pb.Detail = problem.Detail
		pb.Title = problem.Title
		pb.Status = problem.Status
		nsLCMOpp.Error = &pb
		key := new(common.NsLCMOppOccKey)
		key.NsLCMOppOccId = nsLCMOpp.NsLCMOpOccId
		err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", nsLCMOpp)
		if err != nil {
			log.Errorf("Error while recording Instantiation failure %v", err.Error())
		}
		return
	}
	c.LocalNotificationHandler.NsHealDone(nsInfo.NsId, nsLCMOpp.NsLCMOpOccId)
	nsLCMOpp.OperationState = common.LCMOPP_OCC_COMPLETED
	key := new(common.NsLCMOppOccKey)
	key.NsLCMOppOccId = nsLCMOpp.NsLCMOpOccId
	err := db.DBconn.Insert(common.NS_COLLECTION, key, nil, "nslcmoppocc", nsLCMOpp)
	if err != nil {
		log.Errorf("Error while recording Instantiation failure %v", err.Error())
	}
}

func (c *Client) DoHealOperation(nsInfo common.NsInstanceInfo, nsLCMOpp common.NsLCMOpOcc, req HealNsRequest) *nslcm.ProblemDetails {
	var addlParams []string
	for key, value := range req.HealNsData.AdditionalParamsforNs {
		addlParams = append(addlParams, key+"="+value)
	}
	scripts := []string{"/opt/heal/heal" + req.HealNsData.HealScript + ".sh"}
	for _, action := range scripts {
		var params []string
		params = append(params, action)
		params = append(params, nsInfo.NsId)
		params = append(params, req.HealNsData.DegreeHealing)
		params = append(params, addlParams...)
		cmd := exec.Command("/bin/sh", params...)
		err := cmd.Run()
		if err != nil {
			title := "Error while executing script: " + action
			return &nslcm.ProblemDetails{
				Title:  &title,
				Status: http.StatusInternalServerError,
				Detail: err.Error(),
			}
		}
	}
	return nil
}

// Delete NSD identifier
func (c *Client) DeleteNSD(w http.ResponseWriter, r *http.Request) {
	var (
		errMsg string
	)

	vars := mux.Vars(r)

	key := new(common.NsdInfoKey)
	key.Id = vars["nsdId"]

	err := db.DBconn.Remove(common.NS_COLLECTION, key)
	if err != nil {
		errMsg = fmt.Sprintf("Encountered error during removal of NSD identifier %s : %s", vars["nsdId"], err)
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
		}
		common.WriteError(problem, http.StatusInternalServerError, w, r)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusNoContent)
}
