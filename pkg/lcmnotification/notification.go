package lcmnotification

import (
	notificationLib "aarna.com/solsvc/lib/nslcmnotification"
	"context"
	"crypto/tls"
	"crypto/x509"
	log "github.com/sirupsen/logrus"
	"net/http"
	"os"
	"strings"
)

func HandleNsIdentifierCreationNotification(ctx context.Context, notification notificationLib.NsIdentifierCreationNotification, url string) (*http.Response, error) {
	cfg := notificationLib.NewConfiguration()
	server := notificationLib.ServerConfiguration{URL: url}
	endPoint := "endpoint"
	cfg.OperationServers[endPoint] = notificationLib.ServerConfigurations{server}
	isHttps := strings.HasPrefix(url, "https")
	if isHttps {
		caCert, err := os.ReadFile("/opt/emco/rootCA.pem")
		if err != nil {
			log.Errorf("Error while reading certificate from rootCA.crt %v", err)
			return nil, err
		}
		caCertPool := x509.NewCertPool()
		caCertPool.AppendCertsFromPEM(caCert)
		log.Debugf("Notification secure client\n")
		httpClient := http.Client{Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				RootCAs: caCertPool,
			}}}
		cfg.HTTPClient = &httpClient
	}
	client := NewAPIClient(cfg)
	apiService := DefaultApiService{
		client: client,
	}
	request := NsIdCreationNotificationPostRequest{
		ctx:        ctx,
		ApiService: &apiService,
	}
	request = request.NsIdentifierCreationNotification(notification)
	request = request.Version("v2")
	request = request.Endpoint(endPoint)
	request = request.Accept("application/json")
	request = request.ContentType("application/json")
	return request.Execute()
}

func HandleNsIdentifierDeletionNotification(ctx context.Context, notification notificationLib.NsIdentifierDeletionNotification, url string) (*http.Response, error) {
	cfg := notificationLib.NewConfiguration()
	server := notificationLib.ServerConfiguration{URL: url}
	endPoint := "DefaultApiService.NsIdentifierDeletionNotificationPost"
	cfg.OperationServers[endPoint] = notificationLib.ServerConfigurations{server}
	isHttps := strings.HasPrefix(url, "https")
	if isHttps {
		caCert, err := os.ReadFile("/opt/emco/rootCA.pem")
		if err != nil {
			log.Warnf("Error while reading certificate from rootCA.crt %v", err)
			return nil, err
		}
		caCertPool := x509.NewCertPool()
		caCertPool.AppendCertsFromPEM(caCert)
		log.Debugf("Notification secure client\n")
		httpClient := http.Client{Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				RootCAs: caCertPool,
			}}}
		cfg.HTTPClient = &httpClient
	}

	client := NewAPIClient(cfg)
	apiService := DefaultApiService{
		client: client,
	}
	request := NsIdentifierDeletionNotificationPostRequest{
		ctx:        ctx,
		ApiService: &apiService,
	}
	request = request.NsIdentifierDeletionNotification(notification)
	request = request.Version("v2")
	request = request.Accept("application/json")
	request = request.ContentType("application/json")
	return request.Execute()
}

func HandleNsLcmOperationOccurrenceNotification(ctx context.Context, notification notificationLib.NsLcmOperationOccurrenceNotification, url string) (*http.Response, error) {
	cfg := notificationLib.NewConfiguration()
	server := notificationLib.ServerConfiguration{URL: url}
	endPoint := "DefaultApiService.NsLcmOperationOccurrenceNotificationPost"
	cfg.OperationServers[endPoint] = notificationLib.ServerConfigurations{server}
	isHttps := strings.HasPrefix(url, "https")
	if isHttps {
		caCert, err := os.ReadFile("/opt/emco/rootCA.pem")
		if err != nil {
			log.Warnf("Error while reading certificate from rootCA.crt %v", err)
		}
		caCertPool := x509.NewCertPool()
		caCertPool.AppendCertsFromPEM(caCert)
		log.Debugf("Notification secure client\n")
		httpClient := http.Client{Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				RootCAs: caCertPool,
			}}}
		cfg.HTTPClient = &httpClient
	}
	client := NewAPIClient(cfg)
	apiService := DefaultApiService{
		client: client,
	}
	request := NsLcmOperationOccurrenceNotificationPostRequest{
		ctx:        ctx,
		ApiService: &apiService,
	}
	request = request.NsLcmOperationOccurrenceNotification(notification)
	request = request.Version("v2")
	request = request.Accept("application/json")
	request = request.ContentType("application/json")
	return request.Execute()
}
