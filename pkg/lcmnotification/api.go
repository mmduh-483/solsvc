package lcmnotification

import (
	"bytes"
	"context"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"net/url"
)
import notify "aarna.com/solsvc/lib/nslcmnotification"

type DefaultApiService service

type NsIdCreationNotificationPostRequest struct {
	ctx                              context.Context
	ApiService                       *DefaultApiService
	version                          *string
	accept                           *string
	contentType                      *string
	nsIdentifierCreationNotification *notify.NsIdentifierCreationNotification
	authorization                    *string
	endpoint                         *string
}

// Version of the API requested to use when responding to this request.
func (r NsIdCreationNotificationPostRequest) Version(version string) NsIdCreationNotificationPostRequest {
	r.version = &version
	return r
}

func (r NsIdCreationNotificationPostRequest) Accept(accept string) NsIdCreationNotificationPostRequest {
	r.accept = &accept
	return r
}

func (r NsIdCreationNotificationPostRequest) ContentType(contentType string) NsIdCreationNotificationPostRequest {
	r.contentType = &contentType
	return r
}

func (r NsIdCreationNotificationPostRequest) Endpoint(endpoint string) NsIdCreationNotificationPostRequest {
	r.endpoint = &endpoint
	return r
}

func (r NsIdCreationNotificationPostRequest) NsIdentifierCreationNotification(nsIdentifierCreationNotification notify.NsIdentifierCreationNotification) NsIdCreationNotificationPostRequest {
	r.nsIdentifierCreationNotification = &nsIdentifierCreationNotification
	return r
}

func (r NsIdCreationNotificationPostRequest) Authorization(authorization string) NsIdCreationNotificationPostRequest {
	r.authorization = &authorization
	return r
}

func (r NsIdCreationNotificationPostRequest) Execute() (*http.Response, error) {
	return r.ApiService.NsIdentifierCreationNotificationPostExecute(r, *r.endpoint)
}

func (a *DefaultApiService) NsIdentifierCreationNotificationPost(ctx context.Context) NsIdCreationNotificationPostRequest {
	return NsIdCreationNotificationPostRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

// NsIdentifierCreationNotificationPostExecute executes the post request to the given endpoint
func (a *DefaultApiService) NsIdentifierCreationNotificationPostExecute(r NsIdCreationNotificationPostRequest, endpoint string) (*http.Response, error) {
	var (
		localVarHTTPMethod = http.MethodPost
		localVarPostBody   interface{}
		formFiles          []formFile
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, endpoint)
	if err != nil {
		return nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}
	if r.version == nil {
		return nil, reportError("version is required and must be specified")
	}
	if r.accept == nil {
		return nil, reportError("accept is required and must be specified")
	}
	if r.contentType == nil {
		return nil, reportError("contentType is required and must be specified")
	}
	if r.nsIdentifierCreationNotification == nil {
		return nil, reportError("nsIdentifierCreationNotification is required and must be specified")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	localVarHeaderParams["Version"] = parameterToString(*r.version, "")
	if r.authorization != nil {
		localVarHeaderParams["Authorization"] = parameterToString(*r.authorization, "")
	}
	localVarHeaderParams["Accept"] = parameterToString(*r.accept, "")
	localVarHeaderParams["Content-Type"] = parameterToString(*r.contentType, "")
	// body params
	localVarPostBody = r.nsIdentifierCreationNotification
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)

	log.Infof("Notification request: %+v", req)
	if err != nil {
		log.Errorf("Notification error while preparing request: %s", err.Error())
		return nil, err
	}

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil {
		log.Errorf("Notification error while sending request: Error: %s, response:%v ", err.Error(), localVarHTTPResponse)
		return localVarHTTPResponse, err
	}
	if localVarHTTPResponse == nil {
		log.Errorf("Notification error while sending request: error: %v, response:%v ", err, localVarHTTPResponse)
		return localVarHTTPResponse, err
	}

	localVarBody, err := ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		log.Errorf("Notification error while sending request: Error: %s, response:%v ", err.Error(), localVarHTTPResponse)
		return localVarHTTPResponse, err
	}
	log.Infof("Notification send status code: %v, response: %v", localVarHTTPResponse.StatusCode, localVarHTTPResponse)

	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		if localVarHTTPResponse.StatusCode == 400 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
			return localVarHTTPResponse, newErr
		}
		if localVarHTTPResponse.StatusCode == 401 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
			return localVarHTTPResponse, newErr
		}
		if localVarHTTPResponse.StatusCode == 403 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
			return localVarHTTPResponse, newErr
		}
		if localVarHTTPResponse.StatusCode == 405 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
			return localVarHTTPResponse, newErr
		}
		if localVarHTTPResponse.StatusCode == 406 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
			return localVarHTTPResponse, newErr
		}
		if localVarHTTPResponse.StatusCode == 500 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
			return localVarHTTPResponse, newErr
		}
		if localVarHTTPResponse.StatusCode == 503 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
		}
		return localVarHTTPResponse, newErr
	}

	return localVarHTTPResponse, nil
}

type NsIdentifierCreationNotificationGetRequest struct {
	ctx           context.Context
	ApiService    *DefaultApiService
	version       *string
	accept        *string
	authorization *string
}

// Version of the API requested to use when responding to this request.
func (r NsIdentifierCreationNotificationGetRequest) Version(version string) NsIdentifierCreationNotificationGetRequest {
	r.version = &version
	return r
}

func (r NsIdentifierCreationNotificationGetRequest) Accept(accept string) NsIdentifierCreationNotificationGetRequest {
	r.accept = &accept
	return r
}

func (r NsIdentifierCreationNotificationGetRequest) Authorization(authorization string) NsIdentifierCreationNotificationGetRequest {
	r.authorization = &authorization
	return r
}

func (r NsIdentifierCreationNotificationGetRequest) Execute(endpoint string) (*http.Response, error) {
	return r.ApiService.NsIdentifierCreationNotificationGetExecute(r, endpoint)
}

func (a *DefaultApiService) NsIdentifierCreationNotificationGet(ctx context.Context) NsIdentifierCreationNotificationGetRequest {
	return NsIdentifierCreationNotificationGetRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

func (a *DefaultApiService) NsIdentifierCreationNotificationGetExecute(r NsIdentifierCreationNotificationGetRequest, endpoint string) (*http.Response, error) {
	var (
		localVarHTTPMethod = http.MethodGet
		localVarPostBody   interface{}
		formFiles          []formFile
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, endpoint)
	if err != nil {
		return nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}
	if r.version == nil {
		return nil, reportError("version is required and must be specified")
	}
	if r.accept == nil {
		return nil, reportError("accept is required and must be specified")
	}

	// to determine the Content-Type header
	var localVarHTTPContentTypes []string

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	localVarHeaderParams["Version"] = parameterToString(*r.version, "")
	if r.authorization != nil {
		localVarHeaderParams["Authorization"] = parameterToString(*r.authorization, "")
	}
	localVarHeaderParams["Accept"] = parameterToString(*r.accept, "")
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		log.Errorf("Notification error while preparing request: %s", err.Error())
		return nil, err
	}
	log.Infof("Notification request: %+v", req)

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil {
		log.Errorf("Notification error while sending request: Error: %s, response:%v ", err.Error(), localVarHTTPResponse)
		return localVarHTTPResponse, err
	}
	if localVarHTTPResponse == nil {
		log.Errorf("Notification error while sending request: error: %v, response:%v ", err, localVarHTTPResponse)
		return localVarHTTPResponse, err
	}

	localVarBody, err := ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		log.Errorf("Notification error while sending request: Error: %s, response:%v ", err.Error(), localVarHTTPResponse)
		return localVarHTTPResponse, err
	}
	log.Infof("Notification send status code: %v, response: %v", localVarHTTPResponse.StatusCode, localVarHTTPResponse)
	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		if localVarHTTPResponse.StatusCode == 400 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
			return localVarHTTPResponse, newErr
		}
		if localVarHTTPResponse.StatusCode == 401 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
			return localVarHTTPResponse, newErr
		}
		if localVarHTTPResponse.StatusCode == 403 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
			return localVarHTTPResponse, newErr
		}
		if localVarHTTPResponse.StatusCode == 405 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
			return localVarHTTPResponse, newErr
		}
		if localVarHTTPResponse.StatusCode == 406 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
			return localVarHTTPResponse, newErr
		}
		if localVarHTTPResponse.StatusCode == 500 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
			return localVarHTTPResponse, newErr
		}
		if localVarHTTPResponse.StatusCode == 503 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
		}
		return localVarHTTPResponse, newErr
	}

	return localVarHTTPResponse, nil
}

type NsIdentifierDeletionNotificationPostRequest struct {
	ctx                              context.Context
	ApiService                       *DefaultApiService
	version                          *string
	accept                           *string
	contentType                      *string
	nsIdentifierDeletionNotification *notify.NsIdentifierDeletionNotification
	authorization                    *string
}

func (r NsIdentifierDeletionNotificationPostRequest) Version(version string) NsIdentifierDeletionNotificationPostRequest {
	r.version = &version
	return r
}

func (r NsIdentifierDeletionNotificationPostRequest) Accept(accept string) NsIdentifierDeletionNotificationPostRequest {
	r.accept = &accept
	return r
}

func (r NsIdentifierDeletionNotificationPostRequest) ContentType(contentType string) NsIdentifierDeletionNotificationPostRequest {
	r.contentType = &contentType
	return r
}

func (r NsIdentifierDeletionNotificationPostRequest) NsIdentifierDeletionNotification(nsIdentifierDeletionNotification notify.NsIdentifierDeletionNotification) NsIdentifierDeletionNotificationPostRequest {
	r.nsIdentifierDeletionNotification = &nsIdentifierDeletionNotification
	return r
}

func (r NsIdentifierDeletionNotificationPostRequest) Authorization(authorization string) NsIdentifierDeletionNotificationPostRequest {
	r.authorization = &authorization
	return r
}

func (r NsIdentifierDeletionNotificationPostRequest) Execute() (*http.Response, error) {
	return r.ApiService.NsIdentifierDeletionNotificationPostExecute(r)
}

/*
NsIdentifierDeletionNotificationPost Method for NsIdentifierDeletionNotificationPost

The POST method delivers a notification from the API producer to an API consumer. The API consumer shall
have previously created an "Individual subscription" resource with a matching filter.
See clause 6.4.18.3.1.


 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return NsIdentifierDeletionNotificationPostRequest
*/
func (a *DefaultApiService) NsIdentifierDeletionNotificationPost(ctx context.Context) NsIdentifierDeletionNotificationPostRequest {
	return NsIdentifierDeletionNotificationPostRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

func (a *DefaultApiService) NsIdentifierDeletionNotificationPostExecute(r NsIdentifierDeletionNotificationPostRequest) (*http.Response, error) {
	var (
		localVarHTTPMethod = http.MethodPost
		localVarPostBody   interface{}
		formFiles          []formFile
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "DefaultApiService.NsIdentifierDeletionNotificationPost")
	if err != nil {
		return nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}
	if r.version == nil {
		return nil, reportError("version is required and must be specified")
	}
	if r.accept == nil {
		return nil, reportError("accept is required and must be specified")
	}
	if r.contentType == nil {
		return nil, reportError("contentType is required and must be specified")
	}
	if r.nsIdentifierDeletionNotification == nil {
		return nil, reportError("nsIdentifierDeletionNotification is required and must be specified")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	localVarHeaderParams["Version"] = parameterToString(*r.version, "")
	if r.authorization != nil {
		localVarHeaderParams["Authorization"] = parameterToString(*r.authorization, "")
	}
	localVarHeaderParams["Accept"] = parameterToString(*r.accept, "")
	localVarHeaderParams["Content-Type"] = parameterToString(*r.contentType, "")
	// body params
	localVarPostBody = r.nsIdentifierDeletionNotification
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		log.Errorf("Notification error while preparing request: %s", err.Error())
		return nil, err
	}
	log.Infof("Notification request: %+v", req)

	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil {
		log.Errorf("Notification error while sending request: Error: %s, response:%v ", err.Error(), localVarHTTPResponse)
		return localVarHTTPResponse, err
	}
	if localVarHTTPResponse == nil {
		log.Errorf("Notification error while sending request: error: %v, response:%v ", err, localVarHTTPResponse)
		return localVarHTTPResponse, err
	}

	localVarBody, err := ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		log.Errorf("Notification error while sending request: error: %v, response:%v ", err, localVarHTTPResponse)
		return localVarHTTPResponse, err
	}
	log.Infof("Notification send status code: %v, response: %v", localVarHTTPResponse.StatusCode, localVarHTTPResponse)
	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		if localVarHTTPResponse.StatusCode == 400 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
			return localVarHTTPResponse, newErr
		}
		if localVarHTTPResponse.StatusCode == 401 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
			return localVarHTTPResponse, newErr
		}
		if localVarHTTPResponse.StatusCode == 403 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
			return localVarHTTPResponse, newErr
		}
		if localVarHTTPResponse.StatusCode == 405 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
			return localVarHTTPResponse, newErr
		}
		if localVarHTTPResponse.StatusCode == 406 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
			return localVarHTTPResponse, newErr
		}
		if localVarHTTPResponse.StatusCode == 500 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
			return localVarHTTPResponse, newErr
		}
		if localVarHTTPResponse.StatusCode == 503 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
		}
		return localVarHTTPResponse, newErr
	}

	return localVarHTTPResponse, nil
}

type NsLcmOperationOccurrenceNotificationPostRequest struct {
	ctx                                  context.Context
	ApiService                           *DefaultApiService
	version                              *string
	accept                               *string
	contentType                          *string
	nsLcmOperationOccurrenceNotification *notify.NsLcmOperationOccurrenceNotification
	authorization                        *string
}

func (r NsLcmOperationOccurrenceNotificationPostRequest) Version(version string) NsLcmOperationOccurrenceNotificationPostRequest {
	r.version = &version
	return r
}

func (r NsLcmOperationOccurrenceNotificationPostRequest) Accept(accept string) NsLcmOperationOccurrenceNotificationPostRequest {
	r.accept = &accept
	return r
}

func (r NsLcmOperationOccurrenceNotificationPostRequest) ContentType(contentType string) NsLcmOperationOccurrenceNotificationPostRequest {
	r.contentType = &contentType
	return r
}

func (r NsLcmOperationOccurrenceNotificationPostRequest) NsLcmOperationOccurrenceNotification(nsLcmOperationOccurrenceNotification notify.NsLcmOperationOccurrenceNotification) NsLcmOperationOccurrenceNotificationPostRequest {
	r.nsLcmOperationOccurrenceNotification = &nsLcmOperationOccurrenceNotification
	return r
}

func (r NsLcmOperationOccurrenceNotificationPostRequest) Authorization(authorization string) NsLcmOperationOccurrenceNotificationPostRequest {
	r.authorization = &authorization
	return r
}

func (r NsLcmOperationOccurrenceNotificationPostRequest) Execute() (*http.Response, error) {
	return r.ApiService.NsLcmOperationOccurrenceNotificationPostExecute(r)
}

/*
NsLcmOperationOccurrenceNotificationPost Method for NsLcmOperationOccurrenceNotificationPost

The GET method allows the API producer to test the notification endpoint that is provided by the API consumer,
e.g. during subscription. See clause 6.4.18.3.2.


 @param ctx context.Context - for authentication, logging, cancellation, deadlines, tracing, etc. Passed from http.Request or context.Background().
 @return NsLcmOperationOccurrenceNotificationPostRequest
*/
func (a *DefaultApiService) NsLcmOperationOccurrenceNotificationPost(ctx context.Context) NsLcmOperationOccurrenceNotificationPostRequest {
	return NsLcmOperationOccurrenceNotificationPostRequest{
		ApiService: a,
		ctx:        ctx,
	}
}

func (a *DefaultApiService) NsLcmOperationOccurrenceNotificationPostExecute(r NsLcmOperationOccurrenceNotificationPostRequest) (*http.Response, error) {
	var (
		localVarHTTPMethod = http.MethodPost
		localVarPostBody   interface{}
		formFiles          []formFile
	)

	localBasePath, err := a.client.cfg.ServerURLWithContext(r.ctx, "DefaultApiService.NsLcmOperationOccurrenceNotificationPost")
	if err != nil {
		return nil, &GenericOpenAPIError{error: err.Error()}
	}

	localVarPath := localBasePath

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}
	if r.version == nil {
		return nil, reportError("version is required and must be specified")
	}
	if r.accept == nil {
		return nil, reportError("accept is required and must be specified")
	}
	if r.contentType == nil {
		return nil, reportError("contentType is required and must be specified")
	}
	if r.nsLcmOperationOccurrenceNotification == nil {
		return nil, reportError("nsLcmOperationOccurrenceNotification is required and must be specified")
	}

	// to determine the Content-Type header
	localVarHTTPContentTypes := []string{"application/json"}

	// set Content-Type header
	localVarHTTPContentType := selectHeaderContentType(localVarHTTPContentTypes)
	if localVarHTTPContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHTTPContentType
	}

	// to determine the Accept header
	localVarHTTPHeaderAccepts := []string{"application/json"}

	// set Accept header
	localVarHTTPHeaderAccept := selectHeaderAccept(localVarHTTPHeaderAccepts)
	if localVarHTTPHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHTTPHeaderAccept
	}
	localVarHeaderParams["Version"] = parameterToString(*r.version, "")
	if r.authorization != nil {
		localVarHeaderParams["Authorization"] = parameterToString(*r.authorization, "")
	}
	localVarHeaderParams["Accept"] = parameterToString(*r.accept, "")
	localVarHeaderParams["Content-Type"] = parameterToString(*r.contentType, "")
	// body params
	localVarPostBody = r.nsLcmOperationOccurrenceNotification
	req, err := a.client.prepareRequest(r.ctx, localVarPath, localVarHTTPMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, formFiles)
	if err != nil {
		log.Errorf("Notification error while preparing request: %s", err.Error())
		return nil, err
	}
	log.Infof("Notification request: %+v", req)
	localVarHTTPResponse, err := a.client.callAPI(req)
	if err != nil {
		log.Errorf("Notification error while sending request: Error: %s, response:%v ", err.Error(), localVarHTTPResponse)
		return localVarHTTPResponse, err
	}
	if localVarHTTPResponse == nil {
		log.Errorf("Notification error while sending request: error: %v, response:%v ", err, localVarHTTPResponse)
		return localVarHTTPResponse, err
	}

	localVarBody, err := ioutil.ReadAll(localVarHTTPResponse.Body)
	localVarHTTPResponse.Body.Close()
	localVarHTTPResponse.Body = ioutil.NopCloser(bytes.NewBuffer(localVarBody))
	if err != nil {
		log.Errorf("Notification error while sending request: error: %v, response:%v ", err, localVarHTTPResponse)
		return localVarHTTPResponse, err
	}
	log.Infof("Notification send status code: %v, response: %v", localVarHTTPResponse.StatusCode, localVarHTTPResponse)
	if localVarHTTPResponse.StatusCode >= 300 {
		newErr := &GenericOpenAPIError{
			body:  localVarBody,
			error: localVarHTTPResponse.Status,
		}
		if localVarHTTPResponse.StatusCode == 400 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
			return localVarHTTPResponse, newErr
		}
		if localVarHTTPResponse.StatusCode == 401 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
			return localVarHTTPResponse, newErr
		}
		if localVarHTTPResponse.StatusCode == 403 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
			return localVarHTTPResponse, newErr
		}
		if localVarHTTPResponse.StatusCode == 405 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
			return localVarHTTPResponse, newErr
		}
		if localVarHTTPResponse.StatusCode == 406 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
			return localVarHTTPResponse, newErr
		}
		if localVarHTTPResponse.StatusCode == 500 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
			return localVarHTTPResponse, newErr
		}
		if localVarHTTPResponse.StatusCode == 503 {
			var v notify.ProblemDetails
			err = a.client.decode(&v, localVarBody, localVarHTTPResponse.Header.Get("Content-Type"))
			if err != nil {
				newErr.error = err.Error()
				return localVarHTTPResponse, newErr
			}
			newErr.model = v
		}
		return localVarHTTPResponse, newErr
	}

	return localVarHTTPResponse, nil
}
