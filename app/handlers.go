package app

import (
	"aarna.com/solsvc/lib/common"
	"aarna.com/solsvc/pkg/fmsubscription"
	"aarna.com/solsvc/pkg/lcmops"
	"aarna.com/solsvc/pkg/lcmsubscription"
	"aarna.com/solsvc/pkg/notificationInfra"
	"aarna.com/solsvc/pkg/subscriptionclient"
	"github.com/gorilla/mux"
	"net/http"
)

const (
	ApiVersion = "v3"
)

// NewAppHandler interface implementing REST callhandler
func NewAppHandler() *SOLHandler {
	return &SOLHandler{}
}

type SOLHandler struct {
	SOLConfig                common.SOLConf
	response                 ResponseData
	client                   http.Client
	SubscriptionClient       subscriptionclient.SubscriptionClient
	EmcoNotificationHandler  notificationInfra.EmcoNotificationHandler
	LocalNotificationHandler notificationInfra.LocalNotificationHandler
}

type ResponseData struct {
	lastKey   string
	payload   map[string][]byte
	status    map[string]int
	statusMsg map[string]string
}

type HandleFunc func(string, func(http.ResponseWriter, *http.Request)) *mux.Route

// RegisterHandlers is a helper function for net/http/HandleFunc .
func RegisterHandlers(handle HandleFunc, bootConf common.SOLConf) {
	subscriptionClient := subscriptionclient.InitializeSubscriptionClient()
	emcoNotificationClient := notificationInfra.NewEmcoNotificationHandler(&subscriptionClient)
	localNotificationClient := notificationInfra.NewLocalNotificationHandler(&subscriptionClient)
	createInstance := func() *SOLHandler {
		o := NewAppHandler()
		o.SOLConfig = bootConf
		o.SubscriptionClient = subscriptionClient
		o.EmcoNotificationHandler = emcoNotificationClient
		o.LocalNotificationHandler = localNotificationClient
		return o
	}

	h := createInstance()
	subscriptionsHandlers(handle, bootConf, h)
	fmsubscriptionsHandlers(handle, bootConf, h)
	lcmHandlers(handle, bootConf, h)
}

func subscriptionsHandlers(handle HandleFunc, bootConf common.SOLConf, h *SOLHandler) {
	// Copying data to avoid cyclic dependecy. Need a refactor to avoid this
	client := lcmsubscription.Client{
		SubscriptionClient:       h.SubscriptionClient,
		EmcoNotificationHandler:  h.EmcoNotificationHandler,
		LocalNotificationHandler: h.LocalNotificationHandler,
		Conf: lcmsubscription.SOLConf{
			OwnPort:            bootConf.OwnPort,
			MiddleEnd:          bootConf.MiddleEnd,
			Orchestrator:       bootConf.Orchestrator,
			Mongo:              bootConf.Mongo,
			LogLevel:           bootConf.LogLevel,
			StoreName:          bootConf.StoreName,
			Dcm:                bootConf.Dcm,
			OrchestratorStatus: bootConf.OrchestratorStatus,
		},
	}
	handle("/nslcm/"+ApiVersion+"/subscriptions", client.GetHandler).Methods(http.MethodGet)
	handle("/nslcm/"+ApiVersion+"/subscriptions", client.CreateHandler).Methods(http.MethodPost)
	handle("/nslcm/"+ApiVersion+"/subscriptions/{subscriptionId}", client.GetSubscriptionHandler).Methods(http.MethodGet)
	handle("/nslcm/"+ApiVersion+"/subscriptions/{subscriptionId}", client.DeleteHandler).Methods(http.MethodDelete)
}

func lcmHandlers(handle HandleFunc, bootConf common.SOLConf, h *SOLHandler) {
	// Copying data to avoid cyclic dependecy. Need a refactor to avoid this
	clientInfo := common.ClientInfo{
		Conf: common.SOLConf{
			OwnPort:            bootConf.OwnPort,
			MiddleEnd:          bootConf.MiddleEnd,
			Orchestrator:       bootConf.Orchestrator,
			Mongo:              bootConf.Mongo,
			LogLevel:           bootConf.LogLevel,
			StoreName:          bootConf.StoreName,
			Dcm:                bootConf.Dcm,
			OrchestratorStatus: bootConf.OrchestratorStatus,
			Gac:                bootConf.Gac,
		},
	}

	client := lcmops.Client{
		LocalNotificationHandler: h.LocalNotificationHandler,
		EmcoNotificationHandler:  h.EmcoNotificationHandler,
		Info:                     clientInfo,
	}
	// APIs related to network service LCM
	handle("/nslcm/"+ApiVersion+"/ns_instances", client.CreateNSInfo).Methods(http.MethodPost)
	handle("/nslcm/"+ApiVersion+"/ns_instances/{nsInstanceId}/instantiate", client.InstantiateNS).Methods(http.MethodPost)
	handle("/nslcm/"+ApiVersion+"/ns_instances/{nsInstanceId}/terminate", client.TerminateNS).Methods(http.MethodPost)
	handle("/nslcm/"+ApiVersion+"/ns_instances/{nsInstanceId}", client.DeleteNSInfo).Methods(http.MethodDelete)
	handle("/nslcm/"+ApiVersion+"/ns_instances/{nsInstanceId}", client.GetNSInfo).Methods(http.MethodGet)
	handle("/nslcm/"+ApiVersion+"/ns_instances", client.GetNSInstances).Queries("filter", "{filter}")
	handle("/nslcm/"+ApiVersion+"/ns_instances", client.GetNSInstances).Methods(http.MethodGet)
	handle("/nslcm/"+ApiVersion+"/ns_instances/{nsInstanceId}/scale", client.ScalePostRequest).Methods(http.MethodPost)
	handle("/nslcm/"+ApiVersion+"/ns_instances/{nsInstanceId}/update", client.UpdatePostRequest).Methods(http.MethodPost)
	handle("/nslcm/"+ApiVersion+"/ns_instances/{nsInstanceId}/heal", client.HealNS).Methods(http.MethodPost)

	// APIs related to network service LCM Operation Occurrence
	handle("/nslcm/"+ApiVersion+"/ns_lcm_op_occs/{nsLCMOppOccId}", client.GetLCMOppOCCStatus).Methods(http.MethodGet)
	handle("/nslcm/"+ApiVersion+"/ns_lcm_op_occs", client.GetNsLcmOpOccInstances).Queries("filter", "{filter}")
	handle("/nslcm/"+ApiVersion+"/ns_lcm_op_occs", client.GetNsLcmOpOccInstances).Methods(http.MethodGet)
	handle("/nslcm/"+ApiVersion+"/ns_lcm_op_occs/{nsLCMOppOccId}/retry", client.RetryNS).Methods(http.MethodPost)
	handle("/nslcm/"+ApiVersion+"/ns_lcm_op_occs/{nsLCMOppOccId}/rollback", client.RollBackNS).Methods(http.MethodPost)
	handle("/nslcm/"+ApiVersion+"/ns_lcm_op_occs/{nsLCMOppOccId}/cancel", client.CancelOps).Methods(http.MethodPost)
	handle("/nslcm/"+ApiVersion+"/ns_lcm_op_occs/{nsLCMOppOccId}/continue", client.ContinueOps).Methods(http.MethodPost)
	handle("/nslcm/"+ApiVersion+"/ns_lcm_op_occs/{nsLCMOppOccId}/fail", client.FailOps).Methods(http.MethodPost)

	// API related to NSD
	handle("/nslcm/"+ApiVersion+"/nsd", client.CreateNSD).Methods(http.MethodPost)
	handle("/nslcm/"+ApiVersion+"/nsd/{nsdId}", client.GetNSD).Methods(http.MethodGet)
	handle("/nslcm/"+ApiVersion+"/nsd/{nsdId}", client.DeleteNSD).Methods(http.MethodDelete)
	handle("/nslcm/"+ApiVersion+"/nsd", client.GetAllNSD).Methods(http.MethodGet)
	handle("/nslcm/"+ApiVersion+"/scaled", client.ScaleSpecRequest).Methods(http.MethodPost)
	handle("/nslcm/"+ApiVersion+"/scaled/{id}", client.ScaleSpecGet).Methods(http.MethodGet)
	handle("/nslcm/"+ApiVersion+"/scaled/vnf/{id}", client.VnfScaleSpecGet).Methods(http.MethodGet)
}

func fmsubscriptionsHandlers(handle HandleFunc, bootConf common.SOLConf, h *SOLHandler) {
	// Copying data to avoid cyclic dependecy. Need a refactor to avoid this
	client := fmsubscription.Client{
		SubscriptionClient:      h.SubscriptionClient,
		EmcoNotificationHandler: h.EmcoNotificationHandler,
		Conf: fmsubscription.SOLConf{
			OwnPort:            bootConf.OwnPort,
			MiddleEnd:          bootConf.MiddleEnd,
			Orchestrator:       bootConf.Orchestrator,
			Mongo:              bootConf.Mongo,
			LogLevel:           bootConf.LogLevel,
			StoreName:          bootConf.StoreName,
			Dcm:                bootConf.Dcm,
			OrchestratorStatus: bootConf.OrchestratorStatus,
		},
	}
	handle("/nsfm/"+ApiVersion+"/alarms", client.GetAlarms).Methods(http.MethodGet)
	handle("/nsfm/"+ApiVersion+"/alarms/{alarmId}", client.GetAlarm).Methods(http.MethodGet)
	handle("/nsfm/"+ApiVersion+"/alarms/{alarmId}", client.AcknowledgeAlarm).Methods(http.MethodPatch)
	handle("/nsfm/"+ApiVersion+"/subscriptions", client.SubscribeAlarms).Methods(http.MethodPost)
	handle("/nsfm/"+ApiVersion+"/subscriptions", client.GetFMSubcriptions).Methods(http.MethodGet)
	handle("/nsfm/"+ApiVersion+"/subscriptions/{subscriptionId}", client.GetFMSubcription).Methods(http.MethodGet)
	handle("/nsfm/"+ApiVersion+"/subscriptions/{subscriptionId}", client.DeleteFMSubcription).Methods(http.MethodDelete)
	handle("/nsfm/"+ApiVersion+"/receivealerts", client.ReceiveAlerts).Methods(http.MethodPost)
}
