//=======================================================================
// Copyright (c) 2017-2022 Aarna Networks, Inc.
// All rights reserved.
// ======================================================================
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//           http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ========================================================================

package main

import (
	"aarna.com/solsvc/app"
	"aarna.com/solsvc/db"
	"aarna.com/solsvc/lib/common"
	"context"
	"encoding/json"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"time"
)

func init() {
	// Log as JSON instead of the default ASCII formatter.
	log.SetFormatter(&log.JSONFormatter{})

	// Output to stdout instead of the default stderr
	log.SetOutput(os.Stdout)
}

/* This is the main package of the solsvc. This package
 * implements the http server which exposes service ar 9891.
 * It also intialises an API router which handles the APIs with
 * subpath /v3.
 */
func main() {
	configFile, err := os.Open("/opt/emco/config/solsvc.conf")
	if err != nil {
		log.WithError(err).Errorf("%s(): Failed to read solsvc configuration", common.PrintFunctionName())
		return
	}
	defer func(configFile *os.File) {
		err := configFile.Close()
		if err != nil {
			log.WithError(err).Errorf("%s(): Failed to close read file handler", common.PrintFunctionName())
			return
		}
	}(configFile)

	// Read the configuration json
	byteValue, err := ioutil.ReadAll(configFile)
	if err != nil {
		log.WithError(err).Errorf("%s(): Failed to read configuration file", common.PrintFunctionName())
		return
	}
	bootConf := &common.SOLConf{}
	err = json.Unmarshal(byteValue, bootConf)
	if err != nil {
		log.WithError(err).Errorf("%s(): Failed to unmarshal config", common.PrintFunctionName())
		return
	}

	// parse string, this is built-in feature of logrus
	logLevel, err := log.ParseLevel(bootConf.LogLevel)
	if err != nil {
		logLevel = log.DebugLevel
	}

	// set global log level
	log.SetLevel(logLevel)

	// Connect to the DB
	err = db.CreateDBClient("mongo", "soldb", bootConf.Mongo)
	if err != nil {
		log.Error("Failed to connect to DB")
		return
	}

	bootConf.StoreName = "soldb"
	httpRouter := mux.NewRouter().PathPrefix("/api/etsi").Subrouter()
	loggedRouter := handlers.LoggingHandler(os.Stdout, httpRouter)
	log.Infof("%s(): Starting SOL service", common.PrintFunctionName())
	log.WithFields(log.Fields{
		"ownport":   bootConf.OwnPort,
		"middleend": bootConf.MiddleEnd,
		"mongo":     bootConf.Mongo,
		"logLevel":  bootConf.LogLevel,
		"storeName": bootConf.StoreName,
	}).Infof("SOL Configuration")

	httpServer := &http.Server{
		Handler:      loggedRouter,
		Addr:         ":" + bootConf.OwnPort,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	//Package level Handlers
	app.RegisterHandlers(httpRouter.HandleFunc, *bootConf)

	// Start server in a go routine.
	go func() {
		log.Fatal(httpServer.ListenAndServe())
	}()

	// Graceful shutdown of the server,
	// create a channel and wait for SIGINT
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	log.Info("wait for signal")
	<-c
	log.Info("Bye Bye")
	httpServer.Shutdown(context.Background())
}
