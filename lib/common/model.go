package common

import (
	nslcm "aarna.com/solsvc/lib/nslcm"
	nslcmnotification "aarna.com/solsvc/lib/nslcmnotification"
	"time"
)

type ClientInfo struct {
	Conf SOLConf
}

type AlarmKey struct {
	AlarmId string `json:"id"`
}

type SOLConf struct {
	OwnPort            string `json:"ownport"`
	MiddleEnd          string `json:"middleend"`
	Orchestrator       string `json:"orchestrator"`
	Mongo              string `json:"mongo"`
	LogLevel           string `json:"logLevel"`
	StoreName          string `json:"storeName"`
	Dcm                string `json:"dcm"`
	OrchestratorStatus string `json:"orchestratorStatus"`
	Gac                string `json:"gac"`
}

type GenericK8sIntent struct {
	Metadata Metadata `json:"metadata"`
}

type Metadata struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	UserData1   string `json:"userData1"`
	UserData2   string `json:"userData2"`
}

// Resource consists of metadata and Spec
type Resource struct {
	Metadata Metadata     `json:"metadata"`
	Spec     ResourceSpec `json:"spec"`
}

type Labels struct {
	LabelName string `json:"clusterLabel"`
}

type ClusterReferenceFlat struct {
	Metadata struct {
		Name        string `json:"name"`
		Description string `json:"description"`
		Userdata1   string `json:"userData1"`
		Userdata2   string `json:"userData2"`
	} `json:"metadata"`
	Spec struct {
		ClusterProvider string   `json:"clusterProvider"`
		ClusterName     string   `json:"cluster"`
		LoadbalancerIP  string   `json:"loadBalancerIP"`
		Certificate     string   `json:"certificate,omitempty"`
		LabelList       []Labels `json:"labels,omitempty"`
	} `json:"spec"`
}

// ResourceSpec consists of App, NewObject, ExistingResource
type ResourceSpec struct {
	App         string      `json:"app"`
	NewObject   string      `json:"newObject"`
	ResourceGVK ResourceGVK `json:"resourceGVK,omitempty"`
}

// ResourceGVK consists of ApiVersion, Kind, Name
type ResourceGVK struct {
	APIVersion string `json:"apiVersion"`
	Kind       string `json:"kind"`
	Name       string `json:"name"`
}

// ResourceFileContent contains the content of resourceTemplate
type ResourceFileContent struct {
	FileContent string `json:"filecontent"`
}

// Customization consists of metadata and Spec
type Customization struct {
	Metadata Metadata      `json:"metadata"`
	Spec     CustomizeSpec `json:"spec"`
}

// SpecFileContent contains the array of file contents
type SpecFileContent struct {
	FileContents []string
	FileNames    []string
}

// ResourceInfo contains all the required information for creating a resource and applying the required
// customization
type ResourceInfo struct {
	ResourceSpec      ResourceSpec        `json:"rspec"`
	CustomizationSpec CustomizeSpec       `json:"cspec"`
	ResourceFile      ResourceFileContent `json:"resFile,omitempty"`
	ResourceFileName  string              `json:"resFileName,omitempty"`
	CustomFile        SpecFileContent     `json:"czFile,omitempty"`
}

// CustomizeSpec consists of ClusterSpecific and ClusterInfo
type CustomizeSpec struct {
	ClusterSpecific string                   `json:"clusterSpecific"`
	ClusterInfo     ClusterConfigInfo        `json:"clusterInfo"`
	PatchType       string                   `json:"patchType,omitempty"`
	PatchJSON       []map[string]interface{} `json:"patchJson,omitempty"`
}

// ClusterConfigInfo consists of scope, Clusterprovider, ClusterName, ClusterLabel and Mode
type ClusterConfigInfo struct {
	Scope           string `json:"scope"`
	ClusterProvider string `json:"clusterProvider"`
	ClusterName     string `json:"cluster"`
	ClusterLabel    string `json:"clusterLabel"`
	Mode            string `json:"mode"`
}

type NsInstanceKey struct {
	NsId string `json:"nsId"`
}

type NsLCMOppOccKey struct {
	NsLCMOppOccId string `json:"nsLCMOppOccId"`
}

type ApiMetaData struct {
	Name        string `json:"name" bson:"name"`
	Description string `json:"description" bson:"description"`
	UserData1   string `userData1:"userData1"`
	UserData2   string `userData2:"userData2"`
}

type DeployDigData struct {
	Name                string  `json:"name"`
	Description         string  `json:"description"`
	CompositeAppName    string  `json:"compositeApp"`
	CompositeProfile    string  `json:"compositeProfile"`
	DigVersion          string  `json:"version"`
	CompositeAppVersion string  `json:"compositeAppVersion"`
	LogicalCloud        string  `json:"logicalCloud"`
	Spec                DigSpec `json:"spec"`
}

type DigSpec struct {
	ProjectName string     `json:"projectName"`
	Apps        []AppsData `json:"appsData"`
}

// AppsData This is the json payload that the orchestration API expects.
type AppsData struct {
	Metadata struct {
		Name        string `json:"name"`
		Description string `json:"description"`
		FileName    string `json:"filename"`
		FileContent string `json:"filecontent,omitempty"`
	} `json:"metadata"`
	PlacementCriterion string         `json:"placementCriterion"`
	Clusters           []ClusterInfo  `json:"clusters"`
	RsInfo             []ResourceInfo `json:"resourceData"`
}

type Day0Config struct {
	Name       string         `json:"name"`
	ConfigInfo []ResourceInfo `json:"configData"`
}

type ClusterInfo struct {
	Provider         string            `json:"clusterProvider"`
	SelectedClusters []SelectedCluster `json:"selectedClusters"`
	SelectedLabels   []SelectedLabel   `json:"selectedLabels"`
}

type SelectedCluster struct {
	Name string `json:"name"`
}

type SelectedLabel struct {
	Name string `json:"clusterLabel"`
}

// Application structure
type Application struct {
	Metadata ApiMetaData `json:"metadata" bson:"metadata"`
}

type AppsStatus struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Clusters    []struct {
		ClusterProvider string `json:"clusterProvider"`
		Cluster         string `json:"cluster"`
		Connectivity    string `json:"connectivity"`
		Resources       []struct {
			GVK struct {
				Group   string `json:"Group"`
				Version string `json:"Version"`
				Kind    string `json:"Kind"`
			} `json:"GVK"`
			Name string `json:"name"`
			//RsyncStatus    string `json:"rsyncStatus,omitempty"`
			//ClusterStatus  string `json:"clusterStatus,omitempty"`
			DeployedStatus string `json:"deployedStatus"`
		} `json:"resources"`
	} `json:"clusters"`
}

type DigStatus struct {
	Project              string `json:"project"`
	CompositeAppName     string `json:"compositeApp"`
	CompositeAppVersion  string `json:"compositeAppVersion"`
	CompositeProfileName string `json:"compositeProfile"`
	Name                 string `json:"name"`
	States               struct {
		Actions []digActions `json:"actions"`
	} `json:"states"`
	//Status      string `json:"status,omitempty"`
	DeployedStatus string `json:"deployedStatus"`
	//RsyncStatus    struct {
	//	Deleted int `json:"Deleted,omitempty"`
	//} `json:"rsyncStatus,omitempty"`
	DeployedCounts struct {
		Applied int `json:"Applied"`
	} `json:"deployedCounts"`
	//ClusterStatus struct {
	//	NotReady int `json:"NotReady,omitempty"`
	//	Ready    int `json:"Ready,omitempty"`
	//} `json:"clusterStatus,omitempty"`
	Apps          []AppsStatus `json:"apps,omitempty"`
	IsCheckedOut  bool         `json:"isCheckedOut"`
	TargetVersion string       `json:"targetVersion"`
	DigId         string       `json:"digId,omitempty"`
}

type digActions struct {
	State    string    `json:"state"`
	Instance string    `json:"instance"`
	Time     time.Time `json:"time"`
	Revision int       `json:"revision"`
}

// ProblemDetails The definition of the general \"ProblemDetails\" data structure from IETF RFC 7807 [19] is reproduced in this structure. Compared to the general framework defined in IETF RFC 7807 [19], the \"status\" and \"detail\" attributes are mandated to be included by the present document, to ensure that the response contains additional textual information about an error. IETF RFC 7807 [19] foresees extensibility of the \"ProblemDetails\" type. It is possible that particular APIs in the present document, or particular implementations, define extensions to define additional attributes that provide more information about the error. The description column only provides some explanation of the meaning to Facilitate understanding of the design. For a full description, see IETF RFC 7807 [19].
type ProblemDetails struct {
	// A URI reference according to IETF RFC 3986 [5] that identifies the problem type. It is encouraged that the URI provides human-readable documentation for the problem (e.g. using HTML) when dereferenced. When this member is not present, its value is assumed to be \"about:blank\".
	Type *string `json:"type,omitempty"`
	// A short, human-readable summary of the problem type. It should not change from occurrence to occurrence of the problem, except for purposes of localization. If type is given and other than \"about:blank\", this attribute shall also be provided. A short, human-readable summary of the problem type.  It SHOULD NOT change from occurrence to occurrence of the problem, except for purposes of localization (e.g., using proactive content negotiation; see [RFC7231], Section 3.4).
	Title *string `json:"title,omitempty"`
	// The HTTP status code for this occurrence of the problem. The HTTP status code ([RFC7231], Section 6) generated by the origin server for this occurrence of the problem.
	Status int32 `json:"status,omitempty"`
	// A human-readable explanation specific to this occurrence of the problem.
	Detail string `json:"detail,omitempty"`
	// A URI reference that identifies the specific occurrence of the problem. It may yield further information if dereferenced.
	Instance *string `json:"instance,omitempty"`
}

// NsLCMOpOcc Network Service LCM Operation Occurrence Structure
type NsLCMOpOcc struct {
	NsLCMOpOccId          string                          `json:"id" bson:"id"`
	OperationState        string                          `json:"operationState"`
	NsInstanceId          string                          `json:"nsInstanceId"`
	LcmOperationType      string                          `json:"lcmOperationType"`
	StartTime             string                          `json:"startTime"`
	StatusEnteredTime     string                          `json:"statusEnteredTime"`
	IsAutomaticInvocation bool                            `json:"isAutomaticInvocation"`
	OperationParams       interface{}                     `json:"operationParams,omitempty"`
	IsCancelPending       bool                            `json:"isCancelPending"`
	Error                 *ProblemDetails                 `json:"error,omitempty"`
	Links                 Links                           `json:"_links,omitempty"`
	AffectedVnfs          []nslcmnotification.AffectedVnf `json:"affectedVnfs,omitempty"`
}

type NsLCMOpOccInternal struct {
	NsLCMOpOccId   string `json:"id"`
	ContinueStatus string `json:"continueStatus"`
}

type NsInstanceInfo struct {
	NsdId            string              `json:"nsdId" bson:"nsdId"`
	NsdInfoId        string              `json:"nsdInfoId" bson:"nsdInfoId"`
	NsName           string              `json:"nsInstanceName" bson:"nsInstanceName"`
	NsDescription    string              `json:"nsInstanceDescription" bson:"nsInstanceDescription"`
	LogicalCloud     string              `json:"logicalCloud" bson:"logicalCloud"`
	NsId             string              `json:"id,omitempty" bson:"id,omitempty"`
	NsState          string              `json:"nsState,omitempty" bson:"nsState,omitempty"`
	NsDeployLocation string              `json:"cluster,omitempty" bson:"cluster,omitempty"`
	VnfInstance      []VnfInstance       `json:"vnfInstance" bson:"vnfInstance"`
	Links            Links               `json:"_links,omitempty" bson:"_links,omitempty"`
	NsScaleStatus    []nslcm.NsScaleInfo `json:"nsScaleStatus" bson:"nsScaleStatus,omitempty"`
}

type VnfInstance struct {
	VnfInstanceId   string `json:"id" bson:"id"`
	VnfdId          string `json:"vnfdId" bson:"vnfdId"`
	VnfInstanceName string `json:"vnfInstanceName" bson:"vnfInstanceName"`
	Description     string `json:"vnfInstanceDescription" bson:"vnfInstanceDescription"`
	State           string `json:"instantiationState" bson:"instantiationState"`
	Replica         int    `json:"replica,omitempty"`
}

type Links struct {
	Self        *Link `json:"self,omitempty"`
	NsInstance  *Link `json:"nsInstance,omitempty"`
	Instantiate *Link `json:"instantiate,omitempty"`
	Terminate   *Link `json:"terminate,omitempty"`
	Update      *Link `json:"update,omitempty"`
	Scale       *Link `json:"scale,omitempty"`
	Heal        *Link `json:"heal,omitempty"`
}

type Link struct {
	Href string `json:"href,omitempty"`
}

type NsdSpec struct {
	VnfdIds []string `json:"vnfdIds"`
}

type NsdInfoSpec struct {
	VnfdIds []string `json:"vnfdIds"`
	Id      string   `json:"Id"`
}

type NsdInfoKey struct {
	Id string `json:"Id"`
}

type NsdInfo struct {
	Metadata Metadata    `json:"metadata"`
	Spec     NsdInfoSpec `json:"spec"`
}

// CompositeApp application structure
type CompositeApp struct {
	Metadata Metadata         `json:"metadata"`
	Spec     compositeAppSpec `json:"spec"`
}

type compositeAppSpec struct {
	Version string `json:"compositeAppVersion" bson:"compositeAppVersion"`
	Id      string `json:"Id" bson:"Id"`
}

// ServiceStatusInfo status of the service and DIGs
type ServiceStatusInfo struct {
	DeployedStatus string                      `json:"deployedStatus"`
	ReadyStatus    string                      `json:"readyStatus"`
	DigsStatus     map[string]DeploymentStatus `json:"digsStatus"`
}

// DeploymentStatus is the structure used to return general status results
// for the Deployment Intent Group
type DeploymentStatus struct {
	Project              string `json:"project"`
	CompositeAppName     string `json:"compositeApp"`
	CompositeAppVersion  string `json:"compositeAppVersion"`
	CompositeProfileName string `json:"compositeProfile"`
	Name                 string `json:"name"`
	States               struct {
		Actions []digActions `json:"actions"`
	} `json:"states"`
	ReadyStatus    string `json:"readyStatus,omitempty"`
	DeployedStatus string `json:"deployedStatus"`
	DigId          string `json:"digId",omitempty`
}

type AffectedVnf struct {
	VnfInstanceId string `json:"vnfInstanceId"`
	VnfdId        string `json:"vnfdId"`
	VnfName       string `json:"vnfName"`
	ChangeType    string `json:"changeType"`
	ChangeResult  string `json:"changeResult"`
}
