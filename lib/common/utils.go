package common

import (
	"aarna.com/solsvc/db"
	fmsubscriptionLib "aarna.com/solsvc/lib/nsfmsubscription"
	nslcm "aarna.com/solsvc/lib/nslcm"
	subscriptionLib "aarna.com/solsvc/lib/nslcmsubscription"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"runtime"
	"strconv"
	"strings"
)

// HttpHandler interface manages all http related calls
type HttpHandler struct {
	Client *http.Client
}

type subscriptionKey struct {
	Id string `json:"id"`
}

func WriteError(problem nslcm.ProblemDetails, statusCode int, w http.ResponseWriter, r *http.Request) {
	problem.Status = int32(statusCode)
	problem.Instance = &r.RequestURI
	responseJson, err := json.Marshal(problem)
	if err != nil {
		log.Errorf("Error while parsing: %v", err.Error())
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	_, err = w.Write(responseJson)
	if err != nil {
		log.Errorf("Error while http write: %v", err.Error())
	}
	return
}

func (h *HttpHandler) ApiGet(url string) (interface{}, []byte, error) {
	// prepare and GET API
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return http.StatusInternalServerError, nil, err
	}
	resp, err := h.Client.Do(request)
	if err != nil {
		return http.StatusInternalServerError, nil, err
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.WithError(err).Warnf("%s(): Failed to close the reader.", PrintFunctionName())
		}
	}(resp.Body)

	// Prepare the response
	data, _ := ioutil.ReadAll(resp.Body)
	return resp.StatusCode, data, nil
}

func (h *HttpHandler) ApiDel(url string) (interface{}, []byte, error) {
	// prepare and DEL API
	request, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		return http.StatusInternalServerError, nil, err
	}
	resp, err := h.Client.Do(request)
	if err != nil {
		return resp.StatusCode, nil, err
	}
	defer resp.Body.Close()
	b, err := io.ReadAll(resp.Body)
	if err != nil {
		return http.StatusInternalServerError, nil, err
	}

	return resp.StatusCode, b, nil
}

func (h *HttpHandler) ApiPost(jsonLoad []byte, url string) (int, []byte, error) {
	// prepare and POST API
	request, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonLoad))
	if err != nil {
		return http.StatusInternalServerError, nil, err
	}
	resp, err := h.Client.Do(request)
	// Non nil error can be caused by network connectivity related
	// problems, the resp body will nil. Returning 500 for such cases.
	if err != nil {
		return http.StatusInternalServerError, nil, err
	}
	defer resp.Body.Close()
	b, err := io.ReadAll(resp.Body)
	if err != nil {
		return http.StatusInternalServerError, nil, err
	}

	return resp.StatusCode, b, nil
}

func (h *HttpHandler) ApiPostRespBody(jsonLoad []byte, url string) (int, []byte, error) {
	// prepare and POST API
	request, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonLoad))
	if err != nil {
		return http.StatusInternalServerError, nil, err
	}
	resp, err := h.Client.Do(request)
	// Non nil error can be caused by network connectivity related
	// problems, the resp body will nil. Returning 500 for such cases.
	if err != nil {
		return http.StatusInternalServerError, nil, err
	}
	defer resp.Body.Close()
	b, err := io.ReadAll(resp.Body)
	if err != nil {
		return http.StatusInternalServerError, nil, err
	}
	return resp.StatusCode, b, nil
}

func (h *HttpHandler) ApiPostMultipart(jsonLoad []byte,
	fh *multipart.FileHeader, url string) (int, []byte, error) {
	// Open the file
	var file multipart.File
	var err error
	var response []byte
	if fh != nil {
		file, err = fh.Open()
		if err != nil {
			return http.StatusInternalServerError, response, err
		}
		// Close the file later
		defer file.Close()
	}
	// Buffer to store our request body as bytes
	var requestBody bytes.Buffer
	// Create a multipart writer
	multiPartWriter := multipart.NewWriter(&requestBody)
	// Initialize the file field. Arguments are the field name and file name
	// It returns io.Writer
	var fileWriter io.Writer
	fileWriter, err = multiPartWriter.CreateFormFile("file", "file")
	if err != nil {
		return http.StatusInternalServerError, response, err
	}
	// Copy the actual file content to the field field's writer
	if file != nil {
		_, err = io.Copy(fileWriter, file)
		if err != nil {
			return http.StatusInternalServerError, response, err
		}
	}
	// Populate other fields
	fieldWriter, err := multiPartWriter.CreateFormField("metadata")
	if err != nil {
		return http.StatusInternalServerError, response, err
	}

	_, err = fieldWriter.Write(jsonLoad)
	if err != nil {
		return http.StatusInternalServerError, response, err
	}

	// We completed adding the file and the fields, let's close the multipart writer
	// So it writes the ending boundary
	multiPartWriter.Close()

	// By now our original request body should have been populated,
	// so let's just use it with our custom request
	log.Debugf("request body: %v", requestBody)
	req, err := http.NewRequest("POST", url, &requestBody)
	if err != nil {
		log.WithError(err).Errorf("%s(): Failed to create new POST request", PrintFunctionName())
		return http.StatusInternalServerError, response, err
	}
	// We need to set the content type from the writer, it includes necessary boundary as well
	req.Header.Set("Content-Type", multiPartWriter.FormDataContentType())

	// Do the request
	resp, err := h.Client.Do(req)
	if err != nil {
		log.Error(err)
		return resp.StatusCode, response, err
	}

	defer resp.Body.Close()

	response, err = io.ReadAll(resp.Body)
	if err != nil {
		return http.StatusInternalServerError, nil, err
	}

	return resp.StatusCode, response, nil
}

func FetchNSLCMOpOccInfo(nsLCMOpOccId string) (int, NsLCMOpOcc, error) {
	var key NsLCMOppOccKey
	nsLCMOpOccInfo := NsLCMOpOcc{}
	key.NsLCMOppOccId = nsLCMOpOccId

	// Check if nsInfo collection exists
	exists := db.DBconn.CheckCollectionExists(NS_COLLECTION)
	if !exists {
		log.Errorf("NsLCMOpOcc collection does not exists...")
		return http.StatusInternalServerError, nsLCMOpOccInfo, nil
	}

	// Fetch the nsInfo data using the nsInstanceId key
	values, err := db.DBconn.Find(NS_COLLECTION, key, "nslcmoppocc")
	if err != nil {
		log.Errorf("Encountered error while fetching nslcmoppocc: %s", err)
		return http.StatusInternalServerError, nsLCMOpOccInfo, err
	} else if len(values) == 0 {
		log.Infof("Unable to find nslcmoppocc matching Id: %s", key.NsLCMOppOccId)
		return http.StatusNotFound, nsLCMOpOccInfo, err
	}

	// UnMarshal the fetched nslcmoppocc data
	log.Infof("nslcmoppocc: %s", values[0])

	err = db.DBconn.Unmarshal(values[0], &nsLCMOpOccInfo)
	log.Infof("nslcmoppocc after Unmarshalling: %v", nsLCMOpOccInfo)
	if err != nil {
		log.Errorf("Unmarshalling NsInfo failed: %s", err)
		return http.StatusInternalServerError, nsLCMOpOccInfo, err
	}

	return http.StatusOK, nsLCMOpOccInfo, nil
}

func FetchNSLCMOpOccInternalInfo(nsLCMOpOccId string) (int, NsLCMOpOccInternal, error) {
	var key NsLCMOppOccKey
	data := NsLCMOpOccInternal{}
	key.NsLCMOppOccId = nsLCMOpOccId

	exists := db.DBconn.CheckCollectionExists(NS_COLLECTION)
	if !exists {
		log.Errorf("NsLCMOpOcc collection does not exists...")
		return http.StatusInternalServerError, data, nil
	}

	values, err := db.DBconn.Find(NS_COLLECTION, key, "nslcmoppoccinternal")
	if err != nil {
		log.Errorf("Encountered error while fetching nslcmoppoccinternal: %s", err)
		return http.StatusInternalServerError, data, err
	}
	if len(values) == 0 {
		log.Infof("Unable to find nslcmoppoccinternal matching Id: %s", key.NsLCMOppOccId)
		return http.StatusNotFound, data, err
	}

	err = db.DBconn.Unmarshal(values[0], &data)
	if err != nil {
		log.Errorf("Unmarshalling NsInfo failed: %s", err)
		return http.StatusInternalServerError, data, err
	}

	return http.StatusOK, data, nil
}

func FetchNSInfo(nsId string) (int, interface{}, error) {
	var key NsInstanceKey
	nsInfo := NsInstanceInfo{}
	key.NsId = nsId

	// Check if nsInfo collection exists
	exists := db.DBconn.CheckCollectionExists(NS_COLLECTION)
	if !exists {
		log.Errorf("nsInfo collection does not exists...")
		return http.StatusInternalServerError, nsInfo, errors.New("Internal error: DB collection not found")
	}

	// Fetch the nsInfo data using the nsInstanceId key
	values, err := db.DBconn.Find(NS_COLLECTION, key, "nsinfo")
	if err != nil {
		log.Errorf("Encountered error while fetching NsInfo: %s", err)
		return http.StatusInternalServerError, nsInfo, err
	} else if len(values) == 0 {
		log.Debugf("Unable to find NsInfo matching NS instance Id: %s", key.NsId)
		return http.StatusNotFound, nsInfo, errors.New("Network Service " + nsId + " does not exists")
	}

	// UnMarshal the fetched nsInfo data
	log.Debugf("NsInfo: %s", values[0])

	err = db.DBconn.Unmarshal(values[0], &nsInfo)
	log.Debugf("NsInfo after Unmarshalling: %v", nsInfo)
	if err != nil {
		log.Errorf("Unmarshalling NsInfo failed: %s", err)
		return http.StatusInternalServerError, nsInfo, err
	}

	return http.StatusOK, nsInfo, nil
}

func FetchNSAlarm(alarmId string) (int, fmsubscriptionLib.Alarm, error) {
	var key AlarmKey
	alarmInfo := fmsubscriptionLib.Alarm{}
	key.AlarmId = alarmId

	// Check if nsInfo collection exists
	exists := db.DBconn.CheckCollectionExists(NS_COLLECTION)
	if !exists {
		log.Errorf("nsInfo collection does not exists...")
		return http.StatusInternalServerError, alarmInfo, errors.New("Internal error: DB collection not found")
	}

	// Fetch the nsInfo data using the nsInstanceId key
	values, err := db.DBconn.Find(NS_COLLECTION, key, "alarm")
	if err != nil {
		log.Errorf("Encountered error while fetching NsInfo: %s", err)
		return http.StatusInternalServerError, alarmInfo, err
	} else if len(values) == 0 {
		log.Debugf("Unable to find Alarm matching alarm Id: %s", key.AlarmId)
		return http.StatusNotFound, alarmInfo, errors.New("Alarm " + alarmId + " does not exists")
	}

	// UnMarshal the fetched nsInfo data
	log.Debugf("Alarm: %s", values[0])

	err = db.DBconn.Unmarshal(values[0], &alarmInfo)
	log.Debugf("alarmInfo after Unmarshalling: %v", alarmInfo)
	if err != nil {
		log.Errorf("Unmarshalling alarmInfo failed: %s", err)
		return http.StatusInternalServerError, alarmInfo, err
	}

	return http.StatusOK, alarmInfo, nil
}

func FetchFMSubcription(subcriptionId string) (int, fmsubscriptionLib.FmSubscription, error) {
	var key subscriptionKey
	subscriptionInfo := fmsubscriptionLib.FmSubscription{}
	key.Id = subcriptionId

	// Check if nsInfo collection exists
	exists := db.DBconn.CheckCollectionExists(NS_COLLECTION)
	if !exists {
		log.Errorf("nsInfo collection does not exists...")
		return http.StatusInternalServerError, subscriptionInfo, errors.New("Internal error: DB collection not found")
	}

	// Fetch the nsInfo data using the nsInstanceId key
	values, err := db.DBconn.Find(NS_COLLECTION, key, "fm_subscription")
	if err != nil {
		log.Errorf("Encountered error while fetching subscriptionInfo: %s", err)
		return http.StatusInternalServerError, subscriptionInfo, err
	} else if len(values) == 0 {
		log.Debugf("Unable to find subscription matching subcription Id: %s", key.Id)
		return http.StatusNotFound, subscriptionInfo, errors.New("Subscription " + subcriptionId + " does not exists")
	}

	// UnMarshal the fetched nsInfo data
	log.Debugf("Subcription: %s", values[0])

	err = db.DBconn.Unmarshal(values[0], &subscriptionInfo)
	log.Debugf("subscriptionInfo after Unmarshalling: %v", subscriptionInfo)
	if err != nil {
		log.Errorf("Unmarshalling subscriptionInfo failed: %s", err)
		return http.StatusInternalServerError, subscriptionInfo, err
	}

	return http.StatusOK, subscriptionInfo, nil
}

func QueryLCMObjects(w http.ResponseWriter, r *http.Request, lcmType string) ([][]byte, int, int, int) {
	filter := r.URL.Query().Get("filter")
	marker := r.URL.Query().Get("nextpage_opaque_marker")
	log.Infof("filter is %+v", filter)

	var page int
	var err error
	var operator, key, value string
	var values [][]byte
	var skip, limit, maxLimit int

	if filter != "" {
		criteria := filter[1 : len(filter)-1]
		opList := strings.SplitN(criteria, ",", 3)
		log.Debugf("oplist len: %d", len(opList))

		if len(opList) != 3 {
			errMsg := "Invalid number of arguments in filter"
			problem := nslcm.ProblemDetails{
				Title:  &errMsg,
				Detail: err.Error(),
			}
			WriteError(problem, http.StatusInternalServerError, w, r)
			return values, skip, limit, maxLimit
		}
		operator = opList[0]
		key = opList[1]
		value = opList[2]
		log.Debugf("operator:%s, key:%s, value:%s", operator, key, value)
	}

	// Check if nsInfo collection exists
	exists := db.DBconn.CheckCollectionExists(NS_COLLECTION)
	if !exists {
		errMsg := "nsinfo collection does not exists..."
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
		}
		WriteError(problem, http.StatusInternalServerError, w, r)
		return values, skip, limit, maxLimit
	}

	// Fetch the nsInfo data using the nsInstanceId key
	values, err = db.DBconn.FindAll(NS_COLLECTION, key, operator, value, lcmType)
	if err != nil {
		errMsg := fmt.Sprintf("Encountered error while fetching %s: %s", lcmType, err)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
		}
		WriteError(problem, http.StatusInternalServerError, w, r)
		return values, skip, limit, maxLimit

	} else if len(values) == 0 {
		return values, skip, limit, maxLimit
	}

	maxLimit = len(values)
	log.Debugf("items fetched: %d", maxLimit)

	// TODO: Mongo driver skip and limit options not working, hence as of now trying to use custom logic to fetch
	// paged result
	if marker != "" {
		page, err = strconv.Atoi(marker)
		if err != nil {
			errMsg := "Encountered error while converting string to integer"
			problem := nslcm.ProblemDetails{
				Title:  &errMsg,
				Detail: err.Error(),
			}
			WriteError(problem, http.StatusInternalServerError, w, r)
		}
		skip = 5 * page
		limit = skip + 5
		if limit > maxLimit {
			limit = maxLimit
		}
		var nextlink string

		myList := strings.SplitAfter(r.RequestURI, "nextpage_opaque_marker=")
		nextpage := page + 1
		checklimit := (nextpage * 5) + 5
		log.Debugf("checklimit: %d", checklimit)
		log.Debugf("maxLimit: %d", maxLimit)
		if checklimit > maxLimit && (checklimit-maxLimit) >= 5 {
			nextlink = ""
		} else {
			nextlink = myList[0] + strconv.Itoa(nextpage) + "; " + "rel=next"
		}
		log.Debugf("nextlink: %s", nextlink)
		w.Header().Set("Link", nextlink)
	} else {
		skip = 0
		limit = maxLimit
	}
	return values, skip, limit, maxLimit
}

// ApiMultiPartUpload Creates http request for multipart upload
func (h *HttpHandler) ApiMultiPartUpload(jsonLoad []byte,
	fh *multipart.FileHeader, url string, fileNames []string, fileContents []string, vars map[string]string) ([]byte, error) {
	// Open the file
	var file multipart.File
	var err error
	if fh != nil {
		file, err = fh.Open()
		if err != nil {
			return nil, err
		}
		defer file.Close()
	}
	// Buffer to store our request body as bytes
	var requestBody bytes.Buffer
	// Create a multipart writer
	multiPartWriter := multipart.NewWriter(&requestBody)
	// Initialize the file field. Arguments are the field name and file name
	// It returns io.Writer
	for i, fileName := range fileNames {
		var fileWriter io.Writer
		if vars["multipartfiles"] != "true" {
			fileWriter, err = multiPartWriter.CreateFormFile("file", fileNames[0])
		} else {
			fileWriter, err = multiPartWriter.CreateFormFile("files", fileName)
		}
		if err != nil {
			return nil, err
		}
		// Copy the actual file content to the field field's writer
		if file != nil {
			_, err = io.Copy(fileWriter, file)
			if err != nil {
				return nil, err
			}
		} else {
			_, err = io.Copy(fileWriter, strings.NewReader(fileContents[i]))
			if err != nil {
				return nil, err
			}
		}
	}
	// Populate other fields
	fieldWriter, err := multiPartWriter.CreateFormField("metadata")
	if err != nil {
		return nil, err
	}

	_, err = fieldWriter.Write(jsonLoad)
	if err != nil {
		return nil, err
	}

	// We completed adding the file and the fields, let's close the multipart writer
	// So it writes the ending boundary
	multiPartWriter.Close()

	// By now our original request body should have been populated,
	// so let's just use it with our custom request
	log.Debugf("request body: %v", requestBody)
	req, err := http.NewRequest("POST", url, &requestBody)
	if err != nil {
		return nil, err
	}
	// We need to set the content type from the writer, it includes necessary boundary as well
	req.Header.Set("Content-Type", multiPartWriter.FormDataContentType())
	resp, err := h.Client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	return ioutil.ReadAll(resp.Body)
}

func PrintFunctionName() string {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		return "Oops!! runtime language stack couldn't determine function name"
	}
	details := runtime.FuncForPC(pc)
	if details == nil {
		return file + ":" + strconv.Itoa(line)
	}
	return file + ":" + details.Name() + ":" + strconv.Itoa(line)
}

// Update NsInfo based on currently state of DIGs
func  UpdateNsInfo(nsInfo *NsInstanceInfo, middConf string) {
	for i, vnfinstance := range nsInfo.VnfInstance {
		_, compositeapp, version, digName := FetchDIGDetails(vnfinstance.VnfInstanceName)
		digStatus, pb := FetchDigStatus(digName, compositeapp, version, middConf)
		log.Infof("digStatus: %+v", digStatus)
		if pb != nil {
			log.Errorf(pb.Detail)
			return
		}

		if strings.ToUpper(digStatus.DeployedStatus) != NS_LCM_INSTANTIATED_STATE {
			nsInfo.VnfInstance[i].State = NOT_INSTANTIATED
		} else {
			nsInfo.VnfInstance[i].State = strings.ToUpper(digStatus.DeployedStatus)
		}
	}
}

// Fetch DIG status
func  FetchDigStatus(digName string, compAppName string, version string, middConf string) (DigStatus, *nslcm.ProblemDetails) {
	var (
		errMsg string
	)

	h := HttpHandler{}
	h.Client = &http.Client{}
	thisDigStatus := DigStatus{}
	URL := "http://" + middConf + "/middleend/projects/" + DEFAULT_PROJECT +
		"/composite-apps/" + compAppName + "/" + version +
		"/deployment-intent-groups/" + digName + "/status"
	status, data, err := h.ApiGet(URL)
	if err != nil {
		errMsg = fmt.Sprintf("Encountered error while fetching DIG status for NS service: %s", err.Error())
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: status.(int32),
		}
		return thisDigStatus, &problem
	}
	if status != http.StatusOK {
		errMsg = fmt.Sprintf("Encountered error while fetching DIG status for NS service")
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: "Check status code",
			Status: http.StatusInternalServerError,
		}
		return thisDigStatus, &problem
	}

	err = json.Unmarshal(data, &thisDigStatus)
	if err != nil {
		errMsg = "Failed to unmarshal DIG json: " + PrintFunctionName()
		log.Errorf(errMsg)
		problem := nslcm.ProblemDetails{
			Title:  &errMsg,
			Detail: err.Error(),
			Status: http.StatusInternalServerError,
		}
		return thisDigStatus, &problem
	}
	return thisDigStatus, nil
}


// This function updates NsLCMOpOcc objects
func UpdateNSInstances(opType string, state string, nsLCMOpOccId string, nsId string, middConf string) {
	// Fetch NSLCMOpOccInfo from database
	retCode, nsLCMInfo, _ := FetchNSLCMOpOccInfo(nsLCMOpOccId)
	if retCode != http.StatusOK {
		errMsg := "encountered error while fetching nsLCMOppOccId"
		log.Errorf(errMsg)
		return
	}

	// Update the state of nsLCMOpOcc object
	nsLCMInfo.OperationState = state
	log.Infof("updating nsLCMOppOccInfo: %+v to %s", nsLCMInfo, state)

	// Only if state is COMPLETED, go ahead and update the NS info
	if state == LCMOPP_OCC_COMPLETED {
		// Fetch NsInfo from database
		retCode, nsInstanceInfo, _ := FetchNSInfo(nsId)
		if retCode != http.StatusOK {
			errMsg := "encountered error while fetching NSInfo"
			log.Errorf(errMsg)
			return
		}

		nsInfo, ok := nsInstanceInfo.(NsInstanceInfo)
		if !ok {
			errMsg := "Unable to parse nsInfo structure..."
			log.Errorf(errMsg)
			return
		}

		if opType == OPERATION_INSTANTIATE {
			nsInfo.NsState = NS_LCM_INSTANTIATED_STATE
			var terminateLink Link
			terminateLink.Href = nsInfo.Links.Self.Href + "/terminate"
			nsInfo.Links.Terminate = &terminateLink
			nsInfo.Links.Instantiate = nil
		} else {
			nsInfo.NsState = NOT_INSTANTIATED
			var instantiateLink Link
			instantiateLink.Href = nsInfo.Links.Self.Href + "/instantiate"
			nsInfo.Links.Instantiate = &instantiateLink
			nsInfo.Links.Terminate = nil
		}

		// Update NsInfo based on current state of DIG
		UpdateNsInfo(&nsInfo, middConf)

		key := new(NsInstanceKey)
		key.NsId = nsInfo.NsId
		err := db.DBconn.Insert(NS_COLLECTION, key, nil, "nsinfo", nsInfo)
		if err != nil {
			errMsg := fmt.Sprintf("Encountered error during insert of NS info for %s: %s", nsInfo.NsName, err)
			log.Errorf(errMsg)
			return
		}
		log.Infof("update successful for nsid: %d", nsInfo.NsId)
	}

	if state == LCMOPP_OCC_FAILED_TEMP {
		errMsg := fmt.Sprintf("Encountered error while instantiating NS service")
		problem := ProblemDetails{
			Title:  &errMsg,
			Detail: errMsg,
			Status: http.StatusInternalServerError,
		}
		nsLCMInfo.Error = &problem
	}

	var key NsLCMOppOccKey
	key.NsLCMOppOccId = nsLCMOpOccId
	err := db.DBconn.Insert(NS_COLLECTION, key, nil, "nslcmoppocc", nsLCMInfo)
	if err != nil {
		errMsg := fmt.Sprintf("Encountered error during insert of nslcmoppocc info for %s: %s", nsLCMInfo.NsLCMOpOccId, err)
		log.Errorf(errMsg)
		return
	}
	log.Infof("update successful for nsLCMOppOccInfo: %+v", nsLCMInfo)
}

func GetLCMSubcriptionAuth(subscriptionId string) (*subscriptionLib.SubscriptionAuthentication, error) {
	key := subscriptionKey{
		Id: subscriptionId,
	}

	// Fetch the nsInfo data using the nsInstanceId key
	values, err := db.DBconn.Find(NS_COLLECTION, key, "ns_subscription_auth")
	if err != nil {
		log.Errorf("Encountered error while fetching auth info: %s", err)
		return nil, err
	}
	if len(values) == 0 {
		log.Infof("Authentication info not found: %s", subscriptionId)
		return nil, nil
	}

	authInfo := &subscriptionLib.SubscriptionAuthentication{}
	err = db.DBconn.Unmarshal(values[0], authInfo)

	if err != nil {
		log.Errorf("Unmarshalling Auth info failed: %s", err)
		return nil, err
	}
	return authInfo, nil
}

func GetFMSubcriptionAuth(subscriptionId string) (*fmsubscriptionLib.SubscriptionAuthentication, error) {
	key := subscriptionKey{
		Id: subscriptionId,
	}

	// Fetch the nsInfo data using the nsInstanceId key
	values, err := db.DBconn.Find(NS_COLLECTION, key, "fm_subscription_auth")
	if err != nil {
		log.Errorf("Encountered error while fetching auth info: %s", err)
		return nil, err
	}
	if len(values) == 0 {
		log.Infof("Authentication info not found: %s", subscriptionId)
		return nil, nil
	}

	authInfo := &fmsubscriptionLib.SubscriptionAuthentication{}
	err = db.DBconn.Unmarshal(values[0], authInfo)

	if err != nil {
		log.Errorf("Unmarshalling Auth info failed: %s", err)
		return nil, err
	}
	return authInfo, nil
}


func IsValidUUID(u string) bool {
	_, err := uuid.Parse(u)
	return err == nil
}

func FetchNSDInfo(nsdId string) (int, interface{}, error) {
	var key NsdInfoKey
	nsdInfo := NsdInfo{}
	key.Id = nsdId

	// Check if nsInfo collection exists
	exists := db.DBconn.CheckCollectionExists(NS_COLLECTION)
	if !exists {
		log.Errorf("nsInfo collection does not exists...")
		return http.StatusInternalServerError, nsdInfo, errors.New("Internal error: DB collection not found")
	}

	// Fetch the nsdInfo data using the nsdId key
	values, err := db.DBconn.Find(NS_COLLECTION, key, "nsdinfo")
	if err != nil {
		log.Errorf("Encountered error while fetching NsdInfo: %s", err)
		return http.StatusInternalServerError, nsdInfo, err
	} else if len(values) == 0 {
		log.Debugf("Unable to find NsdInfo matching NS instance Id: %s", key.Id)
		return http.StatusNotFound, nsdInfo, errors.New("Network Service Descriptor " + nsdId + " does not exists")
	}

	// UnMarshal the fetched nsInfo data
	log.Debugf("NsInfo: %s", values[0])

	err = db.DBconn.Unmarshal(values[0], &nsdInfo)
	log.Debugf("NsdInfo after Unmarshalling: %v", nsdInfo)
	if err != nil {
		log.Errorf("Unmarshalling NsdInfo failed: %s", err)
		return http.StatusInternalServerError, nsdInfo, err
	}

	return http.StatusOK, nsdInfo, nil
}


func FetchDIGDetails(digId string) (string, string, string, string) {
	idParts := strings.Split(digId, ".")
	if len(idParts) == 4 {
		return idParts[0], idParts[1], idParts[2], idParts[3]
	}
	return "", "", "", ""
}
