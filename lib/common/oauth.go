package common

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"golang.org/x/oauth2"
	"io"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"
)

type TokenCacheStore map[string]oauth2.Token

type OAuth struct {
	Id       string           `json:"id"`
	Password string           `json:"password"`
	Url      string           `json:"url"`
	Cache    *TokenCacheStore `json:"cache"`
}

type OracleResponse struct {
	Token     string `json:"access_token"`
	TokenType string `json:"token_type"`
	Expiry    int    `json:"expires_in"`
}

func IntializeTokenCache() TokenCacheStore {
	m := make(map[string]oauth2.Token)
	return m
}

func (o OAuth) Token() (*oauth2.Token, error) {
	log.Debugf("Token Cache %v", *o.Cache)
	if c, ok := (*o.Cache)[o.Url]; ok {
		log.Debugf("Cache Expiry %v now %v", c.Expiry, time.Now())
		if time.Now().Before(c.Expiry) {
			log.Debugf("Token used from cache")
			return &c, nil
		}
	}
	isHttps := strings.HasPrefix(o.Url, "https")
	client := http.Client{}
	if isHttps {
		caCert, err := os.ReadFile("/opt/emco/tokenserverCA.pem")
		if err != nil {
			log.Warnf("Error while reading certificate from rootCA.crt %v", err)
			return &oauth2.Token{}, errors.Wrap(err, "Could not read certificate")
		}
		caCertPool := x509.NewCertPool()
		caCertPool.AppendCertsFromPEM(caCert)
		log.Debugf("Fetching Token: Start\n")
		client = http.Client{Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				RootCAs: caCertPool,
			}}}
	}
	form := url.Values{"grant_type": {"client_credentials"}}
	request, err := http.NewRequest(http.MethodPost, o.Url, strings.NewReader(form.Encode()))
	request.Form = form
	request.Header = http.Header{
		"Content-Type": {"application/x-www-form-urlencoded"},
	}

	request.SetBasicAuth(o.Id, o.Password)
	log.Infof("Request for accesing token %v\n", request)
	response, err := client.Do(request)
	log.Infof("Response from token server: %v, err: %v", response, err)
	if err != nil {
		log.Errorf("Error while accessing token %s\n", err.Error())
		return &oauth2.Token{}, err
	}
	if response == nil {
		log.Errorf("Error while accessing token. Response is nil\n")
		return &oauth2.Token{}, nil
	}

	if response.StatusCode != http.StatusOK {
		log.Errorf("Error while accessing token, status code: %v \n", response.StatusCode)
		return &oauth2.Token{}, err
	}

	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		log.Errorf("Error while accessing token %s\n", err.Error())
		return &oauth2.Token{}, err
	}
	oResp := OracleResponse{}
	err = json.Unmarshal(body, &oResp)
	if err != nil {
		log.Errorf("Error while decoding token %s\n", err.Error())
		return &oauth2.Token{}, err
	}
	d := time.Duration(oResp.Expiry) * time.Second
	t := oauth2.Token{
		AccessToken:  oResp.Token,
		TokenType:    oResp.TokenType,
		RefreshToken: "",
		Expiry:       time.Now().Add(d),
	}
	(*o.Cache)[o.Url] = t
	log.Debugf("Outh token: %v", t)
	log.Debugf("Token added to cache %v", *o.Cache)
	return &t, nil
}
