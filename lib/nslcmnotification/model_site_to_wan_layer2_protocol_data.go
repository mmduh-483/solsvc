/*
SOL005 - NS Lifecycle Management Notification Interface

SOL005 - NS Lifecycle Management Notification Interface  IMPORTANT: Please note that this file might be not aligned to the current version of the ETSI Group Specification it refers to. In case of discrepancies the published ETSI Group Specification takes precedence. 

API version: 2.1.0-impl:etsi.org:ETSI_NFV_OpenAPI:1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// SiteToWanLayer2ProtocolData This type provides information about Layer 2 protocol specific information for the configuration of the NFVI-PoP network gateway to enable the stitching of the intra-site VN to the MSCS over the WAN. It shall comply with the provisions defined in Table 6.5.3.85-1. NOTE:Either \"networkResources\" or \"vnSegmentsIds\" shall be provided, but not both. 
type SiteToWanLayer2ProtocolData struct {
	Layer2ConnectionInfo SiteToWanLayer2ProtocolDataLayer2ConnectionInfo `json:"layer2ConnectionInfo"`
	// Maximum Transmission Unit (MTU) that can be forwarded at layer 2 (in bytes). Default value is \"1500\" (bytes). 
	MtuL2 *float32 `json:"mtuL2,omitempty"`
	VirtualRoutingAndForwarding *SiteToWanLayer2ProtocolDataVirtualRoutingAndForwarding `json:"virtualRoutingAndForwarding,omitempty"`
	ForwardingConfig *SiteToWanLayer2ProtocolDataForwardingConfig `json:"forwardingConfig,omitempty"`
}

// NewSiteToWanLayer2ProtocolData instantiates a new SiteToWanLayer2ProtocolData object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewSiteToWanLayer2ProtocolData(layer2ConnectionInfo SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) *SiteToWanLayer2ProtocolData {
	this := SiteToWanLayer2ProtocolData{}
	this.Layer2ConnectionInfo = layer2ConnectionInfo
	return &this
}

// NewSiteToWanLayer2ProtocolDataWithDefaults instantiates a new SiteToWanLayer2ProtocolData object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewSiteToWanLayer2ProtocolDataWithDefaults() *SiteToWanLayer2ProtocolData {
	this := SiteToWanLayer2ProtocolData{}
	return &this
}

// GetLayer2ConnectionInfo returns the Layer2ConnectionInfo field value
func (o *SiteToWanLayer2ProtocolData) GetLayer2ConnectionInfo() SiteToWanLayer2ProtocolDataLayer2ConnectionInfo {
	if o == nil {
		var ret SiteToWanLayer2ProtocolDataLayer2ConnectionInfo
		return ret
	}

	return o.Layer2ConnectionInfo
}

// GetLayer2ConnectionInfoOk returns a tuple with the Layer2ConnectionInfo field value
// and a boolean to check if the value has been set.
func (o *SiteToWanLayer2ProtocolData) GetLayer2ConnectionInfoOk() (*SiteToWanLayer2ProtocolDataLayer2ConnectionInfo, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Layer2ConnectionInfo, true
}

// SetLayer2ConnectionInfo sets field value
func (o *SiteToWanLayer2ProtocolData) SetLayer2ConnectionInfo(v SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) {
	o.Layer2ConnectionInfo = v
}

// GetMtuL2 returns the MtuL2 field value if set, zero value otherwise.
func (o *SiteToWanLayer2ProtocolData) GetMtuL2() float32 {
	if o == nil || o.MtuL2 == nil {
		var ret float32
		return ret
	}
	return *o.MtuL2
}

// GetMtuL2Ok returns a tuple with the MtuL2 field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SiteToWanLayer2ProtocolData) GetMtuL2Ok() (*float32, bool) {
	if o == nil || o.MtuL2 == nil {
		return nil, false
	}
	return o.MtuL2, true
}

// HasMtuL2 returns a boolean if a field has been set.
func (o *SiteToWanLayer2ProtocolData) HasMtuL2() bool {
	if o != nil && o.MtuL2 != nil {
		return true
	}

	return false
}

// SetMtuL2 gets a reference to the given float32 and assigns it to the MtuL2 field.
func (o *SiteToWanLayer2ProtocolData) SetMtuL2(v float32) {
	o.MtuL2 = &v
}

// GetVirtualRoutingAndForwarding returns the VirtualRoutingAndForwarding field value if set, zero value otherwise.
func (o *SiteToWanLayer2ProtocolData) GetVirtualRoutingAndForwarding() SiteToWanLayer2ProtocolDataVirtualRoutingAndForwarding {
	if o == nil || o.VirtualRoutingAndForwarding == nil {
		var ret SiteToWanLayer2ProtocolDataVirtualRoutingAndForwarding
		return ret
	}
	return *o.VirtualRoutingAndForwarding
}

// GetVirtualRoutingAndForwardingOk returns a tuple with the VirtualRoutingAndForwarding field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SiteToWanLayer2ProtocolData) GetVirtualRoutingAndForwardingOk() (*SiteToWanLayer2ProtocolDataVirtualRoutingAndForwarding, bool) {
	if o == nil || o.VirtualRoutingAndForwarding == nil {
		return nil, false
	}
	return o.VirtualRoutingAndForwarding, true
}

// HasVirtualRoutingAndForwarding returns a boolean if a field has been set.
func (o *SiteToWanLayer2ProtocolData) HasVirtualRoutingAndForwarding() bool {
	if o != nil && o.VirtualRoutingAndForwarding != nil {
		return true
	}

	return false
}

// SetVirtualRoutingAndForwarding gets a reference to the given SiteToWanLayer2ProtocolDataVirtualRoutingAndForwarding and assigns it to the VirtualRoutingAndForwarding field.
func (o *SiteToWanLayer2ProtocolData) SetVirtualRoutingAndForwarding(v SiteToWanLayer2ProtocolDataVirtualRoutingAndForwarding) {
	o.VirtualRoutingAndForwarding = &v
}

// GetForwardingConfig returns the ForwardingConfig field value if set, zero value otherwise.
func (o *SiteToWanLayer2ProtocolData) GetForwardingConfig() SiteToWanLayer2ProtocolDataForwardingConfig {
	if o == nil || o.ForwardingConfig == nil {
		var ret SiteToWanLayer2ProtocolDataForwardingConfig
		return ret
	}
	return *o.ForwardingConfig
}

// GetForwardingConfigOk returns a tuple with the ForwardingConfig field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *SiteToWanLayer2ProtocolData) GetForwardingConfigOk() (*SiteToWanLayer2ProtocolDataForwardingConfig, bool) {
	if o == nil || o.ForwardingConfig == nil {
		return nil, false
	}
	return o.ForwardingConfig, true
}

// HasForwardingConfig returns a boolean if a field has been set.
func (o *SiteToWanLayer2ProtocolData) HasForwardingConfig() bool {
	if o != nil && o.ForwardingConfig != nil {
		return true
	}

	return false
}

// SetForwardingConfig gets a reference to the given SiteToWanLayer2ProtocolDataForwardingConfig and assigns it to the ForwardingConfig field.
func (o *SiteToWanLayer2ProtocolData) SetForwardingConfig(v SiteToWanLayer2ProtocolDataForwardingConfig) {
	o.ForwardingConfig = &v
}

func (o SiteToWanLayer2ProtocolData) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["layer2ConnectionInfo"] = o.Layer2ConnectionInfo
	}
	if o.MtuL2 != nil {
		toSerialize["mtuL2"] = o.MtuL2
	}
	if o.VirtualRoutingAndForwarding != nil {
		toSerialize["virtualRoutingAndForwarding"] = o.VirtualRoutingAndForwarding
	}
	if o.ForwardingConfig != nil {
		toSerialize["forwardingConfig"] = o.ForwardingConfig
	}
	return json.Marshal(toSerialize)
}

type NullableSiteToWanLayer2ProtocolData struct {
	value *SiteToWanLayer2ProtocolData
	isSet bool
}

func (v NullableSiteToWanLayer2ProtocolData) Get() *SiteToWanLayer2ProtocolData {
	return v.value
}

func (v *NullableSiteToWanLayer2ProtocolData) Set(val *SiteToWanLayer2ProtocolData) {
	v.value = val
	v.isSet = true
}

func (v NullableSiteToWanLayer2ProtocolData) IsSet() bool {
	return v.isSet
}

func (v *NullableSiteToWanLayer2ProtocolData) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableSiteToWanLayer2ProtocolData(val *SiteToWanLayer2ProtocolData) *NullableSiteToWanLayer2ProtocolData {
	return &NullableSiteToWanLayer2ProtocolData{value: val, isSet: true}
}

func (v NullableSiteToWanLayer2ProtocolData) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableSiteToWanLayer2ProtocolData) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


