/*
SOL005 - NS Lifecycle Management Notification Interface

SOL005 - NS Lifecycle Management Notification Interface  IMPORTANT: Please note that this file might be not aligned to the current version of the ETSI Group Specification it refers to. In case of discrepancies the published ETSI Group Specification takes precedence. 

API version: 2.1.0-impl:etsi.org:ETSI_NFV_OpenAPI:1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
	"time"
)

// NsLcmCapacityShortageNotification This type represents an NS LCM capacity shortage notification, which informs the receiver about resource shortage conditions during the execution of NS LCM operations. The notifications are triggered by the NFVO when a capacity shortage condition occurs during the execution of an NS LCM operation, which fails due to the resource shortage, or which succeeds despite the resource shortage because the NFVO has reduced the resource consumption of other NSs by requesting these NSs to be scaled in or terminated. The notification shall comply with the provisions defined in Table 6.5.2.19-1. The support of the notification is mandatory. This notification shall be triggered by the NFVO when there is a capacity shortage condition during the execution of an NS LCM operation which will cause the LCM operation to be not successfully completed, or which will trigger the automatic executing of an LCM operation to reduce the resource consumption of one or more NS instances to resolve a resource shortage situation. The shortage conditions include: •Necessary resources could not be allocated during an LCM operation because of resource shortage which causes   the LCM operation to fail. •An LCM operation on an NS instance with higher priority pre-empted an LCM operation on NS instance with lower   priority because of resource shortage. •An LCM operation on an NS instance with higher priority pre-empted an existing NS instance. Resources were   de-allocated from the lower priority NS instance to allow the LCM operation on a higher priority NS instance. •The resource capacity shortage situation has ended, and it can be expected that an LCM operation that had   failed could succeed now if retried.  NOTE:ETSI GS NFV-IFA 013 [x] defines further shortage situations.       These are not supported by the present version of the present document.  This notification shall also be triggered by the NFVO when a shortage condition has ended that has previously led to NS LCM operation occurrences failing. The notification shall be sent to all API consumers (OSS/BSS) that have subscribed to notifications related to capacity shortage and meeting the filter conditions of all pre-empted (low priority) and all pre-empting (high priority NS instance(s)). See ETSI GS NFV-IFA 010 [2], Annex D.2 for the use cases. The notification about the result of an unsuccessful LCM operation occurrence shall include appropriate information about the resource shortage when the cause for failure is a resource shortage. The notification where a pre-emption occurred due to e.g. a higher priority LCM operation during resource shortage shall include appropriate information about the pre-emption. NOTE:     Not all operation occurrences that are in \"FAILED_TEMP\" have been pre-empted by a resource shortage.           When the operation occurrences were pre-empted, the NS instances affected by the resource shortage end           (“status“ = \"LCM_SHORTAGE_END\") which are pointed by \"affectedNsId\" in the list of \"affectedOpOccs\" structure           can have the operations retried again (which needs a request by the OSS/BSS).  NOTE 1:The present version of the present document supports only the resource shortage status enumeration           values “LCM_RESOURCES_NOT_AVAILABLE” and “LCM_SHORTAGE_END” which represent a subset of the trigger conditions           defined in clause 8.3.5.2  of ETSI GS NFV-IFA 013 [x]. 
type NsLcmCapacityShortageNotification struct {
	// An identifier with the intention of being globally unique. 
	Id string `json:"id"`
	// Discriminator for the different notification types. Shall be set to \"NsLcmCapacityShortageNotification\" for this notification type. 
	NotificationType string `json:"notificationType"`
	// An identifier with the intention of being globally unique. 
	SubscriptionId string `json:"subscriptionId"`
	// Date-time stamp. Representation: String formatted according toas defined by the date-time production in IETF RFC 3339. 
	Timestamp time.Time `json:"timestamp"`
	// An identifier with the intention of being globally unique. 
	PreemptingNsLcmOpOccId *string `json:"preemptingNsLcmOpOccId,omitempty"`
	// An identifier with the intention of being globally unique. 
	HighPrioNsInstanceId *string `json:"highPrioNsInstanceId,omitempty"`
	// Indicates the situation of capacity shortage. Permitted values: -LCM_RESOURCES_NOT_AVAILABLE: the lifecycle operation identified by the nsLcmOpOccId attribute could not   be completed because necessary resources were not available. -LCM_SHORTAGE_END: the shortage situation which has caused the lifecycle management operation identified   by the nsLcmOpOccId attribute to fail has ended. 
	Status string `json:"status"`
	// Indicates whether this notification reports about a resource shortage or NFV-MANO capacity or performance shortage. Permitted values: -RESOURCE_SHORTAGE: the notification reports a resource shortage. Shall be present when a resources shortage situation has been identified starts and the notification is sent with “status“ = “LCM_RESOURCES_NOT_AVAILABLE“ and shall be absent otherwise. 
	ShortageType *string `json:"shortageType,omitempty"`
	// List of NS LCM operation occurrence(s) that were affected by the resource shortage. 
	AffectedOpOccs []NsLcmCapacityShortageNotificationAffectedOpOccsInner `json:"affectedOpOccs,omitempty"`
	// References to NFVI capacity information related to the shortage. 
	CapacityInformation []NsLcmCapacityShortageNotificationCapacityInformationInner `json:"capacityInformation,omitempty"`
	Links *NsLcmCapacityShortageNotificationLinks `json:"_links,omitempty"`
}

// NewNsLcmCapacityShortageNotification instantiates a new NsLcmCapacityShortageNotification object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewNsLcmCapacityShortageNotification(id string, notificationType string, subscriptionId string, timestamp time.Time, status string) *NsLcmCapacityShortageNotification {
	this := NsLcmCapacityShortageNotification{}
	this.Id = id
	this.NotificationType = notificationType
	this.SubscriptionId = subscriptionId
	this.Timestamp = timestamp
	this.Status = status
	return &this
}

// NewNsLcmCapacityShortageNotificationWithDefaults instantiates a new NsLcmCapacityShortageNotification object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewNsLcmCapacityShortageNotificationWithDefaults() *NsLcmCapacityShortageNotification {
	this := NsLcmCapacityShortageNotification{}
	return &this
}

// GetId returns the Id field value
func (o *NsLcmCapacityShortageNotification) GetId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Id
}

// GetIdOk returns a tuple with the Id field value
// and a boolean to check if the value has been set.
func (o *NsLcmCapacityShortageNotification) GetIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Id, true
}

// SetId sets field value
func (o *NsLcmCapacityShortageNotification) SetId(v string) {
	o.Id = v
}

// GetNotificationType returns the NotificationType field value
func (o *NsLcmCapacityShortageNotification) GetNotificationType() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.NotificationType
}

// GetNotificationTypeOk returns a tuple with the NotificationType field value
// and a boolean to check if the value has been set.
func (o *NsLcmCapacityShortageNotification) GetNotificationTypeOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.NotificationType, true
}

// SetNotificationType sets field value
func (o *NsLcmCapacityShortageNotification) SetNotificationType(v string) {
	o.NotificationType = v
}

// GetSubscriptionId returns the SubscriptionId field value
func (o *NsLcmCapacityShortageNotification) GetSubscriptionId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.SubscriptionId
}

// GetSubscriptionIdOk returns a tuple with the SubscriptionId field value
// and a boolean to check if the value has been set.
func (o *NsLcmCapacityShortageNotification) GetSubscriptionIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.SubscriptionId, true
}

// SetSubscriptionId sets field value
func (o *NsLcmCapacityShortageNotification) SetSubscriptionId(v string) {
	o.SubscriptionId = v
}

// GetTimestamp returns the Timestamp field value
func (o *NsLcmCapacityShortageNotification) GetTimestamp() time.Time {
	if o == nil {
		var ret time.Time
		return ret
	}

	return o.Timestamp
}

// GetTimestampOk returns a tuple with the Timestamp field value
// and a boolean to check if the value has been set.
func (o *NsLcmCapacityShortageNotification) GetTimestampOk() (*time.Time, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Timestamp, true
}

// SetTimestamp sets field value
func (o *NsLcmCapacityShortageNotification) SetTimestamp(v time.Time) {
	o.Timestamp = v
}

// GetPreemptingNsLcmOpOccId returns the PreemptingNsLcmOpOccId field value if set, zero value otherwise.
func (o *NsLcmCapacityShortageNotification) GetPreemptingNsLcmOpOccId() string {
	if o == nil || o.PreemptingNsLcmOpOccId == nil {
		var ret string
		return ret
	}
	return *o.PreemptingNsLcmOpOccId
}

// GetPreemptingNsLcmOpOccIdOk returns a tuple with the PreemptingNsLcmOpOccId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NsLcmCapacityShortageNotification) GetPreemptingNsLcmOpOccIdOk() (*string, bool) {
	if o == nil || o.PreemptingNsLcmOpOccId == nil {
		return nil, false
	}
	return o.PreemptingNsLcmOpOccId, true
}

// HasPreemptingNsLcmOpOccId returns a boolean if a field has been set.
func (o *NsLcmCapacityShortageNotification) HasPreemptingNsLcmOpOccId() bool {
	if o != nil && o.PreemptingNsLcmOpOccId != nil {
		return true
	}

	return false
}

// SetPreemptingNsLcmOpOccId gets a reference to the given string and assigns it to the PreemptingNsLcmOpOccId field.
func (o *NsLcmCapacityShortageNotification) SetPreemptingNsLcmOpOccId(v string) {
	o.PreemptingNsLcmOpOccId = &v
}

// GetHighPrioNsInstanceId returns the HighPrioNsInstanceId field value if set, zero value otherwise.
func (o *NsLcmCapacityShortageNotification) GetHighPrioNsInstanceId() string {
	if o == nil || o.HighPrioNsInstanceId == nil {
		var ret string
		return ret
	}
	return *o.HighPrioNsInstanceId
}

// GetHighPrioNsInstanceIdOk returns a tuple with the HighPrioNsInstanceId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NsLcmCapacityShortageNotification) GetHighPrioNsInstanceIdOk() (*string, bool) {
	if o == nil || o.HighPrioNsInstanceId == nil {
		return nil, false
	}
	return o.HighPrioNsInstanceId, true
}

// HasHighPrioNsInstanceId returns a boolean if a field has been set.
func (o *NsLcmCapacityShortageNotification) HasHighPrioNsInstanceId() bool {
	if o != nil && o.HighPrioNsInstanceId != nil {
		return true
	}

	return false
}

// SetHighPrioNsInstanceId gets a reference to the given string and assigns it to the HighPrioNsInstanceId field.
func (o *NsLcmCapacityShortageNotification) SetHighPrioNsInstanceId(v string) {
	o.HighPrioNsInstanceId = &v
}

// GetStatus returns the Status field value
func (o *NsLcmCapacityShortageNotification) GetStatus() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Status
}

// GetStatusOk returns a tuple with the Status field value
// and a boolean to check if the value has been set.
func (o *NsLcmCapacityShortageNotification) GetStatusOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Status, true
}

// SetStatus sets field value
func (o *NsLcmCapacityShortageNotification) SetStatus(v string) {
	o.Status = v
}

// GetShortageType returns the ShortageType field value if set, zero value otherwise.
func (o *NsLcmCapacityShortageNotification) GetShortageType() string {
	if o == nil || o.ShortageType == nil {
		var ret string
		return ret
	}
	return *o.ShortageType
}

// GetShortageTypeOk returns a tuple with the ShortageType field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NsLcmCapacityShortageNotification) GetShortageTypeOk() (*string, bool) {
	if o == nil || o.ShortageType == nil {
		return nil, false
	}
	return o.ShortageType, true
}

// HasShortageType returns a boolean if a field has been set.
func (o *NsLcmCapacityShortageNotification) HasShortageType() bool {
	if o != nil && o.ShortageType != nil {
		return true
	}

	return false
}

// SetShortageType gets a reference to the given string and assigns it to the ShortageType field.
func (o *NsLcmCapacityShortageNotification) SetShortageType(v string) {
	o.ShortageType = &v
}

// GetAffectedOpOccs returns the AffectedOpOccs field value if set, zero value otherwise.
func (o *NsLcmCapacityShortageNotification) GetAffectedOpOccs() []NsLcmCapacityShortageNotificationAffectedOpOccsInner {
	if o == nil || o.AffectedOpOccs == nil {
		var ret []NsLcmCapacityShortageNotificationAffectedOpOccsInner
		return ret
	}
	return o.AffectedOpOccs
}

// GetAffectedOpOccsOk returns a tuple with the AffectedOpOccs field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NsLcmCapacityShortageNotification) GetAffectedOpOccsOk() ([]NsLcmCapacityShortageNotificationAffectedOpOccsInner, bool) {
	if o == nil || o.AffectedOpOccs == nil {
		return nil, false
	}
	return o.AffectedOpOccs, true
}

// HasAffectedOpOccs returns a boolean if a field has been set.
func (o *NsLcmCapacityShortageNotification) HasAffectedOpOccs() bool {
	if o != nil && o.AffectedOpOccs != nil {
		return true
	}

	return false
}

// SetAffectedOpOccs gets a reference to the given []NsLcmCapacityShortageNotificationAffectedOpOccsInner and assigns it to the AffectedOpOccs field.
func (o *NsLcmCapacityShortageNotification) SetAffectedOpOccs(v []NsLcmCapacityShortageNotificationAffectedOpOccsInner) {
	o.AffectedOpOccs = v
}

// GetCapacityInformation returns the CapacityInformation field value if set, zero value otherwise.
func (o *NsLcmCapacityShortageNotification) GetCapacityInformation() []NsLcmCapacityShortageNotificationCapacityInformationInner {
	if o == nil || o.CapacityInformation == nil {
		var ret []NsLcmCapacityShortageNotificationCapacityInformationInner
		return ret
	}
	return o.CapacityInformation
}

// GetCapacityInformationOk returns a tuple with the CapacityInformation field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NsLcmCapacityShortageNotification) GetCapacityInformationOk() ([]NsLcmCapacityShortageNotificationCapacityInformationInner, bool) {
	if o == nil || o.CapacityInformation == nil {
		return nil, false
	}
	return o.CapacityInformation, true
}

// HasCapacityInformation returns a boolean if a field has been set.
func (o *NsLcmCapacityShortageNotification) HasCapacityInformation() bool {
	if o != nil && o.CapacityInformation != nil {
		return true
	}

	return false
}

// SetCapacityInformation gets a reference to the given []NsLcmCapacityShortageNotificationCapacityInformationInner and assigns it to the CapacityInformation field.
func (o *NsLcmCapacityShortageNotification) SetCapacityInformation(v []NsLcmCapacityShortageNotificationCapacityInformationInner) {
	o.CapacityInformation = v
}

// GetLinks returns the Links field value if set, zero value otherwise.
func (o *NsLcmCapacityShortageNotification) GetLinks() NsLcmCapacityShortageNotificationLinks {
	if o == nil || o.Links == nil {
		var ret NsLcmCapacityShortageNotificationLinks
		return ret
	}
	return *o.Links
}

// GetLinksOk returns a tuple with the Links field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NsLcmCapacityShortageNotification) GetLinksOk() (*NsLcmCapacityShortageNotificationLinks, bool) {
	if o == nil || o.Links == nil {
		return nil, false
	}
	return o.Links, true
}

// HasLinks returns a boolean if a field has been set.
func (o *NsLcmCapacityShortageNotification) HasLinks() bool {
	if o != nil && o.Links != nil {
		return true
	}

	return false
}

// SetLinks gets a reference to the given NsLcmCapacityShortageNotificationLinks and assigns it to the Links field.
func (o *NsLcmCapacityShortageNotification) SetLinks(v NsLcmCapacityShortageNotificationLinks) {
	o.Links = &v
}

func (o NsLcmCapacityShortageNotification) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["id"] = o.Id
	}
	if true {
		toSerialize["notificationType"] = o.NotificationType
	}
	if true {
		toSerialize["subscriptionId"] = o.SubscriptionId
	}
	if true {
		toSerialize["timestamp"] = o.Timestamp
	}
	if o.PreemptingNsLcmOpOccId != nil {
		toSerialize["preemptingNsLcmOpOccId"] = o.PreemptingNsLcmOpOccId
	}
	if o.HighPrioNsInstanceId != nil {
		toSerialize["highPrioNsInstanceId"] = o.HighPrioNsInstanceId
	}
	if true {
		toSerialize["status"] = o.Status
	}
	if o.ShortageType != nil {
		toSerialize["shortageType"] = o.ShortageType
	}
	if o.AffectedOpOccs != nil {
		toSerialize["affectedOpOccs"] = o.AffectedOpOccs
	}
	if o.CapacityInformation != nil {
		toSerialize["capacityInformation"] = o.CapacityInformation
	}
	if o.Links != nil {
		toSerialize["_links"] = o.Links
	}
	return json.Marshal(toSerialize)
}

type NullableNsLcmCapacityShortageNotification struct {
	value *NsLcmCapacityShortageNotification
	isSet bool
}

func (v NullableNsLcmCapacityShortageNotification) Get() *NsLcmCapacityShortageNotification {
	return v.value
}

func (v *NullableNsLcmCapacityShortageNotification) Set(val *NsLcmCapacityShortageNotification) {
	v.value = val
	v.isSet = true
}

func (v NullableNsLcmCapacityShortageNotification) IsSet() bool {
	return v.isSet
}

func (v *NullableNsLcmCapacityShortageNotification) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableNsLcmCapacityShortageNotification(val *NsLcmCapacityShortageNotification) *NullableNsLcmCapacityShortageNotification {
	return &NullableNsLcmCapacityShortageNotification{value: val, isSet: true}
}

func (v NullableNsLcmCapacityShortageNotification) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableNsLcmCapacityShortageNotification) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


