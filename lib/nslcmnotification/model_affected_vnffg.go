/*
SOL005 - NS Lifecycle Management Notification Interface

SOL005 - NS Lifecycle Management Notification Interface  IMPORTANT: Please note that this file might be not aligned to the current version of the ETSI Group Specification it refers to. In case of discrepancies the published ETSI Group Specification takes precedence. 

API version: 2.1.0-impl:etsi.org:ETSI_NFV_OpenAPI:1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// AffectedVnffg This type provides information about added, deleted and modified VNFFG instances. It shall comply with the provisions in Table 6.5.3.5-1. 
type AffectedVnffg struct {
	// An identifier that is unique with respect to a NS. Representation: string of variable length. 
	VnffgInstanceId string `json:"vnffgInstanceId"`
	// An identifier that is unique within a NS descriptor. Representation: string of variable length. 
	VnffgdId string `json:"vnffgdId"`
	// Signals the type of change. Permitted values: - ADD - DELETE - MODIFY 
	ChangeType *string `json:"changeType,omitempty"`
	// Signals the result of change identified by the \"changeType\" attribute. Permitted values: - COMPLETED - ROLLED_BACK - FAILED 
	ChangeResult *string `json:"changeResult,omitempty"`
}

// NewAffectedVnffg instantiates a new AffectedVnffg object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewAffectedVnffg(vnffgInstanceId string, vnffgdId string) *AffectedVnffg {
	this := AffectedVnffg{}
	this.VnffgInstanceId = vnffgInstanceId
	this.VnffgdId = vnffgdId
	return &this
}

// NewAffectedVnffgWithDefaults instantiates a new AffectedVnffg object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewAffectedVnffgWithDefaults() *AffectedVnffg {
	this := AffectedVnffg{}
	return &this
}

// GetVnffgInstanceId returns the VnffgInstanceId field value
func (o *AffectedVnffg) GetVnffgInstanceId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.VnffgInstanceId
}

// GetVnffgInstanceIdOk returns a tuple with the VnffgInstanceId field value
// and a boolean to check if the value has been set.
func (o *AffectedVnffg) GetVnffgInstanceIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.VnffgInstanceId, true
}

// SetVnffgInstanceId sets field value
func (o *AffectedVnffg) SetVnffgInstanceId(v string) {
	o.VnffgInstanceId = v
}

// GetVnffgdId returns the VnffgdId field value
func (o *AffectedVnffg) GetVnffgdId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.VnffgdId
}

// GetVnffgdIdOk returns a tuple with the VnffgdId field value
// and a boolean to check if the value has been set.
func (o *AffectedVnffg) GetVnffgdIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.VnffgdId, true
}

// SetVnffgdId sets field value
func (o *AffectedVnffg) SetVnffgdId(v string) {
	o.VnffgdId = v
}

// GetChangeType returns the ChangeType field value if set, zero value otherwise.
func (o *AffectedVnffg) GetChangeType() string {
	if o == nil || o.ChangeType == nil {
		var ret string
		return ret
	}
	return *o.ChangeType
}

// GetChangeTypeOk returns a tuple with the ChangeType field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AffectedVnffg) GetChangeTypeOk() (*string, bool) {
	if o == nil || o.ChangeType == nil {
		return nil, false
	}
	return o.ChangeType, true
}

// HasChangeType returns a boolean if a field has been set.
func (o *AffectedVnffg) HasChangeType() bool {
	if o != nil && o.ChangeType != nil {
		return true
	}

	return false
}

// SetChangeType gets a reference to the given string and assigns it to the ChangeType field.
func (o *AffectedVnffg) SetChangeType(v string) {
	o.ChangeType = &v
}

// GetChangeResult returns the ChangeResult field value if set, zero value otherwise.
func (o *AffectedVnffg) GetChangeResult() string {
	if o == nil || o.ChangeResult == nil {
		var ret string
		return ret
	}
	return *o.ChangeResult
}

// GetChangeResultOk returns a tuple with the ChangeResult field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AffectedVnffg) GetChangeResultOk() (*string, bool) {
	if o == nil || o.ChangeResult == nil {
		return nil, false
	}
	return o.ChangeResult, true
}

// HasChangeResult returns a boolean if a field has been set.
func (o *AffectedVnffg) HasChangeResult() bool {
	if o != nil && o.ChangeResult != nil {
		return true
	}

	return false
}

// SetChangeResult gets a reference to the given string and assigns it to the ChangeResult field.
func (o *AffectedVnffg) SetChangeResult(v string) {
	o.ChangeResult = &v
}

func (o AffectedVnffg) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["vnffgInstanceId"] = o.VnffgInstanceId
	}
	if true {
		toSerialize["vnffgdId"] = o.VnffgdId
	}
	if o.ChangeType != nil {
		toSerialize["changeType"] = o.ChangeType
	}
	if o.ChangeResult != nil {
		toSerialize["changeResult"] = o.ChangeResult
	}
	return json.Marshal(toSerialize)
}

type NullableAffectedVnffg struct {
	value *AffectedVnffg
	isSet bool
}

func (v NullableAffectedVnffg) Get() *AffectedVnffg {
	return v.value
}

func (v *NullableAffectedVnffg) Set(val *AffectedVnffg) {
	v.value = val
	v.isSet = true
}

func (v NullableAffectedVnffg) IsSet() bool {
	return v.isSet
}

func (v *NullableAffectedVnffg) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableAffectedVnffg(val *AffectedVnffg) *NullableAffectedVnffg {
	return &NullableAffectedVnffg{value: val, isSet: true}
}

func (v NullableAffectedVnffg) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableAffectedVnffg) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


