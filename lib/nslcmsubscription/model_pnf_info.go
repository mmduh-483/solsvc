/*
SOL005 - NS Lifecycle Management Interface

SOL005 - NS Lifecycle Management Interface  IMPORTANT: Please note that this file might be not aligned to the current version of the ETSI Group Specification it refers to. In case of discrepancies the published ETSI Group Specification takes precedence.  Please report bugs to https://forge.etsi.org/rep/nfv/SOL005/issues 

API version: 2.2.0-impl:etsi.org:ETSI_NFV_OpenAPI:1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// PnfInfo This type represents the information about a PNF that is part of an NS instance.  It shall comply with the provisions defined in Table 6.5.3.13-1. 
type PnfInfo struct {
	// An identifier with the intention of being globally unique. 
	PnfId string `json:"pnfId"`
	// Name of the PNF. 
	PnfName *string `json:"pnfName,omitempty"`
	// An identifier with the intention of being globally unique. 
	PnfdId string `json:"pnfdId"`
	// An identifier with the intention of being globally unique. 
	PnfdInfoId string `json:"pnfdInfoId"`
	// An identifier that is unique within a NS descriptor. Representation: string of variable length. 
	PnfProfileId string `json:"pnfProfileId"`
	CpInfo *PnfExtCpInfo `json:"cpInfo,omitempty"`
}

// NewPnfInfo instantiates a new PnfInfo object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewPnfInfo(pnfId string, pnfdId string, pnfdInfoId string, pnfProfileId string) *PnfInfo {
	this := PnfInfo{}
	this.PnfId = pnfId
	this.PnfdId = pnfdId
	this.PnfdInfoId = pnfdInfoId
	this.PnfProfileId = pnfProfileId
	return &this
}

// NewPnfInfoWithDefaults instantiates a new PnfInfo object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewPnfInfoWithDefaults() *PnfInfo {
	this := PnfInfo{}
	return &this
}

// GetPnfId returns the PnfId field value
func (o *PnfInfo) GetPnfId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.PnfId
}

// GetPnfIdOk returns a tuple with the PnfId field value
// and a boolean to check if the value has been set.
func (o *PnfInfo) GetPnfIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.PnfId, true
}

// SetPnfId sets field value
func (o *PnfInfo) SetPnfId(v string) {
	o.PnfId = v
}

// GetPnfName returns the PnfName field value if set, zero value otherwise.
func (o *PnfInfo) GetPnfName() string {
	if o == nil || o.PnfName == nil {
		var ret string
		return ret
	}
	return *o.PnfName
}

// GetPnfNameOk returns a tuple with the PnfName field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PnfInfo) GetPnfNameOk() (*string, bool) {
	if o == nil || o.PnfName == nil {
		return nil, false
	}
	return o.PnfName, true
}

// HasPnfName returns a boolean if a field has been set.
func (o *PnfInfo) HasPnfName() bool {
	if o != nil && o.PnfName != nil {
		return true
	}

	return false
}

// SetPnfName gets a reference to the given string and assigns it to the PnfName field.
func (o *PnfInfo) SetPnfName(v string) {
	o.PnfName = &v
}

// GetPnfdId returns the PnfdId field value
func (o *PnfInfo) GetPnfdId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.PnfdId
}

// GetPnfdIdOk returns a tuple with the PnfdId field value
// and a boolean to check if the value has been set.
func (o *PnfInfo) GetPnfdIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.PnfdId, true
}

// SetPnfdId sets field value
func (o *PnfInfo) SetPnfdId(v string) {
	o.PnfdId = v
}

// GetPnfdInfoId returns the PnfdInfoId field value
func (o *PnfInfo) GetPnfdInfoId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.PnfdInfoId
}

// GetPnfdInfoIdOk returns a tuple with the PnfdInfoId field value
// and a boolean to check if the value has been set.
func (o *PnfInfo) GetPnfdInfoIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.PnfdInfoId, true
}

// SetPnfdInfoId sets field value
func (o *PnfInfo) SetPnfdInfoId(v string) {
	o.PnfdInfoId = v
}

// GetPnfProfileId returns the PnfProfileId field value
func (o *PnfInfo) GetPnfProfileId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.PnfProfileId
}

// GetPnfProfileIdOk returns a tuple with the PnfProfileId field value
// and a boolean to check if the value has been set.
func (o *PnfInfo) GetPnfProfileIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.PnfProfileId, true
}

// SetPnfProfileId sets field value
func (o *PnfInfo) SetPnfProfileId(v string) {
	o.PnfProfileId = v
}

// GetCpInfo returns the CpInfo field value if set, zero value otherwise.
func (o *PnfInfo) GetCpInfo() PnfExtCpInfo {
	if o == nil || o.CpInfo == nil {
		var ret PnfExtCpInfo
		return ret
	}
	return *o.CpInfo
}

// GetCpInfoOk returns a tuple with the CpInfo field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PnfInfo) GetCpInfoOk() (*PnfExtCpInfo, bool) {
	if o == nil || o.CpInfo == nil {
		return nil, false
	}
	return o.CpInfo, true
}

// HasCpInfo returns a boolean if a field has been set.
func (o *PnfInfo) HasCpInfo() bool {
	if o != nil && o.CpInfo != nil {
		return true
	}

	return false
}

// SetCpInfo gets a reference to the given PnfExtCpInfo and assigns it to the CpInfo field.
func (o *PnfInfo) SetCpInfo(v PnfExtCpInfo) {
	o.CpInfo = &v
}

func (o PnfInfo) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["pnfId"] = o.PnfId
	}
	if o.PnfName != nil {
		toSerialize["pnfName"] = o.PnfName
	}
	if true {
		toSerialize["pnfdId"] = o.PnfdId
	}
	if true {
		toSerialize["pnfdInfoId"] = o.PnfdInfoId
	}
	if true {
		toSerialize["pnfProfileId"] = o.PnfProfileId
	}
	if o.CpInfo != nil {
		toSerialize["cpInfo"] = o.CpInfo
	}
	return json.Marshal(toSerialize)
}

type NullablePnfInfo struct {
	value *PnfInfo
	isSet bool
}

func (v NullablePnfInfo) Get() *PnfInfo {
	return v.value
}

func (v *NullablePnfInfo) Set(val *PnfInfo) {
	v.value = val
	v.isSet = true
}

func (v NullablePnfInfo) IsSet() bool {
	return v.isSet
}

func (v *NullablePnfInfo) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullablePnfInfo(val *PnfInfo) *NullablePnfInfo {
	return &NullablePnfInfo{value: val, isSet: true}
}

func (v NullablePnfInfo) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullablePnfInfo) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


