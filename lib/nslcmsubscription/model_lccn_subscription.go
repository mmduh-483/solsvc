/*
SOL005 - NS Lifecycle Management Interface

SOL005 - NS Lifecycle Management Interface  IMPORTANT: Please note that this file might be not aligned to the current version of the ETSI Group Specification it refers to. In case of discrepancies the published ETSI Group Specification takes precedence.  Please report bugs to https://forge.etsi.org/rep/nfv/SOL005/issues 

API version: 2.2.0-impl:etsi.org:ETSI_NFV_OpenAPI:1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// LccnSubscription This type represents a subscription related to notifications about NS lifecycle changes.  It shall comply with the provisions defined in Table 6.5.2.4-1. 
type LccnSubscription struct {
	// An identifier with the intention of being globally unique. 
	Id string `json:"id"`
	Filter *LifecycleChangeNotificationsFilter `json:"filter,omitempty"`
	// String formatted according to IETF RFC 3986. 
	CallbackUri string `json:"callbackUri"`
	Verbosity *LcmOpOccNotificationVerbosityType `json:"verbosity,omitempty"`
	Links LccnSubscriptionLinks `json:"_links"`
}

// NewLccnSubscription instantiates a new LccnSubscription object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewLccnSubscription(id string, callbackUri string, links LccnSubscriptionLinks) *LccnSubscription {
	this := LccnSubscription{}
	this.Id = id
	this.CallbackUri = callbackUri
	this.Links = links
	return &this
}

// NewLccnSubscriptionWithDefaults instantiates a new LccnSubscription object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewLccnSubscriptionWithDefaults() *LccnSubscription {
	this := LccnSubscription{}
	return &this
}

// GetId returns the Id field value
func (o *LccnSubscription) GetId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Id
}

// GetIdOk returns a tuple with the Id field value
// and a boolean to check if the value has been set.
func (o *LccnSubscription) GetIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Id, true
}

// SetId sets field value
func (o *LccnSubscription) SetId(v string) {
	o.Id = v
}

// GetFilter returns the Filter field value if set, zero value otherwise.
func (o *LccnSubscription) GetFilter() LifecycleChangeNotificationsFilter {
	if o == nil || o.Filter == nil {
		var ret LifecycleChangeNotificationsFilter
		return ret
	}
	return *o.Filter
}

// GetFilterOk returns a tuple with the Filter field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *LccnSubscription) GetFilterOk() (*LifecycleChangeNotificationsFilter, bool) {
	if o == nil || o.Filter == nil {
		return nil, false
	}
	return o.Filter, true
}

// HasFilter returns a boolean if a field has been set.
func (o *LccnSubscription) HasFilter() bool {
	if o != nil && o.Filter != nil {
		return true
	}

	return false
}

// SetFilter gets a reference to the given LifecycleChangeNotificationsFilter and assigns it to the Filter field.
func (o *LccnSubscription) SetFilter(v LifecycleChangeNotificationsFilter) {
	o.Filter = &v
}

// GetCallbackUri returns the CallbackUri field value
func (o *LccnSubscription) GetCallbackUri() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.CallbackUri
}

// GetCallbackUriOk returns a tuple with the CallbackUri field value
// and a boolean to check if the value has been set.
func (o *LccnSubscription) GetCallbackUriOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.CallbackUri, true
}

// SetCallbackUri sets field value
func (o *LccnSubscription) SetCallbackUri(v string) {
	o.CallbackUri = v
}

// GetVerbosity returns the Verbosity field value if set, zero value otherwise.
func (o *LccnSubscription) GetVerbosity() LcmOpOccNotificationVerbosityType {
	if o == nil || o.Verbosity == nil {
		var ret LcmOpOccNotificationVerbosityType
		return ret
	}
	return *o.Verbosity
}

// GetVerbosityOk returns a tuple with the Verbosity field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *LccnSubscription) GetVerbosityOk() (*LcmOpOccNotificationVerbosityType, bool) {
	if o == nil || o.Verbosity == nil {
		return nil, false
	}
	return o.Verbosity, true
}

// HasVerbosity returns a boolean if a field has been set.
func (o *LccnSubscription) HasVerbosity() bool {
	if o != nil && o.Verbosity != nil {
		return true
	}

	return false
}

// SetVerbosity gets a reference to the given LcmOpOccNotificationVerbosityType and assigns it to the Verbosity field.
func (o *LccnSubscription) SetVerbosity(v LcmOpOccNotificationVerbosityType) {
	o.Verbosity = &v
}

// GetLinks returns the Links field value
func (o *LccnSubscription) GetLinks() LccnSubscriptionLinks {
	if o == nil {
		var ret LccnSubscriptionLinks
		return ret
	}

	return o.Links
}

// GetLinksOk returns a tuple with the Links field value
// and a boolean to check if the value has been set.
func (o *LccnSubscription) GetLinksOk() (*LccnSubscriptionLinks, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Links, true
}

// SetLinks sets field value
func (o *LccnSubscription) SetLinks(v LccnSubscriptionLinks) {
	o.Links = v
}

func (o LccnSubscription) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["id"] = o.Id
	}
	if o.Filter != nil {
		toSerialize["filter"] = o.Filter
	}
	if true {
		toSerialize["callbackUri"] = o.CallbackUri
	}
	if o.Verbosity != nil {
		toSerialize["verbosity"] = o.Verbosity
	}
	if true {
		toSerialize["_links"] = o.Links
	}
	return json.Marshal(toSerialize)
}

type NullableLccnSubscription struct {
	value *LccnSubscription
	isSet bool
}

func (v NullableLccnSubscription) Get() *LccnSubscription {
	return v.value
}

func (v *NullableLccnSubscription) Set(val *LccnSubscription) {
	v.value = val
	v.isSet = true
}

func (v NullableLccnSubscription) IsSet() bool {
	return v.isSet
}

func (v *NullableLccnSubscription) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableLccnSubscription(val *LccnSubscription) *NullableLccnSubscription {
	return &NullableLccnSubscription{value: val, isSet: true}
}

func (v NullableLccnSubscription) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableLccnSubscription) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


