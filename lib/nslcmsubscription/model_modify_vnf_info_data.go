/*
SOL005 - NS Lifecycle Management Interface

SOL005 - NS Lifecycle Management Interface  IMPORTANT: Please note that this file might be not aligned to the current version of the ETSI Group Specification it refers to. In case of discrepancies the published ETSI Group Specification takes precedence.  Please report bugs to https://forge.etsi.org/rep/nfv/SOL005/issues 

API version: 2.2.0-impl:etsi.org:ETSI_NFV_OpenAPI:1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// ModifyVnfInfoData This type represents the information that is requested to be modified for a VNF instance. The information to be modified shall comply with the associated NSD. EXAMPLE. The vnfPkgId attribute value for a particular VNF instance can only be updated with a value that matches the identifier value of a VNF package whose vnfdId is present in the associated profile of the NSD. 
type ModifyVnfInfoData struct {
	// An identifier with the intention of being globally unique. 
	VnfInstanceId string `json:"vnfInstanceId"`
	// New value of the \"vnfInstanceName\" attribute in \"VnfInstance\", or \"null\" to remove the attribute. 
	VnfInstanceName *string `json:"vnfInstanceName,omitempty"`
	// New value of the \"vnfInstanceDescription\" attribute in \"VnfInstance\", or \"null\" to remove the attribute. 
	VnfInstanceDescription *string `json:"vnfInstanceDescription,omitempty"`
	// An identifier with the intention of being globally unique. 
	VnfdId *string `json:"vnfdId,omitempty"`
	// This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159. 
	VnfConfigurableProperties map[string]interface{} `json:"vnfConfigurableProperties,omitempty"`
	// This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159. 
	Metadata map[string]interface{} `json:"metadata,omitempty"`
	// This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159. 
	Extensions map[string]interface{} `json:"extensions,omitempty"`
}

// NewModifyVnfInfoData instantiates a new ModifyVnfInfoData object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewModifyVnfInfoData(vnfInstanceId string) *ModifyVnfInfoData {
	this := ModifyVnfInfoData{}
	this.VnfInstanceId = vnfInstanceId
	return &this
}

// NewModifyVnfInfoDataWithDefaults instantiates a new ModifyVnfInfoData object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewModifyVnfInfoDataWithDefaults() *ModifyVnfInfoData {
	this := ModifyVnfInfoData{}
	return &this
}

// GetVnfInstanceId returns the VnfInstanceId field value
func (o *ModifyVnfInfoData) GetVnfInstanceId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.VnfInstanceId
}

// GetVnfInstanceIdOk returns a tuple with the VnfInstanceId field value
// and a boolean to check if the value has been set.
func (o *ModifyVnfInfoData) GetVnfInstanceIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.VnfInstanceId, true
}

// SetVnfInstanceId sets field value
func (o *ModifyVnfInfoData) SetVnfInstanceId(v string) {
	o.VnfInstanceId = v
}

// GetVnfInstanceName returns the VnfInstanceName field value if set, zero value otherwise.
func (o *ModifyVnfInfoData) GetVnfInstanceName() string {
	if o == nil || o.VnfInstanceName == nil {
		var ret string
		return ret
	}
	return *o.VnfInstanceName
}

// GetVnfInstanceNameOk returns a tuple with the VnfInstanceName field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ModifyVnfInfoData) GetVnfInstanceNameOk() (*string, bool) {
	if o == nil || o.VnfInstanceName == nil {
		return nil, false
	}
	return o.VnfInstanceName, true
}

// HasVnfInstanceName returns a boolean if a field has been set.
func (o *ModifyVnfInfoData) HasVnfInstanceName() bool {
	if o != nil && o.VnfInstanceName != nil {
		return true
	}

	return false
}

// SetVnfInstanceName gets a reference to the given string and assigns it to the VnfInstanceName field.
func (o *ModifyVnfInfoData) SetVnfInstanceName(v string) {
	o.VnfInstanceName = &v
}

// GetVnfInstanceDescription returns the VnfInstanceDescription field value if set, zero value otherwise.
func (o *ModifyVnfInfoData) GetVnfInstanceDescription() string {
	if o == nil || o.VnfInstanceDescription == nil {
		var ret string
		return ret
	}
	return *o.VnfInstanceDescription
}

// GetVnfInstanceDescriptionOk returns a tuple with the VnfInstanceDescription field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ModifyVnfInfoData) GetVnfInstanceDescriptionOk() (*string, bool) {
	if o == nil || o.VnfInstanceDescription == nil {
		return nil, false
	}
	return o.VnfInstanceDescription, true
}

// HasVnfInstanceDescription returns a boolean if a field has been set.
func (o *ModifyVnfInfoData) HasVnfInstanceDescription() bool {
	if o != nil && o.VnfInstanceDescription != nil {
		return true
	}

	return false
}

// SetVnfInstanceDescription gets a reference to the given string and assigns it to the VnfInstanceDescription field.
func (o *ModifyVnfInfoData) SetVnfInstanceDescription(v string) {
	o.VnfInstanceDescription = &v
}

// GetVnfdId returns the VnfdId field value if set, zero value otherwise.
func (o *ModifyVnfInfoData) GetVnfdId() string {
	if o == nil || o.VnfdId == nil {
		var ret string
		return ret
	}
	return *o.VnfdId
}

// GetVnfdIdOk returns a tuple with the VnfdId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ModifyVnfInfoData) GetVnfdIdOk() (*string, bool) {
	if o == nil || o.VnfdId == nil {
		return nil, false
	}
	return o.VnfdId, true
}

// HasVnfdId returns a boolean if a field has been set.
func (o *ModifyVnfInfoData) HasVnfdId() bool {
	if o != nil && o.VnfdId != nil {
		return true
	}

	return false
}

// SetVnfdId gets a reference to the given string and assigns it to the VnfdId field.
func (o *ModifyVnfInfoData) SetVnfdId(v string) {
	o.VnfdId = &v
}

// GetVnfConfigurableProperties returns the VnfConfigurableProperties field value if set, zero value otherwise.
func (o *ModifyVnfInfoData) GetVnfConfigurableProperties() map[string]interface{} {
	if o == nil || o.VnfConfigurableProperties == nil {
		var ret map[string]interface{}
		return ret
	}
	return o.VnfConfigurableProperties
}

// GetVnfConfigurablePropertiesOk returns a tuple with the VnfConfigurableProperties field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ModifyVnfInfoData) GetVnfConfigurablePropertiesOk() (map[string]interface{}, bool) {
	if o == nil || o.VnfConfigurableProperties == nil {
		return nil, false
	}
	return o.VnfConfigurableProperties, true
}

// HasVnfConfigurableProperties returns a boolean if a field has been set.
func (o *ModifyVnfInfoData) HasVnfConfigurableProperties() bool {
	if o != nil && o.VnfConfigurableProperties != nil {
		return true
	}

	return false
}

// SetVnfConfigurableProperties gets a reference to the given map[string]interface{} and assigns it to the VnfConfigurableProperties field.
func (o *ModifyVnfInfoData) SetVnfConfigurableProperties(v map[string]interface{}) {
	o.VnfConfigurableProperties = v
}

// GetMetadata returns the Metadata field value if set, zero value otherwise.
func (o *ModifyVnfInfoData) GetMetadata() map[string]interface{} {
	if o == nil || o.Metadata == nil {
		var ret map[string]interface{}
		return ret
	}
	return o.Metadata
}

// GetMetadataOk returns a tuple with the Metadata field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ModifyVnfInfoData) GetMetadataOk() (map[string]interface{}, bool) {
	if o == nil || o.Metadata == nil {
		return nil, false
	}
	return o.Metadata, true
}

// HasMetadata returns a boolean if a field has been set.
func (o *ModifyVnfInfoData) HasMetadata() bool {
	if o != nil && o.Metadata != nil {
		return true
	}

	return false
}

// SetMetadata gets a reference to the given map[string]interface{} and assigns it to the Metadata field.
func (o *ModifyVnfInfoData) SetMetadata(v map[string]interface{}) {
	o.Metadata = v
}

// GetExtensions returns the Extensions field value if set, zero value otherwise.
func (o *ModifyVnfInfoData) GetExtensions() map[string]interface{} {
	if o == nil || o.Extensions == nil {
		var ret map[string]interface{}
		return ret
	}
	return o.Extensions
}

// GetExtensionsOk returns a tuple with the Extensions field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ModifyVnfInfoData) GetExtensionsOk() (map[string]interface{}, bool) {
	if o == nil || o.Extensions == nil {
		return nil, false
	}
	return o.Extensions, true
}

// HasExtensions returns a boolean if a field has been set.
func (o *ModifyVnfInfoData) HasExtensions() bool {
	if o != nil && o.Extensions != nil {
		return true
	}

	return false
}

// SetExtensions gets a reference to the given map[string]interface{} and assigns it to the Extensions field.
func (o *ModifyVnfInfoData) SetExtensions(v map[string]interface{}) {
	o.Extensions = v
}

func (o ModifyVnfInfoData) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["vnfInstanceId"] = o.VnfInstanceId
	}
	if o.VnfInstanceName != nil {
		toSerialize["vnfInstanceName"] = o.VnfInstanceName
	}
	if o.VnfInstanceDescription != nil {
		toSerialize["vnfInstanceDescription"] = o.VnfInstanceDescription
	}
	if o.VnfdId != nil {
		toSerialize["vnfdId"] = o.VnfdId
	}
	if o.VnfConfigurableProperties != nil {
		toSerialize["vnfConfigurableProperties"] = o.VnfConfigurableProperties
	}
	if o.Metadata != nil {
		toSerialize["metadata"] = o.Metadata
	}
	if o.Extensions != nil {
		toSerialize["extensions"] = o.Extensions
	}
	return json.Marshal(toSerialize)
}

type NullableModifyVnfInfoData struct {
	value *ModifyVnfInfoData
	isSet bool
}

func (v NullableModifyVnfInfoData) Get() *ModifyVnfInfoData {
	return v.value
}

func (v *NullableModifyVnfInfoData) Set(val *ModifyVnfInfoData) {
	v.value = val
	v.isSet = true
}

func (v NullableModifyVnfInfoData) IsSet() bool {
	return v.isSet
}

func (v *NullableModifyVnfInfoData) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableModifyVnfInfoData(val *ModifyVnfInfoData) *NullableModifyVnfInfoData {
	return &NullableModifyVnfInfoData{value: val, isSet: true}
}

func (v NullableModifyVnfInfoData) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableModifyVnfInfoData) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


