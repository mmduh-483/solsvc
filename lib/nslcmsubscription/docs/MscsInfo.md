# MscsInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MscsId** | **string** | An identifier with the intention of being globally unique.  | 
**MscsName** | Pointer to **string** | Human readable name of the MSCS.  | [optional] 
**MscsDescription** | Pointer to **string** | Human readable description of the MSCS.  | [optional] 
**MscsType** | **string** | The type of connectivity that is provided to the virtualized networks in the NFVI-PoP and characterizes the connectivity service across the WAN. Permitted values: - L2 - L3  | 
**MscsLayerProtocol** | Pointer to **string** | Type of underlying connectivity service and protocol associated to the MSCS. Permitted values are as listed below and restricted by the type of MSCS: - EVPN_BGP_MPLS: as specified in IETF RFC 7432. Only applicable for mscsType&#x3D;\&quot;L2\&quot;. - EVPN_VPWS: as specified in IETF RFC 8214. Only applicable for mscsType&#x3D;\&quot;L2\&quot;. - VPLS_BGP: as specified in IETF RFC 4761 and IETF RFC 6624. Only applicable for mscsType&#x3D;\&quot;L2\&quot;. - VPLS_LDP_L2TP: as specified in IETF RFC 4762. Only applicable for mscsType&#x3D;\&quot;L2\&quot;. - VPWS_LDP_L2TP: as specified in IETF RFC 6074. Only applicable for mscsType&#x3D;\&quot;L2\&quot;. - BGP_IP_VPN: BGP/MPLS based IP VPN as specified in IETF RFC 4364. Only applicable for mscsType&#x3D;\&quot;L3\&quot;.  | [optional] 
**SiteAccessProtectionSchemes** | Pointer to [**[]MscsInfoSiteAccessProtectionSchemesInner**](MscsInfoSiteAccessProtectionSchemesInner.md) | Information to determine the proper MSCS endpoints configuration to fulfil certain resiliency/protection requirements, e.g., by considering certain availability and redundancy of connectivity service endpoints in between NFVI-PoP and WAN.  | [optional] 
**MtuMscs** | Pointer to **float32** | Maximum Transmission Unit (MTU) that can be forwarded over the MSCS (in bytes). Default value is \&quot;1500\&quot; (bytes).  | [optional] 
**MscsEndpoints** | Pointer to [**[]MscsEndpointInfo**](MscsEndpointInfo.md) | Information about the MSCS endpoints of the MSCS.  | [optional] 

## Methods

### NewMscsInfo

`func NewMscsInfo(mscsId string, mscsType string, ) *MscsInfo`

NewMscsInfo instantiates a new MscsInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMscsInfoWithDefaults

`func NewMscsInfoWithDefaults() *MscsInfo`

NewMscsInfoWithDefaults instantiates a new MscsInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMscsId

`func (o *MscsInfo) GetMscsId() string`

GetMscsId returns the MscsId field if non-nil, zero value otherwise.

### GetMscsIdOk

`func (o *MscsInfo) GetMscsIdOk() (*string, bool)`

GetMscsIdOk returns a tuple with the MscsId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMscsId

`func (o *MscsInfo) SetMscsId(v string)`

SetMscsId sets MscsId field to given value.


### GetMscsName

`func (o *MscsInfo) GetMscsName() string`

GetMscsName returns the MscsName field if non-nil, zero value otherwise.

### GetMscsNameOk

`func (o *MscsInfo) GetMscsNameOk() (*string, bool)`

GetMscsNameOk returns a tuple with the MscsName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMscsName

`func (o *MscsInfo) SetMscsName(v string)`

SetMscsName sets MscsName field to given value.

### HasMscsName

`func (o *MscsInfo) HasMscsName() bool`

HasMscsName returns a boolean if a field has been set.

### GetMscsDescription

`func (o *MscsInfo) GetMscsDescription() string`

GetMscsDescription returns the MscsDescription field if non-nil, zero value otherwise.

### GetMscsDescriptionOk

`func (o *MscsInfo) GetMscsDescriptionOk() (*string, bool)`

GetMscsDescriptionOk returns a tuple with the MscsDescription field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMscsDescription

`func (o *MscsInfo) SetMscsDescription(v string)`

SetMscsDescription sets MscsDescription field to given value.

### HasMscsDescription

`func (o *MscsInfo) HasMscsDescription() bool`

HasMscsDescription returns a boolean if a field has been set.

### GetMscsType

`func (o *MscsInfo) GetMscsType() string`

GetMscsType returns the MscsType field if non-nil, zero value otherwise.

### GetMscsTypeOk

`func (o *MscsInfo) GetMscsTypeOk() (*string, bool)`

GetMscsTypeOk returns a tuple with the MscsType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMscsType

`func (o *MscsInfo) SetMscsType(v string)`

SetMscsType sets MscsType field to given value.


### GetMscsLayerProtocol

`func (o *MscsInfo) GetMscsLayerProtocol() string`

GetMscsLayerProtocol returns the MscsLayerProtocol field if non-nil, zero value otherwise.

### GetMscsLayerProtocolOk

`func (o *MscsInfo) GetMscsLayerProtocolOk() (*string, bool)`

GetMscsLayerProtocolOk returns a tuple with the MscsLayerProtocol field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMscsLayerProtocol

`func (o *MscsInfo) SetMscsLayerProtocol(v string)`

SetMscsLayerProtocol sets MscsLayerProtocol field to given value.

### HasMscsLayerProtocol

`func (o *MscsInfo) HasMscsLayerProtocol() bool`

HasMscsLayerProtocol returns a boolean if a field has been set.

### GetSiteAccessProtectionSchemes

`func (o *MscsInfo) GetSiteAccessProtectionSchemes() []MscsInfoSiteAccessProtectionSchemesInner`

GetSiteAccessProtectionSchemes returns the SiteAccessProtectionSchemes field if non-nil, zero value otherwise.

### GetSiteAccessProtectionSchemesOk

`func (o *MscsInfo) GetSiteAccessProtectionSchemesOk() (*[]MscsInfoSiteAccessProtectionSchemesInner, bool)`

GetSiteAccessProtectionSchemesOk returns a tuple with the SiteAccessProtectionSchemes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSiteAccessProtectionSchemes

`func (o *MscsInfo) SetSiteAccessProtectionSchemes(v []MscsInfoSiteAccessProtectionSchemesInner)`

SetSiteAccessProtectionSchemes sets SiteAccessProtectionSchemes field to given value.

### HasSiteAccessProtectionSchemes

`func (o *MscsInfo) HasSiteAccessProtectionSchemes() bool`

HasSiteAccessProtectionSchemes returns a boolean if a field has been set.

### GetMtuMscs

`func (o *MscsInfo) GetMtuMscs() float32`

GetMtuMscs returns the MtuMscs field if non-nil, zero value otherwise.

### GetMtuMscsOk

`func (o *MscsInfo) GetMtuMscsOk() (*float32, bool)`

GetMtuMscsOk returns a tuple with the MtuMscs field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMtuMscs

`func (o *MscsInfo) SetMtuMscs(v float32)`

SetMtuMscs sets MtuMscs field to given value.

### HasMtuMscs

`func (o *MscsInfo) HasMtuMscs() bool`

HasMtuMscs returns a boolean if a field has been set.

### GetMscsEndpoints

`func (o *MscsInfo) GetMscsEndpoints() []MscsEndpointInfo`

GetMscsEndpoints returns the MscsEndpoints field if non-nil, zero value otherwise.

### GetMscsEndpointsOk

`func (o *MscsInfo) GetMscsEndpointsOk() (*[]MscsEndpointInfo, bool)`

GetMscsEndpointsOk returns a tuple with the MscsEndpoints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMscsEndpoints

`func (o *MscsInfo) SetMscsEndpoints(v []MscsEndpointInfo)`

SetMscsEndpoints sets MscsEndpoints field to given value.

### HasMscsEndpoints

`func (o *MscsInfo) HasMscsEndpoints() bool`

HasMscsEndpoints returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


