# OperateVnfData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VnfInstanceId** | **string** | An identifier with the intention of being globally unique.  | 
**ChangeStateTo** | [**OperationalStates**](OperationalStates.md) |  | 
**StopType** | Pointer to [**StopType**](StopType.md) |  | [optional] 
**GracefulStopTimeout** | Pointer to **int32** | The time interval (in seconds) to wait for the VNF to be taken out of service during graceful stop, before stopping the VNF. See note.  | [optional] 
**AdditionalParam** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 

## Methods

### NewOperateVnfData

`func NewOperateVnfData(vnfInstanceId string, changeStateTo OperationalStates, ) *OperateVnfData`

NewOperateVnfData instantiates a new OperateVnfData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewOperateVnfDataWithDefaults

`func NewOperateVnfDataWithDefaults() *OperateVnfData`

NewOperateVnfDataWithDefaults instantiates a new OperateVnfData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVnfInstanceId

`func (o *OperateVnfData) GetVnfInstanceId() string`

GetVnfInstanceId returns the VnfInstanceId field if non-nil, zero value otherwise.

### GetVnfInstanceIdOk

`func (o *OperateVnfData) GetVnfInstanceIdOk() (*string, bool)`

GetVnfInstanceIdOk returns a tuple with the VnfInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceId

`func (o *OperateVnfData) SetVnfInstanceId(v string)`

SetVnfInstanceId sets VnfInstanceId field to given value.


### GetChangeStateTo

`func (o *OperateVnfData) GetChangeStateTo() OperationalStates`

GetChangeStateTo returns the ChangeStateTo field if non-nil, zero value otherwise.

### GetChangeStateToOk

`func (o *OperateVnfData) GetChangeStateToOk() (*OperationalStates, bool)`

GetChangeStateToOk returns a tuple with the ChangeStateTo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChangeStateTo

`func (o *OperateVnfData) SetChangeStateTo(v OperationalStates)`

SetChangeStateTo sets ChangeStateTo field to given value.


### GetStopType

`func (o *OperateVnfData) GetStopType() StopType`

GetStopType returns the StopType field if non-nil, zero value otherwise.

### GetStopTypeOk

`func (o *OperateVnfData) GetStopTypeOk() (*StopType, bool)`

GetStopTypeOk returns a tuple with the StopType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStopType

`func (o *OperateVnfData) SetStopType(v StopType)`

SetStopType sets StopType field to given value.

### HasStopType

`func (o *OperateVnfData) HasStopType() bool`

HasStopType returns a boolean if a field has been set.

### GetGracefulStopTimeout

`func (o *OperateVnfData) GetGracefulStopTimeout() int32`

GetGracefulStopTimeout returns the GracefulStopTimeout field if non-nil, zero value otherwise.

### GetGracefulStopTimeoutOk

`func (o *OperateVnfData) GetGracefulStopTimeoutOk() (*int32, bool)`

GetGracefulStopTimeoutOk returns a tuple with the GracefulStopTimeout field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGracefulStopTimeout

`func (o *OperateVnfData) SetGracefulStopTimeout(v int32)`

SetGracefulStopTimeout sets GracefulStopTimeout field to given value.

### HasGracefulStopTimeout

`func (o *OperateVnfData) HasGracefulStopTimeout() bool`

HasGracefulStopTimeout returns a boolean if a field has been set.

### GetAdditionalParam

`func (o *OperateVnfData) GetAdditionalParam() map[string]interface{}`

GetAdditionalParam returns the AdditionalParam field if non-nil, zero value otherwise.

### GetAdditionalParamOk

`func (o *OperateVnfData) GetAdditionalParamOk() (*map[string]interface{}, bool)`

GetAdditionalParamOk returns a tuple with the AdditionalParam field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdditionalParam

`func (o *OperateVnfData) SetAdditionalParam(v map[string]interface{})`

SetAdditionalParam sets AdditionalParam field to given value.

### HasAdditionalParam

`func (o *OperateVnfData) HasAdditionalParam() bool`

HasAdditionalParam returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


