# NestedNsLocationConstraint

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NsProfileId** | **string** | An identifier that is unique with respect to a NS. Representation: string of variable length.  | 
**LocationConstraints** | [**LocationConstraints**](LocationConstraints.md) |  | 

## Methods

### NewNestedNsLocationConstraint

`func NewNestedNsLocationConstraint(nsProfileId string, locationConstraints LocationConstraints, ) *NestedNsLocationConstraint`

NewNestedNsLocationConstraint instantiates a new NestedNsLocationConstraint object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNestedNsLocationConstraintWithDefaults

`func NewNestedNsLocationConstraintWithDefaults() *NestedNsLocationConstraint`

NewNestedNsLocationConstraintWithDefaults instantiates a new NestedNsLocationConstraint object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetNsProfileId

`func (o *NestedNsLocationConstraint) GetNsProfileId() string`

GetNsProfileId returns the NsProfileId field if non-nil, zero value otherwise.

### GetNsProfileIdOk

`func (o *NestedNsLocationConstraint) GetNsProfileIdOk() (*string, bool)`

GetNsProfileIdOk returns a tuple with the NsProfileId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsProfileId

`func (o *NestedNsLocationConstraint) SetNsProfileId(v string)`

SetNsProfileId sets NsProfileId field to given value.


### GetLocationConstraints

`func (o *NestedNsLocationConstraint) GetLocationConstraints() LocationConstraints`

GetLocationConstraints returns the LocationConstraints field if non-nil, zero value otherwise.

### GetLocationConstraintsOk

`func (o *NestedNsLocationConstraint) GetLocationConstraintsOk() (*LocationConstraints, bool)`

GetLocationConstraintsOk returns a tuple with the LocationConstraints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocationConstraints

`func (o *NestedNsLocationConstraint) SetLocationConstraints(v LocationConstraints)`

SetLocationConstraints sets LocationConstraints field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


