# SiteToWanLayer2ProtocolDataForwardingConfig

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NetworkResources** | Pointer to [**[]ResourceHandle**](ResourceHandle.md) | Reference to the VN resource to be forwarded into/from the MSCS. See note.  | [optional] 
**VnSegmentIds** | Pointer to [**SiteToWanLayer2ProtocolDataForwardingConfigVnSegmentIds**](SiteToWanLayer2ProtocolDataForwardingConfigVnSegmentIds.md) |  | [optional] 

## Methods

### NewSiteToWanLayer2ProtocolDataForwardingConfig

`func NewSiteToWanLayer2ProtocolDataForwardingConfig() *SiteToWanLayer2ProtocolDataForwardingConfig`

NewSiteToWanLayer2ProtocolDataForwardingConfig instantiates a new SiteToWanLayer2ProtocolDataForwardingConfig object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSiteToWanLayer2ProtocolDataForwardingConfigWithDefaults

`func NewSiteToWanLayer2ProtocolDataForwardingConfigWithDefaults() *SiteToWanLayer2ProtocolDataForwardingConfig`

NewSiteToWanLayer2ProtocolDataForwardingConfigWithDefaults instantiates a new SiteToWanLayer2ProtocolDataForwardingConfig object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetNetworkResources

`func (o *SiteToWanLayer2ProtocolDataForwardingConfig) GetNetworkResources() []ResourceHandle`

GetNetworkResources returns the NetworkResources field if non-nil, zero value otherwise.

### GetNetworkResourcesOk

`func (o *SiteToWanLayer2ProtocolDataForwardingConfig) GetNetworkResourcesOk() (*[]ResourceHandle, bool)`

GetNetworkResourcesOk returns a tuple with the NetworkResources field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNetworkResources

`func (o *SiteToWanLayer2ProtocolDataForwardingConfig) SetNetworkResources(v []ResourceHandle)`

SetNetworkResources sets NetworkResources field to given value.

### HasNetworkResources

`func (o *SiteToWanLayer2ProtocolDataForwardingConfig) HasNetworkResources() bool`

HasNetworkResources returns a boolean if a field has been set.

### GetVnSegmentIds

`func (o *SiteToWanLayer2ProtocolDataForwardingConfig) GetVnSegmentIds() SiteToWanLayer2ProtocolDataForwardingConfigVnSegmentIds`

GetVnSegmentIds returns the VnSegmentIds field if non-nil, zero value otherwise.

### GetVnSegmentIdsOk

`func (o *SiteToWanLayer2ProtocolDataForwardingConfig) GetVnSegmentIdsOk() (*SiteToWanLayer2ProtocolDataForwardingConfigVnSegmentIds, bool)`

GetVnSegmentIdsOk returns a tuple with the VnSegmentIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnSegmentIds

`func (o *SiteToWanLayer2ProtocolDataForwardingConfig) SetVnSegmentIds(v SiteToWanLayer2ProtocolDataForwardingConfigVnSegmentIds)`

SetVnSegmentIds sets VnSegmentIds field to given value.

### HasVnSegmentIds

`func (o *SiteToWanLayer2ProtocolDataForwardingConfig) HasVnSegmentIds() bool`

HasVnSegmentIds returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


