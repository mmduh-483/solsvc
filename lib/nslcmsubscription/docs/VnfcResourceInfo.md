# VnfcResourceInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | An identifier that is unique for the respective type within a VNF instance, but may not be globally unique.  | 
**VnfdId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**VduId** | **string** | Identifier of the VNF Virtual Link Descriptor (VLD) in the VNFD.  | 
**ComputeResource** | [**ResourceHandle**](ResourceHandle.md) |  | 
**StorageResourceIds** | Pointer to **[]string** | References to the VirtualStorage resources. The value refers to a VirtualStorageResourceInfo item in the VnfInstance.  | [optional] 
**ReservationId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**VnfcCpInfo** | Pointer to [**[]VnfcResourceInfoVnfcCpInfoInner**](VnfcResourceInfoVnfcCpInfoInner.md) | CPs of the VNFC instance. Shall be present when that particular CP of the VNFC instance is exposed as an external CP of the VNF instance or is connected to an external CP of the VNF instance.See note 2. May be present otherwise.  | [optional] 
**Metadata** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 

## Methods

### NewVnfcResourceInfo

`func NewVnfcResourceInfo(id string, vduId string, computeResource ResourceHandle, ) *VnfcResourceInfo`

NewVnfcResourceInfo instantiates a new VnfcResourceInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVnfcResourceInfoWithDefaults

`func NewVnfcResourceInfoWithDefaults() *VnfcResourceInfo`

NewVnfcResourceInfoWithDefaults instantiates a new VnfcResourceInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *VnfcResourceInfo) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *VnfcResourceInfo) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *VnfcResourceInfo) SetId(v string)`

SetId sets Id field to given value.


### GetVnfdId

`func (o *VnfcResourceInfo) GetVnfdId() string`

GetVnfdId returns the VnfdId field if non-nil, zero value otherwise.

### GetVnfdIdOk

`func (o *VnfcResourceInfo) GetVnfdIdOk() (*string, bool)`

GetVnfdIdOk returns a tuple with the VnfdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfdId

`func (o *VnfcResourceInfo) SetVnfdId(v string)`

SetVnfdId sets VnfdId field to given value.

### HasVnfdId

`func (o *VnfcResourceInfo) HasVnfdId() bool`

HasVnfdId returns a boolean if a field has been set.

### GetVduId

`func (o *VnfcResourceInfo) GetVduId() string`

GetVduId returns the VduId field if non-nil, zero value otherwise.

### GetVduIdOk

`func (o *VnfcResourceInfo) GetVduIdOk() (*string, bool)`

GetVduIdOk returns a tuple with the VduId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVduId

`func (o *VnfcResourceInfo) SetVduId(v string)`

SetVduId sets VduId field to given value.


### GetComputeResource

`func (o *VnfcResourceInfo) GetComputeResource() ResourceHandle`

GetComputeResource returns the ComputeResource field if non-nil, zero value otherwise.

### GetComputeResourceOk

`func (o *VnfcResourceInfo) GetComputeResourceOk() (*ResourceHandle, bool)`

GetComputeResourceOk returns a tuple with the ComputeResource field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetComputeResource

`func (o *VnfcResourceInfo) SetComputeResource(v ResourceHandle)`

SetComputeResource sets ComputeResource field to given value.


### GetStorageResourceIds

`func (o *VnfcResourceInfo) GetStorageResourceIds() []string`

GetStorageResourceIds returns the StorageResourceIds field if non-nil, zero value otherwise.

### GetStorageResourceIdsOk

`func (o *VnfcResourceInfo) GetStorageResourceIdsOk() (*[]string, bool)`

GetStorageResourceIdsOk returns a tuple with the StorageResourceIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStorageResourceIds

`func (o *VnfcResourceInfo) SetStorageResourceIds(v []string)`

SetStorageResourceIds sets StorageResourceIds field to given value.

### HasStorageResourceIds

`func (o *VnfcResourceInfo) HasStorageResourceIds() bool`

HasStorageResourceIds returns a boolean if a field has been set.

### GetReservationId

`func (o *VnfcResourceInfo) GetReservationId() string`

GetReservationId returns the ReservationId field if non-nil, zero value otherwise.

### GetReservationIdOk

`func (o *VnfcResourceInfo) GetReservationIdOk() (*string, bool)`

GetReservationIdOk returns a tuple with the ReservationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReservationId

`func (o *VnfcResourceInfo) SetReservationId(v string)`

SetReservationId sets ReservationId field to given value.

### HasReservationId

`func (o *VnfcResourceInfo) HasReservationId() bool`

HasReservationId returns a boolean if a field has been set.

### GetVnfcCpInfo

`func (o *VnfcResourceInfo) GetVnfcCpInfo() []VnfcResourceInfoVnfcCpInfoInner`

GetVnfcCpInfo returns the VnfcCpInfo field if non-nil, zero value otherwise.

### GetVnfcCpInfoOk

`func (o *VnfcResourceInfo) GetVnfcCpInfoOk() (*[]VnfcResourceInfoVnfcCpInfoInner, bool)`

GetVnfcCpInfoOk returns a tuple with the VnfcCpInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfcCpInfo

`func (o *VnfcResourceInfo) SetVnfcCpInfo(v []VnfcResourceInfoVnfcCpInfoInner)`

SetVnfcCpInfo sets VnfcCpInfo field to given value.

### HasVnfcCpInfo

`func (o *VnfcResourceInfo) HasVnfcCpInfo() bool`

HasVnfcCpInfo returns a boolean if a field has been set.

### GetMetadata

`func (o *VnfcResourceInfo) GetMetadata() map[string]interface{}`

GetMetadata returns the Metadata field if non-nil, zero value otherwise.

### GetMetadataOk

`func (o *VnfcResourceInfo) GetMetadataOk() (*map[string]interface{}, bool)`

GetMetadataOk returns a tuple with the Metadata field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMetadata

`func (o *VnfcResourceInfo) SetMetadata(v map[string]interface{})`

SetMetadata sets Metadata field to given value.

### HasMetadata

`func (o *VnfcResourceInfo) HasMetadata() bool`

HasMetadata returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


