# VipCpInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CpInstanceId** | **string** | An identifier that is unique for the respective type within a VNF instance, but may not be globally unique.  | 
**CpdId** | **string** | Identifier of the VNF Virtual Link Descriptor (VLD) in the VNFD.  | 
**VnfdId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**VnfExtCpId** | Pointer to **string** | An identifier that is unique for the respective type within a VNF instance, but may not be globally unique.  | [optional] 
**CpProtocolInfo** | Pointer to [**[]CpProtocolInfo**](CpProtocolInfo.md) | Protocol information for this CP. There shall be one cpProtocolInfo for layer 3. There may be one cpProtocolInfo for layer 2.  | [optional] 
**AssociatedVnfcCpIds** | Pointer to **[]string** | Identifiers of the VnfcCps that share the virtual IP addresse allocated to the VIP CP instance. See note.  | [optional] 
**VnfLinkPortId** | Pointer to **string** | An identifier that is unique for the respective type within a VNF instance, but may not be globally unique.  | [optional] 
**Metadata** | Pointer to **[]map[string]interface{}** | Allows the OSS/BSS to provide additional parameter(s) to the termination process at the NS level.  | [optional] 

## Methods

### NewVipCpInfo

`func NewVipCpInfo(cpInstanceId string, cpdId string, ) *VipCpInfo`

NewVipCpInfo instantiates a new VipCpInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVipCpInfoWithDefaults

`func NewVipCpInfoWithDefaults() *VipCpInfo`

NewVipCpInfoWithDefaults instantiates a new VipCpInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCpInstanceId

`func (o *VipCpInfo) GetCpInstanceId() string`

GetCpInstanceId returns the CpInstanceId field if non-nil, zero value otherwise.

### GetCpInstanceIdOk

`func (o *VipCpInfo) GetCpInstanceIdOk() (*string, bool)`

GetCpInstanceIdOk returns a tuple with the CpInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCpInstanceId

`func (o *VipCpInfo) SetCpInstanceId(v string)`

SetCpInstanceId sets CpInstanceId field to given value.


### GetCpdId

`func (o *VipCpInfo) GetCpdId() string`

GetCpdId returns the CpdId field if non-nil, zero value otherwise.

### GetCpdIdOk

`func (o *VipCpInfo) GetCpdIdOk() (*string, bool)`

GetCpdIdOk returns a tuple with the CpdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCpdId

`func (o *VipCpInfo) SetCpdId(v string)`

SetCpdId sets CpdId field to given value.


### GetVnfdId

`func (o *VipCpInfo) GetVnfdId() string`

GetVnfdId returns the VnfdId field if non-nil, zero value otherwise.

### GetVnfdIdOk

`func (o *VipCpInfo) GetVnfdIdOk() (*string, bool)`

GetVnfdIdOk returns a tuple with the VnfdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfdId

`func (o *VipCpInfo) SetVnfdId(v string)`

SetVnfdId sets VnfdId field to given value.

### HasVnfdId

`func (o *VipCpInfo) HasVnfdId() bool`

HasVnfdId returns a boolean if a field has been set.

### GetVnfExtCpId

`func (o *VipCpInfo) GetVnfExtCpId() string`

GetVnfExtCpId returns the VnfExtCpId field if non-nil, zero value otherwise.

### GetVnfExtCpIdOk

`func (o *VipCpInfo) GetVnfExtCpIdOk() (*string, bool)`

GetVnfExtCpIdOk returns a tuple with the VnfExtCpId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfExtCpId

`func (o *VipCpInfo) SetVnfExtCpId(v string)`

SetVnfExtCpId sets VnfExtCpId field to given value.

### HasVnfExtCpId

`func (o *VipCpInfo) HasVnfExtCpId() bool`

HasVnfExtCpId returns a boolean if a field has been set.

### GetCpProtocolInfo

`func (o *VipCpInfo) GetCpProtocolInfo() []CpProtocolInfo`

GetCpProtocolInfo returns the CpProtocolInfo field if non-nil, zero value otherwise.

### GetCpProtocolInfoOk

`func (o *VipCpInfo) GetCpProtocolInfoOk() (*[]CpProtocolInfo, bool)`

GetCpProtocolInfoOk returns a tuple with the CpProtocolInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCpProtocolInfo

`func (o *VipCpInfo) SetCpProtocolInfo(v []CpProtocolInfo)`

SetCpProtocolInfo sets CpProtocolInfo field to given value.

### HasCpProtocolInfo

`func (o *VipCpInfo) HasCpProtocolInfo() bool`

HasCpProtocolInfo returns a boolean if a field has been set.

### GetAssociatedVnfcCpIds

`func (o *VipCpInfo) GetAssociatedVnfcCpIds() []string`

GetAssociatedVnfcCpIds returns the AssociatedVnfcCpIds field if non-nil, zero value otherwise.

### GetAssociatedVnfcCpIdsOk

`func (o *VipCpInfo) GetAssociatedVnfcCpIdsOk() (*[]string, bool)`

GetAssociatedVnfcCpIdsOk returns a tuple with the AssociatedVnfcCpIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAssociatedVnfcCpIds

`func (o *VipCpInfo) SetAssociatedVnfcCpIds(v []string)`

SetAssociatedVnfcCpIds sets AssociatedVnfcCpIds field to given value.

### HasAssociatedVnfcCpIds

`func (o *VipCpInfo) HasAssociatedVnfcCpIds() bool`

HasAssociatedVnfcCpIds returns a boolean if a field has been set.

### GetVnfLinkPortId

`func (o *VipCpInfo) GetVnfLinkPortId() string`

GetVnfLinkPortId returns the VnfLinkPortId field if non-nil, zero value otherwise.

### GetVnfLinkPortIdOk

`func (o *VipCpInfo) GetVnfLinkPortIdOk() (*string, bool)`

GetVnfLinkPortIdOk returns a tuple with the VnfLinkPortId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfLinkPortId

`func (o *VipCpInfo) SetVnfLinkPortId(v string)`

SetVnfLinkPortId sets VnfLinkPortId field to given value.

### HasVnfLinkPortId

`func (o *VipCpInfo) HasVnfLinkPortId() bool`

HasVnfLinkPortId returns a boolean if a field has been set.

### GetMetadata

`func (o *VipCpInfo) GetMetadata() []map[string]interface{}`

GetMetadata returns the Metadata field if non-nil, zero value otherwise.

### GetMetadataOk

`func (o *VipCpInfo) GetMetadataOk() (*[]map[string]interface{}, bool)`

GetMetadataOk returns a tuple with the Metadata field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMetadata

`func (o *VipCpInfo) SetMetadata(v []map[string]interface{})`

SetMetadata sets Metadata field to given value.

### HasMetadata

`func (o *VipCpInfo) HasMetadata() bool`

HasMetadata returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


