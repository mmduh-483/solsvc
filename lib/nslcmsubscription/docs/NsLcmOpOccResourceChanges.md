# NsLcmOpOccResourceChanges

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AffectedVnfs** | Pointer to [**[]AffectedVnf**](AffectedVnf.md) | Information about the VNF instances that were affected during the lifecycle operation, if this notification represents the result of a lifecycle operation. See note 1.  | [optional] 
**AffectedPnfs** | Pointer to [**[]AffectedPnf**](AffectedPnf.md) | Information about the PNF instances that were affected during the lifecycle operation, if this notification represents the result of a lifecycle operation. See note 1.  | [optional] 
**AffectedVls** | Pointer to [**[]AffectedVirtualLink**](AffectedVirtualLink.md) | Information about the VL instances that were affected during the lifecycle operation, if this notification represents the result of a lifecycle operation. See note 1.  | [optional] 
**AffectedVnffgs** | Pointer to [**[]AffectedVnffg**](AffectedVnffg.md) | Information about the VNFFG instances that were affected during the lifecycle operation, if this notification represents the result of a lifecycle operation. See note 1.  | [optional] 
**AffectedNss** | Pointer to [**[]AffectedNs**](AffectedNs.md) | Information about the nested NS instances that were affected during the lifecycle operation, if this notification represents the result of a lifecycle operation. See note 1.  | [optional] 
**AffectedSaps** | Pointer to [**[]AffectedSap**](AffectedSap.md) | Information about the nested NS instances that were affected during the lifecycle operation, if this notification represents the result of a lifecycle operation. See note 1.  | [optional] 

## Methods

### NewNsLcmOpOccResourceChanges

`func NewNsLcmOpOccResourceChanges() *NsLcmOpOccResourceChanges`

NewNsLcmOpOccResourceChanges instantiates a new NsLcmOpOccResourceChanges object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNsLcmOpOccResourceChangesWithDefaults

`func NewNsLcmOpOccResourceChangesWithDefaults() *NsLcmOpOccResourceChanges`

NewNsLcmOpOccResourceChangesWithDefaults instantiates a new NsLcmOpOccResourceChanges object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAffectedVnfs

`func (o *NsLcmOpOccResourceChanges) GetAffectedVnfs() []AffectedVnf`

GetAffectedVnfs returns the AffectedVnfs field if non-nil, zero value otherwise.

### GetAffectedVnfsOk

`func (o *NsLcmOpOccResourceChanges) GetAffectedVnfsOk() (*[]AffectedVnf, bool)`

GetAffectedVnfsOk returns a tuple with the AffectedVnfs field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAffectedVnfs

`func (o *NsLcmOpOccResourceChanges) SetAffectedVnfs(v []AffectedVnf)`

SetAffectedVnfs sets AffectedVnfs field to given value.

### HasAffectedVnfs

`func (o *NsLcmOpOccResourceChanges) HasAffectedVnfs() bool`

HasAffectedVnfs returns a boolean if a field has been set.

### GetAffectedPnfs

`func (o *NsLcmOpOccResourceChanges) GetAffectedPnfs() []AffectedPnf`

GetAffectedPnfs returns the AffectedPnfs field if non-nil, zero value otherwise.

### GetAffectedPnfsOk

`func (o *NsLcmOpOccResourceChanges) GetAffectedPnfsOk() (*[]AffectedPnf, bool)`

GetAffectedPnfsOk returns a tuple with the AffectedPnfs field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAffectedPnfs

`func (o *NsLcmOpOccResourceChanges) SetAffectedPnfs(v []AffectedPnf)`

SetAffectedPnfs sets AffectedPnfs field to given value.

### HasAffectedPnfs

`func (o *NsLcmOpOccResourceChanges) HasAffectedPnfs() bool`

HasAffectedPnfs returns a boolean if a field has been set.

### GetAffectedVls

`func (o *NsLcmOpOccResourceChanges) GetAffectedVls() []AffectedVirtualLink`

GetAffectedVls returns the AffectedVls field if non-nil, zero value otherwise.

### GetAffectedVlsOk

`func (o *NsLcmOpOccResourceChanges) GetAffectedVlsOk() (*[]AffectedVirtualLink, bool)`

GetAffectedVlsOk returns a tuple with the AffectedVls field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAffectedVls

`func (o *NsLcmOpOccResourceChanges) SetAffectedVls(v []AffectedVirtualLink)`

SetAffectedVls sets AffectedVls field to given value.

### HasAffectedVls

`func (o *NsLcmOpOccResourceChanges) HasAffectedVls() bool`

HasAffectedVls returns a boolean if a field has been set.

### GetAffectedVnffgs

`func (o *NsLcmOpOccResourceChanges) GetAffectedVnffgs() []AffectedVnffg`

GetAffectedVnffgs returns the AffectedVnffgs field if non-nil, zero value otherwise.

### GetAffectedVnffgsOk

`func (o *NsLcmOpOccResourceChanges) GetAffectedVnffgsOk() (*[]AffectedVnffg, bool)`

GetAffectedVnffgsOk returns a tuple with the AffectedVnffgs field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAffectedVnffgs

`func (o *NsLcmOpOccResourceChanges) SetAffectedVnffgs(v []AffectedVnffg)`

SetAffectedVnffgs sets AffectedVnffgs field to given value.

### HasAffectedVnffgs

`func (o *NsLcmOpOccResourceChanges) HasAffectedVnffgs() bool`

HasAffectedVnffgs returns a boolean if a field has been set.

### GetAffectedNss

`func (o *NsLcmOpOccResourceChanges) GetAffectedNss() []AffectedNs`

GetAffectedNss returns the AffectedNss field if non-nil, zero value otherwise.

### GetAffectedNssOk

`func (o *NsLcmOpOccResourceChanges) GetAffectedNssOk() (*[]AffectedNs, bool)`

GetAffectedNssOk returns a tuple with the AffectedNss field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAffectedNss

`func (o *NsLcmOpOccResourceChanges) SetAffectedNss(v []AffectedNs)`

SetAffectedNss sets AffectedNss field to given value.

### HasAffectedNss

`func (o *NsLcmOpOccResourceChanges) HasAffectedNss() bool`

HasAffectedNss returns a boolean if a field has been set.

### GetAffectedSaps

`func (o *NsLcmOpOccResourceChanges) GetAffectedSaps() []AffectedSap`

GetAffectedSaps returns the AffectedSaps field if non-nil, zero value otherwise.

### GetAffectedSapsOk

`func (o *NsLcmOpOccResourceChanges) GetAffectedSapsOk() (*[]AffectedSap, bool)`

GetAffectedSapsOk returns a tuple with the AffectedSaps field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAffectedSaps

`func (o *NsLcmOpOccResourceChanges) SetAffectedSaps(v []AffectedSap)`

SetAffectedSaps sets AffectedSaps field to given value.

### HasAffectedSaps

`func (o *NsLcmOpOccResourceChanges) HasAffectedSaps() bool`

HasAffectedSaps returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


