# LocationConstraints

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CountryCode** | Pointer to **string** | The two-letter ISO 3166 country code in capital letters. Shall be present in case the \&quot;area\&quot; attribute is absent. May be absent if the \&quot;area\&quot; attribute is present (see note).  | [optional] 
**CivicAddressElement** | Pointer to [**[]LocationConstraintsCivicAddressElementInner**](LocationConstraintsCivicAddressElementInner.md) | Zero or more elements comprising the civic address. Shall be absent if the \&quot;area\&quot; attribute is present.  | [optional] 
**Area** | Pointer to **map[string]interface{}** | Geographic area. Shall be absent if the \&quot;civicAddressElement\&quot; attribute is present. The content of this attribute shall follow the provisions for the \&quot;Polygon\&quot; geometry object as defined in IETF RFC 7946, for which the \&quot;type\&quot; member shall be set to the value \&quot;Polygon\&quot;. See note.  | [optional] 

## Methods

### NewLocationConstraints

`func NewLocationConstraints() *LocationConstraints`

NewLocationConstraints instantiates a new LocationConstraints object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewLocationConstraintsWithDefaults

`func NewLocationConstraintsWithDefaults() *LocationConstraints`

NewLocationConstraintsWithDefaults instantiates a new LocationConstraints object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCountryCode

`func (o *LocationConstraints) GetCountryCode() string`

GetCountryCode returns the CountryCode field if non-nil, zero value otherwise.

### GetCountryCodeOk

`func (o *LocationConstraints) GetCountryCodeOk() (*string, bool)`

GetCountryCodeOk returns a tuple with the CountryCode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCountryCode

`func (o *LocationConstraints) SetCountryCode(v string)`

SetCountryCode sets CountryCode field to given value.

### HasCountryCode

`func (o *LocationConstraints) HasCountryCode() bool`

HasCountryCode returns a boolean if a field has been set.

### GetCivicAddressElement

`func (o *LocationConstraints) GetCivicAddressElement() []LocationConstraintsCivicAddressElementInner`

GetCivicAddressElement returns the CivicAddressElement field if non-nil, zero value otherwise.

### GetCivicAddressElementOk

`func (o *LocationConstraints) GetCivicAddressElementOk() (*[]LocationConstraintsCivicAddressElementInner, bool)`

GetCivicAddressElementOk returns a tuple with the CivicAddressElement field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCivicAddressElement

`func (o *LocationConstraints) SetCivicAddressElement(v []LocationConstraintsCivicAddressElementInner)`

SetCivicAddressElement sets CivicAddressElement field to given value.

### HasCivicAddressElement

`func (o *LocationConstraints) HasCivicAddressElement() bool`

HasCivicAddressElement returns a boolean if a field has been set.

### GetArea

`func (o *LocationConstraints) GetArea() map[string]interface{}`

GetArea returns the Area field if non-nil, zero value otherwise.

### GetAreaOk

`func (o *LocationConstraints) GetAreaOk() (*map[string]interface{}, bool)`

GetAreaOk returns a tuple with the Area field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetArea

`func (o *LocationConstraints) SetArea(v map[string]interface{})`

SetArea sets Area field to given value.

### HasArea

`func (o *LocationConstraints) HasArea() bool`

HasArea returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


