# AffectedVnffg

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VnffgInstanceId** | **string** | An identifier that is unique with respect to a NS. Representation: string of variable length.  | 
**VnffgdId** | **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | 
**ChangeType** | Pointer to **string** | Signals the type of change. Permitted values: - ADD - DELETE - MODIFY  | [optional] 
**ChangeResult** | Pointer to **string** | Signals the result of change identified by the \&quot;changeType\&quot; attribute. Permitted values: - COMPLETED - ROLLED_BACK - FAILED  | [optional] 

## Methods

### NewAffectedVnffg

`func NewAffectedVnffg(vnffgInstanceId string, vnffgdId string, ) *AffectedVnffg`

NewAffectedVnffg instantiates a new AffectedVnffg object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAffectedVnffgWithDefaults

`func NewAffectedVnffgWithDefaults() *AffectedVnffg`

NewAffectedVnffgWithDefaults instantiates a new AffectedVnffg object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVnffgInstanceId

`func (o *AffectedVnffg) GetVnffgInstanceId() string`

GetVnffgInstanceId returns the VnffgInstanceId field if non-nil, zero value otherwise.

### GetVnffgInstanceIdOk

`func (o *AffectedVnffg) GetVnffgInstanceIdOk() (*string, bool)`

GetVnffgInstanceIdOk returns a tuple with the VnffgInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnffgInstanceId

`func (o *AffectedVnffg) SetVnffgInstanceId(v string)`

SetVnffgInstanceId sets VnffgInstanceId field to given value.


### GetVnffgdId

`func (o *AffectedVnffg) GetVnffgdId() string`

GetVnffgdId returns the VnffgdId field if non-nil, zero value otherwise.

### GetVnffgdIdOk

`func (o *AffectedVnffg) GetVnffgdIdOk() (*string, bool)`

GetVnffgdIdOk returns a tuple with the VnffgdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnffgdId

`func (o *AffectedVnffg) SetVnffgdId(v string)`

SetVnffgdId sets VnffgdId field to given value.


### GetChangeType

`func (o *AffectedVnffg) GetChangeType() string`

GetChangeType returns the ChangeType field if non-nil, zero value otherwise.

### GetChangeTypeOk

`func (o *AffectedVnffg) GetChangeTypeOk() (*string, bool)`

GetChangeTypeOk returns a tuple with the ChangeType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChangeType

`func (o *AffectedVnffg) SetChangeType(v string)`

SetChangeType sets ChangeType field to given value.

### HasChangeType

`func (o *AffectedVnffg) HasChangeType() bool`

HasChangeType returns a boolean if a field has been set.

### GetChangeResult

`func (o *AffectedVnffg) GetChangeResult() string`

GetChangeResult returns the ChangeResult field if non-nil, zero value otherwise.

### GetChangeResultOk

`func (o *AffectedVnffg) GetChangeResultOk() (*string, bool)`

GetChangeResultOk returns a tuple with the ChangeResult field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChangeResult

`func (o *AffectedVnffg) SetChangeResult(v string)`

SetChangeResult sets ChangeResult field to given value.

### HasChangeResult

`func (o *AffectedVnffg) HasChangeResult() bool`

HasChangeResult returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


