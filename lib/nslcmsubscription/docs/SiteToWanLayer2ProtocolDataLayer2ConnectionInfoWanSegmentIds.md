# SiteToWanLayer2ProtocolDataLayer2ConnectionInfoWanSegmentIds

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**WanSegmentIdValue** | **string** | Identifier of the network segment (e.g., VLAN id or VNI).  | 
**WanSegmentIdUpperRange** | Pointer to **string** | Identifier of the upper range network segment, in case the \&quot;wanSegmentIds\&quot; is used to define a range.  | [optional] 

## Methods

### NewSiteToWanLayer2ProtocolDataLayer2ConnectionInfoWanSegmentIds

`func NewSiteToWanLayer2ProtocolDataLayer2ConnectionInfoWanSegmentIds(wanSegmentIdValue string, ) *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoWanSegmentIds`

NewSiteToWanLayer2ProtocolDataLayer2ConnectionInfoWanSegmentIds instantiates a new SiteToWanLayer2ProtocolDataLayer2ConnectionInfoWanSegmentIds object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSiteToWanLayer2ProtocolDataLayer2ConnectionInfoWanSegmentIdsWithDefaults

`func NewSiteToWanLayer2ProtocolDataLayer2ConnectionInfoWanSegmentIdsWithDefaults() *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoWanSegmentIds`

NewSiteToWanLayer2ProtocolDataLayer2ConnectionInfoWanSegmentIdsWithDefaults instantiates a new SiteToWanLayer2ProtocolDataLayer2ConnectionInfoWanSegmentIds object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetWanSegmentIdValue

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoWanSegmentIds) GetWanSegmentIdValue() string`

GetWanSegmentIdValue returns the WanSegmentIdValue field if non-nil, zero value otherwise.

### GetWanSegmentIdValueOk

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoWanSegmentIds) GetWanSegmentIdValueOk() (*string, bool)`

GetWanSegmentIdValueOk returns a tuple with the WanSegmentIdValue field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWanSegmentIdValue

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoWanSegmentIds) SetWanSegmentIdValue(v string)`

SetWanSegmentIdValue sets WanSegmentIdValue field to given value.


### GetWanSegmentIdUpperRange

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoWanSegmentIds) GetWanSegmentIdUpperRange() string`

GetWanSegmentIdUpperRange returns the WanSegmentIdUpperRange field if non-nil, zero value otherwise.

### GetWanSegmentIdUpperRangeOk

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoWanSegmentIds) GetWanSegmentIdUpperRangeOk() (*string, bool)`

GetWanSegmentIdUpperRangeOk returns a tuple with the WanSegmentIdUpperRange field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWanSegmentIdUpperRange

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoWanSegmentIds) SetWanSegmentIdUpperRange(v string)`

SetWanSegmentIdUpperRange sets WanSegmentIdUpperRange field to given value.

### HasWanSegmentIdUpperRange

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoWanSegmentIds) HasWanSegmentIdUpperRange() bool`

HasWanSegmentIdUpperRange returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


