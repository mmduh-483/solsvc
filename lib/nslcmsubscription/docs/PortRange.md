# PortRange

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LowerPort** | **int32** | Identifies the lower bound of the port range. upperPort Integer  | 
**UpperPort** | **int32** | Identifies the upper bound of the port range.  | 

## Methods

### NewPortRange

`func NewPortRange(lowerPort int32, upperPort int32, ) *PortRange`

NewPortRange instantiates a new PortRange object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPortRangeWithDefaults

`func NewPortRangeWithDefaults() *PortRange`

NewPortRangeWithDefaults instantiates a new PortRange object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLowerPort

`func (o *PortRange) GetLowerPort() int32`

GetLowerPort returns the LowerPort field if non-nil, zero value otherwise.

### GetLowerPortOk

`func (o *PortRange) GetLowerPortOk() (*int32, bool)`

GetLowerPortOk returns a tuple with the LowerPort field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLowerPort

`func (o *PortRange) SetLowerPort(v int32)`

SetLowerPort sets LowerPort field to given value.


### GetUpperPort

`func (o *PortRange) GetUpperPort() int32`

GetUpperPort returns the UpperPort field if non-nil, zero value otherwise.

### GetUpperPortOk

`func (o *PortRange) GetUpperPortOk() (*int32, bool)`

GetUpperPortOk returns a tuple with the UpperPort field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpperPort

`func (o *PortRange) SetUpperPort(v int32)`

SetUpperPort sets UpperPort field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


