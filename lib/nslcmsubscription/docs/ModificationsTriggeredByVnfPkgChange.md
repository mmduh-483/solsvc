# ModificationsTriggeredByVnfPkgChange

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VnfConfigurableProperties** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 
**Metadata** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 
**Extensions** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 
**VnfdId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**VnfProvider** | Pointer to **string** | If present, this attribute signals the new value of the \&quot;vnfProvider\&quot; attribute in \&quot;VnfInstance\&quot;. If present, this attribute (which depends on the value of the \&quot;vnfdId\&quot; attribute) was modified implicitly during the related operation, and contains a copy of the value of the related attribute from the VNFD in the VNF Package identified by the \&quot;vnfdId\&quot; attribute.  | [optional] 
**VnfProductName** | Pointer to **string** | If present, this attribute signals the new value of the \&quot;vnfProductName\&quot; attribute in \&quot;VnfInstance\&quot;. If present, this attribute (which depends on the value of the \&quot;vnfdId\&quot; attribute) was modified implicitly during the related operation, and contains a copy of the value of the related attribute from the VNFD in the VNF Package identified by the \&quot;vnfdId\&quot; attribute.  | [optional] 
**VnfSoftwareVersion** | Pointer to **string** | A Version. Representation: string of variable length.  | [optional] 
**VnfdVersion** | Pointer to **string** | A Version. Representation: string of variable length.  | [optional] 

## Methods

### NewModificationsTriggeredByVnfPkgChange

`func NewModificationsTriggeredByVnfPkgChange() *ModificationsTriggeredByVnfPkgChange`

NewModificationsTriggeredByVnfPkgChange instantiates a new ModificationsTriggeredByVnfPkgChange object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewModificationsTriggeredByVnfPkgChangeWithDefaults

`func NewModificationsTriggeredByVnfPkgChangeWithDefaults() *ModificationsTriggeredByVnfPkgChange`

NewModificationsTriggeredByVnfPkgChangeWithDefaults instantiates a new ModificationsTriggeredByVnfPkgChange object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVnfConfigurableProperties

`func (o *ModificationsTriggeredByVnfPkgChange) GetVnfConfigurableProperties() map[string]interface{}`

GetVnfConfigurableProperties returns the VnfConfigurableProperties field if non-nil, zero value otherwise.

### GetVnfConfigurablePropertiesOk

`func (o *ModificationsTriggeredByVnfPkgChange) GetVnfConfigurablePropertiesOk() (*map[string]interface{}, bool)`

GetVnfConfigurablePropertiesOk returns a tuple with the VnfConfigurableProperties field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfConfigurableProperties

`func (o *ModificationsTriggeredByVnfPkgChange) SetVnfConfigurableProperties(v map[string]interface{})`

SetVnfConfigurableProperties sets VnfConfigurableProperties field to given value.

### HasVnfConfigurableProperties

`func (o *ModificationsTriggeredByVnfPkgChange) HasVnfConfigurableProperties() bool`

HasVnfConfigurableProperties returns a boolean if a field has been set.

### GetMetadata

`func (o *ModificationsTriggeredByVnfPkgChange) GetMetadata() map[string]interface{}`

GetMetadata returns the Metadata field if non-nil, zero value otherwise.

### GetMetadataOk

`func (o *ModificationsTriggeredByVnfPkgChange) GetMetadataOk() (*map[string]interface{}, bool)`

GetMetadataOk returns a tuple with the Metadata field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMetadata

`func (o *ModificationsTriggeredByVnfPkgChange) SetMetadata(v map[string]interface{})`

SetMetadata sets Metadata field to given value.

### HasMetadata

`func (o *ModificationsTriggeredByVnfPkgChange) HasMetadata() bool`

HasMetadata returns a boolean if a field has been set.

### GetExtensions

`func (o *ModificationsTriggeredByVnfPkgChange) GetExtensions() map[string]interface{}`

GetExtensions returns the Extensions field if non-nil, zero value otherwise.

### GetExtensionsOk

`func (o *ModificationsTriggeredByVnfPkgChange) GetExtensionsOk() (*map[string]interface{}, bool)`

GetExtensionsOk returns a tuple with the Extensions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtensions

`func (o *ModificationsTriggeredByVnfPkgChange) SetExtensions(v map[string]interface{})`

SetExtensions sets Extensions field to given value.

### HasExtensions

`func (o *ModificationsTriggeredByVnfPkgChange) HasExtensions() bool`

HasExtensions returns a boolean if a field has been set.

### GetVnfdId

`func (o *ModificationsTriggeredByVnfPkgChange) GetVnfdId() string`

GetVnfdId returns the VnfdId field if non-nil, zero value otherwise.

### GetVnfdIdOk

`func (o *ModificationsTriggeredByVnfPkgChange) GetVnfdIdOk() (*string, bool)`

GetVnfdIdOk returns a tuple with the VnfdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfdId

`func (o *ModificationsTriggeredByVnfPkgChange) SetVnfdId(v string)`

SetVnfdId sets VnfdId field to given value.

### HasVnfdId

`func (o *ModificationsTriggeredByVnfPkgChange) HasVnfdId() bool`

HasVnfdId returns a boolean if a field has been set.

### GetVnfProvider

`func (o *ModificationsTriggeredByVnfPkgChange) GetVnfProvider() string`

GetVnfProvider returns the VnfProvider field if non-nil, zero value otherwise.

### GetVnfProviderOk

`func (o *ModificationsTriggeredByVnfPkgChange) GetVnfProviderOk() (*string, bool)`

GetVnfProviderOk returns a tuple with the VnfProvider field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfProvider

`func (o *ModificationsTriggeredByVnfPkgChange) SetVnfProvider(v string)`

SetVnfProvider sets VnfProvider field to given value.

### HasVnfProvider

`func (o *ModificationsTriggeredByVnfPkgChange) HasVnfProvider() bool`

HasVnfProvider returns a boolean if a field has been set.

### GetVnfProductName

`func (o *ModificationsTriggeredByVnfPkgChange) GetVnfProductName() string`

GetVnfProductName returns the VnfProductName field if non-nil, zero value otherwise.

### GetVnfProductNameOk

`func (o *ModificationsTriggeredByVnfPkgChange) GetVnfProductNameOk() (*string, bool)`

GetVnfProductNameOk returns a tuple with the VnfProductName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfProductName

`func (o *ModificationsTriggeredByVnfPkgChange) SetVnfProductName(v string)`

SetVnfProductName sets VnfProductName field to given value.

### HasVnfProductName

`func (o *ModificationsTriggeredByVnfPkgChange) HasVnfProductName() bool`

HasVnfProductName returns a boolean if a field has been set.

### GetVnfSoftwareVersion

`func (o *ModificationsTriggeredByVnfPkgChange) GetVnfSoftwareVersion() string`

GetVnfSoftwareVersion returns the VnfSoftwareVersion field if non-nil, zero value otherwise.

### GetVnfSoftwareVersionOk

`func (o *ModificationsTriggeredByVnfPkgChange) GetVnfSoftwareVersionOk() (*string, bool)`

GetVnfSoftwareVersionOk returns a tuple with the VnfSoftwareVersion field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfSoftwareVersion

`func (o *ModificationsTriggeredByVnfPkgChange) SetVnfSoftwareVersion(v string)`

SetVnfSoftwareVersion sets VnfSoftwareVersion field to given value.

### HasVnfSoftwareVersion

`func (o *ModificationsTriggeredByVnfPkgChange) HasVnfSoftwareVersion() bool`

HasVnfSoftwareVersion returns a boolean if a field has been set.

### GetVnfdVersion

`func (o *ModificationsTriggeredByVnfPkgChange) GetVnfdVersion() string`

GetVnfdVersion returns the VnfdVersion field if non-nil, zero value otherwise.

### GetVnfdVersionOk

`func (o *ModificationsTriggeredByVnfPkgChange) GetVnfdVersionOk() (*string, bool)`

GetVnfdVersionOk returns a tuple with the VnfdVersion field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfdVersion

`func (o *ModificationsTriggeredByVnfPkgChange) SetVnfdVersion(v string)`

SetVnfdVersion sets VnfdVersion field to given value.

### HasVnfdVersion

`func (o *ModificationsTriggeredByVnfPkgChange) HasVnfdVersion() bool`

HasVnfdVersion returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


