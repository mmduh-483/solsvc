# CpPairInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VnfExtCpIds** | Pointer to **[]string** | Identifier(s) of the VNF CP(s) which form the pair. See note 1 and note 2.  | [optional] 
**PnfExtCpIds** | Pointer to **[]string** | Identifier(s) of the PNF CP(s) which form the pair. See note 1 and note 2.  | [optional] 
**SapIds** | Pointer to **[]string** | Identifier(s) of the SAP(s) which form the pair. See note 1 and note 2.  | [optional] 

## Methods

### NewCpPairInfo

`func NewCpPairInfo() *CpPairInfo`

NewCpPairInfo instantiates a new CpPairInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCpPairInfoWithDefaults

`func NewCpPairInfoWithDefaults() *CpPairInfo`

NewCpPairInfoWithDefaults instantiates a new CpPairInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVnfExtCpIds

`func (o *CpPairInfo) GetVnfExtCpIds() []string`

GetVnfExtCpIds returns the VnfExtCpIds field if non-nil, zero value otherwise.

### GetVnfExtCpIdsOk

`func (o *CpPairInfo) GetVnfExtCpIdsOk() (*[]string, bool)`

GetVnfExtCpIdsOk returns a tuple with the VnfExtCpIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfExtCpIds

`func (o *CpPairInfo) SetVnfExtCpIds(v []string)`

SetVnfExtCpIds sets VnfExtCpIds field to given value.

### HasVnfExtCpIds

`func (o *CpPairInfo) HasVnfExtCpIds() bool`

HasVnfExtCpIds returns a boolean if a field has been set.

### GetPnfExtCpIds

`func (o *CpPairInfo) GetPnfExtCpIds() []string`

GetPnfExtCpIds returns the PnfExtCpIds field if non-nil, zero value otherwise.

### GetPnfExtCpIdsOk

`func (o *CpPairInfo) GetPnfExtCpIdsOk() (*[]string, bool)`

GetPnfExtCpIdsOk returns a tuple with the PnfExtCpIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPnfExtCpIds

`func (o *CpPairInfo) SetPnfExtCpIds(v []string)`

SetPnfExtCpIds sets PnfExtCpIds field to given value.

### HasPnfExtCpIds

`func (o *CpPairInfo) HasPnfExtCpIds() bool`

HasPnfExtCpIds returns a boolean if a field has been set.

### GetSapIds

`func (o *CpPairInfo) GetSapIds() []string`

GetSapIds returns the SapIds field if non-nil, zero value otherwise.

### GetSapIdsOk

`func (o *CpPairInfo) GetSapIdsOk() (*[]string, bool)`

GetSapIdsOk returns a tuple with the SapIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSapIds

`func (o *CpPairInfo) SetSapIds(v []string)`

SetSapIds sets SapIds field to given value.

### HasSapIds

`func (o *CpPairInfo) HasSapIds() bool`

HasSapIds returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


