# AffectedPnf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PnfId** | **string** | An identifier with the intention of being globally unique.  | 
**PnfdId** | **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | 
**PnfProfileId** | **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | 
**PnfName** | Pointer to **string** | Name of the PNF.  | [optional] 
**CpInstanceId** | **[]string** | Identifier of the CP in the scope of the PNF.  | 
**ChangeType** | Pointer to **string** | Signals the type of change. Permitted values: - ADD - REMOVE - MODIFY  | [optional] 
**ChangeResult** | Pointer to **string** | Signals the result of change identified by the \&quot;changeType\&quot; attribute. Permitted values: - COMPLETED - ROLLED_BACK - FAILED  | [optional] 

## Methods

### NewAffectedPnf

`func NewAffectedPnf(pnfId string, pnfdId string, pnfProfileId string, cpInstanceId []string, ) *AffectedPnf`

NewAffectedPnf instantiates a new AffectedPnf object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAffectedPnfWithDefaults

`func NewAffectedPnfWithDefaults() *AffectedPnf`

NewAffectedPnfWithDefaults instantiates a new AffectedPnf object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPnfId

`func (o *AffectedPnf) GetPnfId() string`

GetPnfId returns the PnfId field if non-nil, zero value otherwise.

### GetPnfIdOk

`func (o *AffectedPnf) GetPnfIdOk() (*string, bool)`

GetPnfIdOk returns a tuple with the PnfId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPnfId

`func (o *AffectedPnf) SetPnfId(v string)`

SetPnfId sets PnfId field to given value.


### GetPnfdId

`func (o *AffectedPnf) GetPnfdId() string`

GetPnfdId returns the PnfdId field if non-nil, zero value otherwise.

### GetPnfdIdOk

`func (o *AffectedPnf) GetPnfdIdOk() (*string, bool)`

GetPnfdIdOk returns a tuple with the PnfdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPnfdId

`func (o *AffectedPnf) SetPnfdId(v string)`

SetPnfdId sets PnfdId field to given value.


### GetPnfProfileId

`func (o *AffectedPnf) GetPnfProfileId() string`

GetPnfProfileId returns the PnfProfileId field if non-nil, zero value otherwise.

### GetPnfProfileIdOk

`func (o *AffectedPnf) GetPnfProfileIdOk() (*string, bool)`

GetPnfProfileIdOk returns a tuple with the PnfProfileId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPnfProfileId

`func (o *AffectedPnf) SetPnfProfileId(v string)`

SetPnfProfileId sets PnfProfileId field to given value.


### GetPnfName

`func (o *AffectedPnf) GetPnfName() string`

GetPnfName returns the PnfName field if non-nil, zero value otherwise.

### GetPnfNameOk

`func (o *AffectedPnf) GetPnfNameOk() (*string, bool)`

GetPnfNameOk returns a tuple with the PnfName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPnfName

`func (o *AffectedPnf) SetPnfName(v string)`

SetPnfName sets PnfName field to given value.

### HasPnfName

`func (o *AffectedPnf) HasPnfName() bool`

HasPnfName returns a boolean if a field has been set.

### GetCpInstanceId

`func (o *AffectedPnf) GetCpInstanceId() []string`

GetCpInstanceId returns the CpInstanceId field if non-nil, zero value otherwise.

### GetCpInstanceIdOk

`func (o *AffectedPnf) GetCpInstanceIdOk() (*[]string, bool)`

GetCpInstanceIdOk returns a tuple with the CpInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCpInstanceId

`func (o *AffectedPnf) SetCpInstanceId(v []string)`

SetCpInstanceId sets CpInstanceId field to given value.


### GetChangeType

`func (o *AffectedPnf) GetChangeType() string`

GetChangeType returns the ChangeType field if non-nil, zero value otherwise.

### GetChangeTypeOk

`func (o *AffectedPnf) GetChangeTypeOk() (*string, bool)`

GetChangeTypeOk returns a tuple with the ChangeType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChangeType

`func (o *AffectedPnf) SetChangeType(v string)`

SetChangeType sets ChangeType field to given value.

### HasChangeType

`func (o *AffectedPnf) HasChangeType() bool`

HasChangeType returns a boolean if a field has been set.

### GetChangeResult

`func (o *AffectedPnf) GetChangeResult() string`

GetChangeResult returns the ChangeResult field if non-nil, zero value otherwise.

### GetChangeResultOk

`func (o *AffectedPnf) GetChangeResultOk() (*string, bool)`

GetChangeResultOk returns a tuple with the ChangeResult field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChangeResult

`func (o *AffectedPnf) SetChangeResult(v string)`

SetChangeResult sets ChangeResult field to given value.

### HasChangeResult

`func (o *AffectedPnf) HasChangeResult() bool`

HasChangeResult returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


