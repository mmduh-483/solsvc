# HealNsRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**HealNsData** | Pointer to [**HealNsData**](HealNsData.md) |  | [optional] 
**HealVnfData** | Pointer to [**[]HealVnfData**](HealVnfData.md) | Provides the information needed to heal a VNF. See note.  | [optional] 

## Methods

### NewHealNsRequest

`func NewHealNsRequest() *HealNsRequest`

NewHealNsRequest instantiates a new HealNsRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewHealNsRequestWithDefaults

`func NewHealNsRequestWithDefaults() *HealNsRequest`

NewHealNsRequestWithDefaults instantiates a new HealNsRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetHealNsData

`func (o *HealNsRequest) GetHealNsData() HealNsData`

GetHealNsData returns the HealNsData field if non-nil, zero value otherwise.

### GetHealNsDataOk

`func (o *HealNsRequest) GetHealNsDataOk() (*HealNsData, bool)`

GetHealNsDataOk returns a tuple with the HealNsData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHealNsData

`func (o *HealNsRequest) SetHealNsData(v HealNsData)`

SetHealNsData sets HealNsData field to given value.

### HasHealNsData

`func (o *HealNsRequest) HasHealNsData() bool`

HasHealNsData returns a boolean if a field has been set.

### GetHealVnfData

`func (o *HealNsRequest) GetHealVnfData() []HealVnfData`

GetHealVnfData returns the HealVnfData field if non-nil, zero value otherwise.

### GetHealVnfDataOk

`func (o *HealNsRequest) GetHealVnfDataOk() (*[]HealVnfData, bool)`

GetHealVnfDataOk returns a tuple with the HealVnfData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHealVnfData

`func (o *HealNsRequest) SetHealVnfData(v []HealVnfData)`

SetHealVnfData sets HealVnfData field to given value.

### HasHealVnfData

`func (o *HealNsRequest) HasHealVnfData() bool`

HasHealVnfData returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


