# CreateVnfSnapshotData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VnfInstanceId** | **string** | An identifier with the intention of being globally unique.  | 
**AdditionalParams** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 
**UserDefinedData** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 

## Methods

### NewCreateVnfSnapshotData

`func NewCreateVnfSnapshotData(vnfInstanceId string, ) *CreateVnfSnapshotData`

NewCreateVnfSnapshotData instantiates a new CreateVnfSnapshotData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateVnfSnapshotDataWithDefaults

`func NewCreateVnfSnapshotDataWithDefaults() *CreateVnfSnapshotData`

NewCreateVnfSnapshotDataWithDefaults instantiates a new CreateVnfSnapshotData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVnfInstanceId

`func (o *CreateVnfSnapshotData) GetVnfInstanceId() string`

GetVnfInstanceId returns the VnfInstanceId field if non-nil, zero value otherwise.

### GetVnfInstanceIdOk

`func (o *CreateVnfSnapshotData) GetVnfInstanceIdOk() (*string, bool)`

GetVnfInstanceIdOk returns a tuple with the VnfInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceId

`func (o *CreateVnfSnapshotData) SetVnfInstanceId(v string)`

SetVnfInstanceId sets VnfInstanceId field to given value.


### GetAdditionalParams

`func (o *CreateVnfSnapshotData) GetAdditionalParams() map[string]interface{}`

GetAdditionalParams returns the AdditionalParams field if non-nil, zero value otherwise.

### GetAdditionalParamsOk

`func (o *CreateVnfSnapshotData) GetAdditionalParamsOk() (*map[string]interface{}, bool)`

GetAdditionalParamsOk returns a tuple with the AdditionalParams field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdditionalParams

`func (o *CreateVnfSnapshotData) SetAdditionalParams(v map[string]interface{})`

SetAdditionalParams sets AdditionalParams field to given value.

### HasAdditionalParams

`func (o *CreateVnfSnapshotData) HasAdditionalParams() bool`

HasAdditionalParams returns a boolean if a field has been set.

### GetUserDefinedData

`func (o *CreateVnfSnapshotData) GetUserDefinedData() map[string]interface{}`

GetUserDefinedData returns the UserDefinedData field if non-nil, zero value otherwise.

### GetUserDefinedDataOk

`func (o *CreateVnfSnapshotData) GetUserDefinedDataOk() (*map[string]interface{}, bool)`

GetUserDefinedDataOk returns a tuple with the UserDefinedData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUserDefinedData

`func (o *CreateVnfSnapshotData) SetUserDefinedData(v map[string]interface{})`

SetUserDefinedData sets UserDefinedData field to given value.

### HasUserDefinedData

`func (o *CreateVnfSnapshotData) HasUserDefinedData() bool`

HasUserDefinedData returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


