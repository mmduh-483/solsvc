# PnfInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PnfId** | **string** | An identifier with the intention of being globally unique.  | 
**PnfName** | Pointer to **string** | Name of the PNF.  | [optional] 
**PnfdId** | **string** | An identifier with the intention of being globally unique.  | 
**PnfdInfoId** | **string** | An identifier with the intention of being globally unique.  | 
**PnfProfileId** | **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | 
**CpInfo** | Pointer to [**PnfExtCpInfo**](PnfExtCpInfo.md) |  | [optional] 

## Methods

### NewPnfInfo

`func NewPnfInfo(pnfId string, pnfdId string, pnfdInfoId string, pnfProfileId string, ) *PnfInfo`

NewPnfInfo instantiates a new PnfInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPnfInfoWithDefaults

`func NewPnfInfoWithDefaults() *PnfInfo`

NewPnfInfoWithDefaults instantiates a new PnfInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPnfId

`func (o *PnfInfo) GetPnfId() string`

GetPnfId returns the PnfId field if non-nil, zero value otherwise.

### GetPnfIdOk

`func (o *PnfInfo) GetPnfIdOk() (*string, bool)`

GetPnfIdOk returns a tuple with the PnfId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPnfId

`func (o *PnfInfo) SetPnfId(v string)`

SetPnfId sets PnfId field to given value.


### GetPnfName

`func (o *PnfInfo) GetPnfName() string`

GetPnfName returns the PnfName field if non-nil, zero value otherwise.

### GetPnfNameOk

`func (o *PnfInfo) GetPnfNameOk() (*string, bool)`

GetPnfNameOk returns a tuple with the PnfName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPnfName

`func (o *PnfInfo) SetPnfName(v string)`

SetPnfName sets PnfName field to given value.

### HasPnfName

`func (o *PnfInfo) HasPnfName() bool`

HasPnfName returns a boolean if a field has been set.

### GetPnfdId

`func (o *PnfInfo) GetPnfdId() string`

GetPnfdId returns the PnfdId field if non-nil, zero value otherwise.

### GetPnfdIdOk

`func (o *PnfInfo) GetPnfdIdOk() (*string, bool)`

GetPnfdIdOk returns a tuple with the PnfdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPnfdId

`func (o *PnfInfo) SetPnfdId(v string)`

SetPnfdId sets PnfdId field to given value.


### GetPnfdInfoId

`func (o *PnfInfo) GetPnfdInfoId() string`

GetPnfdInfoId returns the PnfdInfoId field if non-nil, zero value otherwise.

### GetPnfdInfoIdOk

`func (o *PnfInfo) GetPnfdInfoIdOk() (*string, bool)`

GetPnfdInfoIdOk returns a tuple with the PnfdInfoId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPnfdInfoId

`func (o *PnfInfo) SetPnfdInfoId(v string)`

SetPnfdInfoId sets PnfdInfoId field to given value.


### GetPnfProfileId

`func (o *PnfInfo) GetPnfProfileId() string`

GetPnfProfileId returns the PnfProfileId field if non-nil, zero value otherwise.

### GetPnfProfileIdOk

`func (o *PnfInfo) GetPnfProfileIdOk() (*string, bool)`

GetPnfProfileIdOk returns a tuple with the PnfProfileId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPnfProfileId

`func (o *PnfInfo) SetPnfProfileId(v string)`

SetPnfProfileId sets PnfProfileId field to given value.


### GetCpInfo

`func (o *PnfInfo) GetCpInfo() PnfExtCpInfo`

GetCpInfo returns the CpInfo field if non-nil, zero value otherwise.

### GetCpInfoOk

`func (o *PnfInfo) GetCpInfoOk() (*PnfExtCpInfo, bool)`

GetCpInfoOk returns a tuple with the CpInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCpInfo

`func (o *PnfInfo) SetCpInfo(v PnfExtCpInfo)`

SetCpInfo sets CpInfo field to given value.

### HasCpInfo

`func (o *PnfInfo) HasCpInfo() bool`

HasCpInfo returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


