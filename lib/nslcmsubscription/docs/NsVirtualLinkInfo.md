# NsVirtualLinkInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | An identifier that is unique with respect to a NS. Representation: string of variable length.  | 
**NsVirtualLinkDescId** | **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | 
**NsVirtualLinkProfileId** | **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | 
**ResourceHandle** | Pointer to [**[]ResourceHandle**](ResourceHandle.md) | Identifier(s) of the virtualised network resource(s) and/or multi-site connectivity service(s) realizing the VL instance. See note.  | [optional] 
**LinkPort** | Pointer to [**[]NsLinkPortInfo**](NsLinkPortInfo.md) | Link ports of the VL instance. Cardinality of zero indicates that no port has yet been created for the VL instance.  | [optional] 

## Methods

### NewNsVirtualLinkInfo

`func NewNsVirtualLinkInfo(id string, nsVirtualLinkDescId string, nsVirtualLinkProfileId string, ) *NsVirtualLinkInfo`

NewNsVirtualLinkInfo instantiates a new NsVirtualLinkInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNsVirtualLinkInfoWithDefaults

`func NewNsVirtualLinkInfoWithDefaults() *NsVirtualLinkInfo`

NewNsVirtualLinkInfoWithDefaults instantiates a new NsVirtualLinkInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *NsVirtualLinkInfo) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *NsVirtualLinkInfo) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *NsVirtualLinkInfo) SetId(v string)`

SetId sets Id field to given value.


### GetNsVirtualLinkDescId

`func (o *NsVirtualLinkInfo) GetNsVirtualLinkDescId() string`

GetNsVirtualLinkDescId returns the NsVirtualLinkDescId field if non-nil, zero value otherwise.

### GetNsVirtualLinkDescIdOk

`func (o *NsVirtualLinkInfo) GetNsVirtualLinkDescIdOk() (*string, bool)`

GetNsVirtualLinkDescIdOk returns a tuple with the NsVirtualLinkDescId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsVirtualLinkDescId

`func (o *NsVirtualLinkInfo) SetNsVirtualLinkDescId(v string)`

SetNsVirtualLinkDescId sets NsVirtualLinkDescId field to given value.


### GetNsVirtualLinkProfileId

`func (o *NsVirtualLinkInfo) GetNsVirtualLinkProfileId() string`

GetNsVirtualLinkProfileId returns the NsVirtualLinkProfileId field if non-nil, zero value otherwise.

### GetNsVirtualLinkProfileIdOk

`func (o *NsVirtualLinkInfo) GetNsVirtualLinkProfileIdOk() (*string, bool)`

GetNsVirtualLinkProfileIdOk returns a tuple with the NsVirtualLinkProfileId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsVirtualLinkProfileId

`func (o *NsVirtualLinkInfo) SetNsVirtualLinkProfileId(v string)`

SetNsVirtualLinkProfileId sets NsVirtualLinkProfileId field to given value.


### GetResourceHandle

`func (o *NsVirtualLinkInfo) GetResourceHandle() []ResourceHandle`

GetResourceHandle returns the ResourceHandle field if non-nil, zero value otherwise.

### GetResourceHandleOk

`func (o *NsVirtualLinkInfo) GetResourceHandleOk() (*[]ResourceHandle, bool)`

GetResourceHandleOk returns a tuple with the ResourceHandle field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResourceHandle

`func (o *NsVirtualLinkInfo) SetResourceHandle(v []ResourceHandle)`

SetResourceHandle sets ResourceHandle field to given value.

### HasResourceHandle

`func (o *NsVirtualLinkInfo) HasResourceHandle() bool`

HasResourceHandle returns a boolean if a field has been set.

### GetLinkPort

`func (o *NsVirtualLinkInfo) GetLinkPort() []NsLinkPortInfo`

GetLinkPort returns the LinkPort field if non-nil, zero value otherwise.

### GetLinkPortOk

`func (o *NsVirtualLinkInfo) GetLinkPortOk() (*[]NsLinkPortInfo, bool)`

GetLinkPortOk returns a tuple with the LinkPort field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLinkPort

`func (o *NsVirtualLinkInfo) SetLinkPort(v []NsLinkPortInfo)`

SetLinkPort sets LinkPort field to given value.

### HasLinkPort

`func (o *NsVirtualLinkInfo) HasLinkPort() bool`

HasLinkPort returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


