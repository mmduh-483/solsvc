# CpGroupInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CpPairInfo** | Pointer to [**[]CpPairInfo**](CpPairInfo.md) | One or more pair(s) of ingress and egress CPs or SAPs which the NFP passes by. All CP or SAP pairs in a group shall be instantiated from connection point  descriptors or service access point descriptors referenced in the corresponding  NfpPositionDesc. See note.  | [optional] 
**ForwardingBehaviour** | Pointer to **string** | Identifies a rule to apply to forward traffic to the ingress CPs or SAPs of  the group. Permitted values: * ALL &#x3D; Traffic flows shall be forwarded simultaneously to all CPs or SAPs  of the group. * LB &#x3D; Traffic flows shall be forwarded to one CP or SAP of the group selected  based on a loadbalancing algorithm.  | [optional] 
**ForwardingBehaviourInputParameters** | Pointer to [**ForwardingBehaviourInputParameters**](ForwardingBehaviourInputParameters.md) |  | [optional] 

## Methods

### NewCpGroupInfo

`func NewCpGroupInfo() *CpGroupInfo`

NewCpGroupInfo instantiates a new CpGroupInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCpGroupInfoWithDefaults

`func NewCpGroupInfoWithDefaults() *CpGroupInfo`

NewCpGroupInfoWithDefaults instantiates a new CpGroupInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCpPairInfo

`func (o *CpGroupInfo) GetCpPairInfo() []CpPairInfo`

GetCpPairInfo returns the CpPairInfo field if non-nil, zero value otherwise.

### GetCpPairInfoOk

`func (o *CpGroupInfo) GetCpPairInfoOk() (*[]CpPairInfo, bool)`

GetCpPairInfoOk returns a tuple with the CpPairInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCpPairInfo

`func (o *CpGroupInfo) SetCpPairInfo(v []CpPairInfo)`

SetCpPairInfo sets CpPairInfo field to given value.

### HasCpPairInfo

`func (o *CpGroupInfo) HasCpPairInfo() bool`

HasCpPairInfo returns a boolean if a field has been set.

### GetForwardingBehaviour

`func (o *CpGroupInfo) GetForwardingBehaviour() string`

GetForwardingBehaviour returns the ForwardingBehaviour field if non-nil, zero value otherwise.

### GetForwardingBehaviourOk

`func (o *CpGroupInfo) GetForwardingBehaviourOk() (*string, bool)`

GetForwardingBehaviourOk returns a tuple with the ForwardingBehaviour field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetForwardingBehaviour

`func (o *CpGroupInfo) SetForwardingBehaviour(v string)`

SetForwardingBehaviour sets ForwardingBehaviour field to given value.

### HasForwardingBehaviour

`func (o *CpGroupInfo) HasForwardingBehaviour() bool`

HasForwardingBehaviour returns a boolean if a field has been set.

### GetForwardingBehaviourInputParameters

`func (o *CpGroupInfo) GetForwardingBehaviourInputParameters() ForwardingBehaviourInputParameters`

GetForwardingBehaviourInputParameters returns the ForwardingBehaviourInputParameters field if non-nil, zero value otherwise.

### GetForwardingBehaviourInputParametersOk

`func (o *CpGroupInfo) GetForwardingBehaviourInputParametersOk() (*ForwardingBehaviourInputParameters, bool)`

GetForwardingBehaviourInputParametersOk returns a tuple with the ForwardingBehaviourInputParameters field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetForwardingBehaviourInputParameters

`func (o *CpGroupInfo) SetForwardingBehaviourInputParameters(v ForwardingBehaviourInputParameters)`

SetForwardingBehaviourInputParameters sets ForwardingBehaviourInputParameters field to given value.

### HasForwardingBehaviourInputParameters

`func (o *CpGroupInfo) HasForwardingBehaviourInputParameters() bool`

HasForwardingBehaviourInputParameters returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


