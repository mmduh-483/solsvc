# PnfExtCpData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CpInstanceId** | Pointer to **string** | An Identifier that is unique within respect to a PNF. Representation: string of variable length.  | [optional] 
**CpdId** | Pointer to **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | [optional] 
**CpProtocolData** | [**[]CpProtocolData**](CpProtocolData.md) | Address assigned for this CP.  | 

## Methods

### NewPnfExtCpData

`func NewPnfExtCpData(cpProtocolData []CpProtocolData, ) *PnfExtCpData`

NewPnfExtCpData instantiates a new PnfExtCpData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPnfExtCpDataWithDefaults

`func NewPnfExtCpDataWithDefaults() *PnfExtCpData`

NewPnfExtCpDataWithDefaults instantiates a new PnfExtCpData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCpInstanceId

`func (o *PnfExtCpData) GetCpInstanceId() string`

GetCpInstanceId returns the CpInstanceId field if non-nil, zero value otherwise.

### GetCpInstanceIdOk

`func (o *PnfExtCpData) GetCpInstanceIdOk() (*string, bool)`

GetCpInstanceIdOk returns a tuple with the CpInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCpInstanceId

`func (o *PnfExtCpData) SetCpInstanceId(v string)`

SetCpInstanceId sets CpInstanceId field to given value.

### HasCpInstanceId

`func (o *PnfExtCpData) HasCpInstanceId() bool`

HasCpInstanceId returns a boolean if a field has been set.

### GetCpdId

`func (o *PnfExtCpData) GetCpdId() string`

GetCpdId returns the CpdId field if non-nil, zero value otherwise.

### GetCpdIdOk

`func (o *PnfExtCpData) GetCpdIdOk() (*string, bool)`

GetCpdIdOk returns a tuple with the CpdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCpdId

`func (o *PnfExtCpData) SetCpdId(v string)`

SetCpdId sets CpdId field to given value.

### HasCpdId

`func (o *PnfExtCpData) HasCpdId() bool`

HasCpdId returns a boolean if a field has been set.

### GetCpProtocolData

`func (o *PnfExtCpData) GetCpProtocolData() []CpProtocolData`

GetCpProtocolData returns the CpProtocolData field if non-nil, zero value otherwise.

### GetCpProtocolDataOk

`func (o *PnfExtCpData) GetCpProtocolDataOk() (*[]CpProtocolData, bool)`

GetCpProtocolDataOk returns a tuple with the CpProtocolData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCpProtocolData

`func (o *PnfExtCpData) SetCpProtocolData(v []CpProtocolData)`

SetCpProtocolData sets CpProtocolData field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


