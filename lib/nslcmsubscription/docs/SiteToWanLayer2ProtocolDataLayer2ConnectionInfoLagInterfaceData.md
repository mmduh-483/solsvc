# SiteToWanLayer2ProtocolDataLayer2ConnectionInfoLagInterfaceData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AggregatedEndpoints** | **[]string** | List of the connectivity service endpoints that are to be aggregated. Shall be present if connectionType&#x3D;\&quot;AGGREGATE_CSE\&quot;. In case of aggregating connectivity service endpoints, only one SiteToWanLayer2ProtocolData shall be provided for the whole set of aggregated endpoints.  | 
**LacpActivation** | **bool** | Indicates whether to activate LACP on the interface. If \&quot;TRUE\&quot;, the LACP is to be activated, or \&quot;FALSE\&quot; otherwise. Default value is \&quot;FALSE\&quot;.  | 
**LacpConfig** | **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | 

## Methods

### NewSiteToWanLayer2ProtocolDataLayer2ConnectionInfoLagInterfaceData

`func NewSiteToWanLayer2ProtocolDataLayer2ConnectionInfoLagInterfaceData(aggregatedEndpoints []string, lacpActivation bool, lacpConfig map[string]interface{}, ) *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoLagInterfaceData`

NewSiteToWanLayer2ProtocolDataLayer2ConnectionInfoLagInterfaceData instantiates a new SiteToWanLayer2ProtocolDataLayer2ConnectionInfoLagInterfaceData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSiteToWanLayer2ProtocolDataLayer2ConnectionInfoLagInterfaceDataWithDefaults

`func NewSiteToWanLayer2ProtocolDataLayer2ConnectionInfoLagInterfaceDataWithDefaults() *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoLagInterfaceData`

NewSiteToWanLayer2ProtocolDataLayer2ConnectionInfoLagInterfaceDataWithDefaults instantiates a new SiteToWanLayer2ProtocolDataLayer2ConnectionInfoLagInterfaceData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAggregatedEndpoints

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoLagInterfaceData) GetAggregatedEndpoints() []string`

GetAggregatedEndpoints returns the AggregatedEndpoints field if non-nil, zero value otherwise.

### GetAggregatedEndpointsOk

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoLagInterfaceData) GetAggregatedEndpointsOk() (*[]string, bool)`

GetAggregatedEndpointsOk returns a tuple with the AggregatedEndpoints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAggregatedEndpoints

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoLagInterfaceData) SetAggregatedEndpoints(v []string)`

SetAggregatedEndpoints sets AggregatedEndpoints field to given value.


### GetLacpActivation

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoLagInterfaceData) GetLacpActivation() bool`

GetLacpActivation returns the LacpActivation field if non-nil, zero value otherwise.

### GetLacpActivationOk

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoLagInterfaceData) GetLacpActivationOk() (*bool, bool)`

GetLacpActivationOk returns a tuple with the LacpActivation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLacpActivation

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoLagInterfaceData) SetLacpActivation(v bool)`

SetLacpActivation sets LacpActivation field to given value.


### GetLacpConfig

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoLagInterfaceData) GetLacpConfig() map[string]interface{}`

GetLacpConfig returns the LacpConfig field if non-nil, zero value otherwise.

### GetLacpConfigOk

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoLagInterfaceData) GetLacpConfigOk() (*map[string]interface{}, bool)`

GetLacpConfigOk returns a tuple with the LacpConfig field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLacpConfig

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoLagInterfaceData) SetLacpConfig(v map[string]interface{})`

SetLacpConfig sets LacpConfig field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


