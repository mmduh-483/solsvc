# SiteToWanLayer2ProtocolDataVirtualRoutingAndForwarding

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MacVrfName** | **string** | Name (or identifier) of the MAC-VRF instance.  | 

## Methods

### NewSiteToWanLayer2ProtocolDataVirtualRoutingAndForwarding

`func NewSiteToWanLayer2ProtocolDataVirtualRoutingAndForwarding(macVrfName string, ) *SiteToWanLayer2ProtocolDataVirtualRoutingAndForwarding`

NewSiteToWanLayer2ProtocolDataVirtualRoutingAndForwarding instantiates a new SiteToWanLayer2ProtocolDataVirtualRoutingAndForwarding object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSiteToWanLayer2ProtocolDataVirtualRoutingAndForwardingWithDefaults

`func NewSiteToWanLayer2ProtocolDataVirtualRoutingAndForwardingWithDefaults() *SiteToWanLayer2ProtocolDataVirtualRoutingAndForwarding`

NewSiteToWanLayer2ProtocolDataVirtualRoutingAndForwardingWithDefaults instantiates a new SiteToWanLayer2ProtocolDataVirtualRoutingAndForwarding object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMacVrfName

`func (o *SiteToWanLayer2ProtocolDataVirtualRoutingAndForwarding) GetMacVrfName() string`

GetMacVrfName returns the MacVrfName field if non-nil, zero value otherwise.

### GetMacVrfNameOk

`func (o *SiteToWanLayer2ProtocolDataVirtualRoutingAndForwarding) GetMacVrfNameOk() (*string, bool)`

GetMacVrfNameOk returns a tuple with the MacVrfName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMacVrfName

`func (o *SiteToWanLayer2ProtocolDataVirtualRoutingAndForwarding) SetMacVrfName(v string)`

SetMacVrfName sets MacVrfName field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


