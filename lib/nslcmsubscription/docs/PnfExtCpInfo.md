# PnfExtCpInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CpInstanceId** | **string** | An Identifier that is unique within respect to a PNF. Representation: string of variable length.  | 
**CpdId** | **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | 
**CpProtocolData** | Pointer to [**[]CpProtocolData**](CpProtocolData.md) | Parameters for configuring the network protocols on the CP.  | [optional] 

## Methods

### NewPnfExtCpInfo

`func NewPnfExtCpInfo(cpInstanceId string, cpdId string, ) *PnfExtCpInfo`

NewPnfExtCpInfo instantiates a new PnfExtCpInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPnfExtCpInfoWithDefaults

`func NewPnfExtCpInfoWithDefaults() *PnfExtCpInfo`

NewPnfExtCpInfoWithDefaults instantiates a new PnfExtCpInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCpInstanceId

`func (o *PnfExtCpInfo) GetCpInstanceId() string`

GetCpInstanceId returns the CpInstanceId field if non-nil, zero value otherwise.

### GetCpInstanceIdOk

`func (o *PnfExtCpInfo) GetCpInstanceIdOk() (*string, bool)`

GetCpInstanceIdOk returns a tuple with the CpInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCpInstanceId

`func (o *PnfExtCpInfo) SetCpInstanceId(v string)`

SetCpInstanceId sets CpInstanceId field to given value.


### GetCpdId

`func (o *PnfExtCpInfo) GetCpdId() string`

GetCpdId returns the CpdId field if non-nil, zero value otherwise.

### GetCpdIdOk

`func (o *PnfExtCpInfo) GetCpdIdOk() (*string, bool)`

GetCpdIdOk returns a tuple with the CpdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCpdId

`func (o *PnfExtCpInfo) SetCpdId(v string)`

SetCpdId sets CpdId field to given value.


### GetCpProtocolData

`func (o *PnfExtCpInfo) GetCpProtocolData() []CpProtocolData`

GetCpProtocolData returns the CpProtocolData field if non-nil, zero value otherwise.

### GetCpProtocolDataOk

`func (o *PnfExtCpInfo) GetCpProtocolDataOk() (*[]CpProtocolData, bool)`

GetCpProtocolDataOk returns a tuple with the CpProtocolData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCpProtocolData

`func (o *PnfExtCpInfo) SetCpProtocolData(v []CpProtocolData)`

SetCpProtocolData sets CpProtocolData field to given value.

### HasCpProtocolData

`func (o *PnfExtCpInfo) HasCpProtocolData() bool`

HasCpProtocolData returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


