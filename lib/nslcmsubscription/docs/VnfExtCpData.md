# VnfExtCpData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CpdId** | **string** | Identifier of the VNF Virtual Link Descriptor (VLD) in the VNFD.  | 
**CpConfig** | Pointer to [**map[string]VnfExtCpConfig**](VnfExtCpConfig.md) | Map of instance data that need to be configured on the CP instances  created from the respective CPD. The key of the map which identifies  the individual VnfExtCpConfig entries is of type \&quot;IdentifierInVnf\&quot;  and is managed by the API consumer. The entries shall be applied by  the VNFM according to the rules of JSON Merge Patch (see IETF RFC 7396 [11]).  See notes 2, 3 and 4.  | [optional] 

## Methods

### NewVnfExtCpData

`func NewVnfExtCpData(cpdId string, ) *VnfExtCpData`

NewVnfExtCpData instantiates a new VnfExtCpData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVnfExtCpDataWithDefaults

`func NewVnfExtCpDataWithDefaults() *VnfExtCpData`

NewVnfExtCpDataWithDefaults instantiates a new VnfExtCpData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCpdId

`func (o *VnfExtCpData) GetCpdId() string`

GetCpdId returns the CpdId field if non-nil, zero value otherwise.

### GetCpdIdOk

`func (o *VnfExtCpData) GetCpdIdOk() (*string, bool)`

GetCpdIdOk returns a tuple with the CpdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCpdId

`func (o *VnfExtCpData) SetCpdId(v string)`

SetCpdId sets CpdId field to given value.


### GetCpConfig

`func (o *VnfExtCpData) GetCpConfig() map[string]VnfExtCpConfig`

GetCpConfig returns the CpConfig field if non-nil, zero value otherwise.

### GetCpConfigOk

`func (o *VnfExtCpData) GetCpConfigOk() (*map[string]VnfExtCpConfig, bool)`

GetCpConfigOk returns a tuple with the CpConfig field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCpConfig

`func (o *VnfExtCpData) SetCpConfig(v map[string]VnfExtCpConfig)`

SetCpConfig sets CpConfig field to given value.

### HasCpConfig

`func (o *VnfExtCpData) HasCpConfig() bool`

HasCpConfig returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


