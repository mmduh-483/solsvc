# MscsEndpointInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MscsEndpointId** | **string** | An identifier with the intention of being globally unique.  | 
**Directionality** | **string** | Directionality of the data traffic in the context of the terminating MSCS endpoint from WAN’s perspective. Permitted values: - INBOUND: to indicate into the WAN. - OUTBOUND: to indicate from the WAN. - BOTH: to indicate bidirectional data traffic to/from the WAN.  | 
**ConnectivityServiceEndpoinId** | **[]string** | References the connectivity service endpoint configuration information applicable to support the MSCS endpoint. More than one connectivity service endpoint can be referred when endpoints are in LAG mode.  | 

## Methods

### NewMscsEndpointInfo

`func NewMscsEndpointInfo(mscsEndpointId string, directionality string, connectivityServiceEndpoinId []string, ) *MscsEndpointInfo`

NewMscsEndpointInfo instantiates a new MscsEndpointInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMscsEndpointInfoWithDefaults

`func NewMscsEndpointInfoWithDefaults() *MscsEndpointInfo`

NewMscsEndpointInfoWithDefaults instantiates a new MscsEndpointInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMscsEndpointId

`func (o *MscsEndpointInfo) GetMscsEndpointId() string`

GetMscsEndpointId returns the MscsEndpointId field if non-nil, zero value otherwise.

### GetMscsEndpointIdOk

`func (o *MscsEndpointInfo) GetMscsEndpointIdOk() (*string, bool)`

GetMscsEndpointIdOk returns a tuple with the MscsEndpointId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMscsEndpointId

`func (o *MscsEndpointInfo) SetMscsEndpointId(v string)`

SetMscsEndpointId sets MscsEndpointId field to given value.


### GetDirectionality

`func (o *MscsEndpointInfo) GetDirectionality() string`

GetDirectionality returns the Directionality field if non-nil, zero value otherwise.

### GetDirectionalityOk

`func (o *MscsEndpointInfo) GetDirectionalityOk() (*string, bool)`

GetDirectionalityOk returns a tuple with the Directionality field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDirectionality

`func (o *MscsEndpointInfo) SetDirectionality(v string)`

SetDirectionality sets Directionality field to given value.


### GetConnectivityServiceEndpoinId

`func (o *MscsEndpointInfo) GetConnectivityServiceEndpoinId() []string`

GetConnectivityServiceEndpoinId returns the ConnectivityServiceEndpoinId field if non-nil, zero value otherwise.

### GetConnectivityServiceEndpoinIdOk

`func (o *MscsEndpointInfo) GetConnectivityServiceEndpoinIdOk() (*[]string, bool)`

GetConnectivityServiceEndpoinIdOk returns a tuple with the ConnectivityServiceEndpoinId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConnectivityServiceEndpoinId

`func (o *MscsEndpointInfo) SetConnectivityServiceEndpoinId(v []string)`

SetConnectivityServiceEndpoinId sets ConnectivityServiceEndpoinId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


