# LifecycleChangeNotificationsFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NsInstanceSubscriptionFilter** | Pointer to [**NsInstanceSubscriptionFilter**](NsInstanceSubscriptionFilter.md) |  | [optional] 
**NotificationTypes** | Pointer to **[]string** | Match particular notification types. Permitted values: - NsLcmOperationOccurrenceNotification - NsIdentifierCreationNotification - NsIdentifierDeletionNotification - NsLcmCapacityShortageNotification - NsChangeNotification See note.  | [optional] 
**OperationTypes** | Pointer to [**[]NsLcmOpType**](NsLcmOpType.md) | Match particular NS lifecycle operation types for the notification of type NsLcmOperationOccurrenceNotification. May be present if the \&quot;notificationTypes\&quot; attribute contains the value \&quot;NsLcmOperationOccurrenceNotification\&quot;, and shall be absent otherwise.  | [optional] 
**OperationStates** | Pointer to [**[]NsLcmOperationStateType**](NsLcmOperationStateType.md) | Match particular LCM operation state values as reported in notifications of type NsLcmOperationOccurrenceNotification. May be present if the \&quot;notificationTypes\&quot; attribute contains the value \&quot;NsLcmOperationOccurrenceNotification\&quot;, and shall be absent otherwise.  | [optional] 
**AffectedNsInstanceIds** | Pointer to **[]string** | Match particular identifiers of the NS instance(s) related to the operation occurrence that were affected by the shortage as reported in notifications of type NsLcmCapacityShortageNotification. May be present if the \&quot;notificationTypes\&quot; attribute contains the value \&quot;NsLcmCapacityShortageNotification\&quot;, and shall be absent otherwise.  | [optional] 
**NsComponentTypes** | Pointer to [**[]NsComponentType**](NsComponentType.md) | Match particular NS component types for the notification of type NsChangeNotification. May be present if the \&quot;notificationTypes\&quot; attribute contains the value \&quot;NsChang.  | [optional] 
**LcmOpNameImpactingNsComponent** | Pointer to [**[]LcmOpNameForChangeNotificationType**](LcmOpNameForChangeNotificationType.md) | Match particular LCM operation names for the notification of type NsChangeNotification. May be present if the \&quot;notificationTypes\&quot; attribute contains the value \&quot;NsChangeNotification\&quot;, and shall be absent otherwise.  | [optional] 
**LcmOpOccStatusImpactingNsComponent** | Pointer to [**[]LcmOpOccStatusForChangeNotificationType**](LcmOpOccStatusForChangeNotificationType.md) | Match particular LCM operation status values as reported in notifications of type NsChangeNotification. May be present if the \&quot;notificationTypes\&quot; attribute contains the value \&quot;NsChangeNotification\&quot;, and shall be absent otherwise.  | [optional] 

## Methods

### NewLifecycleChangeNotificationsFilter

`func NewLifecycleChangeNotificationsFilter() *LifecycleChangeNotificationsFilter`

NewLifecycleChangeNotificationsFilter instantiates a new LifecycleChangeNotificationsFilter object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewLifecycleChangeNotificationsFilterWithDefaults

`func NewLifecycleChangeNotificationsFilterWithDefaults() *LifecycleChangeNotificationsFilter`

NewLifecycleChangeNotificationsFilterWithDefaults instantiates a new LifecycleChangeNotificationsFilter object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetNsInstanceSubscriptionFilter

`func (o *LifecycleChangeNotificationsFilter) GetNsInstanceSubscriptionFilter() NsInstanceSubscriptionFilter`

GetNsInstanceSubscriptionFilter returns the NsInstanceSubscriptionFilter field if non-nil, zero value otherwise.

### GetNsInstanceSubscriptionFilterOk

`func (o *LifecycleChangeNotificationsFilter) GetNsInstanceSubscriptionFilterOk() (*NsInstanceSubscriptionFilter, bool)`

GetNsInstanceSubscriptionFilterOk returns a tuple with the NsInstanceSubscriptionFilter field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsInstanceSubscriptionFilter

`func (o *LifecycleChangeNotificationsFilter) SetNsInstanceSubscriptionFilter(v NsInstanceSubscriptionFilter)`

SetNsInstanceSubscriptionFilter sets NsInstanceSubscriptionFilter field to given value.

### HasNsInstanceSubscriptionFilter

`func (o *LifecycleChangeNotificationsFilter) HasNsInstanceSubscriptionFilter() bool`

HasNsInstanceSubscriptionFilter returns a boolean if a field has been set.

### GetNotificationTypes

`func (o *LifecycleChangeNotificationsFilter) GetNotificationTypes() []string`

GetNotificationTypes returns the NotificationTypes field if non-nil, zero value otherwise.

### GetNotificationTypesOk

`func (o *LifecycleChangeNotificationsFilter) GetNotificationTypesOk() (*[]string, bool)`

GetNotificationTypesOk returns a tuple with the NotificationTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNotificationTypes

`func (o *LifecycleChangeNotificationsFilter) SetNotificationTypes(v []string)`

SetNotificationTypes sets NotificationTypes field to given value.

### HasNotificationTypes

`func (o *LifecycleChangeNotificationsFilter) HasNotificationTypes() bool`

HasNotificationTypes returns a boolean if a field has been set.

### GetOperationTypes

`func (o *LifecycleChangeNotificationsFilter) GetOperationTypes() []NsLcmOpType`

GetOperationTypes returns the OperationTypes field if non-nil, zero value otherwise.

### GetOperationTypesOk

`func (o *LifecycleChangeNotificationsFilter) GetOperationTypesOk() (*[]NsLcmOpType, bool)`

GetOperationTypesOk returns a tuple with the OperationTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOperationTypes

`func (o *LifecycleChangeNotificationsFilter) SetOperationTypes(v []NsLcmOpType)`

SetOperationTypes sets OperationTypes field to given value.

### HasOperationTypes

`func (o *LifecycleChangeNotificationsFilter) HasOperationTypes() bool`

HasOperationTypes returns a boolean if a field has been set.

### GetOperationStates

`func (o *LifecycleChangeNotificationsFilter) GetOperationStates() []NsLcmOperationStateType`

GetOperationStates returns the OperationStates field if non-nil, zero value otherwise.

### GetOperationStatesOk

`func (o *LifecycleChangeNotificationsFilter) GetOperationStatesOk() (*[]NsLcmOperationStateType, bool)`

GetOperationStatesOk returns a tuple with the OperationStates field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOperationStates

`func (o *LifecycleChangeNotificationsFilter) SetOperationStates(v []NsLcmOperationStateType)`

SetOperationStates sets OperationStates field to given value.

### HasOperationStates

`func (o *LifecycleChangeNotificationsFilter) HasOperationStates() bool`

HasOperationStates returns a boolean if a field has been set.

### GetAffectedNsInstanceIds

`func (o *LifecycleChangeNotificationsFilter) GetAffectedNsInstanceIds() []string`

GetAffectedNsInstanceIds returns the AffectedNsInstanceIds field if non-nil, zero value otherwise.

### GetAffectedNsInstanceIdsOk

`func (o *LifecycleChangeNotificationsFilter) GetAffectedNsInstanceIdsOk() (*[]string, bool)`

GetAffectedNsInstanceIdsOk returns a tuple with the AffectedNsInstanceIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAffectedNsInstanceIds

`func (o *LifecycleChangeNotificationsFilter) SetAffectedNsInstanceIds(v []string)`

SetAffectedNsInstanceIds sets AffectedNsInstanceIds field to given value.

### HasAffectedNsInstanceIds

`func (o *LifecycleChangeNotificationsFilter) HasAffectedNsInstanceIds() bool`

HasAffectedNsInstanceIds returns a boolean if a field has been set.

### GetNsComponentTypes

`func (o *LifecycleChangeNotificationsFilter) GetNsComponentTypes() []NsComponentType`

GetNsComponentTypes returns the NsComponentTypes field if non-nil, zero value otherwise.

### GetNsComponentTypesOk

`func (o *LifecycleChangeNotificationsFilter) GetNsComponentTypesOk() (*[]NsComponentType, bool)`

GetNsComponentTypesOk returns a tuple with the NsComponentTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsComponentTypes

`func (o *LifecycleChangeNotificationsFilter) SetNsComponentTypes(v []NsComponentType)`

SetNsComponentTypes sets NsComponentTypes field to given value.

### HasNsComponentTypes

`func (o *LifecycleChangeNotificationsFilter) HasNsComponentTypes() bool`

HasNsComponentTypes returns a boolean if a field has been set.

### GetLcmOpNameImpactingNsComponent

`func (o *LifecycleChangeNotificationsFilter) GetLcmOpNameImpactingNsComponent() []LcmOpNameForChangeNotificationType`

GetLcmOpNameImpactingNsComponent returns the LcmOpNameImpactingNsComponent field if non-nil, zero value otherwise.

### GetLcmOpNameImpactingNsComponentOk

`func (o *LifecycleChangeNotificationsFilter) GetLcmOpNameImpactingNsComponentOk() (*[]LcmOpNameForChangeNotificationType, bool)`

GetLcmOpNameImpactingNsComponentOk returns a tuple with the LcmOpNameImpactingNsComponent field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLcmOpNameImpactingNsComponent

`func (o *LifecycleChangeNotificationsFilter) SetLcmOpNameImpactingNsComponent(v []LcmOpNameForChangeNotificationType)`

SetLcmOpNameImpactingNsComponent sets LcmOpNameImpactingNsComponent field to given value.

### HasLcmOpNameImpactingNsComponent

`func (o *LifecycleChangeNotificationsFilter) HasLcmOpNameImpactingNsComponent() bool`

HasLcmOpNameImpactingNsComponent returns a boolean if a field has been set.

### GetLcmOpOccStatusImpactingNsComponent

`func (o *LifecycleChangeNotificationsFilter) GetLcmOpOccStatusImpactingNsComponent() []LcmOpOccStatusForChangeNotificationType`

GetLcmOpOccStatusImpactingNsComponent returns the LcmOpOccStatusImpactingNsComponent field if non-nil, zero value otherwise.

### GetLcmOpOccStatusImpactingNsComponentOk

`func (o *LifecycleChangeNotificationsFilter) GetLcmOpOccStatusImpactingNsComponentOk() (*[]LcmOpOccStatusForChangeNotificationType, bool)`

GetLcmOpOccStatusImpactingNsComponentOk returns a tuple with the LcmOpOccStatusImpactingNsComponent field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLcmOpOccStatusImpactingNsComponent

`func (o *LifecycleChangeNotificationsFilter) SetLcmOpOccStatusImpactingNsComponent(v []LcmOpOccStatusForChangeNotificationType)`

SetLcmOpOccStatusImpactingNsComponent sets LcmOpOccStatusImpactingNsComponent field to given value.

### HasLcmOpOccStatusImpactingNsComponent

`func (o *LifecycleChangeNotificationsFilter) HasLcmOpOccStatusImpactingNsComponent() bool`

HasLcmOpOccStatusImpactingNsComponent returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


