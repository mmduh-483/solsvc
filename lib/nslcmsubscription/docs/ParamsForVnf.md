# ParamsForVnf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VnfProfileId** | **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | 
**VnfInstanceName** | Pointer to **string** | Human-readable name of the VNF instance to be created.  | [optional] 
**VnfInstanceDescription** | Pointer to **string** | Human-readable description of the VNF instance to be created.  | [optional] 
**VnfConfigurableProperties** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 
**Metadata** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 
**Extensions** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 
**AdditionalParams** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 

## Methods

### NewParamsForVnf

`func NewParamsForVnf(vnfProfileId string, ) *ParamsForVnf`

NewParamsForVnf instantiates a new ParamsForVnf object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewParamsForVnfWithDefaults

`func NewParamsForVnfWithDefaults() *ParamsForVnf`

NewParamsForVnfWithDefaults instantiates a new ParamsForVnf object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVnfProfileId

`func (o *ParamsForVnf) GetVnfProfileId() string`

GetVnfProfileId returns the VnfProfileId field if non-nil, zero value otherwise.

### GetVnfProfileIdOk

`func (o *ParamsForVnf) GetVnfProfileIdOk() (*string, bool)`

GetVnfProfileIdOk returns a tuple with the VnfProfileId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfProfileId

`func (o *ParamsForVnf) SetVnfProfileId(v string)`

SetVnfProfileId sets VnfProfileId field to given value.


### GetVnfInstanceName

`func (o *ParamsForVnf) GetVnfInstanceName() string`

GetVnfInstanceName returns the VnfInstanceName field if non-nil, zero value otherwise.

### GetVnfInstanceNameOk

`func (o *ParamsForVnf) GetVnfInstanceNameOk() (*string, bool)`

GetVnfInstanceNameOk returns a tuple with the VnfInstanceName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceName

`func (o *ParamsForVnf) SetVnfInstanceName(v string)`

SetVnfInstanceName sets VnfInstanceName field to given value.

### HasVnfInstanceName

`func (o *ParamsForVnf) HasVnfInstanceName() bool`

HasVnfInstanceName returns a boolean if a field has been set.

### GetVnfInstanceDescription

`func (o *ParamsForVnf) GetVnfInstanceDescription() string`

GetVnfInstanceDescription returns the VnfInstanceDescription field if non-nil, zero value otherwise.

### GetVnfInstanceDescriptionOk

`func (o *ParamsForVnf) GetVnfInstanceDescriptionOk() (*string, bool)`

GetVnfInstanceDescriptionOk returns a tuple with the VnfInstanceDescription field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceDescription

`func (o *ParamsForVnf) SetVnfInstanceDescription(v string)`

SetVnfInstanceDescription sets VnfInstanceDescription field to given value.

### HasVnfInstanceDescription

`func (o *ParamsForVnf) HasVnfInstanceDescription() bool`

HasVnfInstanceDescription returns a boolean if a field has been set.

### GetVnfConfigurableProperties

`func (o *ParamsForVnf) GetVnfConfigurableProperties() map[string]interface{}`

GetVnfConfigurableProperties returns the VnfConfigurableProperties field if non-nil, zero value otherwise.

### GetVnfConfigurablePropertiesOk

`func (o *ParamsForVnf) GetVnfConfigurablePropertiesOk() (*map[string]interface{}, bool)`

GetVnfConfigurablePropertiesOk returns a tuple with the VnfConfigurableProperties field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfConfigurableProperties

`func (o *ParamsForVnf) SetVnfConfigurableProperties(v map[string]interface{})`

SetVnfConfigurableProperties sets VnfConfigurableProperties field to given value.

### HasVnfConfigurableProperties

`func (o *ParamsForVnf) HasVnfConfigurableProperties() bool`

HasVnfConfigurableProperties returns a boolean if a field has been set.

### GetMetadata

`func (o *ParamsForVnf) GetMetadata() map[string]interface{}`

GetMetadata returns the Metadata field if non-nil, zero value otherwise.

### GetMetadataOk

`func (o *ParamsForVnf) GetMetadataOk() (*map[string]interface{}, bool)`

GetMetadataOk returns a tuple with the Metadata field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMetadata

`func (o *ParamsForVnf) SetMetadata(v map[string]interface{})`

SetMetadata sets Metadata field to given value.

### HasMetadata

`func (o *ParamsForVnf) HasMetadata() bool`

HasMetadata returns a boolean if a field has been set.

### GetExtensions

`func (o *ParamsForVnf) GetExtensions() map[string]interface{}`

GetExtensions returns the Extensions field if non-nil, zero value otherwise.

### GetExtensionsOk

`func (o *ParamsForVnf) GetExtensionsOk() (*map[string]interface{}, bool)`

GetExtensionsOk returns a tuple with the Extensions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtensions

`func (o *ParamsForVnf) SetExtensions(v map[string]interface{})`

SetExtensions sets Extensions field to given value.

### HasExtensions

`func (o *ParamsForVnf) HasExtensions() bool`

HasExtensions returns a boolean if a field has been set.

### GetAdditionalParams

`func (o *ParamsForVnf) GetAdditionalParams() map[string]interface{}`

GetAdditionalParams returns the AdditionalParams field if non-nil, zero value otherwise.

### GetAdditionalParamsOk

`func (o *ParamsForVnf) GetAdditionalParamsOk() (*map[string]interface{}, bool)`

GetAdditionalParamsOk returns a tuple with the AdditionalParams field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdditionalParams

`func (o *ParamsForVnf) SetAdditionalParams(v map[string]interface{})`

SetAdditionalParams sets AdditionalParams field to given value.

### HasAdditionalParams

`func (o *ParamsForVnf) HasAdditionalParams() bool`

HasAdditionalParams returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


