# SiteToWanLayer3ProtocolData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LogicalInterfaceIpAddress** | Pointer to [**SiteToWanLayer3ProtocolDataLogicalInterfaceIpAddress**](SiteToWanLayer3ProtocolDataLogicalInterfaceIpAddress.md) |  | [optional] 
**RoutingInfo** | [**SiteToWanLayer3ProtocolDataRoutingInfo**](SiteToWanLayer3ProtocolDataRoutingInfo.md) |  | 
**MtuL3** | Pointer to **float32** | Maximum Transmission Unit (MTU) that can be forwarded at layer 3 (in bytes). Default value is \&quot;1500\&quot; (bytes).  | [optional] 
**VirtualRoutingAndForwarding** | Pointer to [**SiteToWanLayer3ProtocolDataVirtualRoutingAndForwarding**](SiteToWanLayer3ProtocolDataVirtualRoutingAndForwarding.md) |  | [optional] 
**BfdConfig** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 

## Methods

### NewSiteToWanLayer3ProtocolData

`func NewSiteToWanLayer3ProtocolData(routingInfo SiteToWanLayer3ProtocolDataRoutingInfo, ) *SiteToWanLayer3ProtocolData`

NewSiteToWanLayer3ProtocolData instantiates a new SiteToWanLayer3ProtocolData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSiteToWanLayer3ProtocolDataWithDefaults

`func NewSiteToWanLayer3ProtocolDataWithDefaults() *SiteToWanLayer3ProtocolData`

NewSiteToWanLayer3ProtocolDataWithDefaults instantiates a new SiteToWanLayer3ProtocolData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLogicalInterfaceIpAddress

`func (o *SiteToWanLayer3ProtocolData) GetLogicalInterfaceIpAddress() SiteToWanLayer3ProtocolDataLogicalInterfaceIpAddress`

GetLogicalInterfaceIpAddress returns the LogicalInterfaceIpAddress field if non-nil, zero value otherwise.

### GetLogicalInterfaceIpAddressOk

`func (o *SiteToWanLayer3ProtocolData) GetLogicalInterfaceIpAddressOk() (*SiteToWanLayer3ProtocolDataLogicalInterfaceIpAddress, bool)`

GetLogicalInterfaceIpAddressOk returns a tuple with the LogicalInterfaceIpAddress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLogicalInterfaceIpAddress

`func (o *SiteToWanLayer3ProtocolData) SetLogicalInterfaceIpAddress(v SiteToWanLayer3ProtocolDataLogicalInterfaceIpAddress)`

SetLogicalInterfaceIpAddress sets LogicalInterfaceIpAddress field to given value.

### HasLogicalInterfaceIpAddress

`func (o *SiteToWanLayer3ProtocolData) HasLogicalInterfaceIpAddress() bool`

HasLogicalInterfaceIpAddress returns a boolean if a field has been set.

### GetRoutingInfo

`func (o *SiteToWanLayer3ProtocolData) GetRoutingInfo() SiteToWanLayer3ProtocolDataRoutingInfo`

GetRoutingInfo returns the RoutingInfo field if non-nil, zero value otherwise.

### GetRoutingInfoOk

`func (o *SiteToWanLayer3ProtocolData) GetRoutingInfoOk() (*SiteToWanLayer3ProtocolDataRoutingInfo, bool)`

GetRoutingInfoOk returns a tuple with the RoutingInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRoutingInfo

`func (o *SiteToWanLayer3ProtocolData) SetRoutingInfo(v SiteToWanLayer3ProtocolDataRoutingInfo)`

SetRoutingInfo sets RoutingInfo field to given value.


### GetMtuL3

`func (o *SiteToWanLayer3ProtocolData) GetMtuL3() float32`

GetMtuL3 returns the MtuL3 field if non-nil, zero value otherwise.

### GetMtuL3Ok

`func (o *SiteToWanLayer3ProtocolData) GetMtuL3Ok() (*float32, bool)`

GetMtuL3Ok returns a tuple with the MtuL3 field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMtuL3

`func (o *SiteToWanLayer3ProtocolData) SetMtuL3(v float32)`

SetMtuL3 sets MtuL3 field to given value.

### HasMtuL3

`func (o *SiteToWanLayer3ProtocolData) HasMtuL3() bool`

HasMtuL3 returns a boolean if a field has been set.

### GetVirtualRoutingAndForwarding

`func (o *SiteToWanLayer3ProtocolData) GetVirtualRoutingAndForwarding() SiteToWanLayer3ProtocolDataVirtualRoutingAndForwarding`

GetVirtualRoutingAndForwarding returns the VirtualRoutingAndForwarding field if non-nil, zero value otherwise.

### GetVirtualRoutingAndForwardingOk

`func (o *SiteToWanLayer3ProtocolData) GetVirtualRoutingAndForwardingOk() (*SiteToWanLayer3ProtocolDataVirtualRoutingAndForwarding, bool)`

GetVirtualRoutingAndForwardingOk returns a tuple with the VirtualRoutingAndForwarding field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVirtualRoutingAndForwarding

`func (o *SiteToWanLayer3ProtocolData) SetVirtualRoutingAndForwarding(v SiteToWanLayer3ProtocolDataVirtualRoutingAndForwarding)`

SetVirtualRoutingAndForwarding sets VirtualRoutingAndForwarding field to given value.

### HasVirtualRoutingAndForwarding

`func (o *SiteToWanLayer3ProtocolData) HasVirtualRoutingAndForwarding() bool`

HasVirtualRoutingAndForwarding returns a boolean if a field has been set.

### GetBfdConfig

`func (o *SiteToWanLayer3ProtocolData) GetBfdConfig() map[string]interface{}`

GetBfdConfig returns the BfdConfig field if non-nil, zero value otherwise.

### GetBfdConfigOk

`func (o *SiteToWanLayer3ProtocolData) GetBfdConfigOk() (*map[string]interface{}, bool)`

GetBfdConfigOk returns a tuple with the BfdConfig field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBfdConfig

`func (o *SiteToWanLayer3ProtocolData) SetBfdConfig(v map[string]interface{})`

SetBfdConfig sets BfdConfig field to given value.

### HasBfdConfig

`func (o *SiteToWanLayer3ProtocolData) HasBfdConfig() bool`

HasBfdConfig returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


