# \DefaultApi

All URIs are relative to *http://127.0.0.1/nslcm/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**SubscriptionsGet**](DefaultApi.md#SubscriptionsGet) | **Get** /subscriptions | 
[**SubscriptionsPost**](DefaultApi.md#SubscriptionsPost) | **Post** /subscriptions | 
[**SubscriptionsSubscriptionIdDelete**](DefaultApi.md#SubscriptionsSubscriptionIdDelete) | **Delete** /subscriptions/{subscriptionId} | 
[**SubscriptionsSubscriptionIdGet**](DefaultApi.md#SubscriptionsSubscriptionIdGet) | **Get** /subscriptions/{subscriptionId} | 



## SubscriptionsGet

> []LccnSubscription SubscriptionsGet(ctx).Version(version).Accept(accept).Authorization(authorization).Filter(filter).NextpageOpaqueMarker(nextpageOpaqueMarker).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    version := "version_example" // string | Version of the API requested to use when responding to this request. 
    accept := "accept_example" // string | Content-Types that are acceptable for the response. Reference: IETF RFC 7231. 
    authorization := "authorization_example" // string | The authorization token for the request. Reference: IETF RFC 7235.  (optional)
    filter := "filter_example" // string | Attribute-based filtering expression according to clause 5.2 of ETSI GS NFV SOL 013. The NFVO shall support receiving this parameter as part of the URI query string.  The OSS/BSS may supply this parameter. All attribute names that appear in the LccnSubscription and in data types referenced  from it shall be supported by the NFVO in the filter expression.  (optional)
    nextpageOpaqueMarker := "nextpageOpaqueMarker_example" // string | Marker to obtain the next page of a paged response. Shall be supported by the NFV-MANO functional entity if the entity supports alternative 2 (paging) according to clause 5.4.2.1 of ETSI GS NFV-SOL 013 for this resource.  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultApi.SubscriptionsGet(context.Background()).Version(version).Accept(accept).Authorization(authorization).Filter(filter).NextpageOpaqueMarker(nextpageOpaqueMarker).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.SubscriptionsGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `SubscriptionsGet`: []LccnSubscription
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.SubscriptionsGet`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiSubscriptionsGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **version** | **string** | Version of the API requested to use when responding to this request.  | 
 **accept** | **string** | Content-Types that are acceptable for the response. Reference: IETF RFC 7231.  | 
 **authorization** | **string** | The authorization token for the request. Reference: IETF RFC 7235.  | 
 **filter** | **string** | Attribute-based filtering expression according to clause 5.2 of ETSI GS NFV SOL 013. The NFVO shall support receiving this parameter as part of the URI query string.  The OSS/BSS may supply this parameter. All attribute names that appear in the LccnSubscription and in data types referenced  from it shall be supported by the NFVO in the filter expression.  | 
 **nextpageOpaqueMarker** | **string** | Marker to obtain the next page of a paged response. Shall be supported by the NFV-MANO functional entity if the entity supports alternative 2 (paging) according to clause 5.4.2.1 of ETSI GS NFV-SOL 013 for this resource.  | 

### Return type

[**[]LccnSubscription**](LccnSubscription.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SubscriptionsPost

> LccnSubscription SubscriptionsPost(ctx).Version(version).Accept(accept).ContentType(contentType).LccnSubscriptionRequest(lccnSubscriptionRequest).Authorization(authorization).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    version := "version_example" // string | Version of the API requested to use when responding to this request. 
    accept := "accept_example" // string | Content-Types that are acceptable for the response. Reference: IETF RFC 7231. 
    contentType := "contentType_example" // string | The MIME type of the body of the request. Reference: IETF RFC 7231 
    lccnSubscriptionRequest := *openapiclient.NewLccnSubscriptionRequest("CallbackUri_example") // LccnSubscriptionRequest | Details of the subscription to be created, as defined in clause 6.5.2.2. 
    authorization := "authorization_example" // string | The authorization token for the request. Reference: IETF RFC 7235.  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultApi.SubscriptionsPost(context.Background()).Version(version).Accept(accept).ContentType(contentType).LccnSubscriptionRequest(lccnSubscriptionRequest).Authorization(authorization).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.SubscriptionsPost``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `SubscriptionsPost`: LccnSubscription
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.SubscriptionsPost`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiSubscriptionsPostRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **version** | **string** | Version of the API requested to use when responding to this request.  | 
 **accept** | **string** | Content-Types that are acceptable for the response. Reference: IETF RFC 7231.  | 
 **contentType** | **string** | The MIME type of the body of the request. Reference: IETF RFC 7231  | 
 **lccnSubscriptionRequest** | [**LccnSubscriptionRequest**](LccnSubscriptionRequest.md) | Details of the subscription to be created, as defined in clause 6.5.2.2.  | 
 **authorization** | **string** | The authorization token for the request. Reference: IETF RFC 7235.  | 

### Return type

[**LccnSubscription**](LccnSubscription.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SubscriptionsSubscriptionIdDelete

> SubscriptionsSubscriptionIdDelete(ctx, subscriptionId).Version(version).Authorization(authorization).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    subscriptionId := "subscriptionId_example" // string | Identifier of this subscription. 
    version := "version_example" // string | Version of the API requested to use when responding to this request. 
    authorization := "authorization_example" // string | The authorization token for the request. Reference: IETF RFC 7235.  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultApi.SubscriptionsSubscriptionIdDelete(context.Background(), subscriptionId).Version(version).Authorization(authorization).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.SubscriptionsSubscriptionIdDelete``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**subscriptionId** | **string** | Identifier of this subscription.  | 

### Other Parameters

Other parameters are passed through a pointer to a apiSubscriptionsSubscriptionIdDeleteRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **version** | **string** | Version of the API requested to use when responding to this request.  | 
 **authorization** | **string** | The authorization token for the request. Reference: IETF RFC 7235.  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SubscriptionsSubscriptionIdGet

> LccnSubscription SubscriptionsSubscriptionIdGet(ctx, subscriptionId).Version(version).Accept(accept).Authorization(authorization).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    subscriptionId := "subscriptionId_example" // string | Identifier of this subscription. 
    version := "version_example" // string | Version of the API requested to use when responding to this request. 
    accept := "accept_example" // string | Content-Types that are acceptable for the response. Reference: IETF RFC 7231. 
    authorization := "authorization_example" // string | The authorization token for the request. Reference: IETF RFC 7235.  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultApi.SubscriptionsSubscriptionIdGet(context.Background(), subscriptionId).Version(version).Accept(accept).Authorization(authorization).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.SubscriptionsSubscriptionIdGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `SubscriptionsSubscriptionIdGet`: LccnSubscription
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.SubscriptionsSubscriptionIdGet`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**subscriptionId** | **string** | Identifier of this subscription.  | 

### Other Parameters

Other parameters are passed through a pointer to a apiSubscriptionsSubscriptionIdGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **version** | **string** | Version of the API requested to use when responding to this request.  | 
 **accept** | **string** | Content-Types that are acceptable for the response. Reference: IETF RFC 7231.  | 
 **authorization** | **string** | The authorization token for the request. Reference: IETF RFC 7235.  | 

### Return type

[**LccnSubscription**](LccnSubscription.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

