# VnfExtCpInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | An identifier that is unique for the respective type within a VNF instance, but may not be globally unique.  | 
**CpdId** | **string** | Identifier of the VNF Virtual Link Descriptor (VLD) in the VNFD.  | 
**CpConfigId** | Pointer to **string** | Identifier of the VNF Virtual Link Descriptor (VLD) in the VNFD.  | [optional] 
**VnfdId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**CpProtocolInfo** | Pointer to [**[]CpProtocolInfo**](CpProtocolInfo.md) | Network protocol information for this CP.  | [optional] 
**ExtLinkPortId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**Metadata** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 
**AssociatedVnfcCpId** | Pointer to **string** | An identifier that is unique for the respective type within a VNF instance, but may not be globally unique.  | [optional] 
**AssociatedVipCpId** | Pointer to **string** | An identifier that is unique for the respective type within a VNF instance, but may not be globally unique.  | [optional] 
**AssociatedVnfVirtualLinkId** | Pointer to **string** | An identifier that is unique for the respective type within a VNF instance, but may not be globally unique.  | [optional] 

## Methods

### NewVnfExtCpInfo

`func NewVnfExtCpInfo(id string, cpdId string, ) *VnfExtCpInfo`

NewVnfExtCpInfo instantiates a new VnfExtCpInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVnfExtCpInfoWithDefaults

`func NewVnfExtCpInfoWithDefaults() *VnfExtCpInfo`

NewVnfExtCpInfoWithDefaults instantiates a new VnfExtCpInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *VnfExtCpInfo) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *VnfExtCpInfo) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *VnfExtCpInfo) SetId(v string)`

SetId sets Id field to given value.


### GetCpdId

`func (o *VnfExtCpInfo) GetCpdId() string`

GetCpdId returns the CpdId field if non-nil, zero value otherwise.

### GetCpdIdOk

`func (o *VnfExtCpInfo) GetCpdIdOk() (*string, bool)`

GetCpdIdOk returns a tuple with the CpdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCpdId

`func (o *VnfExtCpInfo) SetCpdId(v string)`

SetCpdId sets CpdId field to given value.


### GetCpConfigId

`func (o *VnfExtCpInfo) GetCpConfigId() string`

GetCpConfigId returns the CpConfigId field if non-nil, zero value otherwise.

### GetCpConfigIdOk

`func (o *VnfExtCpInfo) GetCpConfigIdOk() (*string, bool)`

GetCpConfigIdOk returns a tuple with the CpConfigId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCpConfigId

`func (o *VnfExtCpInfo) SetCpConfigId(v string)`

SetCpConfigId sets CpConfigId field to given value.

### HasCpConfigId

`func (o *VnfExtCpInfo) HasCpConfigId() bool`

HasCpConfigId returns a boolean if a field has been set.

### GetVnfdId

`func (o *VnfExtCpInfo) GetVnfdId() string`

GetVnfdId returns the VnfdId field if non-nil, zero value otherwise.

### GetVnfdIdOk

`func (o *VnfExtCpInfo) GetVnfdIdOk() (*string, bool)`

GetVnfdIdOk returns a tuple with the VnfdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfdId

`func (o *VnfExtCpInfo) SetVnfdId(v string)`

SetVnfdId sets VnfdId field to given value.

### HasVnfdId

`func (o *VnfExtCpInfo) HasVnfdId() bool`

HasVnfdId returns a boolean if a field has been set.

### GetCpProtocolInfo

`func (o *VnfExtCpInfo) GetCpProtocolInfo() []CpProtocolInfo`

GetCpProtocolInfo returns the CpProtocolInfo field if non-nil, zero value otherwise.

### GetCpProtocolInfoOk

`func (o *VnfExtCpInfo) GetCpProtocolInfoOk() (*[]CpProtocolInfo, bool)`

GetCpProtocolInfoOk returns a tuple with the CpProtocolInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCpProtocolInfo

`func (o *VnfExtCpInfo) SetCpProtocolInfo(v []CpProtocolInfo)`

SetCpProtocolInfo sets CpProtocolInfo field to given value.

### HasCpProtocolInfo

`func (o *VnfExtCpInfo) HasCpProtocolInfo() bool`

HasCpProtocolInfo returns a boolean if a field has been set.

### GetExtLinkPortId

`func (o *VnfExtCpInfo) GetExtLinkPortId() string`

GetExtLinkPortId returns the ExtLinkPortId field if non-nil, zero value otherwise.

### GetExtLinkPortIdOk

`func (o *VnfExtCpInfo) GetExtLinkPortIdOk() (*string, bool)`

GetExtLinkPortIdOk returns a tuple with the ExtLinkPortId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtLinkPortId

`func (o *VnfExtCpInfo) SetExtLinkPortId(v string)`

SetExtLinkPortId sets ExtLinkPortId field to given value.

### HasExtLinkPortId

`func (o *VnfExtCpInfo) HasExtLinkPortId() bool`

HasExtLinkPortId returns a boolean if a field has been set.

### GetMetadata

`func (o *VnfExtCpInfo) GetMetadata() map[string]interface{}`

GetMetadata returns the Metadata field if non-nil, zero value otherwise.

### GetMetadataOk

`func (o *VnfExtCpInfo) GetMetadataOk() (*map[string]interface{}, bool)`

GetMetadataOk returns a tuple with the Metadata field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMetadata

`func (o *VnfExtCpInfo) SetMetadata(v map[string]interface{})`

SetMetadata sets Metadata field to given value.

### HasMetadata

`func (o *VnfExtCpInfo) HasMetadata() bool`

HasMetadata returns a boolean if a field has been set.

### GetAssociatedVnfcCpId

`func (o *VnfExtCpInfo) GetAssociatedVnfcCpId() string`

GetAssociatedVnfcCpId returns the AssociatedVnfcCpId field if non-nil, zero value otherwise.

### GetAssociatedVnfcCpIdOk

`func (o *VnfExtCpInfo) GetAssociatedVnfcCpIdOk() (*string, bool)`

GetAssociatedVnfcCpIdOk returns a tuple with the AssociatedVnfcCpId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAssociatedVnfcCpId

`func (o *VnfExtCpInfo) SetAssociatedVnfcCpId(v string)`

SetAssociatedVnfcCpId sets AssociatedVnfcCpId field to given value.

### HasAssociatedVnfcCpId

`func (o *VnfExtCpInfo) HasAssociatedVnfcCpId() bool`

HasAssociatedVnfcCpId returns a boolean if a field has been set.

### GetAssociatedVipCpId

`func (o *VnfExtCpInfo) GetAssociatedVipCpId() string`

GetAssociatedVipCpId returns the AssociatedVipCpId field if non-nil, zero value otherwise.

### GetAssociatedVipCpIdOk

`func (o *VnfExtCpInfo) GetAssociatedVipCpIdOk() (*string, bool)`

GetAssociatedVipCpIdOk returns a tuple with the AssociatedVipCpId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAssociatedVipCpId

`func (o *VnfExtCpInfo) SetAssociatedVipCpId(v string)`

SetAssociatedVipCpId sets AssociatedVipCpId field to given value.

### HasAssociatedVipCpId

`func (o *VnfExtCpInfo) HasAssociatedVipCpId() bool`

HasAssociatedVipCpId returns a boolean if a field has been set.

### GetAssociatedVnfVirtualLinkId

`func (o *VnfExtCpInfo) GetAssociatedVnfVirtualLinkId() string`

GetAssociatedVnfVirtualLinkId returns the AssociatedVnfVirtualLinkId field if non-nil, zero value otherwise.

### GetAssociatedVnfVirtualLinkIdOk

`func (o *VnfExtCpInfo) GetAssociatedVnfVirtualLinkIdOk() (*string, bool)`

GetAssociatedVnfVirtualLinkIdOk returns a tuple with the AssociatedVnfVirtualLinkId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAssociatedVnfVirtualLinkId

`func (o *VnfExtCpInfo) SetAssociatedVnfVirtualLinkId(v string)`

SetAssociatedVnfVirtualLinkId sets AssociatedVnfVirtualLinkId field to given value.

### HasAssociatedVnfVirtualLinkId

`func (o *VnfExtCpInfo) HasAssociatedVnfVirtualLinkId() bool`

HasAssociatedVnfVirtualLinkId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


