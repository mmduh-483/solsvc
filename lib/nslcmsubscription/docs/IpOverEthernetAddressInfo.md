# IpOverEthernetAddressInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MacAddress** | Pointer to **string** | A MAC address. Representation: string that consists of groups of two hexadecimal digits, separated by hyphens or colons.  | [optional] 
**SegmentationId** | Pointer to **string** | Identification of the network segment to which the Cp instance connects to. See note 3 and note 4.  | [optional] 
**IpAddresses** | Pointer to [**[]OneOfAnyTypeAnyType**](OneOfAnyTypeAnyType.md) | Addresses assigned to the CP instance. Each entry represents IP addresses assigned by fixed or dynamic IP address assignment per subnet. See note 1.  | [optional] 
**Type** | Pointer to **string** | The type of the IP addresses  | [optional] 
**Addresses** | Pointer to **string** | An IPV4 or IPV6 address. Representation: In case of an IPV4 address, string that consists of four decimal integers separated by dots, each integer ranging from 0 to 255. In case of an IPV6 address, string that consists of groups of zero to four hexadecimal digits, separated by colons.  | [optional] 
**IsDynamic** | Pointer to **bool** | Indicates whether this set of addresses was assigned dynamically (true) or based on address information provided as input from the API consumer (false). Shall be present if \&quot;addresses\&quot; is present and shall be absent otherwise.  | [optional] 
**AddressRange** | Pointer to **map[string]interface{}** | An IP address range used, e.g. in case of egress connections. See note.  | [optional] 
**MinAddress** | Pointer to **string** | An IPV4 or IPV6 address. Representation: In case of an IPV4 address, string that consists of four decimal integers separated by dots, each integer ranging from 0 to 255. In case of an IPV6 address, string that consists of groups of zero to four hexadecimal digits, separated by colons.  | [optional] 
**MaxAddress** | Pointer to **string** | An IPV4 or IPV6 address. Representation: In case of an IPV4 address, string that consists of four decimal integers separated by dots, each integer ranging from 0 to 255. In case of an IPV6 address, string that consists of groups of zero to four hexadecimal digits, separated by colons.  | [optional] 
**SubnetId** | Pointer to **string** | An identifier maintained by the VIM or other resource provider. It is expected to be unique within the VIM instance. Representation: string of variable length.  | [optional] 

## Methods

### NewIpOverEthernetAddressInfo

`func NewIpOverEthernetAddressInfo() *IpOverEthernetAddressInfo`

NewIpOverEthernetAddressInfo instantiates a new IpOverEthernetAddressInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewIpOverEthernetAddressInfoWithDefaults

`func NewIpOverEthernetAddressInfoWithDefaults() *IpOverEthernetAddressInfo`

NewIpOverEthernetAddressInfoWithDefaults instantiates a new IpOverEthernetAddressInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMacAddress

`func (o *IpOverEthernetAddressInfo) GetMacAddress() string`

GetMacAddress returns the MacAddress field if non-nil, zero value otherwise.

### GetMacAddressOk

`func (o *IpOverEthernetAddressInfo) GetMacAddressOk() (*string, bool)`

GetMacAddressOk returns a tuple with the MacAddress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMacAddress

`func (o *IpOverEthernetAddressInfo) SetMacAddress(v string)`

SetMacAddress sets MacAddress field to given value.

### HasMacAddress

`func (o *IpOverEthernetAddressInfo) HasMacAddress() bool`

HasMacAddress returns a boolean if a field has been set.

### GetSegmentationId

`func (o *IpOverEthernetAddressInfo) GetSegmentationId() string`

GetSegmentationId returns the SegmentationId field if non-nil, zero value otherwise.

### GetSegmentationIdOk

`func (o *IpOverEthernetAddressInfo) GetSegmentationIdOk() (*string, bool)`

GetSegmentationIdOk returns a tuple with the SegmentationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSegmentationId

`func (o *IpOverEthernetAddressInfo) SetSegmentationId(v string)`

SetSegmentationId sets SegmentationId field to given value.

### HasSegmentationId

`func (o *IpOverEthernetAddressInfo) HasSegmentationId() bool`

HasSegmentationId returns a boolean if a field has been set.

### GetIpAddresses

`func (o *IpOverEthernetAddressInfo) GetIpAddresses() []OneOfAnyTypeAnyType`

GetIpAddresses returns the IpAddresses field if non-nil, zero value otherwise.

### GetIpAddressesOk

`func (o *IpOverEthernetAddressInfo) GetIpAddressesOk() (*[]OneOfAnyTypeAnyType, bool)`

GetIpAddressesOk returns a tuple with the IpAddresses field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIpAddresses

`func (o *IpOverEthernetAddressInfo) SetIpAddresses(v []OneOfAnyTypeAnyType)`

SetIpAddresses sets IpAddresses field to given value.

### HasIpAddresses

`func (o *IpOverEthernetAddressInfo) HasIpAddresses() bool`

HasIpAddresses returns a boolean if a field has been set.

### GetType

`func (o *IpOverEthernetAddressInfo) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *IpOverEthernetAddressInfo) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *IpOverEthernetAddressInfo) SetType(v string)`

SetType sets Type field to given value.

### HasType

`func (o *IpOverEthernetAddressInfo) HasType() bool`

HasType returns a boolean if a field has been set.

### GetAddresses

`func (o *IpOverEthernetAddressInfo) GetAddresses() string`

GetAddresses returns the Addresses field if non-nil, zero value otherwise.

### GetAddressesOk

`func (o *IpOverEthernetAddressInfo) GetAddressesOk() (*string, bool)`

GetAddressesOk returns a tuple with the Addresses field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddresses

`func (o *IpOverEthernetAddressInfo) SetAddresses(v string)`

SetAddresses sets Addresses field to given value.

### HasAddresses

`func (o *IpOverEthernetAddressInfo) HasAddresses() bool`

HasAddresses returns a boolean if a field has been set.

### GetIsDynamic

`func (o *IpOverEthernetAddressInfo) GetIsDynamic() bool`

GetIsDynamic returns the IsDynamic field if non-nil, zero value otherwise.

### GetIsDynamicOk

`func (o *IpOverEthernetAddressInfo) GetIsDynamicOk() (*bool, bool)`

GetIsDynamicOk returns a tuple with the IsDynamic field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIsDynamic

`func (o *IpOverEthernetAddressInfo) SetIsDynamic(v bool)`

SetIsDynamic sets IsDynamic field to given value.

### HasIsDynamic

`func (o *IpOverEthernetAddressInfo) HasIsDynamic() bool`

HasIsDynamic returns a boolean if a field has been set.

### GetAddressRange

`func (o *IpOverEthernetAddressInfo) GetAddressRange() map[string]interface{}`

GetAddressRange returns the AddressRange field if non-nil, zero value otherwise.

### GetAddressRangeOk

`func (o *IpOverEthernetAddressInfo) GetAddressRangeOk() (*map[string]interface{}, bool)`

GetAddressRangeOk returns a tuple with the AddressRange field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddressRange

`func (o *IpOverEthernetAddressInfo) SetAddressRange(v map[string]interface{})`

SetAddressRange sets AddressRange field to given value.

### HasAddressRange

`func (o *IpOverEthernetAddressInfo) HasAddressRange() bool`

HasAddressRange returns a boolean if a field has been set.

### GetMinAddress

`func (o *IpOverEthernetAddressInfo) GetMinAddress() string`

GetMinAddress returns the MinAddress field if non-nil, zero value otherwise.

### GetMinAddressOk

`func (o *IpOverEthernetAddressInfo) GetMinAddressOk() (*string, bool)`

GetMinAddressOk returns a tuple with the MinAddress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMinAddress

`func (o *IpOverEthernetAddressInfo) SetMinAddress(v string)`

SetMinAddress sets MinAddress field to given value.

### HasMinAddress

`func (o *IpOverEthernetAddressInfo) HasMinAddress() bool`

HasMinAddress returns a boolean if a field has been set.

### GetMaxAddress

`func (o *IpOverEthernetAddressInfo) GetMaxAddress() string`

GetMaxAddress returns the MaxAddress field if non-nil, zero value otherwise.

### GetMaxAddressOk

`func (o *IpOverEthernetAddressInfo) GetMaxAddressOk() (*string, bool)`

GetMaxAddressOk returns a tuple with the MaxAddress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMaxAddress

`func (o *IpOverEthernetAddressInfo) SetMaxAddress(v string)`

SetMaxAddress sets MaxAddress field to given value.

### HasMaxAddress

`func (o *IpOverEthernetAddressInfo) HasMaxAddress() bool`

HasMaxAddress returns a boolean if a field has been set.

### GetSubnetId

`func (o *IpOverEthernetAddressInfo) GetSubnetId() string`

GetSubnetId returns the SubnetId field if non-nil, zero value otherwise.

### GetSubnetIdOk

`func (o *IpOverEthernetAddressInfo) GetSubnetIdOk() (*string, bool)`

GetSubnetIdOk returns a tuple with the SubnetId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSubnetId

`func (o *IpOverEthernetAddressInfo) SetSubnetId(v string)`

SetSubnetId sets SubnetId field to given value.

### HasSubnetId

`func (o *IpOverEthernetAddressInfo) HasSubnetId() bool`

HasSubnetId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


