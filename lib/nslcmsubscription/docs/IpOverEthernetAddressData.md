# IpOverEthernetAddressData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MacAddress** | Pointer to **string** | A MAC address. Representation: string that consists of groups of two hexadecimal digits, separated by hyphens or colons.  | [optional] 
**SegmentationType** | Pointer to **string** | Specifies the encapsulation type for the traffics coming in and out of the trunk subport. Permitted values are: - VLAN: The subport uses VLAN as encapsulation type. - INHERIT: The subport gets its segmentation type from the network it is connected to. This attribute may be present for CP instances that represent subports in a trunk and shall be absent otherwise. If this attribute is not present for a subport CP instance, default value VLAN shall be used.  | [optional] 
**SegmentationId** | Pointer to **string** | Identification of the network segment to which the CP instance connects to. See note 3 and note 4.  | [optional] 
**IpAddresses** | Pointer to [**[]OneOfAnyTypeAnyTypeAnyType**](OneOfAnyTypeAnyTypeAnyType.md) | List of IP addresses to assign to the CP instance. Each entry represents IP address data for fixed or dynamic IP address assignment per subnet. If this attribute is not present, no IP address shall be assigned. See note 1.  | [optional] 

## Methods

### NewIpOverEthernetAddressData

`func NewIpOverEthernetAddressData() *IpOverEthernetAddressData`

NewIpOverEthernetAddressData instantiates a new IpOverEthernetAddressData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewIpOverEthernetAddressDataWithDefaults

`func NewIpOverEthernetAddressDataWithDefaults() *IpOverEthernetAddressData`

NewIpOverEthernetAddressDataWithDefaults instantiates a new IpOverEthernetAddressData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMacAddress

`func (o *IpOverEthernetAddressData) GetMacAddress() string`

GetMacAddress returns the MacAddress field if non-nil, zero value otherwise.

### GetMacAddressOk

`func (o *IpOverEthernetAddressData) GetMacAddressOk() (*string, bool)`

GetMacAddressOk returns a tuple with the MacAddress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMacAddress

`func (o *IpOverEthernetAddressData) SetMacAddress(v string)`

SetMacAddress sets MacAddress field to given value.

### HasMacAddress

`func (o *IpOverEthernetAddressData) HasMacAddress() bool`

HasMacAddress returns a boolean if a field has been set.

### GetSegmentationType

`func (o *IpOverEthernetAddressData) GetSegmentationType() string`

GetSegmentationType returns the SegmentationType field if non-nil, zero value otherwise.

### GetSegmentationTypeOk

`func (o *IpOverEthernetAddressData) GetSegmentationTypeOk() (*string, bool)`

GetSegmentationTypeOk returns a tuple with the SegmentationType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSegmentationType

`func (o *IpOverEthernetAddressData) SetSegmentationType(v string)`

SetSegmentationType sets SegmentationType field to given value.

### HasSegmentationType

`func (o *IpOverEthernetAddressData) HasSegmentationType() bool`

HasSegmentationType returns a boolean if a field has been set.

### GetSegmentationId

`func (o *IpOverEthernetAddressData) GetSegmentationId() string`

GetSegmentationId returns the SegmentationId field if non-nil, zero value otherwise.

### GetSegmentationIdOk

`func (o *IpOverEthernetAddressData) GetSegmentationIdOk() (*string, bool)`

GetSegmentationIdOk returns a tuple with the SegmentationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSegmentationId

`func (o *IpOverEthernetAddressData) SetSegmentationId(v string)`

SetSegmentationId sets SegmentationId field to given value.

### HasSegmentationId

`func (o *IpOverEthernetAddressData) HasSegmentationId() bool`

HasSegmentationId returns a boolean if a field has been set.

### GetIpAddresses

`func (o *IpOverEthernetAddressData) GetIpAddresses() []OneOfAnyTypeAnyTypeAnyType`

GetIpAddresses returns the IpAddresses field if non-nil, zero value otherwise.

### GetIpAddressesOk

`func (o *IpOverEthernetAddressData) GetIpAddressesOk() (*[]OneOfAnyTypeAnyTypeAnyType, bool)`

GetIpAddressesOk returns a tuple with the IpAddresses field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIpAddresses

`func (o *IpOverEthernetAddressData) SetIpAddresses(v []OneOfAnyTypeAnyTypeAnyType)`

SetIpAddresses sets IpAddresses field to given value.

### HasIpAddresses

`func (o *IpOverEthernetAddressData) HasIpAddresses() bool`

HasIpAddresses returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


