# VnfLocationConstraint

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VnfProfileId** | **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | 
**LocationConstraints** | Pointer to [**LocationConstraints**](LocationConstraints.md) |  | [optional] 

## Methods

### NewVnfLocationConstraint

`func NewVnfLocationConstraint(vnfProfileId string, ) *VnfLocationConstraint`

NewVnfLocationConstraint instantiates a new VnfLocationConstraint object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVnfLocationConstraintWithDefaults

`func NewVnfLocationConstraintWithDefaults() *VnfLocationConstraint`

NewVnfLocationConstraintWithDefaults instantiates a new VnfLocationConstraint object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVnfProfileId

`func (o *VnfLocationConstraint) GetVnfProfileId() string`

GetVnfProfileId returns the VnfProfileId field if non-nil, zero value otherwise.

### GetVnfProfileIdOk

`func (o *VnfLocationConstraint) GetVnfProfileIdOk() (*string, bool)`

GetVnfProfileIdOk returns a tuple with the VnfProfileId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfProfileId

`func (o *VnfLocationConstraint) SetVnfProfileId(v string)`

SetVnfProfileId sets VnfProfileId field to given value.


### GetLocationConstraints

`func (o *VnfLocationConstraint) GetLocationConstraints() LocationConstraints`

GetLocationConstraints returns the LocationConstraints field if non-nil, zero value otherwise.

### GetLocationConstraintsOk

`func (o *VnfLocationConstraint) GetLocationConstraintsOk() (*LocationConstraints, bool)`

GetLocationConstraintsOk returns a tuple with the LocationConstraints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocationConstraints

`func (o *VnfLocationConstraint) SetLocationConstraints(v LocationConstraints)`

SetLocationConstraints sets LocationConstraints field to given value.

### HasLocationConstraints

`func (o *VnfLocationConstraint) HasLocationConstraints() bool`

HasLocationConstraints returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


