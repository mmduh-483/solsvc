# MscsInfoSiteAccessProtectionSchemesInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LocationConstraints** | Pointer to [**LocationConstraints**](LocationConstraints.md) |  | [optional] 
**ProtectionScheme** | Pointer to **string** | Defines the protection scheme. Permitted values: - UNPROTECTED: to indicate no protection. - ONE_TO_ONE: to indicate an active-passive access protection. - ONE_PLUS_ONE: to indicate an active-active access protection. - ONE_TO_N: to indicate an N active to 1 passive access protection.  | [optional] 

## Methods

### NewMscsInfoSiteAccessProtectionSchemesInner

`func NewMscsInfoSiteAccessProtectionSchemesInner() *MscsInfoSiteAccessProtectionSchemesInner`

NewMscsInfoSiteAccessProtectionSchemesInner instantiates a new MscsInfoSiteAccessProtectionSchemesInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMscsInfoSiteAccessProtectionSchemesInnerWithDefaults

`func NewMscsInfoSiteAccessProtectionSchemesInnerWithDefaults() *MscsInfoSiteAccessProtectionSchemesInner`

NewMscsInfoSiteAccessProtectionSchemesInnerWithDefaults instantiates a new MscsInfoSiteAccessProtectionSchemesInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLocationConstraints

`func (o *MscsInfoSiteAccessProtectionSchemesInner) GetLocationConstraints() LocationConstraints`

GetLocationConstraints returns the LocationConstraints field if non-nil, zero value otherwise.

### GetLocationConstraintsOk

`func (o *MscsInfoSiteAccessProtectionSchemesInner) GetLocationConstraintsOk() (*LocationConstraints, bool)`

GetLocationConstraintsOk returns a tuple with the LocationConstraints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocationConstraints

`func (o *MscsInfoSiteAccessProtectionSchemesInner) SetLocationConstraints(v LocationConstraints)`

SetLocationConstraints sets LocationConstraints field to given value.

### HasLocationConstraints

`func (o *MscsInfoSiteAccessProtectionSchemesInner) HasLocationConstraints() bool`

HasLocationConstraints returns a boolean if a field has been set.

### GetProtectionScheme

`func (o *MscsInfoSiteAccessProtectionSchemesInner) GetProtectionScheme() string`

GetProtectionScheme returns the ProtectionScheme field if non-nil, zero value otherwise.

### GetProtectionSchemeOk

`func (o *MscsInfoSiteAccessProtectionSchemesInner) GetProtectionSchemeOk() (*string, bool)`

GetProtectionSchemeOk returns a tuple with the ProtectionScheme field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProtectionScheme

`func (o *MscsInfoSiteAccessProtectionSchemesInner) SetProtectionScheme(v string)`

SetProtectionScheme sets ProtectionScheme field to given value.

### HasProtectionScheme

`func (o *MscsInfoSiteAccessProtectionSchemesInner) HasProtectionScheme() bool`

HasProtectionScheme returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


