# VnfOperationalStateType

## Enum


* `STARTED` (value: `"STARTED"`)

* `STOPPED` (value: `"STOPPED"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


