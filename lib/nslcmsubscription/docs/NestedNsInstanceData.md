# NestedNsInstanceData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NestedNsInstanceId** | **string** | An identifier with the intention of being globally unique.  | 
**NsProfileId** | Pointer to **string** | An identifier that is unique with respect to a NS. Representation: string of variable length.  | [optional] 

## Methods

### NewNestedNsInstanceData

`func NewNestedNsInstanceData(nestedNsInstanceId string, ) *NestedNsInstanceData`

NewNestedNsInstanceData instantiates a new NestedNsInstanceData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNestedNsInstanceDataWithDefaults

`func NewNestedNsInstanceDataWithDefaults() *NestedNsInstanceData`

NewNestedNsInstanceDataWithDefaults instantiates a new NestedNsInstanceData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetNestedNsInstanceId

`func (o *NestedNsInstanceData) GetNestedNsInstanceId() string`

GetNestedNsInstanceId returns the NestedNsInstanceId field if non-nil, zero value otherwise.

### GetNestedNsInstanceIdOk

`func (o *NestedNsInstanceData) GetNestedNsInstanceIdOk() (*string, bool)`

GetNestedNsInstanceIdOk returns a tuple with the NestedNsInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNestedNsInstanceId

`func (o *NestedNsInstanceData) SetNestedNsInstanceId(v string)`

SetNestedNsInstanceId sets NestedNsInstanceId field to given value.


### GetNsProfileId

`func (o *NestedNsInstanceData) GetNsProfileId() string`

GetNsProfileId returns the NsProfileId field if non-nil, zero value otherwise.

### GetNsProfileIdOk

`func (o *NestedNsInstanceData) GetNsProfileIdOk() (*string, bool)`

GetNsProfileIdOk returns a tuple with the NsProfileId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsProfileId

`func (o *NestedNsInstanceData) SetNsProfileId(v string)`

SetNsProfileId sets NsProfileId field to given value.

### HasNsProfileId

`func (o *NestedNsInstanceData) HasNsProfileId() bool`

HasNsProfileId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


