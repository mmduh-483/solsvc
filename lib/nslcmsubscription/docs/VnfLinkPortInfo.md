# VnfLinkPortInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | An identifier that is unique for the respective type within a VNF instance, but may not be globally unique.  | 
**ResourceHandle** | [**ResourceHandle**](ResourceHandle.md) |  | 
**CpInstanceId** | Pointer to **string** | An identifier that is unique for the respective type within a VNF instance, but may not be globally unique.  | [optional] 
**CpInstanceType** | Pointer to **string** | Type of the CP instance that is identified by cpInstanceId. Shall be present if \&quot;cpInstanceId\&quot; is present, and shall be absent otherwise. Permitted values: * VNFC_CP: The link port is connected to a VNFC CP * EXT_CP: The link port is associated to an external CP. See note 1.  | [optional] 
**VipCpInstanceId** | Pointer to **string** | An identifier that is unique for the respective type within a VNF instance, but may not be globally unique.  | [optional] 
**TrunkResourceId** | Pointer to **string** | An identifier maintained by the VIM or other resource provider. It is expected to be unique within the VIM instance. Representation: string of variable length.  | [optional] 

## Methods

### NewVnfLinkPortInfo

`func NewVnfLinkPortInfo(id string, resourceHandle ResourceHandle, ) *VnfLinkPortInfo`

NewVnfLinkPortInfo instantiates a new VnfLinkPortInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVnfLinkPortInfoWithDefaults

`func NewVnfLinkPortInfoWithDefaults() *VnfLinkPortInfo`

NewVnfLinkPortInfoWithDefaults instantiates a new VnfLinkPortInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *VnfLinkPortInfo) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *VnfLinkPortInfo) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *VnfLinkPortInfo) SetId(v string)`

SetId sets Id field to given value.


### GetResourceHandle

`func (o *VnfLinkPortInfo) GetResourceHandle() ResourceHandle`

GetResourceHandle returns the ResourceHandle field if non-nil, zero value otherwise.

### GetResourceHandleOk

`func (o *VnfLinkPortInfo) GetResourceHandleOk() (*ResourceHandle, bool)`

GetResourceHandleOk returns a tuple with the ResourceHandle field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResourceHandle

`func (o *VnfLinkPortInfo) SetResourceHandle(v ResourceHandle)`

SetResourceHandle sets ResourceHandle field to given value.


### GetCpInstanceId

`func (o *VnfLinkPortInfo) GetCpInstanceId() string`

GetCpInstanceId returns the CpInstanceId field if non-nil, zero value otherwise.

### GetCpInstanceIdOk

`func (o *VnfLinkPortInfo) GetCpInstanceIdOk() (*string, bool)`

GetCpInstanceIdOk returns a tuple with the CpInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCpInstanceId

`func (o *VnfLinkPortInfo) SetCpInstanceId(v string)`

SetCpInstanceId sets CpInstanceId field to given value.

### HasCpInstanceId

`func (o *VnfLinkPortInfo) HasCpInstanceId() bool`

HasCpInstanceId returns a boolean if a field has been set.

### GetCpInstanceType

`func (o *VnfLinkPortInfo) GetCpInstanceType() string`

GetCpInstanceType returns the CpInstanceType field if non-nil, zero value otherwise.

### GetCpInstanceTypeOk

`func (o *VnfLinkPortInfo) GetCpInstanceTypeOk() (*string, bool)`

GetCpInstanceTypeOk returns a tuple with the CpInstanceType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCpInstanceType

`func (o *VnfLinkPortInfo) SetCpInstanceType(v string)`

SetCpInstanceType sets CpInstanceType field to given value.

### HasCpInstanceType

`func (o *VnfLinkPortInfo) HasCpInstanceType() bool`

HasCpInstanceType returns a boolean if a field has been set.

### GetVipCpInstanceId

`func (o *VnfLinkPortInfo) GetVipCpInstanceId() string`

GetVipCpInstanceId returns the VipCpInstanceId field if non-nil, zero value otherwise.

### GetVipCpInstanceIdOk

`func (o *VnfLinkPortInfo) GetVipCpInstanceIdOk() (*string, bool)`

GetVipCpInstanceIdOk returns a tuple with the VipCpInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVipCpInstanceId

`func (o *VnfLinkPortInfo) SetVipCpInstanceId(v string)`

SetVipCpInstanceId sets VipCpInstanceId field to given value.

### HasVipCpInstanceId

`func (o *VnfLinkPortInfo) HasVipCpInstanceId() bool`

HasVipCpInstanceId returns a boolean if a field has been set.

### GetTrunkResourceId

`func (o *VnfLinkPortInfo) GetTrunkResourceId() string`

GetTrunkResourceId returns the TrunkResourceId field if non-nil, zero value otherwise.

### GetTrunkResourceIdOk

`func (o *VnfLinkPortInfo) GetTrunkResourceIdOk() (*string, bool)`

GetTrunkResourceIdOk returns a tuple with the TrunkResourceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTrunkResourceId

`func (o *VnfLinkPortInfo) SetTrunkResourceId(v string)`

SetTrunkResourceId sets TrunkResourceId field to given value.

### HasTrunkResourceId

`func (o *VnfLinkPortInfo) HasTrunkResourceId() bool`

HasTrunkResourceId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


