# ModifyWanConnectionInfoData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**WanConnectionInfoId** | **string** | An identifier with the intention of being globally unique.  | 
**MscsName** | Pointer to **string** | New value of the \&quot;mscsName\&quot; attribute in \&quot;MscsInfo\&quot;, or \&quot;null\&quot; to remove the attribute.  | [optional] 
**MscsDescription** | Pointer to **string** | New value of the \&quot;mscsDescription\&quot; attribute in \&quot;MscsInfo\&quot;, or \&quot;null\&quot; to remove the attribute.  | [optional] 
**MscsEndpoints** | Pointer to [**[]MscsEndpointInfo**](MscsEndpointInfo.md) | New content of certain entries of MSCS endpoints in the \&quot;mscsEndpoints\&quot; attribute in \&quot;MscsInfo\&quot;, as defined below this table.  | [optional] 
**RemoveMscsEndpointIds** | Pointer to **[]string** | List of identifier entries to be deleted from the \&quot;mscsEndpoints\&quot; attribute array in \&quot;MscsInfo\&quot;, to be used as \&quot;deleteIdList\&quot; as defined below this table.  | [optional] 
**ConnectivityServiceEndpoints** | Pointer to [**[]ConnectivityServiceEndpointInfo**](ConnectivityServiceEndpointInfo.md) | New content of certain entries of connectivity service endpoints in the \&quot;connectivityServiceEndpointInfos\&quot; attribute in \&quot;WanConnectionProtocolInfo\&quot;, as defined below this table.  | [optional] 
**RemoveConnectivityServiceEndpoints** | Pointer to **[]string** | List of identifier entries to be deleted from the \&quot;connectivityServiceEndpointInfos\&quot; attribute array in \&quot;WanConnectionProtocolInfo\&quot;, to be used as \&quot;deleteIdList\&quot; as defined below this table.  | [optional] 

## Methods

### NewModifyWanConnectionInfoData

`func NewModifyWanConnectionInfoData(wanConnectionInfoId string, ) *ModifyWanConnectionInfoData`

NewModifyWanConnectionInfoData instantiates a new ModifyWanConnectionInfoData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewModifyWanConnectionInfoDataWithDefaults

`func NewModifyWanConnectionInfoDataWithDefaults() *ModifyWanConnectionInfoData`

NewModifyWanConnectionInfoDataWithDefaults instantiates a new ModifyWanConnectionInfoData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetWanConnectionInfoId

`func (o *ModifyWanConnectionInfoData) GetWanConnectionInfoId() string`

GetWanConnectionInfoId returns the WanConnectionInfoId field if non-nil, zero value otherwise.

### GetWanConnectionInfoIdOk

`func (o *ModifyWanConnectionInfoData) GetWanConnectionInfoIdOk() (*string, bool)`

GetWanConnectionInfoIdOk returns a tuple with the WanConnectionInfoId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWanConnectionInfoId

`func (o *ModifyWanConnectionInfoData) SetWanConnectionInfoId(v string)`

SetWanConnectionInfoId sets WanConnectionInfoId field to given value.


### GetMscsName

`func (o *ModifyWanConnectionInfoData) GetMscsName() string`

GetMscsName returns the MscsName field if non-nil, zero value otherwise.

### GetMscsNameOk

`func (o *ModifyWanConnectionInfoData) GetMscsNameOk() (*string, bool)`

GetMscsNameOk returns a tuple with the MscsName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMscsName

`func (o *ModifyWanConnectionInfoData) SetMscsName(v string)`

SetMscsName sets MscsName field to given value.

### HasMscsName

`func (o *ModifyWanConnectionInfoData) HasMscsName() bool`

HasMscsName returns a boolean if a field has been set.

### GetMscsDescription

`func (o *ModifyWanConnectionInfoData) GetMscsDescription() string`

GetMscsDescription returns the MscsDescription field if non-nil, zero value otherwise.

### GetMscsDescriptionOk

`func (o *ModifyWanConnectionInfoData) GetMscsDescriptionOk() (*string, bool)`

GetMscsDescriptionOk returns a tuple with the MscsDescription field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMscsDescription

`func (o *ModifyWanConnectionInfoData) SetMscsDescription(v string)`

SetMscsDescription sets MscsDescription field to given value.

### HasMscsDescription

`func (o *ModifyWanConnectionInfoData) HasMscsDescription() bool`

HasMscsDescription returns a boolean if a field has been set.

### GetMscsEndpoints

`func (o *ModifyWanConnectionInfoData) GetMscsEndpoints() []MscsEndpointInfo`

GetMscsEndpoints returns the MscsEndpoints field if non-nil, zero value otherwise.

### GetMscsEndpointsOk

`func (o *ModifyWanConnectionInfoData) GetMscsEndpointsOk() (*[]MscsEndpointInfo, bool)`

GetMscsEndpointsOk returns a tuple with the MscsEndpoints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMscsEndpoints

`func (o *ModifyWanConnectionInfoData) SetMscsEndpoints(v []MscsEndpointInfo)`

SetMscsEndpoints sets MscsEndpoints field to given value.

### HasMscsEndpoints

`func (o *ModifyWanConnectionInfoData) HasMscsEndpoints() bool`

HasMscsEndpoints returns a boolean if a field has been set.

### GetRemoveMscsEndpointIds

`func (o *ModifyWanConnectionInfoData) GetRemoveMscsEndpointIds() []string`

GetRemoveMscsEndpointIds returns the RemoveMscsEndpointIds field if non-nil, zero value otherwise.

### GetRemoveMscsEndpointIdsOk

`func (o *ModifyWanConnectionInfoData) GetRemoveMscsEndpointIdsOk() (*[]string, bool)`

GetRemoveMscsEndpointIdsOk returns a tuple with the RemoveMscsEndpointIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRemoveMscsEndpointIds

`func (o *ModifyWanConnectionInfoData) SetRemoveMscsEndpointIds(v []string)`

SetRemoveMscsEndpointIds sets RemoveMscsEndpointIds field to given value.

### HasRemoveMscsEndpointIds

`func (o *ModifyWanConnectionInfoData) HasRemoveMscsEndpointIds() bool`

HasRemoveMscsEndpointIds returns a boolean if a field has been set.

### GetConnectivityServiceEndpoints

`func (o *ModifyWanConnectionInfoData) GetConnectivityServiceEndpoints() []ConnectivityServiceEndpointInfo`

GetConnectivityServiceEndpoints returns the ConnectivityServiceEndpoints field if non-nil, zero value otherwise.

### GetConnectivityServiceEndpointsOk

`func (o *ModifyWanConnectionInfoData) GetConnectivityServiceEndpointsOk() (*[]ConnectivityServiceEndpointInfo, bool)`

GetConnectivityServiceEndpointsOk returns a tuple with the ConnectivityServiceEndpoints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConnectivityServiceEndpoints

`func (o *ModifyWanConnectionInfoData) SetConnectivityServiceEndpoints(v []ConnectivityServiceEndpointInfo)`

SetConnectivityServiceEndpoints sets ConnectivityServiceEndpoints field to given value.

### HasConnectivityServiceEndpoints

`func (o *ModifyWanConnectionInfoData) HasConnectivityServiceEndpoints() bool`

HasConnectivityServiceEndpoints returns a boolean if a field has been set.

### GetRemoveConnectivityServiceEndpoints

`func (o *ModifyWanConnectionInfoData) GetRemoveConnectivityServiceEndpoints() []string`

GetRemoveConnectivityServiceEndpoints returns the RemoveConnectivityServiceEndpoints field if non-nil, zero value otherwise.

### GetRemoveConnectivityServiceEndpointsOk

`func (o *ModifyWanConnectionInfoData) GetRemoveConnectivityServiceEndpointsOk() (*[]string, bool)`

GetRemoveConnectivityServiceEndpointsOk returns a tuple with the RemoveConnectivityServiceEndpoints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRemoveConnectivityServiceEndpoints

`func (o *ModifyWanConnectionInfoData) SetRemoveConnectivityServiceEndpoints(v []string)`

SetRemoveConnectivityServiceEndpoints sets RemoveConnectivityServiceEndpoints field to given value.

### HasRemoveConnectivityServiceEndpoints

`func (o *ModifyWanConnectionInfoData) HasRemoveConnectivityServiceEndpoints() bool`

HasRemoveConnectivityServiceEndpoints returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


