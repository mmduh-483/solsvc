# NsLcmOpOccLcmCoordinationsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | An identifier with the intention of being globally unique.  | 
**CoordinationActionName** | **string** | An identifier with the intention of being globally unique.  | 
**CoordinationResult** | Pointer to [**LcmCoordResultType**](LcmCoordResultType.md) |  | [optional] 
**StartTime** | **time.Time** | Date-time stamp. Representation: String formatted according toas defined by the date-time production in IETF RFC 3339.  | 
**EndTime** | Pointer to **time.Time** | Date-time stamp. Representation: String formatted according toas defined by the date-time production in IETF RFC 3339.  | [optional] 
**EndpointType** | **string** | The endpoint type used by this coordination action. Valid values: - MGMT: coordination with other operation supporting management systems (e.g. OSS/BSS)  | 
**Delay** | Pointer to **time.Time** | Date-time stamp. Representation: String formatted according toas defined by the date-time production in IETF RFC 3339.  | [optional] 

## Methods

### NewNsLcmOpOccLcmCoordinationsInner

`func NewNsLcmOpOccLcmCoordinationsInner(id string, coordinationActionName string, startTime time.Time, endpointType string, ) *NsLcmOpOccLcmCoordinationsInner`

NewNsLcmOpOccLcmCoordinationsInner instantiates a new NsLcmOpOccLcmCoordinationsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNsLcmOpOccLcmCoordinationsInnerWithDefaults

`func NewNsLcmOpOccLcmCoordinationsInnerWithDefaults() *NsLcmOpOccLcmCoordinationsInner`

NewNsLcmOpOccLcmCoordinationsInnerWithDefaults instantiates a new NsLcmOpOccLcmCoordinationsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *NsLcmOpOccLcmCoordinationsInner) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *NsLcmOpOccLcmCoordinationsInner) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *NsLcmOpOccLcmCoordinationsInner) SetId(v string)`

SetId sets Id field to given value.


### GetCoordinationActionName

`func (o *NsLcmOpOccLcmCoordinationsInner) GetCoordinationActionName() string`

GetCoordinationActionName returns the CoordinationActionName field if non-nil, zero value otherwise.

### GetCoordinationActionNameOk

`func (o *NsLcmOpOccLcmCoordinationsInner) GetCoordinationActionNameOk() (*string, bool)`

GetCoordinationActionNameOk returns a tuple with the CoordinationActionName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCoordinationActionName

`func (o *NsLcmOpOccLcmCoordinationsInner) SetCoordinationActionName(v string)`

SetCoordinationActionName sets CoordinationActionName field to given value.


### GetCoordinationResult

`func (o *NsLcmOpOccLcmCoordinationsInner) GetCoordinationResult() LcmCoordResultType`

GetCoordinationResult returns the CoordinationResult field if non-nil, zero value otherwise.

### GetCoordinationResultOk

`func (o *NsLcmOpOccLcmCoordinationsInner) GetCoordinationResultOk() (*LcmCoordResultType, bool)`

GetCoordinationResultOk returns a tuple with the CoordinationResult field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCoordinationResult

`func (o *NsLcmOpOccLcmCoordinationsInner) SetCoordinationResult(v LcmCoordResultType)`

SetCoordinationResult sets CoordinationResult field to given value.

### HasCoordinationResult

`func (o *NsLcmOpOccLcmCoordinationsInner) HasCoordinationResult() bool`

HasCoordinationResult returns a boolean if a field has been set.

### GetStartTime

`func (o *NsLcmOpOccLcmCoordinationsInner) GetStartTime() time.Time`

GetStartTime returns the StartTime field if non-nil, zero value otherwise.

### GetStartTimeOk

`func (o *NsLcmOpOccLcmCoordinationsInner) GetStartTimeOk() (*time.Time, bool)`

GetStartTimeOk returns a tuple with the StartTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStartTime

`func (o *NsLcmOpOccLcmCoordinationsInner) SetStartTime(v time.Time)`

SetStartTime sets StartTime field to given value.


### GetEndTime

`func (o *NsLcmOpOccLcmCoordinationsInner) GetEndTime() time.Time`

GetEndTime returns the EndTime field if non-nil, zero value otherwise.

### GetEndTimeOk

`func (o *NsLcmOpOccLcmCoordinationsInner) GetEndTimeOk() (*time.Time, bool)`

GetEndTimeOk returns a tuple with the EndTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEndTime

`func (o *NsLcmOpOccLcmCoordinationsInner) SetEndTime(v time.Time)`

SetEndTime sets EndTime field to given value.

### HasEndTime

`func (o *NsLcmOpOccLcmCoordinationsInner) HasEndTime() bool`

HasEndTime returns a boolean if a field has been set.

### GetEndpointType

`func (o *NsLcmOpOccLcmCoordinationsInner) GetEndpointType() string`

GetEndpointType returns the EndpointType field if non-nil, zero value otherwise.

### GetEndpointTypeOk

`func (o *NsLcmOpOccLcmCoordinationsInner) GetEndpointTypeOk() (*string, bool)`

GetEndpointTypeOk returns a tuple with the EndpointType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEndpointType

`func (o *NsLcmOpOccLcmCoordinationsInner) SetEndpointType(v string)`

SetEndpointType sets EndpointType field to given value.


### GetDelay

`func (o *NsLcmOpOccLcmCoordinationsInner) GetDelay() time.Time`

GetDelay returns the Delay field if non-nil, zero value otherwise.

### GetDelayOk

`func (o *NsLcmOpOccLcmCoordinationsInner) GetDelayOk() (*time.Time, bool)`

GetDelayOk returns a tuple with the Delay field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDelay

`func (o *NsLcmOpOccLcmCoordinationsInner) SetDelay(v time.Time)`

SetDelay sets Delay field to given value.

### HasDelay

`func (o *NsLcmOpOccLcmCoordinationsInner) HasDelay() bool`

HasDelay returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


