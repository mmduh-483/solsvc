# SiteToWanLayer3ProtocolDataRoutingInfoBgpRouting

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BgpAs** | **map[string]interface{}** | The Autonomous System (AS) identification applicable to the BGP routing info entry.  | 
**BgpNeighbour** | Pointer to **string** | An IPV4 or IPV6 address. Representation: In case of an IPV4 address, string that consists of four decimal integers separated by dots, each integer ranging from 0 to 255. In case of an IPV6 address, string that consists of groups of zero to four hexadecimal digits, separated by colons.  | [optional] 
**BgpAdditionalParam** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 

## Methods

### NewSiteToWanLayer3ProtocolDataRoutingInfoBgpRouting

`func NewSiteToWanLayer3ProtocolDataRoutingInfoBgpRouting(bgpAs map[string]interface{}, ) *SiteToWanLayer3ProtocolDataRoutingInfoBgpRouting`

NewSiteToWanLayer3ProtocolDataRoutingInfoBgpRouting instantiates a new SiteToWanLayer3ProtocolDataRoutingInfoBgpRouting object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSiteToWanLayer3ProtocolDataRoutingInfoBgpRoutingWithDefaults

`func NewSiteToWanLayer3ProtocolDataRoutingInfoBgpRoutingWithDefaults() *SiteToWanLayer3ProtocolDataRoutingInfoBgpRouting`

NewSiteToWanLayer3ProtocolDataRoutingInfoBgpRoutingWithDefaults instantiates a new SiteToWanLayer3ProtocolDataRoutingInfoBgpRouting object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetBgpAs

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoBgpRouting) GetBgpAs() map[string]interface{}`

GetBgpAs returns the BgpAs field if non-nil, zero value otherwise.

### GetBgpAsOk

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoBgpRouting) GetBgpAsOk() (*map[string]interface{}, bool)`

GetBgpAsOk returns a tuple with the BgpAs field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBgpAs

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoBgpRouting) SetBgpAs(v map[string]interface{})`

SetBgpAs sets BgpAs field to given value.


### GetBgpNeighbour

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoBgpRouting) GetBgpNeighbour() string`

GetBgpNeighbour returns the BgpNeighbour field if non-nil, zero value otherwise.

### GetBgpNeighbourOk

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoBgpRouting) GetBgpNeighbourOk() (*string, bool)`

GetBgpNeighbourOk returns a tuple with the BgpNeighbour field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBgpNeighbour

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoBgpRouting) SetBgpNeighbour(v string)`

SetBgpNeighbour sets BgpNeighbour field to given value.

### HasBgpNeighbour

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoBgpRouting) HasBgpNeighbour() bool`

HasBgpNeighbour returns a boolean if a field has been set.

### GetBgpAdditionalParam

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoBgpRouting) GetBgpAdditionalParam() map[string]interface{}`

GetBgpAdditionalParam returns the BgpAdditionalParam field if non-nil, zero value otherwise.

### GetBgpAdditionalParamOk

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoBgpRouting) GetBgpAdditionalParamOk() (*map[string]interface{}, bool)`

GetBgpAdditionalParamOk returns a tuple with the BgpAdditionalParam field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBgpAdditionalParam

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoBgpRouting) SetBgpAdditionalParam(v map[string]interface{})`

SetBgpAdditionalParam sets BgpAdditionalParam field to given value.

### HasBgpAdditionalParam

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoBgpRouting) HasBgpAdditionalParam() bool`

HasBgpAdditionalParam returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


