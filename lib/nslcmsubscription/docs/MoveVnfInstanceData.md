# MoveVnfInstanceData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TargetNsInstanceId** | **string** | An identifier with the intention of being globally unique.  | 
**VnfInstanceId** | Pointer to **[]string** | Specify the VNF instance that is moved.  | [optional] 

## Methods

### NewMoveVnfInstanceData

`func NewMoveVnfInstanceData(targetNsInstanceId string, ) *MoveVnfInstanceData`

NewMoveVnfInstanceData instantiates a new MoveVnfInstanceData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMoveVnfInstanceDataWithDefaults

`func NewMoveVnfInstanceDataWithDefaults() *MoveVnfInstanceData`

NewMoveVnfInstanceDataWithDefaults instantiates a new MoveVnfInstanceData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTargetNsInstanceId

`func (o *MoveVnfInstanceData) GetTargetNsInstanceId() string`

GetTargetNsInstanceId returns the TargetNsInstanceId field if non-nil, zero value otherwise.

### GetTargetNsInstanceIdOk

`func (o *MoveVnfInstanceData) GetTargetNsInstanceIdOk() (*string, bool)`

GetTargetNsInstanceIdOk returns a tuple with the TargetNsInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTargetNsInstanceId

`func (o *MoveVnfInstanceData) SetTargetNsInstanceId(v string)`

SetTargetNsInstanceId sets TargetNsInstanceId field to given value.


### GetVnfInstanceId

`func (o *MoveVnfInstanceData) GetVnfInstanceId() []string`

GetVnfInstanceId returns the VnfInstanceId field if non-nil, zero value otherwise.

### GetVnfInstanceIdOk

`func (o *MoveVnfInstanceData) GetVnfInstanceIdOk() (*[]string, bool)`

GetVnfInstanceIdOk returns a tuple with the VnfInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceId

`func (o *MoveVnfInstanceData) SetVnfInstanceId(v []string)`

SetVnfInstanceId sets VnfInstanceId field to given value.

### HasVnfInstanceId

`func (o *MoveVnfInstanceData) HasVnfInstanceId() bool`

HasVnfInstanceId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


