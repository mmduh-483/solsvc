# ExtManagedVirtualLinkInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | An identifier with the intention of being globally unique.  | 
**VnfdId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**VnfVirtualLinkDescId** | **string** | Identifier of the VNF Virtual Link Descriptor (VLD) in the VNFD.  | 
**NetworkResource** | Pointer to [**ResourceHandle**](ResourceHandle.md) |  | [optional] 
**VnfLinkPorts** | Pointer to [**[]VnfLinkPortInfo**](VnfLinkPortInfo.md) | Link ports of this VL.  | [optional] 
**ExtManagedMultisiteVirtualLinkId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 

## Methods

### NewExtManagedVirtualLinkInfo

`func NewExtManagedVirtualLinkInfo(id string, vnfVirtualLinkDescId string, ) *ExtManagedVirtualLinkInfo`

NewExtManagedVirtualLinkInfo instantiates a new ExtManagedVirtualLinkInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewExtManagedVirtualLinkInfoWithDefaults

`func NewExtManagedVirtualLinkInfoWithDefaults() *ExtManagedVirtualLinkInfo`

NewExtManagedVirtualLinkInfoWithDefaults instantiates a new ExtManagedVirtualLinkInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *ExtManagedVirtualLinkInfo) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *ExtManagedVirtualLinkInfo) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *ExtManagedVirtualLinkInfo) SetId(v string)`

SetId sets Id field to given value.


### GetVnfdId

`func (o *ExtManagedVirtualLinkInfo) GetVnfdId() string`

GetVnfdId returns the VnfdId field if non-nil, zero value otherwise.

### GetVnfdIdOk

`func (o *ExtManagedVirtualLinkInfo) GetVnfdIdOk() (*string, bool)`

GetVnfdIdOk returns a tuple with the VnfdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfdId

`func (o *ExtManagedVirtualLinkInfo) SetVnfdId(v string)`

SetVnfdId sets VnfdId field to given value.

### HasVnfdId

`func (o *ExtManagedVirtualLinkInfo) HasVnfdId() bool`

HasVnfdId returns a boolean if a field has been set.

### GetVnfVirtualLinkDescId

`func (o *ExtManagedVirtualLinkInfo) GetVnfVirtualLinkDescId() string`

GetVnfVirtualLinkDescId returns the VnfVirtualLinkDescId field if non-nil, zero value otherwise.

### GetVnfVirtualLinkDescIdOk

`func (o *ExtManagedVirtualLinkInfo) GetVnfVirtualLinkDescIdOk() (*string, bool)`

GetVnfVirtualLinkDescIdOk returns a tuple with the VnfVirtualLinkDescId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfVirtualLinkDescId

`func (o *ExtManagedVirtualLinkInfo) SetVnfVirtualLinkDescId(v string)`

SetVnfVirtualLinkDescId sets VnfVirtualLinkDescId field to given value.


### GetNetworkResource

`func (o *ExtManagedVirtualLinkInfo) GetNetworkResource() ResourceHandle`

GetNetworkResource returns the NetworkResource field if non-nil, zero value otherwise.

### GetNetworkResourceOk

`func (o *ExtManagedVirtualLinkInfo) GetNetworkResourceOk() (*ResourceHandle, bool)`

GetNetworkResourceOk returns a tuple with the NetworkResource field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNetworkResource

`func (o *ExtManagedVirtualLinkInfo) SetNetworkResource(v ResourceHandle)`

SetNetworkResource sets NetworkResource field to given value.

### HasNetworkResource

`func (o *ExtManagedVirtualLinkInfo) HasNetworkResource() bool`

HasNetworkResource returns a boolean if a field has been set.

### GetVnfLinkPorts

`func (o *ExtManagedVirtualLinkInfo) GetVnfLinkPorts() []VnfLinkPortInfo`

GetVnfLinkPorts returns the VnfLinkPorts field if non-nil, zero value otherwise.

### GetVnfLinkPortsOk

`func (o *ExtManagedVirtualLinkInfo) GetVnfLinkPortsOk() (*[]VnfLinkPortInfo, bool)`

GetVnfLinkPortsOk returns a tuple with the VnfLinkPorts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfLinkPorts

`func (o *ExtManagedVirtualLinkInfo) SetVnfLinkPorts(v []VnfLinkPortInfo)`

SetVnfLinkPorts sets VnfLinkPorts field to given value.

### HasVnfLinkPorts

`func (o *ExtManagedVirtualLinkInfo) HasVnfLinkPorts() bool`

HasVnfLinkPorts returns a boolean if a field has been set.

### GetExtManagedMultisiteVirtualLinkId

`func (o *ExtManagedVirtualLinkInfo) GetExtManagedMultisiteVirtualLinkId() string`

GetExtManagedMultisiteVirtualLinkId returns the ExtManagedMultisiteVirtualLinkId field if non-nil, zero value otherwise.

### GetExtManagedMultisiteVirtualLinkIdOk

`func (o *ExtManagedVirtualLinkInfo) GetExtManagedMultisiteVirtualLinkIdOk() (*string, bool)`

GetExtManagedMultisiteVirtualLinkIdOk returns a tuple with the ExtManagedMultisiteVirtualLinkId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtManagedMultisiteVirtualLinkId

`func (o *ExtManagedVirtualLinkInfo) SetExtManagedMultisiteVirtualLinkId(v string)`

SetExtManagedMultisiteVirtualLinkId sets ExtManagedMultisiteVirtualLinkId field to given value.

### HasExtManagedMultisiteVirtualLinkId

`func (o *ExtManagedVirtualLinkInfo) HasExtManagedMultisiteVirtualLinkId() bool`

HasExtManagedMultisiteVirtualLinkId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


