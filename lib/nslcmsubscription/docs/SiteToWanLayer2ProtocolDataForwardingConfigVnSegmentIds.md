# SiteToWanLayer2ProtocolDataForwardingConfigVnSegmentIds

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VnSegmentIdValue** | **string** | Identifier of the network segment.  | 
**VnSegmentIdUpperRange** | Pointer to **string** | Identifier of the upper range network segment, in case the \&quot;vnSegmentIds\&quot; is used to define a range.  | [optional] 

## Methods

### NewSiteToWanLayer2ProtocolDataForwardingConfigVnSegmentIds

`func NewSiteToWanLayer2ProtocolDataForwardingConfigVnSegmentIds(vnSegmentIdValue string, ) *SiteToWanLayer2ProtocolDataForwardingConfigVnSegmentIds`

NewSiteToWanLayer2ProtocolDataForwardingConfigVnSegmentIds instantiates a new SiteToWanLayer2ProtocolDataForwardingConfigVnSegmentIds object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSiteToWanLayer2ProtocolDataForwardingConfigVnSegmentIdsWithDefaults

`func NewSiteToWanLayer2ProtocolDataForwardingConfigVnSegmentIdsWithDefaults() *SiteToWanLayer2ProtocolDataForwardingConfigVnSegmentIds`

NewSiteToWanLayer2ProtocolDataForwardingConfigVnSegmentIdsWithDefaults instantiates a new SiteToWanLayer2ProtocolDataForwardingConfigVnSegmentIds object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVnSegmentIdValue

`func (o *SiteToWanLayer2ProtocolDataForwardingConfigVnSegmentIds) GetVnSegmentIdValue() string`

GetVnSegmentIdValue returns the VnSegmentIdValue field if non-nil, zero value otherwise.

### GetVnSegmentIdValueOk

`func (o *SiteToWanLayer2ProtocolDataForwardingConfigVnSegmentIds) GetVnSegmentIdValueOk() (*string, bool)`

GetVnSegmentIdValueOk returns a tuple with the VnSegmentIdValue field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnSegmentIdValue

`func (o *SiteToWanLayer2ProtocolDataForwardingConfigVnSegmentIds) SetVnSegmentIdValue(v string)`

SetVnSegmentIdValue sets VnSegmentIdValue field to given value.


### GetVnSegmentIdUpperRange

`func (o *SiteToWanLayer2ProtocolDataForwardingConfigVnSegmentIds) GetVnSegmentIdUpperRange() string`

GetVnSegmentIdUpperRange returns the VnSegmentIdUpperRange field if non-nil, zero value otherwise.

### GetVnSegmentIdUpperRangeOk

`func (o *SiteToWanLayer2ProtocolDataForwardingConfigVnSegmentIds) GetVnSegmentIdUpperRangeOk() (*string, bool)`

GetVnSegmentIdUpperRangeOk returns a tuple with the VnSegmentIdUpperRange field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnSegmentIdUpperRange

`func (o *SiteToWanLayer2ProtocolDataForwardingConfigVnSegmentIds) SetVnSegmentIdUpperRange(v string)`

SetVnSegmentIdUpperRange sets VnSegmentIdUpperRange field to given value.

### HasVnSegmentIdUpperRange

`func (o *SiteToWanLayer2ProtocolDataForwardingConfigVnSegmentIds) HasVnSegmentIdUpperRange() bool`

HasVnSegmentIdUpperRange returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


