# SapInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | An identifier that is unique with respect to a NS. Representation: string of variable length.  | 
**SapdId** | **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | 
**SapName** | **string** | Human readable name for the SAP instance.  | 
**Description** | Pointer to **string** | Human readable description for the SAP instance.  | [optional] 
**SapProtocolInfo** | [**[]CpProtocolInfo**](CpProtocolInfo.md) | Network protocol information for this SAP.  | 

## Methods

### NewSapInfo

`func NewSapInfo(id string, sapdId string, sapName string, sapProtocolInfo []CpProtocolInfo, ) *SapInfo`

NewSapInfo instantiates a new SapInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSapInfoWithDefaults

`func NewSapInfoWithDefaults() *SapInfo`

NewSapInfoWithDefaults instantiates a new SapInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *SapInfo) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *SapInfo) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *SapInfo) SetId(v string)`

SetId sets Id field to given value.


### GetSapdId

`func (o *SapInfo) GetSapdId() string`

GetSapdId returns the SapdId field if non-nil, zero value otherwise.

### GetSapdIdOk

`func (o *SapInfo) GetSapdIdOk() (*string, bool)`

GetSapdIdOk returns a tuple with the SapdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSapdId

`func (o *SapInfo) SetSapdId(v string)`

SetSapdId sets SapdId field to given value.


### GetSapName

`func (o *SapInfo) GetSapName() string`

GetSapName returns the SapName field if non-nil, zero value otherwise.

### GetSapNameOk

`func (o *SapInfo) GetSapNameOk() (*string, bool)`

GetSapNameOk returns a tuple with the SapName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSapName

`func (o *SapInfo) SetSapName(v string)`

SetSapName sets SapName field to given value.


### GetDescription

`func (o *SapInfo) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *SapInfo) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *SapInfo) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *SapInfo) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetSapProtocolInfo

`func (o *SapInfo) GetSapProtocolInfo() []CpProtocolInfo`

GetSapProtocolInfo returns the SapProtocolInfo field if non-nil, zero value otherwise.

### GetSapProtocolInfoOk

`func (o *SapInfo) GetSapProtocolInfoOk() (*[]CpProtocolInfo, bool)`

GetSapProtocolInfoOk returns a tuple with the SapProtocolInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSapProtocolInfo

`func (o *SapInfo) SetSapProtocolInfo(v []CpProtocolInfo)`

SetSapProtocolInfo sets SapProtocolInfo field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


