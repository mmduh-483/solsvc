# VnfInstance

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | An identifier with the intention of being globally unique.  | 
**VnfInstanceName** | Pointer to **string** | Name of the VNF instance. Modifications to this attribute can be requested using the \&quot;ModifyVnfInfoData\&quot; structure.  | [optional] 
**VnfInstanceDescription** | Pointer to **string** | Human-readable description of the VNF instance. Modifications to this attribute can be requested using the \&quot;ModifyVnfInfoData\&quot; structure.  | [optional] 
**VnfdId** | **string** | An identifier with the intention of being globally unique.  | 
**VnfProvider** | **string** | Provider of the VNF and the VNFD. The value is copied from the VNFD.  | 
**VnfProductName** | **string** | Name to identify the VNF Product. The value is copied from the VNFD.  | 
**VnfSoftwareVersion** | **string** | A Version. Representation: string of variable length.  | 
**VnfdVersion** | **string** | A Version. Representation: string of variable length.  | 
**VnfPkgId** | **string** | An identifier with the intention of being globally unique.  | 
**VnfConfigurableProperties** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 
**VimId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**InstantiationState** | **string** | The instantiation state of the VNF. Permitted values: - NOT_INSTANTIATED: The VNF instance is terminated or not instantiated. - INSTANTIATED: The VNF instance is instantiated.  | 
**InstantiatedVnfInfo** | Pointer to [**VnfInstanceInstantiatedVnfInfo**](VnfInstanceInstantiatedVnfInfo.md) |  | [optional] 
**Metadata** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 
**Extensions** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 

## Methods

### NewVnfInstance

`func NewVnfInstance(id string, vnfdId string, vnfProvider string, vnfProductName string, vnfSoftwareVersion string, vnfdVersion string, vnfPkgId string, instantiationState string, ) *VnfInstance`

NewVnfInstance instantiates a new VnfInstance object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVnfInstanceWithDefaults

`func NewVnfInstanceWithDefaults() *VnfInstance`

NewVnfInstanceWithDefaults instantiates a new VnfInstance object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *VnfInstance) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *VnfInstance) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *VnfInstance) SetId(v string)`

SetId sets Id field to given value.


### GetVnfInstanceName

`func (o *VnfInstance) GetVnfInstanceName() string`

GetVnfInstanceName returns the VnfInstanceName field if non-nil, zero value otherwise.

### GetVnfInstanceNameOk

`func (o *VnfInstance) GetVnfInstanceNameOk() (*string, bool)`

GetVnfInstanceNameOk returns a tuple with the VnfInstanceName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceName

`func (o *VnfInstance) SetVnfInstanceName(v string)`

SetVnfInstanceName sets VnfInstanceName field to given value.

### HasVnfInstanceName

`func (o *VnfInstance) HasVnfInstanceName() bool`

HasVnfInstanceName returns a boolean if a field has been set.

### GetVnfInstanceDescription

`func (o *VnfInstance) GetVnfInstanceDescription() string`

GetVnfInstanceDescription returns the VnfInstanceDescription field if non-nil, zero value otherwise.

### GetVnfInstanceDescriptionOk

`func (o *VnfInstance) GetVnfInstanceDescriptionOk() (*string, bool)`

GetVnfInstanceDescriptionOk returns a tuple with the VnfInstanceDescription field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceDescription

`func (o *VnfInstance) SetVnfInstanceDescription(v string)`

SetVnfInstanceDescription sets VnfInstanceDescription field to given value.

### HasVnfInstanceDescription

`func (o *VnfInstance) HasVnfInstanceDescription() bool`

HasVnfInstanceDescription returns a boolean if a field has been set.

### GetVnfdId

`func (o *VnfInstance) GetVnfdId() string`

GetVnfdId returns the VnfdId field if non-nil, zero value otherwise.

### GetVnfdIdOk

`func (o *VnfInstance) GetVnfdIdOk() (*string, bool)`

GetVnfdIdOk returns a tuple with the VnfdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfdId

`func (o *VnfInstance) SetVnfdId(v string)`

SetVnfdId sets VnfdId field to given value.


### GetVnfProvider

`func (o *VnfInstance) GetVnfProvider() string`

GetVnfProvider returns the VnfProvider field if non-nil, zero value otherwise.

### GetVnfProviderOk

`func (o *VnfInstance) GetVnfProviderOk() (*string, bool)`

GetVnfProviderOk returns a tuple with the VnfProvider field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfProvider

`func (o *VnfInstance) SetVnfProvider(v string)`

SetVnfProvider sets VnfProvider field to given value.


### GetVnfProductName

`func (o *VnfInstance) GetVnfProductName() string`

GetVnfProductName returns the VnfProductName field if non-nil, zero value otherwise.

### GetVnfProductNameOk

`func (o *VnfInstance) GetVnfProductNameOk() (*string, bool)`

GetVnfProductNameOk returns a tuple with the VnfProductName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfProductName

`func (o *VnfInstance) SetVnfProductName(v string)`

SetVnfProductName sets VnfProductName field to given value.


### GetVnfSoftwareVersion

`func (o *VnfInstance) GetVnfSoftwareVersion() string`

GetVnfSoftwareVersion returns the VnfSoftwareVersion field if non-nil, zero value otherwise.

### GetVnfSoftwareVersionOk

`func (o *VnfInstance) GetVnfSoftwareVersionOk() (*string, bool)`

GetVnfSoftwareVersionOk returns a tuple with the VnfSoftwareVersion field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfSoftwareVersion

`func (o *VnfInstance) SetVnfSoftwareVersion(v string)`

SetVnfSoftwareVersion sets VnfSoftwareVersion field to given value.


### GetVnfdVersion

`func (o *VnfInstance) GetVnfdVersion() string`

GetVnfdVersion returns the VnfdVersion field if non-nil, zero value otherwise.

### GetVnfdVersionOk

`func (o *VnfInstance) GetVnfdVersionOk() (*string, bool)`

GetVnfdVersionOk returns a tuple with the VnfdVersion field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfdVersion

`func (o *VnfInstance) SetVnfdVersion(v string)`

SetVnfdVersion sets VnfdVersion field to given value.


### GetVnfPkgId

`func (o *VnfInstance) GetVnfPkgId() string`

GetVnfPkgId returns the VnfPkgId field if non-nil, zero value otherwise.

### GetVnfPkgIdOk

`func (o *VnfInstance) GetVnfPkgIdOk() (*string, bool)`

GetVnfPkgIdOk returns a tuple with the VnfPkgId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfPkgId

`func (o *VnfInstance) SetVnfPkgId(v string)`

SetVnfPkgId sets VnfPkgId field to given value.


### GetVnfConfigurableProperties

`func (o *VnfInstance) GetVnfConfigurableProperties() map[string]interface{}`

GetVnfConfigurableProperties returns the VnfConfigurableProperties field if non-nil, zero value otherwise.

### GetVnfConfigurablePropertiesOk

`func (o *VnfInstance) GetVnfConfigurablePropertiesOk() (*map[string]interface{}, bool)`

GetVnfConfigurablePropertiesOk returns a tuple with the VnfConfigurableProperties field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfConfigurableProperties

`func (o *VnfInstance) SetVnfConfigurableProperties(v map[string]interface{})`

SetVnfConfigurableProperties sets VnfConfigurableProperties field to given value.

### HasVnfConfigurableProperties

`func (o *VnfInstance) HasVnfConfigurableProperties() bool`

HasVnfConfigurableProperties returns a boolean if a field has been set.

### GetVimId

`func (o *VnfInstance) GetVimId() string`

GetVimId returns the VimId field if non-nil, zero value otherwise.

### GetVimIdOk

`func (o *VnfInstance) GetVimIdOk() (*string, bool)`

GetVimIdOk returns a tuple with the VimId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVimId

`func (o *VnfInstance) SetVimId(v string)`

SetVimId sets VimId field to given value.

### HasVimId

`func (o *VnfInstance) HasVimId() bool`

HasVimId returns a boolean if a field has been set.

### GetInstantiationState

`func (o *VnfInstance) GetInstantiationState() string`

GetInstantiationState returns the InstantiationState field if non-nil, zero value otherwise.

### GetInstantiationStateOk

`func (o *VnfInstance) GetInstantiationStateOk() (*string, bool)`

GetInstantiationStateOk returns a tuple with the InstantiationState field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInstantiationState

`func (o *VnfInstance) SetInstantiationState(v string)`

SetInstantiationState sets InstantiationState field to given value.


### GetInstantiatedVnfInfo

`func (o *VnfInstance) GetInstantiatedVnfInfo() VnfInstanceInstantiatedVnfInfo`

GetInstantiatedVnfInfo returns the InstantiatedVnfInfo field if non-nil, zero value otherwise.

### GetInstantiatedVnfInfoOk

`func (o *VnfInstance) GetInstantiatedVnfInfoOk() (*VnfInstanceInstantiatedVnfInfo, bool)`

GetInstantiatedVnfInfoOk returns a tuple with the InstantiatedVnfInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInstantiatedVnfInfo

`func (o *VnfInstance) SetInstantiatedVnfInfo(v VnfInstanceInstantiatedVnfInfo)`

SetInstantiatedVnfInfo sets InstantiatedVnfInfo field to given value.

### HasInstantiatedVnfInfo

`func (o *VnfInstance) HasInstantiatedVnfInfo() bool`

HasInstantiatedVnfInfo returns a boolean if a field has been set.

### GetMetadata

`func (o *VnfInstance) GetMetadata() map[string]interface{}`

GetMetadata returns the Metadata field if non-nil, zero value otherwise.

### GetMetadataOk

`func (o *VnfInstance) GetMetadataOk() (*map[string]interface{}, bool)`

GetMetadataOk returns a tuple with the Metadata field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMetadata

`func (o *VnfInstance) SetMetadata(v map[string]interface{})`

SetMetadata sets Metadata field to given value.

### HasMetadata

`func (o *VnfInstance) HasMetadata() bool`

HasMetadata returns a boolean if a field has been set.

### GetExtensions

`func (o *VnfInstance) GetExtensions() map[string]interface{}`

GetExtensions returns the Extensions field if non-nil, zero value otherwise.

### GetExtensionsOk

`func (o *VnfInstance) GetExtensionsOk() (*map[string]interface{}, bool)`

GetExtensionsOk returns a tuple with the Extensions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtensions

`func (o *VnfInstance) SetExtensions(v map[string]interface{})`

SetExtensions sets Extensions field to given value.

### HasExtensions

`func (o *VnfInstance) HasExtensions() bool`

HasExtensions returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


