# ParamsForNestedNs

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NsProfileId** | **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | 
**AdditionalParam** | Pointer to **[]map[string]interface{}** | Additional parameters that are to be applied on a per nested NS instance.  | [optional] 

## Methods

### NewParamsForNestedNs

`func NewParamsForNestedNs(nsProfileId string, ) *ParamsForNestedNs`

NewParamsForNestedNs instantiates a new ParamsForNestedNs object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewParamsForNestedNsWithDefaults

`func NewParamsForNestedNsWithDefaults() *ParamsForNestedNs`

NewParamsForNestedNsWithDefaults instantiates a new ParamsForNestedNs object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetNsProfileId

`func (o *ParamsForNestedNs) GetNsProfileId() string`

GetNsProfileId returns the NsProfileId field if non-nil, zero value otherwise.

### GetNsProfileIdOk

`func (o *ParamsForNestedNs) GetNsProfileIdOk() (*string, bool)`

GetNsProfileIdOk returns a tuple with the NsProfileId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsProfileId

`func (o *ParamsForNestedNs) SetNsProfileId(v string)`

SetNsProfileId sets NsProfileId field to given value.


### GetAdditionalParam

`func (o *ParamsForNestedNs) GetAdditionalParam() []map[string]interface{}`

GetAdditionalParam returns the AdditionalParam field if non-nil, zero value otherwise.

### GetAdditionalParamOk

`func (o *ParamsForNestedNs) GetAdditionalParamOk() (*[]map[string]interface{}, bool)`

GetAdditionalParamOk returns a tuple with the AdditionalParam field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdditionalParam

`func (o *ParamsForNestedNs) SetAdditionalParam(v []map[string]interface{})`

SetAdditionalParam sets AdditionalParam field to given value.

### HasAdditionalParam

`func (o *ParamsForNestedNs) HasAdditionalParam() bool`

HasAdditionalParam returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


