# SiteToWanLayer2ProtocolDataLayer2ConnectionInfoVxlanConfig

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PeerMode** | **string** | Type of VXLAN access mode. Default value is \&quot;STATIC\&quot;. Permitted values: - STATIC - BGP_EVPN  | 
**Peers** | Pointer to **[]string** | List of IP addresses of VTEP peers when using static mode.  | [optional] 

## Methods

### NewSiteToWanLayer2ProtocolDataLayer2ConnectionInfoVxlanConfig

`func NewSiteToWanLayer2ProtocolDataLayer2ConnectionInfoVxlanConfig(peerMode string, ) *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoVxlanConfig`

NewSiteToWanLayer2ProtocolDataLayer2ConnectionInfoVxlanConfig instantiates a new SiteToWanLayer2ProtocolDataLayer2ConnectionInfoVxlanConfig object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSiteToWanLayer2ProtocolDataLayer2ConnectionInfoVxlanConfigWithDefaults

`func NewSiteToWanLayer2ProtocolDataLayer2ConnectionInfoVxlanConfigWithDefaults() *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoVxlanConfig`

NewSiteToWanLayer2ProtocolDataLayer2ConnectionInfoVxlanConfigWithDefaults instantiates a new SiteToWanLayer2ProtocolDataLayer2ConnectionInfoVxlanConfig object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPeerMode

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoVxlanConfig) GetPeerMode() string`

GetPeerMode returns the PeerMode field if non-nil, zero value otherwise.

### GetPeerModeOk

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoVxlanConfig) GetPeerModeOk() (*string, bool)`

GetPeerModeOk returns a tuple with the PeerMode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPeerMode

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoVxlanConfig) SetPeerMode(v string)`

SetPeerMode sets PeerMode field to given value.


### GetPeers

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoVxlanConfig) GetPeers() []string`

GetPeers returns the Peers field if non-nil, zero value otherwise.

### GetPeersOk

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoVxlanConfig) GetPeersOk() (*[]string, bool)`

GetPeersOk returns a tuple with the Peers field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPeers

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoVxlanConfig) SetPeers(v []string)`

SetPeers sets Peers field to given value.

### HasPeers

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfoVxlanConfig) HasPeers() bool`

HasPeers returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


