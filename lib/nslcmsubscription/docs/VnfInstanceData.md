# VnfInstanceData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VnfInstanceId** | **string** | An identifier with the intention of being globally unique.  | 
**VnfProfileId** | **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | 

## Methods

### NewVnfInstanceData

`func NewVnfInstanceData(vnfInstanceId string, vnfProfileId string, ) *VnfInstanceData`

NewVnfInstanceData instantiates a new VnfInstanceData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVnfInstanceDataWithDefaults

`func NewVnfInstanceDataWithDefaults() *VnfInstanceData`

NewVnfInstanceDataWithDefaults instantiates a new VnfInstanceData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVnfInstanceId

`func (o *VnfInstanceData) GetVnfInstanceId() string`

GetVnfInstanceId returns the VnfInstanceId field if non-nil, zero value otherwise.

### GetVnfInstanceIdOk

`func (o *VnfInstanceData) GetVnfInstanceIdOk() (*string, bool)`

GetVnfInstanceIdOk returns a tuple with the VnfInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceId

`func (o *VnfInstanceData) SetVnfInstanceId(v string)`

SetVnfInstanceId sets VnfInstanceId field to given value.


### GetVnfProfileId

`func (o *VnfInstanceData) GetVnfProfileId() string`

GetVnfProfileId returns the VnfProfileId field if non-nil, zero value otherwise.

### GetVnfProfileIdOk

`func (o *VnfInstanceData) GetVnfProfileIdOk() (*string, bool)`

GetVnfProfileIdOk returns a tuple with the VnfProfileId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfProfileId

`func (o *VnfInstanceData) SetVnfProfileId(v string)`

SetVnfProfileId sets VnfProfileId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


