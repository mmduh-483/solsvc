# ChangeExtVnfConnectivityData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VnfInstanceId** | **string** | An identifier with the intention of being globally unique.  | 
**ExtVirtualLinks** | [**[]ExtVirtualLinkData**](ExtVirtualLinkData.md) | Information about external VLs to change (e.g. connect the VNF to).  | 
**AdditionalParams** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 

## Methods

### NewChangeExtVnfConnectivityData

`func NewChangeExtVnfConnectivityData(vnfInstanceId string, extVirtualLinks []ExtVirtualLinkData, ) *ChangeExtVnfConnectivityData`

NewChangeExtVnfConnectivityData instantiates a new ChangeExtVnfConnectivityData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewChangeExtVnfConnectivityDataWithDefaults

`func NewChangeExtVnfConnectivityDataWithDefaults() *ChangeExtVnfConnectivityData`

NewChangeExtVnfConnectivityDataWithDefaults instantiates a new ChangeExtVnfConnectivityData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVnfInstanceId

`func (o *ChangeExtVnfConnectivityData) GetVnfInstanceId() string`

GetVnfInstanceId returns the VnfInstanceId field if non-nil, zero value otherwise.

### GetVnfInstanceIdOk

`func (o *ChangeExtVnfConnectivityData) GetVnfInstanceIdOk() (*string, bool)`

GetVnfInstanceIdOk returns a tuple with the VnfInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceId

`func (o *ChangeExtVnfConnectivityData) SetVnfInstanceId(v string)`

SetVnfInstanceId sets VnfInstanceId field to given value.


### GetExtVirtualLinks

`func (o *ChangeExtVnfConnectivityData) GetExtVirtualLinks() []ExtVirtualLinkData`

GetExtVirtualLinks returns the ExtVirtualLinks field if non-nil, zero value otherwise.

### GetExtVirtualLinksOk

`func (o *ChangeExtVnfConnectivityData) GetExtVirtualLinksOk() (*[]ExtVirtualLinkData, bool)`

GetExtVirtualLinksOk returns a tuple with the ExtVirtualLinks field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtVirtualLinks

`func (o *ChangeExtVnfConnectivityData) SetExtVirtualLinks(v []ExtVirtualLinkData)`

SetExtVirtualLinks sets ExtVirtualLinks field to given value.


### GetAdditionalParams

`func (o *ChangeExtVnfConnectivityData) GetAdditionalParams() map[string]interface{}`

GetAdditionalParams returns the AdditionalParams field if non-nil, zero value otherwise.

### GetAdditionalParamsOk

`func (o *ChangeExtVnfConnectivityData) GetAdditionalParamsOk() (*map[string]interface{}, bool)`

GetAdditionalParamsOk returns a tuple with the AdditionalParams field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdditionalParams

`func (o *ChangeExtVnfConnectivityData) SetAdditionalParams(v map[string]interface{})`

SetAdditionalParams sets AdditionalParams field to given value.

### HasAdditionalParams

`func (o *ChangeExtVnfConnectivityData) HasAdditionalParams() bool`

HasAdditionalParams returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


