# VnfSnapshot

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | An identifier with the intention of being globally unique.  | 
**VnfInstanceId** | **string** | An identifier with the intention of being globally unique.  | 
**CreationStartedAt** | Pointer to **time.Time** | Date-time stamp. Representation: String formatted according toas defined by the date-time production in IETF RFC 3339.  | [optional] 
**CreationFinishedAt** | Pointer to **time.Time** | Date-time stamp. Representation: String formatted according toas defined by the date-time production in IETF RFC 3339.  | [optional] 
**VnfdId** | **string** | An identifier with the intention of being globally unique.  | 
**VnfInstance** | [**VnfInstance**](VnfInstance.md) |  | 
**VnfcSnapshots** | [**[]VnfcSnapshotInfo**](VnfcSnapshotInfo.md) | Information about VNFC Snapshots constituting this VNF Snapshot.  | 
**UserDefinedData** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 

## Methods

### NewVnfSnapshot

`func NewVnfSnapshot(id string, vnfInstanceId string, vnfdId string, vnfInstance VnfInstance, vnfcSnapshots []VnfcSnapshotInfo, ) *VnfSnapshot`

NewVnfSnapshot instantiates a new VnfSnapshot object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVnfSnapshotWithDefaults

`func NewVnfSnapshotWithDefaults() *VnfSnapshot`

NewVnfSnapshotWithDefaults instantiates a new VnfSnapshot object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *VnfSnapshot) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *VnfSnapshot) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *VnfSnapshot) SetId(v string)`

SetId sets Id field to given value.


### GetVnfInstanceId

`func (o *VnfSnapshot) GetVnfInstanceId() string`

GetVnfInstanceId returns the VnfInstanceId field if non-nil, zero value otherwise.

### GetVnfInstanceIdOk

`func (o *VnfSnapshot) GetVnfInstanceIdOk() (*string, bool)`

GetVnfInstanceIdOk returns a tuple with the VnfInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceId

`func (o *VnfSnapshot) SetVnfInstanceId(v string)`

SetVnfInstanceId sets VnfInstanceId field to given value.


### GetCreationStartedAt

`func (o *VnfSnapshot) GetCreationStartedAt() time.Time`

GetCreationStartedAt returns the CreationStartedAt field if non-nil, zero value otherwise.

### GetCreationStartedAtOk

`func (o *VnfSnapshot) GetCreationStartedAtOk() (*time.Time, bool)`

GetCreationStartedAtOk returns a tuple with the CreationStartedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreationStartedAt

`func (o *VnfSnapshot) SetCreationStartedAt(v time.Time)`

SetCreationStartedAt sets CreationStartedAt field to given value.

### HasCreationStartedAt

`func (o *VnfSnapshot) HasCreationStartedAt() bool`

HasCreationStartedAt returns a boolean if a field has been set.

### GetCreationFinishedAt

`func (o *VnfSnapshot) GetCreationFinishedAt() time.Time`

GetCreationFinishedAt returns the CreationFinishedAt field if non-nil, zero value otherwise.

### GetCreationFinishedAtOk

`func (o *VnfSnapshot) GetCreationFinishedAtOk() (*time.Time, bool)`

GetCreationFinishedAtOk returns a tuple with the CreationFinishedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreationFinishedAt

`func (o *VnfSnapshot) SetCreationFinishedAt(v time.Time)`

SetCreationFinishedAt sets CreationFinishedAt field to given value.

### HasCreationFinishedAt

`func (o *VnfSnapshot) HasCreationFinishedAt() bool`

HasCreationFinishedAt returns a boolean if a field has been set.

### GetVnfdId

`func (o *VnfSnapshot) GetVnfdId() string`

GetVnfdId returns the VnfdId field if non-nil, zero value otherwise.

### GetVnfdIdOk

`func (o *VnfSnapshot) GetVnfdIdOk() (*string, bool)`

GetVnfdIdOk returns a tuple with the VnfdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfdId

`func (o *VnfSnapshot) SetVnfdId(v string)`

SetVnfdId sets VnfdId field to given value.


### GetVnfInstance

`func (o *VnfSnapshot) GetVnfInstance() VnfInstance`

GetVnfInstance returns the VnfInstance field if non-nil, zero value otherwise.

### GetVnfInstanceOk

`func (o *VnfSnapshot) GetVnfInstanceOk() (*VnfInstance, bool)`

GetVnfInstanceOk returns a tuple with the VnfInstance field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstance

`func (o *VnfSnapshot) SetVnfInstance(v VnfInstance)`

SetVnfInstance sets VnfInstance field to given value.


### GetVnfcSnapshots

`func (o *VnfSnapshot) GetVnfcSnapshots() []VnfcSnapshotInfo`

GetVnfcSnapshots returns the VnfcSnapshots field if non-nil, zero value otherwise.

### GetVnfcSnapshotsOk

`func (o *VnfSnapshot) GetVnfcSnapshotsOk() (*[]VnfcSnapshotInfo, bool)`

GetVnfcSnapshotsOk returns a tuple with the VnfcSnapshots field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfcSnapshots

`func (o *VnfSnapshot) SetVnfcSnapshots(v []VnfcSnapshotInfo)`

SetVnfcSnapshots sets VnfcSnapshots field to given value.


### GetUserDefinedData

`func (o *VnfSnapshot) GetUserDefinedData() map[string]interface{}`

GetUserDefinedData returns the UserDefinedData field if non-nil, zero value otherwise.

### GetUserDefinedDataOk

`func (o *VnfSnapshot) GetUserDefinedDataOk() (*map[string]interface{}, bool)`

GetUserDefinedDataOk returns a tuple with the UserDefinedData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUserDefinedData

`func (o *VnfSnapshot) SetUserDefinedData(v map[string]interface{})`

SetUserDefinedData sets UserDefinedData field to given value.

### HasUserDefinedData

`func (o *VnfSnapshot) HasUserDefinedData() bool`

HasUserDefinedData returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


