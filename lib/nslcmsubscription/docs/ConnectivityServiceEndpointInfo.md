# ConnectivityServiceEndpointInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ConnectivityServiceEndpointId** | **string** | An identifier with the intention of being globally unique.  | 
**VimId** | **string** | An identifier with the intention of being globally unique.  | 
**SiteToWanLayer2ProtocolData** | Pointer to [**SiteToWanLayer2ProtocolData**](SiteToWanLayer2ProtocolData.md) |  | [optional] 
**SiteToWanLayer3ProtocolData** | Pointer to [**SiteToWanLayer3ProtocolData**](SiteToWanLayer3ProtocolData.md) |  | [optional] 

## Methods

### NewConnectivityServiceEndpointInfo

`func NewConnectivityServiceEndpointInfo(connectivityServiceEndpointId string, vimId string, ) *ConnectivityServiceEndpointInfo`

NewConnectivityServiceEndpointInfo instantiates a new ConnectivityServiceEndpointInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewConnectivityServiceEndpointInfoWithDefaults

`func NewConnectivityServiceEndpointInfoWithDefaults() *ConnectivityServiceEndpointInfo`

NewConnectivityServiceEndpointInfoWithDefaults instantiates a new ConnectivityServiceEndpointInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetConnectivityServiceEndpointId

`func (o *ConnectivityServiceEndpointInfo) GetConnectivityServiceEndpointId() string`

GetConnectivityServiceEndpointId returns the ConnectivityServiceEndpointId field if non-nil, zero value otherwise.

### GetConnectivityServiceEndpointIdOk

`func (o *ConnectivityServiceEndpointInfo) GetConnectivityServiceEndpointIdOk() (*string, bool)`

GetConnectivityServiceEndpointIdOk returns a tuple with the ConnectivityServiceEndpointId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConnectivityServiceEndpointId

`func (o *ConnectivityServiceEndpointInfo) SetConnectivityServiceEndpointId(v string)`

SetConnectivityServiceEndpointId sets ConnectivityServiceEndpointId field to given value.


### GetVimId

`func (o *ConnectivityServiceEndpointInfo) GetVimId() string`

GetVimId returns the VimId field if non-nil, zero value otherwise.

### GetVimIdOk

`func (o *ConnectivityServiceEndpointInfo) GetVimIdOk() (*string, bool)`

GetVimIdOk returns a tuple with the VimId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVimId

`func (o *ConnectivityServiceEndpointInfo) SetVimId(v string)`

SetVimId sets VimId field to given value.


### GetSiteToWanLayer2ProtocolData

`func (o *ConnectivityServiceEndpointInfo) GetSiteToWanLayer2ProtocolData() SiteToWanLayer2ProtocolData`

GetSiteToWanLayer2ProtocolData returns the SiteToWanLayer2ProtocolData field if non-nil, zero value otherwise.

### GetSiteToWanLayer2ProtocolDataOk

`func (o *ConnectivityServiceEndpointInfo) GetSiteToWanLayer2ProtocolDataOk() (*SiteToWanLayer2ProtocolData, bool)`

GetSiteToWanLayer2ProtocolDataOk returns a tuple with the SiteToWanLayer2ProtocolData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSiteToWanLayer2ProtocolData

`func (o *ConnectivityServiceEndpointInfo) SetSiteToWanLayer2ProtocolData(v SiteToWanLayer2ProtocolData)`

SetSiteToWanLayer2ProtocolData sets SiteToWanLayer2ProtocolData field to given value.

### HasSiteToWanLayer2ProtocolData

`func (o *ConnectivityServiceEndpointInfo) HasSiteToWanLayer2ProtocolData() bool`

HasSiteToWanLayer2ProtocolData returns a boolean if a field has been set.

### GetSiteToWanLayer3ProtocolData

`func (o *ConnectivityServiceEndpointInfo) GetSiteToWanLayer3ProtocolData() SiteToWanLayer3ProtocolData`

GetSiteToWanLayer3ProtocolData returns the SiteToWanLayer3ProtocolData field if non-nil, zero value otherwise.

### GetSiteToWanLayer3ProtocolDataOk

`func (o *ConnectivityServiceEndpointInfo) GetSiteToWanLayer3ProtocolDataOk() (*SiteToWanLayer3ProtocolData, bool)`

GetSiteToWanLayer3ProtocolDataOk returns a tuple with the SiteToWanLayer3ProtocolData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSiteToWanLayer3ProtocolData

`func (o *ConnectivityServiceEndpointInfo) SetSiteToWanLayer3ProtocolData(v SiteToWanLayer3ProtocolData)`

SetSiteToWanLayer3ProtocolData sets SiteToWanLayer3ProtocolData field to given value.

### HasSiteToWanLayer3ProtocolData

`func (o *ConnectivityServiceEndpointInfo) HasSiteToWanLayer3ProtocolData() bool`

HasSiteToWanLayer3ProtocolData returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


