# NsCpHandle

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VnfInstanceId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**VnfExtCpInstanceId** | Pointer to **string** | An identifier that is unique for the respective type within a VNF instance, but may not be globally unique.  | [optional] 
**PnfInfoId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**PnfExtCpInstanceId** | Pointer to **string** | An Identifier that is unique within respect to a PNF. Representation: string of variable length.  | [optional] 
**NsInstanceId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**NsSapInstanceId** | Pointer to **string** | An identifier that is unique with respect to a NS. Representation: string of variable length.  | [optional] 

## Methods

### NewNsCpHandle

`func NewNsCpHandle() *NsCpHandle`

NewNsCpHandle instantiates a new NsCpHandle object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNsCpHandleWithDefaults

`func NewNsCpHandleWithDefaults() *NsCpHandle`

NewNsCpHandleWithDefaults instantiates a new NsCpHandle object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVnfInstanceId

`func (o *NsCpHandle) GetVnfInstanceId() string`

GetVnfInstanceId returns the VnfInstanceId field if non-nil, zero value otherwise.

### GetVnfInstanceIdOk

`func (o *NsCpHandle) GetVnfInstanceIdOk() (*string, bool)`

GetVnfInstanceIdOk returns a tuple with the VnfInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceId

`func (o *NsCpHandle) SetVnfInstanceId(v string)`

SetVnfInstanceId sets VnfInstanceId field to given value.

### HasVnfInstanceId

`func (o *NsCpHandle) HasVnfInstanceId() bool`

HasVnfInstanceId returns a boolean if a field has been set.

### GetVnfExtCpInstanceId

`func (o *NsCpHandle) GetVnfExtCpInstanceId() string`

GetVnfExtCpInstanceId returns the VnfExtCpInstanceId field if non-nil, zero value otherwise.

### GetVnfExtCpInstanceIdOk

`func (o *NsCpHandle) GetVnfExtCpInstanceIdOk() (*string, bool)`

GetVnfExtCpInstanceIdOk returns a tuple with the VnfExtCpInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfExtCpInstanceId

`func (o *NsCpHandle) SetVnfExtCpInstanceId(v string)`

SetVnfExtCpInstanceId sets VnfExtCpInstanceId field to given value.

### HasVnfExtCpInstanceId

`func (o *NsCpHandle) HasVnfExtCpInstanceId() bool`

HasVnfExtCpInstanceId returns a boolean if a field has been set.

### GetPnfInfoId

`func (o *NsCpHandle) GetPnfInfoId() string`

GetPnfInfoId returns the PnfInfoId field if non-nil, zero value otherwise.

### GetPnfInfoIdOk

`func (o *NsCpHandle) GetPnfInfoIdOk() (*string, bool)`

GetPnfInfoIdOk returns a tuple with the PnfInfoId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPnfInfoId

`func (o *NsCpHandle) SetPnfInfoId(v string)`

SetPnfInfoId sets PnfInfoId field to given value.

### HasPnfInfoId

`func (o *NsCpHandle) HasPnfInfoId() bool`

HasPnfInfoId returns a boolean if a field has been set.

### GetPnfExtCpInstanceId

`func (o *NsCpHandle) GetPnfExtCpInstanceId() string`

GetPnfExtCpInstanceId returns the PnfExtCpInstanceId field if non-nil, zero value otherwise.

### GetPnfExtCpInstanceIdOk

`func (o *NsCpHandle) GetPnfExtCpInstanceIdOk() (*string, bool)`

GetPnfExtCpInstanceIdOk returns a tuple with the PnfExtCpInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPnfExtCpInstanceId

`func (o *NsCpHandle) SetPnfExtCpInstanceId(v string)`

SetPnfExtCpInstanceId sets PnfExtCpInstanceId field to given value.

### HasPnfExtCpInstanceId

`func (o *NsCpHandle) HasPnfExtCpInstanceId() bool`

HasPnfExtCpInstanceId returns a boolean if a field has been set.

### GetNsInstanceId

`func (o *NsCpHandle) GetNsInstanceId() string`

GetNsInstanceId returns the NsInstanceId field if non-nil, zero value otherwise.

### GetNsInstanceIdOk

`func (o *NsCpHandle) GetNsInstanceIdOk() (*string, bool)`

GetNsInstanceIdOk returns a tuple with the NsInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsInstanceId

`func (o *NsCpHandle) SetNsInstanceId(v string)`

SetNsInstanceId sets NsInstanceId field to given value.

### HasNsInstanceId

`func (o *NsCpHandle) HasNsInstanceId() bool`

HasNsInstanceId returns a boolean if a field has been set.

### GetNsSapInstanceId

`func (o *NsCpHandle) GetNsSapInstanceId() string`

GetNsSapInstanceId returns the NsSapInstanceId field if non-nil, zero value otherwise.

### GetNsSapInstanceIdOk

`func (o *NsCpHandle) GetNsSapInstanceIdOk() (*string, bool)`

GetNsSapInstanceIdOk returns a tuple with the NsSapInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsSapInstanceId

`func (o *NsCpHandle) SetNsSapInstanceId(v string)`

SetNsSapInstanceId sets NsSapInstanceId field to given value.

### HasNsSapInstanceId

`func (o *NsCpHandle) HasNsSapInstanceId() bool`

HasNsSapInstanceId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


