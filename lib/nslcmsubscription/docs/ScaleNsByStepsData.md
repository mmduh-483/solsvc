# ScaleNsByStepsData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ScalingDirection** | **string** | The scaling direction. Possible values are: - SCALE_IN - SCALE_OUT.  | 
**AspectId** | **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | 
**NumberOfSteps** | Pointer to **int32** | The number of scaling steps to be performed. Defaults to 1.  | [optional] [default to 1]

## Methods

### NewScaleNsByStepsData

`func NewScaleNsByStepsData(scalingDirection string, aspectId string, ) *ScaleNsByStepsData`

NewScaleNsByStepsData instantiates a new ScaleNsByStepsData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewScaleNsByStepsDataWithDefaults

`func NewScaleNsByStepsDataWithDefaults() *ScaleNsByStepsData`

NewScaleNsByStepsDataWithDefaults instantiates a new ScaleNsByStepsData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetScalingDirection

`func (o *ScaleNsByStepsData) GetScalingDirection() string`

GetScalingDirection returns the ScalingDirection field if non-nil, zero value otherwise.

### GetScalingDirectionOk

`func (o *ScaleNsByStepsData) GetScalingDirectionOk() (*string, bool)`

GetScalingDirectionOk returns a tuple with the ScalingDirection field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScalingDirection

`func (o *ScaleNsByStepsData) SetScalingDirection(v string)`

SetScalingDirection sets ScalingDirection field to given value.


### GetAspectId

`func (o *ScaleNsByStepsData) GetAspectId() string`

GetAspectId returns the AspectId field if non-nil, zero value otherwise.

### GetAspectIdOk

`func (o *ScaleNsByStepsData) GetAspectIdOk() (*string, bool)`

GetAspectIdOk returns a tuple with the AspectId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAspectId

`func (o *ScaleNsByStepsData) SetAspectId(v string)`

SetAspectId sets AspectId field to given value.


### GetNumberOfSteps

`func (o *ScaleNsByStepsData) GetNumberOfSteps() int32`

GetNumberOfSteps returns the NumberOfSteps field if non-nil, zero value otherwise.

### GetNumberOfStepsOk

`func (o *ScaleNsByStepsData) GetNumberOfStepsOk() (*int32, bool)`

GetNumberOfStepsOk returns a tuple with the NumberOfSteps field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNumberOfSteps

`func (o *ScaleNsByStepsData) SetNumberOfSteps(v int32)`

SetNumberOfSteps sets NumberOfSteps field to given value.

### HasNumberOfSteps

`func (o *ScaleNsByStepsData) HasNumberOfSteps() bool`

HasNumberOfSteps returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


