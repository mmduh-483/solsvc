# ScaleNsToLevelData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NsInstantiationLevel** | Pointer to **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | [optional] 
**NsScaleInfo** | Pointer to [**[]NsScaleInfo**](NsScaleInfo.md) | For each NS scaling aspect of the current DF, defines the target NS scale level to which the NS instance is to be scaled. See note.  | [optional] 

## Methods

### NewScaleNsToLevelData

`func NewScaleNsToLevelData() *ScaleNsToLevelData`

NewScaleNsToLevelData instantiates a new ScaleNsToLevelData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewScaleNsToLevelDataWithDefaults

`func NewScaleNsToLevelDataWithDefaults() *ScaleNsToLevelData`

NewScaleNsToLevelDataWithDefaults instantiates a new ScaleNsToLevelData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetNsInstantiationLevel

`func (o *ScaleNsToLevelData) GetNsInstantiationLevel() string`

GetNsInstantiationLevel returns the NsInstantiationLevel field if non-nil, zero value otherwise.

### GetNsInstantiationLevelOk

`func (o *ScaleNsToLevelData) GetNsInstantiationLevelOk() (*string, bool)`

GetNsInstantiationLevelOk returns a tuple with the NsInstantiationLevel field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsInstantiationLevel

`func (o *ScaleNsToLevelData) SetNsInstantiationLevel(v string)`

SetNsInstantiationLevel sets NsInstantiationLevel field to given value.

### HasNsInstantiationLevel

`func (o *ScaleNsToLevelData) HasNsInstantiationLevel() bool`

HasNsInstantiationLevel returns a boolean if a field has been set.

### GetNsScaleInfo

`func (o *ScaleNsToLevelData) GetNsScaleInfo() []NsScaleInfo`

GetNsScaleInfo returns the NsScaleInfo field if non-nil, zero value otherwise.

### GetNsScaleInfoOk

`func (o *ScaleNsToLevelData) GetNsScaleInfoOk() (*[]NsScaleInfo, bool)`

GetNsScaleInfoOk returns a tuple with the NsScaleInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsScaleInfo

`func (o *ScaleNsToLevelData) SetNsScaleInfo(v []NsScaleInfo)`

SetNsScaleInfo sets NsScaleInfo field to given value.

### HasNsScaleInfo

`func (o *ScaleNsToLevelData) HasNsScaleInfo() bool`

HasNsScaleInfo returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


