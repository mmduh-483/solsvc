# ModifyVnfInfoData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VnfInstanceId** | **string** | An identifier with the intention of being globally unique.  | 
**VnfInstanceName** | Pointer to **string** | New value of the \&quot;vnfInstanceName\&quot; attribute in \&quot;VnfInstance\&quot;, or \&quot;null\&quot; to remove the attribute.  | [optional] 
**VnfInstanceDescription** | Pointer to **string** | New value of the \&quot;vnfInstanceDescription\&quot; attribute in \&quot;VnfInstance\&quot;, or \&quot;null\&quot; to remove the attribute.  | [optional] 
**VnfdId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**VnfConfigurableProperties** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 
**Metadata** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 
**Extensions** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 

## Methods

### NewModifyVnfInfoData

`func NewModifyVnfInfoData(vnfInstanceId string, ) *ModifyVnfInfoData`

NewModifyVnfInfoData instantiates a new ModifyVnfInfoData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewModifyVnfInfoDataWithDefaults

`func NewModifyVnfInfoDataWithDefaults() *ModifyVnfInfoData`

NewModifyVnfInfoDataWithDefaults instantiates a new ModifyVnfInfoData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVnfInstanceId

`func (o *ModifyVnfInfoData) GetVnfInstanceId() string`

GetVnfInstanceId returns the VnfInstanceId field if non-nil, zero value otherwise.

### GetVnfInstanceIdOk

`func (o *ModifyVnfInfoData) GetVnfInstanceIdOk() (*string, bool)`

GetVnfInstanceIdOk returns a tuple with the VnfInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceId

`func (o *ModifyVnfInfoData) SetVnfInstanceId(v string)`

SetVnfInstanceId sets VnfInstanceId field to given value.


### GetVnfInstanceName

`func (o *ModifyVnfInfoData) GetVnfInstanceName() string`

GetVnfInstanceName returns the VnfInstanceName field if non-nil, zero value otherwise.

### GetVnfInstanceNameOk

`func (o *ModifyVnfInfoData) GetVnfInstanceNameOk() (*string, bool)`

GetVnfInstanceNameOk returns a tuple with the VnfInstanceName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceName

`func (o *ModifyVnfInfoData) SetVnfInstanceName(v string)`

SetVnfInstanceName sets VnfInstanceName field to given value.

### HasVnfInstanceName

`func (o *ModifyVnfInfoData) HasVnfInstanceName() bool`

HasVnfInstanceName returns a boolean if a field has been set.

### GetVnfInstanceDescription

`func (o *ModifyVnfInfoData) GetVnfInstanceDescription() string`

GetVnfInstanceDescription returns the VnfInstanceDescription field if non-nil, zero value otherwise.

### GetVnfInstanceDescriptionOk

`func (o *ModifyVnfInfoData) GetVnfInstanceDescriptionOk() (*string, bool)`

GetVnfInstanceDescriptionOk returns a tuple with the VnfInstanceDescription field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceDescription

`func (o *ModifyVnfInfoData) SetVnfInstanceDescription(v string)`

SetVnfInstanceDescription sets VnfInstanceDescription field to given value.

### HasVnfInstanceDescription

`func (o *ModifyVnfInfoData) HasVnfInstanceDescription() bool`

HasVnfInstanceDescription returns a boolean if a field has been set.

### GetVnfdId

`func (o *ModifyVnfInfoData) GetVnfdId() string`

GetVnfdId returns the VnfdId field if non-nil, zero value otherwise.

### GetVnfdIdOk

`func (o *ModifyVnfInfoData) GetVnfdIdOk() (*string, bool)`

GetVnfdIdOk returns a tuple with the VnfdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfdId

`func (o *ModifyVnfInfoData) SetVnfdId(v string)`

SetVnfdId sets VnfdId field to given value.

### HasVnfdId

`func (o *ModifyVnfInfoData) HasVnfdId() bool`

HasVnfdId returns a boolean if a field has been set.

### GetVnfConfigurableProperties

`func (o *ModifyVnfInfoData) GetVnfConfigurableProperties() map[string]interface{}`

GetVnfConfigurableProperties returns the VnfConfigurableProperties field if non-nil, zero value otherwise.

### GetVnfConfigurablePropertiesOk

`func (o *ModifyVnfInfoData) GetVnfConfigurablePropertiesOk() (*map[string]interface{}, bool)`

GetVnfConfigurablePropertiesOk returns a tuple with the VnfConfigurableProperties field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfConfigurableProperties

`func (o *ModifyVnfInfoData) SetVnfConfigurableProperties(v map[string]interface{})`

SetVnfConfigurableProperties sets VnfConfigurableProperties field to given value.

### HasVnfConfigurableProperties

`func (o *ModifyVnfInfoData) HasVnfConfigurableProperties() bool`

HasVnfConfigurableProperties returns a boolean if a field has been set.

### GetMetadata

`func (o *ModifyVnfInfoData) GetMetadata() map[string]interface{}`

GetMetadata returns the Metadata field if non-nil, zero value otherwise.

### GetMetadataOk

`func (o *ModifyVnfInfoData) GetMetadataOk() (*map[string]interface{}, bool)`

GetMetadataOk returns a tuple with the Metadata field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMetadata

`func (o *ModifyVnfInfoData) SetMetadata(v map[string]interface{})`

SetMetadata sets Metadata field to given value.

### HasMetadata

`func (o *ModifyVnfInfoData) HasMetadata() bool`

HasMetadata returns a boolean if a field has been set.

### GetExtensions

`func (o *ModifyVnfInfoData) GetExtensions() map[string]interface{}`

GetExtensions returns the Extensions field if non-nil, zero value otherwise.

### GetExtensionsOk

`func (o *ModifyVnfInfoData) GetExtensionsOk() (*map[string]interface{}, bool)`

GetExtensionsOk returns a tuple with the Extensions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtensions

`func (o *ModifyVnfInfoData) SetExtensions(v map[string]interface{})`

SetExtensions sets Extensions field to given value.

### HasExtensions

`func (o *ModifyVnfInfoData) HasExtensions() bool`

HasExtensions returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


