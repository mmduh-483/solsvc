# ForwardingBehaviourInputParameters

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AlgortihmName** | Pointer to **string** | May be included if forwarding behaviour is equal to LB. Shall not be included otherwise. Permitted values: * ROUND_ROBIN * LEAST_CONNECTION * LEAST_TRAFFIC * LEAST_RESPONSE_TIME * CHAINED_FAILOVER * SOURCE_IP_HASH * SOURCE_MAC_HASH  | [optional] 
**AlgorithmWeights** | Pointer to **[]int32** | Percentage of messages sent to a CP instance. May be included if applicable to  the algorithm. See note 1 and note 2.  | [optional] 

## Methods

### NewForwardingBehaviourInputParameters

`func NewForwardingBehaviourInputParameters() *ForwardingBehaviourInputParameters`

NewForwardingBehaviourInputParameters instantiates a new ForwardingBehaviourInputParameters object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewForwardingBehaviourInputParametersWithDefaults

`func NewForwardingBehaviourInputParametersWithDefaults() *ForwardingBehaviourInputParameters`

NewForwardingBehaviourInputParametersWithDefaults instantiates a new ForwardingBehaviourInputParameters object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAlgortihmName

`func (o *ForwardingBehaviourInputParameters) GetAlgortihmName() string`

GetAlgortihmName returns the AlgortihmName field if non-nil, zero value otherwise.

### GetAlgortihmNameOk

`func (o *ForwardingBehaviourInputParameters) GetAlgortihmNameOk() (*string, bool)`

GetAlgortihmNameOk returns a tuple with the AlgortihmName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAlgortihmName

`func (o *ForwardingBehaviourInputParameters) SetAlgortihmName(v string)`

SetAlgortihmName sets AlgortihmName field to given value.

### HasAlgortihmName

`func (o *ForwardingBehaviourInputParameters) HasAlgortihmName() bool`

HasAlgortihmName returns a boolean if a field has been set.

### GetAlgorithmWeights

`func (o *ForwardingBehaviourInputParameters) GetAlgorithmWeights() []int32`

GetAlgorithmWeights returns the AlgorithmWeights field if non-nil, zero value otherwise.

### GetAlgorithmWeightsOk

`func (o *ForwardingBehaviourInputParameters) GetAlgorithmWeightsOk() (*[]int32, bool)`

GetAlgorithmWeightsOk returns a tuple with the AlgorithmWeights field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAlgorithmWeights

`func (o *ForwardingBehaviourInputParameters) SetAlgorithmWeights(v []int32)`

SetAlgorithmWeights sets AlgorithmWeights field to given value.

### HasAlgorithmWeights

`func (o *ForwardingBehaviourInputParameters) HasAlgorithmWeights() bool`

HasAlgorithmWeights returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


