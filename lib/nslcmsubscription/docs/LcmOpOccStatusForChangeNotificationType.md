# LcmOpOccStatusForChangeNotificationType

## Enum


* `VNF_INSTANTIATE` (value: `"VNF_INSTANTIATE"`)

* `VNF_SCALE` (value: `"VNF_SCALE"`)

* `VNF_SCALE_TO_LEVEL` (value: `"VNF_SCALE_TO_LEVEL"`)

* `VNF_CHANGE_FLAVOUR` (value: `"VNF_CHANGE_FLAVOUR"`)

* `VNF_TERMINATE` (value: `"VNF_TERMINATE"`)

* `VNF_HEAL` (value: `"VNF_HEAL"`)

* `VNF_OPERATE` (value: `"VNF_OPERATE"`)

* `VNF_CHANGE_EXT_CONN` (value: `"VNF_CHANGE_EXT_CONN"`)

* `VNF_MODIFY_INFO` (value: `"VNF_MODIFY_INFO"`)

* `VNF_CREATE_SNAPSHOT` (value: `"VNF_CREATE_SNAPSHOT"`)

* `VNF_REVERT_TO_SNAPSHOT` (value: `"VNF_REVERT_TO_SNAPSHOT"`)

* `VNF_CHANGE_VNFPKG` (value: `"VNF_CHANGE_VNFPKG"`)

* `NS_INSTANTIATE` (value: `"NS_INSTANTIATE"`)

* `NS_SCALE` (value: `"NS_SCALE"`)

* `NS_UPDATE` (value: `"NS_UPDATE"`)

* `NS_TERMINATE` (value: `"NS_TERMINATE"`)

* `NS_HEAL` (value: `"NS_HEAL"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


