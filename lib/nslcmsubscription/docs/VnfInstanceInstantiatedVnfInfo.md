# VnfInstanceInstantiatedVnfInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**FlavourId** | **string** | Identifier of the VNF Virtual Link Descriptor (VLD) in the VNFD.  | 
**VnfState** | [**VnfOperationalStateType**](VnfOperationalStateType.md) |  | 
**ScaleStatus** | Pointer to [**[]VnfScaleInfo**](VnfScaleInfo.md) | Scale status of the VNF, one entry per aspect. Represents for every scaling aspect how \&quot;big\&quot; the VNF has been scaled w.r.t. that aspect.  | [optional] 
**MaxScaleLevels** | Pointer to [**[]VnfScaleInfo**](VnfScaleInfo.md) | Maximum allowed scale levels of the VNF, one entry per aspect. This attribute shall be present if the VNF supports scaling.  | [optional] 
**ExtCpInfo** | [**[]VnfExtCpInfo**](VnfExtCpInfo.md) | Information about the external CPs exposed by the VNF instance. When trunking is enabled, the list of entries includes both, external CPs corresponding to parent ports of a trunk, and external CPs associated to sub-ports of a trunk.  | 
**VipCpInfo** | Pointer to [**[]VipCpInfo**](VipCpInfo.md) | VIP CPs that are part of the VNF instance. Shall be present when that particular VIP CP of the VNFC instance is associated to an external CP of the VNF instance. May be present otherwise.  | [optional] 
**ExtVirtualLinkInfo** | Pointer to [**[]ExtVirtualLinkInfo**](ExtVirtualLinkInfo.md) | Information about the external VLs the VNF instance is connected to.  | [optional] 
**ExtManagedVirtualLinkInfo** | Pointer to [**[]ExtManagedVirtualLinkInfo**](ExtManagedVirtualLinkInfo.md) | Information about the externally-managed internal VLs of the VNF instance.  See note 4 and note 5.  | [optional] 
**MonitoringParameters** | Pointer to [**[]VnfMonitoringParameter**](VnfMonitoringParameter.md) | Performance metrics tracked by the VNFM (e.g. for  auto-scaling purposes) as identified by the VNF  provider in the VNFD.  | [optional] 
**LocalizationLanguage** | Pointer to **string** | Information about localization language of the VNF (includes e.g. strings in the VNFD). The localization languages supported by a VNF can be declared in the VNFD, and localization language selection can take place at instantiation time. The value shall comply with the format defined in IETF RFC 5646.  | [optional] 
**VnfcResourceInfo** | Pointer to [**[]VnfcResourceInfo**](VnfcResourceInfo.md) | Information about the virtualised compute and storage resources used by the VNFCs of the VNF instance.  | [optional] 
**VnfVirtualLinkResourceInfo** | Pointer to [**[]VnfVirtualLinkResourceInfo**](VnfVirtualLinkResourceInfo.md) | Information about the virtualised network resources used by the VLs of the VNF instance. See note 5.  | [optional] 
**VirtualStorageResourceInfo** | Pointer to [**[]VirtualStorageResourceInfo**](VirtualStorageResourceInfo.md) | Information on the virtualised storage resource(s) used as storage for the VNF instance.  | [optional] 

## Methods

### NewVnfInstanceInstantiatedVnfInfo

`func NewVnfInstanceInstantiatedVnfInfo(flavourId string, vnfState VnfOperationalStateType, extCpInfo []VnfExtCpInfo, ) *VnfInstanceInstantiatedVnfInfo`

NewVnfInstanceInstantiatedVnfInfo instantiates a new VnfInstanceInstantiatedVnfInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVnfInstanceInstantiatedVnfInfoWithDefaults

`func NewVnfInstanceInstantiatedVnfInfoWithDefaults() *VnfInstanceInstantiatedVnfInfo`

NewVnfInstanceInstantiatedVnfInfoWithDefaults instantiates a new VnfInstanceInstantiatedVnfInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFlavourId

`func (o *VnfInstanceInstantiatedVnfInfo) GetFlavourId() string`

GetFlavourId returns the FlavourId field if non-nil, zero value otherwise.

### GetFlavourIdOk

`func (o *VnfInstanceInstantiatedVnfInfo) GetFlavourIdOk() (*string, bool)`

GetFlavourIdOk returns a tuple with the FlavourId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFlavourId

`func (o *VnfInstanceInstantiatedVnfInfo) SetFlavourId(v string)`

SetFlavourId sets FlavourId field to given value.


### GetVnfState

`func (o *VnfInstanceInstantiatedVnfInfo) GetVnfState() VnfOperationalStateType`

GetVnfState returns the VnfState field if non-nil, zero value otherwise.

### GetVnfStateOk

`func (o *VnfInstanceInstantiatedVnfInfo) GetVnfStateOk() (*VnfOperationalStateType, bool)`

GetVnfStateOk returns a tuple with the VnfState field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfState

`func (o *VnfInstanceInstantiatedVnfInfo) SetVnfState(v VnfOperationalStateType)`

SetVnfState sets VnfState field to given value.


### GetScaleStatus

`func (o *VnfInstanceInstantiatedVnfInfo) GetScaleStatus() []VnfScaleInfo`

GetScaleStatus returns the ScaleStatus field if non-nil, zero value otherwise.

### GetScaleStatusOk

`func (o *VnfInstanceInstantiatedVnfInfo) GetScaleStatusOk() (*[]VnfScaleInfo, bool)`

GetScaleStatusOk returns a tuple with the ScaleStatus field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScaleStatus

`func (o *VnfInstanceInstantiatedVnfInfo) SetScaleStatus(v []VnfScaleInfo)`

SetScaleStatus sets ScaleStatus field to given value.

### HasScaleStatus

`func (o *VnfInstanceInstantiatedVnfInfo) HasScaleStatus() bool`

HasScaleStatus returns a boolean if a field has been set.

### GetMaxScaleLevels

`func (o *VnfInstanceInstantiatedVnfInfo) GetMaxScaleLevels() []VnfScaleInfo`

GetMaxScaleLevels returns the MaxScaleLevels field if non-nil, zero value otherwise.

### GetMaxScaleLevelsOk

`func (o *VnfInstanceInstantiatedVnfInfo) GetMaxScaleLevelsOk() (*[]VnfScaleInfo, bool)`

GetMaxScaleLevelsOk returns a tuple with the MaxScaleLevels field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMaxScaleLevels

`func (o *VnfInstanceInstantiatedVnfInfo) SetMaxScaleLevels(v []VnfScaleInfo)`

SetMaxScaleLevels sets MaxScaleLevels field to given value.

### HasMaxScaleLevels

`func (o *VnfInstanceInstantiatedVnfInfo) HasMaxScaleLevels() bool`

HasMaxScaleLevels returns a boolean if a field has been set.

### GetExtCpInfo

`func (o *VnfInstanceInstantiatedVnfInfo) GetExtCpInfo() []VnfExtCpInfo`

GetExtCpInfo returns the ExtCpInfo field if non-nil, zero value otherwise.

### GetExtCpInfoOk

`func (o *VnfInstanceInstantiatedVnfInfo) GetExtCpInfoOk() (*[]VnfExtCpInfo, bool)`

GetExtCpInfoOk returns a tuple with the ExtCpInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtCpInfo

`func (o *VnfInstanceInstantiatedVnfInfo) SetExtCpInfo(v []VnfExtCpInfo)`

SetExtCpInfo sets ExtCpInfo field to given value.


### GetVipCpInfo

`func (o *VnfInstanceInstantiatedVnfInfo) GetVipCpInfo() []VipCpInfo`

GetVipCpInfo returns the VipCpInfo field if non-nil, zero value otherwise.

### GetVipCpInfoOk

`func (o *VnfInstanceInstantiatedVnfInfo) GetVipCpInfoOk() (*[]VipCpInfo, bool)`

GetVipCpInfoOk returns a tuple with the VipCpInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVipCpInfo

`func (o *VnfInstanceInstantiatedVnfInfo) SetVipCpInfo(v []VipCpInfo)`

SetVipCpInfo sets VipCpInfo field to given value.

### HasVipCpInfo

`func (o *VnfInstanceInstantiatedVnfInfo) HasVipCpInfo() bool`

HasVipCpInfo returns a boolean if a field has been set.

### GetExtVirtualLinkInfo

`func (o *VnfInstanceInstantiatedVnfInfo) GetExtVirtualLinkInfo() []ExtVirtualLinkInfo`

GetExtVirtualLinkInfo returns the ExtVirtualLinkInfo field if non-nil, zero value otherwise.

### GetExtVirtualLinkInfoOk

`func (o *VnfInstanceInstantiatedVnfInfo) GetExtVirtualLinkInfoOk() (*[]ExtVirtualLinkInfo, bool)`

GetExtVirtualLinkInfoOk returns a tuple with the ExtVirtualLinkInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtVirtualLinkInfo

`func (o *VnfInstanceInstantiatedVnfInfo) SetExtVirtualLinkInfo(v []ExtVirtualLinkInfo)`

SetExtVirtualLinkInfo sets ExtVirtualLinkInfo field to given value.

### HasExtVirtualLinkInfo

`func (o *VnfInstanceInstantiatedVnfInfo) HasExtVirtualLinkInfo() bool`

HasExtVirtualLinkInfo returns a boolean if a field has been set.

### GetExtManagedVirtualLinkInfo

`func (o *VnfInstanceInstantiatedVnfInfo) GetExtManagedVirtualLinkInfo() []ExtManagedVirtualLinkInfo`

GetExtManagedVirtualLinkInfo returns the ExtManagedVirtualLinkInfo field if non-nil, zero value otherwise.

### GetExtManagedVirtualLinkInfoOk

`func (o *VnfInstanceInstantiatedVnfInfo) GetExtManagedVirtualLinkInfoOk() (*[]ExtManagedVirtualLinkInfo, bool)`

GetExtManagedVirtualLinkInfoOk returns a tuple with the ExtManagedVirtualLinkInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtManagedVirtualLinkInfo

`func (o *VnfInstanceInstantiatedVnfInfo) SetExtManagedVirtualLinkInfo(v []ExtManagedVirtualLinkInfo)`

SetExtManagedVirtualLinkInfo sets ExtManagedVirtualLinkInfo field to given value.

### HasExtManagedVirtualLinkInfo

`func (o *VnfInstanceInstantiatedVnfInfo) HasExtManagedVirtualLinkInfo() bool`

HasExtManagedVirtualLinkInfo returns a boolean if a field has been set.

### GetMonitoringParameters

`func (o *VnfInstanceInstantiatedVnfInfo) GetMonitoringParameters() []VnfMonitoringParameter`

GetMonitoringParameters returns the MonitoringParameters field if non-nil, zero value otherwise.

### GetMonitoringParametersOk

`func (o *VnfInstanceInstantiatedVnfInfo) GetMonitoringParametersOk() (*[]VnfMonitoringParameter, bool)`

GetMonitoringParametersOk returns a tuple with the MonitoringParameters field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMonitoringParameters

`func (o *VnfInstanceInstantiatedVnfInfo) SetMonitoringParameters(v []VnfMonitoringParameter)`

SetMonitoringParameters sets MonitoringParameters field to given value.

### HasMonitoringParameters

`func (o *VnfInstanceInstantiatedVnfInfo) HasMonitoringParameters() bool`

HasMonitoringParameters returns a boolean if a field has been set.

### GetLocalizationLanguage

`func (o *VnfInstanceInstantiatedVnfInfo) GetLocalizationLanguage() string`

GetLocalizationLanguage returns the LocalizationLanguage field if non-nil, zero value otherwise.

### GetLocalizationLanguageOk

`func (o *VnfInstanceInstantiatedVnfInfo) GetLocalizationLanguageOk() (*string, bool)`

GetLocalizationLanguageOk returns a tuple with the LocalizationLanguage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocalizationLanguage

`func (o *VnfInstanceInstantiatedVnfInfo) SetLocalizationLanguage(v string)`

SetLocalizationLanguage sets LocalizationLanguage field to given value.

### HasLocalizationLanguage

`func (o *VnfInstanceInstantiatedVnfInfo) HasLocalizationLanguage() bool`

HasLocalizationLanguage returns a boolean if a field has been set.

### GetVnfcResourceInfo

`func (o *VnfInstanceInstantiatedVnfInfo) GetVnfcResourceInfo() []VnfcResourceInfo`

GetVnfcResourceInfo returns the VnfcResourceInfo field if non-nil, zero value otherwise.

### GetVnfcResourceInfoOk

`func (o *VnfInstanceInstantiatedVnfInfo) GetVnfcResourceInfoOk() (*[]VnfcResourceInfo, bool)`

GetVnfcResourceInfoOk returns a tuple with the VnfcResourceInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfcResourceInfo

`func (o *VnfInstanceInstantiatedVnfInfo) SetVnfcResourceInfo(v []VnfcResourceInfo)`

SetVnfcResourceInfo sets VnfcResourceInfo field to given value.

### HasVnfcResourceInfo

`func (o *VnfInstanceInstantiatedVnfInfo) HasVnfcResourceInfo() bool`

HasVnfcResourceInfo returns a boolean if a field has been set.

### GetVnfVirtualLinkResourceInfo

`func (o *VnfInstanceInstantiatedVnfInfo) GetVnfVirtualLinkResourceInfo() []VnfVirtualLinkResourceInfo`

GetVnfVirtualLinkResourceInfo returns the VnfVirtualLinkResourceInfo field if non-nil, zero value otherwise.

### GetVnfVirtualLinkResourceInfoOk

`func (o *VnfInstanceInstantiatedVnfInfo) GetVnfVirtualLinkResourceInfoOk() (*[]VnfVirtualLinkResourceInfo, bool)`

GetVnfVirtualLinkResourceInfoOk returns a tuple with the VnfVirtualLinkResourceInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfVirtualLinkResourceInfo

`func (o *VnfInstanceInstantiatedVnfInfo) SetVnfVirtualLinkResourceInfo(v []VnfVirtualLinkResourceInfo)`

SetVnfVirtualLinkResourceInfo sets VnfVirtualLinkResourceInfo field to given value.

### HasVnfVirtualLinkResourceInfo

`func (o *VnfInstanceInstantiatedVnfInfo) HasVnfVirtualLinkResourceInfo() bool`

HasVnfVirtualLinkResourceInfo returns a boolean if a field has been set.

### GetVirtualStorageResourceInfo

`func (o *VnfInstanceInstantiatedVnfInfo) GetVirtualStorageResourceInfo() []VirtualStorageResourceInfo`

GetVirtualStorageResourceInfo returns the VirtualStorageResourceInfo field if non-nil, zero value otherwise.

### GetVirtualStorageResourceInfoOk

`func (o *VnfInstanceInstantiatedVnfInfo) GetVirtualStorageResourceInfoOk() (*[]VirtualStorageResourceInfo, bool)`

GetVirtualStorageResourceInfoOk returns a tuple with the VirtualStorageResourceInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVirtualStorageResourceInfo

`func (o *VnfInstanceInstantiatedVnfInfo) SetVirtualStorageResourceInfo(v []VirtualStorageResourceInfo)`

SetVirtualStorageResourceInfo sets VirtualStorageResourceInfo field to given value.

### HasVirtualStorageResourceInfo

`func (o *VnfInstanceInstantiatedVnfInfo) HasVirtualStorageResourceInfo() bool`

HasVirtualStorageResourceInfo returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


