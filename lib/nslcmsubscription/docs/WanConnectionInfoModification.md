# WanConnectionInfoModification

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**WanConnectionInfoId** | **string** | An identifier with the intention of being globally unique.  | 
**MscsName** | Pointer to **string** | If present, this attribute signals modifications of the \&quot;mscsName\&quot; attribute in \&quot;MscsInfo\&quot; as defined in clause 6.5.3.82.  | [optional] 
**MscsDescription** | Pointer to **string** | If present, this attribute signals modifications of the \&quot;mscsDescription\&quot; attribute in \&quot;MscsInfo\&quot; as defined in clause 6.5.3.82.  | [optional] 
**MscsEndpoints** | Pointer to [**[]MscsEndpointInfo**](MscsEndpointInfo.md) | If present, this attribute signals modifications of certain entries in the \&quot;mscsEndpoints\&quot; attribute array in \&quot;MscsInfo\&quot;, as defined in clause 6.5.3.82.  | [optional] 
**RemoveMscsEndpointIds** | Pointer to **[]string** | If present, this attribute signals the deletion of certain entries in the \&quot;mscsEndpoints\&quot; attribute array in \&quot;MscsInfo\&quot;, as defined in clause 6.5.3.82.  | [optional] 
**ConnectivityServiceEndpoints** | Pointer to [**[]ConnectivityServiceEndpointInfo**](ConnectivityServiceEndpointInfo.md) | If present, this attribute signals modifications of certain entries in the \&quot;connectivityServiceEndpoints\&quot; attribute array in \&quot;WanConnectionProtocolInfo\&quot;, as defined in clause 6.5.3.91.  | [optional] 
**RemoveConnectivityServiceEndpoints** | Pointer to **[]string** | If present, this attribute signals the deletion of certain entries in the \&quot;connectivityServiceEndpoints\&quot; attribute array in \&quot;WanConnectionProtocolInfo\&quot;, as defined in clause 6.5.3.91.  | [optional] 

## Methods

### NewWanConnectionInfoModification

`func NewWanConnectionInfoModification(wanConnectionInfoId string, ) *WanConnectionInfoModification`

NewWanConnectionInfoModification instantiates a new WanConnectionInfoModification object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWanConnectionInfoModificationWithDefaults

`func NewWanConnectionInfoModificationWithDefaults() *WanConnectionInfoModification`

NewWanConnectionInfoModificationWithDefaults instantiates a new WanConnectionInfoModification object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetWanConnectionInfoId

`func (o *WanConnectionInfoModification) GetWanConnectionInfoId() string`

GetWanConnectionInfoId returns the WanConnectionInfoId field if non-nil, zero value otherwise.

### GetWanConnectionInfoIdOk

`func (o *WanConnectionInfoModification) GetWanConnectionInfoIdOk() (*string, bool)`

GetWanConnectionInfoIdOk returns a tuple with the WanConnectionInfoId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWanConnectionInfoId

`func (o *WanConnectionInfoModification) SetWanConnectionInfoId(v string)`

SetWanConnectionInfoId sets WanConnectionInfoId field to given value.


### GetMscsName

`func (o *WanConnectionInfoModification) GetMscsName() string`

GetMscsName returns the MscsName field if non-nil, zero value otherwise.

### GetMscsNameOk

`func (o *WanConnectionInfoModification) GetMscsNameOk() (*string, bool)`

GetMscsNameOk returns a tuple with the MscsName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMscsName

`func (o *WanConnectionInfoModification) SetMscsName(v string)`

SetMscsName sets MscsName field to given value.

### HasMscsName

`func (o *WanConnectionInfoModification) HasMscsName() bool`

HasMscsName returns a boolean if a field has been set.

### GetMscsDescription

`func (o *WanConnectionInfoModification) GetMscsDescription() string`

GetMscsDescription returns the MscsDescription field if non-nil, zero value otherwise.

### GetMscsDescriptionOk

`func (o *WanConnectionInfoModification) GetMscsDescriptionOk() (*string, bool)`

GetMscsDescriptionOk returns a tuple with the MscsDescription field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMscsDescription

`func (o *WanConnectionInfoModification) SetMscsDescription(v string)`

SetMscsDescription sets MscsDescription field to given value.

### HasMscsDescription

`func (o *WanConnectionInfoModification) HasMscsDescription() bool`

HasMscsDescription returns a boolean if a field has been set.

### GetMscsEndpoints

`func (o *WanConnectionInfoModification) GetMscsEndpoints() []MscsEndpointInfo`

GetMscsEndpoints returns the MscsEndpoints field if non-nil, zero value otherwise.

### GetMscsEndpointsOk

`func (o *WanConnectionInfoModification) GetMscsEndpointsOk() (*[]MscsEndpointInfo, bool)`

GetMscsEndpointsOk returns a tuple with the MscsEndpoints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMscsEndpoints

`func (o *WanConnectionInfoModification) SetMscsEndpoints(v []MscsEndpointInfo)`

SetMscsEndpoints sets MscsEndpoints field to given value.

### HasMscsEndpoints

`func (o *WanConnectionInfoModification) HasMscsEndpoints() bool`

HasMscsEndpoints returns a boolean if a field has been set.

### GetRemoveMscsEndpointIds

`func (o *WanConnectionInfoModification) GetRemoveMscsEndpointIds() []string`

GetRemoveMscsEndpointIds returns the RemoveMscsEndpointIds field if non-nil, zero value otherwise.

### GetRemoveMscsEndpointIdsOk

`func (o *WanConnectionInfoModification) GetRemoveMscsEndpointIdsOk() (*[]string, bool)`

GetRemoveMscsEndpointIdsOk returns a tuple with the RemoveMscsEndpointIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRemoveMscsEndpointIds

`func (o *WanConnectionInfoModification) SetRemoveMscsEndpointIds(v []string)`

SetRemoveMscsEndpointIds sets RemoveMscsEndpointIds field to given value.

### HasRemoveMscsEndpointIds

`func (o *WanConnectionInfoModification) HasRemoveMscsEndpointIds() bool`

HasRemoveMscsEndpointIds returns a boolean if a field has been set.

### GetConnectivityServiceEndpoints

`func (o *WanConnectionInfoModification) GetConnectivityServiceEndpoints() []ConnectivityServiceEndpointInfo`

GetConnectivityServiceEndpoints returns the ConnectivityServiceEndpoints field if non-nil, zero value otherwise.

### GetConnectivityServiceEndpointsOk

`func (o *WanConnectionInfoModification) GetConnectivityServiceEndpointsOk() (*[]ConnectivityServiceEndpointInfo, bool)`

GetConnectivityServiceEndpointsOk returns a tuple with the ConnectivityServiceEndpoints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConnectivityServiceEndpoints

`func (o *WanConnectionInfoModification) SetConnectivityServiceEndpoints(v []ConnectivityServiceEndpointInfo)`

SetConnectivityServiceEndpoints sets ConnectivityServiceEndpoints field to given value.

### HasConnectivityServiceEndpoints

`func (o *WanConnectionInfoModification) HasConnectivityServiceEndpoints() bool`

HasConnectivityServiceEndpoints returns a boolean if a field has been set.

### GetRemoveConnectivityServiceEndpoints

`func (o *WanConnectionInfoModification) GetRemoveConnectivityServiceEndpoints() []string`

GetRemoveConnectivityServiceEndpoints returns the RemoveConnectivityServiceEndpoints field if non-nil, zero value otherwise.

### GetRemoveConnectivityServiceEndpointsOk

`func (o *WanConnectionInfoModification) GetRemoveConnectivityServiceEndpointsOk() (*[]string, bool)`

GetRemoveConnectivityServiceEndpointsOk returns a tuple with the RemoveConnectivityServiceEndpoints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRemoveConnectivityServiceEndpoints

`func (o *WanConnectionInfoModification) SetRemoveConnectivityServiceEndpoints(v []string)`

SetRemoveConnectivityServiceEndpoints sets RemoveConnectivityServiceEndpoints field to given value.

### HasRemoveConnectivityServiceEndpoints

`func (o *WanConnectionInfoModification) HasRemoveConnectivityServiceEndpoints() bool`

HasRemoveConnectivityServiceEndpoints returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


