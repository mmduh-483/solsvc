# HealNsData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DegreeHealing** | **string** | Indicates the degree of healing. Possible values include: - HEAL_RESTORE: Complete the healing of the NS restoring the state of the NS before the failure occurred - HEAL_QOS: Complete the healing of the NS based on the newest QoS values - HEAL_RESET: Complete the healing of the NS resetting to the original instantiation state of the NS - PARTIAL_HEALING  | 
**ActionsHealing** | Pointer to **[]string** | Used to specify dedicated healing actions in a particular order (e.g. as a script). The actionsHealing attribute can be used to provide a specific script whose content and actions might only be possible to be derived during runtime. See note.  | [optional] 
**HealScript** | Pointer to **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | [optional] 
**AdditionalParamsforNs** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 

## Methods

### NewHealNsData

`func NewHealNsData(degreeHealing string, ) *HealNsData`

NewHealNsData instantiates a new HealNsData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewHealNsDataWithDefaults

`func NewHealNsDataWithDefaults() *HealNsData`

NewHealNsDataWithDefaults instantiates a new HealNsData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetDegreeHealing

`func (o *HealNsData) GetDegreeHealing() string`

GetDegreeHealing returns the DegreeHealing field if non-nil, zero value otherwise.

### GetDegreeHealingOk

`func (o *HealNsData) GetDegreeHealingOk() (*string, bool)`

GetDegreeHealingOk returns a tuple with the DegreeHealing field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDegreeHealing

`func (o *HealNsData) SetDegreeHealing(v string)`

SetDegreeHealing sets DegreeHealing field to given value.


### GetActionsHealing

`func (o *HealNsData) GetActionsHealing() []string`

GetActionsHealing returns the ActionsHealing field if non-nil, zero value otherwise.

### GetActionsHealingOk

`func (o *HealNsData) GetActionsHealingOk() (*[]string, bool)`

GetActionsHealingOk returns a tuple with the ActionsHealing field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetActionsHealing

`func (o *HealNsData) SetActionsHealing(v []string)`

SetActionsHealing sets ActionsHealing field to given value.

### HasActionsHealing

`func (o *HealNsData) HasActionsHealing() bool`

HasActionsHealing returns a boolean if a field has been set.

### GetHealScript

`func (o *HealNsData) GetHealScript() string`

GetHealScript returns the HealScript field if non-nil, zero value otherwise.

### GetHealScriptOk

`func (o *HealNsData) GetHealScriptOk() (*string, bool)`

GetHealScriptOk returns a tuple with the HealScript field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHealScript

`func (o *HealNsData) SetHealScript(v string)`

SetHealScript sets HealScript field to given value.

### HasHealScript

`func (o *HealNsData) HasHealScript() bool`

HasHealScript returns a boolean if a field has been set.

### GetAdditionalParamsforNs

`func (o *HealNsData) GetAdditionalParamsforNs() map[string]interface{}`

GetAdditionalParamsforNs returns the AdditionalParamsforNs field if non-nil, zero value otherwise.

### GetAdditionalParamsforNsOk

`func (o *HealNsData) GetAdditionalParamsforNsOk() (*map[string]interface{}, bool)`

GetAdditionalParamsforNsOk returns a tuple with the AdditionalParamsforNs field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdditionalParamsforNs

`func (o *HealNsData) SetAdditionalParamsforNs(v map[string]interface{})`

SetAdditionalParamsforNs sets AdditionalParamsforNs field to given value.

### HasAdditionalParamsforNs

`func (o *HealNsData) HasAdditionalParamsforNs() bool`

HasAdditionalParamsforNs returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


