# NsInstanceLinks

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Self** | [**Link**](Link.md) |  | 
**NestedNsInstances** | Pointer to [**[]Link**](Link.md) | Links to resources related to this notification.  | [optional] 
**VnfSnapshotInfos** | Pointer to [**[]Link**](Link.md) | Links to the VNF snapshots associated to VNF instances which are part of this NS instance.  | [optional] 
**Instantiate** | Pointer to [**Link**](Link.md) |  | [optional] 
**Terminate** | Pointer to [**Link**](Link.md) |  | [optional] 
**Update** | Pointer to [**Link**](Link.md) |  | [optional] 
**Scale** | Pointer to [**Link**](Link.md) |  | [optional] 
**Heal** | Pointer to [**Link**](Link.md) |  | [optional] 

## Methods

### NewNsInstanceLinks

`func NewNsInstanceLinks(self Link, ) *NsInstanceLinks`

NewNsInstanceLinks instantiates a new NsInstanceLinks object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNsInstanceLinksWithDefaults

`func NewNsInstanceLinksWithDefaults() *NsInstanceLinks`

NewNsInstanceLinksWithDefaults instantiates a new NsInstanceLinks object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSelf

`func (o *NsInstanceLinks) GetSelf() Link`

GetSelf returns the Self field if non-nil, zero value otherwise.

### GetSelfOk

`func (o *NsInstanceLinks) GetSelfOk() (*Link, bool)`

GetSelfOk returns a tuple with the Self field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSelf

`func (o *NsInstanceLinks) SetSelf(v Link)`

SetSelf sets Self field to given value.


### GetNestedNsInstances

`func (o *NsInstanceLinks) GetNestedNsInstances() []Link`

GetNestedNsInstances returns the NestedNsInstances field if non-nil, zero value otherwise.

### GetNestedNsInstancesOk

`func (o *NsInstanceLinks) GetNestedNsInstancesOk() (*[]Link, bool)`

GetNestedNsInstancesOk returns a tuple with the NestedNsInstances field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNestedNsInstances

`func (o *NsInstanceLinks) SetNestedNsInstances(v []Link)`

SetNestedNsInstances sets NestedNsInstances field to given value.

### HasNestedNsInstances

`func (o *NsInstanceLinks) HasNestedNsInstances() bool`

HasNestedNsInstances returns a boolean if a field has been set.

### GetVnfSnapshotInfos

`func (o *NsInstanceLinks) GetVnfSnapshotInfos() []Link`

GetVnfSnapshotInfos returns the VnfSnapshotInfos field if non-nil, zero value otherwise.

### GetVnfSnapshotInfosOk

`func (o *NsInstanceLinks) GetVnfSnapshotInfosOk() (*[]Link, bool)`

GetVnfSnapshotInfosOk returns a tuple with the VnfSnapshotInfos field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfSnapshotInfos

`func (o *NsInstanceLinks) SetVnfSnapshotInfos(v []Link)`

SetVnfSnapshotInfos sets VnfSnapshotInfos field to given value.

### HasVnfSnapshotInfos

`func (o *NsInstanceLinks) HasVnfSnapshotInfos() bool`

HasVnfSnapshotInfos returns a boolean if a field has been set.

### GetInstantiate

`func (o *NsInstanceLinks) GetInstantiate() Link`

GetInstantiate returns the Instantiate field if non-nil, zero value otherwise.

### GetInstantiateOk

`func (o *NsInstanceLinks) GetInstantiateOk() (*Link, bool)`

GetInstantiateOk returns a tuple with the Instantiate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInstantiate

`func (o *NsInstanceLinks) SetInstantiate(v Link)`

SetInstantiate sets Instantiate field to given value.

### HasInstantiate

`func (o *NsInstanceLinks) HasInstantiate() bool`

HasInstantiate returns a boolean if a field has been set.

### GetTerminate

`func (o *NsInstanceLinks) GetTerminate() Link`

GetTerminate returns the Terminate field if non-nil, zero value otherwise.

### GetTerminateOk

`func (o *NsInstanceLinks) GetTerminateOk() (*Link, bool)`

GetTerminateOk returns a tuple with the Terminate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTerminate

`func (o *NsInstanceLinks) SetTerminate(v Link)`

SetTerminate sets Terminate field to given value.

### HasTerminate

`func (o *NsInstanceLinks) HasTerminate() bool`

HasTerminate returns a boolean if a field has been set.

### GetUpdate

`func (o *NsInstanceLinks) GetUpdate() Link`

GetUpdate returns the Update field if non-nil, zero value otherwise.

### GetUpdateOk

`func (o *NsInstanceLinks) GetUpdateOk() (*Link, bool)`

GetUpdateOk returns a tuple with the Update field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdate

`func (o *NsInstanceLinks) SetUpdate(v Link)`

SetUpdate sets Update field to given value.

### HasUpdate

`func (o *NsInstanceLinks) HasUpdate() bool`

HasUpdate returns a boolean if a field has been set.

### GetScale

`func (o *NsInstanceLinks) GetScale() Link`

GetScale returns the Scale field if non-nil, zero value otherwise.

### GetScaleOk

`func (o *NsInstanceLinks) GetScaleOk() (*Link, bool)`

GetScaleOk returns a tuple with the Scale field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScale

`func (o *NsInstanceLinks) SetScale(v Link)`

SetScale sets Scale field to given value.

### HasScale

`func (o *NsInstanceLinks) HasScale() bool`

HasScale returns a boolean if a field has been set.

### GetHeal

`func (o *NsInstanceLinks) GetHeal() Link`

GetHeal returns the Heal field if non-nil, zero value otherwise.

### GetHealOk

`func (o *NsInstanceLinks) GetHealOk() (*Link, bool)`

GetHealOk returns a tuple with the Heal field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHeal

`func (o *NsInstanceLinks) SetHeal(v Link)`

SetHeal sets Heal field to given value.

### HasHeal

`func (o *NsInstanceLinks) HasHeal() bool`

HasHeal returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


