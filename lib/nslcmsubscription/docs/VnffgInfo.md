# VnffgInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | An identifier with the intention of being globally unique.  | 
**VnffgdId** | **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | 
**VnfInstanceId** | **[]string** | Identifier(s) of the constituent VNF instance(s) of this VNFFG instance.  | 
**PnfdInfoId** | Pointer to **[]string** | Identifier(s) of the constituent PNF instance(s) of this VNFFG instance.  | [optional] 
**NsVirtualLinkInfoId** | Pointer to **[]string** | Identifier(s) of the constituent VL instance(s) of this VNFFG instance.  | [optional] 
**NsCpHandle** | Pointer to [**NsCpHandle**](NsCpHandle.md) |  | [optional] 

## Methods

### NewVnffgInfo

`func NewVnffgInfo(id string, vnffgdId string, vnfInstanceId []string, ) *VnffgInfo`

NewVnffgInfo instantiates a new VnffgInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVnffgInfoWithDefaults

`func NewVnffgInfoWithDefaults() *VnffgInfo`

NewVnffgInfoWithDefaults instantiates a new VnffgInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *VnffgInfo) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *VnffgInfo) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *VnffgInfo) SetId(v string)`

SetId sets Id field to given value.


### GetVnffgdId

`func (o *VnffgInfo) GetVnffgdId() string`

GetVnffgdId returns the VnffgdId field if non-nil, zero value otherwise.

### GetVnffgdIdOk

`func (o *VnffgInfo) GetVnffgdIdOk() (*string, bool)`

GetVnffgdIdOk returns a tuple with the VnffgdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnffgdId

`func (o *VnffgInfo) SetVnffgdId(v string)`

SetVnffgdId sets VnffgdId field to given value.


### GetVnfInstanceId

`func (o *VnffgInfo) GetVnfInstanceId() []string`

GetVnfInstanceId returns the VnfInstanceId field if non-nil, zero value otherwise.

### GetVnfInstanceIdOk

`func (o *VnffgInfo) GetVnfInstanceIdOk() (*[]string, bool)`

GetVnfInstanceIdOk returns a tuple with the VnfInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceId

`func (o *VnffgInfo) SetVnfInstanceId(v []string)`

SetVnfInstanceId sets VnfInstanceId field to given value.


### GetPnfdInfoId

`func (o *VnffgInfo) GetPnfdInfoId() []string`

GetPnfdInfoId returns the PnfdInfoId field if non-nil, zero value otherwise.

### GetPnfdInfoIdOk

`func (o *VnffgInfo) GetPnfdInfoIdOk() (*[]string, bool)`

GetPnfdInfoIdOk returns a tuple with the PnfdInfoId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPnfdInfoId

`func (o *VnffgInfo) SetPnfdInfoId(v []string)`

SetPnfdInfoId sets PnfdInfoId field to given value.

### HasPnfdInfoId

`func (o *VnffgInfo) HasPnfdInfoId() bool`

HasPnfdInfoId returns a boolean if a field has been set.

### GetNsVirtualLinkInfoId

`func (o *VnffgInfo) GetNsVirtualLinkInfoId() []string`

GetNsVirtualLinkInfoId returns the NsVirtualLinkInfoId field if non-nil, zero value otherwise.

### GetNsVirtualLinkInfoIdOk

`func (o *VnffgInfo) GetNsVirtualLinkInfoIdOk() (*[]string, bool)`

GetNsVirtualLinkInfoIdOk returns a tuple with the NsVirtualLinkInfoId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsVirtualLinkInfoId

`func (o *VnffgInfo) SetNsVirtualLinkInfoId(v []string)`

SetNsVirtualLinkInfoId sets NsVirtualLinkInfoId field to given value.

### HasNsVirtualLinkInfoId

`func (o *VnffgInfo) HasNsVirtualLinkInfoId() bool`

HasNsVirtualLinkInfoId returns a boolean if a field has been set.

### GetNsCpHandle

`func (o *VnffgInfo) GetNsCpHandle() NsCpHandle`

GetNsCpHandle returns the NsCpHandle field if non-nil, zero value otherwise.

### GetNsCpHandleOk

`func (o *VnffgInfo) GetNsCpHandleOk() (*NsCpHandle, bool)`

GetNsCpHandleOk returns a tuple with the NsCpHandle field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsCpHandle

`func (o *VnffgInfo) SetNsCpHandle(v NsCpHandle)`

SetNsCpHandle sets NsCpHandle field to given value.

### HasNsCpHandle

`func (o *VnffgInfo) HasNsCpHandle() bool`

HasNsCpHandle returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


