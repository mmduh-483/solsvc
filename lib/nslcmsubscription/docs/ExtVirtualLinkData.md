# ExtVirtualLinkData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ExtVirtualLinkId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**VimId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**ResourceProviderId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**ResourceId** | **string** | An identifier maintained by the VIM or other resource provider. It is expected to be unique within the VIM instance. Representation: string of variable length.  | 
**ExtCps** | [**[]VnfExtCpData**](VnfExtCpData.md) | External CPs of the VNF to be connected to this external VL.  | 
**ExtLinkPorts** | Pointer to [**[]ExtLinkPortData**](ExtLinkPortData.md) | Externally provided link ports to be used to connect external connection points to this external VL unless the extCp exposes a VIP CP and a link port is not needed for it based on the conditions defined below. See note.  | [optional] 

## Methods

### NewExtVirtualLinkData

`func NewExtVirtualLinkData(resourceId string, extCps []VnfExtCpData, ) *ExtVirtualLinkData`

NewExtVirtualLinkData instantiates a new ExtVirtualLinkData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewExtVirtualLinkDataWithDefaults

`func NewExtVirtualLinkDataWithDefaults() *ExtVirtualLinkData`

NewExtVirtualLinkDataWithDefaults instantiates a new ExtVirtualLinkData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetExtVirtualLinkId

`func (o *ExtVirtualLinkData) GetExtVirtualLinkId() string`

GetExtVirtualLinkId returns the ExtVirtualLinkId field if non-nil, zero value otherwise.

### GetExtVirtualLinkIdOk

`func (o *ExtVirtualLinkData) GetExtVirtualLinkIdOk() (*string, bool)`

GetExtVirtualLinkIdOk returns a tuple with the ExtVirtualLinkId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtVirtualLinkId

`func (o *ExtVirtualLinkData) SetExtVirtualLinkId(v string)`

SetExtVirtualLinkId sets ExtVirtualLinkId field to given value.

### HasExtVirtualLinkId

`func (o *ExtVirtualLinkData) HasExtVirtualLinkId() bool`

HasExtVirtualLinkId returns a boolean if a field has been set.

### GetVimId

`func (o *ExtVirtualLinkData) GetVimId() string`

GetVimId returns the VimId field if non-nil, zero value otherwise.

### GetVimIdOk

`func (o *ExtVirtualLinkData) GetVimIdOk() (*string, bool)`

GetVimIdOk returns a tuple with the VimId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVimId

`func (o *ExtVirtualLinkData) SetVimId(v string)`

SetVimId sets VimId field to given value.

### HasVimId

`func (o *ExtVirtualLinkData) HasVimId() bool`

HasVimId returns a boolean if a field has been set.

### GetResourceProviderId

`func (o *ExtVirtualLinkData) GetResourceProviderId() string`

GetResourceProviderId returns the ResourceProviderId field if non-nil, zero value otherwise.

### GetResourceProviderIdOk

`func (o *ExtVirtualLinkData) GetResourceProviderIdOk() (*string, bool)`

GetResourceProviderIdOk returns a tuple with the ResourceProviderId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResourceProviderId

`func (o *ExtVirtualLinkData) SetResourceProviderId(v string)`

SetResourceProviderId sets ResourceProviderId field to given value.

### HasResourceProviderId

`func (o *ExtVirtualLinkData) HasResourceProviderId() bool`

HasResourceProviderId returns a boolean if a field has been set.

### GetResourceId

`func (o *ExtVirtualLinkData) GetResourceId() string`

GetResourceId returns the ResourceId field if non-nil, zero value otherwise.

### GetResourceIdOk

`func (o *ExtVirtualLinkData) GetResourceIdOk() (*string, bool)`

GetResourceIdOk returns a tuple with the ResourceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResourceId

`func (o *ExtVirtualLinkData) SetResourceId(v string)`

SetResourceId sets ResourceId field to given value.


### GetExtCps

`func (o *ExtVirtualLinkData) GetExtCps() []VnfExtCpData`

GetExtCps returns the ExtCps field if non-nil, zero value otherwise.

### GetExtCpsOk

`func (o *ExtVirtualLinkData) GetExtCpsOk() (*[]VnfExtCpData, bool)`

GetExtCpsOk returns a tuple with the ExtCps field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtCps

`func (o *ExtVirtualLinkData) SetExtCps(v []VnfExtCpData)`

SetExtCps sets ExtCps field to given value.


### GetExtLinkPorts

`func (o *ExtVirtualLinkData) GetExtLinkPorts() []ExtLinkPortData`

GetExtLinkPorts returns the ExtLinkPorts field if non-nil, zero value otherwise.

### GetExtLinkPortsOk

`func (o *ExtVirtualLinkData) GetExtLinkPortsOk() (*[]ExtLinkPortData, bool)`

GetExtLinkPortsOk returns a tuple with the ExtLinkPorts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtLinkPorts

`func (o *ExtVirtualLinkData) SetExtLinkPorts(v []ExtLinkPortData)`

SetExtLinkPorts sets ExtLinkPorts field to given value.

### HasExtLinkPorts

`func (o *ExtVirtualLinkData) HasExtLinkPorts() bool`

HasExtLinkPorts returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


