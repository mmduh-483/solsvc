# WanConnectionProtocolData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MscsInfo** | Pointer to [**MscsInfo**](MscsInfo.md) |  | [optional] 
**ConnectivityServiceEndpointConfigDatas** | Pointer to [**[]ConnectivityServiceEndpointInfo**](ConnectivityServiceEndpointInfo.md) | Configuration data for the network resources in the NFVI-PoP. See note.  | [optional] 
**MscsConfigData** | Pointer to [**MscsConfigData**](MscsConfigData.md) |  | [optional] 

## Methods

### NewWanConnectionProtocolData

`func NewWanConnectionProtocolData() *WanConnectionProtocolData`

NewWanConnectionProtocolData instantiates a new WanConnectionProtocolData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWanConnectionProtocolDataWithDefaults

`func NewWanConnectionProtocolDataWithDefaults() *WanConnectionProtocolData`

NewWanConnectionProtocolDataWithDefaults instantiates a new WanConnectionProtocolData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMscsInfo

`func (o *WanConnectionProtocolData) GetMscsInfo() MscsInfo`

GetMscsInfo returns the MscsInfo field if non-nil, zero value otherwise.

### GetMscsInfoOk

`func (o *WanConnectionProtocolData) GetMscsInfoOk() (*MscsInfo, bool)`

GetMscsInfoOk returns a tuple with the MscsInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMscsInfo

`func (o *WanConnectionProtocolData) SetMscsInfo(v MscsInfo)`

SetMscsInfo sets MscsInfo field to given value.

### HasMscsInfo

`func (o *WanConnectionProtocolData) HasMscsInfo() bool`

HasMscsInfo returns a boolean if a field has been set.

### GetConnectivityServiceEndpointConfigDatas

`func (o *WanConnectionProtocolData) GetConnectivityServiceEndpointConfigDatas() []ConnectivityServiceEndpointInfo`

GetConnectivityServiceEndpointConfigDatas returns the ConnectivityServiceEndpointConfigDatas field if non-nil, zero value otherwise.

### GetConnectivityServiceEndpointConfigDatasOk

`func (o *WanConnectionProtocolData) GetConnectivityServiceEndpointConfigDatasOk() (*[]ConnectivityServiceEndpointInfo, bool)`

GetConnectivityServiceEndpointConfigDatasOk returns a tuple with the ConnectivityServiceEndpointConfigDatas field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConnectivityServiceEndpointConfigDatas

`func (o *WanConnectionProtocolData) SetConnectivityServiceEndpointConfigDatas(v []ConnectivityServiceEndpointInfo)`

SetConnectivityServiceEndpointConfigDatas sets ConnectivityServiceEndpointConfigDatas field to given value.

### HasConnectivityServiceEndpointConfigDatas

`func (o *WanConnectionProtocolData) HasConnectivityServiceEndpointConfigDatas() bool`

HasConnectivityServiceEndpointConfigDatas returns a boolean if a field has been set.

### GetMscsConfigData

`func (o *WanConnectionProtocolData) GetMscsConfigData() MscsConfigData`

GetMscsConfigData returns the MscsConfigData field if non-nil, zero value otherwise.

### GetMscsConfigDataOk

`func (o *WanConnectionProtocolData) GetMscsConfigDataOk() (*MscsConfigData, bool)`

GetMscsConfigDataOk returns a tuple with the MscsConfigData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMscsConfigData

`func (o *WanConnectionProtocolData) SetMscsConfigData(v MscsConfigData)`

SetMscsConfigData sets MscsConfigData field to given value.

### HasMscsConfigData

`func (o *WanConnectionProtocolData) HasMscsConfigData() bool`

HasMscsConfigData returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


