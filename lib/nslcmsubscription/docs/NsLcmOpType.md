# NsLcmOpType

## Enum


* `INSTANTIATE` (value: `"INSTANTIATE"`)

* `SCALE` (value: `"SCALE"`)

* `UPDATE` (value: `"UPDATE"`)

* `TERMINATE` (value: `"TERMINATE"`)

* `HEAL` (value: `"HEAL"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


