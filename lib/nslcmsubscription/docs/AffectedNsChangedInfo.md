# AffectedNsChangedInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**WanConnectionInfoModifications** | Pointer to [**[]WanConnectionInfoModification**](WanConnectionInfoModification.md) | Information about the modified WAN related connectivity information, if applicable.  | [optional] 

## Methods

### NewAffectedNsChangedInfo

`func NewAffectedNsChangedInfo() *AffectedNsChangedInfo`

NewAffectedNsChangedInfo instantiates a new AffectedNsChangedInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAffectedNsChangedInfoWithDefaults

`func NewAffectedNsChangedInfoWithDefaults() *AffectedNsChangedInfo`

NewAffectedNsChangedInfoWithDefaults instantiates a new AffectedNsChangedInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetWanConnectionInfoModifications

`func (o *AffectedNsChangedInfo) GetWanConnectionInfoModifications() []WanConnectionInfoModification`

GetWanConnectionInfoModifications returns the WanConnectionInfoModifications field if non-nil, zero value otherwise.

### GetWanConnectionInfoModificationsOk

`func (o *AffectedNsChangedInfo) GetWanConnectionInfoModificationsOk() (*[]WanConnectionInfoModification, bool)`

GetWanConnectionInfoModificationsOk returns a tuple with the WanConnectionInfoModifications field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWanConnectionInfoModifications

`func (o *AffectedNsChangedInfo) SetWanConnectionInfoModifications(v []WanConnectionInfoModification)`

SetWanConnectionInfoModifications sets WanConnectionInfoModifications field to given value.

### HasWanConnectionInfoModifications

`func (o *AffectedNsChangedInfo) HasWanConnectionInfoModifications() bool`

HasWanConnectionInfoModifications returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


