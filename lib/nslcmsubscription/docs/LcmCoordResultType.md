# LcmCoordResultType

## Enum


* `CONTINUE` (value: `"CONTINUE"`)

* `ABORT` (value: `"ABORT"`)

* `CACELLED` (value: `"CACELLED"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


