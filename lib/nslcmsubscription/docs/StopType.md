# StopType

## Enum


* `FORCEFUL` (value: `"FORCEFUL"`)

* `GRACEFUL` (value: `"GRACEFUL"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


