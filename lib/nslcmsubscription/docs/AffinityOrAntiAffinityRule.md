# AffinityOrAntiAffinityRule

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VnfdId** | Pointer to **[]string** | Reference to a VNFD. When the VNFD which is not used to instantiate VNF, it presents all VNF instances of this type as the subjects of the affinity or anti-affinity rule. The VNF instance which the VNFD presents is not necessary as a part of the NS to be instantiated.  | [optional] 
**VnfProfileId** | Pointer to **[]string** | Reference to a vnfProfile defined in the NSD. At least one VnfProfile which is used to instantiate VNF for the NS to be instantiated as the subject of the affinity or anti-affinity rule shall be present. When the VnfProfile which is not used to instantiate VNF, it presents all VNF instances of this type as the subjects of the affinity or anti-affinity rule. The VNF instance which the VnfProfile presents is not necessary as a part of the NS to be instantiated.  | [optional] 
**VnfInstanceId** | Pointer to **[]string** | Reference to the existing VNF instance as the subject of the affinity or anti-affinity rule. The existing VNF instance is not necessary as a part of the NS to be instantiated.  | [optional] 
**AffinityOrAntiAffiinty** | **string** | The type of the constraint. Permitted values: AFFINITY ANTI_AFFINITY.  | 
**Scope** | **string** | Specifies the scope of the rule where the placement constraint applies. Permitted values: NFVI_POP ZONE ZONE_GROUP NFVI_NODE.  | 

## Methods

### NewAffinityOrAntiAffinityRule

`func NewAffinityOrAntiAffinityRule(affinityOrAntiAffiinty string, scope string, ) *AffinityOrAntiAffinityRule`

NewAffinityOrAntiAffinityRule instantiates a new AffinityOrAntiAffinityRule object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAffinityOrAntiAffinityRuleWithDefaults

`func NewAffinityOrAntiAffinityRuleWithDefaults() *AffinityOrAntiAffinityRule`

NewAffinityOrAntiAffinityRuleWithDefaults instantiates a new AffinityOrAntiAffinityRule object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVnfdId

`func (o *AffinityOrAntiAffinityRule) GetVnfdId() []string`

GetVnfdId returns the VnfdId field if non-nil, zero value otherwise.

### GetVnfdIdOk

`func (o *AffinityOrAntiAffinityRule) GetVnfdIdOk() (*[]string, bool)`

GetVnfdIdOk returns a tuple with the VnfdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfdId

`func (o *AffinityOrAntiAffinityRule) SetVnfdId(v []string)`

SetVnfdId sets VnfdId field to given value.

### HasVnfdId

`func (o *AffinityOrAntiAffinityRule) HasVnfdId() bool`

HasVnfdId returns a boolean if a field has been set.

### GetVnfProfileId

`func (o *AffinityOrAntiAffinityRule) GetVnfProfileId() []string`

GetVnfProfileId returns the VnfProfileId field if non-nil, zero value otherwise.

### GetVnfProfileIdOk

`func (o *AffinityOrAntiAffinityRule) GetVnfProfileIdOk() (*[]string, bool)`

GetVnfProfileIdOk returns a tuple with the VnfProfileId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfProfileId

`func (o *AffinityOrAntiAffinityRule) SetVnfProfileId(v []string)`

SetVnfProfileId sets VnfProfileId field to given value.

### HasVnfProfileId

`func (o *AffinityOrAntiAffinityRule) HasVnfProfileId() bool`

HasVnfProfileId returns a boolean if a field has been set.

### GetVnfInstanceId

`func (o *AffinityOrAntiAffinityRule) GetVnfInstanceId() []string`

GetVnfInstanceId returns the VnfInstanceId field if non-nil, zero value otherwise.

### GetVnfInstanceIdOk

`func (o *AffinityOrAntiAffinityRule) GetVnfInstanceIdOk() (*[]string, bool)`

GetVnfInstanceIdOk returns a tuple with the VnfInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceId

`func (o *AffinityOrAntiAffinityRule) SetVnfInstanceId(v []string)`

SetVnfInstanceId sets VnfInstanceId field to given value.

### HasVnfInstanceId

`func (o *AffinityOrAntiAffinityRule) HasVnfInstanceId() bool`

HasVnfInstanceId returns a boolean if a field has been set.

### GetAffinityOrAntiAffiinty

`func (o *AffinityOrAntiAffinityRule) GetAffinityOrAntiAffiinty() string`

GetAffinityOrAntiAffiinty returns the AffinityOrAntiAffiinty field if non-nil, zero value otherwise.

### GetAffinityOrAntiAffiintyOk

`func (o *AffinityOrAntiAffinityRule) GetAffinityOrAntiAffiintyOk() (*string, bool)`

GetAffinityOrAntiAffiintyOk returns a tuple with the AffinityOrAntiAffiinty field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAffinityOrAntiAffiinty

`func (o *AffinityOrAntiAffinityRule) SetAffinityOrAntiAffiinty(v string)`

SetAffinityOrAntiAffiinty sets AffinityOrAntiAffiinty field to given value.


### GetScope

`func (o *AffinityOrAntiAffinityRule) GetScope() string`

GetScope returns the Scope field if non-nil, zero value otherwise.

### GetScopeOk

`func (o *AffinityOrAntiAffinityRule) GetScopeOk() (*string, bool)`

GetScopeOk returns a tuple with the Scope field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScope

`func (o *AffinityOrAntiAffinityRule) SetScope(v string)`

SetScope sets Scope field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


