# VnfSnapshotInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | An identifier with the intention of being globally unique.  | 
**VnfSnapshotPkgId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**VnfSnapshot** | Pointer to [**VnfSnapshot**](VnfSnapshot.md) |  | [optional] 
**Links** | Pointer to [**LccnSubscriptionLinks**](LccnSubscriptionLinks.md) |  | [optional] 

## Methods

### NewVnfSnapshotInfo

`func NewVnfSnapshotInfo(id string, ) *VnfSnapshotInfo`

NewVnfSnapshotInfo instantiates a new VnfSnapshotInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVnfSnapshotInfoWithDefaults

`func NewVnfSnapshotInfoWithDefaults() *VnfSnapshotInfo`

NewVnfSnapshotInfoWithDefaults instantiates a new VnfSnapshotInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *VnfSnapshotInfo) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *VnfSnapshotInfo) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *VnfSnapshotInfo) SetId(v string)`

SetId sets Id field to given value.


### GetVnfSnapshotPkgId

`func (o *VnfSnapshotInfo) GetVnfSnapshotPkgId() string`

GetVnfSnapshotPkgId returns the VnfSnapshotPkgId field if non-nil, zero value otherwise.

### GetVnfSnapshotPkgIdOk

`func (o *VnfSnapshotInfo) GetVnfSnapshotPkgIdOk() (*string, bool)`

GetVnfSnapshotPkgIdOk returns a tuple with the VnfSnapshotPkgId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfSnapshotPkgId

`func (o *VnfSnapshotInfo) SetVnfSnapshotPkgId(v string)`

SetVnfSnapshotPkgId sets VnfSnapshotPkgId field to given value.

### HasVnfSnapshotPkgId

`func (o *VnfSnapshotInfo) HasVnfSnapshotPkgId() bool`

HasVnfSnapshotPkgId returns a boolean if a field has been set.

### GetVnfSnapshot

`func (o *VnfSnapshotInfo) GetVnfSnapshot() VnfSnapshot`

GetVnfSnapshot returns the VnfSnapshot field if non-nil, zero value otherwise.

### GetVnfSnapshotOk

`func (o *VnfSnapshotInfo) GetVnfSnapshotOk() (*VnfSnapshot, bool)`

GetVnfSnapshotOk returns a tuple with the VnfSnapshot field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfSnapshot

`func (o *VnfSnapshotInfo) SetVnfSnapshot(v VnfSnapshot)`

SetVnfSnapshot sets VnfSnapshot field to given value.

### HasVnfSnapshot

`func (o *VnfSnapshotInfo) HasVnfSnapshot() bool`

HasVnfSnapshot returns a boolean if a field has been set.

### GetLinks

`func (o *VnfSnapshotInfo) GetLinks() LccnSubscriptionLinks`

GetLinks returns the Links field if non-nil, zero value otherwise.

### GetLinksOk

`func (o *VnfSnapshotInfo) GetLinksOk() (*LccnSubscriptionLinks, bool)`

GetLinksOk returns a tuple with the Links field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLinks

`func (o *VnfSnapshotInfo) SetLinks(v LccnSubscriptionLinks)`

SetLinks sets Links field to given value.

### HasLinks

`func (o *VnfSnapshotInfo) HasLinks() bool`

HasLinks returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


