# ScaleVnfData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VnfInstanceId** | **string** | An identifier with the intention of being globally unique.  | 
**ScaleVnfType** | **string** | Type of the scale VNF operation requested. Allowed values are: - SCALE_OUT - SCALE_IN - SCALE_TO_INSTANTIATION_LEVEL - SCALE_TO_SCALE_LEVEL(S) The set of types actually supported depends on the capabilities of the VNF being managed. See note 1.  | 
**ScaleToLevelData** | Pointer to [**ScaleToLevelData**](ScaleToLevelData.md) |  | [optional] 
**ScaleByStepData** | Pointer to [**ScaleByStepData**](ScaleByStepData.md) |  | [optional] 

## Methods

### NewScaleVnfData

`func NewScaleVnfData(vnfInstanceId string, scaleVnfType string, ) *ScaleVnfData`

NewScaleVnfData instantiates a new ScaleVnfData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewScaleVnfDataWithDefaults

`func NewScaleVnfDataWithDefaults() *ScaleVnfData`

NewScaleVnfDataWithDefaults instantiates a new ScaleVnfData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVnfInstanceId

`func (o *ScaleVnfData) GetVnfInstanceId() string`

GetVnfInstanceId returns the VnfInstanceId field if non-nil, zero value otherwise.

### GetVnfInstanceIdOk

`func (o *ScaleVnfData) GetVnfInstanceIdOk() (*string, bool)`

GetVnfInstanceIdOk returns a tuple with the VnfInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceId

`func (o *ScaleVnfData) SetVnfInstanceId(v string)`

SetVnfInstanceId sets VnfInstanceId field to given value.


### GetScaleVnfType

`func (o *ScaleVnfData) GetScaleVnfType() string`

GetScaleVnfType returns the ScaleVnfType field if non-nil, zero value otherwise.

### GetScaleVnfTypeOk

`func (o *ScaleVnfData) GetScaleVnfTypeOk() (*string, bool)`

GetScaleVnfTypeOk returns a tuple with the ScaleVnfType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScaleVnfType

`func (o *ScaleVnfData) SetScaleVnfType(v string)`

SetScaleVnfType sets ScaleVnfType field to given value.


### GetScaleToLevelData

`func (o *ScaleVnfData) GetScaleToLevelData() ScaleToLevelData`

GetScaleToLevelData returns the ScaleToLevelData field if non-nil, zero value otherwise.

### GetScaleToLevelDataOk

`func (o *ScaleVnfData) GetScaleToLevelDataOk() (*ScaleToLevelData, bool)`

GetScaleToLevelDataOk returns a tuple with the ScaleToLevelData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScaleToLevelData

`func (o *ScaleVnfData) SetScaleToLevelData(v ScaleToLevelData)`

SetScaleToLevelData sets ScaleToLevelData field to given value.

### HasScaleToLevelData

`func (o *ScaleVnfData) HasScaleToLevelData() bool`

HasScaleToLevelData returns a boolean if a field has been set.

### GetScaleByStepData

`func (o *ScaleVnfData) GetScaleByStepData() ScaleByStepData`

GetScaleByStepData returns the ScaleByStepData field if non-nil, zero value otherwise.

### GetScaleByStepDataOk

`func (o *ScaleVnfData) GetScaleByStepDataOk() (*ScaleByStepData, bool)`

GetScaleByStepDataOk returns a tuple with the ScaleByStepData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScaleByStepData

`func (o *ScaleVnfData) SetScaleByStepData(v ScaleByStepData)`

SetScaleByStepData sets ScaleByStepData field to given value.

### HasScaleByStepData

`func (o *ScaleVnfData) HasScaleByStepData() bool`

HasScaleByStepData returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


