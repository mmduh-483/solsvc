# MscsConfigDataSiteAccessProtectionSchemesInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LocationConstraints** | [**LocationConstraints**](LocationConstraints.md) |  | 
**ProtectionScheme** | **string** | Defines the protection scheme. Permitted values: - UNPROTECTED - ONE_TO_ONE - ONE_PLUS_ONE - ONE_TO_N  | 

## Methods

### NewMscsConfigDataSiteAccessProtectionSchemesInner

`func NewMscsConfigDataSiteAccessProtectionSchemesInner(locationConstraints LocationConstraints, protectionScheme string, ) *MscsConfigDataSiteAccessProtectionSchemesInner`

NewMscsConfigDataSiteAccessProtectionSchemesInner instantiates a new MscsConfigDataSiteAccessProtectionSchemesInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMscsConfigDataSiteAccessProtectionSchemesInnerWithDefaults

`func NewMscsConfigDataSiteAccessProtectionSchemesInnerWithDefaults() *MscsConfigDataSiteAccessProtectionSchemesInner`

NewMscsConfigDataSiteAccessProtectionSchemesInnerWithDefaults instantiates a new MscsConfigDataSiteAccessProtectionSchemesInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLocationConstraints

`func (o *MscsConfigDataSiteAccessProtectionSchemesInner) GetLocationConstraints() LocationConstraints`

GetLocationConstraints returns the LocationConstraints field if non-nil, zero value otherwise.

### GetLocationConstraintsOk

`func (o *MscsConfigDataSiteAccessProtectionSchemesInner) GetLocationConstraintsOk() (*LocationConstraints, bool)`

GetLocationConstraintsOk returns a tuple with the LocationConstraints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocationConstraints

`func (o *MscsConfigDataSiteAccessProtectionSchemesInner) SetLocationConstraints(v LocationConstraints)`

SetLocationConstraints sets LocationConstraints field to given value.


### GetProtectionScheme

`func (o *MscsConfigDataSiteAccessProtectionSchemesInner) GetProtectionScheme() string`

GetProtectionScheme returns the ProtectionScheme field if non-nil, zero value otherwise.

### GetProtectionSchemeOk

`func (o *MscsConfigDataSiteAccessProtectionSchemesInner) GetProtectionSchemeOk() (*string, bool)`

GetProtectionSchemeOk returns a tuple with the ProtectionScheme field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProtectionScheme

`func (o *MscsConfigDataSiteAccessProtectionSchemesInner) SetProtectionScheme(v string)`

SetProtectionScheme sets ProtectionScheme field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


