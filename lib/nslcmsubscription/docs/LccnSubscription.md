# LccnSubscription

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | An identifier with the intention of being globally unique.  | 
**Filter** | Pointer to [**LifecycleChangeNotificationsFilter**](LifecycleChangeNotificationsFilter.md) |  | [optional] 
**CallbackUri** | **string** | String formatted according to IETF RFC 3986.  | 
**Verbosity** | Pointer to [**LcmOpOccNotificationVerbosityType**](LcmOpOccNotificationVerbosityType.md) |  | [optional] 
**Links** | [**LccnSubscriptionLinks**](LccnSubscriptionLinks.md) |  | 

## Methods

### NewLccnSubscription

`func NewLccnSubscription(id string, callbackUri string, links LccnSubscriptionLinks, ) *LccnSubscription`

NewLccnSubscription instantiates a new LccnSubscription object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewLccnSubscriptionWithDefaults

`func NewLccnSubscriptionWithDefaults() *LccnSubscription`

NewLccnSubscriptionWithDefaults instantiates a new LccnSubscription object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *LccnSubscription) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *LccnSubscription) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *LccnSubscription) SetId(v string)`

SetId sets Id field to given value.


### GetFilter

`func (o *LccnSubscription) GetFilter() LifecycleChangeNotificationsFilter`

GetFilter returns the Filter field if non-nil, zero value otherwise.

### GetFilterOk

`func (o *LccnSubscription) GetFilterOk() (*LifecycleChangeNotificationsFilter, bool)`

GetFilterOk returns a tuple with the Filter field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFilter

`func (o *LccnSubscription) SetFilter(v LifecycleChangeNotificationsFilter)`

SetFilter sets Filter field to given value.

### HasFilter

`func (o *LccnSubscription) HasFilter() bool`

HasFilter returns a boolean if a field has been set.

### GetCallbackUri

`func (o *LccnSubscription) GetCallbackUri() string`

GetCallbackUri returns the CallbackUri field if non-nil, zero value otherwise.

### GetCallbackUriOk

`func (o *LccnSubscription) GetCallbackUriOk() (*string, bool)`

GetCallbackUriOk returns a tuple with the CallbackUri field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCallbackUri

`func (o *LccnSubscription) SetCallbackUri(v string)`

SetCallbackUri sets CallbackUri field to given value.


### GetVerbosity

`func (o *LccnSubscription) GetVerbosity() LcmOpOccNotificationVerbosityType`

GetVerbosity returns the Verbosity field if non-nil, zero value otherwise.

### GetVerbosityOk

`func (o *LccnSubscription) GetVerbosityOk() (*LcmOpOccNotificationVerbosityType, bool)`

GetVerbosityOk returns a tuple with the Verbosity field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVerbosity

`func (o *LccnSubscription) SetVerbosity(v LcmOpOccNotificationVerbosityType)`

SetVerbosity sets Verbosity field to given value.

### HasVerbosity

`func (o *LccnSubscription) HasVerbosity() bool`

HasVerbosity returns a boolean if a field has been set.

### GetLinks

`func (o *LccnSubscription) GetLinks() LccnSubscriptionLinks`

GetLinks returns the Links field if non-nil, zero value otherwise.

### GetLinksOk

`func (o *LccnSubscription) GetLinksOk() (*LccnSubscriptionLinks, bool)`

GetLinksOk returns a tuple with the Links field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLinks

`func (o *LccnSubscription) SetLinks(v LccnSubscriptionLinks)`

SetLinks sets Links field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


