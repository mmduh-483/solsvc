# NfpRule

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EtherDestinationAddress** | Pointer to **string** | A MAC address. Representation: string that consists of groups of two hexadecimal digits, separated by hyphens or colons.  | [optional] 
**EtherSourceAddress** | Pointer to **string** | A MAC address. Representation: string that consists of groups of two hexadecimal digits, separated by hyphens or colons.  | [optional] 
**EtherType** | Pointer to **string** | Indicates the protocol carried over the Ethernet layer. Permitted values: - IPV4 - IPV6 See note.  | [optional] 
**VlanTag** | Pointer to **[]string** | Indicates a VLAN identifier in an IEEE 802.1Q-2018 tag Multiple tags can be included for QinQ stacking. See note.  | [optional] 
**Protocol** | Pointer to **string** | Indicates the L4 protocol, For IPv4 this corresponds to the field called \&quot;Protocol\&quot; to identify the next level protocol. For IPv6 this corresponds to the field is called the \&quot;Next Header\&quot; field. Permitted values: Any keyword defined in the IANA protocol registry, e.g.: TCP UDP ICMP See note.  | [optional] 
**Dscp** | Pointer to **string** | For IPv4 a string of \&quot;0\&quot; and \&quot;1\&quot; digits that corresponds to the 6-bit Differentiated Services Code Point (DSCP) field of the IP header. For IPv6 a string of \&quot;0\&quot; and \&quot;1\&quot; digits that corresponds to the 6 differentiated services bits of the traffic class header field. See note.  | [optional] 
**SourcePortRange** | Pointer to [**PortRange**](PortRange.md) |  | [optional] 
**DestinationPortRange** | Pointer to [**PortRange**](PortRange.md) |  | [optional] 
**SourceIpAddressPrefix** | Pointer to **string** | An IPV4 or IPV6 address range in CIDR format. For IPV4 address range, refer to IETF RFC 4632 [12]. For IPV6 address range, refer to IETF RFC 4291.  | [optional] 
**DestinationIpAddressPrefix** | Pointer to **string** | An IPV4 or IPV6 address range in CIDR format. For IPV4 address range, refer to IETF RFC 4632 [12]. For IPV6 address range, refer to IETF RFC 4291.  | [optional] 
**ExtendedCriteria** | Pointer to [**[]Mask**](Mask.md) | Indicates values of specific bits in a frame. See note.  | [optional] 

## Methods

### NewNfpRule

`func NewNfpRule() *NfpRule`

NewNfpRule instantiates a new NfpRule object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNfpRuleWithDefaults

`func NewNfpRuleWithDefaults() *NfpRule`

NewNfpRuleWithDefaults instantiates a new NfpRule object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetEtherDestinationAddress

`func (o *NfpRule) GetEtherDestinationAddress() string`

GetEtherDestinationAddress returns the EtherDestinationAddress field if non-nil, zero value otherwise.

### GetEtherDestinationAddressOk

`func (o *NfpRule) GetEtherDestinationAddressOk() (*string, bool)`

GetEtherDestinationAddressOk returns a tuple with the EtherDestinationAddress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEtherDestinationAddress

`func (o *NfpRule) SetEtherDestinationAddress(v string)`

SetEtherDestinationAddress sets EtherDestinationAddress field to given value.

### HasEtherDestinationAddress

`func (o *NfpRule) HasEtherDestinationAddress() bool`

HasEtherDestinationAddress returns a boolean if a field has been set.

### GetEtherSourceAddress

`func (o *NfpRule) GetEtherSourceAddress() string`

GetEtherSourceAddress returns the EtherSourceAddress field if non-nil, zero value otherwise.

### GetEtherSourceAddressOk

`func (o *NfpRule) GetEtherSourceAddressOk() (*string, bool)`

GetEtherSourceAddressOk returns a tuple with the EtherSourceAddress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEtherSourceAddress

`func (o *NfpRule) SetEtherSourceAddress(v string)`

SetEtherSourceAddress sets EtherSourceAddress field to given value.

### HasEtherSourceAddress

`func (o *NfpRule) HasEtherSourceAddress() bool`

HasEtherSourceAddress returns a boolean if a field has been set.

### GetEtherType

`func (o *NfpRule) GetEtherType() string`

GetEtherType returns the EtherType field if non-nil, zero value otherwise.

### GetEtherTypeOk

`func (o *NfpRule) GetEtherTypeOk() (*string, bool)`

GetEtherTypeOk returns a tuple with the EtherType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEtherType

`func (o *NfpRule) SetEtherType(v string)`

SetEtherType sets EtherType field to given value.

### HasEtherType

`func (o *NfpRule) HasEtherType() bool`

HasEtherType returns a boolean if a field has been set.

### GetVlanTag

`func (o *NfpRule) GetVlanTag() []string`

GetVlanTag returns the VlanTag field if non-nil, zero value otherwise.

### GetVlanTagOk

`func (o *NfpRule) GetVlanTagOk() (*[]string, bool)`

GetVlanTagOk returns a tuple with the VlanTag field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVlanTag

`func (o *NfpRule) SetVlanTag(v []string)`

SetVlanTag sets VlanTag field to given value.

### HasVlanTag

`func (o *NfpRule) HasVlanTag() bool`

HasVlanTag returns a boolean if a field has been set.

### GetProtocol

`func (o *NfpRule) GetProtocol() string`

GetProtocol returns the Protocol field if non-nil, zero value otherwise.

### GetProtocolOk

`func (o *NfpRule) GetProtocolOk() (*string, bool)`

GetProtocolOk returns a tuple with the Protocol field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProtocol

`func (o *NfpRule) SetProtocol(v string)`

SetProtocol sets Protocol field to given value.

### HasProtocol

`func (o *NfpRule) HasProtocol() bool`

HasProtocol returns a boolean if a field has been set.

### GetDscp

`func (o *NfpRule) GetDscp() string`

GetDscp returns the Dscp field if non-nil, zero value otherwise.

### GetDscpOk

`func (o *NfpRule) GetDscpOk() (*string, bool)`

GetDscpOk returns a tuple with the Dscp field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDscp

`func (o *NfpRule) SetDscp(v string)`

SetDscp sets Dscp field to given value.

### HasDscp

`func (o *NfpRule) HasDscp() bool`

HasDscp returns a boolean if a field has been set.

### GetSourcePortRange

`func (o *NfpRule) GetSourcePortRange() PortRange`

GetSourcePortRange returns the SourcePortRange field if non-nil, zero value otherwise.

### GetSourcePortRangeOk

`func (o *NfpRule) GetSourcePortRangeOk() (*PortRange, bool)`

GetSourcePortRangeOk returns a tuple with the SourcePortRange field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSourcePortRange

`func (o *NfpRule) SetSourcePortRange(v PortRange)`

SetSourcePortRange sets SourcePortRange field to given value.

### HasSourcePortRange

`func (o *NfpRule) HasSourcePortRange() bool`

HasSourcePortRange returns a boolean if a field has been set.

### GetDestinationPortRange

`func (o *NfpRule) GetDestinationPortRange() PortRange`

GetDestinationPortRange returns the DestinationPortRange field if non-nil, zero value otherwise.

### GetDestinationPortRangeOk

`func (o *NfpRule) GetDestinationPortRangeOk() (*PortRange, bool)`

GetDestinationPortRangeOk returns a tuple with the DestinationPortRange field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDestinationPortRange

`func (o *NfpRule) SetDestinationPortRange(v PortRange)`

SetDestinationPortRange sets DestinationPortRange field to given value.

### HasDestinationPortRange

`func (o *NfpRule) HasDestinationPortRange() bool`

HasDestinationPortRange returns a boolean if a field has been set.

### GetSourceIpAddressPrefix

`func (o *NfpRule) GetSourceIpAddressPrefix() string`

GetSourceIpAddressPrefix returns the SourceIpAddressPrefix field if non-nil, zero value otherwise.

### GetSourceIpAddressPrefixOk

`func (o *NfpRule) GetSourceIpAddressPrefixOk() (*string, bool)`

GetSourceIpAddressPrefixOk returns a tuple with the SourceIpAddressPrefix field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSourceIpAddressPrefix

`func (o *NfpRule) SetSourceIpAddressPrefix(v string)`

SetSourceIpAddressPrefix sets SourceIpAddressPrefix field to given value.

### HasSourceIpAddressPrefix

`func (o *NfpRule) HasSourceIpAddressPrefix() bool`

HasSourceIpAddressPrefix returns a boolean if a field has been set.

### GetDestinationIpAddressPrefix

`func (o *NfpRule) GetDestinationIpAddressPrefix() string`

GetDestinationIpAddressPrefix returns the DestinationIpAddressPrefix field if non-nil, zero value otherwise.

### GetDestinationIpAddressPrefixOk

`func (o *NfpRule) GetDestinationIpAddressPrefixOk() (*string, bool)`

GetDestinationIpAddressPrefixOk returns a tuple with the DestinationIpAddressPrefix field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDestinationIpAddressPrefix

`func (o *NfpRule) SetDestinationIpAddressPrefix(v string)`

SetDestinationIpAddressPrefix sets DestinationIpAddressPrefix field to given value.

### HasDestinationIpAddressPrefix

`func (o *NfpRule) HasDestinationIpAddressPrefix() bool`

HasDestinationIpAddressPrefix returns a boolean if a field has been set.

### GetExtendedCriteria

`func (o *NfpRule) GetExtendedCriteria() []Mask`

GetExtendedCriteria returns the ExtendedCriteria field if non-nil, zero value otherwise.

### GetExtendedCriteriaOk

`func (o *NfpRule) GetExtendedCriteriaOk() (*[]Mask, bool)`

GetExtendedCriteriaOk returns a tuple with the ExtendedCriteria field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtendedCriteria

`func (o *NfpRule) SetExtendedCriteria(v []Mask)`

SetExtendedCriteria sets ExtendedCriteria field to given value.

### HasExtendedCriteria

`func (o *NfpRule) HasExtendedCriteria() bool`

HasExtendedCriteria returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


