# CpProtocolInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LayerProtocol** | **string** | The identifier of layer(s) and protocol(s) associated to the network address information. Permitted values:   - IP_OVER_ETHERNET  See note.  | 
**IpOverEthernet** | [**IpOverEthernetAddressInfo**](IpOverEthernetAddressInfo.md) |  | 

## Methods

### NewCpProtocolInfo

`func NewCpProtocolInfo(layerProtocol string, ipOverEthernet IpOverEthernetAddressInfo, ) *CpProtocolInfo`

NewCpProtocolInfo instantiates a new CpProtocolInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCpProtocolInfoWithDefaults

`func NewCpProtocolInfoWithDefaults() *CpProtocolInfo`

NewCpProtocolInfoWithDefaults instantiates a new CpProtocolInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLayerProtocol

`func (o *CpProtocolInfo) GetLayerProtocol() string`

GetLayerProtocol returns the LayerProtocol field if non-nil, zero value otherwise.

### GetLayerProtocolOk

`func (o *CpProtocolInfo) GetLayerProtocolOk() (*string, bool)`

GetLayerProtocolOk returns a tuple with the LayerProtocol field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLayerProtocol

`func (o *CpProtocolInfo) SetLayerProtocol(v string)`

SetLayerProtocol sets LayerProtocol field to given value.


### GetIpOverEthernet

`func (o *CpProtocolInfo) GetIpOverEthernet() IpOverEthernetAddressInfo`

GetIpOverEthernet returns the IpOverEthernet field if non-nil, zero value otherwise.

### GetIpOverEthernetOk

`func (o *CpProtocolInfo) GetIpOverEthernetOk() (*IpOverEthernetAddressInfo, bool)`

GetIpOverEthernetOk returns a tuple with the IpOverEthernet field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIpOverEthernet

`func (o *CpProtocolInfo) SetIpOverEthernet(v IpOverEthernetAddressInfo)`

SetIpOverEthernet sets IpOverEthernet field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


