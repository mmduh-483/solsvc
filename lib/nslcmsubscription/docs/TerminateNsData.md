# TerminateNsData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AdditionalParamsforNs** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 

## Methods

### NewTerminateNsData

`func NewTerminateNsData() *TerminateNsData`

NewTerminateNsData instantiates a new TerminateNsData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTerminateNsDataWithDefaults

`func NewTerminateNsDataWithDefaults() *TerminateNsData`

NewTerminateNsDataWithDefaults instantiates a new TerminateNsData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAdditionalParamsforNs

`func (o *TerminateNsData) GetAdditionalParamsforNs() map[string]interface{}`

GetAdditionalParamsforNs returns the AdditionalParamsforNs field if non-nil, zero value otherwise.

### GetAdditionalParamsforNsOk

`func (o *TerminateNsData) GetAdditionalParamsforNsOk() (*map[string]interface{}, bool)`

GetAdditionalParamsforNsOk returns a tuple with the AdditionalParamsforNs field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdditionalParamsforNs

`func (o *TerminateNsData) SetAdditionalParamsforNs(v map[string]interface{})`

SetAdditionalParamsforNs sets AdditionalParamsforNs field to given value.

### HasAdditionalParamsforNs

`func (o *TerminateNsData) HasAdditionalParamsforNs() bool`

HasAdditionalParamsforNs returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


