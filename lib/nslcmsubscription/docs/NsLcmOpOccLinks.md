# NsLcmOpOccLinks

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Self** | [**Link**](Link.md) |  | 
**NsInstance** | [**Link**](Link.md) |  | 
**Cancel** | Pointer to [**Link**](Link.md) |  | [optional] 
**Retry** | Pointer to [**Link**](Link.md) |  | [optional] 
**Rollback** | Pointer to [**Link**](Link.md) |  | [optional] 
**Continue** | Pointer to [**Link**](Link.md) |  | [optional] 
**Fail** | Pointer to [**Link**](Link.md) |  | [optional] 

## Methods

### NewNsLcmOpOccLinks

`func NewNsLcmOpOccLinks(self Link, nsInstance Link, ) *NsLcmOpOccLinks`

NewNsLcmOpOccLinks instantiates a new NsLcmOpOccLinks object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNsLcmOpOccLinksWithDefaults

`func NewNsLcmOpOccLinksWithDefaults() *NsLcmOpOccLinks`

NewNsLcmOpOccLinksWithDefaults instantiates a new NsLcmOpOccLinks object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSelf

`func (o *NsLcmOpOccLinks) GetSelf() Link`

GetSelf returns the Self field if non-nil, zero value otherwise.

### GetSelfOk

`func (o *NsLcmOpOccLinks) GetSelfOk() (*Link, bool)`

GetSelfOk returns a tuple with the Self field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSelf

`func (o *NsLcmOpOccLinks) SetSelf(v Link)`

SetSelf sets Self field to given value.


### GetNsInstance

`func (o *NsLcmOpOccLinks) GetNsInstance() Link`

GetNsInstance returns the NsInstance field if non-nil, zero value otherwise.

### GetNsInstanceOk

`func (o *NsLcmOpOccLinks) GetNsInstanceOk() (*Link, bool)`

GetNsInstanceOk returns a tuple with the NsInstance field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsInstance

`func (o *NsLcmOpOccLinks) SetNsInstance(v Link)`

SetNsInstance sets NsInstance field to given value.


### GetCancel

`func (o *NsLcmOpOccLinks) GetCancel() Link`

GetCancel returns the Cancel field if non-nil, zero value otherwise.

### GetCancelOk

`func (o *NsLcmOpOccLinks) GetCancelOk() (*Link, bool)`

GetCancelOk returns a tuple with the Cancel field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCancel

`func (o *NsLcmOpOccLinks) SetCancel(v Link)`

SetCancel sets Cancel field to given value.

### HasCancel

`func (o *NsLcmOpOccLinks) HasCancel() bool`

HasCancel returns a boolean if a field has been set.

### GetRetry

`func (o *NsLcmOpOccLinks) GetRetry() Link`

GetRetry returns the Retry field if non-nil, zero value otherwise.

### GetRetryOk

`func (o *NsLcmOpOccLinks) GetRetryOk() (*Link, bool)`

GetRetryOk returns a tuple with the Retry field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRetry

`func (o *NsLcmOpOccLinks) SetRetry(v Link)`

SetRetry sets Retry field to given value.

### HasRetry

`func (o *NsLcmOpOccLinks) HasRetry() bool`

HasRetry returns a boolean if a field has been set.

### GetRollback

`func (o *NsLcmOpOccLinks) GetRollback() Link`

GetRollback returns the Rollback field if non-nil, zero value otherwise.

### GetRollbackOk

`func (o *NsLcmOpOccLinks) GetRollbackOk() (*Link, bool)`

GetRollbackOk returns a tuple with the Rollback field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRollback

`func (o *NsLcmOpOccLinks) SetRollback(v Link)`

SetRollback sets Rollback field to given value.

### HasRollback

`func (o *NsLcmOpOccLinks) HasRollback() bool`

HasRollback returns a boolean if a field has been set.

### GetContinue

`func (o *NsLcmOpOccLinks) GetContinue() Link`

GetContinue returns the Continue field if non-nil, zero value otherwise.

### GetContinueOk

`func (o *NsLcmOpOccLinks) GetContinueOk() (*Link, bool)`

GetContinueOk returns a tuple with the Continue field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContinue

`func (o *NsLcmOpOccLinks) SetContinue(v Link)`

SetContinue sets Continue field to given value.

### HasContinue

`func (o *NsLcmOpOccLinks) HasContinue() bool`

HasContinue returns a boolean if a field has been set.

### GetFail

`func (o *NsLcmOpOccLinks) GetFail() Link`

GetFail returns the Fail field if non-nil, zero value otherwise.

### GetFailOk

`func (o *NsLcmOpOccLinks) GetFailOk() (*Link, bool)`

GetFailOk returns a tuple with the Fail field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFail

`func (o *NsLcmOpOccLinks) SetFail(v Link)`

SetFail sets Fail field to given value.

### HasFail

`func (o *NsLcmOpOccLinks) HasFail() bool`

HasFail returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


