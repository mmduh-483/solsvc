# VnfcResourceInfoVnfcCpInfoInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | An identifier that is unique for the respective type within a VNF instance, but may not be globally unique.  | 
**CpdId** | **string** | Identifier of the VNF Virtual Link Descriptor (VLD) in the VNFD.  | 
**VnfExtCpId** | Pointer to **string** | An identifier that is unique for the respective type within a VNF instance, but may not be globally unique.  | [optional] 
**CpProtocolInfo** | Pointer to [**[]CpProtocolInfo**](CpProtocolInfo.md) | Network protocol information for this CP. May be omitted if the VNFC CP is exposed as an external CP. See note 3.  | [optional] 
**ParentCpId** | Pointer to **string** | An identifier that is unique for the respective type within a VNF instance, but may not be globally unique.  | [optional] 
**VnfLinkPortId** | Pointer to **string** | An identifier that is unique for the respective type within a VNF instance, but may not be globally unique.  | [optional] 
**Metadata** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 

## Methods

### NewVnfcResourceInfoVnfcCpInfoInner

`func NewVnfcResourceInfoVnfcCpInfoInner(id string, cpdId string, ) *VnfcResourceInfoVnfcCpInfoInner`

NewVnfcResourceInfoVnfcCpInfoInner instantiates a new VnfcResourceInfoVnfcCpInfoInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVnfcResourceInfoVnfcCpInfoInnerWithDefaults

`func NewVnfcResourceInfoVnfcCpInfoInnerWithDefaults() *VnfcResourceInfoVnfcCpInfoInner`

NewVnfcResourceInfoVnfcCpInfoInnerWithDefaults instantiates a new VnfcResourceInfoVnfcCpInfoInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *VnfcResourceInfoVnfcCpInfoInner) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *VnfcResourceInfoVnfcCpInfoInner) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *VnfcResourceInfoVnfcCpInfoInner) SetId(v string)`

SetId sets Id field to given value.


### GetCpdId

`func (o *VnfcResourceInfoVnfcCpInfoInner) GetCpdId() string`

GetCpdId returns the CpdId field if non-nil, zero value otherwise.

### GetCpdIdOk

`func (o *VnfcResourceInfoVnfcCpInfoInner) GetCpdIdOk() (*string, bool)`

GetCpdIdOk returns a tuple with the CpdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCpdId

`func (o *VnfcResourceInfoVnfcCpInfoInner) SetCpdId(v string)`

SetCpdId sets CpdId field to given value.


### GetVnfExtCpId

`func (o *VnfcResourceInfoVnfcCpInfoInner) GetVnfExtCpId() string`

GetVnfExtCpId returns the VnfExtCpId field if non-nil, zero value otherwise.

### GetVnfExtCpIdOk

`func (o *VnfcResourceInfoVnfcCpInfoInner) GetVnfExtCpIdOk() (*string, bool)`

GetVnfExtCpIdOk returns a tuple with the VnfExtCpId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfExtCpId

`func (o *VnfcResourceInfoVnfcCpInfoInner) SetVnfExtCpId(v string)`

SetVnfExtCpId sets VnfExtCpId field to given value.

### HasVnfExtCpId

`func (o *VnfcResourceInfoVnfcCpInfoInner) HasVnfExtCpId() bool`

HasVnfExtCpId returns a boolean if a field has been set.

### GetCpProtocolInfo

`func (o *VnfcResourceInfoVnfcCpInfoInner) GetCpProtocolInfo() []CpProtocolInfo`

GetCpProtocolInfo returns the CpProtocolInfo field if non-nil, zero value otherwise.

### GetCpProtocolInfoOk

`func (o *VnfcResourceInfoVnfcCpInfoInner) GetCpProtocolInfoOk() (*[]CpProtocolInfo, bool)`

GetCpProtocolInfoOk returns a tuple with the CpProtocolInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCpProtocolInfo

`func (o *VnfcResourceInfoVnfcCpInfoInner) SetCpProtocolInfo(v []CpProtocolInfo)`

SetCpProtocolInfo sets CpProtocolInfo field to given value.

### HasCpProtocolInfo

`func (o *VnfcResourceInfoVnfcCpInfoInner) HasCpProtocolInfo() bool`

HasCpProtocolInfo returns a boolean if a field has been set.

### GetParentCpId

`func (o *VnfcResourceInfoVnfcCpInfoInner) GetParentCpId() string`

GetParentCpId returns the ParentCpId field if non-nil, zero value otherwise.

### GetParentCpIdOk

`func (o *VnfcResourceInfoVnfcCpInfoInner) GetParentCpIdOk() (*string, bool)`

GetParentCpIdOk returns a tuple with the ParentCpId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetParentCpId

`func (o *VnfcResourceInfoVnfcCpInfoInner) SetParentCpId(v string)`

SetParentCpId sets ParentCpId field to given value.

### HasParentCpId

`func (o *VnfcResourceInfoVnfcCpInfoInner) HasParentCpId() bool`

HasParentCpId returns a boolean if a field has been set.

### GetVnfLinkPortId

`func (o *VnfcResourceInfoVnfcCpInfoInner) GetVnfLinkPortId() string`

GetVnfLinkPortId returns the VnfLinkPortId field if non-nil, zero value otherwise.

### GetVnfLinkPortIdOk

`func (o *VnfcResourceInfoVnfcCpInfoInner) GetVnfLinkPortIdOk() (*string, bool)`

GetVnfLinkPortIdOk returns a tuple with the VnfLinkPortId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfLinkPortId

`func (o *VnfcResourceInfoVnfcCpInfoInner) SetVnfLinkPortId(v string)`

SetVnfLinkPortId sets VnfLinkPortId field to given value.

### HasVnfLinkPortId

`func (o *VnfcResourceInfoVnfcCpInfoInner) HasVnfLinkPortId() bool`

HasVnfLinkPortId returns a boolean if a field has been set.

### GetMetadata

`func (o *VnfcResourceInfoVnfcCpInfoInner) GetMetadata() map[string]interface{}`

GetMetadata returns the Metadata field if non-nil, zero value otherwise.

### GetMetadataOk

`func (o *VnfcResourceInfoVnfcCpInfoInner) GetMetadataOk() (*map[string]interface{}, bool)`

GetMetadataOk returns a tuple with the Metadata field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMetadata

`func (o *VnfcResourceInfoVnfcCpInfoInner) SetMetadata(v map[string]interface{})`

SetMetadata sets Metadata field to given value.

### HasMetadata

`func (o *VnfcResourceInfoVnfcCpInfoInner) HasMetadata() bool`

HasMetadata returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


