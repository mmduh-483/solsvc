# AffectedVnf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VnfInstanceId** | **string** | An identifier with the intention of being globally unique.  | 
**VnfdId** | **string** | An identifier with the intention of being globally unique.  | 
**VnfProfileId** | **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | 
**VnfName** | **string** | Name of the VNF Instance.  | 
**ChangeType** | **string** | Signals the type of change Permitted values: - ADD - REMOVE - INSTANTIATE - TERMINATE - SCALE - CHANGE_FLAVOUR - HEAL - OPERATE - MODIFY_INFORMATION - CHANGE_EXTERNAL_VNF_CONNECTIVITY - CHANGE_VNFPKG  | 
**ChangeResult** | **string** | Signals the result of change identified by the \&quot;changeType\&quot; attribute. Permitted values: - COMPLETED - ROLLED_BACK - FAILED  | 
**ChangedInfo** | Pointer to [**AnyOfAnyTypeAnyTypeAnyType**](anyOf&lt;AnyType,AnyType,AnyType&gt;.md) | Information about the changed VNF instance information, including VNF configurable properties,if applicable. When the \&quot;changedInfo\&quot; attribute is present,  either the \&quot;changedVnfInfo\&quot; attribute or the \&quot;changedExtConnectivity\&quot; attribute or both shall be present.  | [optional] 

## Methods

### NewAffectedVnf

`func NewAffectedVnf(vnfInstanceId string, vnfdId string, vnfProfileId string, vnfName string, changeType string, changeResult string, ) *AffectedVnf`

NewAffectedVnf instantiates a new AffectedVnf object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAffectedVnfWithDefaults

`func NewAffectedVnfWithDefaults() *AffectedVnf`

NewAffectedVnfWithDefaults instantiates a new AffectedVnf object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVnfInstanceId

`func (o *AffectedVnf) GetVnfInstanceId() string`

GetVnfInstanceId returns the VnfInstanceId field if non-nil, zero value otherwise.

### GetVnfInstanceIdOk

`func (o *AffectedVnf) GetVnfInstanceIdOk() (*string, bool)`

GetVnfInstanceIdOk returns a tuple with the VnfInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceId

`func (o *AffectedVnf) SetVnfInstanceId(v string)`

SetVnfInstanceId sets VnfInstanceId field to given value.


### GetVnfdId

`func (o *AffectedVnf) GetVnfdId() string`

GetVnfdId returns the VnfdId field if non-nil, zero value otherwise.

### GetVnfdIdOk

`func (o *AffectedVnf) GetVnfdIdOk() (*string, bool)`

GetVnfdIdOk returns a tuple with the VnfdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfdId

`func (o *AffectedVnf) SetVnfdId(v string)`

SetVnfdId sets VnfdId field to given value.


### GetVnfProfileId

`func (o *AffectedVnf) GetVnfProfileId() string`

GetVnfProfileId returns the VnfProfileId field if non-nil, zero value otherwise.

### GetVnfProfileIdOk

`func (o *AffectedVnf) GetVnfProfileIdOk() (*string, bool)`

GetVnfProfileIdOk returns a tuple with the VnfProfileId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfProfileId

`func (o *AffectedVnf) SetVnfProfileId(v string)`

SetVnfProfileId sets VnfProfileId field to given value.


### GetVnfName

`func (o *AffectedVnf) GetVnfName() string`

GetVnfName returns the VnfName field if non-nil, zero value otherwise.

### GetVnfNameOk

`func (o *AffectedVnf) GetVnfNameOk() (*string, bool)`

GetVnfNameOk returns a tuple with the VnfName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfName

`func (o *AffectedVnf) SetVnfName(v string)`

SetVnfName sets VnfName field to given value.


### GetChangeType

`func (o *AffectedVnf) GetChangeType() string`

GetChangeType returns the ChangeType field if non-nil, zero value otherwise.

### GetChangeTypeOk

`func (o *AffectedVnf) GetChangeTypeOk() (*string, bool)`

GetChangeTypeOk returns a tuple with the ChangeType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChangeType

`func (o *AffectedVnf) SetChangeType(v string)`

SetChangeType sets ChangeType field to given value.


### GetChangeResult

`func (o *AffectedVnf) GetChangeResult() string`

GetChangeResult returns the ChangeResult field if non-nil, zero value otherwise.

### GetChangeResultOk

`func (o *AffectedVnf) GetChangeResultOk() (*string, bool)`

GetChangeResultOk returns a tuple with the ChangeResult field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChangeResult

`func (o *AffectedVnf) SetChangeResult(v string)`

SetChangeResult sets ChangeResult field to given value.


### GetChangedInfo

`func (o *AffectedVnf) GetChangedInfo() AnyOfAnyTypeAnyTypeAnyType`

GetChangedInfo returns the ChangedInfo field if non-nil, zero value otherwise.

### GetChangedInfoOk

`func (o *AffectedVnf) GetChangedInfoOk() (*AnyOfAnyTypeAnyTypeAnyType, bool)`

GetChangedInfoOk returns a tuple with the ChangedInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChangedInfo

`func (o *AffectedVnf) SetChangedInfo(v AnyOfAnyTypeAnyTypeAnyType)`

SetChangedInfo sets ChangedInfo field to given value.

### HasChangedInfo

`func (o *AffectedVnf) HasChangedInfo() bool`

HasChangedInfo returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


