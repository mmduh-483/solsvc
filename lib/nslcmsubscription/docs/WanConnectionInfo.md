# WanConnectionInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**WanConnectionInfoId** | **string** | An identifier that is unique with respect to a NS. Representation: string of variable length.  | 
**NsVirtualLinkInfoId** | Pointer to **string** | An identifier that is unique with respect to a NS. Representation: string of variable length.  | [optional] 
**VnfVirtualLinkResourceInfoId** | Pointer to **string** | An identifier that is unique with respect to a NS. Representation: string of variable length.  | [optional] 
**ProtocolInfo** | Pointer to [**WanConnectionProtocolInfo**](WanConnectionProtocolInfo.md) |  | [optional] 

## Methods

### NewWanConnectionInfo

`func NewWanConnectionInfo(wanConnectionInfoId string, ) *WanConnectionInfo`

NewWanConnectionInfo instantiates a new WanConnectionInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWanConnectionInfoWithDefaults

`func NewWanConnectionInfoWithDefaults() *WanConnectionInfo`

NewWanConnectionInfoWithDefaults instantiates a new WanConnectionInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetWanConnectionInfoId

`func (o *WanConnectionInfo) GetWanConnectionInfoId() string`

GetWanConnectionInfoId returns the WanConnectionInfoId field if non-nil, zero value otherwise.

### GetWanConnectionInfoIdOk

`func (o *WanConnectionInfo) GetWanConnectionInfoIdOk() (*string, bool)`

GetWanConnectionInfoIdOk returns a tuple with the WanConnectionInfoId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWanConnectionInfoId

`func (o *WanConnectionInfo) SetWanConnectionInfoId(v string)`

SetWanConnectionInfoId sets WanConnectionInfoId field to given value.


### GetNsVirtualLinkInfoId

`func (o *WanConnectionInfo) GetNsVirtualLinkInfoId() string`

GetNsVirtualLinkInfoId returns the NsVirtualLinkInfoId field if non-nil, zero value otherwise.

### GetNsVirtualLinkInfoIdOk

`func (o *WanConnectionInfo) GetNsVirtualLinkInfoIdOk() (*string, bool)`

GetNsVirtualLinkInfoIdOk returns a tuple with the NsVirtualLinkInfoId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsVirtualLinkInfoId

`func (o *WanConnectionInfo) SetNsVirtualLinkInfoId(v string)`

SetNsVirtualLinkInfoId sets NsVirtualLinkInfoId field to given value.

### HasNsVirtualLinkInfoId

`func (o *WanConnectionInfo) HasNsVirtualLinkInfoId() bool`

HasNsVirtualLinkInfoId returns a boolean if a field has been set.

### GetVnfVirtualLinkResourceInfoId

`func (o *WanConnectionInfo) GetVnfVirtualLinkResourceInfoId() string`

GetVnfVirtualLinkResourceInfoId returns the VnfVirtualLinkResourceInfoId field if non-nil, zero value otherwise.

### GetVnfVirtualLinkResourceInfoIdOk

`func (o *WanConnectionInfo) GetVnfVirtualLinkResourceInfoIdOk() (*string, bool)`

GetVnfVirtualLinkResourceInfoIdOk returns a tuple with the VnfVirtualLinkResourceInfoId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfVirtualLinkResourceInfoId

`func (o *WanConnectionInfo) SetVnfVirtualLinkResourceInfoId(v string)`

SetVnfVirtualLinkResourceInfoId sets VnfVirtualLinkResourceInfoId field to given value.

### HasVnfVirtualLinkResourceInfoId

`func (o *WanConnectionInfo) HasVnfVirtualLinkResourceInfoId() bool`

HasVnfVirtualLinkResourceInfoId returns a boolean if a field has been set.

### GetProtocolInfo

`func (o *WanConnectionInfo) GetProtocolInfo() WanConnectionProtocolInfo`

GetProtocolInfo returns the ProtocolInfo field if non-nil, zero value otherwise.

### GetProtocolInfoOk

`func (o *WanConnectionInfo) GetProtocolInfoOk() (*WanConnectionProtocolInfo, bool)`

GetProtocolInfoOk returns a tuple with the ProtocolInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProtocolInfo

`func (o *WanConnectionInfo) SetProtocolInfo(v WanConnectionProtocolInfo)`

SetProtocolInfo sets ProtocolInfo field to given value.

### HasProtocolInfo

`func (o *WanConnectionInfo) HasProtocolInfo() bool`

HasProtocolInfo returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


