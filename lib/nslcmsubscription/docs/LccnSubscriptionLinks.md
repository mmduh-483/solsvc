# LccnSubscriptionLinks

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Self** | [**Link**](Link.md) |  | 

## Methods

### NewLccnSubscriptionLinks

`func NewLccnSubscriptionLinks(self Link, ) *LccnSubscriptionLinks`

NewLccnSubscriptionLinks instantiates a new LccnSubscriptionLinks object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewLccnSubscriptionLinksWithDefaults

`func NewLccnSubscriptionLinksWithDefaults() *LccnSubscriptionLinks`

NewLccnSubscriptionLinksWithDefaults instantiates a new LccnSubscriptionLinks object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSelf

`func (o *LccnSubscriptionLinks) GetSelf() Link`

GetSelf returns the Self field if non-nil, zero value otherwise.

### GetSelfOk

`func (o *LccnSubscriptionLinks) GetSelfOk() (*Link, bool)`

GetSelfOk returns a tuple with the Self field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSelf

`func (o *LccnSubscriptionLinks) SetSelf(v Link)`

SetSelf sets Self field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


