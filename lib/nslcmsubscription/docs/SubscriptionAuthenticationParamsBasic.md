# SubscriptionAuthenticationParamsBasic

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**UserName** | Pointer to **string** | Username to be used in HTTP Basic authentication. Shall be present if it has not been provisioned out of band.  | [optional] 
**Password** | Pointer to **string** | Password to be used in HTTP Basic authentication. Shall be present if it has not been provisioned out of band.  | [optional] 

## Methods

### NewSubscriptionAuthenticationParamsBasic

`func NewSubscriptionAuthenticationParamsBasic() *SubscriptionAuthenticationParamsBasic`

NewSubscriptionAuthenticationParamsBasic instantiates a new SubscriptionAuthenticationParamsBasic object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSubscriptionAuthenticationParamsBasicWithDefaults

`func NewSubscriptionAuthenticationParamsBasicWithDefaults() *SubscriptionAuthenticationParamsBasic`

NewSubscriptionAuthenticationParamsBasicWithDefaults instantiates a new SubscriptionAuthenticationParamsBasic object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUserName

`func (o *SubscriptionAuthenticationParamsBasic) GetUserName() string`

GetUserName returns the UserName field if non-nil, zero value otherwise.

### GetUserNameOk

`func (o *SubscriptionAuthenticationParamsBasic) GetUserNameOk() (*string, bool)`

GetUserNameOk returns a tuple with the UserName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUserName

`func (o *SubscriptionAuthenticationParamsBasic) SetUserName(v string)`

SetUserName sets UserName field to given value.

### HasUserName

`func (o *SubscriptionAuthenticationParamsBasic) HasUserName() bool`

HasUserName returns a boolean if a field has been set.

### GetPassword

`func (o *SubscriptionAuthenticationParamsBasic) GetPassword() string`

GetPassword returns the Password field if non-nil, zero value otherwise.

### GetPasswordOk

`func (o *SubscriptionAuthenticationParamsBasic) GetPasswordOk() (*string, bool)`

GetPasswordOk returns a tuple with the Password field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPassword

`func (o *SubscriptionAuthenticationParamsBasic) SetPassword(v string)`

SetPassword sets Password field to given value.

### HasPassword

`func (o *SubscriptionAuthenticationParamsBasic) HasPassword() bool`

HasPassword returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


