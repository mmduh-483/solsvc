# ScaleByStepData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AspectId** | **string** | Identifier of the VNF Virtual Link Descriptor (VLD) in the VNFD.  | 
**NumberOfSteps** | Pointer to **int32** | Number of scaling steps. It shall be a positive number. Defaults to 1. The VNF provider defines in the VNFD whether or not a particular VNF supports performing more than one step at a time. Such a property in the VNFD applies for all instances of a particular VNF. See note.  | [optional] [default to 1]
**AdditionalParams** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 

## Methods

### NewScaleByStepData

`func NewScaleByStepData(aspectId string, ) *ScaleByStepData`

NewScaleByStepData instantiates a new ScaleByStepData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewScaleByStepDataWithDefaults

`func NewScaleByStepDataWithDefaults() *ScaleByStepData`

NewScaleByStepDataWithDefaults instantiates a new ScaleByStepData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAspectId

`func (o *ScaleByStepData) GetAspectId() string`

GetAspectId returns the AspectId field if non-nil, zero value otherwise.

### GetAspectIdOk

`func (o *ScaleByStepData) GetAspectIdOk() (*string, bool)`

GetAspectIdOk returns a tuple with the AspectId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAspectId

`func (o *ScaleByStepData) SetAspectId(v string)`

SetAspectId sets AspectId field to given value.


### GetNumberOfSteps

`func (o *ScaleByStepData) GetNumberOfSteps() int32`

GetNumberOfSteps returns the NumberOfSteps field if non-nil, zero value otherwise.

### GetNumberOfStepsOk

`func (o *ScaleByStepData) GetNumberOfStepsOk() (*int32, bool)`

GetNumberOfStepsOk returns a tuple with the NumberOfSteps field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNumberOfSteps

`func (o *ScaleByStepData) SetNumberOfSteps(v int32)`

SetNumberOfSteps sets NumberOfSteps field to given value.

### HasNumberOfSteps

`func (o *ScaleByStepData) HasNumberOfSteps() bool`

HasNumberOfSteps returns a boolean if a field has been set.

### GetAdditionalParams

`func (o *ScaleByStepData) GetAdditionalParams() map[string]interface{}`

GetAdditionalParams returns the AdditionalParams field if non-nil, zero value otherwise.

### GetAdditionalParamsOk

`func (o *ScaleByStepData) GetAdditionalParamsOk() (*map[string]interface{}, bool)`

GetAdditionalParamsOk returns a tuple with the AdditionalParams field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdditionalParams

`func (o *ScaleByStepData) SetAdditionalParams(v map[string]interface{})`

SetAdditionalParams sets AdditionalParams field to given value.

### HasAdditionalParams

`func (o *ScaleByStepData) HasAdditionalParams() bool`

HasAdditionalParams returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


