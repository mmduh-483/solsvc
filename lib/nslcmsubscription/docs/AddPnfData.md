# AddPnfData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PnfId** | **string** | An identifier with the intention of being globally unique.  | 
**PnfName** | **string** | Name of the PNF  | 
**PnfdId** | **string** | An identifier with the intention of being globally unique.  | 
**PnfProfileId** | **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | 
**CpData** | Pointer to [**[]PnfExtCpData**](PnfExtCpData.md) | Address assigned for the PNF external CP(s).  | [optional] 

## Methods

### NewAddPnfData

`func NewAddPnfData(pnfId string, pnfName string, pnfdId string, pnfProfileId string, ) *AddPnfData`

NewAddPnfData instantiates a new AddPnfData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAddPnfDataWithDefaults

`func NewAddPnfDataWithDefaults() *AddPnfData`

NewAddPnfDataWithDefaults instantiates a new AddPnfData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPnfId

`func (o *AddPnfData) GetPnfId() string`

GetPnfId returns the PnfId field if non-nil, zero value otherwise.

### GetPnfIdOk

`func (o *AddPnfData) GetPnfIdOk() (*string, bool)`

GetPnfIdOk returns a tuple with the PnfId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPnfId

`func (o *AddPnfData) SetPnfId(v string)`

SetPnfId sets PnfId field to given value.


### GetPnfName

`func (o *AddPnfData) GetPnfName() string`

GetPnfName returns the PnfName field if non-nil, zero value otherwise.

### GetPnfNameOk

`func (o *AddPnfData) GetPnfNameOk() (*string, bool)`

GetPnfNameOk returns a tuple with the PnfName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPnfName

`func (o *AddPnfData) SetPnfName(v string)`

SetPnfName sets PnfName field to given value.


### GetPnfdId

`func (o *AddPnfData) GetPnfdId() string`

GetPnfdId returns the PnfdId field if non-nil, zero value otherwise.

### GetPnfdIdOk

`func (o *AddPnfData) GetPnfdIdOk() (*string, bool)`

GetPnfdIdOk returns a tuple with the PnfdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPnfdId

`func (o *AddPnfData) SetPnfdId(v string)`

SetPnfdId sets PnfdId field to given value.


### GetPnfProfileId

`func (o *AddPnfData) GetPnfProfileId() string`

GetPnfProfileId returns the PnfProfileId field if non-nil, zero value otherwise.

### GetPnfProfileIdOk

`func (o *AddPnfData) GetPnfProfileIdOk() (*string, bool)`

GetPnfProfileIdOk returns a tuple with the PnfProfileId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPnfProfileId

`func (o *AddPnfData) SetPnfProfileId(v string)`

SetPnfProfileId sets PnfProfileId field to given value.


### GetCpData

`func (o *AddPnfData) GetCpData() []PnfExtCpData`

GetCpData returns the CpData field if non-nil, zero value otherwise.

### GetCpDataOk

`func (o *AddPnfData) GetCpDataOk() (*[]PnfExtCpData, bool)`

GetCpDataOk returns a tuple with the CpData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCpData

`func (o *AddPnfData) SetCpData(v []PnfExtCpData)`

SetCpData sets CpData field to given value.

### HasCpData

`func (o *AddPnfData) HasCpData() bool`

HasCpData returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


