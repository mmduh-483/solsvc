# ModifyPnfData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PnfId** | **string** | An identifier with the intention of being globally unique.  | 
**PnfName** | Pointer to **string** | Name of the PNF. See note.  | [optional] 
**CpData** | Pointer to [**[]PnfExtCpData**](PnfExtCpData.md) | Address assigned for the PNF external CP(s). See note.  | [optional] 

## Methods

### NewModifyPnfData

`func NewModifyPnfData(pnfId string, ) *ModifyPnfData`

NewModifyPnfData instantiates a new ModifyPnfData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewModifyPnfDataWithDefaults

`func NewModifyPnfDataWithDefaults() *ModifyPnfData`

NewModifyPnfDataWithDefaults instantiates a new ModifyPnfData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPnfId

`func (o *ModifyPnfData) GetPnfId() string`

GetPnfId returns the PnfId field if non-nil, zero value otherwise.

### GetPnfIdOk

`func (o *ModifyPnfData) GetPnfIdOk() (*string, bool)`

GetPnfIdOk returns a tuple with the PnfId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPnfId

`func (o *ModifyPnfData) SetPnfId(v string)`

SetPnfId sets PnfId field to given value.


### GetPnfName

`func (o *ModifyPnfData) GetPnfName() string`

GetPnfName returns the PnfName field if non-nil, zero value otherwise.

### GetPnfNameOk

`func (o *ModifyPnfData) GetPnfNameOk() (*string, bool)`

GetPnfNameOk returns a tuple with the PnfName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPnfName

`func (o *ModifyPnfData) SetPnfName(v string)`

SetPnfName sets PnfName field to given value.

### HasPnfName

`func (o *ModifyPnfData) HasPnfName() bool`

HasPnfName returns a boolean if a field has been set.

### GetCpData

`func (o *ModifyPnfData) GetCpData() []PnfExtCpData`

GetCpData returns the CpData field if non-nil, zero value otherwise.

### GetCpDataOk

`func (o *ModifyPnfData) GetCpDataOk() (*[]PnfExtCpData, bool)`

GetCpDataOk returns a tuple with the CpData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCpData

`func (o *ModifyPnfData) SetCpData(v []PnfExtCpData)`

SetCpData sets CpData field to given value.

### HasCpData

`func (o *ModifyPnfData) HasCpData() bool`

HasCpData returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


