# NsMonitoringParameter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | 
**Name** | Pointer to **string** | Human readable name of the monitoring parameter, as defined in the NSD.  | [optional] 
**PerformanceMetric** | **string** | Performance metric that is monitored. This attribute shall contain the related  \&quot;Measurement Name\&quot; value as defined in clause 7.2 of ETSI GS NFV-IFA 027.  | 

## Methods

### NewNsMonitoringParameter

`func NewNsMonitoringParameter(id string, performanceMetric string, ) *NsMonitoringParameter`

NewNsMonitoringParameter instantiates a new NsMonitoringParameter object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNsMonitoringParameterWithDefaults

`func NewNsMonitoringParameterWithDefaults() *NsMonitoringParameter`

NewNsMonitoringParameterWithDefaults instantiates a new NsMonitoringParameter object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *NsMonitoringParameter) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *NsMonitoringParameter) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *NsMonitoringParameter) SetId(v string)`

SetId sets Id field to given value.


### GetName

`func (o *NsMonitoringParameter) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *NsMonitoringParameter) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *NsMonitoringParameter) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *NsMonitoringParameter) HasName() bool`

HasName returns a boolean if a field has been set.

### GetPerformanceMetric

`func (o *NsMonitoringParameter) GetPerformanceMetric() string`

GetPerformanceMetric returns the PerformanceMetric field if non-nil, zero value otherwise.

### GetPerformanceMetricOk

`func (o *NsMonitoringParameter) GetPerformanceMetricOk() (*string, bool)`

GetPerformanceMetricOk returns a tuple with the PerformanceMetric field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPerformanceMetric

`func (o *NsMonitoringParameter) SetPerformanceMetric(v string)`

SetPerformanceMetric sets PerformanceMetric field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


