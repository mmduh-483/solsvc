# Mask

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StartingPoint** | **int32** | Indicates the offset between the last bit of the source mac address and the first bit of the sequence of bits to be matched.  | 
**Length** | **int32** | Indicates the number of bits to be matched.  | 
**Value** | **string** | Provide the sequence of bit values to be matched.  | 

## Methods

### NewMask

`func NewMask(startingPoint int32, length int32, value string, ) *Mask`

NewMask instantiates a new Mask object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMaskWithDefaults

`func NewMaskWithDefaults() *Mask`

NewMaskWithDefaults instantiates a new Mask object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetStartingPoint

`func (o *Mask) GetStartingPoint() int32`

GetStartingPoint returns the StartingPoint field if non-nil, zero value otherwise.

### GetStartingPointOk

`func (o *Mask) GetStartingPointOk() (*int32, bool)`

GetStartingPointOk returns a tuple with the StartingPoint field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStartingPoint

`func (o *Mask) SetStartingPoint(v int32)`

SetStartingPoint sets StartingPoint field to given value.


### GetLength

`func (o *Mask) GetLength() int32`

GetLength returns the Length field if non-nil, zero value otherwise.

### GetLengthOk

`func (o *Mask) GetLengthOk() (*int32, bool)`

GetLengthOk returns a tuple with the Length field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLength

`func (o *Mask) SetLength(v int32)`

SetLength sets Length field to given value.


### GetValue

`func (o *Mask) GetValue() string`

GetValue returns the Value field if non-nil, zero value otherwise.

### GetValueOk

`func (o *Mask) GetValueOk() (*string, bool)`

GetValueOk returns a tuple with the Value field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValue

`func (o *Mask) SetValue(v string)`

SetValue sets Value field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


