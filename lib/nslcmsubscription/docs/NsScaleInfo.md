# NsScaleInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NsScalingAspectId** | **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | 
**NsScaleLevelId** | **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | 

## Methods

### NewNsScaleInfo

`func NewNsScaleInfo(nsScalingAspectId string, nsScaleLevelId string, ) *NsScaleInfo`

NewNsScaleInfo instantiates a new NsScaleInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNsScaleInfoWithDefaults

`func NewNsScaleInfoWithDefaults() *NsScaleInfo`

NewNsScaleInfoWithDefaults instantiates a new NsScaleInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetNsScalingAspectId

`func (o *NsScaleInfo) GetNsScalingAspectId() string`

GetNsScalingAspectId returns the NsScalingAspectId field if non-nil, zero value otherwise.

### GetNsScalingAspectIdOk

`func (o *NsScaleInfo) GetNsScalingAspectIdOk() (*string, bool)`

GetNsScalingAspectIdOk returns a tuple with the NsScalingAspectId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsScalingAspectId

`func (o *NsScaleInfo) SetNsScalingAspectId(v string)`

SetNsScalingAspectId sets NsScalingAspectId field to given value.


### GetNsScaleLevelId

`func (o *NsScaleInfo) GetNsScaleLevelId() string`

GetNsScaleLevelId returns the NsScaleLevelId field if non-nil, zero value otherwise.

### GetNsScaleLevelIdOk

`func (o *NsScaleInfo) GetNsScaleLevelIdOk() (*string, bool)`

GetNsScaleLevelIdOk returns a tuple with the NsScaleLevelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsScaleLevelId

`func (o *NsScaleInfo) SetNsScaleLevelId(v string)`

SetNsScaleLevelId sets NsScaleLevelId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


