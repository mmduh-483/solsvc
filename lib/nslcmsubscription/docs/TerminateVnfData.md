# TerminateVnfData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VnfInstanceId** | **string** | An identifier with the intention of being globally unique.  | 
**TerminationType** | Pointer to **string** | Indicates whether forceful or graceful termination is requested. See note. Permitted values: - FORCEFUL - GRACEFUL  | [optional] 
**GracefulTerminationTimeout** | Pointer to **int32** | The attribute is only applicable in case of graceful termination. It defines the time to wait for the VNF to be taken out of service before shutting down the VNF and releasing the resources. The unit is seconds.  | [optional] 
**AdditionalParams** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 

## Methods

### NewTerminateVnfData

`func NewTerminateVnfData(vnfInstanceId string, ) *TerminateVnfData`

NewTerminateVnfData instantiates a new TerminateVnfData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTerminateVnfDataWithDefaults

`func NewTerminateVnfDataWithDefaults() *TerminateVnfData`

NewTerminateVnfDataWithDefaults instantiates a new TerminateVnfData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVnfInstanceId

`func (o *TerminateVnfData) GetVnfInstanceId() string`

GetVnfInstanceId returns the VnfInstanceId field if non-nil, zero value otherwise.

### GetVnfInstanceIdOk

`func (o *TerminateVnfData) GetVnfInstanceIdOk() (*string, bool)`

GetVnfInstanceIdOk returns a tuple with the VnfInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceId

`func (o *TerminateVnfData) SetVnfInstanceId(v string)`

SetVnfInstanceId sets VnfInstanceId field to given value.


### GetTerminationType

`func (o *TerminateVnfData) GetTerminationType() string`

GetTerminationType returns the TerminationType field if non-nil, zero value otherwise.

### GetTerminationTypeOk

`func (o *TerminateVnfData) GetTerminationTypeOk() (*string, bool)`

GetTerminationTypeOk returns a tuple with the TerminationType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTerminationType

`func (o *TerminateVnfData) SetTerminationType(v string)`

SetTerminationType sets TerminationType field to given value.

### HasTerminationType

`func (o *TerminateVnfData) HasTerminationType() bool`

HasTerminationType returns a boolean if a field has been set.

### GetGracefulTerminationTimeout

`func (o *TerminateVnfData) GetGracefulTerminationTimeout() int32`

GetGracefulTerminationTimeout returns the GracefulTerminationTimeout field if non-nil, zero value otherwise.

### GetGracefulTerminationTimeoutOk

`func (o *TerminateVnfData) GetGracefulTerminationTimeoutOk() (*int32, bool)`

GetGracefulTerminationTimeoutOk returns a tuple with the GracefulTerminationTimeout field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGracefulTerminationTimeout

`func (o *TerminateVnfData) SetGracefulTerminationTimeout(v int32)`

SetGracefulTerminationTimeout sets GracefulTerminationTimeout field to given value.

### HasGracefulTerminationTimeout

`func (o *TerminateVnfData) HasGracefulTerminationTimeout() bool`

HasGracefulTerminationTimeout returns a boolean if a field has been set.

### GetAdditionalParams

`func (o *TerminateVnfData) GetAdditionalParams() map[string]interface{}`

GetAdditionalParams returns the AdditionalParams field if non-nil, zero value otherwise.

### GetAdditionalParamsOk

`func (o *TerminateVnfData) GetAdditionalParamsOk() (*map[string]interface{}, bool)`

GetAdditionalParamsOk returns a tuple with the AdditionalParams field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdditionalParams

`func (o *TerminateVnfData) SetAdditionalParams(v map[string]interface{})`

SetAdditionalParams sets AdditionalParams field to given value.

### HasAdditionalParams

`func (o *TerminateVnfData) HasAdditionalParams() bool`

HasAdditionalParams returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


