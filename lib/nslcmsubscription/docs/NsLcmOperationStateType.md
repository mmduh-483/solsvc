# NsLcmOperationStateType

## Enum


* `PROCESSING` (value: `"PROCESSING"`)

* `COMPLETED` (value: `"COMPLETED"`)

* `PARTIALLY_COMPLETED` (value: `"PARTIALLY_COMPLETED"`)

* `FAILED_TEMP` (value: `"FAILED_TEMP"`)

* `FAILED` (value: `"FAILED"`)

* `ROLLING_BACK` (value: `"ROLLING_BACK"`)

* `ROLLED_BACK` (value: `"ROLLED_BACK"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


