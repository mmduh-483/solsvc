# CreateNsRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NsdId** | **string** | An identifier with the intention of being globally unique.  | 
**NsName** | **string** | Human-readable name of the NS instance to be created.  | 
**NsDescription** | **string** | Human-readable description of the NS instance to be created.  | 

## Methods

### NewCreateNsRequest

`func NewCreateNsRequest(nsdId string, nsName string, nsDescription string, ) *CreateNsRequest`

NewCreateNsRequest instantiates a new CreateNsRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCreateNsRequestWithDefaults

`func NewCreateNsRequestWithDefaults() *CreateNsRequest`

NewCreateNsRequestWithDefaults instantiates a new CreateNsRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetNsdId

`func (o *CreateNsRequest) GetNsdId() string`

GetNsdId returns the NsdId field if non-nil, zero value otherwise.

### GetNsdIdOk

`func (o *CreateNsRequest) GetNsdIdOk() (*string, bool)`

GetNsdIdOk returns a tuple with the NsdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsdId

`func (o *CreateNsRequest) SetNsdId(v string)`

SetNsdId sets NsdId field to given value.


### GetNsName

`func (o *CreateNsRequest) GetNsName() string`

GetNsName returns the NsName field if non-nil, zero value otherwise.

### GetNsNameOk

`func (o *CreateNsRequest) GetNsNameOk() (*string, bool)`

GetNsNameOk returns a tuple with the NsName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsName

`func (o *CreateNsRequest) SetNsName(v string)`

SetNsName sets NsName field to given value.


### GetNsDescription

`func (o *CreateNsRequest) GetNsDescription() string`

GetNsDescription returns the NsDescription field if non-nil, zero value otherwise.

### GetNsDescriptionOk

`func (o *CreateNsRequest) GetNsDescriptionOk() (*string, bool)`

GetNsDescriptionOk returns a tuple with the NsDescription field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsDescription

`func (o *CreateNsRequest) SetNsDescription(v string)`

SetNsDescription sets NsDescription field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


