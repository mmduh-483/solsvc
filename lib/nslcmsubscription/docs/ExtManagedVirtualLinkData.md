# ExtManagedVirtualLinkData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ExtManagedVirtualLinkId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**VnfVirtualLinkDescId** | **string** | Identifier of the VNF Virtual Link Descriptor (VLD) in the VNFD.  | 
**VimId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**ResourceProviderId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**ResourceId** | **string** | An identifier maintained by the VIM or other resource provider. It is expected to be unique within the VIM instance. Representation: string of variable length.  | 
**VnfLinkPort** | Pointer to [**[]VnfLinkPortData**](VnfLinkPortData.md) | Externally provided link ports to be used to connect VNFC connection points to this externally-managed VL on this network resource. If this attribute is not present, the VNFM shall create the link ports on the externally-managed VL.  | [optional] 
**ExtManagedMultisiteVirtualLinkId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 

## Methods

### NewExtManagedVirtualLinkData

`func NewExtManagedVirtualLinkData(vnfVirtualLinkDescId string, resourceId string, ) *ExtManagedVirtualLinkData`

NewExtManagedVirtualLinkData instantiates a new ExtManagedVirtualLinkData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewExtManagedVirtualLinkDataWithDefaults

`func NewExtManagedVirtualLinkDataWithDefaults() *ExtManagedVirtualLinkData`

NewExtManagedVirtualLinkDataWithDefaults instantiates a new ExtManagedVirtualLinkData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetExtManagedVirtualLinkId

`func (o *ExtManagedVirtualLinkData) GetExtManagedVirtualLinkId() string`

GetExtManagedVirtualLinkId returns the ExtManagedVirtualLinkId field if non-nil, zero value otherwise.

### GetExtManagedVirtualLinkIdOk

`func (o *ExtManagedVirtualLinkData) GetExtManagedVirtualLinkIdOk() (*string, bool)`

GetExtManagedVirtualLinkIdOk returns a tuple with the ExtManagedVirtualLinkId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtManagedVirtualLinkId

`func (o *ExtManagedVirtualLinkData) SetExtManagedVirtualLinkId(v string)`

SetExtManagedVirtualLinkId sets ExtManagedVirtualLinkId field to given value.

### HasExtManagedVirtualLinkId

`func (o *ExtManagedVirtualLinkData) HasExtManagedVirtualLinkId() bool`

HasExtManagedVirtualLinkId returns a boolean if a field has been set.

### GetVnfVirtualLinkDescId

`func (o *ExtManagedVirtualLinkData) GetVnfVirtualLinkDescId() string`

GetVnfVirtualLinkDescId returns the VnfVirtualLinkDescId field if non-nil, zero value otherwise.

### GetVnfVirtualLinkDescIdOk

`func (o *ExtManagedVirtualLinkData) GetVnfVirtualLinkDescIdOk() (*string, bool)`

GetVnfVirtualLinkDescIdOk returns a tuple with the VnfVirtualLinkDescId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfVirtualLinkDescId

`func (o *ExtManagedVirtualLinkData) SetVnfVirtualLinkDescId(v string)`

SetVnfVirtualLinkDescId sets VnfVirtualLinkDescId field to given value.


### GetVimId

`func (o *ExtManagedVirtualLinkData) GetVimId() string`

GetVimId returns the VimId field if non-nil, zero value otherwise.

### GetVimIdOk

`func (o *ExtManagedVirtualLinkData) GetVimIdOk() (*string, bool)`

GetVimIdOk returns a tuple with the VimId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVimId

`func (o *ExtManagedVirtualLinkData) SetVimId(v string)`

SetVimId sets VimId field to given value.

### HasVimId

`func (o *ExtManagedVirtualLinkData) HasVimId() bool`

HasVimId returns a boolean if a field has been set.

### GetResourceProviderId

`func (o *ExtManagedVirtualLinkData) GetResourceProviderId() string`

GetResourceProviderId returns the ResourceProviderId field if non-nil, zero value otherwise.

### GetResourceProviderIdOk

`func (o *ExtManagedVirtualLinkData) GetResourceProviderIdOk() (*string, bool)`

GetResourceProviderIdOk returns a tuple with the ResourceProviderId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResourceProviderId

`func (o *ExtManagedVirtualLinkData) SetResourceProviderId(v string)`

SetResourceProviderId sets ResourceProviderId field to given value.

### HasResourceProviderId

`func (o *ExtManagedVirtualLinkData) HasResourceProviderId() bool`

HasResourceProviderId returns a boolean if a field has been set.

### GetResourceId

`func (o *ExtManagedVirtualLinkData) GetResourceId() string`

GetResourceId returns the ResourceId field if non-nil, zero value otherwise.

### GetResourceIdOk

`func (o *ExtManagedVirtualLinkData) GetResourceIdOk() (*string, bool)`

GetResourceIdOk returns a tuple with the ResourceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResourceId

`func (o *ExtManagedVirtualLinkData) SetResourceId(v string)`

SetResourceId sets ResourceId field to given value.


### GetVnfLinkPort

`func (o *ExtManagedVirtualLinkData) GetVnfLinkPort() []VnfLinkPortData`

GetVnfLinkPort returns the VnfLinkPort field if non-nil, zero value otherwise.

### GetVnfLinkPortOk

`func (o *ExtManagedVirtualLinkData) GetVnfLinkPortOk() (*[]VnfLinkPortData, bool)`

GetVnfLinkPortOk returns a tuple with the VnfLinkPort field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfLinkPort

`func (o *ExtManagedVirtualLinkData) SetVnfLinkPort(v []VnfLinkPortData)`

SetVnfLinkPort sets VnfLinkPort field to given value.

### HasVnfLinkPort

`func (o *ExtManagedVirtualLinkData) HasVnfLinkPort() bool`

HasVnfLinkPort returns a boolean if a field has been set.

### GetExtManagedMultisiteVirtualLinkId

`func (o *ExtManagedVirtualLinkData) GetExtManagedMultisiteVirtualLinkId() string`

GetExtManagedMultisiteVirtualLinkId returns the ExtManagedMultisiteVirtualLinkId field if non-nil, zero value otherwise.

### GetExtManagedMultisiteVirtualLinkIdOk

`func (o *ExtManagedVirtualLinkData) GetExtManagedMultisiteVirtualLinkIdOk() (*string, bool)`

GetExtManagedMultisiteVirtualLinkIdOk returns a tuple with the ExtManagedMultisiteVirtualLinkId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtManagedMultisiteVirtualLinkId

`func (o *ExtManagedVirtualLinkData) SetExtManagedMultisiteVirtualLinkId(v string)`

SetExtManagedMultisiteVirtualLinkId sets ExtManagedMultisiteVirtualLinkId field to given value.

### HasExtManagedMultisiteVirtualLinkId

`func (o *ExtManagedVirtualLinkData) HasExtManagedMultisiteVirtualLinkId() bool`

HasExtManagedMultisiteVirtualLinkId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


