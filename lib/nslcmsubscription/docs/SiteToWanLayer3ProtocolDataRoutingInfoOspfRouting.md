# SiteToWanLayer3ProtocolDataRoutingInfoOspfRouting

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AreaId** | **string** | The routing area identifier, e.g., a number or an IP address.  | 

## Methods

### NewSiteToWanLayer3ProtocolDataRoutingInfoOspfRouting

`func NewSiteToWanLayer3ProtocolDataRoutingInfoOspfRouting(areaId string, ) *SiteToWanLayer3ProtocolDataRoutingInfoOspfRouting`

NewSiteToWanLayer3ProtocolDataRoutingInfoOspfRouting instantiates a new SiteToWanLayer3ProtocolDataRoutingInfoOspfRouting object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSiteToWanLayer3ProtocolDataRoutingInfoOspfRoutingWithDefaults

`func NewSiteToWanLayer3ProtocolDataRoutingInfoOspfRoutingWithDefaults() *SiteToWanLayer3ProtocolDataRoutingInfoOspfRouting`

NewSiteToWanLayer3ProtocolDataRoutingInfoOspfRoutingWithDefaults instantiates a new SiteToWanLayer3ProtocolDataRoutingInfoOspfRouting object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAreaId

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoOspfRouting) GetAreaId() string`

GetAreaId returns the AreaId field if non-nil, zero value otherwise.

### GetAreaIdOk

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoOspfRouting) GetAreaIdOk() (*string, bool)`

GetAreaIdOk returns a tuple with the AreaId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAreaId

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoOspfRouting) SetAreaId(v string)`

SetAreaId sets AreaId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


