# UpdateNsRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**UpdateType** | **string** | The type of update. It determines also which one of the following parameters is present in the operation. Possible values include: * ADD_VNF: Adding existing VNF instance(s) * REMOVE_VNF: Removing VNF instance(s) * INSTANTIATE_VNF: Instantiating new VNF(s) * CHANGE_VNF_DF: Changing VNF DF * OPERATE_VNF: Changing VNF state, * MODIFY_VNF_INFORMATION: Modifying VNF information and/or the configurable properties of VNF instance(s) * CHANGE_EXTERNAL_VNF_CONNECTIVITY: Changing the external connectivity of VNF instance(s) * CHANGE_VNFPKG: Changing the VNF package(s) on which (a) VNF instance(s) is/are based * ADD_SAP: Adding SAP(s) * REMOVE_SAP: Removing SAP(s) * ADD_NESTED_NS: Adding existing NS instance(s) as nested NS(s) * REMOVE_NESTED_NS: Removing existing nested NS instance(s) * ASSOC_NEW_NSD_VERSION: Associating a new NSD version to the NS instance * MOVE_VNF: Moving VNF instance(s) from one origin NS instance to another target NS instance * ADD_VNFFG: Adding VNFFG(s) * REMOVE_VNFFG: Removing VNFFG(s) * UPDATE_VNFFG: Updating VNFFG(s) * CHANGE_NS_DF: Changing NS DF * ADD_PNF: Adding PNF * MODIFY_PNF: Modifying PNF * REMOVE_PNF: Removing PNF * CREATE_VNF_SNAPSHOT: Creating VNF Snapshots of VNF instances belonging to the NS instance. See note 2 * REVERT_VNF_TO_SNAPSHOT: Reverting a VNF instance belonging to the NS instance to a VNF Snapshot. See note 2 and note 3 * DELETE_VNF_SNAPSHOT_INFO: Deleting available VNF Snapshot information for a VNF instance belonging to the NS instance. See note 2 * MODIFY_WAN_CONNECTION_INFO: Modify WAN related connectivity information. * CREATE_NS_VIRTUAL_LINK: Create an NsVirtualLink instance. * DELETE_NS_VIRTUAL_LINK: Delete an NsVirtualLink instance.  | 
**AddVnfIstance** | Pointer to [**[]VnfInstanceData**](VnfInstanceData.md) | Identifies an existing VNF instance to be added to the NS instance. It shall be present only if updateType &#x3D; \&quot;ADD_VNF\&quot;.           | [optional] 
**RemoveVnfInstanceId** | Pointer to **[]string** | Identifies an existing VNF instance to be removed from the NS instance. It contains the identifier(s) of the VNF instances to be removed. It shall be present only if updateType &#x3D; \&quot;REMOVE_VNF.\&quot;. See note 1.  | [optional] 
**InstantiateVnfData** | Pointer to [**[]InstantiateVnfData**](InstantiateVnfData.md) | Identifies the new VNF to be instantiated. It can be used e.g. for the bottom-up NS creation. It shall be present only if updateType &#x3D; \&quot;INSTANTIATE_VNF\&quot;.  | [optional] 
**TerminateVnfData** | Pointer to [**[]TerminateVnfData**](TerminateVnfData.md) | Specifies the details to terminate VNF instance(s). It shall be present only if updateType &#x3D; \&quot;REMOVE_VNF\&quot; and if the VNF instance(s) is(are) to be terminated as part of this operation. See notes 1 and 4.  | [optional] 
**ChangeVnfFlavourData** | Pointer to [**[]ChangeVnfFlavourData**](ChangeVnfFlavourData.md) | Identifies the new DF of the VNF instance to be changed to. It shall be present only if updateType &#x3D; \&quot;CHANGE_VNF_DF\&quot;.  | [optional] 
**OperateVnfData** | Pointer to [**[]OperateVnfData**](OperateVnfData.md) | Identifies the state of the VNF instance to be changed.  It shall be present only if updateType &#x3D; \&quot;OPERATE_VNF\&quot;.  | [optional] 
**ModifyVnfInfoData** | Pointer to [**[]ModifyVnfInfoData**](ModifyVnfInfoData.md) | Identifies the VNF information parameters and/or the configurable properties of VNF instance to be modified. It shall be present only if updateType &#x3D; \&quot;MODIFY_VNF_INFORMATION\&quot;.  | [optional] 
**ChangeExtVnfConnectivityData** | Pointer to [**[]ChangeExtVnfConnectivityData**](ChangeExtVnfConnectivityData.md) | Specifies the new external connectivity data of the VNF instance to be changed. It shall be present only if updateType &#x3D; \&quot;CHANGE_EXTERNAL_VNF_CONNECTIVITY\&quot;.  | [optional] 
**ChangeVnfPackageData** | Pointer to [**[]ChangeVnfPackageData**](ChangeVnfPackageData.md) | Specifies the details to change the VNF package on which the VNF instance is based. It shall be present only if updateType &#x3D; \&quot;CHANGE_VNFPKG\&quot;.  | [optional] 
**AddSap** | Pointer to [**[]SapData**](SapData.md) | Identifies a new SAP to be added to the NS instance. It shall be present only if updateType &#x3D; \&quot;ADD_SAP.\&quot;  | [optional] 
**RemoveSapId** | Pointer to **[]string** | The identifier an existing SAP to be removed from the NS instance. It shall be present only if updateType &#x3D; \&quot;REMOVE_SAP.\&quot;  | [optional] 
**AddNestedNsData** | Pointer to [**[]NestedNsInstanceData**](NestedNsInstanceData.md) | The identifier of an existing nested NS instance to be added to (nested within) the NS instance. It shall be present only if updateType &#x3D; \&quot;ADD_NESTED_NS\&quot;.  | [optional] 
**RemoveNestedNsId** | Pointer to **[]string** | The identifier of an existing nested NS instance to be removed from the NS instance. It shall be present only if updateType &#x3D; \&quot;REMOVE_NESTED_NS\&quot;.  | [optional] 
**AssocNewNsdVersionData** | Pointer to [**AssocNewNsdVersionData**](AssocNewNsdVersionData.md) |  | [optional] 
**MoveVnfInstanceData** | Pointer to [**[]MoveVnfInstanceData**](MoveVnfInstanceData.md) | Specify existing VNF instance to be moved from one NS instance to another NS instance. It shall be present only if updateType &#x3D; MOVE_VNF\&quot;.  | [optional] 
**AddVnffg** | Pointer to [**[]AddVnffgData**](AddVnffgData.md) | Specify the new VNFFG to be created to the NS Instance. It shall be present only if updateType &#x3D; \&quot;ADD_VNFFG\&quot;.  | [optional] 
**RemoveVnffgId** | Pointer to **[]string** | Identifier of an existing VNFFG to be removed from the NS Instance. It shall be present only if updateType &#x3D; \&quot;REMOVE_VNFFG\&quot;.  | [optional] 
**UpdateVnffg** | Pointer to [**[]UpdateVnffgData**](UpdateVnffgData.md) | Specify the new VNFFG Information data to be updated for a VNFFG of the NS Instance. It shall be present only if updateType &#x3D; \&quot;UPDATE_VNFFG\&quot;.  | [optional] 
**ChangeNsFlavourData** | Pointer to [**ChangeNsFlavourData**](ChangeNsFlavourData.md) |  | [optional] 
**AddPnfData** | Pointer to [**[]AddPnfData**](AddPnfData.md) | specifies the PNF to be added into the NS instance.  It shall be present only if updateType &#x3D; \&quot;ADD_PNF\&quot;.  | [optional] 
**ModifyPnfData** | Pointer to [**[]ModifyPnfData**](ModifyPnfData.md) | Specifies the PNF to be modified in the NS instance.  It shall be present only if updateType &#x3D; \&quot;MODIFY_PNF\&quot;.  | [optional] 
**RemovePnfId** | Pointer to **[]string** | Identifier of the PNF to be deleted from the NS instance. It shall be present only if updateType &#x3D; \&quot;REMOVE_PNF\&quot;.  | [optional] 
**ModifyWanConnectionInfoData** | Pointer to [**[]ModifyWanConnectionInfoData**](ModifyWanConnectionInfoData.md) | Specifies the data to modify about WAN related connectivity information. It shall be present only if updateType &#x3D; \&quot;MODIFY_WAN_CONNECTION_INFO\&quot;.  | [optional] 
**UpdateTime** | Pointer to **time.Time** | Date-time stamp. Representation: String formatted according toas defined by the date-time production in IETF RFC 3339.  | [optional] 
**CreateSnapshotData** | Pointer to [**CreateVnfSnapshotData**](CreateVnfSnapshotData.md) |  | [optional] 
**RevertVnfToSnapshotData** | Pointer to [**RevertVnfToSnapshotData**](RevertVnfToSnapshotData.md) |  | [optional] 
**DeleteVnfSnapshotData** | Pointer to [**DeleteVnfSnapshotData**](DeleteVnfSnapshotData.md) |  | [optional] 
**AddNsVirtualLinkData** | Pointer to [**[]AddNsVirtualLinkData**](AddNsVirtualLinkData.md) | Specify data to be used to create a new NsVirtualLink instance. This parameter shall be present only if updateType &#x3D; “CREATE_NS_VIRTUAL_LINK”.  | [optional] 
**DeleteNsVirtualLinkId** | Pointer to **[]string** | Identify an existing NsVirtualLink instance to be deleted. The parameter shall be present only if updateType &#x3D; “DELETE_NS_VIRTUAL_LINK”.  | [optional] 

## Methods

### NewUpdateNsRequest

`func NewUpdateNsRequest(updateType string, ) *UpdateNsRequest`

NewUpdateNsRequest instantiates a new UpdateNsRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUpdateNsRequestWithDefaults

`func NewUpdateNsRequestWithDefaults() *UpdateNsRequest`

NewUpdateNsRequestWithDefaults instantiates a new UpdateNsRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetUpdateType

`func (o *UpdateNsRequest) GetUpdateType() string`

GetUpdateType returns the UpdateType field if non-nil, zero value otherwise.

### GetUpdateTypeOk

`func (o *UpdateNsRequest) GetUpdateTypeOk() (*string, bool)`

GetUpdateTypeOk returns a tuple with the UpdateType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdateType

`func (o *UpdateNsRequest) SetUpdateType(v string)`

SetUpdateType sets UpdateType field to given value.


### GetAddVnfIstance

`func (o *UpdateNsRequest) GetAddVnfIstance() []VnfInstanceData`

GetAddVnfIstance returns the AddVnfIstance field if non-nil, zero value otherwise.

### GetAddVnfIstanceOk

`func (o *UpdateNsRequest) GetAddVnfIstanceOk() (*[]VnfInstanceData, bool)`

GetAddVnfIstanceOk returns a tuple with the AddVnfIstance field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddVnfIstance

`func (o *UpdateNsRequest) SetAddVnfIstance(v []VnfInstanceData)`

SetAddVnfIstance sets AddVnfIstance field to given value.

### HasAddVnfIstance

`func (o *UpdateNsRequest) HasAddVnfIstance() bool`

HasAddVnfIstance returns a boolean if a field has been set.

### GetRemoveVnfInstanceId

`func (o *UpdateNsRequest) GetRemoveVnfInstanceId() []string`

GetRemoveVnfInstanceId returns the RemoveVnfInstanceId field if non-nil, zero value otherwise.

### GetRemoveVnfInstanceIdOk

`func (o *UpdateNsRequest) GetRemoveVnfInstanceIdOk() (*[]string, bool)`

GetRemoveVnfInstanceIdOk returns a tuple with the RemoveVnfInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRemoveVnfInstanceId

`func (o *UpdateNsRequest) SetRemoveVnfInstanceId(v []string)`

SetRemoveVnfInstanceId sets RemoveVnfInstanceId field to given value.

### HasRemoveVnfInstanceId

`func (o *UpdateNsRequest) HasRemoveVnfInstanceId() bool`

HasRemoveVnfInstanceId returns a boolean if a field has been set.

### GetInstantiateVnfData

`func (o *UpdateNsRequest) GetInstantiateVnfData() []InstantiateVnfData`

GetInstantiateVnfData returns the InstantiateVnfData field if non-nil, zero value otherwise.

### GetInstantiateVnfDataOk

`func (o *UpdateNsRequest) GetInstantiateVnfDataOk() (*[]InstantiateVnfData, bool)`

GetInstantiateVnfDataOk returns a tuple with the InstantiateVnfData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInstantiateVnfData

`func (o *UpdateNsRequest) SetInstantiateVnfData(v []InstantiateVnfData)`

SetInstantiateVnfData sets InstantiateVnfData field to given value.

### HasInstantiateVnfData

`func (o *UpdateNsRequest) HasInstantiateVnfData() bool`

HasInstantiateVnfData returns a boolean if a field has been set.

### GetTerminateVnfData

`func (o *UpdateNsRequest) GetTerminateVnfData() []TerminateVnfData`

GetTerminateVnfData returns the TerminateVnfData field if non-nil, zero value otherwise.

### GetTerminateVnfDataOk

`func (o *UpdateNsRequest) GetTerminateVnfDataOk() (*[]TerminateVnfData, bool)`

GetTerminateVnfDataOk returns a tuple with the TerminateVnfData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTerminateVnfData

`func (o *UpdateNsRequest) SetTerminateVnfData(v []TerminateVnfData)`

SetTerminateVnfData sets TerminateVnfData field to given value.

### HasTerminateVnfData

`func (o *UpdateNsRequest) HasTerminateVnfData() bool`

HasTerminateVnfData returns a boolean if a field has been set.

### GetChangeVnfFlavourData

`func (o *UpdateNsRequest) GetChangeVnfFlavourData() []ChangeVnfFlavourData`

GetChangeVnfFlavourData returns the ChangeVnfFlavourData field if non-nil, zero value otherwise.

### GetChangeVnfFlavourDataOk

`func (o *UpdateNsRequest) GetChangeVnfFlavourDataOk() (*[]ChangeVnfFlavourData, bool)`

GetChangeVnfFlavourDataOk returns a tuple with the ChangeVnfFlavourData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChangeVnfFlavourData

`func (o *UpdateNsRequest) SetChangeVnfFlavourData(v []ChangeVnfFlavourData)`

SetChangeVnfFlavourData sets ChangeVnfFlavourData field to given value.

### HasChangeVnfFlavourData

`func (o *UpdateNsRequest) HasChangeVnfFlavourData() bool`

HasChangeVnfFlavourData returns a boolean if a field has been set.

### GetOperateVnfData

`func (o *UpdateNsRequest) GetOperateVnfData() []OperateVnfData`

GetOperateVnfData returns the OperateVnfData field if non-nil, zero value otherwise.

### GetOperateVnfDataOk

`func (o *UpdateNsRequest) GetOperateVnfDataOk() (*[]OperateVnfData, bool)`

GetOperateVnfDataOk returns a tuple with the OperateVnfData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOperateVnfData

`func (o *UpdateNsRequest) SetOperateVnfData(v []OperateVnfData)`

SetOperateVnfData sets OperateVnfData field to given value.

### HasOperateVnfData

`func (o *UpdateNsRequest) HasOperateVnfData() bool`

HasOperateVnfData returns a boolean if a field has been set.

### GetModifyVnfInfoData

`func (o *UpdateNsRequest) GetModifyVnfInfoData() []ModifyVnfInfoData`

GetModifyVnfInfoData returns the ModifyVnfInfoData field if non-nil, zero value otherwise.

### GetModifyVnfInfoDataOk

`func (o *UpdateNsRequest) GetModifyVnfInfoDataOk() (*[]ModifyVnfInfoData, bool)`

GetModifyVnfInfoDataOk returns a tuple with the ModifyVnfInfoData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetModifyVnfInfoData

`func (o *UpdateNsRequest) SetModifyVnfInfoData(v []ModifyVnfInfoData)`

SetModifyVnfInfoData sets ModifyVnfInfoData field to given value.

### HasModifyVnfInfoData

`func (o *UpdateNsRequest) HasModifyVnfInfoData() bool`

HasModifyVnfInfoData returns a boolean if a field has been set.

### GetChangeExtVnfConnectivityData

`func (o *UpdateNsRequest) GetChangeExtVnfConnectivityData() []ChangeExtVnfConnectivityData`

GetChangeExtVnfConnectivityData returns the ChangeExtVnfConnectivityData field if non-nil, zero value otherwise.

### GetChangeExtVnfConnectivityDataOk

`func (o *UpdateNsRequest) GetChangeExtVnfConnectivityDataOk() (*[]ChangeExtVnfConnectivityData, bool)`

GetChangeExtVnfConnectivityDataOk returns a tuple with the ChangeExtVnfConnectivityData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChangeExtVnfConnectivityData

`func (o *UpdateNsRequest) SetChangeExtVnfConnectivityData(v []ChangeExtVnfConnectivityData)`

SetChangeExtVnfConnectivityData sets ChangeExtVnfConnectivityData field to given value.

### HasChangeExtVnfConnectivityData

`func (o *UpdateNsRequest) HasChangeExtVnfConnectivityData() bool`

HasChangeExtVnfConnectivityData returns a boolean if a field has been set.

### GetChangeVnfPackageData

`func (o *UpdateNsRequest) GetChangeVnfPackageData() []ChangeVnfPackageData`

GetChangeVnfPackageData returns the ChangeVnfPackageData field if non-nil, zero value otherwise.

### GetChangeVnfPackageDataOk

`func (o *UpdateNsRequest) GetChangeVnfPackageDataOk() (*[]ChangeVnfPackageData, bool)`

GetChangeVnfPackageDataOk returns a tuple with the ChangeVnfPackageData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChangeVnfPackageData

`func (o *UpdateNsRequest) SetChangeVnfPackageData(v []ChangeVnfPackageData)`

SetChangeVnfPackageData sets ChangeVnfPackageData field to given value.

### HasChangeVnfPackageData

`func (o *UpdateNsRequest) HasChangeVnfPackageData() bool`

HasChangeVnfPackageData returns a boolean if a field has been set.

### GetAddSap

`func (o *UpdateNsRequest) GetAddSap() []SapData`

GetAddSap returns the AddSap field if non-nil, zero value otherwise.

### GetAddSapOk

`func (o *UpdateNsRequest) GetAddSapOk() (*[]SapData, bool)`

GetAddSapOk returns a tuple with the AddSap field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddSap

`func (o *UpdateNsRequest) SetAddSap(v []SapData)`

SetAddSap sets AddSap field to given value.

### HasAddSap

`func (o *UpdateNsRequest) HasAddSap() bool`

HasAddSap returns a boolean if a field has been set.

### GetRemoveSapId

`func (o *UpdateNsRequest) GetRemoveSapId() []string`

GetRemoveSapId returns the RemoveSapId field if non-nil, zero value otherwise.

### GetRemoveSapIdOk

`func (o *UpdateNsRequest) GetRemoveSapIdOk() (*[]string, bool)`

GetRemoveSapIdOk returns a tuple with the RemoveSapId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRemoveSapId

`func (o *UpdateNsRequest) SetRemoveSapId(v []string)`

SetRemoveSapId sets RemoveSapId field to given value.

### HasRemoveSapId

`func (o *UpdateNsRequest) HasRemoveSapId() bool`

HasRemoveSapId returns a boolean if a field has been set.

### GetAddNestedNsData

`func (o *UpdateNsRequest) GetAddNestedNsData() []NestedNsInstanceData`

GetAddNestedNsData returns the AddNestedNsData field if non-nil, zero value otherwise.

### GetAddNestedNsDataOk

`func (o *UpdateNsRequest) GetAddNestedNsDataOk() (*[]NestedNsInstanceData, bool)`

GetAddNestedNsDataOk returns a tuple with the AddNestedNsData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddNestedNsData

`func (o *UpdateNsRequest) SetAddNestedNsData(v []NestedNsInstanceData)`

SetAddNestedNsData sets AddNestedNsData field to given value.

### HasAddNestedNsData

`func (o *UpdateNsRequest) HasAddNestedNsData() bool`

HasAddNestedNsData returns a boolean if a field has been set.

### GetRemoveNestedNsId

`func (o *UpdateNsRequest) GetRemoveNestedNsId() []string`

GetRemoveNestedNsId returns the RemoveNestedNsId field if non-nil, zero value otherwise.

### GetRemoveNestedNsIdOk

`func (o *UpdateNsRequest) GetRemoveNestedNsIdOk() (*[]string, bool)`

GetRemoveNestedNsIdOk returns a tuple with the RemoveNestedNsId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRemoveNestedNsId

`func (o *UpdateNsRequest) SetRemoveNestedNsId(v []string)`

SetRemoveNestedNsId sets RemoveNestedNsId field to given value.

### HasRemoveNestedNsId

`func (o *UpdateNsRequest) HasRemoveNestedNsId() bool`

HasRemoveNestedNsId returns a boolean if a field has been set.

### GetAssocNewNsdVersionData

`func (o *UpdateNsRequest) GetAssocNewNsdVersionData() AssocNewNsdVersionData`

GetAssocNewNsdVersionData returns the AssocNewNsdVersionData field if non-nil, zero value otherwise.

### GetAssocNewNsdVersionDataOk

`func (o *UpdateNsRequest) GetAssocNewNsdVersionDataOk() (*AssocNewNsdVersionData, bool)`

GetAssocNewNsdVersionDataOk returns a tuple with the AssocNewNsdVersionData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAssocNewNsdVersionData

`func (o *UpdateNsRequest) SetAssocNewNsdVersionData(v AssocNewNsdVersionData)`

SetAssocNewNsdVersionData sets AssocNewNsdVersionData field to given value.

### HasAssocNewNsdVersionData

`func (o *UpdateNsRequest) HasAssocNewNsdVersionData() bool`

HasAssocNewNsdVersionData returns a boolean if a field has been set.

### GetMoveVnfInstanceData

`func (o *UpdateNsRequest) GetMoveVnfInstanceData() []MoveVnfInstanceData`

GetMoveVnfInstanceData returns the MoveVnfInstanceData field if non-nil, zero value otherwise.

### GetMoveVnfInstanceDataOk

`func (o *UpdateNsRequest) GetMoveVnfInstanceDataOk() (*[]MoveVnfInstanceData, bool)`

GetMoveVnfInstanceDataOk returns a tuple with the MoveVnfInstanceData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMoveVnfInstanceData

`func (o *UpdateNsRequest) SetMoveVnfInstanceData(v []MoveVnfInstanceData)`

SetMoveVnfInstanceData sets MoveVnfInstanceData field to given value.

### HasMoveVnfInstanceData

`func (o *UpdateNsRequest) HasMoveVnfInstanceData() bool`

HasMoveVnfInstanceData returns a boolean if a field has been set.

### GetAddVnffg

`func (o *UpdateNsRequest) GetAddVnffg() []AddVnffgData`

GetAddVnffg returns the AddVnffg field if non-nil, zero value otherwise.

### GetAddVnffgOk

`func (o *UpdateNsRequest) GetAddVnffgOk() (*[]AddVnffgData, bool)`

GetAddVnffgOk returns a tuple with the AddVnffg field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddVnffg

`func (o *UpdateNsRequest) SetAddVnffg(v []AddVnffgData)`

SetAddVnffg sets AddVnffg field to given value.

### HasAddVnffg

`func (o *UpdateNsRequest) HasAddVnffg() bool`

HasAddVnffg returns a boolean if a field has been set.

### GetRemoveVnffgId

`func (o *UpdateNsRequest) GetRemoveVnffgId() []string`

GetRemoveVnffgId returns the RemoveVnffgId field if non-nil, zero value otherwise.

### GetRemoveVnffgIdOk

`func (o *UpdateNsRequest) GetRemoveVnffgIdOk() (*[]string, bool)`

GetRemoveVnffgIdOk returns a tuple with the RemoveVnffgId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRemoveVnffgId

`func (o *UpdateNsRequest) SetRemoveVnffgId(v []string)`

SetRemoveVnffgId sets RemoveVnffgId field to given value.

### HasRemoveVnffgId

`func (o *UpdateNsRequest) HasRemoveVnffgId() bool`

HasRemoveVnffgId returns a boolean if a field has been set.

### GetUpdateVnffg

`func (o *UpdateNsRequest) GetUpdateVnffg() []UpdateVnffgData`

GetUpdateVnffg returns the UpdateVnffg field if non-nil, zero value otherwise.

### GetUpdateVnffgOk

`func (o *UpdateNsRequest) GetUpdateVnffgOk() (*[]UpdateVnffgData, bool)`

GetUpdateVnffgOk returns a tuple with the UpdateVnffg field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdateVnffg

`func (o *UpdateNsRequest) SetUpdateVnffg(v []UpdateVnffgData)`

SetUpdateVnffg sets UpdateVnffg field to given value.

### HasUpdateVnffg

`func (o *UpdateNsRequest) HasUpdateVnffg() bool`

HasUpdateVnffg returns a boolean if a field has been set.

### GetChangeNsFlavourData

`func (o *UpdateNsRequest) GetChangeNsFlavourData() ChangeNsFlavourData`

GetChangeNsFlavourData returns the ChangeNsFlavourData field if non-nil, zero value otherwise.

### GetChangeNsFlavourDataOk

`func (o *UpdateNsRequest) GetChangeNsFlavourDataOk() (*ChangeNsFlavourData, bool)`

GetChangeNsFlavourDataOk returns a tuple with the ChangeNsFlavourData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChangeNsFlavourData

`func (o *UpdateNsRequest) SetChangeNsFlavourData(v ChangeNsFlavourData)`

SetChangeNsFlavourData sets ChangeNsFlavourData field to given value.

### HasChangeNsFlavourData

`func (o *UpdateNsRequest) HasChangeNsFlavourData() bool`

HasChangeNsFlavourData returns a boolean if a field has been set.

### GetAddPnfData

`func (o *UpdateNsRequest) GetAddPnfData() []AddPnfData`

GetAddPnfData returns the AddPnfData field if non-nil, zero value otherwise.

### GetAddPnfDataOk

`func (o *UpdateNsRequest) GetAddPnfDataOk() (*[]AddPnfData, bool)`

GetAddPnfDataOk returns a tuple with the AddPnfData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddPnfData

`func (o *UpdateNsRequest) SetAddPnfData(v []AddPnfData)`

SetAddPnfData sets AddPnfData field to given value.

### HasAddPnfData

`func (o *UpdateNsRequest) HasAddPnfData() bool`

HasAddPnfData returns a boolean if a field has been set.

### GetModifyPnfData

`func (o *UpdateNsRequest) GetModifyPnfData() []ModifyPnfData`

GetModifyPnfData returns the ModifyPnfData field if non-nil, zero value otherwise.

### GetModifyPnfDataOk

`func (o *UpdateNsRequest) GetModifyPnfDataOk() (*[]ModifyPnfData, bool)`

GetModifyPnfDataOk returns a tuple with the ModifyPnfData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetModifyPnfData

`func (o *UpdateNsRequest) SetModifyPnfData(v []ModifyPnfData)`

SetModifyPnfData sets ModifyPnfData field to given value.

### HasModifyPnfData

`func (o *UpdateNsRequest) HasModifyPnfData() bool`

HasModifyPnfData returns a boolean if a field has been set.

### GetRemovePnfId

`func (o *UpdateNsRequest) GetRemovePnfId() []string`

GetRemovePnfId returns the RemovePnfId field if non-nil, zero value otherwise.

### GetRemovePnfIdOk

`func (o *UpdateNsRequest) GetRemovePnfIdOk() (*[]string, bool)`

GetRemovePnfIdOk returns a tuple with the RemovePnfId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRemovePnfId

`func (o *UpdateNsRequest) SetRemovePnfId(v []string)`

SetRemovePnfId sets RemovePnfId field to given value.

### HasRemovePnfId

`func (o *UpdateNsRequest) HasRemovePnfId() bool`

HasRemovePnfId returns a boolean if a field has been set.

### GetModifyWanConnectionInfoData

`func (o *UpdateNsRequest) GetModifyWanConnectionInfoData() []ModifyWanConnectionInfoData`

GetModifyWanConnectionInfoData returns the ModifyWanConnectionInfoData field if non-nil, zero value otherwise.

### GetModifyWanConnectionInfoDataOk

`func (o *UpdateNsRequest) GetModifyWanConnectionInfoDataOk() (*[]ModifyWanConnectionInfoData, bool)`

GetModifyWanConnectionInfoDataOk returns a tuple with the ModifyWanConnectionInfoData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetModifyWanConnectionInfoData

`func (o *UpdateNsRequest) SetModifyWanConnectionInfoData(v []ModifyWanConnectionInfoData)`

SetModifyWanConnectionInfoData sets ModifyWanConnectionInfoData field to given value.

### HasModifyWanConnectionInfoData

`func (o *UpdateNsRequest) HasModifyWanConnectionInfoData() bool`

HasModifyWanConnectionInfoData returns a boolean if a field has been set.

### GetUpdateTime

`func (o *UpdateNsRequest) GetUpdateTime() time.Time`

GetUpdateTime returns the UpdateTime field if non-nil, zero value otherwise.

### GetUpdateTimeOk

`func (o *UpdateNsRequest) GetUpdateTimeOk() (*time.Time, bool)`

GetUpdateTimeOk returns a tuple with the UpdateTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUpdateTime

`func (o *UpdateNsRequest) SetUpdateTime(v time.Time)`

SetUpdateTime sets UpdateTime field to given value.

### HasUpdateTime

`func (o *UpdateNsRequest) HasUpdateTime() bool`

HasUpdateTime returns a boolean if a field has been set.

### GetCreateSnapshotData

`func (o *UpdateNsRequest) GetCreateSnapshotData() CreateVnfSnapshotData`

GetCreateSnapshotData returns the CreateSnapshotData field if non-nil, zero value otherwise.

### GetCreateSnapshotDataOk

`func (o *UpdateNsRequest) GetCreateSnapshotDataOk() (*CreateVnfSnapshotData, bool)`

GetCreateSnapshotDataOk returns a tuple with the CreateSnapshotData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreateSnapshotData

`func (o *UpdateNsRequest) SetCreateSnapshotData(v CreateVnfSnapshotData)`

SetCreateSnapshotData sets CreateSnapshotData field to given value.

### HasCreateSnapshotData

`func (o *UpdateNsRequest) HasCreateSnapshotData() bool`

HasCreateSnapshotData returns a boolean if a field has been set.

### GetRevertVnfToSnapshotData

`func (o *UpdateNsRequest) GetRevertVnfToSnapshotData() RevertVnfToSnapshotData`

GetRevertVnfToSnapshotData returns the RevertVnfToSnapshotData field if non-nil, zero value otherwise.

### GetRevertVnfToSnapshotDataOk

`func (o *UpdateNsRequest) GetRevertVnfToSnapshotDataOk() (*RevertVnfToSnapshotData, bool)`

GetRevertVnfToSnapshotDataOk returns a tuple with the RevertVnfToSnapshotData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRevertVnfToSnapshotData

`func (o *UpdateNsRequest) SetRevertVnfToSnapshotData(v RevertVnfToSnapshotData)`

SetRevertVnfToSnapshotData sets RevertVnfToSnapshotData field to given value.

### HasRevertVnfToSnapshotData

`func (o *UpdateNsRequest) HasRevertVnfToSnapshotData() bool`

HasRevertVnfToSnapshotData returns a boolean if a field has been set.

### GetDeleteVnfSnapshotData

`func (o *UpdateNsRequest) GetDeleteVnfSnapshotData() DeleteVnfSnapshotData`

GetDeleteVnfSnapshotData returns the DeleteVnfSnapshotData field if non-nil, zero value otherwise.

### GetDeleteVnfSnapshotDataOk

`func (o *UpdateNsRequest) GetDeleteVnfSnapshotDataOk() (*DeleteVnfSnapshotData, bool)`

GetDeleteVnfSnapshotDataOk returns a tuple with the DeleteVnfSnapshotData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDeleteVnfSnapshotData

`func (o *UpdateNsRequest) SetDeleteVnfSnapshotData(v DeleteVnfSnapshotData)`

SetDeleteVnfSnapshotData sets DeleteVnfSnapshotData field to given value.

### HasDeleteVnfSnapshotData

`func (o *UpdateNsRequest) HasDeleteVnfSnapshotData() bool`

HasDeleteVnfSnapshotData returns a boolean if a field has been set.

### GetAddNsVirtualLinkData

`func (o *UpdateNsRequest) GetAddNsVirtualLinkData() []AddNsVirtualLinkData`

GetAddNsVirtualLinkData returns the AddNsVirtualLinkData field if non-nil, zero value otherwise.

### GetAddNsVirtualLinkDataOk

`func (o *UpdateNsRequest) GetAddNsVirtualLinkDataOk() (*[]AddNsVirtualLinkData, bool)`

GetAddNsVirtualLinkDataOk returns a tuple with the AddNsVirtualLinkData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddNsVirtualLinkData

`func (o *UpdateNsRequest) SetAddNsVirtualLinkData(v []AddNsVirtualLinkData)`

SetAddNsVirtualLinkData sets AddNsVirtualLinkData field to given value.

### HasAddNsVirtualLinkData

`func (o *UpdateNsRequest) HasAddNsVirtualLinkData() bool`

HasAddNsVirtualLinkData returns a boolean if a field has been set.

### GetDeleteNsVirtualLinkId

`func (o *UpdateNsRequest) GetDeleteNsVirtualLinkId() []string`

GetDeleteNsVirtualLinkId returns the DeleteNsVirtualLinkId field if non-nil, zero value otherwise.

### GetDeleteNsVirtualLinkIdOk

`func (o *UpdateNsRequest) GetDeleteNsVirtualLinkIdOk() (*[]string, bool)`

GetDeleteNsVirtualLinkIdOk returns a tuple with the DeleteNsVirtualLinkId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDeleteNsVirtualLinkId

`func (o *UpdateNsRequest) SetDeleteNsVirtualLinkId(v []string)`

SetDeleteNsVirtualLinkId sets DeleteNsVirtualLinkId field to given value.

### HasDeleteNsVirtualLinkId

`func (o *UpdateNsRequest) HasDeleteNsVirtualLinkId() bool`

HasDeleteNsVirtualLinkId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


