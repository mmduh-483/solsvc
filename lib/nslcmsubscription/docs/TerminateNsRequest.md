# TerminateNsRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TerminationTime** | Pointer to **time.Time** | Date-time stamp. Representation: String formatted according toas defined by the date-time production in IETF RFC 3339.  | [optional] 
**TerminateNsData** | Pointer to [**TerminateNsData**](TerminateNsData.md) |  | [optional] 
**TerminateVnfData** | Pointer to [**[]time.Time**](time.Time.md) | Provides the information to terminate VNF instance(s). See note 1 and 2.  | [optional] 

## Methods

### NewTerminateNsRequest

`func NewTerminateNsRequest() *TerminateNsRequest`

NewTerminateNsRequest instantiates a new TerminateNsRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTerminateNsRequestWithDefaults

`func NewTerminateNsRequestWithDefaults() *TerminateNsRequest`

NewTerminateNsRequestWithDefaults instantiates a new TerminateNsRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTerminationTime

`func (o *TerminateNsRequest) GetTerminationTime() time.Time`

GetTerminationTime returns the TerminationTime field if non-nil, zero value otherwise.

### GetTerminationTimeOk

`func (o *TerminateNsRequest) GetTerminationTimeOk() (*time.Time, bool)`

GetTerminationTimeOk returns a tuple with the TerminationTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTerminationTime

`func (o *TerminateNsRequest) SetTerminationTime(v time.Time)`

SetTerminationTime sets TerminationTime field to given value.

### HasTerminationTime

`func (o *TerminateNsRequest) HasTerminationTime() bool`

HasTerminationTime returns a boolean if a field has been set.

### GetTerminateNsData

`func (o *TerminateNsRequest) GetTerminateNsData() TerminateNsData`

GetTerminateNsData returns the TerminateNsData field if non-nil, zero value otherwise.

### GetTerminateNsDataOk

`func (o *TerminateNsRequest) GetTerminateNsDataOk() (*TerminateNsData, bool)`

GetTerminateNsDataOk returns a tuple with the TerminateNsData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTerminateNsData

`func (o *TerminateNsRequest) SetTerminateNsData(v TerminateNsData)`

SetTerminateNsData sets TerminateNsData field to given value.

### HasTerminateNsData

`func (o *TerminateNsRequest) HasTerminateNsData() bool`

HasTerminateNsData returns a boolean if a field has been set.

### GetTerminateVnfData

`func (o *TerminateNsRequest) GetTerminateVnfData() []time.Time`

GetTerminateVnfData returns the TerminateVnfData field if non-nil, zero value otherwise.

### GetTerminateVnfDataOk

`func (o *TerminateNsRequest) GetTerminateVnfDataOk() (*[]time.Time, bool)`

GetTerminateVnfDataOk returns a tuple with the TerminateVnfData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTerminateVnfData

`func (o *TerminateNsRequest) SetTerminateVnfData(v []time.Time)`

SetTerminateVnfData sets TerminateVnfData field to given value.

### HasTerminateVnfData

`func (o *TerminateNsRequest) HasTerminateVnfData() bool`

HasTerminateVnfData returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


