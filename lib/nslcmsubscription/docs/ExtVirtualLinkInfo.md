# ExtVirtualLinkInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | An identifier with the intention of being globally unique.  | 
**ResourceHandle** | [**ResourceHandle**](ResourceHandle.md) |  | 
**ExtLinkPorts** | Pointer to [**[]ExtLinkPortInfo**](ExtLinkPortInfo.md) | Link ports of this VL.  | [optional] 
**CurrentVnfExtCpData** | Pointer to [**VnfExtCpData**](VnfExtCpData.md) |  | [optional] 

## Methods

### NewExtVirtualLinkInfo

`func NewExtVirtualLinkInfo(id string, resourceHandle ResourceHandle, ) *ExtVirtualLinkInfo`

NewExtVirtualLinkInfo instantiates a new ExtVirtualLinkInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewExtVirtualLinkInfoWithDefaults

`func NewExtVirtualLinkInfoWithDefaults() *ExtVirtualLinkInfo`

NewExtVirtualLinkInfoWithDefaults instantiates a new ExtVirtualLinkInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *ExtVirtualLinkInfo) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *ExtVirtualLinkInfo) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *ExtVirtualLinkInfo) SetId(v string)`

SetId sets Id field to given value.


### GetResourceHandle

`func (o *ExtVirtualLinkInfo) GetResourceHandle() ResourceHandle`

GetResourceHandle returns the ResourceHandle field if non-nil, zero value otherwise.

### GetResourceHandleOk

`func (o *ExtVirtualLinkInfo) GetResourceHandleOk() (*ResourceHandle, bool)`

GetResourceHandleOk returns a tuple with the ResourceHandle field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResourceHandle

`func (o *ExtVirtualLinkInfo) SetResourceHandle(v ResourceHandle)`

SetResourceHandle sets ResourceHandle field to given value.


### GetExtLinkPorts

`func (o *ExtVirtualLinkInfo) GetExtLinkPorts() []ExtLinkPortInfo`

GetExtLinkPorts returns the ExtLinkPorts field if non-nil, zero value otherwise.

### GetExtLinkPortsOk

`func (o *ExtVirtualLinkInfo) GetExtLinkPortsOk() (*[]ExtLinkPortInfo, bool)`

GetExtLinkPortsOk returns a tuple with the ExtLinkPorts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtLinkPorts

`func (o *ExtVirtualLinkInfo) SetExtLinkPorts(v []ExtLinkPortInfo)`

SetExtLinkPorts sets ExtLinkPorts field to given value.

### HasExtLinkPorts

`func (o *ExtVirtualLinkInfo) HasExtLinkPorts() bool`

HasExtLinkPorts returns a boolean if a field has been set.

### GetCurrentVnfExtCpData

`func (o *ExtVirtualLinkInfo) GetCurrentVnfExtCpData() VnfExtCpData`

GetCurrentVnfExtCpData returns the CurrentVnfExtCpData field if non-nil, zero value otherwise.

### GetCurrentVnfExtCpDataOk

`func (o *ExtVirtualLinkInfo) GetCurrentVnfExtCpDataOk() (*VnfExtCpData, bool)`

GetCurrentVnfExtCpDataOk returns a tuple with the CurrentVnfExtCpData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCurrentVnfExtCpData

`func (o *ExtVirtualLinkInfo) SetCurrentVnfExtCpData(v VnfExtCpData)`

SetCurrentVnfExtCpData sets CurrentVnfExtCpData field to given value.

### HasCurrentVnfExtCpData

`func (o *ExtVirtualLinkInfo) HasCurrentVnfExtCpData() bool`

HasCurrentVnfExtCpData returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


