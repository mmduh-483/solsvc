# WanConnectionProtocolInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MscsInfo** | Pointer to [**MscsInfo**](MscsInfo.md) |  | [optional] 
**ConnectivityServiceEndpoints** | Pointer to [**[]ConnectivityServiceEndpointInfo**](ConnectivityServiceEndpointInfo.md) | Configuration information about the network resources in the NFVI-PoP and their connectivity to the WAN.  | [optional] 

## Methods

### NewWanConnectionProtocolInfo

`func NewWanConnectionProtocolInfo() *WanConnectionProtocolInfo`

NewWanConnectionProtocolInfo instantiates a new WanConnectionProtocolInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWanConnectionProtocolInfoWithDefaults

`func NewWanConnectionProtocolInfoWithDefaults() *WanConnectionProtocolInfo`

NewWanConnectionProtocolInfoWithDefaults instantiates a new WanConnectionProtocolInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMscsInfo

`func (o *WanConnectionProtocolInfo) GetMscsInfo() MscsInfo`

GetMscsInfo returns the MscsInfo field if non-nil, zero value otherwise.

### GetMscsInfoOk

`func (o *WanConnectionProtocolInfo) GetMscsInfoOk() (*MscsInfo, bool)`

GetMscsInfoOk returns a tuple with the MscsInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMscsInfo

`func (o *WanConnectionProtocolInfo) SetMscsInfo(v MscsInfo)`

SetMscsInfo sets MscsInfo field to given value.

### HasMscsInfo

`func (o *WanConnectionProtocolInfo) HasMscsInfo() bool`

HasMscsInfo returns a boolean if a field has been set.

### GetConnectivityServiceEndpoints

`func (o *WanConnectionProtocolInfo) GetConnectivityServiceEndpoints() []ConnectivityServiceEndpointInfo`

GetConnectivityServiceEndpoints returns the ConnectivityServiceEndpoints field if non-nil, zero value otherwise.

### GetConnectivityServiceEndpointsOk

`func (o *WanConnectionProtocolInfo) GetConnectivityServiceEndpointsOk() (*[]ConnectivityServiceEndpointInfo, bool)`

GetConnectivityServiceEndpointsOk returns a tuple with the ConnectivityServiceEndpoints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConnectivityServiceEndpoints

`func (o *WanConnectionProtocolInfo) SetConnectivityServiceEndpoints(v []ConnectivityServiceEndpointInfo)`

SetConnectivityServiceEndpoints sets ConnectivityServiceEndpoints field to given value.

### HasConnectivityServiceEndpoints

`func (o *WanConnectionProtocolInfo) HasConnectivityServiceEndpoints() bool`

HasConnectivityServiceEndpoints returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


