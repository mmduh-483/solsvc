# SiteToWanLayer3ProtocolDataVirtualRoutingAndForwarding

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VrfName** | **string** | Name (or identifier) of the VRF instance.  | 

## Methods

### NewSiteToWanLayer3ProtocolDataVirtualRoutingAndForwarding

`func NewSiteToWanLayer3ProtocolDataVirtualRoutingAndForwarding(vrfName string, ) *SiteToWanLayer3ProtocolDataVirtualRoutingAndForwarding`

NewSiteToWanLayer3ProtocolDataVirtualRoutingAndForwarding instantiates a new SiteToWanLayer3ProtocolDataVirtualRoutingAndForwarding object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSiteToWanLayer3ProtocolDataVirtualRoutingAndForwardingWithDefaults

`func NewSiteToWanLayer3ProtocolDataVirtualRoutingAndForwardingWithDefaults() *SiteToWanLayer3ProtocolDataVirtualRoutingAndForwarding`

NewSiteToWanLayer3ProtocolDataVirtualRoutingAndForwardingWithDefaults instantiates a new SiteToWanLayer3ProtocolDataVirtualRoutingAndForwarding object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVrfName

`func (o *SiteToWanLayer3ProtocolDataVirtualRoutingAndForwarding) GetVrfName() string`

GetVrfName returns the VrfName field if non-nil, zero value otherwise.

### GetVrfNameOk

`func (o *SiteToWanLayer3ProtocolDataVirtualRoutingAndForwarding) GetVrfNameOk() (*string, bool)`

GetVrfNameOk returns a tuple with the VrfName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVrfName

`func (o *SiteToWanLayer3ProtocolDataVirtualRoutingAndForwarding) SetVrfName(v string)`

SetVrfName sets VrfName field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


