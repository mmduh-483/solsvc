# CancelMode

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CancelMode** | [**CancelModeType**](CancelModeType.md) |  | 

## Methods

### NewCancelMode

`func NewCancelMode(cancelMode CancelModeType, ) *CancelMode`

NewCancelMode instantiates a new CancelMode object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCancelModeWithDefaults

`func NewCancelModeWithDefaults() *CancelMode`

NewCancelModeWithDefaults instantiates a new CancelMode object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCancelMode

`func (o *CancelMode) GetCancelMode() CancelModeType`

GetCancelMode returns the CancelMode field if non-nil, zero value otherwise.

### GetCancelModeOk

`func (o *CancelMode) GetCancelModeOk() (*CancelModeType, bool)`

GetCancelModeOk returns a tuple with the CancelMode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCancelMode

`func (o *CancelMode) SetCancelMode(v CancelModeType)`

SetCancelMode sets CancelMode field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


