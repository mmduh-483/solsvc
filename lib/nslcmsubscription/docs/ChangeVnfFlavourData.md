# ChangeVnfFlavourData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VnfInstanceId** | **string** | An identifier with the intention of being globally unique.  | 
**NewFlavourId** | **string** | Identifier of the VNF Virtual Link Descriptor (VLD) in the VNFD.  | 
**InstantiationLevelId** | Pointer to **string** | Identifier of the VNF Virtual Link Descriptor (VLD) in the VNFD.  | [optional] 
**ExtVirtualLinks** | Pointer to [**[]ExtVirtualLinkData**](ExtVirtualLinkData.md) | Information about external VLs to connect the VNF to.  | [optional] 
**ExtManagedVirtualLinks** | Pointer to [**[]ExtManagedVirtualLinkData**](ExtManagedVirtualLinkData.md) | information about internal VLs that are managed by NFVO. See note 1 and note 2.  | [optional] 
**AdditionalParams** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 
**Extensions** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 
**VnfConfigurableProperties** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 

## Methods

### NewChangeVnfFlavourData

`func NewChangeVnfFlavourData(vnfInstanceId string, newFlavourId string, ) *ChangeVnfFlavourData`

NewChangeVnfFlavourData instantiates a new ChangeVnfFlavourData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewChangeVnfFlavourDataWithDefaults

`func NewChangeVnfFlavourDataWithDefaults() *ChangeVnfFlavourData`

NewChangeVnfFlavourDataWithDefaults instantiates a new ChangeVnfFlavourData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVnfInstanceId

`func (o *ChangeVnfFlavourData) GetVnfInstanceId() string`

GetVnfInstanceId returns the VnfInstanceId field if non-nil, zero value otherwise.

### GetVnfInstanceIdOk

`func (o *ChangeVnfFlavourData) GetVnfInstanceIdOk() (*string, bool)`

GetVnfInstanceIdOk returns a tuple with the VnfInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceId

`func (o *ChangeVnfFlavourData) SetVnfInstanceId(v string)`

SetVnfInstanceId sets VnfInstanceId field to given value.


### GetNewFlavourId

`func (o *ChangeVnfFlavourData) GetNewFlavourId() string`

GetNewFlavourId returns the NewFlavourId field if non-nil, zero value otherwise.

### GetNewFlavourIdOk

`func (o *ChangeVnfFlavourData) GetNewFlavourIdOk() (*string, bool)`

GetNewFlavourIdOk returns a tuple with the NewFlavourId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNewFlavourId

`func (o *ChangeVnfFlavourData) SetNewFlavourId(v string)`

SetNewFlavourId sets NewFlavourId field to given value.


### GetInstantiationLevelId

`func (o *ChangeVnfFlavourData) GetInstantiationLevelId() string`

GetInstantiationLevelId returns the InstantiationLevelId field if non-nil, zero value otherwise.

### GetInstantiationLevelIdOk

`func (o *ChangeVnfFlavourData) GetInstantiationLevelIdOk() (*string, bool)`

GetInstantiationLevelIdOk returns a tuple with the InstantiationLevelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInstantiationLevelId

`func (o *ChangeVnfFlavourData) SetInstantiationLevelId(v string)`

SetInstantiationLevelId sets InstantiationLevelId field to given value.

### HasInstantiationLevelId

`func (o *ChangeVnfFlavourData) HasInstantiationLevelId() bool`

HasInstantiationLevelId returns a boolean if a field has been set.

### GetExtVirtualLinks

`func (o *ChangeVnfFlavourData) GetExtVirtualLinks() []ExtVirtualLinkData`

GetExtVirtualLinks returns the ExtVirtualLinks field if non-nil, zero value otherwise.

### GetExtVirtualLinksOk

`func (o *ChangeVnfFlavourData) GetExtVirtualLinksOk() (*[]ExtVirtualLinkData, bool)`

GetExtVirtualLinksOk returns a tuple with the ExtVirtualLinks field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtVirtualLinks

`func (o *ChangeVnfFlavourData) SetExtVirtualLinks(v []ExtVirtualLinkData)`

SetExtVirtualLinks sets ExtVirtualLinks field to given value.

### HasExtVirtualLinks

`func (o *ChangeVnfFlavourData) HasExtVirtualLinks() bool`

HasExtVirtualLinks returns a boolean if a field has been set.

### GetExtManagedVirtualLinks

`func (o *ChangeVnfFlavourData) GetExtManagedVirtualLinks() []ExtManagedVirtualLinkData`

GetExtManagedVirtualLinks returns the ExtManagedVirtualLinks field if non-nil, zero value otherwise.

### GetExtManagedVirtualLinksOk

`func (o *ChangeVnfFlavourData) GetExtManagedVirtualLinksOk() (*[]ExtManagedVirtualLinkData, bool)`

GetExtManagedVirtualLinksOk returns a tuple with the ExtManagedVirtualLinks field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtManagedVirtualLinks

`func (o *ChangeVnfFlavourData) SetExtManagedVirtualLinks(v []ExtManagedVirtualLinkData)`

SetExtManagedVirtualLinks sets ExtManagedVirtualLinks field to given value.

### HasExtManagedVirtualLinks

`func (o *ChangeVnfFlavourData) HasExtManagedVirtualLinks() bool`

HasExtManagedVirtualLinks returns a boolean if a field has been set.

### GetAdditionalParams

`func (o *ChangeVnfFlavourData) GetAdditionalParams() map[string]interface{}`

GetAdditionalParams returns the AdditionalParams field if non-nil, zero value otherwise.

### GetAdditionalParamsOk

`func (o *ChangeVnfFlavourData) GetAdditionalParamsOk() (*map[string]interface{}, bool)`

GetAdditionalParamsOk returns a tuple with the AdditionalParams field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdditionalParams

`func (o *ChangeVnfFlavourData) SetAdditionalParams(v map[string]interface{})`

SetAdditionalParams sets AdditionalParams field to given value.

### HasAdditionalParams

`func (o *ChangeVnfFlavourData) HasAdditionalParams() bool`

HasAdditionalParams returns a boolean if a field has been set.

### GetExtensions

`func (o *ChangeVnfFlavourData) GetExtensions() map[string]interface{}`

GetExtensions returns the Extensions field if non-nil, zero value otherwise.

### GetExtensionsOk

`func (o *ChangeVnfFlavourData) GetExtensionsOk() (*map[string]interface{}, bool)`

GetExtensionsOk returns a tuple with the Extensions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtensions

`func (o *ChangeVnfFlavourData) SetExtensions(v map[string]interface{})`

SetExtensions sets Extensions field to given value.

### HasExtensions

`func (o *ChangeVnfFlavourData) HasExtensions() bool`

HasExtensions returns a boolean if a field has been set.

### GetVnfConfigurableProperties

`func (o *ChangeVnfFlavourData) GetVnfConfigurableProperties() map[string]interface{}`

GetVnfConfigurableProperties returns the VnfConfigurableProperties field if non-nil, zero value otherwise.

### GetVnfConfigurablePropertiesOk

`func (o *ChangeVnfFlavourData) GetVnfConfigurablePropertiesOk() (*map[string]interface{}, bool)`

GetVnfConfigurablePropertiesOk returns a tuple with the VnfConfigurableProperties field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfConfigurableProperties

`func (o *ChangeVnfFlavourData) SetVnfConfigurableProperties(v map[string]interface{})`

SetVnfConfigurableProperties sets VnfConfigurableProperties field to given value.

### HasVnfConfigurableProperties

`func (o *ChangeVnfFlavourData) HasVnfConfigurableProperties() bool`

HasVnfConfigurableProperties returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


