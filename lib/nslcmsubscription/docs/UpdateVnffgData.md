# UpdateVnffgData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VnffgInfoId** | **string** | An identifier that is unique with respect to a NS. Representation: string of variable length.  | 
**Nfp** | Pointer to [**[]NfpData**](NfpData.md) | Indicate the desired new NFP(s) for a given VNFFG after the operations of addition/removal of NS components (e.g. VNFs, VLs, etc.) have been completed, or indicate the updated or newly created NFP classification and selection rule which applied to an existing NFP.  | [optional] 
**NfpInfoId** | Pointer to **[]string** | Identifier(s) of the NFP to be deleted from a given VNFFG.  | [optional] 

## Methods

### NewUpdateVnffgData

`func NewUpdateVnffgData(vnffgInfoId string, ) *UpdateVnffgData`

NewUpdateVnffgData instantiates a new UpdateVnffgData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUpdateVnffgDataWithDefaults

`func NewUpdateVnffgDataWithDefaults() *UpdateVnffgData`

NewUpdateVnffgDataWithDefaults instantiates a new UpdateVnffgData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVnffgInfoId

`func (o *UpdateVnffgData) GetVnffgInfoId() string`

GetVnffgInfoId returns the VnffgInfoId field if non-nil, zero value otherwise.

### GetVnffgInfoIdOk

`func (o *UpdateVnffgData) GetVnffgInfoIdOk() (*string, bool)`

GetVnffgInfoIdOk returns a tuple with the VnffgInfoId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnffgInfoId

`func (o *UpdateVnffgData) SetVnffgInfoId(v string)`

SetVnffgInfoId sets VnffgInfoId field to given value.


### GetNfp

`func (o *UpdateVnffgData) GetNfp() []NfpData`

GetNfp returns the Nfp field if non-nil, zero value otherwise.

### GetNfpOk

`func (o *UpdateVnffgData) GetNfpOk() (*[]NfpData, bool)`

GetNfpOk returns a tuple with the Nfp field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNfp

`func (o *UpdateVnffgData) SetNfp(v []NfpData)`

SetNfp sets Nfp field to given value.

### HasNfp

`func (o *UpdateVnffgData) HasNfp() bool`

HasNfp returns a boolean if a field has been set.

### GetNfpInfoId

`func (o *UpdateVnffgData) GetNfpInfoId() []string`

GetNfpInfoId returns the NfpInfoId field if non-nil, zero value otherwise.

### GetNfpInfoIdOk

`func (o *UpdateVnffgData) GetNfpInfoIdOk() (*[]string, bool)`

GetNfpInfoIdOk returns a tuple with the NfpInfoId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNfpInfoId

`func (o *UpdateVnffgData) SetNfpInfoId(v []string)`

SetNfpInfoId sets NfpInfoId field to given value.

### HasNfpInfoId

`func (o *UpdateVnffgData) HasNfpInfoId() bool`

HasNfpInfoId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


