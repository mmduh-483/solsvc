# SapData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**SapdId** | **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | 
**SapName** | **string** | Human readable name for the SAP.  | 
**Description** | **string** | Human readable description for the SAP.  | 
**SapProtocolData** | Pointer to [**[]CpProtocolData**](CpProtocolData.md) | Parameters for configuring the network protocols on the SAP.  | [optional] 

## Methods

### NewSapData

`func NewSapData(sapdId string, sapName string, description string, ) *SapData`

NewSapData instantiates a new SapData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSapDataWithDefaults

`func NewSapDataWithDefaults() *SapData`

NewSapDataWithDefaults instantiates a new SapData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSapdId

`func (o *SapData) GetSapdId() string`

GetSapdId returns the SapdId field if non-nil, zero value otherwise.

### GetSapdIdOk

`func (o *SapData) GetSapdIdOk() (*string, bool)`

GetSapdIdOk returns a tuple with the SapdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSapdId

`func (o *SapData) SetSapdId(v string)`

SetSapdId sets SapdId field to given value.


### GetSapName

`func (o *SapData) GetSapName() string`

GetSapName returns the SapName field if non-nil, zero value otherwise.

### GetSapNameOk

`func (o *SapData) GetSapNameOk() (*string, bool)`

GetSapNameOk returns a tuple with the SapName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSapName

`func (o *SapData) SetSapName(v string)`

SetSapName sets SapName field to given value.


### GetDescription

`func (o *SapData) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *SapData) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *SapData) SetDescription(v string)`

SetDescription sets Description field to given value.


### GetSapProtocolData

`func (o *SapData) GetSapProtocolData() []CpProtocolData`

GetSapProtocolData returns the SapProtocolData field if non-nil, zero value otherwise.

### GetSapProtocolDataOk

`func (o *SapData) GetSapProtocolDataOk() (*[]CpProtocolData, bool)`

GetSapProtocolDataOk returns a tuple with the SapProtocolData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSapProtocolData

`func (o *SapData) SetSapProtocolData(v []CpProtocolData)`

SetSapProtocolData sets SapProtocolData field to given value.

### HasSapProtocolData

`func (o *SapData) HasSapProtocolData() bool`

HasSapProtocolData returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


