# WanLayer2ProtocolData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MscsLayer2Protocol** | Pointer to **string** | Type of underlying connectivity service and protocol associated to the type of MSCS. Permitted values are as listed below and restricted by the type of MSCS: - EVPN_BGP_MPLS: as specified in IETF RFC 7432. - EVPN_VPWS: as specified in IETF RFC 8214. - VPLS_BGP: as specified in IETF RFC 4761 and IETF RFC 6624. - VPLS_LDP_L2TP: as specified in IETF RFC 4762 and IETF RFC 6074. - VPWS_LDP_L2TP: as specified in IETF RFC 6074.  | [optional] 
**IsSegmentPreservation** | **bool** | Indicates the requirement of whether to ensure network segment (e.g., VLAN id) preservation across the MSCS endpoints (i.e., from/to the NFVI-PoPs). If \&quot;TRUE\&quot;, segment identifiers shall be preserved, \&quot;FALSE\&quot; otherwise. Default value is \&quot;FALSE\&quot;.  | 
**IsSegmentCosPreservation** | **bool** | Indicates the requirement of whether to ensure network segment class of service preservation across the MSCS endpoints (i.e., from/to the NFVI-PoPs). If \&quot;TRUE\&quot;, segment class of service shall be preserved, \&quot;FALSE\&quot; otherwise. Default value is \&quot;FALSE\&quot;.  | 

## Methods

### NewWanLayer2ProtocolData

`func NewWanLayer2ProtocolData(isSegmentPreservation bool, isSegmentCosPreservation bool, ) *WanLayer2ProtocolData`

NewWanLayer2ProtocolData instantiates a new WanLayer2ProtocolData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWanLayer2ProtocolDataWithDefaults

`func NewWanLayer2ProtocolDataWithDefaults() *WanLayer2ProtocolData`

NewWanLayer2ProtocolDataWithDefaults instantiates a new WanLayer2ProtocolData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMscsLayer2Protocol

`func (o *WanLayer2ProtocolData) GetMscsLayer2Protocol() string`

GetMscsLayer2Protocol returns the MscsLayer2Protocol field if non-nil, zero value otherwise.

### GetMscsLayer2ProtocolOk

`func (o *WanLayer2ProtocolData) GetMscsLayer2ProtocolOk() (*string, bool)`

GetMscsLayer2ProtocolOk returns a tuple with the MscsLayer2Protocol field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMscsLayer2Protocol

`func (o *WanLayer2ProtocolData) SetMscsLayer2Protocol(v string)`

SetMscsLayer2Protocol sets MscsLayer2Protocol field to given value.

### HasMscsLayer2Protocol

`func (o *WanLayer2ProtocolData) HasMscsLayer2Protocol() bool`

HasMscsLayer2Protocol returns a boolean if a field has been set.

### GetIsSegmentPreservation

`func (o *WanLayer2ProtocolData) GetIsSegmentPreservation() bool`

GetIsSegmentPreservation returns the IsSegmentPreservation field if non-nil, zero value otherwise.

### GetIsSegmentPreservationOk

`func (o *WanLayer2ProtocolData) GetIsSegmentPreservationOk() (*bool, bool)`

GetIsSegmentPreservationOk returns a tuple with the IsSegmentPreservation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIsSegmentPreservation

`func (o *WanLayer2ProtocolData) SetIsSegmentPreservation(v bool)`

SetIsSegmentPreservation sets IsSegmentPreservation field to given value.


### GetIsSegmentCosPreservation

`func (o *WanLayer2ProtocolData) GetIsSegmentCosPreservation() bool`

GetIsSegmentCosPreservation returns the IsSegmentCosPreservation field if non-nil, zero value otherwise.

### GetIsSegmentCosPreservationOk

`func (o *WanLayer2ProtocolData) GetIsSegmentCosPreservationOk() (*bool, bool)`

GetIsSegmentCosPreservationOk returns a tuple with the IsSegmentCosPreservation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIsSegmentCosPreservation

`func (o *WanLayer2ProtocolData) SetIsSegmentCosPreservation(v bool)`

SetIsSegmentCosPreservation sets IsSegmentCosPreservation field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


