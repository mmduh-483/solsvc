# MscsConfigData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MscsType** | **string** | The type of connectivity that is requested to be provided to the virtualized networks in the NFVI-PoP and characterizes the connectivity service across the WAN. Permitted values: - L2 - L3  | 
**SiteAccessProtectionSchemes** | Pointer to [**[]MscsConfigDataSiteAccessProtectionSchemesInner**](MscsConfigDataSiteAccessProtectionSchemesInner.md) | Information to determine the proper MSCS endpoints configuration to fulfil certain resiliency/protection requirements, e.g., by considering certain availability and redundancy of connectivity service endpoints in between NFVI-PoP and WAN.  | [optional] 
**MtuMscs** | Pointer to **float32** | Maximum Transmission Unit (MTU) that can be forwarded over the MSCS (in bytes). Default value is \&quot;1500\&quot; (bytes).  | [optional] 
**WanLayer2ProtocolData** | Pointer to [**WanLayer2ProtocolData**](WanLayer2ProtocolData.md) |  | [optional] 
**WanLayer3ProtocolData** | Pointer to [**WanLayer3ProtocolData**](WanLayer3ProtocolData.md) |  | [optional] 

## Methods

### NewMscsConfigData

`func NewMscsConfigData(mscsType string, ) *MscsConfigData`

NewMscsConfigData instantiates a new MscsConfigData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMscsConfigDataWithDefaults

`func NewMscsConfigDataWithDefaults() *MscsConfigData`

NewMscsConfigDataWithDefaults instantiates a new MscsConfigData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMscsType

`func (o *MscsConfigData) GetMscsType() string`

GetMscsType returns the MscsType field if non-nil, zero value otherwise.

### GetMscsTypeOk

`func (o *MscsConfigData) GetMscsTypeOk() (*string, bool)`

GetMscsTypeOk returns a tuple with the MscsType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMscsType

`func (o *MscsConfigData) SetMscsType(v string)`

SetMscsType sets MscsType field to given value.


### GetSiteAccessProtectionSchemes

`func (o *MscsConfigData) GetSiteAccessProtectionSchemes() []MscsConfigDataSiteAccessProtectionSchemesInner`

GetSiteAccessProtectionSchemes returns the SiteAccessProtectionSchemes field if non-nil, zero value otherwise.

### GetSiteAccessProtectionSchemesOk

`func (o *MscsConfigData) GetSiteAccessProtectionSchemesOk() (*[]MscsConfigDataSiteAccessProtectionSchemesInner, bool)`

GetSiteAccessProtectionSchemesOk returns a tuple with the SiteAccessProtectionSchemes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSiteAccessProtectionSchemes

`func (o *MscsConfigData) SetSiteAccessProtectionSchemes(v []MscsConfigDataSiteAccessProtectionSchemesInner)`

SetSiteAccessProtectionSchemes sets SiteAccessProtectionSchemes field to given value.

### HasSiteAccessProtectionSchemes

`func (o *MscsConfigData) HasSiteAccessProtectionSchemes() bool`

HasSiteAccessProtectionSchemes returns a boolean if a field has been set.

### GetMtuMscs

`func (o *MscsConfigData) GetMtuMscs() float32`

GetMtuMscs returns the MtuMscs field if non-nil, zero value otherwise.

### GetMtuMscsOk

`func (o *MscsConfigData) GetMtuMscsOk() (*float32, bool)`

GetMtuMscsOk returns a tuple with the MtuMscs field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMtuMscs

`func (o *MscsConfigData) SetMtuMscs(v float32)`

SetMtuMscs sets MtuMscs field to given value.

### HasMtuMscs

`func (o *MscsConfigData) HasMtuMscs() bool`

HasMtuMscs returns a boolean if a field has been set.

### GetWanLayer2ProtocolData

`func (o *MscsConfigData) GetWanLayer2ProtocolData() WanLayer2ProtocolData`

GetWanLayer2ProtocolData returns the WanLayer2ProtocolData field if non-nil, zero value otherwise.

### GetWanLayer2ProtocolDataOk

`func (o *MscsConfigData) GetWanLayer2ProtocolDataOk() (*WanLayer2ProtocolData, bool)`

GetWanLayer2ProtocolDataOk returns a tuple with the WanLayer2ProtocolData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWanLayer2ProtocolData

`func (o *MscsConfigData) SetWanLayer2ProtocolData(v WanLayer2ProtocolData)`

SetWanLayer2ProtocolData sets WanLayer2ProtocolData field to given value.

### HasWanLayer2ProtocolData

`func (o *MscsConfigData) HasWanLayer2ProtocolData() bool`

HasWanLayer2ProtocolData returns a boolean if a field has been set.

### GetWanLayer3ProtocolData

`func (o *MscsConfigData) GetWanLayer3ProtocolData() WanLayer3ProtocolData`

GetWanLayer3ProtocolData returns the WanLayer3ProtocolData field if non-nil, zero value otherwise.

### GetWanLayer3ProtocolDataOk

`func (o *MscsConfigData) GetWanLayer3ProtocolDataOk() (*WanLayer3ProtocolData, bool)`

GetWanLayer3ProtocolDataOk returns a tuple with the WanLayer3ProtocolData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWanLayer3ProtocolData

`func (o *MscsConfigData) SetWanLayer3ProtocolData(v WanLayer3ProtocolData)`

SetWanLayer3ProtocolData sets WanLayer3ProtocolData field to given value.

### HasWanLayer3ProtocolData

`func (o *MscsConfigData) HasWanLayer3ProtocolData() bool`

HasWanLayer3ProtocolData returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


