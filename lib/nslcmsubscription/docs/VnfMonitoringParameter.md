# VnfMonitoringParameter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | 
**VnfdId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**Name** | Pointer to **string** | Human readable name of the monitoring parameter, as defined in the VNFD.  | [optional] 
**PerformanceMetric** | **string** | Performance metric that is monitored. This attribute shall contain the related  \&quot;Measurement Name\&quot; value as defined in clause 7.2 of ETSI GS NFV-IFA 027.  | 

## Methods

### NewVnfMonitoringParameter

`func NewVnfMonitoringParameter(id string, performanceMetric string, ) *VnfMonitoringParameter`

NewVnfMonitoringParameter instantiates a new VnfMonitoringParameter object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVnfMonitoringParameterWithDefaults

`func NewVnfMonitoringParameterWithDefaults() *VnfMonitoringParameter`

NewVnfMonitoringParameterWithDefaults instantiates a new VnfMonitoringParameter object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *VnfMonitoringParameter) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *VnfMonitoringParameter) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *VnfMonitoringParameter) SetId(v string)`

SetId sets Id field to given value.


### GetVnfdId

`func (o *VnfMonitoringParameter) GetVnfdId() string`

GetVnfdId returns the VnfdId field if non-nil, zero value otherwise.

### GetVnfdIdOk

`func (o *VnfMonitoringParameter) GetVnfdIdOk() (*string, bool)`

GetVnfdIdOk returns a tuple with the VnfdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfdId

`func (o *VnfMonitoringParameter) SetVnfdId(v string)`

SetVnfdId sets VnfdId field to given value.

### HasVnfdId

`func (o *VnfMonitoringParameter) HasVnfdId() bool`

HasVnfdId returns a boolean if a field has been set.

### GetName

`func (o *VnfMonitoringParameter) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *VnfMonitoringParameter) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *VnfMonitoringParameter) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *VnfMonitoringParameter) HasName() bool`

HasName returns a boolean if a field has been set.

### GetPerformanceMetric

`func (o *VnfMonitoringParameter) GetPerformanceMetric() string`

GetPerformanceMetric returns the PerformanceMetric field if non-nil, zero value otherwise.

### GetPerformanceMetricOk

`func (o *VnfMonitoringParameter) GetPerformanceMetricOk() (*string, bool)`

GetPerformanceMetricOk returns a tuple with the PerformanceMetric field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPerformanceMetric

`func (o *VnfMonitoringParameter) SetPerformanceMetric(v string)`

SetPerformanceMetric sets PerformanceMetric field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


