# VnfcSnapshotInfoStorageSnapshotResourcesInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StorageResourceId** | **string** | An identifier that is unique for the respective type within a VNF instance, but may not be globally unique.  | 
**StorageSnapshotResources** | Pointer to [**ResourceHandle**](ResourceHandle.md) |  | [optional] 

## Methods

### NewVnfcSnapshotInfoStorageSnapshotResourcesInner

`func NewVnfcSnapshotInfoStorageSnapshotResourcesInner(storageResourceId string, ) *VnfcSnapshotInfoStorageSnapshotResourcesInner`

NewVnfcSnapshotInfoStorageSnapshotResourcesInner instantiates a new VnfcSnapshotInfoStorageSnapshotResourcesInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVnfcSnapshotInfoStorageSnapshotResourcesInnerWithDefaults

`func NewVnfcSnapshotInfoStorageSnapshotResourcesInnerWithDefaults() *VnfcSnapshotInfoStorageSnapshotResourcesInner`

NewVnfcSnapshotInfoStorageSnapshotResourcesInnerWithDefaults instantiates a new VnfcSnapshotInfoStorageSnapshotResourcesInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetStorageResourceId

`func (o *VnfcSnapshotInfoStorageSnapshotResourcesInner) GetStorageResourceId() string`

GetStorageResourceId returns the StorageResourceId field if non-nil, zero value otherwise.

### GetStorageResourceIdOk

`func (o *VnfcSnapshotInfoStorageSnapshotResourcesInner) GetStorageResourceIdOk() (*string, bool)`

GetStorageResourceIdOk returns a tuple with the StorageResourceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStorageResourceId

`func (o *VnfcSnapshotInfoStorageSnapshotResourcesInner) SetStorageResourceId(v string)`

SetStorageResourceId sets StorageResourceId field to given value.


### GetStorageSnapshotResources

`func (o *VnfcSnapshotInfoStorageSnapshotResourcesInner) GetStorageSnapshotResources() ResourceHandle`

GetStorageSnapshotResources returns the StorageSnapshotResources field if non-nil, zero value otherwise.

### GetStorageSnapshotResourcesOk

`func (o *VnfcSnapshotInfoStorageSnapshotResourcesInner) GetStorageSnapshotResourcesOk() (*ResourceHandle, bool)`

GetStorageSnapshotResourcesOk returns a tuple with the StorageSnapshotResources field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStorageSnapshotResources

`func (o *VnfcSnapshotInfoStorageSnapshotResourcesInner) SetStorageSnapshotResources(v ResourceHandle)`

SetStorageSnapshotResources sets StorageSnapshotResources field to given value.

### HasStorageSnapshotResources

`func (o *VnfcSnapshotInfoStorageSnapshotResourcesInner) HasStorageSnapshotResources() bool`

HasStorageSnapshotResources returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


