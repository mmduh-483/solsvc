# ExtLinkPortData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | An identifier with the intention of being globally unique.  | 
**ResourceHandle** | [**ResourceHandle**](ResourceHandle.md) |  | 
**TrunkResourceId** | Pointer to **string** | An identifier maintained by the VIM or other resource provider. It is expected to be unique within the VIM instance. Representation: string of variable length.  | [optional] 

## Methods

### NewExtLinkPortData

`func NewExtLinkPortData(id string, resourceHandle ResourceHandle, ) *ExtLinkPortData`

NewExtLinkPortData instantiates a new ExtLinkPortData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewExtLinkPortDataWithDefaults

`func NewExtLinkPortDataWithDefaults() *ExtLinkPortData`

NewExtLinkPortDataWithDefaults instantiates a new ExtLinkPortData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *ExtLinkPortData) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *ExtLinkPortData) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *ExtLinkPortData) SetId(v string)`

SetId sets Id field to given value.


### GetResourceHandle

`func (o *ExtLinkPortData) GetResourceHandle() ResourceHandle`

GetResourceHandle returns the ResourceHandle field if non-nil, zero value otherwise.

### GetResourceHandleOk

`func (o *ExtLinkPortData) GetResourceHandleOk() (*ResourceHandle, bool)`

GetResourceHandleOk returns a tuple with the ResourceHandle field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResourceHandle

`func (o *ExtLinkPortData) SetResourceHandle(v ResourceHandle)`

SetResourceHandle sets ResourceHandle field to given value.


### GetTrunkResourceId

`func (o *ExtLinkPortData) GetTrunkResourceId() string`

GetTrunkResourceId returns the TrunkResourceId field if non-nil, zero value otherwise.

### GetTrunkResourceIdOk

`func (o *ExtLinkPortData) GetTrunkResourceIdOk() (*string, bool)`

GetTrunkResourceIdOk returns a tuple with the TrunkResourceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTrunkResourceId

`func (o *ExtLinkPortData) SetTrunkResourceId(v string)`

SetTrunkResourceId sets TrunkResourceId field to given value.

### HasTrunkResourceId

`func (o *ExtLinkPortData) HasTrunkResourceId() bool`

HasTrunkResourceId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


