# VirtualStorageResourceInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | An identifier that is unique for the respective type within a VNF instance, but may not be globally unique.  | 
**VirtualStorageDescId** | **string** | Identifier of the VNF Virtual Link Descriptor (VLD) in the VNFD.  | 
**VnfdId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**StorageResource** | [**ResourceHandle**](ResourceHandle.md) |  | 
**ReservationId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**Metadata** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 

## Methods

### NewVirtualStorageResourceInfo

`func NewVirtualStorageResourceInfo(id string, virtualStorageDescId string, storageResource ResourceHandle, ) *VirtualStorageResourceInfo`

NewVirtualStorageResourceInfo instantiates a new VirtualStorageResourceInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVirtualStorageResourceInfoWithDefaults

`func NewVirtualStorageResourceInfoWithDefaults() *VirtualStorageResourceInfo`

NewVirtualStorageResourceInfoWithDefaults instantiates a new VirtualStorageResourceInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *VirtualStorageResourceInfo) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *VirtualStorageResourceInfo) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *VirtualStorageResourceInfo) SetId(v string)`

SetId sets Id field to given value.


### GetVirtualStorageDescId

`func (o *VirtualStorageResourceInfo) GetVirtualStorageDescId() string`

GetVirtualStorageDescId returns the VirtualStorageDescId field if non-nil, zero value otherwise.

### GetVirtualStorageDescIdOk

`func (o *VirtualStorageResourceInfo) GetVirtualStorageDescIdOk() (*string, bool)`

GetVirtualStorageDescIdOk returns a tuple with the VirtualStorageDescId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVirtualStorageDescId

`func (o *VirtualStorageResourceInfo) SetVirtualStorageDescId(v string)`

SetVirtualStorageDescId sets VirtualStorageDescId field to given value.


### GetVnfdId

`func (o *VirtualStorageResourceInfo) GetVnfdId() string`

GetVnfdId returns the VnfdId field if non-nil, zero value otherwise.

### GetVnfdIdOk

`func (o *VirtualStorageResourceInfo) GetVnfdIdOk() (*string, bool)`

GetVnfdIdOk returns a tuple with the VnfdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfdId

`func (o *VirtualStorageResourceInfo) SetVnfdId(v string)`

SetVnfdId sets VnfdId field to given value.

### HasVnfdId

`func (o *VirtualStorageResourceInfo) HasVnfdId() bool`

HasVnfdId returns a boolean if a field has been set.

### GetStorageResource

`func (o *VirtualStorageResourceInfo) GetStorageResource() ResourceHandle`

GetStorageResource returns the StorageResource field if non-nil, zero value otherwise.

### GetStorageResourceOk

`func (o *VirtualStorageResourceInfo) GetStorageResourceOk() (*ResourceHandle, bool)`

GetStorageResourceOk returns a tuple with the StorageResource field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStorageResource

`func (o *VirtualStorageResourceInfo) SetStorageResource(v ResourceHandle)`

SetStorageResource sets StorageResource field to given value.


### GetReservationId

`func (o *VirtualStorageResourceInfo) GetReservationId() string`

GetReservationId returns the ReservationId field if non-nil, zero value otherwise.

### GetReservationIdOk

`func (o *VirtualStorageResourceInfo) GetReservationIdOk() (*string, bool)`

GetReservationIdOk returns a tuple with the ReservationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReservationId

`func (o *VirtualStorageResourceInfo) SetReservationId(v string)`

SetReservationId sets ReservationId field to given value.

### HasReservationId

`func (o *VirtualStorageResourceInfo) HasReservationId() bool`

HasReservationId returns a boolean if a field has been set.

### GetMetadata

`func (o *VirtualStorageResourceInfo) GetMetadata() map[string]interface{}`

GetMetadata returns the Metadata field if non-nil, zero value otherwise.

### GetMetadataOk

`func (o *VirtualStorageResourceInfo) GetMetadataOk() (*map[string]interface{}, bool)`

GetMetadataOk returns a tuple with the Metadata field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMetadata

`func (o *VirtualStorageResourceInfo) SetMetadata(v map[string]interface{})`

SetMetadata sets Metadata field to given value.

### HasMetadata

`func (o *VirtualStorageResourceInfo) HasMetadata() bool`

HasMetadata returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


