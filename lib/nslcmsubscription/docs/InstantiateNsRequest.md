# InstantiateNsRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NsFlavourId** | **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | 
**SapData** | Pointer to [**[]SapData**](SapData.md) | Create data concerning the SAPs of this NS.  | [optional] 
**AddpnfData** | Pointer to [**[]AddPnfData**](AddPnfData.md) | Information on the PNF(s) that are part of this NS.  | [optional] 
**VnfInstanceData** | Pointer to [**[]VnfInstanceData**](VnfInstanceData.md) | Specify an existing VNF instance to be used in the NS. If needed, the VNF Profile to be used for this VNF instance is also provided. See note 1.  | [optional] 
**NestedNsInstanceData** | Pointer to [**[]NestedNsInstanceData**](NestedNsInstanceData.md) | Specify an existing NS instance to be used as a nested NS within the NS. If needed, the NS Profile to be used for this nested NS  instance is also provided. See note 2 and note 3.  | [optional] 
**LocationConstraints** | Pointer to [**[]VnfLocationConstraint**](VnfLocationConstraint.md) | Defines the location constraints for the VNF to be instantiated as part of the NS instantiation. An example can be a constraint for the VNF to be in a specific geographic location..  | [optional] 
**NestedNsLocationConstraints** | Pointer to [**[]NestedNsLocationConstraint**](NestedNsLocationConstraint.md) | Defines the location constraints for the nested NS to be instantiated as part of the NS instantiation. An example can be a constraint for the nested NS to be in a specific geographic location.  | [optional] 
**AdditionalParamsForNs** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 
**AdditionalParamsForNestedNs** | Pointer to [**[]ParamsForNestedNs**](ParamsForNestedNs.md) | Allows the OSS/BSS to provide additional parameter(s) per nested NS instance (as opposed to the composite NS level, which is covered in additionalParamsForNs, and as opposed to the VNF level, which is covered in additionalParamsForVnf). This is for nested NS instances that are to be created by the NFVO as part of the NS instantiation and not for existing nested NS instances that are referenced for reuse.  | [optional] 
**AdditionalParamsForVnf** | Pointer to [**[]ParamsForVnf**](ParamsForVnf.md) | Allows the OSS/BSS to provide additional parameter(s) per VNF instance (as opposed to the composite NS level,  which is covered in additionalParamsForNs and as opposed  to the nested NS level, which is covered in  additionalParamsForNestedNs). This is for VNFs that are to be created by the NFVO as part of the NS instantiation  and not for existing VNF that are referenced for reuse.  | [optional] 
**StartTime** | Pointer to **time.Time** | Date-time stamp. Representation: String formatted according toas defined by the date-time production in IETF RFC 3339.  | [optional] 
**NsInstantiationLevelId** | Pointer to **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | [optional] 
**WanConnectionData** | Pointer to [**[]WanConnectionData**](WanConnectionData.md) | Information for connecting VNs to the WAN when VLs are deployed across a WAN. See note 4.  | [optional] 
**AdditionalAffinityOrAntiAffinityRule** | Pointer to [**[]AffinityOrAntiAffinityRule**](AffinityOrAntiAffinityRule.md) | Specifies additional affinity or anti-affinity constraint for the VNF instances to be instantiated as part of the NS instantiation. Shall not conflict with rules already specified in the NSD.  | [optional] 

## Methods

### NewInstantiateNsRequest

`func NewInstantiateNsRequest(nsFlavourId string, ) *InstantiateNsRequest`

NewInstantiateNsRequest instantiates a new InstantiateNsRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInstantiateNsRequestWithDefaults

`func NewInstantiateNsRequestWithDefaults() *InstantiateNsRequest`

NewInstantiateNsRequestWithDefaults instantiates a new InstantiateNsRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetNsFlavourId

`func (o *InstantiateNsRequest) GetNsFlavourId() string`

GetNsFlavourId returns the NsFlavourId field if non-nil, zero value otherwise.

### GetNsFlavourIdOk

`func (o *InstantiateNsRequest) GetNsFlavourIdOk() (*string, bool)`

GetNsFlavourIdOk returns a tuple with the NsFlavourId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsFlavourId

`func (o *InstantiateNsRequest) SetNsFlavourId(v string)`

SetNsFlavourId sets NsFlavourId field to given value.


### GetSapData

`func (o *InstantiateNsRequest) GetSapData() []SapData`

GetSapData returns the SapData field if non-nil, zero value otherwise.

### GetSapDataOk

`func (o *InstantiateNsRequest) GetSapDataOk() (*[]SapData, bool)`

GetSapDataOk returns a tuple with the SapData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSapData

`func (o *InstantiateNsRequest) SetSapData(v []SapData)`

SetSapData sets SapData field to given value.

### HasSapData

`func (o *InstantiateNsRequest) HasSapData() bool`

HasSapData returns a boolean if a field has been set.

### GetAddpnfData

`func (o *InstantiateNsRequest) GetAddpnfData() []AddPnfData`

GetAddpnfData returns the AddpnfData field if non-nil, zero value otherwise.

### GetAddpnfDataOk

`func (o *InstantiateNsRequest) GetAddpnfDataOk() (*[]AddPnfData, bool)`

GetAddpnfDataOk returns a tuple with the AddpnfData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddpnfData

`func (o *InstantiateNsRequest) SetAddpnfData(v []AddPnfData)`

SetAddpnfData sets AddpnfData field to given value.

### HasAddpnfData

`func (o *InstantiateNsRequest) HasAddpnfData() bool`

HasAddpnfData returns a boolean if a field has been set.

### GetVnfInstanceData

`func (o *InstantiateNsRequest) GetVnfInstanceData() []VnfInstanceData`

GetVnfInstanceData returns the VnfInstanceData field if non-nil, zero value otherwise.

### GetVnfInstanceDataOk

`func (o *InstantiateNsRequest) GetVnfInstanceDataOk() (*[]VnfInstanceData, bool)`

GetVnfInstanceDataOk returns a tuple with the VnfInstanceData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceData

`func (o *InstantiateNsRequest) SetVnfInstanceData(v []VnfInstanceData)`

SetVnfInstanceData sets VnfInstanceData field to given value.

### HasVnfInstanceData

`func (o *InstantiateNsRequest) HasVnfInstanceData() bool`

HasVnfInstanceData returns a boolean if a field has been set.

### GetNestedNsInstanceData

`func (o *InstantiateNsRequest) GetNestedNsInstanceData() []NestedNsInstanceData`

GetNestedNsInstanceData returns the NestedNsInstanceData field if non-nil, zero value otherwise.

### GetNestedNsInstanceDataOk

`func (o *InstantiateNsRequest) GetNestedNsInstanceDataOk() (*[]NestedNsInstanceData, bool)`

GetNestedNsInstanceDataOk returns a tuple with the NestedNsInstanceData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNestedNsInstanceData

`func (o *InstantiateNsRequest) SetNestedNsInstanceData(v []NestedNsInstanceData)`

SetNestedNsInstanceData sets NestedNsInstanceData field to given value.

### HasNestedNsInstanceData

`func (o *InstantiateNsRequest) HasNestedNsInstanceData() bool`

HasNestedNsInstanceData returns a boolean if a field has been set.

### GetLocationConstraints

`func (o *InstantiateNsRequest) GetLocationConstraints() []VnfLocationConstraint`

GetLocationConstraints returns the LocationConstraints field if non-nil, zero value otherwise.

### GetLocationConstraintsOk

`func (o *InstantiateNsRequest) GetLocationConstraintsOk() (*[]VnfLocationConstraint, bool)`

GetLocationConstraintsOk returns a tuple with the LocationConstraints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocationConstraints

`func (o *InstantiateNsRequest) SetLocationConstraints(v []VnfLocationConstraint)`

SetLocationConstraints sets LocationConstraints field to given value.

### HasLocationConstraints

`func (o *InstantiateNsRequest) HasLocationConstraints() bool`

HasLocationConstraints returns a boolean if a field has been set.

### GetNestedNsLocationConstraints

`func (o *InstantiateNsRequest) GetNestedNsLocationConstraints() []NestedNsLocationConstraint`

GetNestedNsLocationConstraints returns the NestedNsLocationConstraints field if non-nil, zero value otherwise.

### GetNestedNsLocationConstraintsOk

`func (o *InstantiateNsRequest) GetNestedNsLocationConstraintsOk() (*[]NestedNsLocationConstraint, bool)`

GetNestedNsLocationConstraintsOk returns a tuple with the NestedNsLocationConstraints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNestedNsLocationConstraints

`func (o *InstantiateNsRequest) SetNestedNsLocationConstraints(v []NestedNsLocationConstraint)`

SetNestedNsLocationConstraints sets NestedNsLocationConstraints field to given value.

### HasNestedNsLocationConstraints

`func (o *InstantiateNsRequest) HasNestedNsLocationConstraints() bool`

HasNestedNsLocationConstraints returns a boolean if a field has been set.

### GetAdditionalParamsForNs

`func (o *InstantiateNsRequest) GetAdditionalParamsForNs() map[string]interface{}`

GetAdditionalParamsForNs returns the AdditionalParamsForNs field if non-nil, zero value otherwise.

### GetAdditionalParamsForNsOk

`func (o *InstantiateNsRequest) GetAdditionalParamsForNsOk() (*map[string]interface{}, bool)`

GetAdditionalParamsForNsOk returns a tuple with the AdditionalParamsForNs field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdditionalParamsForNs

`func (o *InstantiateNsRequest) SetAdditionalParamsForNs(v map[string]interface{})`

SetAdditionalParamsForNs sets AdditionalParamsForNs field to given value.

### HasAdditionalParamsForNs

`func (o *InstantiateNsRequest) HasAdditionalParamsForNs() bool`

HasAdditionalParamsForNs returns a boolean if a field has been set.

### GetAdditionalParamsForNestedNs

`func (o *InstantiateNsRequest) GetAdditionalParamsForNestedNs() []ParamsForNestedNs`

GetAdditionalParamsForNestedNs returns the AdditionalParamsForNestedNs field if non-nil, zero value otherwise.

### GetAdditionalParamsForNestedNsOk

`func (o *InstantiateNsRequest) GetAdditionalParamsForNestedNsOk() (*[]ParamsForNestedNs, bool)`

GetAdditionalParamsForNestedNsOk returns a tuple with the AdditionalParamsForNestedNs field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdditionalParamsForNestedNs

`func (o *InstantiateNsRequest) SetAdditionalParamsForNestedNs(v []ParamsForNestedNs)`

SetAdditionalParamsForNestedNs sets AdditionalParamsForNestedNs field to given value.

### HasAdditionalParamsForNestedNs

`func (o *InstantiateNsRequest) HasAdditionalParamsForNestedNs() bool`

HasAdditionalParamsForNestedNs returns a boolean if a field has been set.

### GetAdditionalParamsForVnf

`func (o *InstantiateNsRequest) GetAdditionalParamsForVnf() []ParamsForVnf`

GetAdditionalParamsForVnf returns the AdditionalParamsForVnf field if non-nil, zero value otherwise.

### GetAdditionalParamsForVnfOk

`func (o *InstantiateNsRequest) GetAdditionalParamsForVnfOk() (*[]ParamsForVnf, bool)`

GetAdditionalParamsForVnfOk returns a tuple with the AdditionalParamsForVnf field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdditionalParamsForVnf

`func (o *InstantiateNsRequest) SetAdditionalParamsForVnf(v []ParamsForVnf)`

SetAdditionalParamsForVnf sets AdditionalParamsForVnf field to given value.

### HasAdditionalParamsForVnf

`func (o *InstantiateNsRequest) HasAdditionalParamsForVnf() bool`

HasAdditionalParamsForVnf returns a boolean if a field has been set.

### GetStartTime

`func (o *InstantiateNsRequest) GetStartTime() time.Time`

GetStartTime returns the StartTime field if non-nil, zero value otherwise.

### GetStartTimeOk

`func (o *InstantiateNsRequest) GetStartTimeOk() (*time.Time, bool)`

GetStartTimeOk returns a tuple with the StartTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStartTime

`func (o *InstantiateNsRequest) SetStartTime(v time.Time)`

SetStartTime sets StartTime field to given value.

### HasStartTime

`func (o *InstantiateNsRequest) HasStartTime() bool`

HasStartTime returns a boolean if a field has been set.

### GetNsInstantiationLevelId

`func (o *InstantiateNsRequest) GetNsInstantiationLevelId() string`

GetNsInstantiationLevelId returns the NsInstantiationLevelId field if non-nil, zero value otherwise.

### GetNsInstantiationLevelIdOk

`func (o *InstantiateNsRequest) GetNsInstantiationLevelIdOk() (*string, bool)`

GetNsInstantiationLevelIdOk returns a tuple with the NsInstantiationLevelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsInstantiationLevelId

`func (o *InstantiateNsRequest) SetNsInstantiationLevelId(v string)`

SetNsInstantiationLevelId sets NsInstantiationLevelId field to given value.

### HasNsInstantiationLevelId

`func (o *InstantiateNsRequest) HasNsInstantiationLevelId() bool`

HasNsInstantiationLevelId returns a boolean if a field has been set.

### GetWanConnectionData

`func (o *InstantiateNsRequest) GetWanConnectionData() []WanConnectionData`

GetWanConnectionData returns the WanConnectionData field if non-nil, zero value otherwise.

### GetWanConnectionDataOk

`func (o *InstantiateNsRequest) GetWanConnectionDataOk() (*[]WanConnectionData, bool)`

GetWanConnectionDataOk returns a tuple with the WanConnectionData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWanConnectionData

`func (o *InstantiateNsRequest) SetWanConnectionData(v []WanConnectionData)`

SetWanConnectionData sets WanConnectionData field to given value.

### HasWanConnectionData

`func (o *InstantiateNsRequest) HasWanConnectionData() bool`

HasWanConnectionData returns a boolean if a field has been set.

### GetAdditionalAffinityOrAntiAffinityRule

`func (o *InstantiateNsRequest) GetAdditionalAffinityOrAntiAffinityRule() []AffinityOrAntiAffinityRule`

GetAdditionalAffinityOrAntiAffinityRule returns the AdditionalAffinityOrAntiAffinityRule field if non-nil, zero value otherwise.

### GetAdditionalAffinityOrAntiAffinityRuleOk

`func (o *InstantiateNsRequest) GetAdditionalAffinityOrAntiAffinityRuleOk() (*[]AffinityOrAntiAffinityRule, bool)`

GetAdditionalAffinityOrAntiAffinityRuleOk returns a tuple with the AdditionalAffinityOrAntiAffinityRule field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdditionalAffinityOrAntiAffinityRule

`func (o *InstantiateNsRequest) SetAdditionalAffinityOrAntiAffinityRule(v []AffinityOrAntiAffinityRule)`

SetAdditionalAffinityOrAntiAffinityRule sets AdditionalAffinityOrAntiAffinityRule field to given value.

### HasAdditionalAffinityOrAntiAffinityRule

`func (o *InstantiateNsRequest) HasAdditionalAffinityOrAntiAffinityRule() bool`

HasAdditionalAffinityOrAntiAffinityRule returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


