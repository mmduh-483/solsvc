# AddVnffgData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TargetNsInstanceId** | Pointer to **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | [optional] 
**VnffgName** | **string** | Human readable name for the VNFFG.  | 
**Description** | **string** | Human readable description for the VNFFG.  | 

## Methods

### NewAddVnffgData

`func NewAddVnffgData(vnffgName string, description string, ) *AddVnffgData`

NewAddVnffgData instantiates a new AddVnffgData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAddVnffgDataWithDefaults

`func NewAddVnffgDataWithDefaults() *AddVnffgData`

NewAddVnffgDataWithDefaults instantiates a new AddVnffgData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTargetNsInstanceId

`func (o *AddVnffgData) GetTargetNsInstanceId() string`

GetTargetNsInstanceId returns the TargetNsInstanceId field if non-nil, zero value otherwise.

### GetTargetNsInstanceIdOk

`func (o *AddVnffgData) GetTargetNsInstanceIdOk() (*string, bool)`

GetTargetNsInstanceIdOk returns a tuple with the TargetNsInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTargetNsInstanceId

`func (o *AddVnffgData) SetTargetNsInstanceId(v string)`

SetTargetNsInstanceId sets TargetNsInstanceId field to given value.

### HasTargetNsInstanceId

`func (o *AddVnffgData) HasTargetNsInstanceId() bool`

HasTargetNsInstanceId returns a boolean if a field has been set.

### GetVnffgName

`func (o *AddVnffgData) GetVnffgName() string`

GetVnffgName returns the VnffgName field if non-nil, zero value otherwise.

### GetVnffgNameOk

`func (o *AddVnffgData) GetVnffgNameOk() (*string, bool)`

GetVnffgNameOk returns a tuple with the VnffgName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnffgName

`func (o *AddVnffgData) SetVnffgName(v string)`

SetVnffgName sets VnffgName field to given value.


### GetDescription

`func (o *AddVnffgData) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *AddVnffgData) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *AddVnffgData) SetDescription(v string)`

SetDescription sets Description field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


