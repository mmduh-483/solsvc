# ExtLinkPortInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | An identifier with the intention of being globally unique.  | 
**ResourceHandle** | [**ResourceHandle**](ResourceHandle.md) |  | 
**CpInstanceId** | Pointer to **string** | An identifier that is unique for the respective type within a VNF instance, but may not be globally unique.  | [optional] 
**SecondaryCpInstanceId** | Pointer to **string** | An identifier that is unique for the respective type within a VNF instance, but may not be globally unique.  | [optional] 
**TrunkResourceId** | Pointer to **string** | An identifier maintained by the VIM or other resource provider. It is expected to be unique within the VIM instance. Representation: string of variable length.  | [optional] 

## Methods

### NewExtLinkPortInfo

`func NewExtLinkPortInfo(id string, resourceHandle ResourceHandle, ) *ExtLinkPortInfo`

NewExtLinkPortInfo instantiates a new ExtLinkPortInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewExtLinkPortInfoWithDefaults

`func NewExtLinkPortInfoWithDefaults() *ExtLinkPortInfo`

NewExtLinkPortInfoWithDefaults instantiates a new ExtLinkPortInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *ExtLinkPortInfo) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *ExtLinkPortInfo) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *ExtLinkPortInfo) SetId(v string)`

SetId sets Id field to given value.


### GetResourceHandle

`func (o *ExtLinkPortInfo) GetResourceHandle() ResourceHandle`

GetResourceHandle returns the ResourceHandle field if non-nil, zero value otherwise.

### GetResourceHandleOk

`func (o *ExtLinkPortInfo) GetResourceHandleOk() (*ResourceHandle, bool)`

GetResourceHandleOk returns a tuple with the ResourceHandle field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResourceHandle

`func (o *ExtLinkPortInfo) SetResourceHandle(v ResourceHandle)`

SetResourceHandle sets ResourceHandle field to given value.


### GetCpInstanceId

`func (o *ExtLinkPortInfo) GetCpInstanceId() string`

GetCpInstanceId returns the CpInstanceId field if non-nil, zero value otherwise.

### GetCpInstanceIdOk

`func (o *ExtLinkPortInfo) GetCpInstanceIdOk() (*string, bool)`

GetCpInstanceIdOk returns a tuple with the CpInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCpInstanceId

`func (o *ExtLinkPortInfo) SetCpInstanceId(v string)`

SetCpInstanceId sets CpInstanceId field to given value.

### HasCpInstanceId

`func (o *ExtLinkPortInfo) HasCpInstanceId() bool`

HasCpInstanceId returns a boolean if a field has been set.

### GetSecondaryCpInstanceId

`func (o *ExtLinkPortInfo) GetSecondaryCpInstanceId() string`

GetSecondaryCpInstanceId returns the SecondaryCpInstanceId field if non-nil, zero value otherwise.

### GetSecondaryCpInstanceIdOk

`func (o *ExtLinkPortInfo) GetSecondaryCpInstanceIdOk() (*string, bool)`

GetSecondaryCpInstanceIdOk returns a tuple with the SecondaryCpInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSecondaryCpInstanceId

`func (o *ExtLinkPortInfo) SetSecondaryCpInstanceId(v string)`

SetSecondaryCpInstanceId sets SecondaryCpInstanceId field to given value.

### HasSecondaryCpInstanceId

`func (o *ExtLinkPortInfo) HasSecondaryCpInstanceId() bool`

HasSecondaryCpInstanceId returns a boolean if a field has been set.

### GetTrunkResourceId

`func (o *ExtLinkPortInfo) GetTrunkResourceId() string`

GetTrunkResourceId returns the TrunkResourceId field if non-nil, zero value otherwise.

### GetTrunkResourceIdOk

`func (o *ExtLinkPortInfo) GetTrunkResourceIdOk() (*string, bool)`

GetTrunkResourceIdOk returns a tuple with the TrunkResourceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTrunkResourceId

`func (o *ExtLinkPortInfo) SetTrunkResourceId(v string)`

SetTrunkResourceId sets TrunkResourceId field to given value.

### HasTrunkResourceId

`func (o *ExtLinkPortInfo) HasTrunkResourceId() bool`

HasTrunkResourceId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


