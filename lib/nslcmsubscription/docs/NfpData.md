# NfpData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NfpInfoId** | Pointer to **string** | An identifier that is unique with respect to a NS. Representation: string of variable length.  | [optional] 
**NfpName** | Pointer to **string** | Human readable name for the NFP. It shall be present for the new NFP, and it may be present otherwise. See note 2.  | [optional] 
**Description** | Pointer to **string** | Human readable description for the NFP. It shall be present for the new NFP, and it may be present otherwise. See note 2.  | [optional] 
**CpGroup** | Pointer to [**[]CpGroupInfo**](CpGroupInfo.md) | Group(s) of CPs and/or SAPs which the NFP passes by. Cardinality can be 0 if only updated or newly created NFP classification and selection rule which applied to an existing NFP is provided. See note 3 and 4.  | [optional] 
**NfpRule** | Pointer to [**NfpRule**](NfpRule.md) |  | [optional] 

## Methods

### NewNfpData

`func NewNfpData() *NfpData`

NewNfpData instantiates a new NfpData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNfpDataWithDefaults

`func NewNfpDataWithDefaults() *NfpData`

NewNfpDataWithDefaults instantiates a new NfpData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetNfpInfoId

`func (o *NfpData) GetNfpInfoId() string`

GetNfpInfoId returns the NfpInfoId field if non-nil, zero value otherwise.

### GetNfpInfoIdOk

`func (o *NfpData) GetNfpInfoIdOk() (*string, bool)`

GetNfpInfoIdOk returns a tuple with the NfpInfoId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNfpInfoId

`func (o *NfpData) SetNfpInfoId(v string)`

SetNfpInfoId sets NfpInfoId field to given value.

### HasNfpInfoId

`func (o *NfpData) HasNfpInfoId() bool`

HasNfpInfoId returns a boolean if a field has been set.

### GetNfpName

`func (o *NfpData) GetNfpName() string`

GetNfpName returns the NfpName field if non-nil, zero value otherwise.

### GetNfpNameOk

`func (o *NfpData) GetNfpNameOk() (*string, bool)`

GetNfpNameOk returns a tuple with the NfpName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNfpName

`func (o *NfpData) SetNfpName(v string)`

SetNfpName sets NfpName field to given value.

### HasNfpName

`func (o *NfpData) HasNfpName() bool`

HasNfpName returns a boolean if a field has been set.

### GetDescription

`func (o *NfpData) GetDescription() string`

GetDescription returns the Description field if non-nil, zero value otherwise.

### GetDescriptionOk

`func (o *NfpData) GetDescriptionOk() (*string, bool)`

GetDescriptionOk returns a tuple with the Description field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDescription

`func (o *NfpData) SetDescription(v string)`

SetDescription sets Description field to given value.

### HasDescription

`func (o *NfpData) HasDescription() bool`

HasDescription returns a boolean if a field has been set.

### GetCpGroup

`func (o *NfpData) GetCpGroup() []CpGroupInfo`

GetCpGroup returns the CpGroup field if non-nil, zero value otherwise.

### GetCpGroupOk

`func (o *NfpData) GetCpGroupOk() (*[]CpGroupInfo, bool)`

GetCpGroupOk returns a tuple with the CpGroup field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCpGroup

`func (o *NfpData) SetCpGroup(v []CpGroupInfo)`

SetCpGroup sets CpGroup field to given value.

### HasCpGroup

`func (o *NfpData) HasCpGroup() bool`

HasCpGroup returns a boolean if a field has been set.

### GetNfpRule

`func (o *NfpData) GetNfpRule() NfpRule`

GetNfpRule returns the NfpRule field if non-nil, zero value otherwise.

### GetNfpRuleOk

`func (o *NfpData) GetNfpRuleOk() (*NfpRule, bool)`

GetNfpRuleOk returns a tuple with the NfpRule field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNfpRule

`func (o *NfpData) SetNfpRule(v NfpRule)`

SetNfpRule sets NfpRule field to given value.

### HasNfpRule

`func (o *NfpData) HasNfpRule() bool`

HasNfpRule returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


