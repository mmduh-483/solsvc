# NsInstanceSubscriptionFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NsdIds** | Pointer to **[]string** | If present, match NS instances that were created based on a NSD identified by one of the nsdId values listed in this attribute. See note 1.  | [optional] 
**VnfdIds** | Pointer to **[]string** | If present, match NS instances that contain VNF instances that were created based on a VNFD identified by one of the vnfdId values listed in this attribute. See note 1.  | [optional] 
**PnfdIds** | Pointer to **[]string** | If present, match NS instances that contain PNFs that are represented by a PNFD identified by one of the pnfdId values listed in this attribute. See note 1.  | [optional] 
**NsInstanceIds** | Pointer to **[]string** | If present, match NS instances with an instance identifier listed in this attribute. See note 2.  | [optional] 
**NsInstanceNames** | Pointer to **[]string** | If present, match NS instances with a NS Instance Name listed in this attribute. See note 2.  | [optional] 

## Methods

### NewNsInstanceSubscriptionFilter

`func NewNsInstanceSubscriptionFilter() *NsInstanceSubscriptionFilter`

NewNsInstanceSubscriptionFilter instantiates a new NsInstanceSubscriptionFilter object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNsInstanceSubscriptionFilterWithDefaults

`func NewNsInstanceSubscriptionFilterWithDefaults() *NsInstanceSubscriptionFilter`

NewNsInstanceSubscriptionFilterWithDefaults instantiates a new NsInstanceSubscriptionFilter object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetNsdIds

`func (o *NsInstanceSubscriptionFilter) GetNsdIds() []string`

GetNsdIds returns the NsdIds field if non-nil, zero value otherwise.

### GetNsdIdsOk

`func (o *NsInstanceSubscriptionFilter) GetNsdIdsOk() (*[]string, bool)`

GetNsdIdsOk returns a tuple with the NsdIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsdIds

`func (o *NsInstanceSubscriptionFilter) SetNsdIds(v []string)`

SetNsdIds sets NsdIds field to given value.

### HasNsdIds

`func (o *NsInstanceSubscriptionFilter) HasNsdIds() bool`

HasNsdIds returns a boolean if a field has been set.

### GetVnfdIds

`func (o *NsInstanceSubscriptionFilter) GetVnfdIds() []string`

GetVnfdIds returns the VnfdIds field if non-nil, zero value otherwise.

### GetVnfdIdsOk

`func (o *NsInstanceSubscriptionFilter) GetVnfdIdsOk() (*[]string, bool)`

GetVnfdIdsOk returns a tuple with the VnfdIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfdIds

`func (o *NsInstanceSubscriptionFilter) SetVnfdIds(v []string)`

SetVnfdIds sets VnfdIds field to given value.

### HasVnfdIds

`func (o *NsInstanceSubscriptionFilter) HasVnfdIds() bool`

HasVnfdIds returns a boolean if a field has been set.

### GetPnfdIds

`func (o *NsInstanceSubscriptionFilter) GetPnfdIds() []string`

GetPnfdIds returns the PnfdIds field if non-nil, zero value otherwise.

### GetPnfdIdsOk

`func (o *NsInstanceSubscriptionFilter) GetPnfdIdsOk() (*[]string, bool)`

GetPnfdIdsOk returns a tuple with the PnfdIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPnfdIds

`func (o *NsInstanceSubscriptionFilter) SetPnfdIds(v []string)`

SetPnfdIds sets PnfdIds field to given value.

### HasPnfdIds

`func (o *NsInstanceSubscriptionFilter) HasPnfdIds() bool`

HasPnfdIds returns a boolean if a field has been set.

### GetNsInstanceIds

`func (o *NsInstanceSubscriptionFilter) GetNsInstanceIds() []string`

GetNsInstanceIds returns the NsInstanceIds field if non-nil, zero value otherwise.

### GetNsInstanceIdsOk

`func (o *NsInstanceSubscriptionFilter) GetNsInstanceIdsOk() (*[]string, bool)`

GetNsInstanceIdsOk returns a tuple with the NsInstanceIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsInstanceIds

`func (o *NsInstanceSubscriptionFilter) SetNsInstanceIds(v []string)`

SetNsInstanceIds sets NsInstanceIds field to given value.

### HasNsInstanceIds

`func (o *NsInstanceSubscriptionFilter) HasNsInstanceIds() bool`

HasNsInstanceIds returns a boolean if a field has been set.

### GetNsInstanceNames

`func (o *NsInstanceSubscriptionFilter) GetNsInstanceNames() []string`

GetNsInstanceNames returns the NsInstanceNames field if non-nil, zero value otherwise.

### GetNsInstanceNamesOk

`func (o *NsInstanceSubscriptionFilter) GetNsInstanceNamesOk() (*[]string, bool)`

GetNsInstanceNamesOk returns a tuple with the NsInstanceNames field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsInstanceNames

`func (o *NsInstanceSubscriptionFilter) SetNsInstanceNames(v []string)`

SetNsInstanceNames sets NsInstanceNames field to given value.

### HasNsInstanceNames

`func (o *NsInstanceSubscriptionFilter) HasNsInstanceNames() bool`

HasNsInstanceNames returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


