# VnfcSnapshotInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | An identifier with the intention of being globally unique.  | 
**VnfcInstanceId** | **string** | An identifier that is unique for the respective type within a VNF instance, but may not be globally unique.  | 
**CreationStartedAt** | Pointer to **time.Time** | Date-time stamp. Representation: String formatted according toas defined by the date-time production in IETF RFC 3339.  | [optional] 
**CreationFinishedAt** | Pointer to **time.Time** | Date-time stamp. Representation: String formatted according toas defined by the date-time production in IETF RFC 3339.  | [optional] 
**VnfcResourceInfoId** | Pointer to **string** | An identifier that is unique for the respective type within a VNF instance, but may not be globally unique.  | [optional] 
**ComputeSnapshotResource** | Pointer to [**ResourceHandle**](ResourceHandle.md) |  | [optional] 
**StorageSnapshotResources** | Pointer to [**[]VnfcSnapshotInfoStorageSnapshotResourcesInner**](VnfcSnapshotInfoStorageSnapshotResourcesInner.md) | Reference to the \&quot;VirtualStorageResourceInfo\&quot; structure in the \&quot;VnfInstance\&quot; structure that represents the virtual storage resource. See note 2.  | [optional] 
**UserDefinedData** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 

## Methods

### NewVnfcSnapshotInfo

`func NewVnfcSnapshotInfo(id string, vnfcInstanceId string, ) *VnfcSnapshotInfo`

NewVnfcSnapshotInfo instantiates a new VnfcSnapshotInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVnfcSnapshotInfoWithDefaults

`func NewVnfcSnapshotInfoWithDefaults() *VnfcSnapshotInfo`

NewVnfcSnapshotInfoWithDefaults instantiates a new VnfcSnapshotInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *VnfcSnapshotInfo) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *VnfcSnapshotInfo) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *VnfcSnapshotInfo) SetId(v string)`

SetId sets Id field to given value.


### GetVnfcInstanceId

`func (o *VnfcSnapshotInfo) GetVnfcInstanceId() string`

GetVnfcInstanceId returns the VnfcInstanceId field if non-nil, zero value otherwise.

### GetVnfcInstanceIdOk

`func (o *VnfcSnapshotInfo) GetVnfcInstanceIdOk() (*string, bool)`

GetVnfcInstanceIdOk returns a tuple with the VnfcInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfcInstanceId

`func (o *VnfcSnapshotInfo) SetVnfcInstanceId(v string)`

SetVnfcInstanceId sets VnfcInstanceId field to given value.


### GetCreationStartedAt

`func (o *VnfcSnapshotInfo) GetCreationStartedAt() time.Time`

GetCreationStartedAt returns the CreationStartedAt field if non-nil, zero value otherwise.

### GetCreationStartedAtOk

`func (o *VnfcSnapshotInfo) GetCreationStartedAtOk() (*time.Time, bool)`

GetCreationStartedAtOk returns a tuple with the CreationStartedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreationStartedAt

`func (o *VnfcSnapshotInfo) SetCreationStartedAt(v time.Time)`

SetCreationStartedAt sets CreationStartedAt field to given value.

### HasCreationStartedAt

`func (o *VnfcSnapshotInfo) HasCreationStartedAt() bool`

HasCreationStartedAt returns a boolean if a field has been set.

### GetCreationFinishedAt

`func (o *VnfcSnapshotInfo) GetCreationFinishedAt() time.Time`

GetCreationFinishedAt returns the CreationFinishedAt field if non-nil, zero value otherwise.

### GetCreationFinishedAtOk

`func (o *VnfcSnapshotInfo) GetCreationFinishedAtOk() (*time.Time, bool)`

GetCreationFinishedAtOk returns a tuple with the CreationFinishedAt field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreationFinishedAt

`func (o *VnfcSnapshotInfo) SetCreationFinishedAt(v time.Time)`

SetCreationFinishedAt sets CreationFinishedAt field to given value.

### HasCreationFinishedAt

`func (o *VnfcSnapshotInfo) HasCreationFinishedAt() bool`

HasCreationFinishedAt returns a boolean if a field has been set.

### GetVnfcResourceInfoId

`func (o *VnfcSnapshotInfo) GetVnfcResourceInfoId() string`

GetVnfcResourceInfoId returns the VnfcResourceInfoId field if non-nil, zero value otherwise.

### GetVnfcResourceInfoIdOk

`func (o *VnfcSnapshotInfo) GetVnfcResourceInfoIdOk() (*string, bool)`

GetVnfcResourceInfoIdOk returns a tuple with the VnfcResourceInfoId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfcResourceInfoId

`func (o *VnfcSnapshotInfo) SetVnfcResourceInfoId(v string)`

SetVnfcResourceInfoId sets VnfcResourceInfoId field to given value.

### HasVnfcResourceInfoId

`func (o *VnfcSnapshotInfo) HasVnfcResourceInfoId() bool`

HasVnfcResourceInfoId returns a boolean if a field has been set.

### GetComputeSnapshotResource

`func (o *VnfcSnapshotInfo) GetComputeSnapshotResource() ResourceHandle`

GetComputeSnapshotResource returns the ComputeSnapshotResource field if non-nil, zero value otherwise.

### GetComputeSnapshotResourceOk

`func (o *VnfcSnapshotInfo) GetComputeSnapshotResourceOk() (*ResourceHandle, bool)`

GetComputeSnapshotResourceOk returns a tuple with the ComputeSnapshotResource field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetComputeSnapshotResource

`func (o *VnfcSnapshotInfo) SetComputeSnapshotResource(v ResourceHandle)`

SetComputeSnapshotResource sets ComputeSnapshotResource field to given value.

### HasComputeSnapshotResource

`func (o *VnfcSnapshotInfo) HasComputeSnapshotResource() bool`

HasComputeSnapshotResource returns a boolean if a field has been set.

### GetStorageSnapshotResources

`func (o *VnfcSnapshotInfo) GetStorageSnapshotResources() []VnfcSnapshotInfoStorageSnapshotResourcesInner`

GetStorageSnapshotResources returns the StorageSnapshotResources field if non-nil, zero value otherwise.

### GetStorageSnapshotResourcesOk

`func (o *VnfcSnapshotInfo) GetStorageSnapshotResourcesOk() (*[]VnfcSnapshotInfoStorageSnapshotResourcesInner, bool)`

GetStorageSnapshotResourcesOk returns a tuple with the StorageSnapshotResources field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStorageSnapshotResources

`func (o *VnfcSnapshotInfo) SetStorageSnapshotResources(v []VnfcSnapshotInfoStorageSnapshotResourcesInner)`

SetStorageSnapshotResources sets StorageSnapshotResources field to given value.

### HasStorageSnapshotResources

`func (o *VnfcSnapshotInfo) HasStorageSnapshotResources() bool`

HasStorageSnapshotResources returns a boolean if a field has been set.

### GetUserDefinedData

`func (o *VnfcSnapshotInfo) GetUserDefinedData() map[string]interface{}`

GetUserDefinedData returns the UserDefinedData field if non-nil, zero value otherwise.

### GetUserDefinedDataOk

`func (o *VnfcSnapshotInfo) GetUserDefinedDataOk() (*map[string]interface{}, bool)`

GetUserDefinedDataOk returns a tuple with the UserDefinedData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUserDefinedData

`func (o *VnfcSnapshotInfo) SetUserDefinedData(v map[string]interface{})`

SetUserDefinedData sets UserDefinedData field to given value.

### HasUserDefinedData

`func (o *VnfcSnapshotInfo) HasUserDefinedData() bool`

HasUserDefinedData returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


