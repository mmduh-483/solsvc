# InstantiateVnfData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VnfdId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**VnfFlavourId** | Pointer to **string** | Identifier of the VNF Virtual Link Descriptor (VLD) in the VNFD.  | [optional] 
**VnfInstantiationLevelId** | Pointer to **string** | Identifier of the VNF Virtual Link Descriptor (VLD) in the VNFD.  | [optional] 
**VnfProfileId** | Pointer to **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | [optional] 
**VnfInstanceName** | Pointer to **string** | Human-readable name of the VNF instance to be created.  | [optional] 
**VnfInstanceDescription** | Pointer to **string** | Human-readable description of the VNF instance to be created.  | [optional] 
**ExtVirtualLinks** | Pointer to [**[]ExtVirtualLinkData**](ExtVirtualLinkData.md) | Information about external VLs to connect the VNF to.  | [optional] 
**ExtManagedVirtualLinks** | Pointer to [**[]ExtManagedVirtualLinkData**](ExtManagedVirtualLinkData.md) | Information about internal VLs that are managed by other entities than the VNFM. See note 1.  | [optional] 
**LocalizationLanguage** | Pointer to **string** | Localization language of the VNF to be instantiated. The value shall comply with the format defined in IETF RFC 5646.  | [optional] 
**VnfConfigurableProperties** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 
**AdditionalParams** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 
**Metadata** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 
**Extensions** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 
**LocationConstraints** | Pointer to [**VnfLocationConstraint**](VnfLocationConstraint.md) |  | [optional] 

## Methods

### NewInstantiateVnfData

`func NewInstantiateVnfData() *InstantiateVnfData`

NewInstantiateVnfData instantiates a new InstantiateVnfData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewInstantiateVnfDataWithDefaults

`func NewInstantiateVnfDataWithDefaults() *InstantiateVnfData`

NewInstantiateVnfDataWithDefaults instantiates a new InstantiateVnfData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVnfdId

`func (o *InstantiateVnfData) GetVnfdId() string`

GetVnfdId returns the VnfdId field if non-nil, zero value otherwise.

### GetVnfdIdOk

`func (o *InstantiateVnfData) GetVnfdIdOk() (*string, bool)`

GetVnfdIdOk returns a tuple with the VnfdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfdId

`func (o *InstantiateVnfData) SetVnfdId(v string)`

SetVnfdId sets VnfdId field to given value.

### HasVnfdId

`func (o *InstantiateVnfData) HasVnfdId() bool`

HasVnfdId returns a boolean if a field has been set.

### GetVnfFlavourId

`func (o *InstantiateVnfData) GetVnfFlavourId() string`

GetVnfFlavourId returns the VnfFlavourId field if non-nil, zero value otherwise.

### GetVnfFlavourIdOk

`func (o *InstantiateVnfData) GetVnfFlavourIdOk() (*string, bool)`

GetVnfFlavourIdOk returns a tuple with the VnfFlavourId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfFlavourId

`func (o *InstantiateVnfData) SetVnfFlavourId(v string)`

SetVnfFlavourId sets VnfFlavourId field to given value.

### HasVnfFlavourId

`func (o *InstantiateVnfData) HasVnfFlavourId() bool`

HasVnfFlavourId returns a boolean if a field has been set.

### GetVnfInstantiationLevelId

`func (o *InstantiateVnfData) GetVnfInstantiationLevelId() string`

GetVnfInstantiationLevelId returns the VnfInstantiationLevelId field if non-nil, zero value otherwise.

### GetVnfInstantiationLevelIdOk

`func (o *InstantiateVnfData) GetVnfInstantiationLevelIdOk() (*string, bool)`

GetVnfInstantiationLevelIdOk returns a tuple with the VnfInstantiationLevelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstantiationLevelId

`func (o *InstantiateVnfData) SetVnfInstantiationLevelId(v string)`

SetVnfInstantiationLevelId sets VnfInstantiationLevelId field to given value.

### HasVnfInstantiationLevelId

`func (o *InstantiateVnfData) HasVnfInstantiationLevelId() bool`

HasVnfInstantiationLevelId returns a boolean if a field has been set.

### GetVnfProfileId

`func (o *InstantiateVnfData) GetVnfProfileId() string`

GetVnfProfileId returns the VnfProfileId field if non-nil, zero value otherwise.

### GetVnfProfileIdOk

`func (o *InstantiateVnfData) GetVnfProfileIdOk() (*string, bool)`

GetVnfProfileIdOk returns a tuple with the VnfProfileId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfProfileId

`func (o *InstantiateVnfData) SetVnfProfileId(v string)`

SetVnfProfileId sets VnfProfileId field to given value.

### HasVnfProfileId

`func (o *InstantiateVnfData) HasVnfProfileId() bool`

HasVnfProfileId returns a boolean if a field has been set.

### GetVnfInstanceName

`func (o *InstantiateVnfData) GetVnfInstanceName() string`

GetVnfInstanceName returns the VnfInstanceName field if non-nil, zero value otherwise.

### GetVnfInstanceNameOk

`func (o *InstantiateVnfData) GetVnfInstanceNameOk() (*string, bool)`

GetVnfInstanceNameOk returns a tuple with the VnfInstanceName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceName

`func (o *InstantiateVnfData) SetVnfInstanceName(v string)`

SetVnfInstanceName sets VnfInstanceName field to given value.

### HasVnfInstanceName

`func (o *InstantiateVnfData) HasVnfInstanceName() bool`

HasVnfInstanceName returns a boolean if a field has been set.

### GetVnfInstanceDescription

`func (o *InstantiateVnfData) GetVnfInstanceDescription() string`

GetVnfInstanceDescription returns the VnfInstanceDescription field if non-nil, zero value otherwise.

### GetVnfInstanceDescriptionOk

`func (o *InstantiateVnfData) GetVnfInstanceDescriptionOk() (*string, bool)`

GetVnfInstanceDescriptionOk returns a tuple with the VnfInstanceDescription field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceDescription

`func (o *InstantiateVnfData) SetVnfInstanceDescription(v string)`

SetVnfInstanceDescription sets VnfInstanceDescription field to given value.

### HasVnfInstanceDescription

`func (o *InstantiateVnfData) HasVnfInstanceDescription() bool`

HasVnfInstanceDescription returns a boolean if a field has been set.

### GetExtVirtualLinks

`func (o *InstantiateVnfData) GetExtVirtualLinks() []ExtVirtualLinkData`

GetExtVirtualLinks returns the ExtVirtualLinks field if non-nil, zero value otherwise.

### GetExtVirtualLinksOk

`func (o *InstantiateVnfData) GetExtVirtualLinksOk() (*[]ExtVirtualLinkData, bool)`

GetExtVirtualLinksOk returns a tuple with the ExtVirtualLinks field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtVirtualLinks

`func (o *InstantiateVnfData) SetExtVirtualLinks(v []ExtVirtualLinkData)`

SetExtVirtualLinks sets ExtVirtualLinks field to given value.

### HasExtVirtualLinks

`func (o *InstantiateVnfData) HasExtVirtualLinks() bool`

HasExtVirtualLinks returns a boolean if a field has been set.

### GetExtManagedVirtualLinks

`func (o *InstantiateVnfData) GetExtManagedVirtualLinks() []ExtManagedVirtualLinkData`

GetExtManagedVirtualLinks returns the ExtManagedVirtualLinks field if non-nil, zero value otherwise.

### GetExtManagedVirtualLinksOk

`func (o *InstantiateVnfData) GetExtManagedVirtualLinksOk() (*[]ExtManagedVirtualLinkData, bool)`

GetExtManagedVirtualLinksOk returns a tuple with the ExtManagedVirtualLinks field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtManagedVirtualLinks

`func (o *InstantiateVnfData) SetExtManagedVirtualLinks(v []ExtManagedVirtualLinkData)`

SetExtManagedVirtualLinks sets ExtManagedVirtualLinks field to given value.

### HasExtManagedVirtualLinks

`func (o *InstantiateVnfData) HasExtManagedVirtualLinks() bool`

HasExtManagedVirtualLinks returns a boolean if a field has been set.

### GetLocalizationLanguage

`func (o *InstantiateVnfData) GetLocalizationLanguage() string`

GetLocalizationLanguage returns the LocalizationLanguage field if non-nil, zero value otherwise.

### GetLocalizationLanguageOk

`func (o *InstantiateVnfData) GetLocalizationLanguageOk() (*string, bool)`

GetLocalizationLanguageOk returns a tuple with the LocalizationLanguage field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocalizationLanguage

`func (o *InstantiateVnfData) SetLocalizationLanguage(v string)`

SetLocalizationLanguage sets LocalizationLanguage field to given value.

### HasLocalizationLanguage

`func (o *InstantiateVnfData) HasLocalizationLanguage() bool`

HasLocalizationLanguage returns a boolean if a field has been set.

### GetVnfConfigurableProperties

`func (o *InstantiateVnfData) GetVnfConfigurableProperties() map[string]interface{}`

GetVnfConfigurableProperties returns the VnfConfigurableProperties field if non-nil, zero value otherwise.

### GetVnfConfigurablePropertiesOk

`func (o *InstantiateVnfData) GetVnfConfigurablePropertiesOk() (*map[string]interface{}, bool)`

GetVnfConfigurablePropertiesOk returns a tuple with the VnfConfigurableProperties field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfConfigurableProperties

`func (o *InstantiateVnfData) SetVnfConfigurableProperties(v map[string]interface{})`

SetVnfConfigurableProperties sets VnfConfigurableProperties field to given value.

### HasVnfConfigurableProperties

`func (o *InstantiateVnfData) HasVnfConfigurableProperties() bool`

HasVnfConfigurableProperties returns a boolean if a field has been set.

### GetAdditionalParams

`func (o *InstantiateVnfData) GetAdditionalParams() map[string]interface{}`

GetAdditionalParams returns the AdditionalParams field if non-nil, zero value otherwise.

### GetAdditionalParamsOk

`func (o *InstantiateVnfData) GetAdditionalParamsOk() (*map[string]interface{}, bool)`

GetAdditionalParamsOk returns a tuple with the AdditionalParams field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdditionalParams

`func (o *InstantiateVnfData) SetAdditionalParams(v map[string]interface{})`

SetAdditionalParams sets AdditionalParams field to given value.

### HasAdditionalParams

`func (o *InstantiateVnfData) HasAdditionalParams() bool`

HasAdditionalParams returns a boolean if a field has been set.

### GetMetadata

`func (o *InstantiateVnfData) GetMetadata() map[string]interface{}`

GetMetadata returns the Metadata field if non-nil, zero value otherwise.

### GetMetadataOk

`func (o *InstantiateVnfData) GetMetadataOk() (*map[string]interface{}, bool)`

GetMetadataOk returns a tuple with the Metadata field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMetadata

`func (o *InstantiateVnfData) SetMetadata(v map[string]interface{})`

SetMetadata sets Metadata field to given value.

### HasMetadata

`func (o *InstantiateVnfData) HasMetadata() bool`

HasMetadata returns a boolean if a field has been set.

### GetExtensions

`func (o *InstantiateVnfData) GetExtensions() map[string]interface{}`

GetExtensions returns the Extensions field if non-nil, zero value otherwise.

### GetExtensionsOk

`func (o *InstantiateVnfData) GetExtensionsOk() (*map[string]interface{}, bool)`

GetExtensionsOk returns a tuple with the Extensions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtensions

`func (o *InstantiateVnfData) SetExtensions(v map[string]interface{})`

SetExtensions sets Extensions field to given value.

### HasExtensions

`func (o *InstantiateVnfData) HasExtensions() bool`

HasExtensions returns a boolean if a field has been set.

### GetLocationConstraints

`func (o *InstantiateVnfData) GetLocationConstraints() VnfLocationConstraint`

GetLocationConstraints returns the LocationConstraints field if non-nil, zero value otherwise.

### GetLocationConstraintsOk

`func (o *InstantiateVnfData) GetLocationConstraintsOk() (*VnfLocationConstraint, bool)`

GetLocationConstraintsOk returns a tuple with the LocationConstraints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocationConstraints

`func (o *InstantiateVnfData) SetLocationConstraints(v VnfLocationConstraint)`

SetLocationConstraints sets LocationConstraints field to given value.

### HasLocationConstraints

`func (o *InstantiateVnfData) HasLocationConstraints() bool`

HasLocationConstraints returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


