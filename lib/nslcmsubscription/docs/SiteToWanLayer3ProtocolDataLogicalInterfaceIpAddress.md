# SiteToWanLayer3ProtocolDataLogicalInterfaceIpAddress

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IpAddress** | **string** | An IPV4 or IPV6 address. Representation: In case of an IPV4 address, string that consists of four decimal integers separated by dots, each integer ranging from 0 to 255. In case of an IPV6 address, string that consists of groups of zero to four hexadecimal digits, separated by colons.  | 
**AssociatedSegmentId** | **string** | The associated segment identifier that has triggered the creation of the logical interface. The value shall be one of the values listed in the \&quot;wanSegmentIds\&quot; of the \&quot;siteToWanLayer2ProtocolData\&quot;.  | 

## Methods

### NewSiteToWanLayer3ProtocolDataLogicalInterfaceIpAddress

`func NewSiteToWanLayer3ProtocolDataLogicalInterfaceIpAddress(ipAddress string, associatedSegmentId string, ) *SiteToWanLayer3ProtocolDataLogicalInterfaceIpAddress`

NewSiteToWanLayer3ProtocolDataLogicalInterfaceIpAddress instantiates a new SiteToWanLayer3ProtocolDataLogicalInterfaceIpAddress object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSiteToWanLayer3ProtocolDataLogicalInterfaceIpAddressWithDefaults

`func NewSiteToWanLayer3ProtocolDataLogicalInterfaceIpAddressWithDefaults() *SiteToWanLayer3ProtocolDataLogicalInterfaceIpAddress`

NewSiteToWanLayer3ProtocolDataLogicalInterfaceIpAddressWithDefaults instantiates a new SiteToWanLayer3ProtocolDataLogicalInterfaceIpAddress object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetIpAddress

`func (o *SiteToWanLayer3ProtocolDataLogicalInterfaceIpAddress) GetIpAddress() string`

GetIpAddress returns the IpAddress field if non-nil, zero value otherwise.

### GetIpAddressOk

`func (o *SiteToWanLayer3ProtocolDataLogicalInterfaceIpAddress) GetIpAddressOk() (*string, bool)`

GetIpAddressOk returns a tuple with the IpAddress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIpAddress

`func (o *SiteToWanLayer3ProtocolDataLogicalInterfaceIpAddress) SetIpAddress(v string)`

SetIpAddress sets IpAddress field to given value.


### GetAssociatedSegmentId

`func (o *SiteToWanLayer3ProtocolDataLogicalInterfaceIpAddress) GetAssociatedSegmentId() string`

GetAssociatedSegmentId returns the AssociatedSegmentId field if non-nil, zero value otherwise.

### GetAssociatedSegmentIdOk

`func (o *SiteToWanLayer3ProtocolDataLogicalInterfaceIpAddress) GetAssociatedSegmentIdOk() (*string, bool)`

GetAssociatedSegmentIdOk returns a tuple with the AssociatedSegmentId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAssociatedSegmentId

`func (o *SiteToWanLayer3ProtocolDataLogicalInterfaceIpAddress) SetAssociatedSegmentId(v string)`

SetAssociatedSegmentId sets AssociatedSegmentId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


