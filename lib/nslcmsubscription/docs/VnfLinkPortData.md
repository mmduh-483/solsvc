# VnfLinkPortData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VnfLinkPortId** | **string** | An identifier with the intention of being globally unique.  | 
**ResourceHandle** | [**ResourceHandle**](ResourceHandle.md) |  | 

## Methods

### NewVnfLinkPortData

`func NewVnfLinkPortData(vnfLinkPortId string, resourceHandle ResourceHandle, ) *VnfLinkPortData`

NewVnfLinkPortData instantiates a new VnfLinkPortData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVnfLinkPortDataWithDefaults

`func NewVnfLinkPortDataWithDefaults() *VnfLinkPortData`

NewVnfLinkPortDataWithDefaults instantiates a new VnfLinkPortData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVnfLinkPortId

`func (o *VnfLinkPortData) GetVnfLinkPortId() string`

GetVnfLinkPortId returns the VnfLinkPortId field if non-nil, zero value otherwise.

### GetVnfLinkPortIdOk

`func (o *VnfLinkPortData) GetVnfLinkPortIdOk() (*string, bool)`

GetVnfLinkPortIdOk returns a tuple with the VnfLinkPortId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfLinkPortId

`func (o *VnfLinkPortData) SetVnfLinkPortId(v string)`

SetVnfLinkPortId sets VnfLinkPortId field to given value.


### GetResourceHandle

`func (o *VnfLinkPortData) GetResourceHandle() ResourceHandle`

GetResourceHandle returns the ResourceHandle field if non-nil, zero value otherwise.

### GetResourceHandleOk

`func (o *VnfLinkPortData) GetResourceHandleOk() (*ResourceHandle, bool)`

GetResourceHandleOk returns a tuple with the ResourceHandle field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResourceHandle

`func (o *VnfLinkPortData) SetResourceHandle(v ResourceHandle)`

SetResourceHandle sets ResourceHandle field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


