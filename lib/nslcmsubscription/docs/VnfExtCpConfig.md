# VnfExtCpConfig

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ParentCpConfigId** | Pointer to **string** | An identifier that is unique for the respective type within a VNF instance, but may not be globally unique.  | [optional] 
**LinkPortId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**CreateExtLinkPort** | Pointer to **bool** | Indicates the need to create a dedicated link port for the external CP. If set to True, a link port is created. If set to False, no link port is created. This attribute is only applicable for external CP instances without a floating IP address that expose a VIP CP instance for which a dedicated IP address is allocated. It shall be present in that case and shall be absent otherwise.  | [optional] 
**CpProtocolData** | Pointer to [**[]CpProtocolData**](CpProtocolData.md) | Parameters for configuring the network protocols on the link port that connects the CP to a VL. See note 1.  | [optional] 

## Methods

### NewVnfExtCpConfig

`func NewVnfExtCpConfig() *VnfExtCpConfig`

NewVnfExtCpConfig instantiates a new VnfExtCpConfig object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVnfExtCpConfigWithDefaults

`func NewVnfExtCpConfigWithDefaults() *VnfExtCpConfig`

NewVnfExtCpConfigWithDefaults instantiates a new VnfExtCpConfig object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetParentCpConfigId

`func (o *VnfExtCpConfig) GetParentCpConfigId() string`

GetParentCpConfigId returns the ParentCpConfigId field if non-nil, zero value otherwise.

### GetParentCpConfigIdOk

`func (o *VnfExtCpConfig) GetParentCpConfigIdOk() (*string, bool)`

GetParentCpConfigIdOk returns a tuple with the ParentCpConfigId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetParentCpConfigId

`func (o *VnfExtCpConfig) SetParentCpConfigId(v string)`

SetParentCpConfigId sets ParentCpConfigId field to given value.

### HasParentCpConfigId

`func (o *VnfExtCpConfig) HasParentCpConfigId() bool`

HasParentCpConfigId returns a boolean if a field has been set.

### GetLinkPortId

`func (o *VnfExtCpConfig) GetLinkPortId() string`

GetLinkPortId returns the LinkPortId field if non-nil, zero value otherwise.

### GetLinkPortIdOk

`func (o *VnfExtCpConfig) GetLinkPortIdOk() (*string, bool)`

GetLinkPortIdOk returns a tuple with the LinkPortId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLinkPortId

`func (o *VnfExtCpConfig) SetLinkPortId(v string)`

SetLinkPortId sets LinkPortId field to given value.

### HasLinkPortId

`func (o *VnfExtCpConfig) HasLinkPortId() bool`

HasLinkPortId returns a boolean if a field has been set.

### GetCreateExtLinkPort

`func (o *VnfExtCpConfig) GetCreateExtLinkPort() bool`

GetCreateExtLinkPort returns the CreateExtLinkPort field if non-nil, zero value otherwise.

### GetCreateExtLinkPortOk

`func (o *VnfExtCpConfig) GetCreateExtLinkPortOk() (*bool, bool)`

GetCreateExtLinkPortOk returns a tuple with the CreateExtLinkPort field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCreateExtLinkPort

`func (o *VnfExtCpConfig) SetCreateExtLinkPort(v bool)`

SetCreateExtLinkPort sets CreateExtLinkPort field to given value.

### HasCreateExtLinkPort

`func (o *VnfExtCpConfig) HasCreateExtLinkPort() bool`

HasCreateExtLinkPort returns a boolean if a field has been set.

### GetCpProtocolData

`func (o *VnfExtCpConfig) GetCpProtocolData() []CpProtocolData`

GetCpProtocolData returns the CpProtocolData field if non-nil, zero value otherwise.

### GetCpProtocolDataOk

`func (o *VnfExtCpConfig) GetCpProtocolDataOk() (*[]CpProtocolData, bool)`

GetCpProtocolDataOk returns a tuple with the CpProtocolData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCpProtocolData

`func (o *VnfExtCpConfig) SetCpProtocolData(v []CpProtocolData)`

SetCpProtocolData sets CpProtocolData field to given value.

### HasCpProtocolData

`func (o *VnfExtCpConfig) HasCpProtocolData() bool`

HasCpProtocolData returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


