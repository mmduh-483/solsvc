# ChangeNsFlavourData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NewNsFlavourId** | **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | 
**InstantiationLevelId** | Pointer to **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | [optional] 

## Methods

### NewChangeNsFlavourData

`func NewChangeNsFlavourData(newNsFlavourId string, ) *ChangeNsFlavourData`

NewChangeNsFlavourData instantiates a new ChangeNsFlavourData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewChangeNsFlavourDataWithDefaults

`func NewChangeNsFlavourDataWithDefaults() *ChangeNsFlavourData`

NewChangeNsFlavourDataWithDefaults instantiates a new ChangeNsFlavourData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetNewNsFlavourId

`func (o *ChangeNsFlavourData) GetNewNsFlavourId() string`

GetNewNsFlavourId returns the NewNsFlavourId field if non-nil, zero value otherwise.

### GetNewNsFlavourIdOk

`func (o *ChangeNsFlavourData) GetNewNsFlavourIdOk() (*string, bool)`

GetNewNsFlavourIdOk returns a tuple with the NewNsFlavourId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNewNsFlavourId

`func (o *ChangeNsFlavourData) SetNewNsFlavourId(v string)`

SetNewNsFlavourId sets NewNsFlavourId field to given value.


### GetInstantiationLevelId

`func (o *ChangeNsFlavourData) GetInstantiationLevelId() string`

GetInstantiationLevelId returns the InstantiationLevelId field if non-nil, zero value otherwise.

### GetInstantiationLevelIdOk

`func (o *ChangeNsFlavourData) GetInstantiationLevelIdOk() (*string, bool)`

GetInstantiationLevelIdOk returns a tuple with the InstantiationLevelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInstantiationLevelId

`func (o *ChangeNsFlavourData) SetInstantiationLevelId(v string)`

SetInstantiationLevelId sets InstantiationLevelId field to given value.

### HasInstantiationLevelId

`func (o *ChangeNsFlavourData) HasInstantiationLevelId() bool`

HasInstantiationLevelId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


