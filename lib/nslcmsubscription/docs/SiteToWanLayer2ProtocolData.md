# SiteToWanLayer2ProtocolData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Layer2ConnectionInfo** | [**SiteToWanLayer2ProtocolDataLayer2ConnectionInfo**](SiteToWanLayer2ProtocolDataLayer2ConnectionInfo.md) |  | 
**MtuL2** | Pointer to **float32** | Maximum Transmission Unit (MTU) that can be forwarded at layer 2 (in bytes). Default value is \&quot;1500\&quot; (bytes).  | [optional] 
**VirtualRoutingAndForwarding** | Pointer to [**SiteToWanLayer2ProtocolDataVirtualRoutingAndForwarding**](SiteToWanLayer2ProtocolDataVirtualRoutingAndForwarding.md) |  | [optional] 
**ForwardingConfig** | Pointer to [**SiteToWanLayer2ProtocolDataForwardingConfig**](SiteToWanLayer2ProtocolDataForwardingConfig.md) |  | [optional] 

## Methods

### NewSiteToWanLayer2ProtocolData

`func NewSiteToWanLayer2ProtocolData(layer2ConnectionInfo SiteToWanLayer2ProtocolDataLayer2ConnectionInfo, ) *SiteToWanLayer2ProtocolData`

NewSiteToWanLayer2ProtocolData instantiates a new SiteToWanLayer2ProtocolData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSiteToWanLayer2ProtocolDataWithDefaults

`func NewSiteToWanLayer2ProtocolDataWithDefaults() *SiteToWanLayer2ProtocolData`

NewSiteToWanLayer2ProtocolDataWithDefaults instantiates a new SiteToWanLayer2ProtocolData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLayer2ConnectionInfo

`func (o *SiteToWanLayer2ProtocolData) GetLayer2ConnectionInfo() SiteToWanLayer2ProtocolDataLayer2ConnectionInfo`

GetLayer2ConnectionInfo returns the Layer2ConnectionInfo field if non-nil, zero value otherwise.

### GetLayer2ConnectionInfoOk

`func (o *SiteToWanLayer2ProtocolData) GetLayer2ConnectionInfoOk() (*SiteToWanLayer2ProtocolDataLayer2ConnectionInfo, bool)`

GetLayer2ConnectionInfoOk returns a tuple with the Layer2ConnectionInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLayer2ConnectionInfo

`func (o *SiteToWanLayer2ProtocolData) SetLayer2ConnectionInfo(v SiteToWanLayer2ProtocolDataLayer2ConnectionInfo)`

SetLayer2ConnectionInfo sets Layer2ConnectionInfo field to given value.


### GetMtuL2

`func (o *SiteToWanLayer2ProtocolData) GetMtuL2() float32`

GetMtuL2 returns the MtuL2 field if non-nil, zero value otherwise.

### GetMtuL2Ok

`func (o *SiteToWanLayer2ProtocolData) GetMtuL2Ok() (*float32, bool)`

GetMtuL2Ok returns a tuple with the MtuL2 field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMtuL2

`func (o *SiteToWanLayer2ProtocolData) SetMtuL2(v float32)`

SetMtuL2 sets MtuL2 field to given value.

### HasMtuL2

`func (o *SiteToWanLayer2ProtocolData) HasMtuL2() bool`

HasMtuL2 returns a boolean if a field has been set.

### GetVirtualRoutingAndForwarding

`func (o *SiteToWanLayer2ProtocolData) GetVirtualRoutingAndForwarding() SiteToWanLayer2ProtocolDataVirtualRoutingAndForwarding`

GetVirtualRoutingAndForwarding returns the VirtualRoutingAndForwarding field if non-nil, zero value otherwise.

### GetVirtualRoutingAndForwardingOk

`func (o *SiteToWanLayer2ProtocolData) GetVirtualRoutingAndForwardingOk() (*SiteToWanLayer2ProtocolDataVirtualRoutingAndForwarding, bool)`

GetVirtualRoutingAndForwardingOk returns a tuple with the VirtualRoutingAndForwarding field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVirtualRoutingAndForwarding

`func (o *SiteToWanLayer2ProtocolData) SetVirtualRoutingAndForwarding(v SiteToWanLayer2ProtocolDataVirtualRoutingAndForwarding)`

SetVirtualRoutingAndForwarding sets VirtualRoutingAndForwarding field to given value.

### HasVirtualRoutingAndForwarding

`func (o *SiteToWanLayer2ProtocolData) HasVirtualRoutingAndForwarding() bool`

HasVirtualRoutingAndForwarding returns a boolean if a field has been set.

### GetForwardingConfig

`func (o *SiteToWanLayer2ProtocolData) GetForwardingConfig() SiteToWanLayer2ProtocolDataForwardingConfig`

GetForwardingConfig returns the ForwardingConfig field if non-nil, zero value otherwise.

### GetForwardingConfigOk

`func (o *SiteToWanLayer2ProtocolData) GetForwardingConfigOk() (*SiteToWanLayer2ProtocolDataForwardingConfig, bool)`

GetForwardingConfigOk returns a tuple with the ForwardingConfig field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetForwardingConfig

`func (o *SiteToWanLayer2ProtocolData) SetForwardingConfig(v SiteToWanLayer2ProtocolDataForwardingConfig)`

SetForwardingConfig sets ForwardingConfig field to given value.

### HasForwardingConfig

`func (o *SiteToWanLayer2ProtocolData) HasForwardingConfig() bool`

HasForwardingConfig returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


