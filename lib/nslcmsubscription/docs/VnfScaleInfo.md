# VnfScaleInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AspectId** | **string** | Identifier of the VNF Virtual Link Descriptor (VLD) in the VNFD.  | 
**VnfdId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**ScaleLevel** | **int32** | Indicates the scale level. The minimum value shall be 0 and the maximum value shall be &lt;&#x3D; maxScaleLevel as described in the VNFD.  | 

## Methods

### NewVnfScaleInfo

`func NewVnfScaleInfo(aspectId string, scaleLevel int32, ) *VnfScaleInfo`

NewVnfScaleInfo instantiates a new VnfScaleInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVnfScaleInfoWithDefaults

`func NewVnfScaleInfoWithDefaults() *VnfScaleInfo`

NewVnfScaleInfoWithDefaults instantiates a new VnfScaleInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAspectId

`func (o *VnfScaleInfo) GetAspectId() string`

GetAspectId returns the AspectId field if non-nil, zero value otherwise.

### GetAspectIdOk

`func (o *VnfScaleInfo) GetAspectIdOk() (*string, bool)`

GetAspectIdOk returns a tuple with the AspectId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAspectId

`func (o *VnfScaleInfo) SetAspectId(v string)`

SetAspectId sets AspectId field to given value.


### GetVnfdId

`func (o *VnfScaleInfo) GetVnfdId() string`

GetVnfdId returns the VnfdId field if non-nil, zero value otherwise.

### GetVnfdIdOk

`func (o *VnfScaleInfo) GetVnfdIdOk() (*string, bool)`

GetVnfdIdOk returns a tuple with the VnfdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfdId

`func (o *VnfScaleInfo) SetVnfdId(v string)`

SetVnfdId sets VnfdId field to given value.

### HasVnfdId

`func (o *VnfScaleInfo) HasVnfdId() bool`

HasVnfdId returns a boolean if a field has been set.

### GetScaleLevel

`func (o *VnfScaleInfo) GetScaleLevel() int32`

GetScaleLevel returns the ScaleLevel field if non-nil, zero value otherwise.

### GetScaleLevelOk

`func (o *VnfScaleInfo) GetScaleLevelOk() (*int32, bool)`

GetScaleLevelOk returns a tuple with the ScaleLevel field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScaleLevel

`func (o *VnfScaleInfo) SetScaleLevel(v int32)`

SetScaleLevel sets ScaleLevel field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


