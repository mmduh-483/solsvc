# AffectedNs

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NsInstanceId** | **string** | An identifier with the intention of being globally unique.  | 
**NsdId** | **string** | An identifier with the intention of being globally unique.  | 
**ChangeType** | **string** | Signals the type of lifecycle change. Permitted values: - ADD - REMOVE - INSTANTIATE - SCALE - UPDATE - HEAL - TERMINATE  | 
**ChangeResult** | **string** | Signals the result of change identified by the \&quot;changeType\&quot; attribute. Permitted values: - COMPLETED - ROLLED_BACK - FAILED - PARTIALLY_COMPLETED  | 
**ChangedInfo** | Pointer to [**AffectedNsChangedInfo**](AffectedNsChangedInfo.md) |  | [optional] 

## Methods

### NewAffectedNs

`func NewAffectedNs(nsInstanceId string, nsdId string, changeType string, changeResult string, ) *AffectedNs`

NewAffectedNs instantiates a new AffectedNs object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAffectedNsWithDefaults

`func NewAffectedNsWithDefaults() *AffectedNs`

NewAffectedNsWithDefaults instantiates a new AffectedNs object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetNsInstanceId

`func (o *AffectedNs) GetNsInstanceId() string`

GetNsInstanceId returns the NsInstanceId field if non-nil, zero value otherwise.

### GetNsInstanceIdOk

`func (o *AffectedNs) GetNsInstanceIdOk() (*string, bool)`

GetNsInstanceIdOk returns a tuple with the NsInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsInstanceId

`func (o *AffectedNs) SetNsInstanceId(v string)`

SetNsInstanceId sets NsInstanceId field to given value.


### GetNsdId

`func (o *AffectedNs) GetNsdId() string`

GetNsdId returns the NsdId field if non-nil, zero value otherwise.

### GetNsdIdOk

`func (o *AffectedNs) GetNsdIdOk() (*string, bool)`

GetNsdIdOk returns a tuple with the NsdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsdId

`func (o *AffectedNs) SetNsdId(v string)`

SetNsdId sets NsdId field to given value.


### GetChangeType

`func (o *AffectedNs) GetChangeType() string`

GetChangeType returns the ChangeType field if non-nil, zero value otherwise.

### GetChangeTypeOk

`func (o *AffectedNs) GetChangeTypeOk() (*string, bool)`

GetChangeTypeOk returns a tuple with the ChangeType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChangeType

`func (o *AffectedNs) SetChangeType(v string)`

SetChangeType sets ChangeType field to given value.


### GetChangeResult

`func (o *AffectedNs) GetChangeResult() string`

GetChangeResult returns the ChangeResult field if non-nil, zero value otherwise.

### GetChangeResultOk

`func (o *AffectedNs) GetChangeResultOk() (*string, bool)`

GetChangeResultOk returns a tuple with the ChangeResult field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChangeResult

`func (o *AffectedNs) SetChangeResult(v string)`

SetChangeResult sets ChangeResult field to given value.


### GetChangedInfo

`func (o *AffectedNs) GetChangedInfo() AffectedNsChangedInfo`

GetChangedInfo returns the ChangedInfo field if non-nil, zero value otherwise.

### GetChangedInfoOk

`func (o *AffectedNs) GetChangedInfoOk() (*AffectedNsChangedInfo, bool)`

GetChangedInfoOk returns a tuple with the ChangedInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChangedInfo

`func (o *AffectedNs) SetChangedInfo(v AffectedNsChangedInfo)`

SetChangedInfo sets ChangedInfo field to given value.

### HasChangedInfo

`func (o *AffectedNs) HasChangedInfo() bool`

HasChangedInfo returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


