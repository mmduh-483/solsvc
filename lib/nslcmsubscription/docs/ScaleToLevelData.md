# ScaleToLevelData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VnfInstantiationLevelId** | Pointer to **string** | Identifier of the VNF Virtual Link Descriptor (VLD) in the VNFD.  | [optional] 
**VnfScaleInfo** | Pointer to [**[]VnfScaleInfo**](VnfScaleInfo.md) | For each scaling aspect of the current deployment flavor, indicates the target scale level to which the VNF is to be scaled. See note.  | [optional] 
**AdditionalParams** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 

## Methods

### NewScaleToLevelData

`func NewScaleToLevelData() *ScaleToLevelData`

NewScaleToLevelData instantiates a new ScaleToLevelData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewScaleToLevelDataWithDefaults

`func NewScaleToLevelDataWithDefaults() *ScaleToLevelData`

NewScaleToLevelDataWithDefaults instantiates a new ScaleToLevelData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVnfInstantiationLevelId

`func (o *ScaleToLevelData) GetVnfInstantiationLevelId() string`

GetVnfInstantiationLevelId returns the VnfInstantiationLevelId field if non-nil, zero value otherwise.

### GetVnfInstantiationLevelIdOk

`func (o *ScaleToLevelData) GetVnfInstantiationLevelIdOk() (*string, bool)`

GetVnfInstantiationLevelIdOk returns a tuple with the VnfInstantiationLevelId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstantiationLevelId

`func (o *ScaleToLevelData) SetVnfInstantiationLevelId(v string)`

SetVnfInstantiationLevelId sets VnfInstantiationLevelId field to given value.

### HasVnfInstantiationLevelId

`func (o *ScaleToLevelData) HasVnfInstantiationLevelId() bool`

HasVnfInstantiationLevelId returns a boolean if a field has been set.

### GetVnfScaleInfo

`func (o *ScaleToLevelData) GetVnfScaleInfo() []VnfScaleInfo`

GetVnfScaleInfo returns the VnfScaleInfo field if non-nil, zero value otherwise.

### GetVnfScaleInfoOk

`func (o *ScaleToLevelData) GetVnfScaleInfoOk() (*[]VnfScaleInfo, bool)`

GetVnfScaleInfoOk returns a tuple with the VnfScaleInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfScaleInfo

`func (o *ScaleToLevelData) SetVnfScaleInfo(v []VnfScaleInfo)`

SetVnfScaleInfo sets VnfScaleInfo field to given value.

### HasVnfScaleInfo

`func (o *ScaleToLevelData) HasVnfScaleInfo() bool`

HasVnfScaleInfo returns a boolean if a field has been set.

### GetAdditionalParams

`func (o *ScaleToLevelData) GetAdditionalParams() map[string]interface{}`

GetAdditionalParams returns the AdditionalParams field if non-nil, zero value otherwise.

### GetAdditionalParamsOk

`func (o *ScaleToLevelData) GetAdditionalParamsOk() (*map[string]interface{}, bool)`

GetAdditionalParamsOk returns a tuple with the AdditionalParams field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdditionalParams

`func (o *ScaleToLevelData) SetAdditionalParams(v map[string]interface{})`

SetAdditionalParams sets AdditionalParams field to given value.

### HasAdditionalParams

`func (o *ScaleToLevelData) HasAdditionalParams() bool`

HasAdditionalParams returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


