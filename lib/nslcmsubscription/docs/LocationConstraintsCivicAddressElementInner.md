# LocationConstraintsCivicAddressElementInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CaType** | **int32** | Describe the content type of caValue. The value of caType shall comply with Section 3.4 of IETF RFC 4776 [13].  | 
**CaValue** | **string** | Content of civic address element corresponding to the caType. The format caValue shall comply with Section 3.4 of IETF RFC 4776 [13].  | 

## Methods

### NewLocationConstraintsCivicAddressElementInner

`func NewLocationConstraintsCivicAddressElementInner(caType int32, caValue string, ) *LocationConstraintsCivicAddressElementInner`

NewLocationConstraintsCivicAddressElementInner instantiates a new LocationConstraintsCivicAddressElementInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewLocationConstraintsCivicAddressElementInnerWithDefaults

`func NewLocationConstraintsCivicAddressElementInnerWithDefaults() *LocationConstraintsCivicAddressElementInner`

NewLocationConstraintsCivicAddressElementInnerWithDefaults instantiates a new LocationConstraintsCivicAddressElementInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCaType

`func (o *LocationConstraintsCivicAddressElementInner) GetCaType() int32`

GetCaType returns the CaType field if non-nil, zero value otherwise.

### GetCaTypeOk

`func (o *LocationConstraintsCivicAddressElementInner) GetCaTypeOk() (*int32, bool)`

GetCaTypeOk returns a tuple with the CaType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCaType

`func (o *LocationConstraintsCivicAddressElementInner) SetCaType(v int32)`

SetCaType sets CaType field to given value.


### GetCaValue

`func (o *LocationConstraintsCivicAddressElementInner) GetCaValue() string`

GetCaValue returns the CaValue field if non-nil, zero value otherwise.

### GetCaValueOk

`func (o *LocationConstraintsCivicAddressElementInner) GetCaValueOk() (*string, bool)`

GetCaValueOk returns a tuple with the CaValue field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCaValue

`func (o *LocationConstraintsCivicAddressElementInner) SetCaValue(v string)`

SetCaValue sets CaValue field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


