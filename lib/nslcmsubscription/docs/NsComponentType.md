# NsComponentType

## Enum


* `VNF` (value: `"VNF"`)

* `PNF` (value: `"PNF"`)

* `NS` (value: `"NS"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


