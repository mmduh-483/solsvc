# CpProtocolData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LayerProtocol** | **string** | Identifier of layer(s) and protocol(s). Permitted values: IP_OVER_ETHERNET.  | 
**IpOverEthernet** | Pointer to [**IpOverEthernetAddressData**](IpOverEthernetAddressData.md) |  | [optional] 

## Methods

### NewCpProtocolData

`func NewCpProtocolData(layerProtocol string, ) *CpProtocolData`

NewCpProtocolData instantiates a new CpProtocolData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCpProtocolDataWithDefaults

`func NewCpProtocolDataWithDefaults() *CpProtocolData`

NewCpProtocolDataWithDefaults instantiates a new CpProtocolData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLayerProtocol

`func (o *CpProtocolData) GetLayerProtocol() string`

GetLayerProtocol returns the LayerProtocol field if non-nil, zero value otherwise.

### GetLayerProtocolOk

`func (o *CpProtocolData) GetLayerProtocolOk() (*string, bool)`

GetLayerProtocolOk returns a tuple with the LayerProtocol field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLayerProtocol

`func (o *CpProtocolData) SetLayerProtocol(v string)`

SetLayerProtocol sets LayerProtocol field to given value.


### GetIpOverEthernet

`func (o *CpProtocolData) GetIpOverEthernet() IpOverEthernetAddressData`

GetIpOverEthernet returns the IpOverEthernet field if non-nil, zero value otherwise.

### GetIpOverEthernetOk

`func (o *CpProtocolData) GetIpOverEthernetOk() (*IpOverEthernetAddressData, bool)`

GetIpOverEthernetOk returns a tuple with the IpOverEthernet field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIpOverEthernet

`func (o *CpProtocolData) SetIpOverEthernet(v IpOverEthernetAddressData)`

SetIpOverEthernet sets IpOverEthernet field to given value.

### HasIpOverEthernet

`func (o *CpProtocolData) HasIpOverEthernet() bool`

HasIpOverEthernet returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


