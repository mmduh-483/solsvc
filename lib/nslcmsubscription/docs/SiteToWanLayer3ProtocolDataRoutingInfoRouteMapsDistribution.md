# SiteToWanLayer3ProtocolDataRoutingInfoRouteMapsDistribution

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Policy** | **string** | The policy to apply to the route distribution. Permitted values: - PERMIT - DENY  | 
**Sequence** | **float32** | Sequence or index number assigned to the route-map.  | 
**MatchAndSetRule** | **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | 

## Methods

### NewSiteToWanLayer3ProtocolDataRoutingInfoRouteMapsDistribution

`func NewSiteToWanLayer3ProtocolDataRoutingInfoRouteMapsDistribution(policy string, sequence float32, matchAndSetRule map[string]interface{}, ) *SiteToWanLayer3ProtocolDataRoutingInfoRouteMapsDistribution`

NewSiteToWanLayer3ProtocolDataRoutingInfoRouteMapsDistribution instantiates a new SiteToWanLayer3ProtocolDataRoutingInfoRouteMapsDistribution object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSiteToWanLayer3ProtocolDataRoutingInfoRouteMapsDistributionWithDefaults

`func NewSiteToWanLayer3ProtocolDataRoutingInfoRouteMapsDistributionWithDefaults() *SiteToWanLayer3ProtocolDataRoutingInfoRouteMapsDistribution`

NewSiteToWanLayer3ProtocolDataRoutingInfoRouteMapsDistributionWithDefaults instantiates a new SiteToWanLayer3ProtocolDataRoutingInfoRouteMapsDistribution object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPolicy

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoRouteMapsDistribution) GetPolicy() string`

GetPolicy returns the Policy field if non-nil, zero value otherwise.

### GetPolicyOk

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoRouteMapsDistribution) GetPolicyOk() (*string, bool)`

GetPolicyOk returns a tuple with the Policy field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPolicy

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoRouteMapsDistribution) SetPolicy(v string)`

SetPolicy sets Policy field to given value.


### GetSequence

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoRouteMapsDistribution) GetSequence() float32`

GetSequence returns the Sequence field if non-nil, zero value otherwise.

### GetSequenceOk

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoRouteMapsDistribution) GetSequenceOk() (*float32, bool)`

GetSequenceOk returns a tuple with the Sequence field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSequence

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoRouteMapsDistribution) SetSequence(v float32)`

SetSequence sets Sequence field to given value.


### GetMatchAndSetRule

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoRouteMapsDistribution) GetMatchAndSetRule() map[string]interface{}`

GetMatchAndSetRule returns the MatchAndSetRule field if non-nil, zero value otherwise.

### GetMatchAndSetRuleOk

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoRouteMapsDistribution) GetMatchAndSetRuleOk() (*map[string]interface{}, bool)`

GetMatchAndSetRuleOk returns a tuple with the MatchAndSetRule field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMatchAndSetRule

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoRouteMapsDistribution) SetMatchAndSetRule(v map[string]interface{})`

SetMatchAndSetRule sets MatchAndSetRule field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


