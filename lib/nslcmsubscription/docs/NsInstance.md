# NsInstance

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | An identifier with the intention of being globally unique.  | 
**NsInstanceName** | **string** | Human readable name of the NS instance.  | 
**NsInstanceDescription** | **string** | Human readable description of the NS instance.  | 
**NsdId** | **string** | An identifier with the intention of being globally unique.  | 
**NsdInfoId** | **string** | An identifier with the intention of being globally unique.  | 
**FlavourId** | Pointer to **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | [optional] 
**Priority** | Pointer to **float32** | A number as defined in IETF RFC 8259.  | [optional] 
**VnfInstance** | Pointer to [**[]VnfInstance**](VnfInstance.md) | Information on constituent VNF(s) of the NS instance. See note 1.  | [optional] 
**PnfInfo** | Pointer to [**[]PnfInfo**](PnfInfo.md) | Information on the PNF(s) that are part of the NS instance.  | [optional] 
**VirtualLinkInfo** | Pointer to [**[]NsVirtualLinkInfo**](NsVirtualLinkInfo.md) | Information on the VL(s) of the NS instance. This attribute shall be present if the nsState attribute value is INSTANTIATED and if the NS instance has specified connectivity.  | [optional] 
**VnffgInfo** | Pointer to [**[]VnffgInfo**](VnffgInfo.md) | Information on the VNFFG(s) of the NS instance.  | [optional] 
**SapInfo** | Pointer to [**[]SapInfo**](SapInfo.md) | Information on the SAP(s) of the NS instance.  | [optional] 
**NestedNsInstanceId** | Pointer to **[]string** | Identifier of the nested NS(s) of the NS instance. See note.  | [optional] 
**VnfSnapshotInfoIds** | Pointer to **[]string** | Identifier of information on VNF snapshots of VNF instances that are part of this NS instance.  | [optional] 
**NsState** | **string** | The state of the NS instance. Permitted values: NOT_INSTANTIATED: The NS instance is terminated or not instantiated. INSTANTIATED: The NS instance is instantiated.  | 
**MonitoringParameter** | Pointer to [**[]NsMonitoringParameter**](NsMonitoringParameter.md) | Performance metrics tracked by the NFVO (e.g. for auto-scaling purposes) as identified by the NS designer in the NSD.  | [optional] 
**NsScaleStatus** | Pointer to [**[]NsScaleInfo**](NsScaleInfo.md) | Status of each NS scaling aspect declared in the applicable DF, how \&quot;big\&quot; the NS instance has been scaled w.r.t. that aspect. This attribute shall be present if the nsState attribute value is INSTANTIATED.  | [optional] 
**AdditionalAffinityOrAntiAffinityRule** | Pointer to [**[]AffinityOrAntiAffinityRule**](AffinityOrAntiAffinityRule.md) | Information on the additional affinity or anti-affinity rule from NS instantiation operation. Shall not conflict with rules already specified in the NSD.  | [optional] 
**WanConnectionInfo** | Pointer to [**[]WanConnectionInfo**](WanConnectionInfo.md) | Information about WAN related connectivity enabling multi-site VLs.  | [optional] 
**Links** | [**NsInstanceLinks**](NsInstanceLinks.md) |  | 

## Methods

### NewNsInstance

`func NewNsInstance(id string, nsInstanceName string, nsInstanceDescription string, nsdId string, nsdInfoId string, nsState string, links NsInstanceLinks, ) *NsInstance`

NewNsInstance instantiates a new NsInstance object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNsInstanceWithDefaults

`func NewNsInstanceWithDefaults() *NsInstance`

NewNsInstanceWithDefaults instantiates a new NsInstance object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *NsInstance) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *NsInstance) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *NsInstance) SetId(v string)`

SetId sets Id field to given value.


### GetNsInstanceName

`func (o *NsInstance) GetNsInstanceName() string`

GetNsInstanceName returns the NsInstanceName field if non-nil, zero value otherwise.

### GetNsInstanceNameOk

`func (o *NsInstance) GetNsInstanceNameOk() (*string, bool)`

GetNsInstanceNameOk returns a tuple with the NsInstanceName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsInstanceName

`func (o *NsInstance) SetNsInstanceName(v string)`

SetNsInstanceName sets NsInstanceName field to given value.


### GetNsInstanceDescription

`func (o *NsInstance) GetNsInstanceDescription() string`

GetNsInstanceDescription returns the NsInstanceDescription field if non-nil, zero value otherwise.

### GetNsInstanceDescriptionOk

`func (o *NsInstance) GetNsInstanceDescriptionOk() (*string, bool)`

GetNsInstanceDescriptionOk returns a tuple with the NsInstanceDescription field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsInstanceDescription

`func (o *NsInstance) SetNsInstanceDescription(v string)`

SetNsInstanceDescription sets NsInstanceDescription field to given value.


### GetNsdId

`func (o *NsInstance) GetNsdId() string`

GetNsdId returns the NsdId field if non-nil, zero value otherwise.

### GetNsdIdOk

`func (o *NsInstance) GetNsdIdOk() (*string, bool)`

GetNsdIdOk returns a tuple with the NsdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsdId

`func (o *NsInstance) SetNsdId(v string)`

SetNsdId sets NsdId field to given value.


### GetNsdInfoId

`func (o *NsInstance) GetNsdInfoId() string`

GetNsdInfoId returns the NsdInfoId field if non-nil, zero value otherwise.

### GetNsdInfoIdOk

`func (o *NsInstance) GetNsdInfoIdOk() (*string, bool)`

GetNsdInfoIdOk returns a tuple with the NsdInfoId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsdInfoId

`func (o *NsInstance) SetNsdInfoId(v string)`

SetNsdInfoId sets NsdInfoId field to given value.


### GetFlavourId

`func (o *NsInstance) GetFlavourId() string`

GetFlavourId returns the FlavourId field if non-nil, zero value otherwise.

### GetFlavourIdOk

`func (o *NsInstance) GetFlavourIdOk() (*string, bool)`

GetFlavourIdOk returns a tuple with the FlavourId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFlavourId

`func (o *NsInstance) SetFlavourId(v string)`

SetFlavourId sets FlavourId field to given value.

### HasFlavourId

`func (o *NsInstance) HasFlavourId() bool`

HasFlavourId returns a boolean if a field has been set.

### GetPriority

`func (o *NsInstance) GetPriority() float32`

GetPriority returns the Priority field if non-nil, zero value otherwise.

### GetPriorityOk

`func (o *NsInstance) GetPriorityOk() (*float32, bool)`

GetPriorityOk returns a tuple with the Priority field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPriority

`func (o *NsInstance) SetPriority(v float32)`

SetPriority sets Priority field to given value.

### HasPriority

`func (o *NsInstance) HasPriority() bool`

HasPriority returns a boolean if a field has been set.

### GetVnfInstance

`func (o *NsInstance) GetVnfInstance() []VnfInstance`

GetVnfInstance returns the VnfInstance field if non-nil, zero value otherwise.

### GetVnfInstanceOk

`func (o *NsInstance) GetVnfInstanceOk() (*[]VnfInstance, bool)`

GetVnfInstanceOk returns a tuple with the VnfInstance field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstance

`func (o *NsInstance) SetVnfInstance(v []VnfInstance)`

SetVnfInstance sets VnfInstance field to given value.

### HasVnfInstance

`func (o *NsInstance) HasVnfInstance() bool`

HasVnfInstance returns a boolean if a field has been set.

### GetPnfInfo

`func (o *NsInstance) GetPnfInfo() []PnfInfo`

GetPnfInfo returns the PnfInfo field if non-nil, zero value otherwise.

### GetPnfInfoOk

`func (o *NsInstance) GetPnfInfoOk() (*[]PnfInfo, bool)`

GetPnfInfoOk returns a tuple with the PnfInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPnfInfo

`func (o *NsInstance) SetPnfInfo(v []PnfInfo)`

SetPnfInfo sets PnfInfo field to given value.

### HasPnfInfo

`func (o *NsInstance) HasPnfInfo() bool`

HasPnfInfo returns a boolean if a field has been set.

### GetVirtualLinkInfo

`func (o *NsInstance) GetVirtualLinkInfo() []NsVirtualLinkInfo`

GetVirtualLinkInfo returns the VirtualLinkInfo field if non-nil, zero value otherwise.

### GetVirtualLinkInfoOk

`func (o *NsInstance) GetVirtualLinkInfoOk() (*[]NsVirtualLinkInfo, bool)`

GetVirtualLinkInfoOk returns a tuple with the VirtualLinkInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVirtualLinkInfo

`func (o *NsInstance) SetVirtualLinkInfo(v []NsVirtualLinkInfo)`

SetVirtualLinkInfo sets VirtualLinkInfo field to given value.

### HasVirtualLinkInfo

`func (o *NsInstance) HasVirtualLinkInfo() bool`

HasVirtualLinkInfo returns a boolean if a field has been set.

### GetVnffgInfo

`func (o *NsInstance) GetVnffgInfo() []VnffgInfo`

GetVnffgInfo returns the VnffgInfo field if non-nil, zero value otherwise.

### GetVnffgInfoOk

`func (o *NsInstance) GetVnffgInfoOk() (*[]VnffgInfo, bool)`

GetVnffgInfoOk returns a tuple with the VnffgInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnffgInfo

`func (o *NsInstance) SetVnffgInfo(v []VnffgInfo)`

SetVnffgInfo sets VnffgInfo field to given value.

### HasVnffgInfo

`func (o *NsInstance) HasVnffgInfo() bool`

HasVnffgInfo returns a boolean if a field has been set.

### GetSapInfo

`func (o *NsInstance) GetSapInfo() []SapInfo`

GetSapInfo returns the SapInfo field if non-nil, zero value otherwise.

### GetSapInfoOk

`func (o *NsInstance) GetSapInfoOk() (*[]SapInfo, bool)`

GetSapInfoOk returns a tuple with the SapInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSapInfo

`func (o *NsInstance) SetSapInfo(v []SapInfo)`

SetSapInfo sets SapInfo field to given value.

### HasSapInfo

`func (o *NsInstance) HasSapInfo() bool`

HasSapInfo returns a boolean if a field has been set.

### GetNestedNsInstanceId

`func (o *NsInstance) GetNestedNsInstanceId() []string`

GetNestedNsInstanceId returns the NestedNsInstanceId field if non-nil, zero value otherwise.

### GetNestedNsInstanceIdOk

`func (o *NsInstance) GetNestedNsInstanceIdOk() (*[]string, bool)`

GetNestedNsInstanceIdOk returns a tuple with the NestedNsInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNestedNsInstanceId

`func (o *NsInstance) SetNestedNsInstanceId(v []string)`

SetNestedNsInstanceId sets NestedNsInstanceId field to given value.

### HasNestedNsInstanceId

`func (o *NsInstance) HasNestedNsInstanceId() bool`

HasNestedNsInstanceId returns a boolean if a field has been set.

### GetVnfSnapshotInfoIds

`func (o *NsInstance) GetVnfSnapshotInfoIds() []string`

GetVnfSnapshotInfoIds returns the VnfSnapshotInfoIds field if non-nil, zero value otherwise.

### GetVnfSnapshotInfoIdsOk

`func (o *NsInstance) GetVnfSnapshotInfoIdsOk() (*[]string, bool)`

GetVnfSnapshotInfoIdsOk returns a tuple with the VnfSnapshotInfoIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfSnapshotInfoIds

`func (o *NsInstance) SetVnfSnapshotInfoIds(v []string)`

SetVnfSnapshotInfoIds sets VnfSnapshotInfoIds field to given value.

### HasVnfSnapshotInfoIds

`func (o *NsInstance) HasVnfSnapshotInfoIds() bool`

HasVnfSnapshotInfoIds returns a boolean if a field has been set.

### GetNsState

`func (o *NsInstance) GetNsState() string`

GetNsState returns the NsState field if non-nil, zero value otherwise.

### GetNsStateOk

`func (o *NsInstance) GetNsStateOk() (*string, bool)`

GetNsStateOk returns a tuple with the NsState field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsState

`func (o *NsInstance) SetNsState(v string)`

SetNsState sets NsState field to given value.


### GetMonitoringParameter

`func (o *NsInstance) GetMonitoringParameter() []NsMonitoringParameter`

GetMonitoringParameter returns the MonitoringParameter field if non-nil, zero value otherwise.

### GetMonitoringParameterOk

`func (o *NsInstance) GetMonitoringParameterOk() (*[]NsMonitoringParameter, bool)`

GetMonitoringParameterOk returns a tuple with the MonitoringParameter field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMonitoringParameter

`func (o *NsInstance) SetMonitoringParameter(v []NsMonitoringParameter)`

SetMonitoringParameter sets MonitoringParameter field to given value.

### HasMonitoringParameter

`func (o *NsInstance) HasMonitoringParameter() bool`

HasMonitoringParameter returns a boolean if a field has been set.

### GetNsScaleStatus

`func (o *NsInstance) GetNsScaleStatus() []NsScaleInfo`

GetNsScaleStatus returns the NsScaleStatus field if non-nil, zero value otherwise.

### GetNsScaleStatusOk

`func (o *NsInstance) GetNsScaleStatusOk() (*[]NsScaleInfo, bool)`

GetNsScaleStatusOk returns a tuple with the NsScaleStatus field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsScaleStatus

`func (o *NsInstance) SetNsScaleStatus(v []NsScaleInfo)`

SetNsScaleStatus sets NsScaleStatus field to given value.

### HasNsScaleStatus

`func (o *NsInstance) HasNsScaleStatus() bool`

HasNsScaleStatus returns a boolean if a field has been set.

### GetAdditionalAffinityOrAntiAffinityRule

`func (o *NsInstance) GetAdditionalAffinityOrAntiAffinityRule() []AffinityOrAntiAffinityRule`

GetAdditionalAffinityOrAntiAffinityRule returns the AdditionalAffinityOrAntiAffinityRule field if non-nil, zero value otherwise.

### GetAdditionalAffinityOrAntiAffinityRuleOk

`func (o *NsInstance) GetAdditionalAffinityOrAntiAffinityRuleOk() (*[]AffinityOrAntiAffinityRule, bool)`

GetAdditionalAffinityOrAntiAffinityRuleOk returns a tuple with the AdditionalAffinityOrAntiAffinityRule field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdditionalAffinityOrAntiAffinityRule

`func (o *NsInstance) SetAdditionalAffinityOrAntiAffinityRule(v []AffinityOrAntiAffinityRule)`

SetAdditionalAffinityOrAntiAffinityRule sets AdditionalAffinityOrAntiAffinityRule field to given value.

### HasAdditionalAffinityOrAntiAffinityRule

`func (o *NsInstance) HasAdditionalAffinityOrAntiAffinityRule() bool`

HasAdditionalAffinityOrAntiAffinityRule returns a boolean if a field has been set.

### GetWanConnectionInfo

`func (o *NsInstance) GetWanConnectionInfo() []WanConnectionInfo`

GetWanConnectionInfo returns the WanConnectionInfo field if non-nil, zero value otherwise.

### GetWanConnectionInfoOk

`func (o *NsInstance) GetWanConnectionInfoOk() (*[]WanConnectionInfo, bool)`

GetWanConnectionInfoOk returns a tuple with the WanConnectionInfo field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWanConnectionInfo

`func (o *NsInstance) SetWanConnectionInfo(v []WanConnectionInfo)`

SetWanConnectionInfo sets WanConnectionInfo field to given value.

### HasWanConnectionInfo

`func (o *NsInstance) HasWanConnectionInfo() bool`

HasWanConnectionInfo returns a boolean if a field has been set.

### GetLinks

`func (o *NsInstance) GetLinks() NsInstanceLinks`

GetLinks returns the Links field if non-nil, zero value otherwise.

### GetLinksOk

`func (o *NsInstance) GetLinksOk() (*NsInstanceLinks, bool)`

GetLinksOk returns a tuple with the Links field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLinks

`func (o *NsInstance) SetLinks(v NsInstanceLinks)`

SetLinks sets Links field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


