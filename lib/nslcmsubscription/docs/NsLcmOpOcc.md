# NsLcmOpOcc

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | An identifier with the intention of being globally unique.  | 
**OperationState** | [**NsLcmOperationStateType**](NsLcmOperationStateType.md) |  | 
**StateEnteredTime** | Pointer to **time.Time** | Date-time stamp. Representation: String formatted according toas defined by the date-time production in IETF RFC 3339.  | [optional] 
**NsInstanceId** | **string** | An identifier with the intention of being globally unique.  | 
**LcmOperationType** | [**NsLcmOpType**](NsLcmOpType.md) |  | 
**StartTime** | **time.Time** | Date-time stamp. Representation: String formatted according toas defined by the date-time production in IETF RFC 3339.  | 
**IsAutomaticInvocation** | **bool** | Set to true if this NS LCM operation occurrence has been automatically triggered by the NFVO. This occurs in the case of auto-scaling, auto-healing and when a nested NS is modified as a result of an operation on its composite NS. Set to false otherwise.  | 
**OperationParams** | Pointer to **string** | Input parameters of the LCM operation. This attribute shall be formatted according to the request data type of the related LCM operation. In addition, the provisions in clause 6.7 shall apply. The following mapping between lcmOperationType and the data type of this attribute shall apply: - INSTANTIATE: InstantiateNsRequest - SCALE: ScaleNsRequest - UPDATE: UpdateNsRequest - HEAL: HealNsRequest - TERMINATE: TerminateNsRequest This attribute shall be present if this data type is returned in a response to reading an individual resource, and may be present according to the chosen attribute selector parameter if this data type is returned in a response to a query of a container resource.  | [optional] 
**IsCancelPending** | **bool** | If the LCM operation occurrence is in \&quot;PROCESSING\&quot; or \&quot;ROLLING_BACK\&quot; state and the operation is being cancelled, this attribute shall be set to true. Otherwise, it shall be set to false.  | 
**CancelMode** | Pointer to [**CancelModeType**](CancelModeType.md) |  | [optional] 
**Error** | Pointer to [**ProblemDetails**](ProblemDetails.md) |  | [optional] 
**ResourceChanges** | Pointer to [**NsLcmOpOccResourceChanges**](NsLcmOpOccResourceChanges.md) |  | [optional] 
**LcmCoordinations** | Pointer to [**[]NsLcmOpOccLcmCoordinationsInner**](NsLcmOpOccLcmCoordinationsInner.md) | Information about LCM coordination actions (see clause 12) related to this LCM operation occurrence.  | [optional] 
**RejectedLcmCoordinations** | Pointer to [**[]NsLcmOpOccRejectedLcmCoordinationsInner**](NsLcmOpOccRejectedLcmCoordinationsInner.md) | Information about LCM coordination actions (see clause 12) that were rejected by 503 error which means they can be tried again after a delay. See note 3.  | [optional] 
**Warnings** | Pointer to **[]string** | Warning messages that were generated while the operation was executing. If the operation has included VNF LCM operations or NS LCM coordination actions and these have resulted in warnings, such warnings should be added to this attribute.  | [optional] 
**Links** | [**NsLcmOpOccLinks**](NsLcmOpOccLinks.md) |  | 

## Methods

### NewNsLcmOpOcc

`func NewNsLcmOpOcc(id string, operationState NsLcmOperationStateType, nsInstanceId string, lcmOperationType NsLcmOpType, startTime time.Time, isAutomaticInvocation bool, isCancelPending bool, links NsLcmOpOccLinks, ) *NsLcmOpOcc`

NewNsLcmOpOcc instantiates a new NsLcmOpOcc object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNsLcmOpOccWithDefaults

`func NewNsLcmOpOccWithDefaults() *NsLcmOpOcc`

NewNsLcmOpOccWithDefaults instantiates a new NsLcmOpOcc object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *NsLcmOpOcc) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *NsLcmOpOcc) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *NsLcmOpOcc) SetId(v string)`

SetId sets Id field to given value.


### GetOperationState

`func (o *NsLcmOpOcc) GetOperationState() NsLcmOperationStateType`

GetOperationState returns the OperationState field if non-nil, zero value otherwise.

### GetOperationStateOk

`func (o *NsLcmOpOcc) GetOperationStateOk() (*NsLcmOperationStateType, bool)`

GetOperationStateOk returns a tuple with the OperationState field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOperationState

`func (o *NsLcmOpOcc) SetOperationState(v NsLcmOperationStateType)`

SetOperationState sets OperationState field to given value.


### GetStateEnteredTime

`func (o *NsLcmOpOcc) GetStateEnteredTime() time.Time`

GetStateEnteredTime returns the StateEnteredTime field if non-nil, zero value otherwise.

### GetStateEnteredTimeOk

`func (o *NsLcmOpOcc) GetStateEnteredTimeOk() (*time.Time, bool)`

GetStateEnteredTimeOk returns a tuple with the StateEnteredTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStateEnteredTime

`func (o *NsLcmOpOcc) SetStateEnteredTime(v time.Time)`

SetStateEnteredTime sets StateEnteredTime field to given value.

### HasStateEnteredTime

`func (o *NsLcmOpOcc) HasStateEnteredTime() bool`

HasStateEnteredTime returns a boolean if a field has been set.

### GetNsInstanceId

`func (o *NsLcmOpOcc) GetNsInstanceId() string`

GetNsInstanceId returns the NsInstanceId field if non-nil, zero value otherwise.

### GetNsInstanceIdOk

`func (o *NsLcmOpOcc) GetNsInstanceIdOk() (*string, bool)`

GetNsInstanceIdOk returns a tuple with the NsInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsInstanceId

`func (o *NsLcmOpOcc) SetNsInstanceId(v string)`

SetNsInstanceId sets NsInstanceId field to given value.


### GetLcmOperationType

`func (o *NsLcmOpOcc) GetLcmOperationType() NsLcmOpType`

GetLcmOperationType returns the LcmOperationType field if non-nil, zero value otherwise.

### GetLcmOperationTypeOk

`func (o *NsLcmOpOcc) GetLcmOperationTypeOk() (*NsLcmOpType, bool)`

GetLcmOperationTypeOk returns a tuple with the LcmOperationType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLcmOperationType

`func (o *NsLcmOpOcc) SetLcmOperationType(v NsLcmOpType)`

SetLcmOperationType sets LcmOperationType field to given value.


### GetStartTime

`func (o *NsLcmOpOcc) GetStartTime() time.Time`

GetStartTime returns the StartTime field if non-nil, zero value otherwise.

### GetStartTimeOk

`func (o *NsLcmOpOcc) GetStartTimeOk() (*time.Time, bool)`

GetStartTimeOk returns a tuple with the StartTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStartTime

`func (o *NsLcmOpOcc) SetStartTime(v time.Time)`

SetStartTime sets StartTime field to given value.


### GetIsAutomaticInvocation

`func (o *NsLcmOpOcc) GetIsAutomaticInvocation() bool`

GetIsAutomaticInvocation returns the IsAutomaticInvocation field if non-nil, zero value otherwise.

### GetIsAutomaticInvocationOk

`func (o *NsLcmOpOcc) GetIsAutomaticInvocationOk() (*bool, bool)`

GetIsAutomaticInvocationOk returns a tuple with the IsAutomaticInvocation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIsAutomaticInvocation

`func (o *NsLcmOpOcc) SetIsAutomaticInvocation(v bool)`

SetIsAutomaticInvocation sets IsAutomaticInvocation field to given value.


### GetOperationParams

`func (o *NsLcmOpOcc) GetOperationParams() string`

GetOperationParams returns the OperationParams field if non-nil, zero value otherwise.

### GetOperationParamsOk

`func (o *NsLcmOpOcc) GetOperationParamsOk() (*string, bool)`

GetOperationParamsOk returns a tuple with the OperationParams field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOperationParams

`func (o *NsLcmOpOcc) SetOperationParams(v string)`

SetOperationParams sets OperationParams field to given value.

### HasOperationParams

`func (o *NsLcmOpOcc) HasOperationParams() bool`

HasOperationParams returns a boolean if a field has been set.

### GetIsCancelPending

`func (o *NsLcmOpOcc) GetIsCancelPending() bool`

GetIsCancelPending returns the IsCancelPending field if non-nil, zero value otherwise.

### GetIsCancelPendingOk

`func (o *NsLcmOpOcc) GetIsCancelPendingOk() (*bool, bool)`

GetIsCancelPendingOk returns a tuple with the IsCancelPending field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIsCancelPending

`func (o *NsLcmOpOcc) SetIsCancelPending(v bool)`

SetIsCancelPending sets IsCancelPending field to given value.


### GetCancelMode

`func (o *NsLcmOpOcc) GetCancelMode() CancelModeType`

GetCancelMode returns the CancelMode field if non-nil, zero value otherwise.

### GetCancelModeOk

`func (o *NsLcmOpOcc) GetCancelModeOk() (*CancelModeType, bool)`

GetCancelModeOk returns a tuple with the CancelMode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCancelMode

`func (o *NsLcmOpOcc) SetCancelMode(v CancelModeType)`

SetCancelMode sets CancelMode field to given value.

### HasCancelMode

`func (o *NsLcmOpOcc) HasCancelMode() bool`

HasCancelMode returns a boolean if a field has been set.

### GetError

`func (o *NsLcmOpOcc) GetError() ProblemDetails`

GetError returns the Error field if non-nil, zero value otherwise.

### GetErrorOk

`func (o *NsLcmOpOcc) GetErrorOk() (*ProblemDetails, bool)`

GetErrorOk returns a tuple with the Error field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetError

`func (o *NsLcmOpOcc) SetError(v ProblemDetails)`

SetError sets Error field to given value.

### HasError

`func (o *NsLcmOpOcc) HasError() bool`

HasError returns a boolean if a field has been set.

### GetResourceChanges

`func (o *NsLcmOpOcc) GetResourceChanges() NsLcmOpOccResourceChanges`

GetResourceChanges returns the ResourceChanges field if non-nil, zero value otherwise.

### GetResourceChangesOk

`func (o *NsLcmOpOcc) GetResourceChangesOk() (*NsLcmOpOccResourceChanges, bool)`

GetResourceChangesOk returns a tuple with the ResourceChanges field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResourceChanges

`func (o *NsLcmOpOcc) SetResourceChanges(v NsLcmOpOccResourceChanges)`

SetResourceChanges sets ResourceChanges field to given value.

### HasResourceChanges

`func (o *NsLcmOpOcc) HasResourceChanges() bool`

HasResourceChanges returns a boolean if a field has been set.

### GetLcmCoordinations

`func (o *NsLcmOpOcc) GetLcmCoordinations() []NsLcmOpOccLcmCoordinationsInner`

GetLcmCoordinations returns the LcmCoordinations field if non-nil, zero value otherwise.

### GetLcmCoordinationsOk

`func (o *NsLcmOpOcc) GetLcmCoordinationsOk() (*[]NsLcmOpOccLcmCoordinationsInner, bool)`

GetLcmCoordinationsOk returns a tuple with the LcmCoordinations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLcmCoordinations

`func (o *NsLcmOpOcc) SetLcmCoordinations(v []NsLcmOpOccLcmCoordinationsInner)`

SetLcmCoordinations sets LcmCoordinations field to given value.

### HasLcmCoordinations

`func (o *NsLcmOpOcc) HasLcmCoordinations() bool`

HasLcmCoordinations returns a boolean if a field has been set.

### GetRejectedLcmCoordinations

`func (o *NsLcmOpOcc) GetRejectedLcmCoordinations() []NsLcmOpOccRejectedLcmCoordinationsInner`

GetRejectedLcmCoordinations returns the RejectedLcmCoordinations field if non-nil, zero value otherwise.

### GetRejectedLcmCoordinationsOk

`func (o *NsLcmOpOcc) GetRejectedLcmCoordinationsOk() (*[]NsLcmOpOccRejectedLcmCoordinationsInner, bool)`

GetRejectedLcmCoordinationsOk returns a tuple with the RejectedLcmCoordinations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRejectedLcmCoordinations

`func (o *NsLcmOpOcc) SetRejectedLcmCoordinations(v []NsLcmOpOccRejectedLcmCoordinationsInner)`

SetRejectedLcmCoordinations sets RejectedLcmCoordinations field to given value.

### HasRejectedLcmCoordinations

`func (o *NsLcmOpOcc) HasRejectedLcmCoordinations() bool`

HasRejectedLcmCoordinations returns a boolean if a field has been set.

### GetWarnings

`func (o *NsLcmOpOcc) GetWarnings() []string`

GetWarnings returns the Warnings field if non-nil, zero value otherwise.

### GetWarningsOk

`func (o *NsLcmOpOcc) GetWarningsOk() (*[]string, bool)`

GetWarningsOk returns a tuple with the Warnings field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWarnings

`func (o *NsLcmOpOcc) SetWarnings(v []string)`

SetWarnings sets Warnings field to given value.

### HasWarnings

`func (o *NsLcmOpOcc) HasWarnings() bool`

HasWarnings returns a boolean if a field has been set.

### GetLinks

`func (o *NsLcmOpOcc) GetLinks() NsLcmOpOccLinks`

GetLinks returns the Links field if non-nil, zero value otherwise.

### GetLinksOk

`func (o *NsLcmOpOcc) GetLinksOk() (*NsLcmOpOccLinks, bool)`

GetLinksOk returns a tuple with the Links field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLinks

`func (o *NsLcmOpOcc) SetLinks(v NsLcmOpOccLinks)`

SetLinks sets Links field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


