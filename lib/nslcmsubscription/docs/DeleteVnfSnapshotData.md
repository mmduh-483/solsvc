# DeleteVnfSnapshotData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VnfInstanceId** | **string** | An identifier with the intention of being globally unique.  | 
**VnfSnapshotInfoId** | **string** | An identifier with the intention of being globally unique.  | 

## Methods

### NewDeleteVnfSnapshotData

`func NewDeleteVnfSnapshotData(vnfInstanceId string, vnfSnapshotInfoId string, ) *DeleteVnfSnapshotData`

NewDeleteVnfSnapshotData instantiates a new DeleteVnfSnapshotData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewDeleteVnfSnapshotDataWithDefaults

`func NewDeleteVnfSnapshotDataWithDefaults() *DeleteVnfSnapshotData`

NewDeleteVnfSnapshotDataWithDefaults instantiates a new DeleteVnfSnapshotData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVnfInstanceId

`func (o *DeleteVnfSnapshotData) GetVnfInstanceId() string`

GetVnfInstanceId returns the VnfInstanceId field if non-nil, zero value otherwise.

### GetVnfInstanceIdOk

`func (o *DeleteVnfSnapshotData) GetVnfInstanceIdOk() (*string, bool)`

GetVnfInstanceIdOk returns a tuple with the VnfInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceId

`func (o *DeleteVnfSnapshotData) SetVnfInstanceId(v string)`

SetVnfInstanceId sets VnfInstanceId field to given value.


### GetVnfSnapshotInfoId

`func (o *DeleteVnfSnapshotData) GetVnfSnapshotInfoId() string`

GetVnfSnapshotInfoId returns the VnfSnapshotInfoId field if non-nil, zero value otherwise.

### GetVnfSnapshotInfoIdOk

`func (o *DeleteVnfSnapshotData) GetVnfSnapshotInfoIdOk() (*string, bool)`

GetVnfSnapshotInfoIdOk returns a tuple with the VnfSnapshotInfoId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfSnapshotInfoId

`func (o *DeleteVnfSnapshotData) SetVnfSnapshotInfoId(v string)`

SetVnfSnapshotInfoId sets VnfSnapshotInfoId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


