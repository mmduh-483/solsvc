# ScaleNsData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VnfInstanceToBeAdded** | Pointer to [**[]VnfInstanceData**](VnfInstanceData.md) | An existing VNF instance to be added to the NS instance as part of the scaling operation. If needed, the VNF Profile to be used for this VNF instance may also be provided. See note 1, note 2 and note 3.  | [optional] 
**VnfInstanceToBeRemoved** | Pointer to **[]string** | The VNF instance to be removed from the NS instance as part of the scaling operation. See note 1 and note 4.  | [optional] 
**ScaleNsByStepsData** | Pointer to [**ScaleNsByStepsData**](ScaleNsByStepsData.md) |  | [optional] 
**ScaleNsToLevelData** | Pointer to [**ScaleNsToLevelData**](ScaleNsToLevelData.md) |  | [optional] 
**AdditionalParamsForNs** | Pointer to [**ParamsForVnf**](ParamsForVnf.md) |  | [optional] 
**AdditionalParamsForVnf** | Pointer to [**[]ParamsForVnf**](ParamsForVnf.md) | Allows the OSS/BSS to provide additional parameter(s) per VNF instance (as opposed to the NS level, which is covered in additionalParamsforNs). This is for VNFs that are to be created by the NFVO as part of the NS scaling and not for existing VNF that are covered by the scaleVnfData.  | [optional] 
**LocationConstraints** | Pointer to [**[]VnfLocationConstraint**](VnfLocationConstraint.md) | The location constraints for the VNF to be instantiated as part of the NS scaling. An example can be a constraint for the VNF to be in a specific geographic location.  | [optional] 
**NestedNsLocationConstraints** | Pointer to [**[]NestedNsLocationConstraint**](NestedNsLocationConstraint.md) | Defines the location constraints for the nested NS to be instantiated as part of the NS instantiation. An example can be a constraint for the nested NS to be in a specific geographic location.  | [optional] 

## Methods

### NewScaleNsData

`func NewScaleNsData() *ScaleNsData`

NewScaleNsData instantiates a new ScaleNsData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewScaleNsDataWithDefaults

`func NewScaleNsDataWithDefaults() *ScaleNsData`

NewScaleNsDataWithDefaults instantiates a new ScaleNsData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVnfInstanceToBeAdded

`func (o *ScaleNsData) GetVnfInstanceToBeAdded() []VnfInstanceData`

GetVnfInstanceToBeAdded returns the VnfInstanceToBeAdded field if non-nil, zero value otherwise.

### GetVnfInstanceToBeAddedOk

`func (o *ScaleNsData) GetVnfInstanceToBeAddedOk() (*[]VnfInstanceData, bool)`

GetVnfInstanceToBeAddedOk returns a tuple with the VnfInstanceToBeAdded field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceToBeAdded

`func (o *ScaleNsData) SetVnfInstanceToBeAdded(v []VnfInstanceData)`

SetVnfInstanceToBeAdded sets VnfInstanceToBeAdded field to given value.

### HasVnfInstanceToBeAdded

`func (o *ScaleNsData) HasVnfInstanceToBeAdded() bool`

HasVnfInstanceToBeAdded returns a boolean if a field has been set.

### GetVnfInstanceToBeRemoved

`func (o *ScaleNsData) GetVnfInstanceToBeRemoved() []string`

GetVnfInstanceToBeRemoved returns the VnfInstanceToBeRemoved field if non-nil, zero value otherwise.

### GetVnfInstanceToBeRemovedOk

`func (o *ScaleNsData) GetVnfInstanceToBeRemovedOk() (*[]string, bool)`

GetVnfInstanceToBeRemovedOk returns a tuple with the VnfInstanceToBeRemoved field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceToBeRemoved

`func (o *ScaleNsData) SetVnfInstanceToBeRemoved(v []string)`

SetVnfInstanceToBeRemoved sets VnfInstanceToBeRemoved field to given value.

### HasVnfInstanceToBeRemoved

`func (o *ScaleNsData) HasVnfInstanceToBeRemoved() bool`

HasVnfInstanceToBeRemoved returns a boolean if a field has been set.

### GetScaleNsByStepsData

`func (o *ScaleNsData) GetScaleNsByStepsData() ScaleNsByStepsData`

GetScaleNsByStepsData returns the ScaleNsByStepsData field if non-nil, zero value otherwise.

### GetScaleNsByStepsDataOk

`func (o *ScaleNsData) GetScaleNsByStepsDataOk() (*ScaleNsByStepsData, bool)`

GetScaleNsByStepsDataOk returns a tuple with the ScaleNsByStepsData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScaleNsByStepsData

`func (o *ScaleNsData) SetScaleNsByStepsData(v ScaleNsByStepsData)`

SetScaleNsByStepsData sets ScaleNsByStepsData field to given value.

### HasScaleNsByStepsData

`func (o *ScaleNsData) HasScaleNsByStepsData() bool`

HasScaleNsByStepsData returns a boolean if a field has been set.

### GetScaleNsToLevelData

`func (o *ScaleNsData) GetScaleNsToLevelData() ScaleNsToLevelData`

GetScaleNsToLevelData returns the ScaleNsToLevelData field if non-nil, zero value otherwise.

### GetScaleNsToLevelDataOk

`func (o *ScaleNsData) GetScaleNsToLevelDataOk() (*ScaleNsToLevelData, bool)`

GetScaleNsToLevelDataOk returns a tuple with the ScaleNsToLevelData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScaleNsToLevelData

`func (o *ScaleNsData) SetScaleNsToLevelData(v ScaleNsToLevelData)`

SetScaleNsToLevelData sets ScaleNsToLevelData field to given value.

### HasScaleNsToLevelData

`func (o *ScaleNsData) HasScaleNsToLevelData() bool`

HasScaleNsToLevelData returns a boolean if a field has been set.

### GetAdditionalParamsForNs

`func (o *ScaleNsData) GetAdditionalParamsForNs() ParamsForVnf`

GetAdditionalParamsForNs returns the AdditionalParamsForNs field if non-nil, zero value otherwise.

### GetAdditionalParamsForNsOk

`func (o *ScaleNsData) GetAdditionalParamsForNsOk() (*ParamsForVnf, bool)`

GetAdditionalParamsForNsOk returns a tuple with the AdditionalParamsForNs field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdditionalParamsForNs

`func (o *ScaleNsData) SetAdditionalParamsForNs(v ParamsForVnf)`

SetAdditionalParamsForNs sets AdditionalParamsForNs field to given value.

### HasAdditionalParamsForNs

`func (o *ScaleNsData) HasAdditionalParamsForNs() bool`

HasAdditionalParamsForNs returns a boolean if a field has been set.

### GetAdditionalParamsForVnf

`func (o *ScaleNsData) GetAdditionalParamsForVnf() []ParamsForVnf`

GetAdditionalParamsForVnf returns the AdditionalParamsForVnf field if non-nil, zero value otherwise.

### GetAdditionalParamsForVnfOk

`func (o *ScaleNsData) GetAdditionalParamsForVnfOk() (*[]ParamsForVnf, bool)`

GetAdditionalParamsForVnfOk returns a tuple with the AdditionalParamsForVnf field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdditionalParamsForVnf

`func (o *ScaleNsData) SetAdditionalParamsForVnf(v []ParamsForVnf)`

SetAdditionalParamsForVnf sets AdditionalParamsForVnf field to given value.

### HasAdditionalParamsForVnf

`func (o *ScaleNsData) HasAdditionalParamsForVnf() bool`

HasAdditionalParamsForVnf returns a boolean if a field has been set.

### GetLocationConstraints

`func (o *ScaleNsData) GetLocationConstraints() []VnfLocationConstraint`

GetLocationConstraints returns the LocationConstraints field if non-nil, zero value otherwise.

### GetLocationConstraintsOk

`func (o *ScaleNsData) GetLocationConstraintsOk() (*[]VnfLocationConstraint, bool)`

GetLocationConstraintsOk returns a tuple with the LocationConstraints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLocationConstraints

`func (o *ScaleNsData) SetLocationConstraints(v []VnfLocationConstraint)`

SetLocationConstraints sets LocationConstraints field to given value.

### HasLocationConstraints

`func (o *ScaleNsData) HasLocationConstraints() bool`

HasLocationConstraints returns a boolean if a field has been set.

### GetNestedNsLocationConstraints

`func (o *ScaleNsData) GetNestedNsLocationConstraints() []NestedNsLocationConstraint`

GetNestedNsLocationConstraints returns the NestedNsLocationConstraints field if non-nil, zero value otherwise.

### GetNestedNsLocationConstraintsOk

`func (o *ScaleNsData) GetNestedNsLocationConstraintsOk() (*[]NestedNsLocationConstraint, bool)`

GetNestedNsLocationConstraintsOk returns a tuple with the NestedNsLocationConstraints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNestedNsLocationConstraints

`func (o *ScaleNsData) SetNestedNsLocationConstraints(v []NestedNsLocationConstraint)`

SetNestedNsLocationConstraints sets NestedNsLocationConstraints field to given value.

### HasNestedNsLocationConstraints

`func (o *ScaleNsData) HasNestedNsLocationConstraints() bool`

HasNestedNsLocationConstraints returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


