# NsLcmOpOccRejectedLcmCoordinationsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CoordinationActionName** | **string** | An identifier with the intention of being globally unique.  | 
**RejectionTime** | **time.Time** | Date-time stamp. Representation: String formatted according toas defined by the date-time production in IETF RFC 3339.  | 
**EndpointType** | **string** | The endpoint type used by this coordination action. Valid values: - MGMT: coordination with other operation supporting management systems (e.g. OSS/BSS)  | 
**Delay** | **time.Time** | Date-time stamp. Representation: String formatted according toas defined by the date-time production in IETF RFC 3339.  | 

## Methods

### NewNsLcmOpOccRejectedLcmCoordinationsInner

`func NewNsLcmOpOccRejectedLcmCoordinationsInner(coordinationActionName string, rejectionTime time.Time, endpointType string, delay time.Time, ) *NsLcmOpOccRejectedLcmCoordinationsInner`

NewNsLcmOpOccRejectedLcmCoordinationsInner instantiates a new NsLcmOpOccRejectedLcmCoordinationsInner object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNsLcmOpOccRejectedLcmCoordinationsInnerWithDefaults

`func NewNsLcmOpOccRejectedLcmCoordinationsInnerWithDefaults() *NsLcmOpOccRejectedLcmCoordinationsInner`

NewNsLcmOpOccRejectedLcmCoordinationsInnerWithDefaults instantiates a new NsLcmOpOccRejectedLcmCoordinationsInner object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCoordinationActionName

`func (o *NsLcmOpOccRejectedLcmCoordinationsInner) GetCoordinationActionName() string`

GetCoordinationActionName returns the CoordinationActionName field if non-nil, zero value otherwise.

### GetCoordinationActionNameOk

`func (o *NsLcmOpOccRejectedLcmCoordinationsInner) GetCoordinationActionNameOk() (*string, bool)`

GetCoordinationActionNameOk returns a tuple with the CoordinationActionName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCoordinationActionName

`func (o *NsLcmOpOccRejectedLcmCoordinationsInner) SetCoordinationActionName(v string)`

SetCoordinationActionName sets CoordinationActionName field to given value.


### GetRejectionTime

`func (o *NsLcmOpOccRejectedLcmCoordinationsInner) GetRejectionTime() time.Time`

GetRejectionTime returns the RejectionTime field if non-nil, zero value otherwise.

### GetRejectionTimeOk

`func (o *NsLcmOpOccRejectedLcmCoordinationsInner) GetRejectionTimeOk() (*time.Time, bool)`

GetRejectionTimeOk returns a tuple with the RejectionTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRejectionTime

`func (o *NsLcmOpOccRejectedLcmCoordinationsInner) SetRejectionTime(v time.Time)`

SetRejectionTime sets RejectionTime field to given value.


### GetEndpointType

`func (o *NsLcmOpOccRejectedLcmCoordinationsInner) GetEndpointType() string`

GetEndpointType returns the EndpointType field if non-nil, zero value otherwise.

### GetEndpointTypeOk

`func (o *NsLcmOpOccRejectedLcmCoordinationsInner) GetEndpointTypeOk() (*string, bool)`

GetEndpointTypeOk returns a tuple with the EndpointType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEndpointType

`func (o *NsLcmOpOccRejectedLcmCoordinationsInner) SetEndpointType(v string)`

SetEndpointType sets EndpointType field to given value.


### GetDelay

`func (o *NsLcmOpOccRejectedLcmCoordinationsInner) GetDelay() time.Time`

GetDelay returns the Delay field if non-nil, zero value otherwise.

### GetDelayOk

`func (o *NsLcmOpOccRejectedLcmCoordinationsInner) GetDelayOk() (*time.Time, bool)`

GetDelayOk returns a tuple with the Delay field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDelay

`func (o *NsLcmOpOccRejectedLcmCoordinationsInner) SetDelay(v time.Time)`

SetDelay sets Delay field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


