# CancelModeType

## Enum


* `GRACEFUL` (value: `"GRACEFUL"`)

* `FORCEFUL` (value: `"FORCEFUL"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


