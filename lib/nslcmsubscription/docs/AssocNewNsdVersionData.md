# AssocNewNsdVersionData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NewNsdId** | **string** | An identifier with the intention of being globally unique.  | 
**Sync** | Pointer to **bool** | Specify whether the NS instance shall be automatically synchronized to the new NSD by the NFVO (in case of true value) or the NFVO shall not do any action (in case of a false value) and wait for further guidance from OSS/BSS (i.e. waiting for OSS/BSS to issue NS lifecycle management operation to explicitly add/remove VNFs and modify information of VNF instances according to the new NSD). The synchronization to the new NSD means e.g. instantiating/adding those VNFs whose VNFD is referenced by the new NSD version but not referenced by the old one, terminating/removing those VNFs whose VNFD is referenced by the old NSD version but not referenced by the new NSD version, modifying information of VNF instances to the new applicable VNFD provided in the new NSD version. A cardinality of 0 indicates that synchronization shall not be done.  | [optional] 

## Methods

### NewAssocNewNsdVersionData

`func NewAssocNewNsdVersionData(newNsdId string, ) *AssocNewNsdVersionData`

NewAssocNewNsdVersionData instantiates a new AssocNewNsdVersionData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAssocNewNsdVersionDataWithDefaults

`func NewAssocNewNsdVersionDataWithDefaults() *AssocNewNsdVersionData`

NewAssocNewNsdVersionDataWithDefaults instantiates a new AssocNewNsdVersionData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetNewNsdId

`func (o *AssocNewNsdVersionData) GetNewNsdId() string`

GetNewNsdId returns the NewNsdId field if non-nil, zero value otherwise.

### GetNewNsdIdOk

`func (o *AssocNewNsdVersionData) GetNewNsdIdOk() (*string, bool)`

GetNewNsdIdOk returns a tuple with the NewNsdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNewNsdId

`func (o *AssocNewNsdVersionData) SetNewNsdId(v string)`

SetNewNsdId sets NewNsdId field to given value.


### GetSync

`func (o *AssocNewNsdVersionData) GetSync() bool`

GetSync returns the Sync field if non-nil, zero value otherwise.

### GetSyncOk

`func (o *AssocNewNsdVersionData) GetSyncOk() (*bool, bool)`

GetSyncOk returns a tuple with the Sync field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSync

`func (o *AssocNewNsdVersionData) SetSync(v bool)`

SetSync sets Sync field to given value.

### HasSync

`func (o *AssocNewNsdVersionData) HasSync() bool`

HasSync returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


