# SiteToWanLayer3ProtocolDataRoutingInfoStaticRouting

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IpVersion** | **string** | The IP version applicable to the routing entry. Permitted values: - IPV4 - IPV6  | 
**IpPrefix** | **string** | An IPV4 or IPV6 address. Representation: In case of an IPV4 address, string that consists of four decimal integers separated by dots, each integer ranging from 0 to 255. In case of an IPV6 address, string that consists of groups of zero to four hexadecimal digits, separated by colons.  | 
**PrefixSize** | **float32** | The IP prefix size.  | 
**NextHop** | **string** | An IPV4 or IPV6 address. Representation: In case of an IPV4 address, string that consists of four decimal integers separated by dots, each integer ranging from 0 to 255. In case of an IPV6 address, string that consists of groups of zero to four hexadecimal digits, separated by colons.  | 

## Methods

### NewSiteToWanLayer3ProtocolDataRoutingInfoStaticRouting

`func NewSiteToWanLayer3ProtocolDataRoutingInfoStaticRouting(ipVersion string, ipPrefix string, prefixSize float32, nextHop string, ) *SiteToWanLayer3ProtocolDataRoutingInfoStaticRouting`

NewSiteToWanLayer3ProtocolDataRoutingInfoStaticRouting instantiates a new SiteToWanLayer3ProtocolDataRoutingInfoStaticRouting object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSiteToWanLayer3ProtocolDataRoutingInfoStaticRoutingWithDefaults

`func NewSiteToWanLayer3ProtocolDataRoutingInfoStaticRoutingWithDefaults() *SiteToWanLayer3ProtocolDataRoutingInfoStaticRouting`

NewSiteToWanLayer3ProtocolDataRoutingInfoStaticRoutingWithDefaults instantiates a new SiteToWanLayer3ProtocolDataRoutingInfoStaticRouting object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetIpVersion

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoStaticRouting) GetIpVersion() string`

GetIpVersion returns the IpVersion field if non-nil, zero value otherwise.

### GetIpVersionOk

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoStaticRouting) GetIpVersionOk() (*string, bool)`

GetIpVersionOk returns a tuple with the IpVersion field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIpVersion

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoStaticRouting) SetIpVersion(v string)`

SetIpVersion sets IpVersion field to given value.


### GetIpPrefix

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoStaticRouting) GetIpPrefix() string`

GetIpPrefix returns the IpPrefix field if non-nil, zero value otherwise.

### GetIpPrefixOk

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoStaticRouting) GetIpPrefixOk() (*string, bool)`

GetIpPrefixOk returns a tuple with the IpPrefix field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIpPrefix

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoStaticRouting) SetIpPrefix(v string)`

SetIpPrefix sets IpPrefix field to given value.


### GetPrefixSize

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoStaticRouting) GetPrefixSize() float32`

GetPrefixSize returns the PrefixSize field if non-nil, zero value otherwise.

### GetPrefixSizeOk

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoStaticRouting) GetPrefixSizeOk() (*float32, bool)`

GetPrefixSizeOk returns a tuple with the PrefixSize field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPrefixSize

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoStaticRouting) SetPrefixSize(v float32)`

SetPrefixSize sets PrefixSize field to given value.


### GetNextHop

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoStaticRouting) GetNextHop() string`

GetNextHop returns the NextHop field if non-nil, zero value otherwise.

### GetNextHopOk

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoStaticRouting) GetNextHopOk() (*string, bool)`

GetNextHopOk returns a tuple with the NextHop field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNextHop

`func (o *SiteToWanLayer3ProtocolDataRoutingInfoStaticRouting) SetNextHop(v string)`

SetNextHop sets NextHop field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


