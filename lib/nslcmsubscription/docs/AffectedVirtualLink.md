# AffectedVirtualLink

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NsVirtualLinkInstanceId** | **string** | An identifier that is unique with respect to a NS. Representation: string of variable length.  | 
**NsVirtualLinkDescId** | **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | 
**VlProfileId** | **string** | An identifier that is unique within a NS descriptor. Representation: string of variable length.  | 
**ChangeType** | **string** | Signals the type of change. Permitted values: * ADD * DELETE * MODIFY * ADD_LINK_PORT * REMOVE_LINK_PORT  | 
**LinkPortIds** | Pointer to **[]string** | Identifiers of the link ports of the affected VL related to the change. Each identifier references an \&quot;NsLinkPortInfo\&quot; structure. Shall be set when changeType is equal to \&quot;ADD_LINK_PORT\&quot; or \&quot;REMOVE_LINK_PORT\&quot;, and the related \&quot;NsLinkPortInfo\&quot; structures are present (case \&quot;add\&quot;) or have been present (case \&quot;remove\&quot;) in the \&quot;NsVirtualLinkInfo\&quot; structure that is represented by the \&quot;virtualLink¬Info\&quot; attribute in the \&quot;NsInstance\&quot; structure. See note.  | [optional] 
**ChangeResult** | **string** | Signals the result of change identified by the \&quot;changeType\&quot; attribute. Permitted values: * COMPLETED * ROLLED_BACK * FAILED  | 

## Methods

### NewAffectedVirtualLink

`func NewAffectedVirtualLink(nsVirtualLinkInstanceId string, nsVirtualLinkDescId string, vlProfileId string, changeType string, changeResult string, ) *AffectedVirtualLink`

NewAffectedVirtualLink instantiates a new AffectedVirtualLink object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAffectedVirtualLinkWithDefaults

`func NewAffectedVirtualLinkWithDefaults() *AffectedVirtualLink`

NewAffectedVirtualLinkWithDefaults instantiates a new AffectedVirtualLink object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetNsVirtualLinkInstanceId

`func (o *AffectedVirtualLink) GetNsVirtualLinkInstanceId() string`

GetNsVirtualLinkInstanceId returns the NsVirtualLinkInstanceId field if non-nil, zero value otherwise.

### GetNsVirtualLinkInstanceIdOk

`func (o *AffectedVirtualLink) GetNsVirtualLinkInstanceIdOk() (*string, bool)`

GetNsVirtualLinkInstanceIdOk returns a tuple with the NsVirtualLinkInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsVirtualLinkInstanceId

`func (o *AffectedVirtualLink) SetNsVirtualLinkInstanceId(v string)`

SetNsVirtualLinkInstanceId sets NsVirtualLinkInstanceId field to given value.


### GetNsVirtualLinkDescId

`func (o *AffectedVirtualLink) GetNsVirtualLinkDescId() string`

GetNsVirtualLinkDescId returns the NsVirtualLinkDescId field if non-nil, zero value otherwise.

### GetNsVirtualLinkDescIdOk

`func (o *AffectedVirtualLink) GetNsVirtualLinkDescIdOk() (*string, bool)`

GetNsVirtualLinkDescIdOk returns a tuple with the NsVirtualLinkDescId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsVirtualLinkDescId

`func (o *AffectedVirtualLink) SetNsVirtualLinkDescId(v string)`

SetNsVirtualLinkDescId sets NsVirtualLinkDescId field to given value.


### GetVlProfileId

`func (o *AffectedVirtualLink) GetVlProfileId() string`

GetVlProfileId returns the VlProfileId field if non-nil, zero value otherwise.

### GetVlProfileIdOk

`func (o *AffectedVirtualLink) GetVlProfileIdOk() (*string, bool)`

GetVlProfileIdOk returns a tuple with the VlProfileId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVlProfileId

`func (o *AffectedVirtualLink) SetVlProfileId(v string)`

SetVlProfileId sets VlProfileId field to given value.


### GetChangeType

`func (o *AffectedVirtualLink) GetChangeType() string`

GetChangeType returns the ChangeType field if non-nil, zero value otherwise.

### GetChangeTypeOk

`func (o *AffectedVirtualLink) GetChangeTypeOk() (*string, bool)`

GetChangeTypeOk returns a tuple with the ChangeType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChangeType

`func (o *AffectedVirtualLink) SetChangeType(v string)`

SetChangeType sets ChangeType field to given value.


### GetLinkPortIds

`func (o *AffectedVirtualLink) GetLinkPortIds() []string`

GetLinkPortIds returns the LinkPortIds field if non-nil, zero value otherwise.

### GetLinkPortIdsOk

`func (o *AffectedVirtualLink) GetLinkPortIdsOk() (*[]string, bool)`

GetLinkPortIdsOk returns a tuple with the LinkPortIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLinkPortIds

`func (o *AffectedVirtualLink) SetLinkPortIds(v []string)`

SetLinkPortIds sets LinkPortIds field to given value.

### HasLinkPortIds

`func (o *AffectedVirtualLink) HasLinkPortIds() bool`

HasLinkPortIds returns a boolean if a field has been set.

### GetChangeResult

`func (o *AffectedVirtualLink) GetChangeResult() string`

GetChangeResult returns the ChangeResult field if non-nil, zero value otherwise.

### GetChangeResultOk

`func (o *AffectedVirtualLink) GetChangeResultOk() (*string, bool)`

GetChangeResultOk returns a tuple with the ChangeResult field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChangeResult

`func (o *AffectedVirtualLink) SetChangeResult(v string)`

SetChangeResult sets ChangeResult field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


