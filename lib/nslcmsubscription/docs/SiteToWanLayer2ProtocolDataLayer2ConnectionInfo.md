# SiteToWanLayer2ProtocolDataLayer2ConnectionInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ConnectionType** | **string** | The type of connection to be established on the connectivity service point. Permitted values: - CSE: defined by the characteristics of the existing referred connectivity service point. - AGGREGATE_CSE: create an aggregation of the connectivity service endpoints.  | 
**InterfaceType** | **string** | To indicate whether to create logical interfaces on the referred connectivity service endpoint or new aggregated connectivity service endpoint. Permitted values: - PARENT: use the mapped interface to the connectivity service endpoint as is, i.e., do not create logical interfaces. - LOGICAL: create logical interfaces.  | 
**InterfaceTagging** | **string** | The type of frames to forward on the connectivity service point. Permitted values: - UNTAGGED: an interface where frames are not tagged. - TAGGED: an interface configured to forward tagged frames (i.e., enabled for VLAN tagging).  | 
**EncapsulationType** | **string** | The type of encapsulation. If the interfaceTagging&#x3D;\&quot;TAGGED\&quot;, either \&quot;VLAN\&quot; or \&quot;VXLAN\&quot; shall be set. Permitted values: - ETH: generic Ethernet encapsulation. - VLAN: encapsulation based on VLAN. - VXLAN: encapsulation based on VXLAN.  | 
**VlanTaggingType** | Pointer to **string** | Type of encapsulation method for VLAN tagging. Shall be present if interfaceTagging&#x3D;\&quot;TAGGED\&quot; and encapsulationType&#x3D;\&quot;VLAN\&quot;. Permitted values: - DOT1Q: used when packets on the CSE are encapsulated with one or a set of customer VLAN identifiers. - QINQ: used when packets on the CSE are encapsulated with multiple customer VLAN identifiers and a single   service VLAN identifier. - QINANY: used when packets on the CSE have no specific customer VLAN and a service VLAN identifier is used.  | [optional] 
**WanSegmentIds** | Pointer to [**SiteToWanLayer2ProtocolDataLayer2ConnectionInfoWanSegmentIds**](SiteToWanLayer2ProtocolDataLayer2ConnectionInfoWanSegmentIds.md) |  | [optional] 
**VxlanConfig** | Pointer to [**SiteToWanLayer2ProtocolDataLayer2ConnectionInfoVxlanConfig**](SiteToWanLayer2ProtocolDataLayer2ConnectionInfoVxlanConfig.md) |  | [optional] 
**LagInterfaceData** | Pointer to [**SiteToWanLayer2ProtocolDataLayer2ConnectionInfoLagInterfaceData**](SiteToWanLayer2ProtocolDataLayer2ConnectionInfoLagInterfaceData.md) |  | [optional] 
**Layer2ControlProtocol** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 

## Methods

### NewSiteToWanLayer2ProtocolDataLayer2ConnectionInfo

`func NewSiteToWanLayer2ProtocolDataLayer2ConnectionInfo(connectionType string, interfaceType string, interfaceTagging string, encapsulationType string, ) *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo`

NewSiteToWanLayer2ProtocolDataLayer2ConnectionInfo instantiates a new SiteToWanLayer2ProtocolDataLayer2ConnectionInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSiteToWanLayer2ProtocolDataLayer2ConnectionInfoWithDefaults

`func NewSiteToWanLayer2ProtocolDataLayer2ConnectionInfoWithDefaults() *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo`

NewSiteToWanLayer2ProtocolDataLayer2ConnectionInfoWithDefaults instantiates a new SiteToWanLayer2ProtocolDataLayer2ConnectionInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetConnectionType

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) GetConnectionType() string`

GetConnectionType returns the ConnectionType field if non-nil, zero value otherwise.

### GetConnectionTypeOk

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) GetConnectionTypeOk() (*string, bool)`

GetConnectionTypeOk returns a tuple with the ConnectionType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConnectionType

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) SetConnectionType(v string)`

SetConnectionType sets ConnectionType field to given value.


### GetInterfaceType

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) GetInterfaceType() string`

GetInterfaceType returns the InterfaceType field if non-nil, zero value otherwise.

### GetInterfaceTypeOk

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) GetInterfaceTypeOk() (*string, bool)`

GetInterfaceTypeOk returns a tuple with the InterfaceType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInterfaceType

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) SetInterfaceType(v string)`

SetInterfaceType sets InterfaceType field to given value.


### GetInterfaceTagging

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) GetInterfaceTagging() string`

GetInterfaceTagging returns the InterfaceTagging field if non-nil, zero value otherwise.

### GetInterfaceTaggingOk

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) GetInterfaceTaggingOk() (*string, bool)`

GetInterfaceTaggingOk returns a tuple with the InterfaceTagging field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInterfaceTagging

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) SetInterfaceTagging(v string)`

SetInterfaceTagging sets InterfaceTagging field to given value.


### GetEncapsulationType

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) GetEncapsulationType() string`

GetEncapsulationType returns the EncapsulationType field if non-nil, zero value otherwise.

### GetEncapsulationTypeOk

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) GetEncapsulationTypeOk() (*string, bool)`

GetEncapsulationTypeOk returns a tuple with the EncapsulationType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEncapsulationType

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) SetEncapsulationType(v string)`

SetEncapsulationType sets EncapsulationType field to given value.


### GetVlanTaggingType

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) GetVlanTaggingType() string`

GetVlanTaggingType returns the VlanTaggingType field if non-nil, zero value otherwise.

### GetVlanTaggingTypeOk

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) GetVlanTaggingTypeOk() (*string, bool)`

GetVlanTaggingTypeOk returns a tuple with the VlanTaggingType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVlanTaggingType

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) SetVlanTaggingType(v string)`

SetVlanTaggingType sets VlanTaggingType field to given value.

### HasVlanTaggingType

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) HasVlanTaggingType() bool`

HasVlanTaggingType returns a boolean if a field has been set.

### GetWanSegmentIds

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) GetWanSegmentIds() SiteToWanLayer2ProtocolDataLayer2ConnectionInfoWanSegmentIds`

GetWanSegmentIds returns the WanSegmentIds field if non-nil, zero value otherwise.

### GetWanSegmentIdsOk

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) GetWanSegmentIdsOk() (*SiteToWanLayer2ProtocolDataLayer2ConnectionInfoWanSegmentIds, bool)`

GetWanSegmentIdsOk returns a tuple with the WanSegmentIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWanSegmentIds

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) SetWanSegmentIds(v SiteToWanLayer2ProtocolDataLayer2ConnectionInfoWanSegmentIds)`

SetWanSegmentIds sets WanSegmentIds field to given value.

### HasWanSegmentIds

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) HasWanSegmentIds() bool`

HasWanSegmentIds returns a boolean if a field has been set.

### GetVxlanConfig

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) GetVxlanConfig() SiteToWanLayer2ProtocolDataLayer2ConnectionInfoVxlanConfig`

GetVxlanConfig returns the VxlanConfig field if non-nil, zero value otherwise.

### GetVxlanConfigOk

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) GetVxlanConfigOk() (*SiteToWanLayer2ProtocolDataLayer2ConnectionInfoVxlanConfig, bool)`

GetVxlanConfigOk returns a tuple with the VxlanConfig field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVxlanConfig

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) SetVxlanConfig(v SiteToWanLayer2ProtocolDataLayer2ConnectionInfoVxlanConfig)`

SetVxlanConfig sets VxlanConfig field to given value.

### HasVxlanConfig

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) HasVxlanConfig() bool`

HasVxlanConfig returns a boolean if a field has been set.

### GetLagInterfaceData

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) GetLagInterfaceData() SiteToWanLayer2ProtocolDataLayer2ConnectionInfoLagInterfaceData`

GetLagInterfaceData returns the LagInterfaceData field if non-nil, zero value otherwise.

### GetLagInterfaceDataOk

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) GetLagInterfaceDataOk() (*SiteToWanLayer2ProtocolDataLayer2ConnectionInfoLagInterfaceData, bool)`

GetLagInterfaceDataOk returns a tuple with the LagInterfaceData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLagInterfaceData

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) SetLagInterfaceData(v SiteToWanLayer2ProtocolDataLayer2ConnectionInfoLagInterfaceData)`

SetLagInterfaceData sets LagInterfaceData field to given value.

### HasLagInterfaceData

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) HasLagInterfaceData() bool`

HasLagInterfaceData returns a boolean if a field has been set.

### GetLayer2ControlProtocol

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) GetLayer2ControlProtocol() map[string]interface{}`

GetLayer2ControlProtocol returns the Layer2ControlProtocol field if non-nil, zero value otherwise.

### GetLayer2ControlProtocolOk

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) GetLayer2ControlProtocolOk() (*map[string]interface{}, bool)`

GetLayer2ControlProtocolOk returns a tuple with the Layer2ControlProtocol field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLayer2ControlProtocol

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) SetLayer2ControlProtocol(v map[string]interface{})`

SetLayer2ControlProtocol sets Layer2ControlProtocol field to given value.

### HasLayer2ControlProtocol

`func (o *SiteToWanLayer2ProtocolDataLayer2ConnectionInfo) HasLayer2ControlProtocol() bool`

HasLayer2ControlProtocol returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


