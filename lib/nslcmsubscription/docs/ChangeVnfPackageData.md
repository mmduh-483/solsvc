# ChangeVnfPackageData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VnfInstanceId** | **string** | An identifier with the intention of being globally unique.  | 
**VnfdId** | **string** | An identifier with the intention of being globally unique.  | 
**ExtVirtualLinks** | Pointer to [**[]ExtVirtualLinkData**](ExtVirtualLinkData.md) | Information about external VLs to connect the VNF to. Entries in the list that are unchanged need not be supplied as part of this request.  | [optional] 
**ExtManagedVirtualLinks** | Pointer to [**[]ExtManagedVirtualLinkData**](ExtManagedVirtualLinkData.md) | Information about internal VLs that are managed by the NFVO.  | [optional] 
**AdditionalParams** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 
**Extensions** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 
**VnfConfigurableProperties** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 

## Methods

### NewChangeVnfPackageData

`func NewChangeVnfPackageData(vnfInstanceId string, vnfdId string, ) *ChangeVnfPackageData`

NewChangeVnfPackageData instantiates a new ChangeVnfPackageData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewChangeVnfPackageDataWithDefaults

`func NewChangeVnfPackageDataWithDefaults() *ChangeVnfPackageData`

NewChangeVnfPackageDataWithDefaults instantiates a new ChangeVnfPackageData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVnfInstanceId

`func (o *ChangeVnfPackageData) GetVnfInstanceId() string`

GetVnfInstanceId returns the VnfInstanceId field if non-nil, zero value otherwise.

### GetVnfInstanceIdOk

`func (o *ChangeVnfPackageData) GetVnfInstanceIdOk() (*string, bool)`

GetVnfInstanceIdOk returns a tuple with the VnfInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceId

`func (o *ChangeVnfPackageData) SetVnfInstanceId(v string)`

SetVnfInstanceId sets VnfInstanceId field to given value.


### GetVnfdId

`func (o *ChangeVnfPackageData) GetVnfdId() string`

GetVnfdId returns the VnfdId field if non-nil, zero value otherwise.

### GetVnfdIdOk

`func (o *ChangeVnfPackageData) GetVnfdIdOk() (*string, bool)`

GetVnfdIdOk returns a tuple with the VnfdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfdId

`func (o *ChangeVnfPackageData) SetVnfdId(v string)`

SetVnfdId sets VnfdId field to given value.


### GetExtVirtualLinks

`func (o *ChangeVnfPackageData) GetExtVirtualLinks() []ExtVirtualLinkData`

GetExtVirtualLinks returns the ExtVirtualLinks field if non-nil, zero value otherwise.

### GetExtVirtualLinksOk

`func (o *ChangeVnfPackageData) GetExtVirtualLinksOk() (*[]ExtVirtualLinkData, bool)`

GetExtVirtualLinksOk returns a tuple with the ExtVirtualLinks field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtVirtualLinks

`func (o *ChangeVnfPackageData) SetExtVirtualLinks(v []ExtVirtualLinkData)`

SetExtVirtualLinks sets ExtVirtualLinks field to given value.

### HasExtVirtualLinks

`func (o *ChangeVnfPackageData) HasExtVirtualLinks() bool`

HasExtVirtualLinks returns a boolean if a field has been set.

### GetExtManagedVirtualLinks

`func (o *ChangeVnfPackageData) GetExtManagedVirtualLinks() []ExtManagedVirtualLinkData`

GetExtManagedVirtualLinks returns the ExtManagedVirtualLinks field if non-nil, zero value otherwise.

### GetExtManagedVirtualLinksOk

`func (o *ChangeVnfPackageData) GetExtManagedVirtualLinksOk() (*[]ExtManagedVirtualLinkData, bool)`

GetExtManagedVirtualLinksOk returns a tuple with the ExtManagedVirtualLinks field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtManagedVirtualLinks

`func (o *ChangeVnfPackageData) SetExtManagedVirtualLinks(v []ExtManagedVirtualLinkData)`

SetExtManagedVirtualLinks sets ExtManagedVirtualLinks field to given value.

### HasExtManagedVirtualLinks

`func (o *ChangeVnfPackageData) HasExtManagedVirtualLinks() bool`

HasExtManagedVirtualLinks returns a boolean if a field has been set.

### GetAdditionalParams

`func (o *ChangeVnfPackageData) GetAdditionalParams() map[string]interface{}`

GetAdditionalParams returns the AdditionalParams field if non-nil, zero value otherwise.

### GetAdditionalParamsOk

`func (o *ChangeVnfPackageData) GetAdditionalParamsOk() (*map[string]interface{}, bool)`

GetAdditionalParamsOk returns a tuple with the AdditionalParams field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdditionalParams

`func (o *ChangeVnfPackageData) SetAdditionalParams(v map[string]interface{})`

SetAdditionalParams sets AdditionalParams field to given value.

### HasAdditionalParams

`func (o *ChangeVnfPackageData) HasAdditionalParams() bool`

HasAdditionalParams returns a boolean if a field has been set.

### GetExtensions

`func (o *ChangeVnfPackageData) GetExtensions() map[string]interface{}`

GetExtensions returns the Extensions field if non-nil, zero value otherwise.

### GetExtensionsOk

`func (o *ChangeVnfPackageData) GetExtensionsOk() (*map[string]interface{}, bool)`

GetExtensionsOk returns a tuple with the Extensions field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExtensions

`func (o *ChangeVnfPackageData) SetExtensions(v map[string]interface{})`

SetExtensions sets Extensions field to given value.

### HasExtensions

`func (o *ChangeVnfPackageData) HasExtensions() bool`

HasExtensions returns a boolean if a field has been set.

### GetVnfConfigurableProperties

`func (o *ChangeVnfPackageData) GetVnfConfigurableProperties() map[string]interface{}`

GetVnfConfigurableProperties returns the VnfConfigurableProperties field if non-nil, zero value otherwise.

### GetVnfConfigurablePropertiesOk

`func (o *ChangeVnfPackageData) GetVnfConfigurablePropertiesOk() (*map[string]interface{}, bool)`

GetVnfConfigurablePropertiesOk returns a tuple with the VnfConfigurableProperties field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfConfigurableProperties

`func (o *ChangeVnfPackageData) SetVnfConfigurableProperties(v map[string]interface{})`

SetVnfConfigurableProperties sets VnfConfigurableProperties field to given value.

### HasVnfConfigurableProperties

`func (o *ChangeVnfPackageData) HasVnfConfigurableProperties() bool`

HasVnfConfigurableProperties returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


