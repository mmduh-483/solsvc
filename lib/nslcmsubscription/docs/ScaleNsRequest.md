# ScaleNsRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ScaleType** | **string** | Indicates the type of scaling to be performed. Possible values: - SCALE_NS - SCALE_VNF  | 
**ScaleNsData** | Pointer to [**ScaleNsData**](ScaleNsData.md) |  | [optional] 
**ScaleVnfData** | Pointer to [**[]ScaleVnfData**](ScaleVnfData.md) | The necessary information to scale the referenced NS instance. It shall be present when scaleType &#x3D; SCALE_VNF. See note.  | [optional] 
**ScaleTime** | Pointer to **time.Time** | Date-time stamp. Representation: String formatted according toas defined by the date-time production in IETF RFC 3339.  | [optional] 

## Methods

### NewScaleNsRequest

`func NewScaleNsRequest(scaleType string, ) *ScaleNsRequest`

NewScaleNsRequest instantiates a new ScaleNsRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewScaleNsRequestWithDefaults

`func NewScaleNsRequestWithDefaults() *ScaleNsRequest`

NewScaleNsRequestWithDefaults instantiates a new ScaleNsRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetScaleType

`func (o *ScaleNsRequest) GetScaleType() string`

GetScaleType returns the ScaleType field if non-nil, zero value otherwise.

### GetScaleTypeOk

`func (o *ScaleNsRequest) GetScaleTypeOk() (*string, bool)`

GetScaleTypeOk returns a tuple with the ScaleType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScaleType

`func (o *ScaleNsRequest) SetScaleType(v string)`

SetScaleType sets ScaleType field to given value.


### GetScaleNsData

`func (o *ScaleNsRequest) GetScaleNsData() ScaleNsData`

GetScaleNsData returns the ScaleNsData field if non-nil, zero value otherwise.

### GetScaleNsDataOk

`func (o *ScaleNsRequest) GetScaleNsDataOk() (*ScaleNsData, bool)`

GetScaleNsDataOk returns a tuple with the ScaleNsData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScaleNsData

`func (o *ScaleNsRequest) SetScaleNsData(v ScaleNsData)`

SetScaleNsData sets ScaleNsData field to given value.

### HasScaleNsData

`func (o *ScaleNsRequest) HasScaleNsData() bool`

HasScaleNsData returns a boolean if a field has been set.

### GetScaleVnfData

`func (o *ScaleNsRequest) GetScaleVnfData() []ScaleVnfData`

GetScaleVnfData returns the ScaleVnfData field if non-nil, zero value otherwise.

### GetScaleVnfDataOk

`func (o *ScaleNsRequest) GetScaleVnfDataOk() (*[]ScaleVnfData, bool)`

GetScaleVnfDataOk returns a tuple with the ScaleVnfData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScaleVnfData

`func (o *ScaleNsRequest) SetScaleVnfData(v []ScaleVnfData)`

SetScaleVnfData sets ScaleVnfData field to given value.

### HasScaleVnfData

`func (o *ScaleNsRequest) HasScaleVnfData() bool`

HasScaleVnfData returns a boolean if a field has been set.

### GetScaleTime

`func (o *ScaleNsRequest) GetScaleTime() time.Time`

GetScaleTime returns the ScaleTime field if non-nil, zero value otherwise.

### GetScaleTimeOk

`func (o *ScaleNsRequest) GetScaleTimeOk() (*time.Time, bool)`

GetScaleTimeOk returns a tuple with the ScaleTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetScaleTime

`func (o *ScaleNsRequest) SetScaleTime(v time.Time)`

SetScaleTime sets ScaleTime field to given value.

### HasScaleTime

`func (o *ScaleNsRequest) HasScaleTime() bool`

HasScaleTime returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


