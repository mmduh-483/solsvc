# NsLinkPortInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | An identifier with the intention of being globally unique.  | 
**ResourceHandle** | [**ResourceHandle**](ResourceHandle.md) |  | 
**NsCpHandle** | Pointer to [**NsCpHandle**](NsCpHandle.md) |  | [optional] 

## Methods

### NewNsLinkPortInfo

`func NewNsLinkPortInfo(id string, resourceHandle ResourceHandle, ) *NsLinkPortInfo`

NewNsLinkPortInfo instantiates a new NsLinkPortInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNsLinkPortInfoWithDefaults

`func NewNsLinkPortInfoWithDefaults() *NsLinkPortInfo`

NewNsLinkPortInfoWithDefaults instantiates a new NsLinkPortInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *NsLinkPortInfo) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *NsLinkPortInfo) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *NsLinkPortInfo) SetId(v string)`

SetId sets Id field to given value.


### GetResourceHandle

`func (o *NsLinkPortInfo) GetResourceHandle() ResourceHandle`

GetResourceHandle returns the ResourceHandle field if non-nil, zero value otherwise.

### GetResourceHandleOk

`func (o *NsLinkPortInfo) GetResourceHandleOk() (*ResourceHandle, bool)`

GetResourceHandleOk returns a tuple with the ResourceHandle field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResourceHandle

`func (o *NsLinkPortInfo) SetResourceHandle(v ResourceHandle)`

SetResourceHandle sets ResourceHandle field to given value.


### GetNsCpHandle

`func (o *NsLinkPortInfo) GetNsCpHandle() NsCpHandle`

GetNsCpHandle returns the NsCpHandle field if non-nil, zero value otherwise.

### GetNsCpHandleOk

`func (o *NsLinkPortInfo) GetNsCpHandleOk() (*NsCpHandle, bool)`

GetNsCpHandleOk returns a tuple with the NsCpHandle field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsCpHandle

`func (o *NsLinkPortInfo) SetNsCpHandle(v NsCpHandle)`

SetNsCpHandle sets NsCpHandle field to given value.

### HasNsCpHandle

`func (o *NsLinkPortInfo) HasNsCpHandle() bool`

HasNsCpHandle returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


