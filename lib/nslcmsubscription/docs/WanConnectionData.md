# WanConnectionData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NsVirtualLink** | Pointer to **map[string]interface{}** | Information used to identify the NS VL for which the WAN connectivity data is applicable. See note.  | [optional] 
**VnfVirtualLink** | Pointer to **map[string]interface{}** | Information used to identify the VNF VL for which the WAN connectivity data is applicable. Either a \&quot;nsVirtualLink\&quot; or a \&quot;vnfVirtualLink\&quot; shall be provided, but not both.  | [optional] 
**ProtocolData** | [**WanConnectionProtocolData**](WanConnectionProtocolData.md) |  | 

## Methods

### NewWanConnectionData

`func NewWanConnectionData(protocolData WanConnectionProtocolData, ) *WanConnectionData`

NewWanConnectionData instantiates a new WanConnectionData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewWanConnectionDataWithDefaults

`func NewWanConnectionDataWithDefaults() *WanConnectionData`

NewWanConnectionDataWithDefaults instantiates a new WanConnectionData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetNsVirtualLink

`func (o *WanConnectionData) GetNsVirtualLink() map[string]interface{}`

GetNsVirtualLink returns the NsVirtualLink field if non-nil, zero value otherwise.

### GetNsVirtualLinkOk

`func (o *WanConnectionData) GetNsVirtualLinkOk() (*map[string]interface{}, bool)`

GetNsVirtualLinkOk returns a tuple with the NsVirtualLink field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsVirtualLink

`func (o *WanConnectionData) SetNsVirtualLink(v map[string]interface{})`

SetNsVirtualLink sets NsVirtualLink field to given value.

### HasNsVirtualLink

`func (o *WanConnectionData) HasNsVirtualLink() bool`

HasNsVirtualLink returns a boolean if a field has been set.

### GetVnfVirtualLink

`func (o *WanConnectionData) GetVnfVirtualLink() map[string]interface{}`

GetVnfVirtualLink returns the VnfVirtualLink field if non-nil, zero value otherwise.

### GetVnfVirtualLinkOk

`func (o *WanConnectionData) GetVnfVirtualLinkOk() (*map[string]interface{}, bool)`

GetVnfVirtualLinkOk returns a tuple with the VnfVirtualLink field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfVirtualLink

`func (o *WanConnectionData) SetVnfVirtualLink(v map[string]interface{})`

SetVnfVirtualLink sets VnfVirtualLink field to given value.

### HasVnfVirtualLink

`func (o *WanConnectionData) HasVnfVirtualLink() bool`

HasVnfVirtualLink returns a boolean if a field has been set.

### GetProtocolData

`func (o *WanConnectionData) GetProtocolData() WanConnectionProtocolData`

GetProtocolData returns the ProtocolData field if non-nil, zero value otherwise.

### GetProtocolDataOk

`func (o *WanConnectionData) GetProtocolDataOk() (*WanConnectionProtocolData, bool)`

GetProtocolDataOk returns a tuple with the ProtocolData field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProtocolData

`func (o *WanConnectionData) SetProtocolData(v WanConnectionProtocolData)`

SetProtocolData sets ProtocolData field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


