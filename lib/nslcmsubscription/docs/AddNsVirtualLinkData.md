# AddNsVirtualLinkData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NsVirtualLinkProfileId** | **string** | An identifier with the intention of being globally unique.  | 

## Methods

### NewAddNsVirtualLinkData

`func NewAddNsVirtualLinkData(nsVirtualLinkProfileId string, ) *AddNsVirtualLinkData`

NewAddNsVirtualLinkData instantiates a new AddNsVirtualLinkData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAddNsVirtualLinkDataWithDefaults

`func NewAddNsVirtualLinkDataWithDefaults() *AddNsVirtualLinkData`

NewAddNsVirtualLinkDataWithDefaults instantiates a new AddNsVirtualLinkData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetNsVirtualLinkProfileId

`func (o *AddNsVirtualLinkData) GetNsVirtualLinkProfileId() string`

GetNsVirtualLinkProfileId returns the NsVirtualLinkProfileId field if non-nil, zero value otherwise.

### GetNsVirtualLinkProfileIdOk

`func (o *AddNsVirtualLinkData) GetNsVirtualLinkProfileIdOk() (*string, bool)`

GetNsVirtualLinkProfileIdOk returns a tuple with the NsVirtualLinkProfileId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsVirtualLinkProfileId

`func (o *AddNsVirtualLinkData) SetNsVirtualLinkProfileId(v string)`

SetNsVirtualLinkProfileId sets NsVirtualLinkProfileId field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


