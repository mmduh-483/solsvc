# AffectedSap

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**SapInstanceId** | **string** | An identifier with the intention of being globally unique.  | 
**SapdId** | **string** | An identifier with the intention of being globally unique.  | 
**SapName** | Pointer to **string** | Human readable name for the SAP.  | [optional] 
**ChangeType** | Pointer to **string** | Signals the type of lifecycle change. Permitted values: - ADD - REMOVE - MODIFY  | [optional] 
**ChangeResult** | Pointer to **string** | Signals the result of change identified by the \&quot;changeType\&quot; attribute. Permitted values: - COMPLETED - ROLLED_BACK - FAILED  | [optional] 

## Methods

### NewAffectedSap

`func NewAffectedSap(sapInstanceId string, sapdId string, ) *AffectedSap`

NewAffectedSap instantiates a new AffectedSap object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAffectedSapWithDefaults

`func NewAffectedSapWithDefaults() *AffectedSap`

NewAffectedSapWithDefaults instantiates a new AffectedSap object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSapInstanceId

`func (o *AffectedSap) GetSapInstanceId() string`

GetSapInstanceId returns the SapInstanceId field if non-nil, zero value otherwise.

### GetSapInstanceIdOk

`func (o *AffectedSap) GetSapInstanceIdOk() (*string, bool)`

GetSapInstanceIdOk returns a tuple with the SapInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSapInstanceId

`func (o *AffectedSap) SetSapInstanceId(v string)`

SetSapInstanceId sets SapInstanceId field to given value.


### GetSapdId

`func (o *AffectedSap) GetSapdId() string`

GetSapdId returns the SapdId field if non-nil, zero value otherwise.

### GetSapdIdOk

`func (o *AffectedSap) GetSapdIdOk() (*string, bool)`

GetSapdIdOk returns a tuple with the SapdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSapdId

`func (o *AffectedSap) SetSapdId(v string)`

SetSapdId sets SapdId field to given value.


### GetSapName

`func (o *AffectedSap) GetSapName() string`

GetSapName returns the SapName field if non-nil, zero value otherwise.

### GetSapNameOk

`func (o *AffectedSap) GetSapNameOk() (*string, bool)`

GetSapNameOk returns a tuple with the SapName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSapName

`func (o *AffectedSap) SetSapName(v string)`

SetSapName sets SapName field to given value.

### HasSapName

`func (o *AffectedSap) HasSapName() bool`

HasSapName returns a boolean if a field has been set.

### GetChangeType

`func (o *AffectedSap) GetChangeType() string`

GetChangeType returns the ChangeType field if non-nil, zero value otherwise.

### GetChangeTypeOk

`func (o *AffectedSap) GetChangeTypeOk() (*string, bool)`

GetChangeTypeOk returns a tuple with the ChangeType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChangeType

`func (o *AffectedSap) SetChangeType(v string)`

SetChangeType sets ChangeType field to given value.

### HasChangeType

`func (o *AffectedSap) HasChangeType() bool`

HasChangeType returns a boolean if a field has been set.

### GetChangeResult

`func (o *AffectedSap) GetChangeResult() string`

GetChangeResult returns the ChangeResult field if non-nil, zero value otherwise.

### GetChangeResultOk

`func (o *AffectedSap) GetChangeResultOk() (*string, bool)`

GetChangeResultOk returns a tuple with the ChangeResult field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetChangeResult

`func (o *AffectedSap) SetChangeResult(v string)`

SetChangeResult sets ChangeResult field to given value.

### HasChangeResult

`func (o *AffectedSap) HasChangeResult() bool`

HasChangeResult returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


