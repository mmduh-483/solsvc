# RevertVnfToSnapshotData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VnfInstanceId** | **string** | An identifier with the intention of being globally unique.  | 
**VnfSnapshotInfoId** | **string** | An identifier with the intention of being globally unique.  | 
**AdditionalParams** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 

## Methods

### NewRevertVnfToSnapshotData

`func NewRevertVnfToSnapshotData(vnfInstanceId string, vnfSnapshotInfoId string, ) *RevertVnfToSnapshotData`

NewRevertVnfToSnapshotData instantiates a new RevertVnfToSnapshotData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewRevertVnfToSnapshotDataWithDefaults

`func NewRevertVnfToSnapshotDataWithDefaults() *RevertVnfToSnapshotData`

NewRevertVnfToSnapshotDataWithDefaults instantiates a new RevertVnfToSnapshotData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVnfInstanceId

`func (o *RevertVnfToSnapshotData) GetVnfInstanceId() string`

GetVnfInstanceId returns the VnfInstanceId field if non-nil, zero value otherwise.

### GetVnfInstanceIdOk

`func (o *RevertVnfToSnapshotData) GetVnfInstanceIdOk() (*string, bool)`

GetVnfInstanceIdOk returns a tuple with the VnfInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfInstanceId

`func (o *RevertVnfToSnapshotData) SetVnfInstanceId(v string)`

SetVnfInstanceId sets VnfInstanceId field to given value.


### GetVnfSnapshotInfoId

`func (o *RevertVnfToSnapshotData) GetVnfSnapshotInfoId() string`

GetVnfSnapshotInfoId returns the VnfSnapshotInfoId field if non-nil, zero value otherwise.

### GetVnfSnapshotInfoIdOk

`func (o *RevertVnfToSnapshotData) GetVnfSnapshotInfoIdOk() (*string, bool)`

GetVnfSnapshotInfoIdOk returns a tuple with the VnfSnapshotInfoId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfSnapshotInfoId

`func (o *RevertVnfToSnapshotData) SetVnfSnapshotInfoId(v string)`

SetVnfSnapshotInfoId sets VnfSnapshotInfoId field to given value.


### GetAdditionalParams

`func (o *RevertVnfToSnapshotData) GetAdditionalParams() map[string]interface{}`

GetAdditionalParams returns the AdditionalParams field if non-nil, zero value otherwise.

### GetAdditionalParamsOk

`func (o *RevertVnfToSnapshotData) GetAdditionalParamsOk() (*map[string]interface{}, bool)`

GetAdditionalParamsOk returns a tuple with the AdditionalParams field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAdditionalParams

`func (o *RevertVnfToSnapshotData) SetAdditionalParams(v map[string]interface{})`

SetAdditionalParams sets AdditionalParams field to given value.

### HasAdditionalParams

`func (o *RevertVnfToSnapshotData) HasAdditionalParams() bool`

HasAdditionalParams returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


