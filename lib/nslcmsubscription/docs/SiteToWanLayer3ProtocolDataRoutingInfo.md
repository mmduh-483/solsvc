# SiteToWanLayer3ProtocolDataRoutingInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**RoutingProtocol** | **string** | The routing protocol that is activated on the connectivity service endpoint. Permitted values: - BGP: used for dynamic routing BGPv4. - RIP: used for dynamic routing RIPv2. - OSPF: used for dynamic routing (OSPF version 2 for IPv4; and OSPF version 3 for IPv6). - STATIC: used for static routing. - DIRECT: used when the NFVI-PoP network is directly connected to the WAN provider network. - VRRP: used when the NFVI-PoP network is directly connected to the WAN provider network with virtual   router redundancy protocol support (VRRP).  | 
**StaticRouting** | Pointer to [**SiteToWanLayer3ProtocolDataRoutingInfoStaticRouting**](SiteToWanLayer3ProtocolDataRoutingInfoStaticRouting.md) |  | [optional] 
**RoutingAddressFamily** | **string** | The IP version applicable to the dynamic routing protocol. Shall be present for dynamic routing protocols. Permitted values: - IPV4 - IPV6  | 
**OspfRouting** | Pointer to [**SiteToWanLayer3ProtocolDataRoutingInfoOspfRouting**](SiteToWanLayer3ProtocolDataRoutingInfoOspfRouting.md) |  | [optional] 
**BgpRouting** | Pointer to [**SiteToWanLayer3ProtocolDataRoutingInfoBgpRouting**](SiteToWanLayer3ProtocolDataRoutingInfoBgpRouting.md) |  | [optional] 
**RouteMapsDistribution** | Pointer to [**SiteToWanLayer3ProtocolDataRoutingInfoRouteMapsDistribution**](SiteToWanLayer3ProtocolDataRoutingInfoRouteMapsDistribution.md) |  | [optional] 

## Methods

### NewSiteToWanLayer3ProtocolDataRoutingInfo

`func NewSiteToWanLayer3ProtocolDataRoutingInfo(routingProtocol string, routingAddressFamily string, ) *SiteToWanLayer3ProtocolDataRoutingInfo`

NewSiteToWanLayer3ProtocolDataRoutingInfo instantiates a new SiteToWanLayer3ProtocolDataRoutingInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSiteToWanLayer3ProtocolDataRoutingInfoWithDefaults

`func NewSiteToWanLayer3ProtocolDataRoutingInfoWithDefaults() *SiteToWanLayer3ProtocolDataRoutingInfo`

NewSiteToWanLayer3ProtocolDataRoutingInfoWithDefaults instantiates a new SiteToWanLayer3ProtocolDataRoutingInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetRoutingProtocol

`func (o *SiteToWanLayer3ProtocolDataRoutingInfo) GetRoutingProtocol() string`

GetRoutingProtocol returns the RoutingProtocol field if non-nil, zero value otherwise.

### GetRoutingProtocolOk

`func (o *SiteToWanLayer3ProtocolDataRoutingInfo) GetRoutingProtocolOk() (*string, bool)`

GetRoutingProtocolOk returns a tuple with the RoutingProtocol field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRoutingProtocol

`func (o *SiteToWanLayer3ProtocolDataRoutingInfo) SetRoutingProtocol(v string)`

SetRoutingProtocol sets RoutingProtocol field to given value.


### GetStaticRouting

`func (o *SiteToWanLayer3ProtocolDataRoutingInfo) GetStaticRouting() SiteToWanLayer3ProtocolDataRoutingInfoStaticRouting`

GetStaticRouting returns the StaticRouting field if non-nil, zero value otherwise.

### GetStaticRoutingOk

`func (o *SiteToWanLayer3ProtocolDataRoutingInfo) GetStaticRoutingOk() (*SiteToWanLayer3ProtocolDataRoutingInfoStaticRouting, bool)`

GetStaticRoutingOk returns a tuple with the StaticRouting field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStaticRouting

`func (o *SiteToWanLayer3ProtocolDataRoutingInfo) SetStaticRouting(v SiteToWanLayer3ProtocolDataRoutingInfoStaticRouting)`

SetStaticRouting sets StaticRouting field to given value.

### HasStaticRouting

`func (o *SiteToWanLayer3ProtocolDataRoutingInfo) HasStaticRouting() bool`

HasStaticRouting returns a boolean if a field has been set.

### GetRoutingAddressFamily

`func (o *SiteToWanLayer3ProtocolDataRoutingInfo) GetRoutingAddressFamily() string`

GetRoutingAddressFamily returns the RoutingAddressFamily field if non-nil, zero value otherwise.

### GetRoutingAddressFamilyOk

`func (o *SiteToWanLayer3ProtocolDataRoutingInfo) GetRoutingAddressFamilyOk() (*string, bool)`

GetRoutingAddressFamilyOk returns a tuple with the RoutingAddressFamily field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRoutingAddressFamily

`func (o *SiteToWanLayer3ProtocolDataRoutingInfo) SetRoutingAddressFamily(v string)`

SetRoutingAddressFamily sets RoutingAddressFamily field to given value.


### GetOspfRouting

`func (o *SiteToWanLayer3ProtocolDataRoutingInfo) GetOspfRouting() SiteToWanLayer3ProtocolDataRoutingInfoOspfRouting`

GetOspfRouting returns the OspfRouting field if non-nil, zero value otherwise.

### GetOspfRoutingOk

`func (o *SiteToWanLayer3ProtocolDataRoutingInfo) GetOspfRoutingOk() (*SiteToWanLayer3ProtocolDataRoutingInfoOspfRouting, bool)`

GetOspfRoutingOk returns a tuple with the OspfRouting field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOspfRouting

`func (o *SiteToWanLayer3ProtocolDataRoutingInfo) SetOspfRouting(v SiteToWanLayer3ProtocolDataRoutingInfoOspfRouting)`

SetOspfRouting sets OspfRouting field to given value.

### HasOspfRouting

`func (o *SiteToWanLayer3ProtocolDataRoutingInfo) HasOspfRouting() bool`

HasOspfRouting returns a boolean if a field has been set.

### GetBgpRouting

`func (o *SiteToWanLayer3ProtocolDataRoutingInfo) GetBgpRouting() SiteToWanLayer3ProtocolDataRoutingInfoBgpRouting`

GetBgpRouting returns the BgpRouting field if non-nil, zero value otherwise.

### GetBgpRoutingOk

`func (o *SiteToWanLayer3ProtocolDataRoutingInfo) GetBgpRoutingOk() (*SiteToWanLayer3ProtocolDataRoutingInfoBgpRouting, bool)`

GetBgpRoutingOk returns a tuple with the BgpRouting field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBgpRouting

`func (o *SiteToWanLayer3ProtocolDataRoutingInfo) SetBgpRouting(v SiteToWanLayer3ProtocolDataRoutingInfoBgpRouting)`

SetBgpRouting sets BgpRouting field to given value.

### HasBgpRouting

`func (o *SiteToWanLayer3ProtocolDataRoutingInfo) HasBgpRouting() bool`

HasBgpRouting returns a boolean if a field has been set.

### GetRouteMapsDistribution

`func (o *SiteToWanLayer3ProtocolDataRoutingInfo) GetRouteMapsDistribution() SiteToWanLayer3ProtocolDataRoutingInfoRouteMapsDistribution`

GetRouteMapsDistribution returns the RouteMapsDistribution field if non-nil, zero value otherwise.

### GetRouteMapsDistributionOk

`func (o *SiteToWanLayer3ProtocolDataRoutingInfo) GetRouteMapsDistributionOk() (*SiteToWanLayer3ProtocolDataRoutingInfoRouteMapsDistribution, bool)`

GetRouteMapsDistributionOk returns a tuple with the RouteMapsDistribution field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRouteMapsDistribution

`func (o *SiteToWanLayer3ProtocolDataRoutingInfo) SetRouteMapsDistribution(v SiteToWanLayer3ProtocolDataRoutingInfoRouteMapsDistribution)`

SetRouteMapsDistribution sets RouteMapsDistribution field to given value.

### HasRouteMapsDistribution

`func (o *SiteToWanLayer3ProtocolDataRoutingInfo) HasRouteMapsDistribution() bool`

HasRouteMapsDistribution returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


