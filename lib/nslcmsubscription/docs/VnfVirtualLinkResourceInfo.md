# VnfVirtualLinkResourceInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | An identifier that is unique for the respective type within a VNF instance, but may not be globally unique.  | 
**VnfdId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**VnfVirtualLinkDescId** | **string** | Identifier of the VNF Virtual Link Descriptor (VLD) in the VNFD.  | 
**NetworkResource** | [**ResourceHandle**](ResourceHandle.md) |  | 
**ReservationId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**VnfLinkPorts** | Pointer to [**[]VnfLinkPortInfo**](VnfLinkPortInfo.md) | Links ports of this VL. Shall be present when the linkPort is used for external connectivity by the VNF (refer to VnfLinkPortInfo). May be present otherwise.  | [optional] 
**Metadata** | Pointer to **map[string]interface{}** | This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159.  | [optional] 

## Methods

### NewVnfVirtualLinkResourceInfo

`func NewVnfVirtualLinkResourceInfo(id string, vnfVirtualLinkDescId string, networkResource ResourceHandle, ) *VnfVirtualLinkResourceInfo`

NewVnfVirtualLinkResourceInfo instantiates a new VnfVirtualLinkResourceInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewVnfVirtualLinkResourceInfoWithDefaults

`func NewVnfVirtualLinkResourceInfoWithDefaults() *VnfVirtualLinkResourceInfo`

NewVnfVirtualLinkResourceInfoWithDefaults instantiates a new VnfVirtualLinkResourceInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *VnfVirtualLinkResourceInfo) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *VnfVirtualLinkResourceInfo) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *VnfVirtualLinkResourceInfo) SetId(v string)`

SetId sets Id field to given value.


### GetVnfdId

`func (o *VnfVirtualLinkResourceInfo) GetVnfdId() string`

GetVnfdId returns the VnfdId field if non-nil, zero value otherwise.

### GetVnfdIdOk

`func (o *VnfVirtualLinkResourceInfo) GetVnfdIdOk() (*string, bool)`

GetVnfdIdOk returns a tuple with the VnfdId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfdId

`func (o *VnfVirtualLinkResourceInfo) SetVnfdId(v string)`

SetVnfdId sets VnfdId field to given value.

### HasVnfdId

`func (o *VnfVirtualLinkResourceInfo) HasVnfdId() bool`

HasVnfdId returns a boolean if a field has been set.

### GetVnfVirtualLinkDescId

`func (o *VnfVirtualLinkResourceInfo) GetVnfVirtualLinkDescId() string`

GetVnfVirtualLinkDescId returns the VnfVirtualLinkDescId field if non-nil, zero value otherwise.

### GetVnfVirtualLinkDescIdOk

`func (o *VnfVirtualLinkResourceInfo) GetVnfVirtualLinkDescIdOk() (*string, bool)`

GetVnfVirtualLinkDescIdOk returns a tuple with the VnfVirtualLinkDescId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfVirtualLinkDescId

`func (o *VnfVirtualLinkResourceInfo) SetVnfVirtualLinkDescId(v string)`

SetVnfVirtualLinkDescId sets VnfVirtualLinkDescId field to given value.


### GetNetworkResource

`func (o *VnfVirtualLinkResourceInfo) GetNetworkResource() ResourceHandle`

GetNetworkResource returns the NetworkResource field if non-nil, zero value otherwise.

### GetNetworkResourceOk

`func (o *VnfVirtualLinkResourceInfo) GetNetworkResourceOk() (*ResourceHandle, bool)`

GetNetworkResourceOk returns a tuple with the NetworkResource field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNetworkResource

`func (o *VnfVirtualLinkResourceInfo) SetNetworkResource(v ResourceHandle)`

SetNetworkResource sets NetworkResource field to given value.


### GetReservationId

`func (o *VnfVirtualLinkResourceInfo) GetReservationId() string`

GetReservationId returns the ReservationId field if non-nil, zero value otherwise.

### GetReservationIdOk

`func (o *VnfVirtualLinkResourceInfo) GetReservationIdOk() (*string, bool)`

GetReservationIdOk returns a tuple with the ReservationId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReservationId

`func (o *VnfVirtualLinkResourceInfo) SetReservationId(v string)`

SetReservationId sets ReservationId field to given value.

### HasReservationId

`func (o *VnfVirtualLinkResourceInfo) HasReservationId() bool`

HasReservationId returns a boolean if a field has been set.

### GetVnfLinkPorts

`func (o *VnfVirtualLinkResourceInfo) GetVnfLinkPorts() []VnfLinkPortInfo`

GetVnfLinkPorts returns the VnfLinkPorts field if non-nil, zero value otherwise.

### GetVnfLinkPortsOk

`func (o *VnfVirtualLinkResourceInfo) GetVnfLinkPortsOk() (*[]VnfLinkPortInfo, bool)`

GetVnfLinkPortsOk returns a tuple with the VnfLinkPorts field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVnfLinkPorts

`func (o *VnfVirtualLinkResourceInfo) SetVnfLinkPorts(v []VnfLinkPortInfo)`

SetVnfLinkPorts sets VnfLinkPorts field to given value.

### HasVnfLinkPorts

`func (o *VnfVirtualLinkResourceInfo) HasVnfLinkPorts() bool`

HasVnfLinkPorts returns a boolean if a field has been set.

### GetMetadata

`func (o *VnfVirtualLinkResourceInfo) GetMetadata() map[string]interface{}`

GetMetadata returns the Metadata field if non-nil, zero value otherwise.

### GetMetadataOk

`func (o *VnfVirtualLinkResourceInfo) GetMetadataOk() (*map[string]interface{}, bool)`

GetMetadataOk returns a tuple with the Metadata field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMetadata

`func (o *VnfVirtualLinkResourceInfo) SetMetadata(v map[string]interface{})`

SetMetadata sets Metadata field to given value.

### HasMetadata

`func (o *VnfVirtualLinkResourceInfo) HasMetadata() bool`

HasMetadata returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


