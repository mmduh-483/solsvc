# LccnSubscriptionRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Filter** | Pointer to [**LifecycleChangeNotificationsFilter**](LifecycleChangeNotificationsFilter.md) |  | [optional] 
**CallbackUri** | **string** | String formatted according to IETF RFC 3986.  | 
**Authentication** | Pointer to [**SubscriptionAuthentication**](SubscriptionAuthentication.md) |  | [optional] 
**Verbosity** | Pointer to [**LcmOpOccNotificationVerbosityType**](LcmOpOccNotificationVerbosityType.md) |  | [optional] 

## Methods

### NewLccnSubscriptionRequest

`func NewLccnSubscriptionRequest(callbackUri string, ) *LccnSubscriptionRequest`

NewLccnSubscriptionRequest instantiates a new LccnSubscriptionRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewLccnSubscriptionRequestWithDefaults

`func NewLccnSubscriptionRequestWithDefaults() *LccnSubscriptionRequest`

NewLccnSubscriptionRequestWithDefaults instantiates a new LccnSubscriptionRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFilter

`func (o *LccnSubscriptionRequest) GetFilter() LifecycleChangeNotificationsFilter`

GetFilter returns the Filter field if non-nil, zero value otherwise.

### GetFilterOk

`func (o *LccnSubscriptionRequest) GetFilterOk() (*LifecycleChangeNotificationsFilter, bool)`

GetFilterOk returns a tuple with the Filter field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFilter

`func (o *LccnSubscriptionRequest) SetFilter(v LifecycleChangeNotificationsFilter)`

SetFilter sets Filter field to given value.

### HasFilter

`func (o *LccnSubscriptionRequest) HasFilter() bool`

HasFilter returns a boolean if a field has been set.

### GetCallbackUri

`func (o *LccnSubscriptionRequest) GetCallbackUri() string`

GetCallbackUri returns the CallbackUri field if non-nil, zero value otherwise.

### GetCallbackUriOk

`func (o *LccnSubscriptionRequest) GetCallbackUriOk() (*string, bool)`

GetCallbackUriOk returns a tuple with the CallbackUri field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCallbackUri

`func (o *LccnSubscriptionRequest) SetCallbackUri(v string)`

SetCallbackUri sets CallbackUri field to given value.


### GetAuthentication

`func (o *LccnSubscriptionRequest) GetAuthentication() SubscriptionAuthentication`

GetAuthentication returns the Authentication field if non-nil, zero value otherwise.

### GetAuthenticationOk

`func (o *LccnSubscriptionRequest) GetAuthenticationOk() (*SubscriptionAuthentication, bool)`

GetAuthenticationOk returns a tuple with the Authentication field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAuthentication

`func (o *LccnSubscriptionRequest) SetAuthentication(v SubscriptionAuthentication)`

SetAuthentication sets Authentication field to given value.

### HasAuthentication

`func (o *LccnSubscriptionRequest) HasAuthentication() bool`

HasAuthentication returns a boolean if a field has been set.

### GetVerbosity

`func (o *LccnSubscriptionRequest) GetVerbosity() LcmOpOccNotificationVerbosityType`

GetVerbosity returns the Verbosity field if non-nil, zero value otherwise.

### GetVerbosityOk

`func (o *LccnSubscriptionRequest) GetVerbosityOk() (*LcmOpOccNotificationVerbosityType, bool)`

GetVerbosityOk returns a tuple with the Verbosity field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVerbosity

`func (o *LccnSubscriptionRequest) SetVerbosity(v LcmOpOccNotificationVerbosityType)`

SetVerbosity sets Verbosity field to given value.

### HasVerbosity

`func (o *LccnSubscriptionRequest) HasVerbosity() bool`

HasVerbosity returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


