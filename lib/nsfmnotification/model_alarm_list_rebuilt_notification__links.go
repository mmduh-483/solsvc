/*
SOL005 - NS Fault Management Notification Interface

SOL005 - NS Fault Management Notification Interface  IMPORTANT: Please note that this file might be not aligned to the current version of the ETSI Group Specification it refers to. In case of discrepancies the published ETSI Group Specification takes precedence.  Please report bugs to https://forge.etsi.org/rep/nfv/SOL005/issues 

API version: 1.2.0-impl:etsi.org:ETSI_NFV_OpenAPI:1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// AlarmListRebuiltNotificationLinks Links to resources related to this notification. 
type AlarmListRebuiltNotificationLinks struct {
	Subscription NotificationLink `json:"subscription"`
	Alarms NotificationLink `json:"alarms"`
}

// NewAlarmListRebuiltNotificationLinks instantiates a new AlarmListRebuiltNotificationLinks object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewAlarmListRebuiltNotificationLinks(subscription NotificationLink, alarms NotificationLink) *AlarmListRebuiltNotificationLinks {
	this := AlarmListRebuiltNotificationLinks{}
	this.Subscription = subscription
	this.Alarms = alarms
	return &this
}

// NewAlarmListRebuiltNotificationLinksWithDefaults instantiates a new AlarmListRebuiltNotificationLinks object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewAlarmListRebuiltNotificationLinksWithDefaults() *AlarmListRebuiltNotificationLinks {
	this := AlarmListRebuiltNotificationLinks{}
	return &this
}

// GetSubscription returns the Subscription field value
func (o *AlarmListRebuiltNotificationLinks) GetSubscription() NotificationLink {
	if o == nil {
		var ret NotificationLink
		return ret
	}

	return o.Subscription
}

// GetSubscriptionOk returns a tuple with the Subscription field value
// and a boolean to check if the value has been set.
func (o *AlarmListRebuiltNotificationLinks) GetSubscriptionOk() (*NotificationLink, bool) {
	if o == nil {
    return nil, false
	}
	return &o.Subscription, true
}

// SetSubscription sets field value
func (o *AlarmListRebuiltNotificationLinks) SetSubscription(v NotificationLink) {
	o.Subscription = v
}

// GetAlarms returns the Alarms field value
func (o *AlarmListRebuiltNotificationLinks) GetAlarms() NotificationLink {
	if o == nil {
		var ret NotificationLink
		return ret
	}

	return o.Alarms
}

// GetAlarmsOk returns a tuple with the Alarms field value
// and a boolean to check if the value has been set.
func (o *AlarmListRebuiltNotificationLinks) GetAlarmsOk() (*NotificationLink, bool) {
	if o == nil {
    return nil, false
	}
	return &o.Alarms, true
}

// SetAlarms sets field value
func (o *AlarmListRebuiltNotificationLinks) SetAlarms(v NotificationLink) {
	o.Alarms = v
}

func (o AlarmListRebuiltNotificationLinks) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["subscription"] = o.Subscription
	}
	if true {
		toSerialize["alarms"] = o.Alarms
	}
	return json.Marshal(toSerialize)
}

type NullableAlarmListRebuiltNotificationLinks struct {
	value *AlarmListRebuiltNotificationLinks
	isSet bool
}

func (v NullableAlarmListRebuiltNotificationLinks) Get() *AlarmListRebuiltNotificationLinks {
	return v.value
}

func (v *NullableAlarmListRebuiltNotificationLinks) Set(val *AlarmListRebuiltNotificationLinks) {
	v.value = val
	v.isSet = true
}

func (v NullableAlarmListRebuiltNotificationLinks) IsSet() bool {
	return v.isSet
}

func (v *NullableAlarmListRebuiltNotificationLinks) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableAlarmListRebuiltNotificationLinks(val *AlarmListRebuiltNotificationLinks) *NullableAlarmListRebuiltNotificationLinks {
	return &NullableAlarmListRebuiltNotificationLinks{value: val, isSet: true}
}

func (v NullableAlarmListRebuiltNotificationLinks) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableAlarmListRebuiltNotificationLinks) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


