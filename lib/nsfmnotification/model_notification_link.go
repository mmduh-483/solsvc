/*
SOL005 - NS Fault Management Notification Interface

SOL005 - NS Fault Management Notification Interface  IMPORTANT: Please note that this file might be not aligned to the current version of the ETSI Group Specification it refers to. In case of discrepancies the published ETSI Group Specification takes precedence.  Please report bugs to https://forge.etsi.org/rep/nfv/SOL005/issues 

API version: 1.2.0-impl:etsi.org:ETSI_NFV_OpenAPI:1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// NotificationLink This type represents a link to a resource in a notification, using an absolute or relative URI. 
type NotificationLink struct {
	// URI of a resource referenced from a notification. Should be an absolute URI (i.e. a URI that contains {apiRoot}), however, may be a relative URI (i.e. a URI where the {apiRoot} part is omitted) if the {apiRoot} information is not available. 
	Href string `json:"href"`
}

// NewNotificationLink instantiates a new NotificationLink object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewNotificationLink(href string) *NotificationLink {
	this := NotificationLink{}
	this.Href = href
	return &this
}

// NewNotificationLinkWithDefaults instantiates a new NotificationLink object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewNotificationLinkWithDefaults() *NotificationLink {
	this := NotificationLink{}
	return &this
}

// GetHref returns the Href field value
func (o *NotificationLink) GetHref() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Href
}

// GetHrefOk returns a tuple with the Href field value
// and a boolean to check if the value has been set.
func (o *NotificationLink) GetHrefOk() (*string, bool) {
	if o == nil {
    return nil, false
	}
	return &o.Href, true
}

// SetHref sets field value
func (o *NotificationLink) SetHref(v string) {
	o.Href = v
}

func (o NotificationLink) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["href"] = o.Href
	}
	return json.Marshal(toSerialize)
}

type NullableNotificationLink struct {
	value *NotificationLink
	isSet bool
}

func (v NullableNotificationLink) Get() *NotificationLink {
	return v.value
}

func (v *NullableNotificationLink) Set(val *NotificationLink) {
	v.value = val
	v.isSet = true
}

func (v NullableNotificationLink) IsSet() bool {
	return v.isSet
}

func (v *NullableNotificationLink) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableNotificationLink(val *NotificationLink) *NullableNotificationLink {
	return &NullableNotificationLink{value: val, isSet: true}
}

func (v NullableNotificationLink) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableNotificationLink) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


