# AlarmLinks

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Self** | [**Link**](Link.md) |  | 

## Methods

### NewAlarmLinks

`func NewAlarmLinks(self Link, ) *AlarmLinks`

NewAlarmLinks instantiates a new AlarmLinks object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAlarmLinksWithDefaults

`func NewAlarmLinksWithDefaults() *AlarmLinks`

NewAlarmLinksWithDefaults instantiates a new AlarmLinks object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSelf

`func (o *AlarmLinks) GetSelf() Link`

GetSelf returns the Self field if non-nil, zero value otherwise.

### GetSelfOk

`func (o *AlarmLinks) GetSelfOk() (*Link, bool)`

GetSelfOk returns a tuple with the Self field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSelf

`func (o *AlarmLinks) SetSelf(v Link)`

SetSelf sets Self field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


