# FaultyComponentInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**FaultyNestedNsInstanceId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**FaultyResourceType** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**FaultyNsVirtualLinkInstanceId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 

## Methods

### NewFaultyComponentInfo

`func NewFaultyComponentInfo() *FaultyComponentInfo`

NewFaultyComponentInfo instantiates a new FaultyComponentInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewFaultyComponentInfoWithDefaults

`func NewFaultyComponentInfoWithDefaults() *FaultyComponentInfo`

NewFaultyComponentInfoWithDefaults instantiates a new FaultyComponentInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFaultyNestedNsInstanceId

`func (o *FaultyComponentInfo) GetFaultyNestedNsInstanceId() string`

GetFaultyNestedNsInstanceId returns the FaultyNestedNsInstanceId field if non-nil, zero value otherwise.

### GetFaultyNestedNsInstanceIdOk

`func (o *FaultyComponentInfo) GetFaultyNestedNsInstanceIdOk() (*string, bool)`

GetFaultyNestedNsInstanceIdOk returns a tuple with the FaultyNestedNsInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFaultyNestedNsInstanceId

`func (o *FaultyComponentInfo) SetFaultyNestedNsInstanceId(v string)`

SetFaultyNestedNsInstanceId sets FaultyNestedNsInstanceId field to given value.

### HasFaultyNestedNsInstanceId

`func (o *FaultyComponentInfo) HasFaultyNestedNsInstanceId() bool`

HasFaultyNestedNsInstanceId returns a boolean if a field has been set.

### GetFaultyResourceType

`func (o *FaultyComponentInfo) GetFaultyResourceType() string`

GetFaultyResourceType returns the FaultyResourceType field if non-nil, zero value otherwise.

### GetFaultyResourceTypeOk

`func (o *FaultyComponentInfo) GetFaultyResourceTypeOk() (*string, bool)`

GetFaultyResourceTypeOk returns a tuple with the FaultyResourceType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFaultyResourceType

`func (o *FaultyComponentInfo) SetFaultyResourceType(v string)`

SetFaultyResourceType sets FaultyResourceType field to given value.

### HasFaultyResourceType

`func (o *FaultyComponentInfo) HasFaultyResourceType() bool`

HasFaultyResourceType returns a boolean if a field has been set.

### GetFaultyNsVirtualLinkInstanceId

`func (o *FaultyComponentInfo) GetFaultyNsVirtualLinkInstanceId() string`

GetFaultyNsVirtualLinkInstanceId returns the FaultyNsVirtualLinkInstanceId field if non-nil, zero value otherwise.

### GetFaultyNsVirtualLinkInstanceIdOk

`func (o *FaultyComponentInfo) GetFaultyNsVirtualLinkInstanceIdOk() (*string, bool)`

GetFaultyNsVirtualLinkInstanceIdOk returns a tuple with the FaultyNsVirtualLinkInstanceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFaultyNsVirtualLinkInstanceId

`func (o *FaultyComponentInfo) SetFaultyNsVirtualLinkInstanceId(v string)`

SetFaultyNsVirtualLinkInstanceId sets FaultyNsVirtualLinkInstanceId field to given value.

### HasFaultyNsVirtualLinkInstanceId

`func (o *FaultyComponentInfo) HasFaultyNsVirtualLinkInstanceId() bool`

HasFaultyNsVirtualLinkInstanceId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


