# FmSubscription

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | An identifier with the intention of being globally unique.  | 
**Filter** | Pointer to [**FmNotificationsFilter**](FmNotificationsFilter.md) |  | [optional] 
**CallbackUri** | **string** | String formatted according to IETF RFC 3986.  | 
**Links** | [**AlarmLinks**](AlarmLinks.md) |  | 

## Methods

### NewFmSubscription

`func NewFmSubscription(id string, callbackUri string, links AlarmLinks, ) *FmSubscription`

NewFmSubscription instantiates a new FmSubscription object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewFmSubscriptionWithDefaults

`func NewFmSubscriptionWithDefaults() *FmSubscription`

NewFmSubscriptionWithDefaults instantiates a new FmSubscription object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *FmSubscription) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *FmSubscription) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *FmSubscription) SetId(v string)`

SetId sets Id field to given value.


### GetFilter

`func (o *FmSubscription) GetFilter() FmNotificationsFilter`

GetFilter returns the Filter field if non-nil, zero value otherwise.

### GetFilterOk

`func (o *FmSubscription) GetFilterOk() (*FmNotificationsFilter, bool)`

GetFilterOk returns a tuple with the Filter field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFilter

`func (o *FmSubscription) SetFilter(v FmNotificationsFilter)`

SetFilter sets Filter field to given value.

### HasFilter

`func (o *FmSubscription) HasFilter() bool`

HasFilter returns a boolean if a field has been set.

### GetCallbackUri

`func (o *FmSubscription) GetCallbackUri() string`

GetCallbackUri returns the CallbackUri field if non-nil, zero value otherwise.

### GetCallbackUriOk

`func (o *FmSubscription) GetCallbackUriOk() (*string, bool)`

GetCallbackUriOk returns a tuple with the CallbackUri field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCallbackUri

`func (o *FmSubscription) SetCallbackUri(v string)`

SetCallbackUri sets CallbackUri field to given value.


### GetLinks

`func (o *FmSubscription) GetLinks() AlarmLinks`

GetLinks returns the Links field if non-nil, zero value otherwise.

### GetLinksOk

`func (o *FmSubscription) GetLinksOk() (*AlarmLinks, bool)`

GetLinksOk returns a tuple with the Links field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLinks

`func (o *FmSubscription) SetLinks(v AlarmLinks)`

SetLinks sets Links field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


