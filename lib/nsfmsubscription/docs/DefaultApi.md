# \DefaultApi

All URIs are relative to *http://127.0.0.1/nsfm/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AlarmsAlarmIdGet**](DefaultApi.md#AlarmsAlarmIdGet) | **Get** /alarms/{alarmId} | 
[**AlarmsAlarmIdPatch**](DefaultApi.md#AlarmsAlarmIdPatch) | **Patch** /alarms/{alarmId} | 
[**AlarmsGet**](DefaultApi.md#AlarmsGet) | **Get** /alarms | 
[**ApiVersionsDelete**](DefaultApi.md#ApiVersionsDelete) | **Delete** /api_versions | 
[**ApiVersionsGet**](DefaultApi.md#ApiVersionsGet) | **Get** /api_versions | 
[**ApiVersionsPatch**](DefaultApi.md#ApiVersionsPatch) | **Patch** /api_versions | 
[**ApiVersionsPost**](DefaultApi.md#ApiVersionsPost) | **Post** /api_versions | 
[**ApiVersionsPut**](DefaultApi.md#ApiVersionsPut) | **Put** /api_versions | 
[**SubscriptionsGet**](DefaultApi.md#SubscriptionsGet) | **Get** /subscriptions | 
[**SubscriptionsPost**](DefaultApi.md#SubscriptionsPost) | **Post** /subscriptions | 
[**SubscriptionsSubscriptionIdDelete**](DefaultApi.md#SubscriptionsSubscriptionIdDelete) | **Delete** /subscriptions/{subscriptionId} | 
[**SubscriptionsSubscriptionIdGet**](DefaultApi.md#SubscriptionsSubscriptionIdGet) | **Get** /subscriptions/{subscriptionId} | 



## AlarmsAlarmIdGet

> Alarm AlarmsAlarmIdGet(ctx, alarmId).Version(version).Accept(accept).Authorization(authorization).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    alarmId := "alarmId_example" // string | Identifier of the alarm. This identifier can be retrieved from the \"id\" attribute of the \"alarm\" attribute in the AlarmNotification or AlarmClearedNotification.  It can also be retrieved from the \"id\" attribute of the applicable array element in the payload body of the response to a GET request to the \"Alarms\" resource. 
    version := "version_example" // string | Version of the API requested to use when responding to this request. 
    accept := "accept_example" // string | Content-Types that are acceptable for the response. Reference: IETF RFC 7231. 
    authorization := "authorization_example" // string | The authorization token for the request. Reference: IETF RFC 7235.  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultApi.AlarmsAlarmIdGet(context.Background(), alarmId).Version(version).Accept(accept).Authorization(authorization).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.AlarmsAlarmIdGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AlarmsAlarmIdGet`: Alarm
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.AlarmsAlarmIdGet`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**alarmId** | **string** | Identifier of the alarm. This identifier can be retrieved from the \&quot;id\&quot; attribute of the \&quot;alarm\&quot; attribute in the AlarmNotification or AlarmClearedNotification.  It can also be retrieved from the \&quot;id\&quot; attribute of the applicable array element in the payload body of the response to a GET request to the \&quot;Alarms\&quot; resource.  | 

### Other Parameters

Other parameters are passed through a pointer to a apiAlarmsAlarmIdGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **version** | **string** | Version of the API requested to use when responding to this request.  | 
 **accept** | **string** | Content-Types that are acceptable for the response. Reference: IETF RFC 7231.  | 
 **authorization** | **string** | The authorization token for the request. Reference: IETF RFC 7235.  | 

### Return type

[**Alarm**](Alarm.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AlarmsAlarmIdPatch

> AlarmModifications AlarmsAlarmIdPatch(ctx, alarmId).Version(version).Accept(accept).ContentType(contentType).AlarmModifications(alarmModifications).Authorization(authorization).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    alarmId := "alarmId_example" // string | Identifier of the alarm. This identifier can be retrieved from the \"id\" attribute of the \"alarm\" attribute in the AlarmNotification or AlarmClearedNotification.  It can also be retrieved from the \"id\" attribute of the applicable array element in the payload body of the response to a GET request to the \"Alarms\" resource. 
    version := "version_example" // string | Version of the API requested to use when responding to this request. 
    accept := "accept_example" // string | Content-Types that are acceptable for the response. Reference: IETF RFC 7231. 
    contentType := "contentType_example" // string | The MIME type of the body of the request. Reference: IETF RFC 7231 
    alarmModifications := *openapiclient.NewAlarmModifications("AckState_example") // AlarmModifications | The parameter for the alarm modification, as defined in clause 8.5.2.8. 
    authorization := "authorization_example" // string | The authorization token for the request. Reference: IETF RFC 7235.  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultApi.AlarmsAlarmIdPatch(context.Background(), alarmId).Version(version).Accept(accept).ContentType(contentType).AlarmModifications(alarmModifications).Authorization(authorization).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.AlarmsAlarmIdPatch``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AlarmsAlarmIdPatch`: AlarmModifications
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.AlarmsAlarmIdPatch`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**alarmId** | **string** | Identifier of the alarm. This identifier can be retrieved from the \&quot;id\&quot; attribute of the \&quot;alarm\&quot; attribute in the AlarmNotification or AlarmClearedNotification.  It can also be retrieved from the \&quot;id\&quot; attribute of the applicable array element in the payload body of the response to a GET request to the \&quot;Alarms\&quot; resource.  | 

### Other Parameters

Other parameters are passed through a pointer to a apiAlarmsAlarmIdPatchRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **version** | **string** | Version of the API requested to use when responding to this request.  | 
 **accept** | **string** | Content-Types that are acceptable for the response. Reference: IETF RFC 7231.  | 
 **contentType** | **string** | The MIME type of the body of the request. Reference: IETF RFC 7231  | 
 **alarmModifications** | [**AlarmModifications**](AlarmModifications.md) | The parameter for the alarm modification, as defined in clause 8.5.2.8.  | 
 **authorization** | **string** | The authorization token for the request. Reference: IETF RFC 7235.  | 

### Return type

[**AlarmModifications**](AlarmModifications.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AlarmsGet

> []Alarm AlarmsGet(ctx).Version(version).Accept(accept).Authorization(authorization).Filter(filter).NextpageOpaqueMarker(nextpageOpaqueMarker).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    version := "version_example" // string | Version of the API requested to use when responding to this request. 
    accept := "accept_example" // string | Content-Types that are acceptable for the response. Reference: IETF RFC 7231. 
    authorization := "authorization_example" // string | The authorization token for the request. Reference: IETF RFC 7235.  (optional)
    filter := "filter_example" // string | Attribute-based filtering expression according to clause 5.2 of ETSI GS NFV SOL 013. The NFVO shall support receiving this parameter as part of the URI query string.  The OSS/BSS may supply this parameter. The following attribute names shall be supported by the NFVO in the filter expression: - id - nsInstanceId - rootCauseFaultyComponent.faultyNestedNsInstanceId - rootCauseFaultyComponent.faultyNsVirtualLinkInstanceId - rootCauseFaultyComponent.faultyVnfInstanceId - rootCauseFaultyResource.faultyResourceType - eventType - perceivedSeverity - probableCause  (optional)
    nextpageOpaqueMarker := "nextpageOpaqueMarker_example" // string | Marker to obtain the next page of a paged response. Shall be supported by the NFVO if the NFVO supports alternative 2  (paging) according to clause 5.4.2.1 of ETSI GS NFV-SOL 013 for this resource.  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultApi.AlarmsGet(context.Background()).Version(version).Accept(accept).Authorization(authorization).Filter(filter).NextpageOpaqueMarker(nextpageOpaqueMarker).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.AlarmsGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `AlarmsGet`: []Alarm
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.AlarmsGet`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiAlarmsGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **version** | **string** | Version of the API requested to use when responding to this request.  | 
 **accept** | **string** | Content-Types that are acceptable for the response. Reference: IETF RFC 7231.  | 
 **authorization** | **string** | The authorization token for the request. Reference: IETF RFC 7235.  | 
 **filter** | **string** | Attribute-based filtering expression according to clause 5.2 of ETSI GS NFV SOL 013. The NFVO shall support receiving this parameter as part of the URI query string.  The OSS/BSS may supply this parameter. The following attribute names shall be supported by the NFVO in the filter expression: - id - nsInstanceId - rootCauseFaultyComponent.faultyNestedNsInstanceId - rootCauseFaultyComponent.faultyNsVirtualLinkInstanceId - rootCauseFaultyComponent.faultyVnfInstanceId - rootCauseFaultyResource.faultyResourceType - eventType - perceivedSeverity - probableCause  | 
 **nextpageOpaqueMarker** | **string** | Marker to obtain the next page of a paged response. Shall be supported by the NFVO if the NFVO supports alternative 2  (paging) according to clause 5.4.2.1 of ETSI GS NFV-SOL 013 for this resource.  | 

### Return type

[**[]Alarm**](Alarm.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ApiVersionsDelete

> ApiVersionsDelete(ctx).Version(version).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    version := "version_example" // string | Version of the API requested to use when responding to this request. 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultApi.ApiVersionsDelete(context.Background()).Version(version).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.ApiVersionsDelete``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiApiVersionsDeleteRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **version** | **string** | Version of the API requested to use when responding to this request.  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ApiVersionsGet

> ApiVersionsGet(ctx).Version(version).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    version := "version_example" // string | Version of the API requested to use when responding to this request. 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultApi.ApiVersionsGet(context.Background()).Version(version).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.ApiVersionsGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiApiVersionsGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **version** | **string** | Version of the API requested to use when responding to this request.  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ApiVersionsPatch

> ApiVersionsPatch(ctx).Version(version).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    version := "version_example" // string | Version of the API requested to use when responding to this request. 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultApi.ApiVersionsPatch(context.Background()).Version(version).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.ApiVersionsPatch``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiApiVersionsPatchRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **version** | **string** | Version of the API requested to use when responding to this request.  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ApiVersionsPost

> ApiVersionsPost(ctx).Version(version).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    version := "version_example" // string | Version of the API requested to use when responding to this request. 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultApi.ApiVersionsPost(context.Background()).Version(version).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.ApiVersionsPost``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiApiVersionsPostRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **version** | **string** | Version of the API requested to use when responding to this request.  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ApiVersionsPut

> ApiVersionsPut(ctx).Version(version).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    version := "version_example" // string | Version of the API requested to use when responding to this request. 

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultApi.ApiVersionsPut(context.Background()).Version(version).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.ApiVersionsPut``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiApiVersionsPutRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **version** | **string** | Version of the API requested to use when responding to this request.  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SubscriptionsGet

> []FmSubscription SubscriptionsGet(ctx).Version(version).Accept(accept).Authorization(authorization).Filter(filter).NextpageOpaqueMarker(nextpageOpaqueMarker).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    version := "version_example" // string | Version of the API requested to use when responding to this request. 
    accept := "accept_example" // string | Content-Types that are acceptable for the response. Reference: IETF RFC 7231. 
    authorization := "authorization_example" // string | The authorization token for the request. Reference: IETF RFC 7235.  (optional)
    filter := "filter_example" // string | Attribute-based filtering expression according to clause 5.2 of ETSI GS NFV SOL 013. The NFVO shall support receiving this parameter as part of the URI query string.  The OSS/BSS may supply this parameter. All attribute names that appear in the FmSubscription and in data types referenced from  it shall be supported by the NFVO in the filter expression.  (optional)
    nextpageOpaqueMarker := "nextpageOpaqueMarker_example" // string | Marker to obtain the next page of a paged response. Shall be supported by the NFVO if the NFVO supports alternative 2  (paging) according to clause 5.4.2.1 of ETSI GS NFV-SOL 013 for this resource.  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultApi.SubscriptionsGet(context.Background()).Version(version).Accept(accept).Authorization(authorization).Filter(filter).NextpageOpaqueMarker(nextpageOpaqueMarker).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.SubscriptionsGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `SubscriptionsGet`: []FmSubscription
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.SubscriptionsGet`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiSubscriptionsGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **version** | **string** | Version of the API requested to use when responding to this request.  | 
 **accept** | **string** | Content-Types that are acceptable for the response. Reference: IETF RFC 7231.  | 
 **authorization** | **string** | The authorization token for the request. Reference: IETF RFC 7235.  | 
 **filter** | **string** | Attribute-based filtering expression according to clause 5.2 of ETSI GS NFV SOL 013. The NFVO shall support receiving this parameter as part of the URI query string.  The OSS/BSS may supply this parameter. All attribute names that appear in the FmSubscription and in data types referenced from  it shall be supported by the NFVO in the filter expression.  | 
 **nextpageOpaqueMarker** | **string** | Marker to obtain the next page of a paged response. Shall be supported by the NFVO if the NFVO supports alternative 2  (paging) according to clause 5.4.2.1 of ETSI GS NFV-SOL 013 for this resource.  | 

### Return type

[**[]FmSubscription**](FmSubscription.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SubscriptionsPost

> FmSubscription SubscriptionsPost(ctx).Version(version).Accept(accept).ContentType(contentType).FmSubscriptionRequest(fmSubscriptionRequest).Authorization(authorization).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    version := "version_example" // string | Version of the API requested to use when responding to this request. 
    accept := "accept_example" // string | Content-Types that are acceptable for the response. Reference: IETF RFC 7231. 
    contentType := "contentType_example" // string | The MIME type of the body of the request. Reference: IETF RFC 7231 
    fmSubscriptionRequest := *openapiclient.NewFmSubscriptionRequest("CallbackUri_example") // FmSubscriptionRequest | Details of the subscription to be created, as defined in clause 8.5.2.2. 
    authorization := "authorization_example" // string | The authorization token for the request. Reference: IETF RFC 7235.  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultApi.SubscriptionsPost(context.Background()).Version(version).Accept(accept).ContentType(contentType).FmSubscriptionRequest(fmSubscriptionRequest).Authorization(authorization).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.SubscriptionsPost``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `SubscriptionsPost`: FmSubscription
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.SubscriptionsPost`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiSubscriptionsPostRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **version** | **string** | Version of the API requested to use when responding to this request.  | 
 **accept** | **string** | Content-Types that are acceptable for the response. Reference: IETF RFC 7231.  | 
 **contentType** | **string** | The MIME type of the body of the request. Reference: IETF RFC 7231  | 
 **fmSubscriptionRequest** | [**FmSubscriptionRequest**](FmSubscriptionRequest.md) | Details of the subscription to be created, as defined in clause 8.5.2.2.  | 
 **authorization** | **string** | The authorization token for the request. Reference: IETF RFC 7235.  | 

### Return type

[**FmSubscription**](FmSubscription.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SubscriptionsSubscriptionIdDelete

> SubscriptionsSubscriptionIdDelete(ctx, subscriptionId).Version(version).Authorization(authorization).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    subscriptionId := "subscriptionId_example" // string | Identifier of this subscription. This identifier can be retrieved from the resource referenced by the \"Location\" HTTP header in the response to a POST request creating a new subscription resource. It can also be retrieved from the \"id\" attribute in the payload body of that response. 
    version := "version_example" // string | Version of the API requested to use when responding to this request. 
    authorization := "authorization_example" // string | The authorization token for the request. Reference: IETF RFC 7235.  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultApi.SubscriptionsSubscriptionIdDelete(context.Background(), subscriptionId).Version(version).Authorization(authorization).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.SubscriptionsSubscriptionIdDelete``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**subscriptionId** | **string** | Identifier of this subscription. This identifier can be retrieved from the resource referenced by the \&quot;Location\&quot; HTTP header in the response to a POST request creating a new subscription resource. It can also be retrieved from the \&quot;id\&quot; attribute in the payload body of that response.  | 

### Other Parameters

Other parameters are passed through a pointer to a apiSubscriptionsSubscriptionIdDeleteRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **version** | **string** | Version of the API requested to use when responding to this request.  | 
 **authorization** | **string** | The authorization token for the request. Reference: IETF RFC 7235.  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SubscriptionsSubscriptionIdGet

> FmSubscription SubscriptionsSubscriptionIdGet(ctx, subscriptionId).Version(version).Accept(accept).Authorization(authorization).Execute()





### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    subscriptionId := "subscriptionId_example" // string | Identifier of this subscription. This identifier can be retrieved from the resource referenced by the \"Location\" HTTP header in the response to a POST request creating a new subscription resource. It can also be retrieved from the \"id\" attribute in the payload body of that response. 
    version := "version_example" // string | Version of the API requested to use when responding to this request. 
    accept := "accept_example" // string | Content-Types that are acceptable for the response. Reference: IETF RFC 7231. 
    authorization := "authorization_example" // string | The authorization token for the request. Reference: IETF RFC 7235.  (optional)

    configuration := openapiclient.NewConfiguration()
    apiClient := openapiclient.NewAPIClient(configuration)
    resp, r, err := apiClient.DefaultApi.SubscriptionsSubscriptionIdGet(context.Background(), subscriptionId).Version(version).Accept(accept).Authorization(authorization).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.SubscriptionsSubscriptionIdGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `SubscriptionsSubscriptionIdGet`: FmSubscription
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.SubscriptionsSubscriptionIdGet`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**subscriptionId** | **string** | Identifier of this subscription. This identifier can be retrieved from the resource referenced by the \&quot;Location\&quot; HTTP header in the response to a POST request creating a new subscription resource. It can also be retrieved from the \&quot;id\&quot; attribute in the payload body of that response.  | 

### Other Parameters

Other parameters are passed through a pointer to a apiSubscriptionsSubscriptionIdGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **version** | **string** | Version of the API requested to use when responding to this request.  | 
 **accept** | **string** | Content-Types that are acceptable for the response. Reference: IETF RFC 7231.  | 
 **authorization** | **string** | The authorization token for the request. Reference: IETF RFC 7235.  | 

### Return type

[**FmSubscription**](FmSubscription.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

