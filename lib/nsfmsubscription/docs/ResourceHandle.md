# ResourceHandle

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VimId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**ResourceProviderId** | Pointer to **string** | An identifier with the intention of being globally unique.  | [optional] 
**ResourceId** | **string** | An identifier maintained by the VIM or other resource provider. It is expected to be unique within the VIM instance. Representation: string of variable length.  | 
**VimLevelResourceType** | Pointer to **string** | Type of the resource in the scope of the VIM, the WIM or the resource provider. The value set of the \&quot;vimLevelResourceType\&quot; attribute is within the scope of the VIM, the WIM or the resource provider and can be used as information that complements the ResourceHandle. See note.  | [optional] 

## Methods

### NewResourceHandle

`func NewResourceHandle(resourceId string, ) *ResourceHandle`

NewResourceHandle instantiates a new ResourceHandle object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewResourceHandleWithDefaults

`func NewResourceHandleWithDefaults() *ResourceHandle`

NewResourceHandleWithDefaults instantiates a new ResourceHandle object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetVimId

`func (o *ResourceHandle) GetVimId() string`

GetVimId returns the VimId field if non-nil, zero value otherwise.

### GetVimIdOk

`func (o *ResourceHandle) GetVimIdOk() (*string, bool)`

GetVimIdOk returns a tuple with the VimId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVimId

`func (o *ResourceHandle) SetVimId(v string)`

SetVimId sets VimId field to given value.

### HasVimId

`func (o *ResourceHandle) HasVimId() bool`

HasVimId returns a boolean if a field has been set.

### GetResourceProviderId

`func (o *ResourceHandle) GetResourceProviderId() string`

GetResourceProviderId returns the ResourceProviderId field if non-nil, zero value otherwise.

### GetResourceProviderIdOk

`func (o *ResourceHandle) GetResourceProviderIdOk() (*string, bool)`

GetResourceProviderIdOk returns a tuple with the ResourceProviderId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResourceProviderId

`func (o *ResourceHandle) SetResourceProviderId(v string)`

SetResourceProviderId sets ResourceProviderId field to given value.

### HasResourceProviderId

`func (o *ResourceHandle) HasResourceProviderId() bool`

HasResourceProviderId returns a boolean if a field has been set.

### GetResourceId

`func (o *ResourceHandle) GetResourceId() string`

GetResourceId returns the ResourceId field if non-nil, zero value otherwise.

### GetResourceIdOk

`func (o *ResourceHandle) GetResourceIdOk() (*string, bool)`

GetResourceIdOk returns a tuple with the ResourceId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetResourceId

`func (o *ResourceHandle) SetResourceId(v string)`

SetResourceId sets ResourceId field to given value.


### GetVimLevelResourceType

`func (o *ResourceHandle) GetVimLevelResourceType() string`

GetVimLevelResourceType returns the VimLevelResourceType field if non-nil, zero value otherwise.

### GetVimLevelResourceTypeOk

`func (o *ResourceHandle) GetVimLevelResourceTypeOk() (*string, bool)`

GetVimLevelResourceTypeOk returns a tuple with the VimLevelResourceType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetVimLevelResourceType

`func (o *ResourceHandle) SetVimLevelResourceType(v string)`

SetVimLevelResourceType sets VimLevelResourceType field to given value.

### HasVimLevelResourceType

`func (o *ResourceHandle) HasVimLevelResourceType() bool`

HasVimLevelResourceType returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


