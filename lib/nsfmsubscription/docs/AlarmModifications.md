# AlarmModifications

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AckState** | **string** | New value of the \&quot;ackState\&quot; attribute in \&quot;Alarm\&quot;. Permitted values: - ACKNOWLEDGED - UNACKNOWLEDGED  | 

## Methods

### NewAlarmModifications

`func NewAlarmModifications(ackState string, ) *AlarmModifications`

NewAlarmModifications instantiates a new AlarmModifications object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAlarmModificationsWithDefaults

`func NewAlarmModificationsWithDefaults() *AlarmModifications`

NewAlarmModificationsWithDefaults instantiates a new AlarmModifications object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAckState

`func (o *AlarmModifications) GetAckState() string`

GetAckState returns the AckState field if non-nil, zero value otherwise.

### GetAckStateOk

`func (o *AlarmModifications) GetAckStateOk() (*string, bool)`

GetAckStateOk returns a tuple with the AckState field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAckState

`func (o *AlarmModifications) SetAckState(v string)`

SetAckState sets AckState field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


