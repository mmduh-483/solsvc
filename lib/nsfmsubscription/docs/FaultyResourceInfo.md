# FaultyResourceInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**FaultyResource** | [**ResourceHandle**](ResourceHandle.md) |  | 
**FaultyResourceType** | [**FaultyResourceType**](FaultyResourceType.md) |  | 

## Methods

### NewFaultyResourceInfo

`func NewFaultyResourceInfo(faultyResource ResourceHandle, faultyResourceType FaultyResourceType, ) *FaultyResourceInfo`

NewFaultyResourceInfo instantiates a new FaultyResourceInfo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewFaultyResourceInfoWithDefaults

`func NewFaultyResourceInfoWithDefaults() *FaultyResourceInfo`

NewFaultyResourceInfoWithDefaults instantiates a new FaultyResourceInfo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFaultyResource

`func (o *FaultyResourceInfo) GetFaultyResource() ResourceHandle`

GetFaultyResource returns the FaultyResource field if non-nil, zero value otherwise.

### GetFaultyResourceOk

`func (o *FaultyResourceInfo) GetFaultyResourceOk() (*ResourceHandle, bool)`

GetFaultyResourceOk returns a tuple with the FaultyResource field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFaultyResource

`func (o *FaultyResourceInfo) SetFaultyResource(v ResourceHandle)`

SetFaultyResource sets FaultyResource field to given value.


### GetFaultyResourceType

`func (o *FaultyResourceInfo) GetFaultyResourceType() FaultyResourceType`

GetFaultyResourceType returns the FaultyResourceType field if non-nil, zero value otherwise.

### GetFaultyResourceTypeOk

`func (o *FaultyResourceInfo) GetFaultyResourceTypeOk() (*FaultyResourceType, bool)`

GetFaultyResourceTypeOk returns a tuple with the FaultyResourceType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFaultyResourceType

`func (o *FaultyResourceInfo) SetFaultyResourceType(v FaultyResourceType)`

SetFaultyResourceType sets FaultyResourceType field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


