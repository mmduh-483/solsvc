# EventType

## Enum


* `COMMUNICATIONS_ALARM` (value: `"COMMUNICATIONS_ALARM"`)

* `PROCESSING_ERROR_ALARM` (value: `"PROCESSING_ERROR_ALARM"`)

* `ENVIRONMENTAL_ALARM` (value: `"ENVIRONMENTAL_ALARM"`)

* `QOS_ALARM` (value: `"QOS_ALARM"`)

* `EQUIPMENT_ALARM` (value: `"EQUIPMENT_ALARM"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


