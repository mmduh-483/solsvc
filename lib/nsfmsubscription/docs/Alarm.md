# Alarm

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | An identifier with the intention of being globally unique.  | 
**ManagedObjectId** | **string** | An identifier with the intention of being globally unique.  | 
**RootCauseFaultyComponent** | Pointer to [**FaultyComponentInfo**](FaultyComponentInfo.md) |  | [optional] 
**RootCauseFaultyResource** | Pointer to [**FaultyResourceInfo**](FaultyResourceInfo.md) |  | [optional] 
**AlarmRaisedTime** | **time.Time** | Date-time stamp. Representation: String formatted according toas defined by the date-time production in IETF RFC 3339.  | 
**AlarmChangedTime** | Pointer to **time.Time** | Date-time stamp. Representation: String formatted according toas defined by the date-time production in IETF RFC 3339.  | [optional] 
**AlarmClearedTime** | Pointer to **time.Time** | Date-time stamp. Representation: String formatted according toas defined by the date-time production in IETF RFC 3339.  | [optional] 
**AlarmAcknowledgedTime** | Pointer to **time.Time** | Date-time stamp. Representation: String formatted according toas defined by the date-time production in IETF RFC 3339.  | [optional] 
**AckState** | **string** | Acknowledgment state of the alarm. Permitted values: UNACKNOWLEDGED ACKNOWLEDGED  | 
**PerceivedSeverity** | [**PerceivedSeverityType**](PerceivedSeverityType.md) |  | 
**EventTime** | **time.Time** | Date-time stamp. Representation: String formatted according toas defined by the date-time production in IETF RFC 3339.  | 
**EventType** | [**EventType**](EventType.md) |  | 
**FaultType** | Pointer to **string** | Additional information to clarify the type of the fault.  | [optional] 
**ProbableCause** | **string** | Information about the probable cause of the fault.  | 
**IsRootCause** | **bool** | Attribute indicating if this fault is the root for other correlated alarms. If TRUE, then the alarms listed in the attribute CorrelatedAlarmId are caused by this fault.  | 
**CorrelatedAlarmIds** | Pointer to **[]string** | List of identifiers of other alarms correlated to this fault.  | [optional] 
**FaultDetails** | Pointer to **[]string** | Provides additional information about the fault..  | [optional] 
**Links** | [**AlarmLinks**](AlarmLinks.md) |  | 

## Methods

### NewAlarm

`func NewAlarm(id string, managedObjectId string, alarmRaisedTime time.Time, ackState string, perceivedSeverity PerceivedSeverityType, eventTime time.Time, eventType EventType, probableCause string, isRootCause bool, links AlarmLinks, ) *Alarm`

NewAlarm instantiates a new Alarm object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAlarmWithDefaults

`func NewAlarmWithDefaults() *Alarm`

NewAlarmWithDefaults instantiates a new Alarm object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Alarm) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Alarm) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Alarm) SetId(v string)`

SetId sets Id field to given value.


### GetManagedObjectId

`func (o *Alarm) GetManagedObjectId() string`

GetManagedObjectId returns the ManagedObjectId field if non-nil, zero value otherwise.

### GetManagedObjectIdOk

`func (o *Alarm) GetManagedObjectIdOk() (*string, bool)`

GetManagedObjectIdOk returns a tuple with the ManagedObjectId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetManagedObjectId

`func (o *Alarm) SetManagedObjectId(v string)`

SetManagedObjectId sets ManagedObjectId field to given value.


### GetRootCauseFaultyComponent

`func (o *Alarm) GetRootCauseFaultyComponent() FaultyComponentInfo`

GetRootCauseFaultyComponent returns the RootCauseFaultyComponent field if non-nil, zero value otherwise.

### GetRootCauseFaultyComponentOk

`func (o *Alarm) GetRootCauseFaultyComponentOk() (*FaultyComponentInfo, bool)`

GetRootCauseFaultyComponentOk returns a tuple with the RootCauseFaultyComponent field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRootCauseFaultyComponent

`func (o *Alarm) SetRootCauseFaultyComponent(v FaultyComponentInfo)`

SetRootCauseFaultyComponent sets RootCauseFaultyComponent field to given value.

### HasRootCauseFaultyComponent

`func (o *Alarm) HasRootCauseFaultyComponent() bool`

HasRootCauseFaultyComponent returns a boolean if a field has been set.

### GetRootCauseFaultyResource

`func (o *Alarm) GetRootCauseFaultyResource() FaultyResourceInfo`

GetRootCauseFaultyResource returns the RootCauseFaultyResource field if non-nil, zero value otherwise.

### GetRootCauseFaultyResourceOk

`func (o *Alarm) GetRootCauseFaultyResourceOk() (*FaultyResourceInfo, bool)`

GetRootCauseFaultyResourceOk returns a tuple with the RootCauseFaultyResource field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRootCauseFaultyResource

`func (o *Alarm) SetRootCauseFaultyResource(v FaultyResourceInfo)`

SetRootCauseFaultyResource sets RootCauseFaultyResource field to given value.

### HasRootCauseFaultyResource

`func (o *Alarm) HasRootCauseFaultyResource() bool`

HasRootCauseFaultyResource returns a boolean if a field has been set.

### GetAlarmRaisedTime

`func (o *Alarm) GetAlarmRaisedTime() time.Time`

GetAlarmRaisedTime returns the AlarmRaisedTime field if non-nil, zero value otherwise.

### GetAlarmRaisedTimeOk

`func (o *Alarm) GetAlarmRaisedTimeOk() (*time.Time, bool)`

GetAlarmRaisedTimeOk returns a tuple with the AlarmRaisedTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAlarmRaisedTime

`func (o *Alarm) SetAlarmRaisedTime(v time.Time)`

SetAlarmRaisedTime sets AlarmRaisedTime field to given value.


### GetAlarmChangedTime

`func (o *Alarm) GetAlarmChangedTime() time.Time`

GetAlarmChangedTime returns the AlarmChangedTime field if non-nil, zero value otherwise.

### GetAlarmChangedTimeOk

`func (o *Alarm) GetAlarmChangedTimeOk() (*time.Time, bool)`

GetAlarmChangedTimeOk returns a tuple with the AlarmChangedTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAlarmChangedTime

`func (o *Alarm) SetAlarmChangedTime(v time.Time)`

SetAlarmChangedTime sets AlarmChangedTime field to given value.

### HasAlarmChangedTime

`func (o *Alarm) HasAlarmChangedTime() bool`

HasAlarmChangedTime returns a boolean if a field has been set.

### GetAlarmClearedTime

`func (o *Alarm) GetAlarmClearedTime() time.Time`

GetAlarmClearedTime returns the AlarmClearedTime field if non-nil, zero value otherwise.

### GetAlarmClearedTimeOk

`func (o *Alarm) GetAlarmClearedTimeOk() (*time.Time, bool)`

GetAlarmClearedTimeOk returns a tuple with the AlarmClearedTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAlarmClearedTime

`func (o *Alarm) SetAlarmClearedTime(v time.Time)`

SetAlarmClearedTime sets AlarmClearedTime field to given value.

### HasAlarmClearedTime

`func (o *Alarm) HasAlarmClearedTime() bool`

HasAlarmClearedTime returns a boolean if a field has been set.

### GetAlarmAcknowledgedTime

`func (o *Alarm) GetAlarmAcknowledgedTime() time.Time`

GetAlarmAcknowledgedTime returns the AlarmAcknowledgedTime field if non-nil, zero value otherwise.

### GetAlarmAcknowledgedTimeOk

`func (o *Alarm) GetAlarmAcknowledgedTimeOk() (*time.Time, bool)`

GetAlarmAcknowledgedTimeOk returns a tuple with the AlarmAcknowledgedTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAlarmAcknowledgedTime

`func (o *Alarm) SetAlarmAcknowledgedTime(v time.Time)`

SetAlarmAcknowledgedTime sets AlarmAcknowledgedTime field to given value.

### HasAlarmAcknowledgedTime

`func (o *Alarm) HasAlarmAcknowledgedTime() bool`

HasAlarmAcknowledgedTime returns a boolean if a field has been set.

### GetAckState

`func (o *Alarm) GetAckState() string`

GetAckState returns the AckState field if non-nil, zero value otherwise.

### GetAckStateOk

`func (o *Alarm) GetAckStateOk() (*string, bool)`

GetAckStateOk returns a tuple with the AckState field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAckState

`func (o *Alarm) SetAckState(v string)`

SetAckState sets AckState field to given value.


### GetPerceivedSeverity

`func (o *Alarm) GetPerceivedSeverity() PerceivedSeverityType`

GetPerceivedSeverity returns the PerceivedSeverity field if non-nil, zero value otherwise.

### GetPerceivedSeverityOk

`func (o *Alarm) GetPerceivedSeverityOk() (*PerceivedSeverityType, bool)`

GetPerceivedSeverityOk returns a tuple with the PerceivedSeverity field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPerceivedSeverity

`func (o *Alarm) SetPerceivedSeverity(v PerceivedSeverityType)`

SetPerceivedSeverity sets PerceivedSeverity field to given value.


### GetEventTime

`func (o *Alarm) GetEventTime() time.Time`

GetEventTime returns the EventTime field if non-nil, zero value otherwise.

### GetEventTimeOk

`func (o *Alarm) GetEventTimeOk() (*time.Time, bool)`

GetEventTimeOk returns a tuple with the EventTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEventTime

`func (o *Alarm) SetEventTime(v time.Time)`

SetEventTime sets EventTime field to given value.


### GetEventType

`func (o *Alarm) GetEventType() EventType`

GetEventType returns the EventType field if non-nil, zero value otherwise.

### GetEventTypeOk

`func (o *Alarm) GetEventTypeOk() (*EventType, bool)`

GetEventTypeOk returns a tuple with the EventType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEventType

`func (o *Alarm) SetEventType(v EventType)`

SetEventType sets EventType field to given value.


### GetFaultType

`func (o *Alarm) GetFaultType() string`

GetFaultType returns the FaultType field if non-nil, zero value otherwise.

### GetFaultTypeOk

`func (o *Alarm) GetFaultTypeOk() (*string, bool)`

GetFaultTypeOk returns a tuple with the FaultType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFaultType

`func (o *Alarm) SetFaultType(v string)`

SetFaultType sets FaultType field to given value.

### HasFaultType

`func (o *Alarm) HasFaultType() bool`

HasFaultType returns a boolean if a field has been set.

### GetProbableCause

`func (o *Alarm) GetProbableCause() string`

GetProbableCause returns the ProbableCause field if non-nil, zero value otherwise.

### GetProbableCauseOk

`func (o *Alarm) GetProbableCauseOk() (*string, bool)`

GetProbableCauseOk returns a tuple with the ProbableCause field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProbableCause

`func (o *Alarm) SetProbableCause(v string)`

SetProbableCause sets ProbableCause field to given value.


### GetIsRootCause

`func (o *Alarm) GetIsRootCause() bool`

GetIsRootCause returns the IsRootCause field if non-nil, zero value otherwise.

### GetIsRootCauseOk

`func (o *Alarm) GetIsRootCauseOk() (*bool, bool)`

GetIsRootCauseOk returns a tuple with the IsRootCause field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIsRootCause

`func (o *Alarm) SetIsRootCause(v bool)`

SetIsRootCause sets IsRootCause field to given value.


### GetCorrelatedAlarmIds

`func (o *Alarm) GetCorrelatedAlarmIds() []string`

GetCorrelatedAlarmIds returns the CorrelatedAlarmIds field if non-nil, zero value otherwise.

### GetCorrelatedAlarmIdsOk

`func (o *Alarm) GetCorrelatedAlarmIdsOk() (*[]string, bool)`

GetCorrelatedAlarmIdsOk returns a tuple with the CorrelatedAlarmIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCorrelatedAlarmIds

`func (o *Alarm) SetCorrelatedAlarmIds(v []string)`

SetCorrelatedAlarmIds sets CorrelatedAlarmIds field to given value.

### HasCorrelatedAlarmIds

`func (o *Alarm) HasCorrelatedAlarmIds() bool`

HasCorrelatedAlarmIds returns a boolean if a field has been set.

### GetFaultDetails

`func (o *Alarm) GetFaultDetails() []string`

GetFaultDetails returns the FaultDetails field if non-nil, zero value otherwise.

### GetFaultDetailsOk

`func (o *Alarm) GetFaultDetailsOk() (*[]string, bool)`

GetFaultDetailsOk returns a tuple with the FaultDetails field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFaultDetails

`func (o *Alarm) SetFaultDetails(v []string)`

SetFaultDetails sets FaultDetails field to given value.

### HasFaultDetails

`func (o *Alarm) HasFaultDetails() bool`

HasFaultDetails returns a boolean if a field has been set.

### GetLinks

`func (o *Alarm) GetLinks() AlarmLinks`

GetLinks returns the Links field if non-nil, zero value otherwise.

### GetLinksOk

`func (o *Alarm) GetLinksOk() (*AlarmLinks, bool)`

GetLinksOk returns a tuple with the Links field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLinks

`func (o *Alarm) SetLinks(v AlarmLinks)`

SetLinks sets Links field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


