# PerceivedSeverityType

## Enum


* `CRITICAL` (value: `"CRITICAL"`)

* `MAJOR` (value: `"MAJOR"`)

* `MINOR` (value: `"MINOR"`)

* `WARNING` (value: `"WARNING"`)

* `INDETERMINATE` (value: `"INDETERMINATE"`)

* `CLEARED` (value: `"CLEARED"`)


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


