# FmSubscriptionRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Filter** | Pointer to [**FmNotificationsFilter**](FmNotificationsFilter.md) |  | [optional] 
**CallbackUri** | **string** | String formatted according to IETF RFC 3986.  | 
**Authentication** | Pointer to [**SubscriptionAuthentication**](SubscriptionAuthentication.md) |  | [optional] 

## Methods

### NewFmSubscriptionRequest

`func NewFmSubscriptionRequest(callbackUri string, ) *FmSubscriptionRequest`

NewFmSubscriptionRequest instantiates a new FmSubscriptionRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewFmSubscriptionRequestWithDefaults

`func NewFmSubscriptionRequestWithDefaults() *FmSubscriptionRequest`

NewFmSubscriptionRequestWithDefaults instantiates a new FmSubscriptionRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFilter

`func (o *FmSubscriptionRequest) GetFilter() FmNotificationsFilter`

GetFilter returns the Filter field if non-nil, zero value otherwise.

### GetFilterOk

`func (o *FmSubscriptionRequest) GetFilterOk() (*FmNotificationsFilter, bool)`

GetFilterOk returns a tuple with the Filter field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFilter

`func (o *FmSubscriptionRequest) SetFilter(v FmNotificationsFilter)`

SetFilter sets Filter field to given value.

### HasFilter

`func (o *FmSubscriptionRequest) HasFilter() bool`

HasFilter returns a boolean if a field has been set.

### GetCallbackUri

`func (o *FmSubscriptionRequest) GetCallbackUri() string`

GetCallbackUri returns the CallbackUri field if non-nil, zero value otherwise.

### GetCallbackUriOk

`func (o *FmSubscriptionRequest) GetCallbackUriOk() (*string, bool)`

GetCallbackUriOk returns a tuple with the CallbackUri field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCallbackUri

`func (o *FmSubscriptionRequest) SetCallbackUri(v string)`

SetCallbackUri sets CallbackUri field to given value.


### GetAuthentication

`func (o *FmSubscriptionRequest) GetAuthentication() SubscriptionAuthentication`

GetAuthentication returns the Authentication field if non-nil, zero value otherwise.

### GetAuthenticationOk

`func (o *FmSubscriptionRequest) GetAuthenticationOk() (*SubscriptionAuthentication, bool)`

GetAuthenticationOk returns a tuple with the Authentication field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAuthentication

`func (o *FmSubscriptionRequest) SetAuthentication(v SubscriptionAuthentication)`

SetAuthentication sets Authentication field to given value.

### HasAuthentication

`func (o *FmSubscriptionRequest) HasAuthentication() bool`

HasAuthentication returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


