# FmNotificationsFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NsInstanceSubscriptionFilter** | Pointer to [**NsInstanceSubscriptionFilter**](NsInstanceSubscriptionFilter.md) |  | [optional] 
**NotificationTypes** | Pointer to **[]string** | Match particular notification types. Permitted values: - AlarmNotification - AlarmClearedNotification - AlarmListRebuiltNotification. See note.  | [optional] 
**FaultyResourceTypes** | Pointer to [**[]FaultyResourceType**](FaultyResourceType.md) | Match alarms related to NSs with a faulty resource type listed in this attribute.  | [optional] 
**PerceivedSeverities** | Pointer to [**[]PerceivedSeverityType**](PerceivedSeverityType.md) | Match VNF alarms with a perceived severity listed in this attribute.  | [optional] 
**EventTypes** | Pointer to [**[]EventType**](EventType.md) | Match VNF alarms related to NSs with an event type listed  in this attribute.  | [optional] 
**ProbableCauses** | Pointer to **[]string** | Match VNF alarms with a probable cause listed in this attribute.  | [optional] 

## Methods

### NewFmNotificationsFilter

`func NewFmNotificationsFilter() *FmNotificationsFilter`

NewFmNotificationsFilter instantiates a new FmNotificationsFilter object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewFmNotificationsFilterWithDefaults

`func NewFmNotificationsFilterWithDefaults() *FmNotificationsFilter`

NewFmNotificationsFilterWithDefaults instantiates a new FmNotificationsFilter object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetNsInstanceSubscriptionFilter

`func (o *FmNotificationsFilter) GetNsInstanceSubscriptionFilter() NsInstanceSubscriptionFilter`

GetNsInstanceSubscriptionFilter returns the NsInstanceSubscriptionFilter field if non-nil, zero value otherwise.

### GetNsInstanceSubscriptionFilterOk

`func (o *FmNotificationsFilter) GetNsInstanceSubscriptionFilterOk() (*NsInstanceSubscriptionFilter, bool)`

GetNsInstanceSubscriptionFilterOk returns a tuple with the NsInstanceSubscriptionFilter field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNsInstanceSubscriptionFilter

`func (o *FmNotificationsFilter) SetNsInstanceSubscriptionFilter(v NsInstanceSubscriptionFilter)`

SetNsInstanceSubscriptionFilter sets NsInstanceSubscriptionFilter field to given value.

### HasNsInstanceSubscriptionFilter

`func (o *FmNotificationsFilter) HasNsInstanceSubscriptionFilter() bool`

HasNsInstanceSubscriptionFilter returns a boolean if a field has been set.

### GetNotificationTypes

`func (o *FmNotificationsFilter) GetNotificationTypes() []string`

GetNotificationTypes returns the NotificationTypes field if non-nil, zero value otherwise.

### GetNotificationTypesOk

`func (o *FmNotificationsFilter) GetNotificationTypesOk() (*[]string, bool)`

GetNotificationTypesOk returns a tuple with the NotificationTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNotificationTypes

`func (o *FmNotificationsFilter) SetNotificationTypes(v []string)`

SetNotificationTypes sets NotificationTypes field to given value.

### HasNotificationTypes

`func (o *FmNotificationsFilter) HasNotificationTypes() bool`

HasNotificationTypes returns a boolean if a field has been set.

### GetFaultyResourceTypes

`func (o *FmNotificationsFilter) GetFaultyResourceTypes() []FaultyResourceType`

GetFaultyResourceTypes returns the FaultyResourceTypes field if non-nil, zero value otherwise.

### GetFaultyResourceTypesOk

`func (o *FmNotificationsFilter) GetFaultyResourceTypesOk() (*[]FaultyResourceType, bool)`

GetFaultyResourceTypesOk returns a tuple with the FaultyResourceTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFaultyResourceTypes

`func (o *FmNotificationsFilter) SetFaultyResourceTypes(v []FaultyResourceType)`

SetFaultyResourceTypes sets FaultyResourceTypes field to given value.

### HasFaultyResourceTypes

`func (o *FmNotificationsFilter) HasFaultyResourceTypes() bool`

HasFaultyResourceTypes returns a boolean if a field has been set.

### GetPerceivedSeverities

`func (o *FmNotificationsFilter) GetPerceivedSeverities() []PerceivedSeverityType`

GetPerceivedSeverities returns the PerceivedSeverities field if non-nil, zero value otherwise.

### GetPerceivedSeveritiesOk

`func (o *FmNotificationsFilter) GetPerceivedSeveritiesOk() (*[]PerceivedSeverityType, bool)`

GetPerceivedSeveritiesOk returns a tuple with the PerceivedSeverities field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPerceivedSeverities

`func (o *FmNotificationsFilter) SetPerceivedSeverities(v []PerceivedSeverityType)`

SetPerceivedSeverities sets PerceivedSeverities field to given value.

### HasPerceivedSeverities

`func (o *FmNotificationsFilter) HasPerceivedSeverities() bool`

HasPerceivedSeverities returns a boolean if a field has been set.

### GetEventTypes

`func (o *FmNotificationsFilter) GetEventTypes() []EventType`

GetEventTypes returns the EventTypes field if non-nil, zero value otherwise.

### GetEventTypesOk

`func (o *FmNotificationsFilter) GetEventTypesOk() (*[]EventType, bool)`

GetEventTypesOk returns a tuple with the EventTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEventTypes

`func (o *FmNotificationsFilter) SetEventTypes(v []EventType)`

SetEventTypes sets EventTypes field to given value.

### HasEventTypes

`func (o *FmNotificationsFilter) HasEventTypes() bool`

HasEventTypes returns a boolean if a field has been set.

### GetProbableCauses

`func (o *FmNotificationsFilter) GetProbableCauses() []string`

GetProbableCauses returns the ProbableCauses field if non-nil, zero value otherwise.

### GetProbableCausesOk

`func (o *FmNotificationsFilter) GetProbableCausesOk() (*[]string, bool)`

GetProbableCausesOk returns a tuple with the ProbableCauses field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProbableCauses

`func (o *FmNotificationsFilter) SetProbableCauses(v []string)`

SetProbableCauses sets ProbableCauses field to given value.

### HasProbableCauses

`func (o *FmNotificationsFilter) HasProbableCauses() bool`

HasProbableCauses returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


