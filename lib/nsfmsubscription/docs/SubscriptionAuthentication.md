# SubscriptionAuthentication

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AuthType** | **[]string** | Defines the types of Authentication / Authorization which the API consumer is willing to accept when receiving a notification. Permitted values: - BASIC: In every HTTP request to the notification endpoint, use   HTTP Basic authentication with the client credentials. - OAUTH2_CLIENT_CREDENTIALS: In every HTTP request to the   notification endpoint, use an OAuth 2.0 Bearer token, obtained   using the client credentials grant type. - TLS_CERT: Every HTTP request to the notification endpoint is sent   over a mutually authenticated TLS session, i.e. not only the   server is authenticated, but also the client is authenticated   during the TLS tunnel setup.  | 
**ParamsBasic** | Pointer to [**SubscriptionAuthenticationParamsBasic**](SubscriptionAuthenticationParamsBasic.md) |  | [optional] 
**ParamsOauth2ClientCredentials** | Pointer to [**SubscriptionAuthenticationParamsOauth2ClientCredentials**](SubscriptionAuthenticationParamsOauth2ClientCredentials.md) |  | [optional] 

## Methods

### NewSubscriptionAuthentication

`func NewSubscriptionAuthentication(authType []string, ) *SubscriptionAuthentication`

NewSubscriptionAuthentication instantiates a new SubscriptionAuthentication object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSubscriptionAuthenticationWithDefaults

`func NewSubscriptionAuthenticationWithDefaults() *SubscriptionAuthentication`

NewSubscriptionAuthenticationWithDefaults instantiates a new SubscriptionAuthentication object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAuthType

`func (o *SubscriptionAuthentication) GetAuthType() []string`

GetAuthType returns the AuthType field if non-nil, zero value otherwise.

### GetAuthTypeOk

`func (o *SubscriptionAuthentication) GetAuthTypeOk() (*[]string, bool)`

GetAuthTypeOk returns a tuple with the AuthType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAuthType

`func (o *SubscriptionAuthentication) SetAuthType(v []string)`

SetAuthType sets AuthType field to given value.


### GetParamsBasic

`func (o *SubscriptionAuthentication) GetParamsBasic() SubscriptionAuthenticationParamsBasic`

GetParamsBasic returns the ParamsBasic field if non-nil, zero value otherwise.

### GetParamsBasicOk

`func (o *SubscriptionAuthentication) GetParamsBasicOk() (*SubscriptionAuthenticationParamsBasic, bool)`

GetParamsBasicOk returns a tuple with the ParamsBasic field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetParamsBasic

`func (o *SubscriptionAuthentication) SetParamsBasic(v SubscriptionAuthenticationParamsBasic)`

SetParamsBasic sets ParamsBasic field to given value.

### HasParamsBasic

`func (o *SubscriptionAuthentication) HasParamsBasic() bool`

HasParamsBasic returns a boolean if a field has been set.

### GetParamsOauth2ClientCredentials

`func (o *SubscriptionAuthentication) GetParamsOauth2ClientCredentials() SubscriptionAuthenticationParamsOauth2ClientCredentials`

GetParamsOauth2ClientCredentials returns the ParamsOauth2ClientCredentials field if non-nil, zero value otherwise.

### GetParamsOauth2ClientCredentialsOk

`func (o *SubscriptionAuthentication) GetParamsOauth2ClientCredentialsOk() (*SubscriptionAuthenticationParamsOauth2ClientCredentials, bool)`

GetParamsOauth2ClientCredentialsOk returns a tuple with the ParamsOauth2ClientCredentials field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetParamsOauth2ClientCredentials

`func (o *SubscriptionAuthentication) SetParamsOauth2ClientCredentials(v SubscriptionAuthenticationParamsOauth2ClientCredentials)`

SetParamsOauth2ClientCredentials sets ParamsOauth2ClientCredentials field to given value.

### HasParamsOauth2ClientCredentials

`func (o *SubscriptionAuthentication) HasParamsOauth2ClientCredentials() bool`

HasParamsOauth2ClientCredentials returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


