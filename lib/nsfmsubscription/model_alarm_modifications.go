/*
SOL005 - NS Fault Management Interface

SOL005 - NS Fault Management Interface  IMPORTANT: Please note that this file might be not aligned to the current version of the ETSI Group Specification it refers to. In case of discrepancies the published ETSI Group Specification takes precedence.  Please report bugs to https://forge.etsi.org/rep/nfv/SOL005/issues 

API version: 1.2.0-impl:etsi.org:ETSI_NFV_OpenAPI:1
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// AlarmModifications This type represents attribute modifications for an \"Individual alarm\" resource, i.e. modifications to a resource representation based on the \"Alarm\" data type. The attributes of \"Alarm\" that can be modified according to the provisions in clause 8.5.2.4 are included in the \"AlarmModifications\" data type. The \"AlarmModifications\" data type shall comply with the provisions defined in Table 8.5.2.8-1. 
type AlarmModifications struct {
	// New value of the \"ackState\" attribute in \"Alarm\". Permitted values: - ACKNOWLEDGED - UNACKNOWLEDGED 
	AckState string `json:"ackState"`
}

// NewAlarmModifications instantiates a new AlarmModifications object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewAlarmModifications(ackState string) *AlarmModifications {
	this := AlarmModifications{}
	this.AckState = ackState
	return &this
}

// NewAlarmModificationsWithDefaults instantiates a new AlarmModifications object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewAlarmModificationsWithDefaults() *AlarmModifications {
	this := AlarmModifications{}
	return &this
}

// GetAckState returns the AckState field value
func (o *AlarmModifications) GetAckState() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.AckState
}

// GetAckStateOk returns a tuple with the AckState field value
// and a boolean to check if the value has been set.
func (o *AlarmModifications) GetAckStateOk() (*string, bool) {
	if o == nil {
    return nil, false
	}
	return &o.AckState, true
}

// SetAckState sets field value
func (o *AlarmModifications) SetAckState(v string) {
	o.AckState = v
}

func (o AlarmModifications) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["ackState"] = o.AckState
	}
	return json.Marshal(toSerialize)
}

type NullableAlarmModifications struct {
	value *AlarmModifications
	isSet bool
}

func (v NullableAlarmModifications) Get() *AlarmModifications {
	return v.value
}

func (v *NullableAlarmModifications) Set(val *AlarmModifications) {
	v.value = val
	v.isSet = true
}

func (v NullableAlarmModifications) IsSet() bool {
	return v.isSet
}

func (v *NullableAlarmModifications) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableAlarmModifications(val *AlarmModifications) *NullableAlarmModifications {
	return &NullableAlarmModifications{value: val, isSet: true}
}

func (v NullableAlarmModifications) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableAlarmModifications) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


