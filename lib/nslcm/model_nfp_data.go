/*
SOL005 - NS Lifecycle Management Interface

SOL005 - NS Lifecycle Management Interface  IMPORTANT: Please note that this file might be not aligned to the current version of the ETSI Group Specification it refers to. In case of discrepancies the published ETSI Group Specification takes precedence.  Please report bugs to https://forge.etsi.org/rep/nfv/SOL005/issues 

API version: 2.1.0-impl:etsi.org:ETSI_NFV_OpenAPI:2
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// NfpData This type contains information used to create or modify NFP instance parameters for the update of an existing VNFFG instance. It shall comply with the provisions defined in Table 6.5.3.38-1. NOTE 1: It shall be present for modified NFPs and shall be absent for the new NFP. NOTE 2: It shall be present for the new NFP, and it may be present otherwise. NOTE 3: At least a CP or an nfpRule shall be present. NOTE 4:  When multiple identifiers are included, the position of the identifier in the cpGroup value specifies the position of the group in the path. 
type NfpData struct {
	// An identifier that is unique with respect to a NS. Representation: string of variable length. 
	NfpInfoId *string `json:"nfpInfoId,omitempty"`
	// Human readable name for the NFP. It shall be present for the new NFP, and it may be present otherwise. See note 2. 
	NfpName *string `json:"nfpName,omitempty"`
	// Human readable description for the NFP. It shall be present for the new NFP, and it may be present otherwise. See note 2. 
	Description *string `json:"description,omitempty"`
	// Group(s) of CPs and/or SAPs which the NFP passes by. Cardinality can be 0 if only updated or newly created NFP classification and selection rule which applied to an existing NFP is provided. See note 3 and 4. 
	CpGroup []CpGroupInfo `json:"cpGroup,omitempty"`
	NfpRule *NfpRule `json:"nfpRule,omitempty"`
}

// NewNfpData instantiates a new NfpData object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewNfpData() *NfpData {
	this := NfpData{}
	return &this
}

// NewNfpDataWithDefaults instantiates a new NfpData object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewNfpDataWithDefaults() *NfpData {
	this := NfpData{}
	return &this
}

// GetNfpInfoId returns the NfpInfoId field value if set, zero value otherwise.
func (o *NfpData) GetNfpInfoId() string {
	if o == nil || o.NfpInfoId == nil {
		var ret string
		return ret
	}
	return *o.NfpInfoId
}

// GetNfpInfoIdOk returns a tuple with the NfpInfoId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NfpData) GetNfpInfoIdOk() (*string, bool) {
	if o == nil || o.NfpInfoId == nil {
		return nil, false
	}
	return o.NfpInfoId, true
}

// HasNfpInfoId returns a boolean if a field has been set.
func (o *NfpData) HasNfpInfoId() bool {
	if o != nil && o.NfpInfoId != nil {
		return true
	}

	return false
}

// SetNfpInfoId gets a reference to the given string and assigns it to the NfpInfoId field.
func (o *NfpData) SetNfpInfoId(v string) {
	o.NfpInfoId = &v
}

// GetNfpName returns the NfpName field value if set, zero value otherwise.
func (o *NfpData) GetNfpName() string {
	if o == nil || o.NfpName == nil {
		var ret string
		return ret
	}
	return *o.NfpName
}

// GetNfpNameOk returns a tuple with the NfpName field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NfpData) GetNfpNameOk() (*string, bool) {
	if o == nil || o.NfpName == nil {
		return nil, false
	}
	return o.NfpName, true
}

// HasNfpName returns a boolean if a field has been set.
func (o *NfpData) HasNfpName() bool {
	if o != nil && o.NfpName != nil {
		return true
	}

	return false
}

// SetNfpName gets a reference to the given string and assigns it to the NfpName field.
func (o *NfpData) SetNfpName(v string) {
	o.NfpName = &v
}

// GetDescription returns the Description field value if set, zero value otherwise.
func (o *NfpData) GetDescription() string {
	if o == nil || o.Description == nil {
		var ret string
		return ret
	}
	return *o.Description
}

// GetDescriptionOk returns a tuple with the Description field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NfpData) GetDescriptionOk() (*string, bool) {
	if o == nil || o.Description == nil {
		return nil, false
	}
	return o.Description, true
}

// HasDescription returns a boolean if a field has been set.
func (o *NfpData) HasDescription() bool {
	if o != nil && o.Description != nil {
		return true
	}

	return false
}

// SetDescription gets a reference to the given string and assigns it to the Description field.
func (o *NfpData) SetDescription(v string) {
	o.Description = &v
}

// GetCpGroup returns the CpGroup field value if set, zero value otherwise.
func (o *NfpData) GetCpGroup() []CpGroupInfo {
	if o == nil || o.CpGroup == nil {
		var ret []CpGroupInfo
		return ret
	}
	return o.CpGroup
}

// GetCpGroupOk returns a tuple with the CpGroup field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NfpData) GetCpGroupOk() ([]CpGroupInfo, bool) {
	if o == nil || o.CpGroup == nil {
		return nil, false
	}
	return o.CpGroup, true
}

// HasCpGroup returns a boolean if a field has been set.
func (o *NfpData) HasCpGroup() bool {
	if o != nil && o.CpGroup != nil {
		return true
	}

	return false
}

// SetCpGroup gets a reference to the given []CpGroupInfo and assigns it to the CpGroup field.
func (o *NfpData) SetCpGroup(v []CpGroupInfo) {
	o.CpGroup = v
}

// GetNfpRule returns the NfpRule field value if set, zero value otherwise.
func (o *NfpData) GetNfpRule() NfpRule {
	if o == nil || o.NfpRule == nil {
		var ret NfpRule
		return ret
	}
	return *o.NfpRule
}

// GetNfpRuleOk returns a tuple with the NfpRule field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NfpData) GetNfpRuleOk() (*NfpRule, bool) {
	if o == nil || o.NfpRule == nil {
		return nil, false
	}
	return o.NfpRule, true
}

// HasNfpRule returns a boolean if a field has been set.
func (o *NfpData) HasNfpRule() bool {
	if o != nil && o.NfpRule != nil {
		return true
	}

	return false
}

// SetNfpRule gets a reference to the given NfpRule and assigns it to the NfpRule field.
func (o *NfpData) SetNfpRule(v NfpRule) {
	o.NfpRule = &v
}

func (o NfpData) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.NfpInfoId != nil {
		toSerialize["nfpInfoId"] = o.NfpInfoId
	}
	if o.NfpName != nil {
		toSerialize["nfpName"] = o.NfpName
	}
	if o.Description != nil {
		toSerialize["description"] = o.Description
	}
	if o.CpGroup != nil {
		toSerialize["cpGroup"] = o.CpGroup
	}
	if o.NfpRule != nil {
		toSerialize["nfpRule"] = o.NfpRule
	}
	return json.Marshal(toSerialize)
}

type NullableNfpData struct {
	value *NfpData
	isSet bool
}

func (v NullableNfpData) Get() *NfpData {
	return v.value
}

func (v *NullableNfpData) Set(val *NfpData) {
	v.value = val
	v.isSet = true
}

func (v NullableNfpData) IsSet() bool {
	return v.isSet
}

func (v *NullableNfpData) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableNfpData(val *NfpData) *NullableNfpData {
	return &NullableNfpData{value: val, isSet: true}
}

func (v NullableNfpData) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableNfpData) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


