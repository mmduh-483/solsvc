/*
SOL005 - NS Lifecycle Management Interface

SOL005 - NS Lifecycle Management Interface  IMPORTANT: Please note that this file might be not aligned to the current version of the ETSI Group Specification it refers to. In case of discrepancies the published ETSI Group Specification takes precedence.  Please report bugs to https://forge.etsi.org/rep/nfv/SOL005/issues 

API version: 2.1.0-impl:etsi.org:ETSI_NFV_OpenAPI:2
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// NsLinkPortInfo This type represents information about a link port of a VL instance. It shall comply with the provisions defined in Table 6.5.3.55-1. NOTE: When the NsVirtualLink, from which the present NsLinkPort is part of, is provided as an ExtVirtualLink as input of a VNF LCM operation, the id of the ExtLinkPort shall be the same as the corresponding NsLinkPort. 
type NsLinkPortInfo struct {
	// An identifier with the intention of being globally unique. 
	Id string `json:"id"`
	ResourceHandle ResourceHandle `json:"resourceHandle"`
	NsCpHandle *NsCpHandle `json:"nsCpHandle,omitempty"`
}

// NewNsLinkPortInfo instantiates a new NsLinkPortInfo object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewNsLinkPortInfo(id string, resourceHandle ResourceHandle) *NsLinkPortInfo {
	this := NsLinkPortInfo{}
	this.Id = id
	this.ResourceHandle = resourceHandle
	return &this
}

// NewNsLinkPortInfoWithDefaults instantiates a new NsLinkPortInfo object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewNsLinkPortInfoWithDefaults() *NsLinkPortInfo {
	this := NsLinkPortInfo{}
	return &this
}

// GetId returns the Id field value
func (o *NsLinkPortInfo) GetId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Id
}

// GetIdOk returns a tuple with the Id field value
// and a boolean to check if the value has been set.
func (o *NsLinkPortInfo) GetIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.Id, true
}

// SetId sets field value
func (o *NsLinkPortInfo) SetId(v string) {
	o.Id = v
}

// GetResourceHandle returns the ResourceHandle field value
func (o *NsLinkPortInfo) GetResourceHandle() ResourceHandle {
	if o == nil {
		var ret ResourceHandle
		return ret
	}

	return o.ResourceHandle
}

// GetResourceHandleOk returns a tuple with the ResourceHandle field value
// and a boolean to check if the value has been set.
func (o *NsLinkPortInfo) GetResourceHandleOk() (*ResourceHandle, bool) {
	if o == nil {
		return nil, false
	}
	return &o.ResourceHandle, true
}

// SetResourceHandle sets field value
func (o *NsLinkPortInfo) SetResourceHandle(v ResourceHandle) {
	o.ResourceHandle = v
}

// GetNsCpHandle returns the NsCpHandle field value if set, zero value otherwise.
func (o *NsLinkPortInfo) GetNsCpHandle() NsCpHandle {
	if o == nil || o.NsCpHandle == nil {
		var ret NsCpHandle
		return ret
	}
	return *o.NsCpHandle
}

// GetNsCpHandleOk returns a tuple with the NsCpHandle field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NsLinkPortInfo) GetNsCpHandleOk() (*NsCpHandle, bool) {
	if o == nil || o.NsCpHandle == nil {
		return nil, false
	}
	return o.NsCpHandle, true
}

// HasNsCpHandle returns a boolean if a field has been set.
func (o *NsLinkPortInfo) HasNsCpHandle() bool {
	if o != nil && o.NsCpHandle != nil {
		return true
	}

	return false
}

// SetNsCpHandle gets a reference to the given NsCpHandle and assigns it to the NsCpHandle field.
func (o *NsLinkPortInfo) SetNsCpHandle(v NsCpHandle) {
	o.NsCpHandle = &v
}

func (o NsLinkPortInfo) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["id"] = o.Id
	}
	if true {
		toSerialize["resourceHandle"] = o.ResourceHandle
	}
	if o.NsCpHandle != nil {
		toSerialize["nsCpHandle"] = o.NsCpHandle
	}
	return json.Marshal(toSerialize)
}

type NullableNsLinkPortInfo struct {
	value *NsLinkPortInfo
	isSet bool
}

func (v NullableNsLinkPortInfo) Get() *NsLinkPortInfo {
	return v.value
}

func (v *NullableNsLinkPortInfo) Set(val *NsLinkPortInfo) {
	v.value = val
	v.isSet = true
}

func (v NullableNsLinkPortInfo) IsSet() bool {
	return v.isSet
}

func (v *NullableNsLinkPortInfo) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableNsLinkPortInfo(val *NsLinkPortInfo) *NullableNsLinkPortInfo {
	return &NullableNsLinkPortInfo{value: val, isSet: true}
}

func (v NullableNsLinkPortInfo) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableNsLinkPortInfo) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


