/*
SOL005 - NS Lifecycle Management Interface

SOL005 - NS Lifecycle Management Interface  IMPORTANT: Please note that this file might be not aligned to the current version of the ETSI Group Specification it refers to. In case of discrepancies the published ETSI Group Specification takes precedence.  Please report bugs to https://forge.etsi.org/rep/nfv/SOL005/issues 

API version: 2.1.0-impl:etsi.org:ETSI_NFV_OpenAPI:2
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// ScaleNsData This type represents the information to scale a NS. NOTE 1: No more than two attributes between vnfInstanceToBeAdded, vnfInstanceToBeRemoved, scaleNsByStepsData and scaleNsToLevelData shall be present. In case of two, the attributes shall be vnfInstanceToBeAdded and vnfInstanceToBeRemoved. NOTE 2: The DF of the VNF instance shall match the VNF DF present in the associated VNF Profile of the new NS flavour. NOTE 3: This functionality is the same as the one provided by the Update NS operation when the AddVnf update type is selected (see clause 7.3.5). NOTE 4: This functionality is the same as the one provided by the Update NS operation when the RemoveVnf update type is selected (see clause 7.3.5). 
type ScaleNsData struct {
	// An existing VNF instance to be added to the NS instance as part of the scaling operation. If needed, the VNF Profile to be used for this VNF instance may also be provided. See note 1, note 2 and note 3. 
	VnfInstanceToBeAdded []VnfInstanceData `json:"vnfInstanceToBeAdded,omitempty"`
	// The VNF instance to be removed from the NS instance as part of the scaling operation. See note 1 and note 4. 
	VnfInstanceToBeRemoved []string `json:"vnfInstanceToBeRemoved,omitempty"`
	ScaleNsByStepsData *ScaleNsByStepsData `json:"scaleNsByStepsData,omitempty"`
	ScaleNsToLevelData *ScaleNsToLevelData `json:"scaleNsToLevelData,omitempty"`
	AdditionalParamsForNs *ParamsForVnf `json:"additionalParamsForNs,omitempty"`
	// Allows the OSS/BSS to provide additional parameter(s) per VNF instance (as opposed to the NS level, which is covered in additionalParamsforNs). This is for VNFs that are to be created by the NFVO as part of the NS scaling and not for existing VNF that are covered by the scaleVnfData. 
	AdditionalParamsForVnf []ParamsForVnf `json:"additionalParamsForVnf,omitempty"`
	// The location constraints for the VNF to be instantiated as part of the NS scaling. An example can be a constraint for the VNF to be in a specific geographic location. 
	LocationConstraints []VnfLocationConstraint `json:"locationConstraints,omitempty"`
	// Defines the location constraints for the nested NS to be instantiated as part of the NS instantiation. An example can be a constraint for the nested NS to be in a specific geographic location. 
	NestedNslocationConstraints []NestedNsLocationConstraint `json:"nestedNslocationConstraints,omitempty"`
}

// NewScaleNsData instantiates a new ScaleNsData object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewScaleNsData() *ScaleNsData {
	this := ScaleNsData{}
	return &this
}

// NewScaleNsDataWithDefaults instantiates a new ScaleNsData object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewScaleNsDataWithDefaults() *ScaleNsData {
	this := ScaleNsData{}
	return &this
}

// GetVnfInstanceToBeAdded returns the VnfInstanceToBeAdded field value if set, zero value otherwise.
func (o *ScaleNsData) GetVnfInstanceToBeAdded() []VnfInstanceData {
	if o == nil || o.VnfInstanceToBeAdded == nil {
		var ret []VnfInstanceData
		return ret
	}
	return o.VnfInstanceToBeAdded
}

// GetVnfInstanceToBeAddedOk returns a tuple with the VnfInstanceToBeAdded field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ScaleNsData) GetVnfInstanceToBeAddedOk() ([]VnfInstanceData, bool) {
	if o == nil || o.VnfInstanceToBeAdded == nil {
		return nil, false
	}
	return o.VnfInstanceToBeAdded, true
}

// HasVnfInstanceToBeAdded returns a boolean if a field has been set.
func (o *ScaleNsData) HasVnfInstanceToBeAdded() bool {
	if o != nil && o.VnfInstanceToBeAdded != nil {
		return true
	}

	return false
}

// SetVnfInstanceToBeAdded gets a reference to the given []VnfInstanceData and assigns it to the VnfInstanceToBeAdded field.
func (o *ScaleNsData) SetVnfInstanceToBeAdded(v []VnfInstanceData) {
	o.VnfInstanceToBeAdded = v
}

// GetVnfInstanceToBeRemoved returns the VnfInstanceToBeRemoved field value if set, zero value otherwise.
func (o *ScaleNsData) GetVnfInstanceToBeRemoved() []string {
	if o == nil || o.VnfInstanceToBeRemoved == nil {
		var ret []string
		return ret
	}
	return o.VnfInstanceToBeRemoved
}

// GetVnfInstanceToBeRemovedOk returns a tuple with the VnfInstanceToBeRemoved field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ScaleNsData) GetVnfInstanceToBeRemovedOk() ([]string, bool) {
	if o == nil || o.VnfInstanceToBeRemoved == nil {
		return nil, false
	}
	return o.VnfInstanceToBeRemoved, true
}

// HasVnfInstanceToBeRemoved returns a boolean if a field has been set.
func (o *ScaleNsData) HasVnfInstanceToBeRemoved() bool {
	if o != nil && o.VnfInstanceToBeRemoved != nil {
		return true
	}

	return false
}

// SetVnfInstanceToBeRemoved gets a reference to the given []string and assigns it to the VnfInstanceToBeRemoved field.
func (o *ScaleNsData) SetVnfInstanceToBeRemoved(v []string) {
	o.VnfInstanceToBeRemoved = v
}

// GetScaleNsByStepsData returns the ScaleNsByStepsData field value if set, zero value otherwise.
func (o *ScaleNsData) GetScaleNsByStepsData() ScaleNsByStepsData {
	if o == nil || o.ScaleNsByStepsData == nil {
		var ret ScaleNsByStepsData
		return ret
	}
	return *o.ScaleNsByStepsData
}

// GetScaleNsByStepsDataOk returns a tuple with the ScaleNsByStepsData field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ScaleNsData) GetScaleNsByStepsDataOk() (*ScaleNsByStepsData, bool) {
	if o == nil || o.ScaleNsByStepsData == nil {
		return nil, false
	}
	return o.ScaleNsByStepsData, true
}

// HasScaleNsByStepsData returns a boolean if a field has been set.
func (o *ScaleNsData) HasScaleNsByStepsData() bool {
	if o != nil && o.ScaleNsByStepsData != nil {
		return true
	}

	return false
}

// SetScaleNsByStepsData gets a reference to the given ScaleNsByStepsData and assigns it to the ScaleNsByStepsData field.
func (o *ScaleNsData) SetScaleNsByStepsData(v ScaleNsByStepsData) {
	o.ScaleNsByStepsData = &v
}

// GetScaleNsToLevelData returns the ScaleNsToLevelData field value if set, zero value otherwise.
func (o *ScaleNsData) GetScaleNsToLevelData() ScaleNsToLevelData {
	if o == nil || o.ScaleNsToLevelData == nil {
		var ret ScaleNsToLevelData
		return ret
	}
	return *o.ScaleNsToLevelData
}

// GetScaleNsToLevelDataOk returns a tuple with the ScaleNsToLevelData field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ScaleNsData) GetScaleNsToLevelDataOk() (*ScaleNsToLevelData, bool) {
	if o == nil || o.ScaleNsToLevelData == nil {
		return nil, false
	}
	return o.ScaleNsToLevelData, true
}

// HasScaleNsToLevelData returns a boolean if a field has been set.
func (o *ScaleNsData) HasScaleNsToLevelData() bool {
	if o != nil && o.ScaleNsToLevelData != nil {
		return true
	}

	return false
}

// SetScaleNsToLevelData gets a reference to the given ScaleNsToLevelData and assigns it to the ScaleNsToLevelData field.
func (o *ScaleNsData) SetScaleNsToLevelData(v ScaleNsToLevelData) {
	o.ScaleNsToLevelData = &v
}

// GetAdditionalParamsForNs returns the AdditionalParamsForNs field value if set, zero value otherwise.
func (o *ScaleNsData) GetAdditionalParamsForNs() ParamsForVnf {
	if o == nil || o.AdditionalParamsForNs == nil {
		var ret ParamsForVnf
		return ret
	}
	return *o.AdditionalParamsForNs
}

// GetAdditionalParamsForNsOk returns a tuple with the AdditionalParamsForNs field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ScaleNsData) GetAdditionalParamsForNsOk() (*ParamsForVnf, bool) {
	if o == nil || o.AdditionalParamsForNs == nil {
		return nil, false
	}
	return o.AdditionalParamsForNs, true
}

// HasAdditionalParamsForNs returns a boolean if a field has been set.
func (o *ScaleNsData) HasAdditionalParamsForNs() bool {
	if o != nil && o.AdditionalParamsForNs != nil {
		return true
	}

	return false
}

// SetAdditionalParamsForNs gets a reference to the given ParamsForVnf and assigns it to the AdditionalParamsForNs field.
func (o *ScaleNsData) SetAdditionalParamsForNs(v ParamsForVnf) {
	o.AdditionalParamsForNs = &v
}

// GetAdditionalParamsForVnf returns the AdditionalParamsForVnf field value if set, zero value otherwise.
func (o *ScaleNsData) GetAdditionalParamsForVnf() []ParamsForVnf {
	if o == nil || o.AdditionalParamsForVnf == nil {
		var ret []ParamsForVnf
		return ret
	}
	return o.AdditionalParamsForVnf
}

// GetAdditionalParamsForVnfOk returns a tuple with the AdditionalParamsForVnf field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ScaleNsData) GetAdditionalParamsForVnfOk() ([]ParamsForVnf, bool) {
	if o == nil || o.AdditionalParamsForVnf == nil {
		return nil, false
	}
	return o.AdditionalParamsForVnf, true
}

// HasAdditionalParamsForVnf returns a boolean if a field has been set.
func (o *ScaleNsData) HasAdditionalParamsForVnf() bool {
	if o != nil && o.AdditionalParamsForVnf != nil {
		return true
	}

	return false
}

// SetAdditionalParamsForVnf gets a reference to the given []ParamsForVnf and assigns it to the AdditionalParamsForVnf field.
func (o *ScaleNsData) SetAdditionalParamsForVnf(v []ParamsForVnf) {
	o.AdditionalParamsForVnf = v
}

// GetLocationConstraints returns the LocationConstraints field value if set, zero value otherwise.
func (o *ScaleNsData) GetLocationConstraints() []VnfLocationConstraint {
	if o == nil || o.LocationConstraints == nil {
		var ret []VnfLocationConstraint
		return ret
	}
	return o.LocationConstraints
}

// GetLocationConstraintsOk returns a tuple with the LocationConstraints field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ScaleNsData) GetLocationConstraintsOk() ([]VnfLocationConstraint, bool) {
	if o == nil || o.LocationConstraints == nil {
		return nil, false
	}
	return o.LocationConstraints, true
}

// HasLocationConstraints returns a boolean if a field has been set.
func (o *ScaleNsData) HasLocationConstraints() bool {
	if o != nil && o.LocationConstraints != nil {
		return true
	}

	return false
}

// SetLocationConstraints gets a reference to the given []VnfLocationConstraint and assigns it to the LocationConstraints field.
func (o *ScaleNsData) SetLocationConstraints(v []VnfLocationConstraint) {
	o.LocationConstraints = v
}

// GetNestedNslocationConstraints returns the NestedNslocationConstraints field value if set, zero value otherwise.
func (o *ScaleNsData) GetNestedNslocationConstraints() []NestedNsLocationConstraint {
	if o == nil || o.NestedNslocationConstraints == nil {
		var ret []NestedNsLocationConstraint
		return ret
	}
	return o.NestedNslocationConstraints
}

// GetNestedNslocationConstraintsOk returns a tuple with the NestedNslocationConstraints field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ScaleNsData) GetNestedNslocationConstraintsOk() ([]NestedNsLocationConstraint, bool) {
	if o == nil || o.NestedNslocationConstraints == nil {
		return nil, false
	}
	return o.NestedNslocationConstraints, true
}

// HasNestedNslocationConstraints returns a boolean if a field has been set.
func (o *ScaleNsData) HasNestedNslocationConstraints() bool {
	if o != nil && o.NestedNslocationConstraints != nil {
		return true
	}

	return false
}

// SetNestedNslocationConstraints gets a reference to the given []NestedNsLocationConstraint and assigns it to the NestedNslocationConstraints field.
func (o *ScaleNsData) SetNestedNslocationConstraints(v []NestedNsLocationConstraint) {
	o.NestedNslocationConstraints = v
}

func (o ScaleNsData) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.VnfInstanceToBeAdded != nil {
		toSerialize["vnfInstanceToBeAdded"] = o.VnfInstanceToBeAdded
	}
	if o.VnfInstanceToBeRemoved != nil {
		toSerialize["vnfInstanceToBeRemoved"] = o.VnfInstanceToBeRemoved
	}
	if o.ScaleNsByStepsData != nil {
		toSerialize["scaleNsByStepsData"] = o.ScaleNsByStepsData
	}
	if o.ScaleNsToLevelData != nil {
		toSerialize["scaleNsToLevelData"] = o.ScaleNsToLevelData
	}
	if o.AdditionalParamsForNs != nil {
		toSerialize["additionalParamsForNs"] = o.AdditionalParamsForNs
	}
	if o.AdditionalParamsForVnf != nil {
		toSerialize["additionalParamsForVnf"] = o.AdditionalParamsForVnf
	}
	if o.LocationConstraints != nil {
		toSerialize["locationConstraints"] = o.LocationConstraints
	}
	if o.NestedNslocationConstraints != nil {
		toSerialize["nestedNslocationConstraints"] = o.NestedNslocationConstraints
	}
	return json.Marshal(toSerialize)
}

type NullableScaleNsData struct {
	value *ScaleNsData
	isSet bool
}

func (v NullableScaleNsData) Get() *ScaleNsData {
	return v.value
}

func (v *NullableScaleNsData) Set(val *ScaleNsData) {
	v.value = val
	v.isSet = true
}

func (v NullableScaleNsData) IsSet() bool {
	return v.isSet
}

func (v *NullableScaleNsData) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableScaleNsData(val *ScaleNsData) *NullableScaleNsData {
	return &NullableScaleNsData{value: val, isSet: true}
}

func (v NullableScaleNsData) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableScaleNsData) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


