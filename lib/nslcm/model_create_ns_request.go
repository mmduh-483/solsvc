/*
SOL005 - NS Lifecycle Management Interface

SOL005 - NS Lifecycle Management Interface  IMPORTANT: Please note that this file might be not aligned to the current version of the ETSI Group Specification it refers to. In case of discrepancies the published ETSI Group Specification takes precedence.  Please report bugs to https://forge.etsi.org/rep/nfv/SOL005/issues 

API version: 2.1.0-impl:etsi.org:ETSI_NFV_OpenAPI:2
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// CreateNsRequest struct for CreateNsRequest
type CreateNsRequest struct {
	// An identifier with the intention of being globally unique. 
	NsdId string `json:"nsdId"`
	// Human-readable name of the NS instance to be created. 
	NsName string `json:"nsName"`
	// Human-readable description of the NS instance to be created. 
	NsDescription string `json:"nsDescription"`
}

// NewCreateNsRequest instantiates a new CreateNsRequest object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewCreateNsRequest(nsdId string, nsName string, nsDescription string) *CreateNsRequest {
	this := CreateNsRequest{}
	this.NsdId = nsdId
	this.NsName = nsName
	this.NsDescription = nsDescription
	return &this
}

// NewCreateNsRequestWithDefaults instantiates a new CreateNsRequest object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewCreateNsRequestWithDefaults() *CreateNsRequest {
	this := CreateNsRequest{}
	return &this
}

// GetNsdId returns the NsdId field value
func (o *CreateNsRequest) GetNsdId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.NsdId
}

// GetNsdIdOk returns a tuple with the NsdId field value
// and a boolean to check if the value has been set.
func (o *CreateNsRequest) GetNsdIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.NsdId, true
}

// SetNsdId sets field value
func (o *CreateNsRequest) SetNsdId(v string) {
	o.NsdId = v
}

// GetNsName returns the NsName field value
func (o *CreateNsRequest) GetNsName() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.NsName
}

// GetNsNameOk returns a tuple with the NsName field value
// and a boolean to check if the value has been set.
func (o *CreateNsRequest) GetNsNameOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.NsName, true
}

// SetNsName sets field value
func (o *CreateNsRequest) SetNsName(v string) {
	o.NsName = v
}

// GetNsDescription returns the NsDescription field value
func (o *CreateNsRequest) GetNsDescription() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.NsDescription
}

// GetNsDescriptionOk returns a tuple with the NsDescription field value
// and a boolean to check if the value has been set.
func (o *CreateNsRequest) GetNsDescriptionOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.NsDescription, true
}

// SetNsDescription sets field value
func (o *CreateNsRequest) SetNsDescription(v string) {
	o.NsDescription = v
}

func (o CreateNsRequest) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["nsdId"] = o.NsdId
	}
	if true {
		toSerialize["nsName"] = o.NsName
	}
	if true {
		toSerialize["nsDescription"] = o.NsDescription
	}
	return json.Marshal(toSerialize)
}

type NullableCreateNsRequest struct {
	value *CreateNsRequest
	isSet bool
}

func (v NullableCreateNsRequest) Get() *CreateNsRequest {
	return v.value
}

func (v *NullableCreateNsRequest) Set(val *CreateNsRequest) {
	v.value = val
	v.isSet = true
}

func (v NullableCreateNsRequest) IsSet() bool {
	return v.isSet
}

func (v *NullableCreateNsRequest) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableCreateNsRequest(val *CreateNsRequest) *NullableCreateNsRequest {
	return &NullableCreateNsRequest{value: val, isSet: true}
}

func (v NullableCreateNsRequest) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableCreateNsRequest) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


