/*
SOL005 - NS Lifecycle Management Interface

SOL005 - NS Lifecycle Management Interface  IMPORTANT: Please note that this file might be not aligned to the current version of the ETSI Group Specification it refers to. In case of discrepancies the published ETSI Group Specification takes precedence.  Please report bugs to https://forge.etsi.org/rep/nfv/SOL005/issues 

API version: 2.1.0-impl:etsi.org:ETSI_NFV_OpenAPI:2
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// AssocNewNsdVersionData This type specifies a new NSD version that is associated to the NS instance. After issuing the Update NS operation with updateType = \"AssocNewNsdVersion\", the NFVO shall use the referred NSD as a basis for the given NS instance. Different versions of the same NSD have same nsdInvariantId, but different nsdId attributes, therefore if the nsdInvariantId of the NSD version that is to be associated to this NS instance is different from the one used before, the NFVO shall reject the request. Only new versions of the same NSD can be associated to an existing NS instance. This data type shall comply with the provisions defined in Table 6.5.3.34-1. 
type AssocNewNsdVersionData struct {
	// An identifier with the intention of being globally unique. 
	NewNsdId string `json:"newNsdId"`
	// Specify whether the NS instance shall be automatically synchronized to the new NSD by the NFVO (in case of true value) or the NFVO shall not do any action (in case of a false value) and wait for further guidance from OSS/BSS (i.e. waiting for OSS/BSS to issue NS lifecycle management operation to explicitly add/remove VNFs and modify information of VNF instances according to the new NSD). The synchronization to the new NSD means e.g. instantiating/adding those VNFs whose VNFD is referenced by the new NSD version but not referenced by the old one, terminating/removing those VNFs whose VNFD is referenced by the old NSD version but not referenced by the new NSD version, modifying information of VNF instances to the new applicable VNFD provided in the new NSD version. A cardinality of 0 indicates that synchronization shall not be done. 
	Sync *bool `json:"sync,omitempty"`
}

// NewAssocNewNsdVersionData instantiates a new AssocNewNsdVersionData object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewAssocNewNsdVersionData(newNsdId string) *AssocNewNsdVersionData {
	this := AssocNewNsdVersionData{}
	this.NewNsdId = newNsdId
	return &this
}

// NewAssocNewNsdVersionDataWithDefaults instantiates a new AssocNewNsdVersionData object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewAssocNewNsdVersionDataWithDefaults() *AssocNewNsdVersionData {
	this := AssocNewNsdVersionData{}
	return &this
}

// GetNewNsdId returns the NewNsdId field value
func (o *AssocNewNsdVersionData) GetNewNsdId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.NewNsdId
}

// GetNewNsdIdOk returns a tuple with the NewNsdId field value
// and a boolean to check if the value has been set.
func (o *AssocNewNsdVersionData) GetNewNsdIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.NewNsdId, true
}

// SetNewNsdId sets field value
func (o *AssocNewNsdVersionData) SetNewNsdId(v string) {
	o.NewNsdId = v
}

// GetSync returns the Sync field value if set, zero value otherwise.
func (o *AssocNewNsdVersionData) GetSync() bool {
	if o == nil || o.Sync == nil {
		var ret bool
		return ret
	}
	return *o.Sync
}

// GetSyncOk returns a tuple with the Sync field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *AssocNewNsdVersionData) GetSyncOk() (*bool, bool) {
	if o == nil || o.Sync == nil {
		return nil, false
	}
	return o.Sync, true
}

// HasSync returns a boolean if a field has been set.
func (o *AssocNewNsdVersionData) HasSync() bool {
	if o != nil && o.Sync != nil {
		return true
	}

	return false
}

// SetSync gets a reference to the given bool and assigns it to the Sync field.
func (o *AssocNewNsdVersionData) SetSync(v bool) {
	o.Sync = &v
}

func (o AssocNewNsdVersionData) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["newNsdId"] = o.NewNsdId
	}
	if o.Sync != nil {
		toSerialize["sync"] = o.Sync
	}
	return json.Marshal(toSerialize)
}

type NullableAssocNewNsdVersionData struct {
	value *AssocNewNsdVersionData
	isSet bool
}

func (v NullableAssocNewNsdVersionData) Get() *AssocNewNsdVersionData {
	return v.value
}

func (v *NullableAssocNewNsdVersionData) Set(val *AssocNewNsdVersionData) {
	v.value = val
	v.isSet = true
}

func (v NullableAssocNewNsdVersionData) IsSet() bool {
	return v.isSet
}

func (v *NullableAssocNewNsdVersionData) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableAssocNewNsdVersionData(val *AssocNewNsdVersionData) *NullableAssocNewNsdVersionData {
	return &NullableAssocNewNsdVersionData{value: val, isSet: true}
}

func (v NullableAssocNewNsdVersionData) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableAssocNewNsdVersionData) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


