/*
SOL005 - NS Lifecycle Management Interface

SOL005 - NS Lifecycle Management Interface  IMPORTANT: Please note that this file might be not aligned to the current version of the ETSI Group Specification it refers to. In case of discrepancies the published ETSI Group Specification takes precedence.  Please report bugs to https://forge.etsi.org/rep/nfv/SOL005/issues 

API version: 2.1.0-impl:etsi.org:ETSI_NFV_OpenAPI:2
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// NsLcmOpOccResourceChanges This attribute contains information about the cumulative changes to virtualised resources that were performed so far by the LCM operation since its start, if applicable 
type NsLcmOpOccResourceChanges struct {
	// Information about the VNF instances that were affected during the lifecycle operation, if this notification represents the result of a lifecycle operation. See note 1. 
	AffectedVnfs []AffectedVnf `json:"affectedVnfs,omitempty"`
	// Information about the PNF instances that were affected during the lifecycle operation, if this notification represents the result of a lifecycle operation. See note 1. 
	AffectedPnfs []AffectedPnf `json:"affectedPnfs,omitempty"`
	// Information about the VL instances that were affected during the lifecycle operation, if this notification represents the result of a lifecycle operation. See note 1. 
	AffectedVls []AffectedVirtualLink `json:"affectedVls,omitempty"`
	// Information about the VNFFG instances that were affected during the lifecycle operation, if this notification represents the result of a lifecycle operation. See note 1. 
	AffectedVnffgs []AffectedVnffg `json:"affectedVnffgs,omitempty"`
	// Information about the nested NS instances that were affected during the lifecycle operation, if this notification represents the result of a lifecycle operation. See note 1. 
	AffectedNss []AffectedNs `json:"affectedNss,omitempty"`
	// Information about the nested NS instances that were affected during the lifecycle operation, if this notification represents the result of a lifecycle operation. See note 1. 
	AffectedSaps []AffectedSap `json:"affectedSaps,omitempty"`
}

// NewNsLcmOpOccResourceChanges instantiates a new NsLcmOpOccResourceChanges object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewNsLcmOpOccResourceChanges() *NsLcmOpOccResourceChanges {
	this := NsLcmOpOccResourceChanges{}
	return &this
}

// NewNsLcmOpOccResourceChangesWithDefaults instantiates a new NsLcmOpOccResourceChanges object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewNsLcmOpOccResourceChangesWithDefaults() *NsLcmOpOccResourceChanges {
	this := NsLcmOpOccResourceChanges{}
	return &this
}

// GetAffectedVnfs returns the AffectedVnfs field value if set, zero value otherwise.
func (o *NsLcmOpOccResourceChanges) GetAffectedVnfs() []AffectedVnf {
	if o == nil || o.AffectedVnfs == nil {
		var ret []AffectedVnf
		return ret
	}
	return o.AffectedVnfs
}

// GetAffectedVnfsOk returns a tuple with the AffectedVnfs field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NsLcmOpOccResourceChanges) GetAffectedVnfsOk() ([]AffectedVnf, bool) {
	if o == nil || o.AffectedVnfs == nil {
		return nil, false
	}
	return o.AffectedVnfs, true
}

// HasAffectedVnfs returns a boolean if a field has been set.
func (o *NsLcmOpOccResourceChanges) HasAffectedVnfs() bool {
	if o != nil && o.AffectedVnfs != nil {
		return true
	}

	return false
}

// SetAffectedVnfs gets a reference to the given []AffectedVnf and assigns it to the AffectedVnfs field.
func (o *NsLcmOpOccResourceChanges) SetAffectedVnfs(v []AffectedVnf) {
	o.AffectedVnfs = v
}

// GetAffectedPnfs returns the AffectedPnfs field value if set, zero value otherwise.
func (o *NsLcmOpOccResourceChanges) GetAffectedPnfs() []AffectedPnf {
	if o == nil || o.AffectedPnfs == nil {
		var ret []AffectedPnf
		return ret
	}
	return o.AffectedPnfs
}

// GetAffectedPnfsOk returns a tuple with the AffectedPnfs field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NsLcmOpOccResourceChanges) GetAffectedPnfsOk() ([]AffectedPnf, bool) {
	if o == nil || o.AffectedPnfs == nil {
		return nil, false
	}
	return o.AffectedPnfs, true
}

// HasAffectedPnfs returns a boolean if a field has been set.
func (o *NsLcmOpOccResourceChanges) HasAffectedPnfs() bool {
	if o != nil && o.AffectedPnfs != nil {
		return true
	}

	return false
}

// SetAffectedPnfs gets a reference to the given []AffectedPnf and assigns it to the AffectedPnfs field.
func (o *NsLcmOpOccResourceChanges) SetAffectedPnfs(v []AffectedPnf) {
	o.AffectedPnfs = v
}

// GetAffectedVls returns the AffectedVls field value if set, zero value otherwise.
func (o *NsLcmOpOccResourceChanges) GetAffectedVls() []AffectedVirtualLink {
	if o == nil || o.AffectedVls == nil {
		var ret []AffectedVirtualLink
		return ret
	}
	return o.AffectedVls
}

// GetAffectedVlsOk returns a tuple with the AffectedVls field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NsLcmOpOccResourceChanges) GetAffectedVlsOk() ([]AffectedVirtualLink, bool) {
	if o == nil || o.AffectedVls == nil {
		return nil, false
	}
	return o.AffectedVls, true
}

// HasAffectedVls returns a boolean if a field has been set.
func (o *NsLcmOpOccResourceChanges) HasAffectedVls() bool {
	if o != nil && o.AffectedVls != nil {
		return true
	}

	return false
}

// SetAffectedVls gets a reference to the given []AffectedVirtualLink and assigns it to the AffectedVls field.
func (o *NsLcmOpOccResourceChanges) SetAffectedVls(v []AffectedVirtualLink) {
	o.AffectedVls = v
}

// GetAffectedVnffgs returns the AffectedVnffgs field value if set, zero value otherwise.
func (o *NsLcmOpOccResourceChanges) GetAffectedVnffgs() []AffectedVnffg {
	if o == nil || o.AffectedVnffgs == nil {
		var ret []AffectedVnffg
		return ret
	}
	return o.AffectedVnffgs
}

// GetAffectedVnffgsOk returns a tuple with the AffectedVnffgs field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NsLcmOpOccResourceChanges) GetAffectedVnffgsOk() ([]AffectedVnffg, bool) {
	if o == nil || o.AffectedVnffgs == nil {
		return nil, false
	}
	return o.AffectedVnffgs, true
}

// HasAffectedVnffgs returns a boolean if a field has been set.
func (o *NsLcmOpOccResourceChanges) HasAffectedVnffgs() bool {
	if o != nil && o.AffectedVnffgs != nil {
		return true
	}

	return false
}

// SetAffectedVnffgs gets a reference to the given []AffectedVnffg and assigns it to the AffectedVnffgs field.
func (o *NsLcmOpOccResourceChanges) SetAffectedVnffgs(v []AffectedVnffg) {
	o.AffectedVnffgs = v
}

// GetAffectedNss returns the AffectedNss field value if set, zero value otherwise.
func (o *NsLcmOpOccResourceChanges) GetAffectedNss() []AffectedNs {
	if o == nil || o.AffectedNss == nil {
		var ret []AffectedNs
		return ret
	}
	return o.AffectedNss
}

// GetAffectedNssOk returns a tuple with the AffectedNss field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NsLcmOpOccResourceChanges) GetAffectedNssOk() ([]AffectedNs, bool) {
	if o == nil || o.AffectedNss == nil {
		return nil, false
	}
	return o.AffectedNss, true
}

// HasAffectedNss returns a boolean if a field has been set.
func (o *NsLcmOpOccResourceChanges) HasAffectedNss() bool {
	if o != nil && o.AffectedNss != nil {
		return true
	}

	return false
}

// SetAffectedNss gets a reference to the given []AffectedNs and assigns it to the AffectedNss field.
func (o *NsLcmOpOccResourceChanges) SetAffectedNss(v []AffectedNs) {
	o.AffectedNss = v
}

// GetAffectedSaps returns the AffectedSaps field value if set, zero value otherwise.
func (o *NsLcmOpOccResourceChanges) GetAffectedSaps() []AffectedSap {
	if o == nil || o.AffectedSaps == nil {
		var ret []AffectedSap
		return ret
	}
	return o.AffectedSaps
}

// GetAffectedSapsOk returns a tuple with the AffectedSaps field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *NsLcmOpOccResourceChanges) GetAffectedSapsOk() ([]AffectedSap, bool) {
	if o == nil || o.AffectedSaps == nil {
		return nil, false
	}
	return o.AffectedSaps, true
}

// HasAffectedSaps returns a boolean if a field has been set.
func (o *NsLcmOpOccResourceChanges) HasAffectedSaps() bool {
	if o != nil && o.AffectedSaps != nil {
		return true
	}

	return false
}

// SetAffectedSaps gets a reference to the given []AffectedSap and assigns it to the AffectedSaps field.
func (o *NsLcmOpOccResourceChanges) SetAffectedSaps(v []AffectedSap) {
	o.AffectedSaps = v
}

func (o NsLcmOpOccResourceChanges) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.AffectedVnfs != nil {
		toSerialize["affectedVnfs"] = o.AffectedVnfs
	}
	if o.AffectedPnfs != nil {
		toSerialize["affectedPnfs"] = o.AffectedPnfs
	}
	if o.AffectedVls != nil {
		toSerialize["affectedVls"] = o.AffectedVls
	}
	if o.AffectedVnffgs != nil {
		toSerialize["affectedVnffgs"] = o.AffectedVnffgs
	}
	if o.AffectedNss != nil {
		toSerialize["affectedNss"] = o.AffectedNss
	}
	if o.AffectedSaps != nil {
		toSerialize["affectedSaps"] = o.AffectedSaps
	}
	return json.Marshal(toSerialize)
}

type NullableNsLcmOpOccResourceChanges struct {
	value *NsLcmOpOccResourceChanges
	isSet bool
}

func (v NullableNsLcmOpOccResourceChanges) Get() *NsLcmOpOccResourceChanges {
	return v.value
}

func (v *NullableNsLcmOpOccResourceChanges) Set(val *NsLcmOpOccResourceChanges) {
	v.value = val
	v.isSet = true
}

func (v NullableNsLcmOpOccResourceChanges) IsSet() bool {
	return v.isSet
}

func (v *NullableNsLcmOpOccResourceChanges) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableNsLcmOpOccResourceChanges(val *NsLcmOpOccResourceChanges) *NullableNsLcmOpOccResourceChanges {
	return &NullableNsLcmOpOccResourceChanges{value: val, isSet: true}
}

func (v NullableNsLcmOpOccResourceChanges) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableNsLcmOpOccResourceChanges) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


