/*
SOL005 - NS Lifecycle Management Interface

SOL005 - NS Lifecycle Management Interface  IMPORTANT: Please note that this file might be not aligned to the current version of the ETSI Group Specification it refers to. In case of discrepancies the published ETSI Group Specification takes precedence.  Please report bugs to https://forge.etsi.org/rep/nfv/SOL005/issues 

API version: 2.1.0-impl:etsi.org:ETSI_NFV_OpenAPI:2
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// ChangeVnfFlavourData The type represents the information that is requested to be changed deployment flavor for an existing VNF instance. NOTE 1: The indication of externally-managed internal VLs is needed in case networks have been pre-configured for use with certain VNFs, for instance to ensure that these networks have certain properties such as security or acceleration features, or to address particular network topologies. The present document assumes that externally-managed internal VLs are managed by the NFVO and created towards the VIM. NOTE 2: It is possible to have several ExtManagedVirtualLinkData for the same VNF internal VL in case of a multi-site VNF spanning several VIMs. The set of ExtManagedVirtualLinkData corresponding to the same VNF internal VL shall indicate so by referencing to the same VnfVirtualLinkDesc and externally-managed multi-site VL instance (refer to clause 6.5.3.27). 
type ChangeVnfFlavourData struct {
	// An identifier with the intention of being globally unique. 
	VnfInstanceId string `json:"vnfInstanceId"`
	// Identifier of the VNF Virtual Link Descriptor (VLD) in the VNFD. 
	NewFlavourId string `json:"newFlavourId"`
	// Identifier of the VNF Virtual Link Descriptor (VLD) in the VNFD. 
	InstantiationLevelId *string `json:"instantiationLevelId,omitempty"`
	// Information about external VLs to connect the VNF to. 
	ExtVirtualLinks []ExtVirtualLinkData `json:"extVirtualLinks,omitempty"`
	// information about internal VLs that are managed by NFVO. See note 1 and note 2. 
	ExtManagedVirtualLinks []ExtManagedVirtualLinkData `json:"extManagedVirtualLinks,omitempty"`
	// This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159. 
	AdditionalParams map[string]interface{} `json:"additionalParams,omitempty"`
	// This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159. 
	Extensions map[string]interface{} `json:"extensions,omitempty"`
	// This type represents a list of key-value pairs. The order of the pairs in the list is not significant. In JSON, a set of key- value pairs is represented as an object. It shall comply with the provisions defined in clause 4 of IETF RFC 7159. 
	VnfConfigurableProperties map[string]interface{} `json:"vnfConfigurableProperties,omitempty"`
}

// NewChangeVnfFlavourData instantiates a new ChangeVnfFlavourData object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewChangeVnfFlavourData(vnfInstanceId string, newFlavourId string) *ChangeVnfFlavourData {
	this := ChangeVnfFlavourData{}
	this.VnfInstanceId = vnfInstanceId
	this.NewFlavourId = newFlavourId
	return &this
}

// NewChangeVnfFlavourDataWithDefaults instantiates a new ChangeVnfFlavourData object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewChangeVnfFlavourDataWithDefaults() *ChangeVnfFlavourData {
	this := ChangeVnfFlavourData{}
	return &this
}

// GetVnfInstanceId returns the VnfInstanceId field value
func (o *ChangeVnfFlavourData) GetVnfInstanceId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.VnfInstanceId
}

// GetVnfInstanceIdOk returns a tuple with the VnfInstanceId field value
// and a boolean to check if the value has been set.
func (o *ChangeVnfFlavourData) GetVnfInstanceIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.VnfInstanceId, true
}

// SetVnfInstanceId sets field value
func (o *ChangeVnfFlavourData) SetVnfInstanceId(v string) {
	o.VnfInstanceId = v
}

// GetNewFlavourId returns the NewFlavourId field value
func (o *ChangeVnfFlavourData) GetNewFlavourId() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.NewFlavourId
}

// GetNewFlavourIdOk returns a tuple with the NewFlavourId field value
// and a boolean to check if the value has been set.
func (o *ChangeVnfFlavourData) GetNewFlavourIdOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.NewFlavourId, true
}

// SetNewFlavourId sets field value
func (o *ChangeVnfFlavourData) SetNewFlavourId(v string) {
	o.NewFlavourId = v
}

// GetInstantiationLevelId returns the InstantiationLevelId field value if set, zero value otherwise.
func (o *ChangeVnfFlavourData) GetInstantiationLevelId() string {
	if o == nil || o.InstantiationLevelId == nil {
		var ret string
		return ret
	}
	return *o.InstantiationLevelId
}

// GetInstantiationLevelIdOk returns a tuple with the InstantiationLevelId field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ChangeVnfFlavourData) GetInstantiationLevelIdOk() (*string, bool) {
	if o == nil || o.InstantiationLevelId == nil {
		return nil, false
	}
	return o.InstantiationLevelId, true
}

// HasInstantiationLevelId returns a boolean if a field has been set.
func (o *ChangeVnfFlavourData) HasInstantiationLevelId() bool {
	if o != nil && o.InstantiationLevelId != nil {
		return true
	}

	return false
}

// SetInstantiationLevelId gets a reference to the given string and assigns it to the InstantiationLevelId field.
func (o *ChangeVnfFlavourData) SetInstantiationLevelId(v string) {
	o.InstantiationLevelId = &v
}

// GetExtVirtualLinks returns the ExtVirtualLinks field value if set, zero value otherwise.
func (o *ChangeVnfFlavourData) GetExtVirtualLinks() []ExtVirtualLinkData {
	if o == nil || o.ExtVirtualLinks == nil {
		var ret []ExtVirtualLinkData
		return ret
	}
	return o.ExtVirtualLinks
}

// GetExtVirtualLinksOk returns a tuple with the ExtVirtualLinks field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ChangeVnfFlavourData) GetExtVirtualLinksOk() ([]ExtVirtualLinkData, bool) {
	if o == nil || o.ExtVirtualLinks == nil {
		return nil, false
	}
	return o.ExtVirtualLinks, true
}

// HasExtVirtualLinks returns a boolean if a field has been set.
func (o *ChangeVnfFlavourData) HasExtVirtualLinks() bool {
	if o != nil && o.ExtVirtualLinks != nil {
		return true
	}

	return false
}

// SetExtVirtualLinks gets a reference to the given []ExtVirtualLinkData and assigns it to the ExtVirtualLinks field.
func (o *ChangeVnfFlavourData) SetExtVirtualLinks(v []ExtVirtualLinkData) {
	o.ExtVirtualLinks = v
}

// GetExtManagedVirtualLinks returns the ExtManagedVirtualLinks field value if set, zero value otherwise.
func (o *ChangeVnfFlavourData) GetExtManagedVirtualLinks() []ExtManagedVirtualLinkData {
	if o == nil || o.ExtManagedVirtualLinks == nil {
		var ret []ExtManagedVirtualLinkData
		return ret
	}
	return o.ExtManagedVirtualLinks
}

// GetExtManagedVirtualLinksOk returns a tuple with the ExtManagedVirtualLinks field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ChangeVnfFlavourData) GetExtManagedVirtualLinksOk() ([]ExtManagedVirtualLinkData, bool) {
	if o == nil || o.ExtManagedVirtualLinks == nil {
		return nil, false
	}
	return o.ExtManagedVirtualLinks, true
}

// HasExtManagedVirtualLinks returns a boolean if a field has been set.
func (o *ChangeVnfFlavourData) HasExtManagedVirtualLinks() bool {
	if o != nil && o.ExtManagedVirtualLinks != nil {
		return true
	}

	return false
}

// SetExtManagedVirtualLinks gets a reference to the given []ExtManagedVirtualLinkData and assigns it to the ExtManagedVirtualLinks field.
func (o *ChangeVnfFlavourData) SetExtManagedVirtualLinks(v []ExtManagedVirtualLinkData) {
	o.ExtManagedVirtualLinks = v
}

// GetAdditionalParams returns the AdditionalParams field value if set, zero value otherwise.
func (o *ChangeVnfFlavourData) GetAdditionalParams() map[string]interface{} {
	if o == nil || o.AdditionalParams == nil {
		var ret map[string]interface{}
		return ret
	}
	return o.AdditionalParams
}

// GetAdditionalParamsOk returns a tuple with the AdditionalParams field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ChangeVnfFlavourData) GetAdditionalParamsOk() (map[string]interface{}, bool) {
	if o == nil || o.AdditionalParams == nil {
		return nil, false
	}
	return o.AdditionalParams, true
}

// HasAdditionalParams returns a boolean if a field has been set.
func (o *ChangeVnfFlavourData) HasAdditionalParams() bool {
	if o != nil && o.AdditionalParams != nil {
		return true
	}

	return false
}

// SetAdditionalParams gets a reference to the given map[string]interface{} and assigns it to the AdditionalParams field.
func (o *ChangeVnfFlavourData) SetAdditionalParams(v map[string]interface{}) {
	o.AdditionalParams = v
}

// GetExtensions returns the Extensions field value if set, zero value otherwise.
func (o *ChangeVnfFlavourData) GetExtensions() map[string]interface{} {
	if o == nil || o.Extensions == nil {
		var ret map[string]interface{}
		return ret
	}
	return o.Extensions
}

// GetExtensionsOk returns a tuple with the Extensions field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ChangeVnfFlavourData) GetExtensionsOk() (map[string]interface{}, bool) {
	if o == nil || o.Extensions == nil {
		return nil, false
	}
	return o.Extensions, true
}

// HasExtensions returns a boolean if a field has been set.
func (o *ChangeVnfFlavourData) HasExtensions() bool {
	if o != nil && o.Extensions != nil {
		return true
	}

	return false
}

// SetExtensions gets a reference to the given map[string]interface{} and assigns it to the Extensions field.
func (o *ChangeVnfFlavourData) SetExtensions(v map[string]interface{}) {
	o.Extensions = v
}

// GetVnfConfigurableProperties returns the VnfConfigurableProperties field value if set, zero value otherwise.
func (o *ChangeVnfFlavourData) GetVnfConfigurableProperties() map[string]interface{} {
	if o == nil || o.VnfConfigurableProperties == nil {
		var ret map[string]interface{}
		return ret
	}
	return o.VnfConfigurableProperties
}

// GetVnfConfigurablePropertiesOk returns a tuple with the VnfConfigurableProperties field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ChangeVnfFlavourData) GetVnfConfigurablePropertiesOk() (map[string]interface{}, bool) {
	if o == nil || o.VnfConfigurableProperties == nil {
		return nil, false
	}
	return o.VnfConfigurableProperties, true
}

// HasVnfConfigurableProperties returns a boolean if a field has been set.
func (o *ChangeVnfFlavourData) HasVnfConfigurableProperties() bool {
	if o != nil && o.VnfConfigurableProperties != nil {
		return true
	}

	return false
}

// SetVnfConfigurableProperties gets a reference to the given map[string]interface{} and assigns it to the VnfConfigurableProperties field.
func (o *ChangeVnfFlavourData) SetVnfConfigurableProperties(v map[string]interface{}) {
	o.VnfConfigurableProperties = v
}

func (o ChangeVnfFlavourData) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["vnfInstanceId"] = o.VnfInstanceId
	}
	if true {
		toSerialize["newFlavourId"] = o.NewFlavourId
	}
	if o.InstantiationLevelId != nil {
		toSerialize["instantiationLevelId"] = o.InstantiationLevelId
	}
	if o.ExtVirtualLinks != nil {
		toSerialize["extVirtualLinks"] = o.ExtVirtualLinks
	}
	if o.ExtManagedVirtualLinks != nil {
		toSerialize["extManagedVirtualLinks"] = o.ExtManagedVirtualLinks
	}
	if o.AdditionalParams != nil {
		toSerialize["additionalParams"] = o.AdditionalParams
	}
	if o.Extensions != nil {
		toSerialize["extensions"] = o.Extensions
	}
	if o.VnfConfigurableProperties != nil {
		toSerialize["vnfConfigurableProperties"] = o.VnfConfigurableProperties
	}
	return json.Marshal(toSerialize)
}

type NullableChangeVnfFlavourData struct {
	value *ChangeVnfFlavourData
	isSet bool
}

func (v NullableChangeVnfFlavourData) Get() *ChangeVnfFlavourData {
	return v.value
}

func (v *NullableChangeVnfFlavourData) Set(val *ChangeVnfFlavourData) {
	v.value = val
	v.isSet = true
}

func (v NullableChangeVnfFlavourData) IsSet() bool {
	return v.isSet
}

func (v *NullableChangeVnfFlavourData) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableChangeVnfFlavourData(val *ChangeVnfFlavourData) *NullableChangeVnfFlavourData {
	return &NullableChangeVnfFlavourData{value: val, isSet: true}
}

func (v NullableChangeVnfFlavourData) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableChangeVnfFlavourData) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


