/*
SOL005 - NS Lifecycle Management Interface

SOL005 - NS Lifecycle Management Interface  IMPORTANT: Please note that this file might be not aligned to the current version of the ETSI Group Specification it refers to. In case of discrepancies the published ETSI Group Specification takes precedence.  Please report bugs to https://forge.etsi.org/rep/nfv/SOL005/issues 

API version: 2.1.0-impl:etsi.org:ETSI_NFV_OpenAPI:2
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// LccnSubscriptionRequest This type represents a subscription request related to notifications  about NS lifecycle changes. It shall comply with the provisions defined in Table 6.5.2.2-1.. 
type LccnSubscriptionRequest struct {
	Filter *LifecycleChangeNotificationsFilter `json:"filter,omitempty"`
	// String formatted according to IETF RFC 3986. 
	CallbackUri string `json:"callbackUri"`
	Authentication *SubscriptionAuthentication `json:"authentication,omitempty"`
	Verbosity *LcmOpOccNotificationVerbosityType `json:"verbosity,omitempty"`
}

// NewLccnSubscriptionRequest instantiates a new LccnSubscriptionRequest object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewLccnSubscriptionRequest(callbackUri string) *LccnSubscriptionRequest {
	this := LccnSubscriptionRequest{}
	this.CallbackUri = callbackUri
	return &this
}

// NewLccnSubscriptionRequestWithDefaults instantiates a new LccnSubscriptionRequest object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewLccnSubscriptionRequestWithDefaults() *LccnSubscriptionRequest {
	this := LccnSubscriptionRequest{}
	return &this
}

// GetFilter returns the Filter field value if set, zero value otherwise.
func (o *LccnSubscriptionRequest) GetFilter() LifecycleChangeNotificationsFilter {
	if o == nil || o.Filter == nil {
		var ret LifecycleChangeNotificationsFilter
		return ret
	}
	return *o.Filter
}

// GetFilterOk returns a tuple with the Filter field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *LccnSubscriptionRequest) GetFilterOk() (*LifecycleChangeNotificationsFilter, bool) {
	if o == nil || o.Filter == nil {
		return nil, false
	}
	return o.Filter, true
}

// HasFilter returns a boolean if a field has been set.
func (o *LccnSubscriptionRequest) HasFilter() bool {
	if o != nil && o.Filter != nil {
		return true
	}

	return false
}

// SetFilter gets a reference to the given LifecycleChangeNotificationsFilter and assigns it to the Filter field.
func (o *LccnSubscriptionRequest) SetFilter(v LifecycleChangeNotificationsFilter) {
	o.Filter = &v
}

// GetCallbackUri returns the CallbackUri field value
func (o *LccnSubscriptionRequest) GetCallbackUri() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.CallbackUri
}

// GetCallbackUriOk returns a tuple with the CallbackUri field value
// and a boolean to check if the value has been set.
func (o *LccnSubscriptionRequest) GetCallbackUriOk() (*string, bool) {
	if o == nil {
		return nil, false
	}
	return &o.CallbackUri, true
}

// SetCallbackUri sets field value
func (o *LccnSubscriptionRequest) SetCallbackUri(v string) {
	o.CallbackUri = v
}

// GetAuthentication returns the Authentication field value if set, zero value otherwise.
func (o *LccnSubscriptionRequest) GetAuthentication() SubscriptionAuthentication {
	if o == nil || o.Authentication == nil {
		var ret SubscriptionAuthentication
		return ret
	}
	return *o.Authentication
}

// GetAuthenticationOk returns a tuple with the Authentication field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *LccnSubscriptionRequest) GetAuthenticationOk() (*SubscriptionAuthentication, bool) {
	if o == nil || o.Authentication == nil {
		return nil, false
	}
	return o.Authentication, true
}

// HasAuthentication returns a boolean if a field has been set.
func (o *LccnSubscriptionRequest) HasAuthentication() bool {
	if o != nil && o.Authentication != nil {
		return true
	}

	return false
}

// SetAuthentication gets a reference to the given SubscriptionAuthentication and assigns it to the Authentication field.
func (o *LccnSubscriptionRequest) SetAuthentication(v SubscriptionAuthentication) {
	o.Authentication = &v
}

// GetVerbosity returns the Verbosity field value if set, zero value otherwise.
func (o *LccnSubscriptionRequest) GetVerbosity() LcmOpOccNotificationVerbosityType {
	if o == nil || o.Verbosity == nil {
		var ret LcmOpOccNotificationVerbosityType
		return ret
	}
	return *o.Verbosity
}

// GetVerbosityOk returns a tuple with the Verbosity field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *LccnSubscriptionRequest) GetVerbosityOk() (*LcmOpOccNotificationVerbosityType, bool) {
	if o == nil || o.Verbosity == nil {
		return nil, false
	}
	return o.Verbosity, true
}

// HasVerbosity returns a boolean if a field has been set.
func (o *LccnSubscriptionRequest) HasVerbosity() bool {
	if o != nil && o.Verbosity != nil {
		return true
	}

	return false
}

// SetVerbosity gets a reference to the given LcmOpOccNotificationVerbosityType and assigns it to the Verbosity field.
func (o *LccnSubscriptionRequest) SetVerbosity(v LcmOpOccNotificationVerbosityType) {
	o.Verbosity = &v
}

func (o LccnSubscriptionRequest) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Filter != nil {
		toSerialize["filter"] = o.Filter
	}
	if true {
		toSerialize["callbackUri"] = o.CallbackUri
	}
	if o.Authentication != nil {
		toSerialize["authentication"] = o.Authentication
	}
	if o.Verbosity != nil {
		toSerialize["verbosity"] = o.Verbosity
	}
	return json.Marshal(toSerialize)
}

type NullableLccnSubscriptionRequest struct {
	value *LccnSubscriptionRequest
	isSet bool
}

func (v NullableLccnSubscriptionRequest) Get() *LccnSubscriptionRequest {
	return v.value
}

func (v *NullableLccnSubscriptionRequest) Set(val *LccnSubscriptionRequest) {
	v.value = val
	v.isSet = true
}

func (v NullableLccnSubscriptionRequest) IsSet() bool {
	return v.isSet
}

func (v *NullableLccnSubscriptionRequest) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableLccnSubscriptionRequest(val *LccnSubscriptionRequest) *NullableLccnSubscriptionRequest {
	return &NullableLccnSubscriptionRequest{value: val, isSet: true}
}

func (v NullableLccnSubscriptionRequest) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableLccnSubscriptionRequest) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


