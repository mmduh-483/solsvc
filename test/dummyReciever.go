package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type OracleResponse struct {
	Token     string `json:"access_token"`
	TokenType string `json:"token_type"`
	Expiry    int    `json:"expires_in"`
}

func main() {
	http.HandleFunc("/receive", print)
	http.HandleFunc("/token", basicAuth(token, "admin", "password"))
	http.HandleFunc("/receive/basic", basicAuth(print, "admin", "password"))
	fmt.Printf("Starting server at port 8080\n")
	if err := http.ListenAndServe(":8080", nil); err != nil {
		fmt.Println(err)
	}
}

func token(w http.ResponseWriter, r *http.Request) {

	o := OracleResponse{
		Token:     "blahblab",
		TokenType: "Bearer",
		Expiry:    3600,
	}
	marshal, err := json.Marshal(o)
	if err != nil {
		fmt.Println("eror", err.Error())
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, err = w.Write(marshal)
	if err != nil {
		fmt.Println("eror", err.Error())
		return
	}
	fmt.Println("wrote:", o)
}

func basicAuth(next http.HandlerFunc, u, p string) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		username, password, ok := r.BasicAuth()
		fmt.Println("u,p = ", username, password)
		if ok && username == u && password == p {
			next.ServeHTTP(w, r)
			return
		}
		w.Header().Set("WWW-Authenticate", `Basic realm="restricted", charset="UTF-8"`)
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
	})
}

func print(w http.ResponseWriter, r *http.Request) {
	fmt.Println(r)
	defer r.Body.Close()
	b, err := ioutil.ReadAll(r.Body)
	fmt.Println("Recieved New Data")
	fmt.Println("Body ", string(b))
	fmt.Println("Error ", err)
	fmt.Fprintf(w, "Recieved")
}
